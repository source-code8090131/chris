namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_IncentiveBonusEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_IncentiveBonusEntry));
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.slTBOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.txtUser1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.slPanelBonusApproval = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.txt_Category = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_ID = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Promoted1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_JoiningDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_BO_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_BO_INC_APP = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_BO_INC_AMOUNT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_AVG_OT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_NO_OT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_AS_CTT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_GOVT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_LAST_BASIC = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_OT_AREAR = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_TOT_OT = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_PName = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txt_Year = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txt_Pr_P_No = new CrplControlLibrary.SLTextBox(this.components);
            this.lookup_Pr_p_No = new CrplControlLibrary.LookupButton(this.components);
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.slPanelBonusApproval.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(589, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(625, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 419);
            this.panel1.Size = new System.Drawing.Size(625, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.slTBOption);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.txtUser1);
            this.pnlHead.Controls.Add(this.txtLocation1);
            this.pnlHead.Controls.Add(this.label33);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label34);
            this.pnlHead.Controls.Add(this.label35);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label36);
            this.pnlHead.Controls.Add(this.label37);
            this.pnlHead.Location = new System.Drawing.Point(10, 83);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(610, 52);
            this.pnlHead.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(461, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 15);
            this.label3.TabIndex = 56;
            this.label3.Text = "Option:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(23, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "User :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTBOption
            // 
            this.slTBOption.AllowSpace = true;
            this.slTBOption.AssociatedLookUpName = "";
            this.slTBOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTBOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTBOption.ContinuationTextBox = null;
            this.slTBOption.CustomEnabled = true;
            this.slTBOption.DataFieldMapping = "";
            this.slTBOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTBOption.GetRecordsOnUpDownKeys = false;
            this.slTBOption.IsDate = false;
            this.slTBOption.Location = new System.Drawing.Point(520, 3);
            this.slTBOption.MaxLength = 10;
            this.slTBOption.Name = "slTBOption";
            this.slTBOption.NumberFormat = "###,###,##0.00";
            this.slTBOption.Postfix = "";
            this.slTBOption.Prefix = "";
            this.slTBOption.ReadOnly = true;
            this.slTBOption.Size = new System.Drawing.Size(80, 20);
            this.slTBOption.SkipValidation = false;
            this.slTBOption.TabIndex = 54;
            this.slTBOption.TabStop = false;
            this.slTBOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(1, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 53;
            this.label2.Text = "Location :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUser1
            // 
            this.txtUser1.AllowSpace = true;
            this.txtUser1.AssociatedLookUpName = "";
            this.txtUser1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser1.ContinuationTextBox = null;
            this.txtUser1.CustomEnabled = false;
            this.txtUser1.DataFieldMapping = "";
            this.txtUser1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser1.GetRecordsOnUpDownKeys = false;
            this.txtUser1.IsDate = false;
            this.txtUser1.Location = new System.Drawing.Point(83, 7);
            this.txtUser1.MaxLength = 10;
            this.txtUser1.Name = "txtUser1";
            this.txtUser1.NumberFormat = "###,###,##0.00";
            this.txtUser1.Postfix = "";
            this.txtUser1.Prefix = "";
            this.txtUser1.ReadOnly = true;
            this.txtUser1.Size = new System.Drawing.Size(87, 20);
            this.txtUser1.SkipValidation = false;
            this.txtUser1.TabIndex = 21;
            this.txtUser1.TabStop = false;
            this.txtUser1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocation1
            // 
            this.txtLocation1.AllowSpace = true;
            this.txtLocation1.AssociatedLookUpName = "";
            this.txtLocation1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation1.ContinuationTextBox = null;
            this.txtLocation1.CustomEnabled = false;
            this.txtLocation1.DataFieldMapping = "";
            this.txtLocation1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation1.GetRecordsOnUpDownKeys = false;
            this.txtLocation1.IsDate = false;
            this.txtLocation1.Location = new System.Drawing.Point(83, 29);
            this.txtLocation1.MaxLength = 10;
            this.txtLocation1.Name = "txtLocation1";
            this.txtLocation1.NumberFormat = "###,###,##0.00";
            this.txtLocation1.Postfix = "";
            this.txtLocation1.Prefix = "";
            this.txtLocation1.ReadOnly = true;
            this.txtLocation1.Size = new System.Drawing.Size(87, 20);
            this.txtLocation1.SkipValidation = false;
            this.txtLocation1.TabIndex = 13;
            this.txtLocation1.TabStop = false;
            this.txtLocation1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(190, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(276, 30);
            this.label33.TabIndex = 19;
            this.label33.Text = "PAYROLL SYSTEM \n INCENTIVE BONUS APPROVALS";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(520, 27);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(501, 77);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(473, 31);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 15);
            this.label34.TabIndex = 16;
            this.label34.Text = "Date:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(450, 79);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 15);
            this.label35.TabIndex = 15;
            this.label35.Text = "Option:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label35.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(83, 87);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(83, 61);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(7, 87);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 20);
            this.label36.TabIndex = 2;
            this.label36.Text = "Location:";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label36.Visible = false;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(16, 61);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 20);
            this.label37.TabIndex = 1;
            this.label37.Text = "User:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label37.Visible = false;
            // 
            // slPanelBonusApproval
            // 
            this.slPanelBonusApproval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPanelBonusApproval.ConcurrentPanels = null;
            this.slPanelBonusApproval.Controls.Add(this.label15);
            this.slPanelBonusApproval.Controls.Add(this.txt_Category);
            this.slPanelBonusApproval.Controls.Add(this.txt_ID);
            this.slPanelBonusApproval.Controls.Add(this.txt_Promoted1);
            this.slPanelBonusApproval.Controls.Add(this.txt_JoiningDate);
            this.slPanelBonusApproval.Controls.Add(this.txt_BO_P_NO);
            this.slPanelBonusApproval.Controls.Add(this.txt_BO_INC_APP);
            this.slPanelBonusApproval.Controls.Add(this.txt_BO_INC_AMOUNT);
            this.slPanelBonusApproval.Controls.Add(this.txt_W_AVG_OT);
            this.slPanelBonusApproval.Controls.Add(this.txt_W_NO_OT);
            this.slPanelBonusApproval.Controls.Add(this.txt_W_AS_CTT);
            this.slPanelBonusApproval.Controls.Add(this.txt_W_GOVT);
            this.slPanelBonusApproval.Controls.Add(this.txt_W_LAST_BASIC);
            this.slPanelBonusApproval.Controls.Add(this.txt_W_OT_AREAR);
            this.slPanelBonusApproval.Controls.Add(this.txt_W_TOT_OT);
            this.slPanelBonusApproval.Controls.Add(this.label14);
            this.slPanelBonusApproval.Controls.Add(this.label13);
            this.slPanelBonusApproval.Controls.Add(this.label12);
            this.slPanelBonusApproval.Controls.Add(this.label11);
            this.slPanelBonusApproval.Controls.Add(this.label10);
            this.slPanelBonusApproval.Controls.Add(this.label9);
            this.slPanelBonusApproval.Controls.Add(this.label8);
            this.slPanelBonusApproval.Controls.Add(this.label7);
            this.slPanelBonusApproval.Controls.Add(this.label6);
            this.slPanelBonusApproval.Controls.Add(this.txt_PName);
            this.slPanelBonusApproval.Controls.Add(this.label5);
            this.slPanelBonusApproval.Controls.Add(this.txt_Year);
            this.slPanelBonusApproval.Controls.Add(this.label4);
            this.slPanelBonusApproval.Controls.Add(this.txt_Pr_P_No);
            this.slPanelBonusApproval.Controls.Add(this.lookup_Pr_p_No);
            this.slPanelBonusApproval.DataManager = null;
            this.slPanelBonusApproval.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelBonusApproval.DependentPanels = null;
            this.slPanelBonusApproval.DisableDependentLoad = false;
            this.slPanelBonusApproval.EnableDelete = true;
            this.slPanelBonusApproval.EnableInsert = true;
            this.slPanelBonusApproval.EnableQuery = false;
            this.slPanelBonusApproval.EnableUpdate = true;
            this.slPanelBonusApproval.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BonusTabCommand";
            this.slPanelBonusApproval.Location = new System.Drawing.Point(10, 144);
            this.slPanelBonusApproval.MasterPanel = null;
            this.slPanelBonusApproval.Name = "slPanelBonusApproval";
            this.slPanelBonusApproval.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelBonusApproval.Size = new System.Drawing.Size(610, 259);
            this.slPanelBonusApproval.SPName = "CHRIS_SP_BONUS_TAB_MANAGER_EXPLICIT";
            this.slPanelBonusApproval.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(-4, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(751, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "_________________________________________________________________________________" +
                "___________________________________________";
            // 
            // txt_Category
            // 
            this.txt_Category.AllowSpace = true;
            this.txt_Category.AssociatedLookUpName = "";
            this.txt_Category.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Category.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Category.ContinuationTextBox = null;
            this.txt_Category.CustomEnabled = true;
            this.txt_Category.DataFieldMapping = "PR_CATEGORY";
            this.txt_Category.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Category.GetRecordsOnUpDownKeys = false;
            this.txt_Category.IsDate = false;
            this.txt_Category.Location = new System.Drawing.Point(206, 175);
            this.txt_Category.MaxLength = 4;
            this.txt_Category.Name = "txt_Category";
            this.txt_Category.NumberFormat = "###,###,##0.00";
            this.txt_Category.Postfix = "";
            this.txt_Category.Prefix = "";
            this.txt_Category.Size = new System.Drawing.Size(100, 20);
            this.txt_Category.SkipValidation = false;
            this.txt_Category.TabIndex = 26;
            this.txt_Category.TabStop = false;
            this.txt_Category.TextType = CrplControlLibrary.TextType.String;
            this.txt_Category.Visible = false;
            // 
            // txt_ID
            // 
            this.txt_ID.AllowSpace = true;
            this.txt_ID.AssociatedLookUpName = "";
            this.txt_ID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_ID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_ID.ContinuationTextBox = null;
            this.txt_ID.CustomEnabled = true;
            this.txt_ID.DataFieldMapping = "ID";
            this.txt_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ID.GetRecordsOnUpDownKeys = false;
            this.txt_ID.IsDate = false;
            this.txt_ID.Location = new System.Drawing.Point(83, 201);
            this.txt_ID.MaxLength = 4;
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.NumberFormat = "###,###,##0.00";
            this.txt_ID.Postfix = "";
            this.txt_ID.Prefix = "";
            this.txt_ID.Size = new System.Drawing.Size(100, 20);
            this.txt_ID.SkipValidation = false;
            this.txt_ID.TabIndex = 25;
            this.txt_ID.TabStop = false;
            this.txt_ID.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_ID.Visible = false;
            // 
            // txt_Promoted1
            // 
            this.txt_Promoted1.AllowSpace = true;
            this.txt_Promoted1.AssociatedLookUpName = "";
            this.txt_Promoted1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Promoted1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Promoted1.ContinuationTextBox = null;
            this.txt_Promoted1.CustomEnabled = true;
            this.txt_Promoted1.DataFieldMapping = "PR_JOINING_DATE";
            this.txt_Promoted1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Promoted1.GetRecordsOnUpDownKeys = false;
            this.txt_Promoted1.IsDate = false;
            this.txt_Promoted1.Location = new System.Drawing.Point(189, 74);
            this.txt_Promoted1.Name = "txt_Promoted1";
            this.txt_Promoted1.NumberFormat = "###,###,##0.00";
            this.txt_Promoted1.Postfix = "";
            this.txt_Promoted1.Prefix = "";
            this.txt_Promoted1.Size = new System.Drawing.Size(100, 20);
            this.txt_Promoted1.SkipValidation = false;
            this.txt_Promoted1.TabIndex = 24;
            this.txt_Promoted1.TabStop = false;
            this.txt_Promoted1.TextType = CrplControlLibrary.TextType.DateTime;
            this.txt_Promoted1.Visible = false;
            // 
            // txt_JoiningDate
            // 
            this.txt_JoiningDate.AllowSpace = true;
            this.txt_JoiningDate.AssociatedLookUpName = "";
            this.txt_JoiningDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_JoiningDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_JoiningDate.ContinuationTextBox = null;
            this.txt_JoiningDate.CustomEnabled = true;
            this.txt_JoiningDate.DataFieldMapping = "PR_JOINING_DATE";
            this.txt_JoiningDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_JoiningDate.GetRecordsOnUpDownKeys = false;
            this.txt_JoiningDate.IsDate = false;
            this.txt_JoiningDate.Location = new System.Drawing.Point(83, 74);
            this.txt_JoiningDate.Name = "txt_JoiningDate";
            this.txt_JoiningDate.NumberFormat = "###,###,##0.00";
            this.txt_JoiningDate.Postfix = "";
            this.txt_JoiningDate.Prefix = "";
            this.txt_JoiningDate.Size = new System.Drawing.Size(100, 20);
            this.txt_JoiningDate.SkipValidation = false;
            this.txt_JoiningDate.TabIndex = 23;
            this.txt_JoiningDate.TabStop = false;
            this.txt_JoiningDate.TextType = CrplControlLibrary.TextType.DateTime;
            this.txt_JoiningDate.Visible = false;
            // 
            // txt_BO_P_NO
            // 
            this.txt_BO_P_NO.AllowSpace = true;
            this.txt_BO_P_NO.AssociatedLookUpName = "";
            this.txt_BO_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_BO_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_BO_P_NO.ContinuationTextBox = null;
            this.txt_BO_P_NO.CustomEnabled = true;
            this.txt_BO_P_NO.DataFieldMapping = "BO_P_NO";
            this.txt_BO_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_BO_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_BO_P_NO.IsDate = false;
            this.txt_BO_P_NO.Location = new System.Drawing.Point(83, 175);
            this.txt_BO_P_NO.MaxLength = 4;
            this.txt_BO_P_NO.Name = "txt_BO_P_NO";
            this.txt_BO_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_BO_P_NO.Postfix = "";
            this.txt_BO_P_NO.Prefix = "";
            this.txt_BO_P_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_BO_P_NO.SkipValidation = false;
            this.txt_BO_P_NO.TabIndex = 22;
            this.txt_BO_P_NO.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_BO_P_NO.Visible = false;
            // 
            // txt_BO_INC_APP
            // 
            this.txt_BO_INC_APP.AllowSpace = true;
            this.txt_BO_INC_APP.AssociatedLookUpName = "";
            this.txt_BO_INC_APP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_BO_INC_APP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_BO_INC_APP.ContinuationTextBox = null;
            this.txt_BO_INC_APP.CustomEnabled = true;
            this.txt_BO_INC_APP.DataFieldMapping = "BO_INC_APP";
            this.txt_BO_INC_APP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_BO_INC_APP.GetRecordsOnUpDownKeys = false;
            this.txt_BO_INC_APP.IsDate = false;
            this.txt_BO_INC_APP.IsRequired = true;
            this.txt_BO_INC_APP.Location = new System.Drawing.Point(448, 235);
            this.txt_BO_INC_APP.MaxLength = 1;
            this.txt_BO_INC_APP.Name = "txt_BO_INC_APP";
            this.txt_BO_INC_APP.NumberFormat = "###,###,##0.00";
            this.txt_BO_INC_APP.Postfix = "";
            this.txt_BO_INC_APP.Prefix = "";
            this.txt_BO_INC_APP.Size = new System.Drawing.Size(33, 20);
            this.txt_BO_INC_APP.SkipValidation = false;
            this.txt_BO_INC_APP.TabIndex = 4;
            this.txt_BO_INC_APP.TextType = CrplControlLibrary.TextType.String;
            this.txt_BO_INC_APP.Validated += new System.EventHandler(this.txt_BO_INC_APP_Validated);
            this.txt_BO_INC_APP.Validating += new System.ComponentModel.CancelEventHandler(this.txt_BO_INC_APP_Validating);
            // 
            // txt_BO_INC_AMOUNT
            // 
            this.txt_BO_INC_AMOUNT.AllowSpace = true;
            this.txt_BO_INC_AMOUNT.AssociatedLookUpName = "";
            this.txt_BO_INC_AMOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_BO_INC_AMOUNT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_BO_INC_AMOUNT.ContinuationTextBox = null;
            this.txt_BO_INC_AMOUNT.CustomEnabled = true;
            this.txt_BO_INC_AMOUNT.DataFieldMapping = "BO_INC_AMOUNT";
            this.txt_BO_INC_AMOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_BO_INC_AMOUNT.GetRecordsOnUpDownKeys = false;
            this.txt_BO_INC_AMOUNT.IsDate = false;
            this.txt_BO_INC_AMOUNT.IsRequired = true;
            this.txt_BO_INC_AMOUNT.Location = new System.Drawing.Point(206, 235);
            this.txt_BO_INC_AMOUNT.MaxLength = 9;
            this.txt_BO_INC_AMOUNT.Name = "txt_BO_INC_AMOUNT";
            this.txt_BO_INC_AMOUNT.NumberFormat = "###,###,##0.00";
            this.txt_BO_INC_AMOUNT.Postfix = "";
            this.txt_BO_INC_AMOUNT.Prefix = "";
            this.txt_BO_INC_AMOUNT.Size = new System.Drawing.Size(90, 20);
            this.txt_BO_INC_AMOUNT.SkipValidation = false;
            this.txt_BO_INC_AMOUNT.TabIndex = 3;
            this.txt_BO_INC_AMOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_BO_INC_AMOUNT.TextType = CrplControlLibrary.TextType.Amount;
            this.txt_BO_INC_AMOUNT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_BO_INC_AMOUNT_KeyDown);
            this.txt_BO_INC_AMOUNT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_BO_INC_AMOUNT_KeyPress);
            this.txt_BO_INC_AMOUNT.Validating += new System.ComponentModel.CancelEventHandler(this.txt_BO_INC_AMOUNT_Validating);
            // 
            // txt_W_AVG_OT
            // 
            this.txt_W_AVG_OT.AllowSpace = true;
            this.txt_W_AVG_OT.AssociatedLookUpName = "";
            this.txt_W_AVG_OT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_AVG_OT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_AVG_OT.ContinuationTextBox = null;
            this.txt_W_AVG_OT.CustomEnabled = true;
            this.txt_W_AVG_OT.DataFieldMapping = "";
            this.txt_W_AVG_OT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_AVG_OT.GetRecordsOnUpDownKeys = false;
            this.txt_W_AVG_OT.IsDate = false;
            this.txt_W_AVG_OT.Location = new System.Drawing.Point(538, 97);
            this.txt_W_AVG_OT.Name = "txt_W_AVG_OT";
            this.txt_W_AVG_OT.NumberFormat = "###,###,##0.00";
            this.txt_W_AVG_OT.Postfix = "";
            this.txt_W_AVG_OT.Prefix = "";
            this.txt_W_AVG_OT.ReadOnly = true;
            this.txt_W_AVG_OT.Size = new System.Drawing.Size(63, 20);
            this.txt_W_AVG_OT.SkipValidation = false;
            this.txt_W_AVG_OT.TabIndex = 21;
            this.txt_W_AVG_OT.TabStop = false;
            this.txt_W_AVG_OT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_NO_OT
            // 
            this.txt_W_NO_OT.AllowSpace = true;
            this.txt_W_NO_OT.AssociatedLookUpName = "";
            this.txt_W_NO_OT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_NO_OT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_NO_OT.ContinuationTextBox = null;
            this.txt_W_NO_OT.CustomEnabled = true;
            this.txt_W_NO_OT.DataFieldMapping = "";
            this.txt_W_NO_OT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_NO_OT.GetRecordsOnUpDownKeys = false;
            this.txt_W_NO_OT.IsDate = false;
            this.txt_W_NO_OT.Location = new System.Drawing.Point(315, 98);
            this.txt_W_NO_OT.Name = "txt_W_NO_OT";
            this.txt_W_NO_OT.NumberFormat = "###,###,##0.00";
            this.txt_W_NO_OT.Postfix = "";
            this.txt_W_NO_OT.Prefix = "";
            this.txt_W_NO_OT.ReadOnly = true;
            this.txt_W_NO_OT.Size = new System.Drawing.Size(33, 20);
            this.txt_W_NO_OT.SkipValidation = false;
            this.txt_W_NO_OT.TabIndex = 20;
            this.txt_W_NO_OT.TabStop = false;
            this.txt_W_NO_OT.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_W_AS_CTT
            // 
            this.txt_W_AS_CTT.AllowSpace = true;
            this.txt_W_AS_CTT.AssociatedLookUpName = "";
            this.txt_W_AS_CTT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_AS_CTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_AS_CTT.ContinuationTextBox = null;
            this.txt_W_AS_CTT.CustomEnabled = true;
            this.txt_W_AS_CTT.DataFieldMapping = "";
            this.txt_W_AS_CTT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_AS_CTT.GetRecordsOnUpDownKeys = false;
            this.txt_W_AS_CTT.IsDate = false;
            this.txt_W_AS_CTT.Location = new System.Drawing.Point(538, 149);
            this.txt_W_AS_CTT.Name = "txt_W_AS_CTT";
            this.txt_W_AS_CTT.NumberFormat = "###,###,##0.00";
            this.txt_W_AS_CTT.Postfix = "";
            this.txt_W_AS_CTT.Prefix = "";
            this.txt_W_AS_CTT.ReadOnly = true;
            this.txt_W_AS_CTT.Size = new System.Drawing.Size(22, 20);
            this.txt_W_AS_CTT.SkipValidation = false;
            this.txt_W_AS_CTT.TabIndex = 19;
            this.txt_W_AS_CTT.TabStop = false;
            this.txt_W_AS_CTT.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_W_GOVT
            // 
            this.txt_W_GOVT.AllowSpace = true;
            this.txt_W_GOVT.AssociatedLookUpName = "";
            this.txt_W_GOVT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_GOVT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_GOVT.ContinuationTextBox = null;
            this.txt_W_GOVT.CustomEnabled = true;
            this.txt_W_GOVT.DataFieldMapping = "";
            this.txt_W_GOVT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_GOVT.GetRecordsOnUpDownKeys = false;
            this.txt_W_GOVT.IsDate = false;
            this.txt_W_GOVT.Location = new System.Drawing.Point(315, 149);
            this.txt_W_GOVT.Name = "txt_W_GOVT";
            this.txt_W_GOVT.NumberFormat = "###,###,##0.00";
            this.txt_W_GOVT.Postfix = "";
            this.txt_W_GOVT.Prefix = "";
            this.txt_W_GOVT.ReadOnly = true;
            this.txt_W_GOVT.Size = new System.Drawing.Size(79, 20);
            this.txt_W_GOVT.SkipValidation = false;
            this.txt_W_GOVT.TabIndex = 18;
            this.txt_W_GOVT.TabStop = false;
            this.txt_W_GOVT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_LAST_BASIC
            // 
            this.txt_W_LAST_BASIC.AllowSpace = true;
            this.txt_W_LAST_BASIC.AssociatedLookUpName = "";
            this.txt_W_LAST_BASIC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_LAST_BASIC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_LAST_BASIC.ContinuationTextBox = null;
            this.txt_W_LAST_BASIC.CustomEnabled = true;
            this.txt_W_LAST_BASIC.DataFieldMapping = "";
            this.txt_W_LAST_BASIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_LAST_BASIC.GetRecordsOnUpDownKeys = false;
            this.txt_W_LAST_BASIC.IsDate = false;
            this.txt_W_LAST_BASIC.Location = new System.Drawing.Point(83, 149);
            this.txt_W_LAST_BASIC.Name = "txt_W_LAST_BASIC";
            this.txt_W_LAST_BASIC.NumberFormat = "###,###,##0.00";
            this.txt_W_LAST_BASIC.Postfix = "";
            this.txt_W_LAST_BASIC.Prefix = "";
            this.txt_W_LAST_BASIC.ReadOnly = true;
            this.txt_W_LAST_BASIC.Size = new System.Drawing.Size(100, 20);
            this.txt_W_LAST_BASIC.SkipValidation = false;
            this.txt_W_LAST_BASIC.TabIndex = 17;
            this.txt_W_LAST_BASIC.TabStop = false;
            this.txt_W_LAST_BASIC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_W_LAST_BASIC.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txt_W_OT_AREAR
            // 
            this.txt_W_OT_AREAR.AllowSpace = true;
            this.txt_W_OT_AREAR.AssociatedLookUpName = "";
            this.txt_W_OT_AREAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OT_AREAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OT_AREAR.ContinuationTextBox = null;
            this.txt_W_OT_AREAR.CustomEnabled = true;
            this.txt_W_OT_AREAR.DataFieldMapping = "";
            this.txt_W_OT_AREAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OT_AREAR.GetRecordsOnUpDownKeys = false;
            this.txt_W_OT_AREAR.IsDate = false;
            this.txt_W_OT_AREAR.Location = new System.Drawing.Point(83, 123);
            this.txt_W_OT_AREAR.Name = "txt_W_OT_AREAR";
            this.txt_W_OT_AREAR.NumberFormat = "###,###,##0.00";
            this.txt_W_OT_AREAR.Postfix = "";
            this.txt_W_OT_AREAR.Prefix = "";
            this.txt_W_OT_AREAR.ReadOnly = true;
            this.txt_W_OT_AREAR.Size = new System.Drawing.Size(100, 20);
            this.txt_W_OT_AREAR.SkipValidation = false;
            this.txt_W_OT_AREAR.TabIndex = 16;
            this.txt_W_OT_AREAR.TabStop = false;
            this.txt_W_OT_AREAR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_W_OT_AREAR.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txt_W_TOT_OT
            // 
            this.txt_W_TOT_OT.AllowSpace = true;
            this.txt_W_TOT_OT.AssociatedLookUpName = "";
            this.txt_W_TOT_OT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_TOT_OT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_TOT_OT.ContinuationTextBox = null;
            this.txt_W_TOT_OT.CustomEnabled = true;
            this.txt_W_TOT_OT.DataFieldMapping = "";
            this.txt_W_TOT_OT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_TOT_OT.GetRecordsOnUpDownKeys = false;
            this.txt_W_TOT_OT.IsDate = false;
            this.txt_W_TOT_OT.Location = new System.Drawing.Point(83, 97);
            this.txt_W_TOT_OT.Name = "txt_W_TOT_OT";
            this.txt_W_TOT_OT.NumberFormat = "###,###,##0.00";
            this.txt_W_TOT_OT.Postfix = "";
            this.txt_W_TOT_OT.Prefix = "";
            this.txt_W_TOT_OT.ReadOnly = true;
            this.txt_W_TOT_OT.Size = new System.Drawing.Size(100, 20);
            this.txt_W_TOT_OT.SkipValidation = false;
            this.txt_W_TOT_OT.TabIndex = 15;
            this.txt_W_TOT_OT.TabStop = false;
            this.txt_W_TOT_OT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_W_TOT_OT.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(356, 237);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Approved [Y/N] ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(162, 237);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Bonus :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(401, 151);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(136, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "# Of Months As Clerk :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(186, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "# Of Months For OT :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(401, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Avg. OT Cost :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(206, 151);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Govt. Allowance :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Last Basic :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "OT Arears :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Total OT :";
            // 
            // txt_PName
            // 
            this.txt_PName.AllowSpace = true;
            this.txt_PName.AssociatedLookUpName = "";
            this.txt_PName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PName.ContinuationTextBox = null;
            this.txt_PName.CustomEnabled = true;
            this.txt_PName.DataFieldMapping = "PNAME";
            this.txt_PName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PName.GetRecordsOnUpDownKeys = false;
            this.txt_PName.IsDate = false;
            this.txt_PName.Location = new System.Drawing.Point(238, 34);
            this.txt_PName.Name = "txt_PName";
            this.txt_PName.NumberFormat = "###,###,##0.00";
            this.txt_PName.Postfix = "";
            this.txt_PName.Prefix = "";
            this.txt_PName.ReadOnly = true;
            this.txt_PName.Size = new System.Drawing.Size(324, 20);
            this.txt_PName.SkipValidation = false;
            this.txt_PName.TabIndex = 5;
            this.txt_PName.TabStop = false;
            this.txt_PName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "P. No :";
            // 
            // txt_Year
            // 
            this.txt_Year.AllowSpace = true;
            this.txt_Year.AssociatedLookUpName = "";
            this.txt_Year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Year.ContinuationTextBox = null;
            this.txt_Year.CustomEnabled = true;
            this.txt_Year.DataFieldMapping = "";
            this.txt_Year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Year.GetRecordsOnUpDownKeys = false;
            this.txt_Year.IsDate = false;
            this.txt_Year.Location = new System.Drawing.Point(83, 8);
            this.txt_Year.MaxLength = 4;
            this.txt_Year.Name = "txt_Year";
            this.txt_Year.NumberFormat = "###,###,##0.00";
            this.txt_Year.Postfix = "";
            this.txt_Year.Prefix = "";
            this.txt_Year.Size = new System.Drawing.Size(100, 20);
            this.txt_Year.SkipValidation = false;
            this.txt_Year.TabIndex = 1;
            this.txt_Year.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_Year.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Year_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Year   :";
            // 
            // txt_Pr_P_No
            // 
            this.txt_Pr_P_No.AllowSpace = true;
            this.txt_Pr_P_No.AssociatedLookUpName = "lookup_Pr_p_No";
            this.txt_Pr_P_No.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pr_P_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pr_P_No.ContinuationTextBox = null;
            this.txt_Pr_P_No.CustomEnabled = true;
            this.txt_Pr_P_No.DataFieldMapping = "PR_P_NO";
            this.txt_Pr_P_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pr_P_No.GetRecordsOnUpDownKeys = false;
            this.txt_Pr_P_No.IsDate = false;
            this.txt_Pr_P_No.IsLookUpField = true;
            this.txt_Pr_P_No.IsRequired = true;
            this.txt_Pr_P_No.Location = new System.Drawing.Point(83, 34);
            this.txt_Pr_P_No.MaxLength = 4;
            this.txt_Pr_P_No.Name = "txt_Pr_P_No";
            this.txt_Pr_P_No.NumberFormat = "###,###,##0.00";
            this.txt_Pr_P_No.Postfix = "";
            this.txt_Pr_P_No.Prefix = "";
            this.txt_Pr_P_No.Size = new System.Drawing.Size(100, 20);
            this.txt_Pr_P_No.SkipValidation = false;
            this.txt_Pr_P_No.TabIndex = 2;
            this.txt_Pr_P_No.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_Pr_P_No.Validated += new System.EventHandler(this.txt_Pr_P_No_Validated);
            // 
            // lookup_Pr_p_No
            // 
            this.lookup_Pr_p_No.ActionLOVExists = "Pr_P_No_LovExist";
            this.lookup_Pr_p_No.ActionType = "Pr_P_No_Lov";
            this.lookup_Pr_p_No.ConditionalFields = "";
            this.lookup_Pr_p_No.CustomEnabled = true;
            this.lookup_Pr_p_No.DataFieldMapping = "";
            this.lookup_Pr_p_No.DependentLovControls = "";
            this.lookup_Pr_p_No.HiddenColumns = "PR_JOINING_DATE|BO_INC_AMOUNT|ID|BO_INC_APP|BO_P_NO|Column1";
            this.lookup_Pr_p_No.Image = ((System.Drawing.Image)(resources.GetObject("lookup_Pr_p_No.Image")));
            this.lookup_Pr_p_No.LoadDependentEntities = false;
            this.lookup_Pr_p_No.Location = new System.Drawing.Point(189, 33);
            this.lookup_Pr_p_No.LookUpTitle = null;
            this.lookup_Pr_p_No.Name = "lookup_Pr_p_No";
            this.lookup_Pr_p_No.Size = new System.Drawing.Size(26, 21);
            this.lookup_Pr_p_No.SkipValidationOnLeave = false;
            this.lookup_Pr_p_No.SPName = "CHRIS_SP_PERSONNEL_MANAGER";
            this.lookup_Pr_p_No.TabIndex = 0;
            this.lookup_Pr_p_No.TabStop = false;
            this.lookup_Pr_p_No.UseVisualStyleBackColor = true;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(434, 6);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 27;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Payroll_IncentiveBonusEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(625, 479);
            this.Controls.Add(this.slPanelBonusApproval);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "slPanelBonusApproval";
            this.CurrrentOptionTextBox = this.slTBOption;
            this.Name = "CHRIS_Payroll_IncentiveBonusEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Payroll_IncentiveBonusEntry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.slPanelBonusApproval, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.slPanelBonusApproval.ResumeLayout(false);
            this.slPanelBonusApproval.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox slTBOption;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtUser1;
        private CrplControlLibrary.SLTextBox txtLocation1;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelBonusApproval;
        private CrplControlLibrary.LookupButton lookup_Pr_p_No;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txt_Year;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txt_Pr_P_No;
        private CrplControlLibrary.SLTextBox txt_PName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txt_W_AS_CTT;
        private CrplControlLibrary.SLTextBox txt_W_GOVT;
        private CrplControlLibrary.SLTextBox txt_W_LAST_BASIC;
        private CrplControlLibrary.SLTextBox txt_W_OT_AREAR;
        private CrplControlLibrary.SLTextBox txt_W_TOT_OT;
        private CrplControlLibrary.SLTextBox txt_W_AVG_OT;
        private CrplControlLibrary.SLTextBox txt_W_NO_OT;
        private CrplControlLibrary.SLTextBox txt_BO_INC_AMOUNT;
        private CrplControlLibrary.SLTextBox txt_BO_INC_APP;
        private CrplControlLibrary.SLTextBox txt_BO_P_NO;
        private CrplControlLibrary.SLTextBox txt_JoiningDate;
        private CrplControlLibrary.SLTextBox txt_Promoted1;
        private CrplControlLibrary.SLTextBox txt_ID;
        private CrplControlLibrary.SLTextBox txt_Category;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label15;
    }
}