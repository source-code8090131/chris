namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_StandardTimings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPnlHeader = new iCORE.COMMON.SLCONTROLS.SLPanel(this.components);
            this.lblHeader = new System.Windows.Forms.Label();
            this.slPnlItems = new iCORE.COMMON.SLCONTROLS.SLPanel(this.components);
            this.lblShiftC = new System.Windows.Forms.Label();
            this.lblShiftB = new System.Windows.Forms.Label();
            this.lblShiftA = new System.Windows.Forms.Label();
            this.txtW03 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtW13 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtW01 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtW02 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtW12 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtW11 = new CrplControlLibrary.SLTextBox(this.components);
            this.lblSubHeader = new System.Windows.Forms.Label();
            this.slPnlHeader.SuspendLayout();
            this.slPnlItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPnlHeader
            // 
            this.slPnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPnlHeader.ConcurrentPanels = null;
            this.slPnlHeader.Controls.Add(this.lblHeader);
            this.slPnlHeader.DataManager = null;
            this.slPnlHeader.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlHeader.DependentPanels = null;
            this.slPnlHeader.DisableDependentLoad = false;
            this.slPnlHeader.EnableDelete = true;
            this.slPnlHeader.EnableInsert = true;
            this.slPnlHeader.EnableQuery = false;
            this.slPnlHeader.EnableUpdate = true;
            this.slPnlHeader.EntityName = null;
            this.slPnlHeader.Location = new System.Drawing.Point(62, 3);
            this.slPnlHeader.MasterPanel = null;
            this.slPnlHeader.Name = "slPnlHeader";
            this.slPnlHeader.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlHeader.Size = new System.Drawing.Size(230, 48);
            this.slPnlHeader.SPName = null;
            this.slPnlHeader.TabIndex = 15;
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(47, 14);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(142, 15);
            this.lblHeader.TabIndex = 9;
            this.lblHeader.Text = " Shift Standard Timing\'s";
            this.lblHeader.Click += new System.EventHandler(this.lblHeader_Click);
            // 
            // slPnlItems
            // 
            this.slPnlItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPnlItems.ConcurrentPanels = null;
            this.slPnlItems.Controls.Add(this.lblShiftC);
            this.slPnlItems.Controls.Add(this.lblShiftB);
            this.slPnlItems.Controls.Add(this.lblShiftA);
            this.slPnlItems.Controls.Add(this.txtW03);
            this.slPnlItems.Controls.Add(this.txtW13);
            this.slPnlItems.Controls.Add(this.txtW01);
            this.slPnlItems.Controls.Add(this.txtW02);
            this.slPnlItems.Controls.Add(this.txtW12);
            this.slPnlItems.Controls.Add(this.txtW11);
            this.slPnlItems.Controls.Add(this.lblSubHeader);
            this.slPnlItems.DataManager = null;
            this.slPnlItems.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlItems.DependentPanels = null;
            this.slPnlItems.DisableDependentLoad = false;
            this.slPnlItems.EnableDelete = true;
            this.slPnlItems.EnableInsert = true;
            this.slPnlItems.EnableQuery = false;
            this.slPnlItems.EnableUpdate = true;
            this.slPnlItems.EntityName = null;
            this.slPnlItems.Location = new System.Drawing.Point(12, 57);
            this.slPnlItems.MasterPanel = null;
            this.slPnlItems.Name = "slPnlItems";
            this.slPnlItems.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlItems.Size = new System.Drawing.Size(323, 138);
            this.slPnlItems.SPName = null;
            this.slPnlItems.TabIndex = 14;
            // 
            // lblShiftC
            // 
            this.lblShiftC.AutoSize = true;
            this.lblShiftC.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShiftC.Location = new System.Drawing.Point(24, 102);
            this.lblShiftC.Name = "lblShiftC";
            this.lblShiftC.Size = new System.Drawing.Size(27, 15);
            this.lblShiftC.TabIndex = 13;
            this.lblShiftC.Text = " [C]";
            // 
            // lblShiftB
            // 
            this.lblShiftB.AutoSize = true;
            this.lblShiftB.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShiftB.Location = new System.Drawing.Point(24, 76);
            this.lblShiftB.Name = "lblShiftB";
            this.lblShiftB.Size = new System.Drawing.Size(27, 15);
            this.lblShiftB.TabIndex = 12;
            this.lblShiftB.Text = " [B]";
            // 
            // lblShiftA
            // 
            this.lblShiftA.AutoSize = true;
            this.lblShiftA.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShiftA.Location = new System.Drawing.Point(24, 50);
            this.lblShiftA.Name = "lblShiftA";
            this.lblShiftA.Size = new System.Drawing.Size(27, 15);
            this.lblShiftA.TabIndex = 11;
            this.lblShiftA.Text = " [A]";
            // 
            // txtW03
            // 
            this.txtW03.AllowSpace = true;
            this.txtW03.AssociatedLookUpName = "";
            this.txtW03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW03.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW03.ContinuationTextBox = null;
            this.txtW03.CustomEnabled = true;
            this.txtW03.DataFieldMapping = "";
            this.txtW03.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW03.GetRecordsOnUpDownKeys = false;
            this.txtW03.IsDate = false;
            this.txtW03.Location = new System.Drawing.Point(228, 99);
            this.txtW03.MaxLength = 5;
            this.txtW03.Name = "txtW03";
            this.txtW03.NumberFormat = "###,###,##0.00";
            this.txtW03.Postfix = "";
            this.txtW03.Prefix = "";
            this.txtW03.Size = new System.Drawing.Size(62, 20);
            this.txtW03.SkipValidation = false;
            this.txtW03.TabIndex = 10;
            this.txtW03.TextType = CrplControlLibrary.TextType.String;
            this.txtW03.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtW03_KeyPress);
            // 
            // txtW13
            // 
            this.txtW13.AllowSpace = true;
            this.txtW13.AssociatedLookUpName = "";
            this.txtW13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW13.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW13.ContinuationTextBox = null;
            this.txtW13.CustomEnabled = true;
            this.txtW13.DataFieldMapping = "";
            this.txtW13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW13.GetRecordsOnUpDownKeys = false;
            this.txtW13.IsDate = false;
            this.txtW13.Location = new System.Drawing.Point(113, 99);
            this.txtW13.MaxLength = 5;
            this.txtW13.Name = "txtW13";
            this.txtW13.NumberFormat = "###,###,##0.00";
            this.txtW13.Postfix = "";
            this.txtW13.Prefix = "";
            this.txtW13.Size = new System.Drawing.Size(62, 20);
            this.txtW13.SkipValidation = false;
            this.txtW13.TabIndex = 10;
            this.txtW13.TextType = CrplControlLibrary.TextType.String;
            this.txtW13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtW13_KeyPress);
            // 
            // txtW01
            // 
            this.txtW01.AllowSpace = true;
            this.txtW01.AssociatedLookUpName = "";
            this.txtW01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW01.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW01.ContinuationTextBox = null;
            this.txtW01.CustomEnabled = true;
            this.txtW01.DataFieldMapping = "";
            this.txtW01.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW01.GetRecordsOnUpDownKeys = false;
            this.txtW01.IsDate = false;
            this.txtW01.Location = new System.Drawing.Point(228, 47);
            this.txtW01.MaxLength = 5;
            this.txtW01.Name = "txtW01";
            this.txtW01.NumberFormat = "###,###,##0.00";
            this.txtW01.Postfix = "";
            this.txtW01.Prefix = "";
            this.txtW01.Size = new System.Drawing.Size(62, 20);
            this.txtW01.SkipValidation = false;
            this.txtW01.TabIndex = 10;
            this.txtW01.TextType = CrplControlLibrary.TextType.String;
            this.txtW01.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtW01_KeyPress);
            // 
            // txtW02
            // 
            this.txtW02.AllowSpace = true;
            this.txtW02.AssociatedLookUpName = "";
            this.txtW02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW02.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW02.ContinuationTextBox = null;
            this.txtW02.CustomEnabled = true;
            this.txtW02.DataFieldMapping = "";
            this.txtW02.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW02.GetRecordsOnUpDownKeys = false;
            this.txtW02.IsDate = false;
            this.txtW02.Location = new System.Drawing.Point(228, 73);
            this.txtW02.MaxLength = 5;
            this.txtW02.Name = "txtW02";
            this.txtW02.NumberFormat = "###,###,##0.00";
            this.txtW02.Postfix = "";
            this.txtW02.Prefix = "";
            this.txtW02.Size = new System.Drawing.Size(62, 20);
            this.txtW02.SkipValidation = false;
            this.txtW02.TabIndex = 10;
            this.txtW02.TextType = CrplControlLibrary.TextType.String;
            this.txtW02.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtW02_KeyPress);
            // 
            // txtW12
            // 
            this.txtW12.AllowSpace = true;
            this.txtW12.AssociatedLookUpName = "";
            this.txtW12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW12.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW12.ContinuationTextBox = null;
            this.txtW12.CustomEnabled = true;
            this.txtW12.DataFieldMapping = "";
            this.txtW12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW12.GetRecordsOnUpDownKeys = false;
            this.txtW12.IsDate = false;
            this.txtW12.Location = new System.Drawing.Point(113, 73);
            this.txtW12.MaxLength = 5;
            this.txtW12.Name = "txtW12";
            this.txtW12.NumberFormat = "###,###,##0.00";
            this.txtW12.Postfix = "";
            this.txtW12.Prefix = "";
            this.txtW12.Size = new System.Drawing.Size(62, 20);
            this.txtW12.SkipValidation = false;
            this.txtW12.TabIndex = 10;
            this.txtW12.TextType = CrplControlLibrary.TextType.String;
            this.txtW12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtW12_KeyPress);
            // 
            // txtW11
            // 
            this.txtW11.AllowSpace = true;
            this.txtW11.AssociatedLookUpName = "";
            this.txtW11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW11.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW11.ContinuationTextBox = null;
            this.txtW11.CustomEnabled = true;
            this.txtW11.DataFieldMapping = "";
            this.txtW11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW11.GetRecordsOnUpDownKeys = false;
            this.txtW11.IsDate = false;
            this.txtW11.Location = new System.Drawing.Point(113, 47);
            this.txtW11.MaxLength = 5;
            this.txtW11.Name = "txtW11";
            this.txtW11.NumberFormat = "###,###,##0.00";
            this.txtW11.Postfix = "";
            this.txtW11.Prefix = "";
            this.txtW11.Size = new System.Drawing.Size(62, 20);
            this.txtW11.SkipValidation = false;
            this.txtW11.TabIndex = 10;
            this.txtW11.TextType = CrplControlLibrary.TextType.String;
            this.txtW11.Leave += new System.EventHandler(this.txtW11_Leave);
            this.txtW11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtW11_KeyPress);
            // 
            // lblSubHeader
            // 
            this.lblSubHeader.AutoSize = true;
            this.lblSubHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubHeader.Location = new System.Drawing.Point(9, 17);
            this.lblSubHeader.Name = "lblSubHeader";
            this.lblSubHeader.Size = new System.Drawing.Size(284, 15);
            this.lblSubHeader.TabIndex = 9;
            this.lblSubHeader.Text = "   Shift                       Time In                        Time Out  ";
            // 
            // CHRIS_Payroll_StandardTimings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(347, 218);
            this.Controls.Add(this.slPnlHeader);
            this.Controls.Add(this.slPnlItems);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Payroll_StandardTimings";
            this.Text = "iCORE CHRIS :  Shift Entry - Standard Timings";
            this.slPnlHeader.ResumeLayout(false);
            this.slPnlHeader.PerformLayout();
            this.slPnlItems.ResumeLayout(false);
            this.slPnlItems.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanel slPnlItems;
        private CrplControlLibrary.SLTextBox txtW11;
        private System.Windows.Forms.Label lblSubHeader;
        private iCORE.COMMON.SLCONTROLS.SLPanel slPnlHeader;
        private System.Windows.Forms.Label lblHeader;
        private CrplControlLibrary.SLTextBox txtW03;
        private CrplControlLibrary.SLTextBox txtW13;
        private CrplControlLibrary.SLTextBox txtW01;
        private CrplControlLibrary.SLTextBox txtW02;
        private CrplControlLibrary.SLTextBox txtW12;
        private System.Windows.Forms.Label lblShiftC;
        private System.Windows.Forms.Label lblShiftB;
        private System.Windows.Forms.Label lblShiftA;
    }
}