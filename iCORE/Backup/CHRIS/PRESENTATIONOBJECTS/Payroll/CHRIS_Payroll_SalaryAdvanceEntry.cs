using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;




namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_SalaryAdvanceEntry : ChrisSimpleForm
    {

        #region Members

        CmnDataManager cmnDM = new CmnDataManager();
        int globalNum;
        private double _saAdvance = 0;

        double global_sa_advance = 0;
        double global_Pr = 0;
        double global_mn = 0;
        int global_ee = 0;
        bool isSaved = false;

        /*-----------------------------To store data on form level------------------------------*/
        Dictionary<string, object> dicAdvanceSalary = new Dictionary<string, object>();

        /*----------------------To store input parameters of StoredProcedure--------------------*/
        Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();

        #endregion

        #region Constructors

        public CHRIS_Payroll_SalaryAdvanceEntry()
        {
            InitializeComponent();
        }
        // int w_val1 = 0;

        public CHRIS_Payroll_SalaryAdvanceEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            base.DoToolbarActions(ctrlsCollection, actionType);
            if (actionType == "Cancel")
            {
                InitializeBlockVaraibles();
                this.txtPersonnel.Text = "";
                this.txtName.Text = "";
                this.txtMonth1.Text = "";
                this.txtMonth2.Text = "";
                this.txtAmount.Text = "";
                this.txtHiddenField.Text = "";
                this.txtMonth1.ReadOnly = false;
                this.txtMonth2.ReadOnly = false;
                lbtn.SkipValidationOnLeave = false;
                this.FunctionConfig.CurrentOption = Function.None;
                DtSdate.Value = null;
                this.txtOption.Text = "";
                this.txtOption.Focus();
                lbtn.Enabled = false;
                lbtn.ActionLOVExists = "PR_P_NO_Exists";
                lbtn.ActionType = "BLK_P_PR_P_NO_LOV0";
                this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            }

        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();

            this.pnlHead.SendToBack();
            this.txtOption.Select();
            this.txtOption.Focus();

            //this.txtLocation.Text = this.CurrentLocation;
            this.lblUserName.Text = this.userID;
            this.DtSdate.Value = null;

            this.tbtList.Available = false;
            this.tbtEdit.Available = false;
            this.tbtSave.Available = false;
            this.tbtDelete.Visible = false;
            this.tsOptions.Text = "OPTIONS:";
            this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            this.FunctionConfig.EnableF8 = false;
            lbtn.Enabled = false;
            InitializeBlockVaraibles();
            this.tbtAdd.Visible = false;
        }

        protected override bool View()
        {
            base.View();

            CHRIS_Payroll_SalaryAdvanceEntryGrid gridfrm = new CHRIS_Payroll_SalaryAdvanceEntryGrid();
            gridfrm.ShowDialog();

            this.Cancel();

            this.txtDate.Text = this.CurrentDate.ToString("MM/dd/yyyy");
            lbtn.Enabled = true;
            this.DtSdate.Value = null;

            return false;

        }
        protected override bool Delete()
        {
            this.Cancel();

            bool flag = false;

            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Delete";
            this.txtOption.Text = "D";
            this.FunctionConfig.CurrentOption = Function.Delete;
            lbtn.Enabled = true;
            lbtn.ActionLOVExists = "PR_P_NO_MODIFY_DEL_LOV_EXIST";
            lbtn.ActionType = "PR_P_NO_MODIFY_DEL_LOV";
            //this.tbtDelete.PerformClick();
            this.txtPersonnel.Select();
            this.txtPersonnel.Focus();

            return flag;

        }
        protected override bool Add()
        {

            base.Add();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            lbtn.Enabled = true;
            lbtn.ActionLOVExists = "PR_P_NO_Exists";
            lbtn.ActionType = "BLK_P_PR_P_NO_LOV0";
            this.DtSdate.Value = null;//DateTime.Now.Date;//null;
            this.txtPersonnel.Select();
            this.txtPersonnel.Focus();

            return false;
        }
        protected override bool Cancel()
        {
            bool flag = false;
            base.Cancel();
            //this.tbtCancel.PerformClick();
            // base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
            lbtn.Enabled = false;
            this.txtMonth1.ReadOnly = false;
            this.txtMonth2.ReadOnly = false;
            return flag;
        }
        protected override bool Edit()
        {
            bool flag = false;
            base.Edit();
            lbtn.Enabled = true;
            lbtn.ActionLOVExists = "PR_P_NO_MODIFY_DEL_LOV_EXIST";
            lbtn.ActionType = "PR_P_NO_MODIFY_DEL_LOV";
            return flag;
        }

        /// <summary>
        /// Initialize Block level variables
        /// </summary>
        void InitializeBlockVaraibles()
        {
            try
            {
                dicAdvanceSalary.Clear();

                //-----BLK_HEAD---------16 ITEMS
                dicAdvanceSalary.Add("BLK_HEAD_W_OPTION", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_CNT", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_CATEGORY", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_VAL1", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_VAL2", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_BASIC", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_SUM", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_YEAR", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_FLAG", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_YSY", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_ANSWER", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_ANSWER3", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_TOTAL", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_DIS", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_USER", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_LOC", null);

                //-------BLK_P-----------2 ITEMS
                dicAdvanceSalary.Add("BLK_P_PR_P_NO", null);
                dicAdvanceSalary.Add("BLK_P_PR_FIRST_NAME", null);

                //-----SAL_ADVANCE---------5 ITEMS
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_P_NO", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_DATE", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_FOR_THE_MONTH1", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_FOR_THE_MONTH2", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_ADV_AMOUNT", null);

                //------PER-----------2 ITEMS
                dicAdvanceSalary.Add("PER_PR_P_NO", null);
                dicAdvanceSalary.Add("PER_PR_FIRST_NAME", null);

                //-----SAL_ADVANCE1-------5 ITEMS
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_P_NO", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_W_NAME", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_FOR_THE_MONTH1", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_FOR_THE_MONTH2", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_ADV_AMOUNT", null);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// check personnel exist 
        /// </summary>
        /// <returns>
        /// return true if personnel exist
        /// return false if personnel not exist
        /// </returns>
        bool CheckPersonnelExist()
        {
            bool isPersonnelExist = false;

            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("SA_P_NO", (object)this.txtPersonnel.Text);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "PerNo", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            dicAdvanceSalary["BLK_P_PR_P_NO"] = rslt.dstResult.Tables[0].Rows[0]["PR_P_NO"];
                            global_Pr = Convert.ToDouble(rslt.dstResult.Tables[0].Rows[0]["PR_P_NO"]);
                            dicAdvanceSalary["BLK_P_PR_FIRST_NAME"] = rslt.dstResult.Tables[0].Rows[0]["PR_FIRST_NAME"];
                            dicAdvanceSalary["BLK_HEAD_W_CATEGORY"] = rslt.dstResult.Tables[0].Rows[0]["pr_category"];

                            this.txtName.Text = dicAdvanceSalary["BLK_P_PR_FIRST_NAME"].ToString();

                            isPersonnelExist = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isPersonnelExist;
        }

        /// <summary>
        /// Perform specified operation (Addition, Modification and Deletion)
        /// </summary>
        void PerformOption()
        {
            try
            {
                Result rsltSalary = new Result();
                CmnDataManager objCmnDataManager = new CmnDataManager();

                dicAdvanceSalary["SAL_ADVANCE_SA_P_NO"] = this.txtPersonnel.Text;

                if (this.txtOption.Text == "A")
                {
                    #region Insertion

                    //system date Time
                    dicAdvanceSalary["BLK_P_W_DATE"] = base.Now().ToString("dd/MM/yyyy");// System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                    this.txtDate.Text = base.Now().ToString("dd/MM/yyyy");//System.DateTime.Now.Date.ToString("dd/MM/yyyy");

                    dicInputParameters.Clear();
                    dicInputParameters.Add("SA_P_NO", (object)this.txtPersonnel.Text);
                    rsltSalary = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "Check_Insertion", dicInputParameters);

                    if (rsltSalary.isSuccessful)
                    {
                        if (rsltSalary.dstResult.Tables.Count > 0)
                        {
                            if (rsltSalary.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dicAdvanceSalary["BLK_HEAD_W_VAL1"] = rsltSalary.dstResult.Tables[0].Rows[0]["SA_FOR_THE_MONTH1"].ToString() == "" ? "00" : rsltSalary.dstResult.Tables[0].Rows[0]["SA_FOR_THE_MONTH1"];
                                dicAdvanceSalary["BLK_HEAD_W_VAL2"] = rsltSalary.dstResult.Tables[0].Rows[0]["SA_FOR_THE_MONTH2"].ToString() == "" ? "00" : rsltSalary.dstResult.Tables[0].Rows[0]["SA_FOR_THE_MONTH2"];

                                /** If Advance Has Been Taken For Both The Months **/
                                if ((Convert.ToInt32(dicAdvanceSalary["BLK_HEAD_W_VAL1"]) != 00) && (Convert.ToInt32(dicAdvanceSalary["BLK_HEAD_W_VAL2"]) != 00))
                                {
                                    MessageBox.Show("VALUE IS ALREADY THERE FOR THE CURRENT MONTH");
                                    this.txtPersonnel.Text = "";
                                    this.txtName.Text = "";
                                    this.txtPersonnel.Focus();
                                    return;
                                }
                                /** If Advance is Taken For Month 1 & Not Month 2 **/
                                else if ((Convert.ToInt32(dicAdvanceSalary["BLK_HEAD_W_VAL1"]) != 00) && (Convert.ToInt32(dicAdvanceSalary["BLK_HEAD_W_VAL2"]) == 00))
                                {
                                    SalAdvanceExecuteQuery();
                                    this.txtMonth2.Focus();
                                    return;
                                }
                            }
                            else
                            {
                                //No data found exception
                                dicAdvanceSalary["SAL_ADVANCE_SA_DATE"] = this.CurrentDate.ToString("dd/MM/yyyy"); ;
                                this.DtSdate.Value = base.Now();//DateTime.Now.Date;//this.CurrentDate;
                                //this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
                            }
                        }
                    }
                    #endregion
                }
                else if ((this.txtOption.Text == "M") || (this.txtOption.Text == "D"))
                {
                    SalAdvanceExecuteQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check Modification and Deletion
        /// </summary>
        void SalAdvanceExecuteQuery()
        {
            try
            {
                if (FunctionConfig.CurrentOption != Function.Delete && FunctionConfig.CurrentOption != Function.Modify)
                {
                    this.txtHiddenField.Text = string.Empty;
                    dicInputParameters.Clear();
                    dicInputParameters.Add("SA_P_NO", (object)this.txtPersonnel.Text);

                    CmnDataManager objCmnDataManager = new CmnDataManager();
                    Result rslt = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "execute", dicInputParameters);

                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                this.txtMonth1.Text = rslt.dstResult.Tables[0].Rows[0]["SA_FOR_THE_MONTH1"].ToString();
                                this.txtMonth2.Text = rslt.dstResult.Tables[0].Rows[0]["SA_FOR_THE_MONTH2"].ToString();
                                this.txtAmount.Text = rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"].ToString();
                                DateTime dtSA_DATE = DateTime.Parse(rslt.dstResult.Tables[0].Rows[0]["SA_DATE"].ToString());
                                this.DtSdate.Value = dtSA_DATE;
                                this.txtHiddenField.Text = rslt.dstResult.Tables[0].Rows[0]["ID"].ToString();
                                dicAdvanceSalary["SAL_ADVANCE_SA_DATE"] = dtSA_DATE.ToString("dd/MM/yyyy");
                                this.txtMonth1.ReadOnly = true;
                                this.txtMonth2.ReadOnly = true;
                            }

                        }
                    }
                }
                else
                {

                    this.txtHiddenField.Text = txtID.Text;
                    if (this.DtSdate.Value != null)
                        dicAdvanceSalary["SAL_ADVANCE_SA_DATE"] = DtSdate.Value;
                    this.txtMonth1.ReadOnly = true;
                    this.txtMonth2.ReadOnly = true;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate Salary Amount
        /// </summary>
        void CalculateSalaryAmount()
        {
            try
            {
                #region when validate item handling

                dicInputParameters.Clear();
                dicInputParameters.Add("SA_P_NO", (object)this.txtPersonnel.Text);
                dicInputParameters.Add("SA_FOR_THE_MONTH2", this.txtMonth2.Text);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "SALADV", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"] != null)
                            {
                                if (this.FunctionConfig.CurrentOption != Function.Modify)
                                {
                                    this.txtAmount.Text = rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"].ToString();
                                }
                                dicAdvanceSalary["SAL_ADVANCE_SA_ADV_AMOUNT"] = rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"].ToString();
                                global_sa_advance = double.Parse(dicAdvanceSalary["SAL_ADVANCE_SA_ADV_AMOUNT"].ToString());



                            }
                        }
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Reset Controls
        /// </summary>
        void ResetControls()
        {
            this.Cancel();
            InitializeBlockVaraibles();
            this.txtPersonnel.Text = "";
            this.txtName.Text = "";
            this.txtMonth1.Text = "";
            this.txtMonth2.Text = "";
            this.txtMonth1.ReadOnly = false;
            this.txtMonth2.ReadOnly = false;
            this.txtAmount.Text = "";
            this.txtHiddenField.Text = "";
            this.FunctionConfig.CurrentOption = Function.None;
            lbtn.ActionLOVExists = "PR_P_NO_Exists";
            lbtn.ActionType = "BLK_P_PR_P_NO_LOV0";
            lbtn.SkipValidationOnLeave = false;
            DtSdate.Value = null;
            this.txtOption.Text = "";
            this.txtOption.Focus();
        }

        /// <summary>
        /// Save Record
        /// </summary>
        void SaveRecord()
        {
            DialogResult dRes = MessageBox.Show("DO YOU WANT TO SAVE THE CHANGES [Y]es [N]o", "Note"
                               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {
                if (this.FunctionConfig.CurrentOption == Function.Add)
                {
                    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                    isSaved = true;
                    base.DoToolbarActions(this.Controls, "Save");
                    ResetControls();
                }
                else if (this.FunctionConfig.CurrentOption == Function.Modify)
                {
                    if (this.txtHiddenField.Text.Trim() == string.Empty || this.txtHiddenField.Text.Trim() == "" || this.txtHiddenField.Text.Trim() == "0")
                    {
                        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                        isSaved = true;
                        base.DoToolbarActions(this.Controls, "Save");
                    }
                    else
                        UpdateRecord();
                    ResetControls();
                }

            }
            else if (dRes == DialogResult.No)
            {
                ResetControls();
                return;
            }
        }

        /// <summary>
        /// Update Record
        /// </summary>
        void UpdateRecord()
        {
            try
            {
                #region when validate item handling

                dicInputParameters.Clear();
                dicInputParameters.Add("ID", (object)this.txtHiddenField.Text);
                dicInputParameters.Add("SA_FOR_THE_MONTH1", (object)this.txtMonth1.Text);
                dicInputParameters.Add("SA_FOR_THE_MONTH2", (object)this.txtMonth2.Text);
                dicInputParameters.Add("SA_ADV_AMOUNT", (object)this.txtAmount.Text);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "Update", dicInputParameters);

                #endregion

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Delete Record
        /// </summary>
        void DeleteRecord()
        {

            global_Pr = double.Parse(dicAdvanceSalary["SAL_ADVANCE_SA_P_NO"].ToString());
            global_mn = double.Parse(dicAdvanceSalary["SAL_ADVANCE_SA_FOR_THE_MONTH1"].ToString());

            DialogResult dRes = MessageBox.Show("DO YOU WANT TO DELETE THE RECORD [Y]es [N]o", "Note"
                               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("ID", (object)this.txtID.Text);
                //dicInputParameters.Add("SA_P_NO", (object)this.txtPersonnel.Text);
                //dicInputParameters.Add("SA_FOR_THE_MONTH1", (object)this.txtMonth1.Text);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "Delete_Query", dicInputParameters);
                if (rslt.isSuccessful)
                {
                    ResetControls();
                    return;
                }
            }
            else if (dRes == DialogResult.No)
            {
                ResetControls();
                return;
            }
        }

        #endregion

        #region Event Handlers

        private void txtPersonnel_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.txtPersonnel.Text != string.Empty)
                {
                    if (CheckPersonnelExist())
                    {
                        if (txtAmount.Text.Trim() != string.Empty)
                            lbtn.SkipValidationOnLeave = true;
                        isSaved = false;
                        //If personnel exist then perform desired operation
                        PerformOption();
                    }
                    else
                    {
                        MessageBox.Show("NO DATA FOR THIS Per No.PRESS <F6> Exit W/O Save");
                        this.txtPersonnel.Focus();
                        this.txtPersonnel.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void txtMonth1_Leave(object sender, EventArgs e)
        {
            //try
            //{
            //    if (this.txtMonth1.Text != "")
            //    {
            //        dicAdvanceSalary["SAL_ADVANCE_SA_FOR_THE_MONTH1"] = this.txtMonth1.Text;
            //        DateTime dtSaDate = Convert.ToDateTime(dicAdvanceSalary["SAL_ADVANCE_SA_DATE"]);

            //        if (dtSaDate.Year !=1 && dtSaDate.Month != Int32.Parse(txtMonth1.Text))     
            //        {
            //            MessageBox.Show("VALUE HAS TO BE " + dtSaDate.Month /*+ ". ADVANCE DATE IS " + dtSaDate.ToString("dd/MM/yyyy")*/);
            //            this.txtMonth2.Focus();
            //        }
            //    }
            //    else
            //    {
            //        this.txtMonth2.Focus();
            //    }

            //    if (this.FunctionConfig.CurrentOption == Function.Delete)
            //    {
            //        DeleteRecord();                  
            //    }

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
        }
        private void txtMonth2_Leave(object sender, EventArgs e)
        {
            //try
            //{             
            //    if (this.txtMonth2.Text != "")
            //    {
            //        dicAdvanceSalary["SAL_ADVANCE_SA_FOR_THE_MONTH2"] = this.txtMonth2.Text;
            //        DateTime dtSaDate = Convert.ToDateTime(dicAdvanceSalary["SAL_ADVANCE_SA_DATE"]);
            //        if (dtSaDate.Month != 12)
            //        {
            //            if (((Int32.Parse(txtMonth2.Text)) - dtSaDate.Month) != 1)
            //            {
            //                globalNum = dtSaDate.Month + 1;
            //                txtMonth2.Text = string.Empty;
            //                MessageBox.Show(" VALUE CAN ONLY BE " + globalNum.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            //            }
            //            else
            //            {
            //                this.txtAmount.Focus();
            //            }
            //        }
            //        else 
            //        {
            //            if (Int32.Parse(txtMonth2.Text) != 1)
            //            {
            //                MessageBox.Show(" VALUE CAN ONLY BE 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            //            }
            //            else
            //            {
            //                this.txtAmount.Focus();
            //            }
            //        }
            //    }

            //    CalculateSalaryAmount();

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
        }
        private void txtAmount_Leave(object sender, EventArgs e)
        {
            //if (!isSaved)
            //{
            //    if (this.txtAmount.Text == string.Empty)
            //    {
            //        if (global_ee < 1)
            //        {
            //            MessageBox.Show("VALUE SHOULD BE ENTERED");
            //            global_ee = 1;
            //            this.txtAmount.Focus();
            //            return;
            //        }
            //        else
            //        {
            //            global_ee = 0;
            //            this.Cancel();
            //            this.txtOption.Select();
            //            this.txtOption.Focus();
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        Double amount = Convert.ToDouble(txtAmount.Text == "" ? "0" : txtAmount.Text);
            //        if (amount > this.global_sa_advance)
            //        {
            //            MessageBox.Show("VALUE HAS TO BE < " + this.global_sa_advance.ToString("00.00"));
            //            this.SaveRecord();
            //            return;
            //        }
            //        else
            //        {
            //            if ((this.txtMonth2.Text != "" /*|| this.txtMonth2.Text != "0"*/) && amount <= this.global_sa_advance / 2)
            //            {
            //                MessageBox.Show("YOU ARE TAKING 2 MONTHs ADVANCE & YOU ENTERED THE AMOUNT..\nWHICH CAN BE COVERED BY ONE MONTH ..........");
            //                this.SaveRecord();
            //                return;
            //            }
            //            else
            //            {
            //                this.SaveRecord();
            //                return;
            //            }
            //            this.SaveRecord();
            //            return;
            //        }
            //    }
            //}

        }
        private void lbtn_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                this.lbtn.Enabled = false;
                this.lbtn.Enabled = true;
            }
        }
        private void txtPersonnel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' && this.txtPersonnel.Text == "")
            {
                this.Cancel();
            }
        }
        private void txtMonth2_Validating(object sender, CancelEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.Modify && this.txtMonth2.Text != "")
            {
                if (this.DtSdate.Value != null)
                    dicAdvanceSalary["SAL_ADVANCE_SA_DATE"] = DtSdate.Value;
                return;
            }
            try
            {
                if (this.txtMonth2.Text != "")
                {
                    dicAdvanceSalary["SAL_ADVANCE_SA_FOR_THE_MONTH2"] = this.txtMonth2.Text;
                    DateTime dtSaDate = Convert.ToDateTime(dicAdvanceSalary["SAL_ADVANCE_SA_DATE"]);
                    if (dtSaDate.Month != 12)
                    {
                        if (((Int32.Parse(txtMonth2.Text)) - dtSaDate.Month) != 1)
                        {
                            globalNum = dtSaDate.Month + 1;
                            //txtMonth2.Text = string.Empty;
                            MessageBox.Show(" VALUE CAN ONLY BE " + globalNum.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            e.Cancel = true;
                        }
                        else
                        {
                            if (txtMonth2.Text.Trim().Length == 1)
                                txtMonth2.Text.Trim().PadLeft(2, '0');
                            CalculateSalaryAmount();
                            this.txtAmount.Focus();
                        }
                    }
                    else
                    {
                        if (Int32.Parse(txtMonth2.Text) != 1)
                        {
                            MessageBox.Show(" VALUE CAN ONLY BE 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        }
                        else
                        {
                            if (txtMonth2.Text.Trim().Length == 1)
                                txtMonth2.Text.Trim().PadLeft(2, '0');
                            CalculateSalaryAmount();
                            this.txtAmount.Focus();
                        }
                    }
                }
                else
                    CalculateSalaryAmount();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void txtMonth1_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtMonth1.Text != "")
                {
                    dicAdvanceSalary["SAL_ADVANCE_SA_FOR_THE_MONTH1"] = this.txtMonth1.Text;
                    DateTime dtSaDate = Convert.ToDateTime(dicAdvanceSalary["SAL_ADVANCE_SA_DATE"]);

                    if (dtSaDate.Year != 1 && dtSaDate.Month != Int32.Parse(txtMonth1.Text))
                    {
                        if (txtMonth1.Text.Trim().Length == 1)
                            txtMonth1.Text = txtMonth1.Text.Trim().PadLeft(2, '0');
                        MessageBox.Show("VALUE HAS TO BE " + dtSaDate.Month + ". ADVANCE DATE IS " + dtSaDate.ToString("dd/MM/yyyy"));

                        this.txtMonth2.Focus();
                    }
                    if (txtMonth1.Text.Trim().Length == 1)
                        txtMonth1.Text = txtMonth1.Text.Trim().PadLeft(2, '0');
                }
                else
                {
                    if (this.FunctionConfig.CurrentOption != Function.None)
                    {
                        this.ResetControls();//this.Cancel();
                        this.DtSdate.Value = null;
                        this.FunctionConfig.CurrentOption = Function.None;
                    }
                    //this.txtMonth2.Focus();
                }

                if (this.FunctionConfig.CurrentOption == Function.Delete)
                {
                    DeleteRecord();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #region Commented
            //if (txtMonth1.Text == "" || txtMonth1.Text == "0" || txtMonth1.Text == "00")
            //{
            //    this.Cancel();
            //    this.txtOption.Select();
            //    this.txtOption.Focus();
            //    return;
            //}
            //if (txtMonth1.Text.Trim().Length != 2)
            //{
            //    MessageBox.Show("Field must be of form 09.", "Forms");
            //    e.Cancel = true;
            //    return;
            //}
            ////else
            //{
            //    DateTime dt_sdate = Convert.ToDateTime(this.DtSdate.Value);
            //    //DateTime dt_sdate = DtSdate.Value;
            //    int Month = dt_sdate.Month;
            //    int Month1 = Int32.Parse(txtMonth1.Text);
            //    if (Month != Month1)
            //    {
            //        //MessageBox.Show("VALUE HAS TO BE" + Month.ToString());
            //        //MessageBox.Show("ADVANCE DATE IS " + dt_sdate.ToString());

            //        MessageBox.Show("VALUE HAS TO BE " + Month.ToString() + ". ADVANCE DATE IS " + dt_sdate.ToString("dd/MM/yyyy"));
            //        //e.Cancel = true;
            //        return;
            //        //e.Cancel = true;
            //    }

            //}
            ////this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));
            //if (this.FunctionConfig.CurrentOption == Function.Delete && this.txtMonth1.ToString() != string.Empty)
            //{
            //    DialogResult dRes = MessageBox.Show("Do you want to Delete this record [Y/N]..", "Note"
            //   , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    if (dRes == DialogResult.Yes)
            //    {
            //        Dictionary<string, object> param = new Dictionary<string, object>();
            //    param.Add("SA_P_NO", this.txtPersonnel.Text);
            //    rslt = cmnDM.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "Delete_Query", param);
            //        //base.DoToolbarActions(this.Controls, "Delete");
            //        return;

            //    }
            //    else if (dRes == DialogResult.No)
            //    {
            //        base.ClearForm(pnlHead.Controls);

            //    }             
            //    //base.Delete();

            //    base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));
            //    base.ClearForm(pnlHead.Controls);

            //    this.FunctionConfig.CurrentOption = Function.None;
            //    this.txtOption.Focus();
            //    return;
            //}
            #endregion
        }

        #region Un-usedCode

        private void DoSave()
        {
            DialogResult dRes = MessageBox.Show("DO YOU WANT TO SAVE THE CHANGES [Y]es [N]o", "Note"
                               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {
                if (this.FunctionConfig.CurrentOption == Function.Add)
                {
                    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                }
                else if (this.FunctionConfig.CurrentOption == Function.Modify)
                {
                    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                }
                base.DoToolbarActions(this.Controls, "Save");
                DtSdate.Value = null;
                return;
            }
            else if (dRes == DialogResult.No)
            {
                this.Cancel();
                this.DtSdate.Value = null;
            }
        }
        protected void Get()
        {
            Result rslt;

            Dictionary<string, object> param = new Dictionary<string, object>();

            param.Add("SA_P_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "PerNo", param);

        }
        private void AddCase()
        {
            DataTable rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("W_DATE", this.CurrentDate.ToShortDateString());
            param.Add("SA_P_NO", this.txtPersonnel.Text);

            rslt = GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "Check_Insertion", param);
            if (rslt != null && rslt.Rows.Count > 0)
            {
                //this.txtMonth1.Text = rslt.Rows[0][0] == null ? "" : rslt.Rows[0][0].ToString();
                //this.txtMonth2.Text = rslt.Rows[0][1] == null ? "" : rslt.Rows[0][1].ToString();
                //this.txtAmount.Text = rslt.Rows[0][3] == null ? "" : rslt.Rows[0][3].ToString();
                //object ID = rslt.Rows[0][5];
                //this.txtID.Text = ID == null ? "0" : ID.ToString();
                //this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                this.txtMonth1.Text = rslt.Rows[0][0] == null ? "" : rslt.Rows[0][0].ToString();
                this.txtMonth2.Text = rslt.Rows[0][1] == null ? "" : rslt.Rows[0][1].ToString();
                this.DtSdate.Value = rslt.Rows[0][2];
                this.txtAmount.Text = rslt.Rows[0][3] == null ? "" : rslt.Rows[0][3].ToString();
                object ID = rslt.Rows[0][5];
                ID = rslt.Rows[0][5];
                this.txtID.Text = ID == null ? "0" : ID.ToString();
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            }
            else
            {
                this.DtSdate.Value = this.CurrentDate;
                this.txtMonth1.Select();
                this.txtMonth1.Focus();
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                return;
            }
            if (this.txtMonth1.Text != "00" && this.txtMonth2.Text != "00")
            {
                MessageBox.Show("VALUE IS ALREADY THERE FOR THE CURRENT MONTH", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                this.Cancel();
                this.txtOption.Select();
                this.txtOption.Focus();
                return;
            }
            if (this.txtMonth1.Text != "00" && this.txtMonth2.Text == "00")
            {
                this.txtMonth2.Select();
                this.txtMonth2.Focus();
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                return;
            }
        }
        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {

            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }

        //private void txtMonth2_Validating(object sender, CancelEventArgs e)
        //{
        //if (txtMonth2.Text == "" || txtMonth2.Text == "0" || txtMonth2.Text == "00")
        //{
        //    txtMonth2.Text = "";
        //    //e.Cancel = true;
        //    //return;
        //}
        //if (txtMonth2.Text != "" && txtMonth2.Text.Trim().Length != 2)
        //{
        //    MessageBox.Show("Field must be of form 09.", "Forms");
        //    txtMonth2.Text = "";
        //    e.Cancel = true;
        //    return;
        //}
        ////****SALADV****//
        ///** If Advance Is Taken For 1 Month  and ** If Advance is Taken For Both The Months **/

        //#region key_next_item handling

        ////DateTime dt_sdate = Convert.ToDateTime(this.DtSdate.Value);
        ////int Month = dt_sdate.Month;                //month of current date
        ////int Month2 = Int32.Parse(txtMonth2.Text == "" ? "0" : txtMonth2.Text);
        ////if (this.txtMonth2.Text != "")
        ////{
        ////    if (Month != 12)
        ////    {
        ////        if ((Month2 - Month) != 1)
        ////        {
        ////            globalNum = Month + 1;
        ////            txtMonth2.Text = string.Empty;
        ////            MessageBox.Show("'VALUE CAN ONLY BE " + globalNum.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
        ////            e.Cancel = true;
        ////            return;
        ////            //this.FunctionConfig.CurrentOption = Function.None;
        ////            //base.ClearForm(pnlHead.Controls);
        ////            //this.txtOption.Focus();

        ////        }
        ////        //else
        ////        //{
        ////        //    if (Month2 != 1)
        ////        //    {
        ////        //        MessageBox.Show("VALUE CAN ONLY BE 1");
        ////        //        e.Cancel = true;
        ////        //        return;
        ////        //    }
        ////        //    this.FunctionConfig.CurrentOption = Function.None;


        ////        //}
        ////    }
        ////}

        //#endregion

        //#region when validate item handling

        ////double salPak = 0;
        ////Result rslt;
        ////Dictionary<string, object> param = new Dictionary<string, object>();
        ////param.Add("SA_P_NO", this.txtPersonnel.Text);
        ////param.Add("SA_FOR_THE_MONTH2", this.txtMonth2.Text);
        ////rslt = cmnDM.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "SALADV", param);
        ////if (rslt.isSuccessful)
        ////{
        ////    if (rslt.dstResult.Tables[0].Rows.Count > 0)
        ////    {
        ////        if (rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"] != null && rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"].ToString() != "")
        ////        {
        ////            salPak = double.Parse(rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"] == null ? "0" : rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"].ToString());
        ////            this.txtAmount.Text = salPak.ToString();
        ////            this._saAdvance = salPak;
        ////        }
        ////    }
        ////}

        ////Commented by Anila on 9th march 2011 because this task has already performed on SP
        ////if (txtMonth2.Text == "" || txtMonth2.Text == "0")
        ////{
        ////    salPak = salPak / 12;
        ////    this.txtAmount.Text = salPak.ToString();
        ////}
        ////else
        ////{
        ////    salPak = salPak / 6;
        ////    this.txtAmount.Text = salPak.ToString();
        ////}
        ////this._saAdvance = salPak;

        //#endregion
        //}
        private void txtAmount_Validating(object sender, CancelEventArgs e)
        {
            //double amount = 0;
            //if (txtAmount.Text == string.Empty)
            //{
            //    if (global_ee < 1)
            //    {
            //        MessageBox.Show("VALUE SHOULD BE ENTERED");
            //        global_ee = 1;
            //        e.Cancel = true;
            //        return;
            //    }
            //    else
            //    {
            //        global_ee = 0;
            //        this.Cancel();
            //        this.txtOption.Select();
            //        this.txtOption.Focus();
            //        return;
            //    }
            //}
            //else
            //{
            //    amount = Convert.ToDouble(txtAmount.Text == "" ? "0" : txtAmount.Text);
            //    if (amount > this._saAdvance)
            //    {
            //        MessageBox.Show("VALUE HAS TO BE < " + this._saAdvance.ToString("00.00"));
            //        this.DoSave();
            //        return;
            //    }
            //    else
            //    {
            //        if ((this.txtMonth2.Text != "" || this.txtMonth2.Text != "0") && amount <= this._saAdvance)
            //        {
            //            MessageBox.Show("YOU ARE TAKING 2 MNTH ADVANCE & YOU ENTERED THE AMOUNT..\nWHICH CAN BE COVERED BY ONE MONTH ..........");
            //            this.DoSave();
            //            return;
            //        }
            //        else
            //        {
            //            this.DoSave();
            //            return;
            //        }
            //        this.DoSave();
            //    }
            //}
        }
        private void CHRIS_Payroll_SalaryAdvanceEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            #region Commented Code
            //this.txtMonth1.ReadOnly = false;
            //this.txtMonth2.ReadOnly = false;

            //if (actionType == "BLK_P_PR_P_NO_LOV0")
            //{
            //    if (txtPersonnel.Text != "")
            //    {
            //        this.DtSdate.Value = DateTime.Today.Date;
            //    }
            //    object ID = null;
            //    DataTable rslt;
            //    Dictionary<string, object> param = new Dictionary<string, object>();
            //    switch (this.FunctionConfig.CurrentOption)
            //    {
            //        case Function.Add:
            //            AddCase();
            //            //this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            //            break;
            //        case Function.Modify:
            //            param.Add("W_DATE", this.CurrentDate.ToShortDateString());
            //            param.Add("SA_P_NO", this.txtPersonnel.Text);

            //            rslt = GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "COUNT", param);
            //            if (rslt != null && rslt.Rows.Count > 0)
            //            {
            //                this.txtMonth1.Text = rslt.Rows[0][0] == null ? "" : rslt.Rows[0][0].ToString();
            //                this.txtMonth2.Text = rslt.Rows[0][1] == null ? "" : rslt.Rows[0][1].ToString();
            //                this.DtSdate.Value = rslt.Rows[0][2];
            //                this.txtAmount.Text = rslt.Rows[0][3] == null ? "" : rslt.Rows[0][3].ToString();
            //                ID = rslt.Rows[0][5];
            //                this.txtID.Text = ID == null ? "0" : ID.ToString();
            //                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            //                this.txtMonth1.Select();
            //                this.txtMonth1.Focus();
            //                this.txtMonth1.ReadOnly = true;
            //                this.txtMonth2.ReadOnly = true;
            //            }
            //            else
            //            {
            //                this.txtMonth1.Text = "";
            //                this.txtMonth2.Text = "";
            //                this.DtSdate.Value = DateTime.Today.Date;
            //                this.txtAmount.Text = "";
            //                this.txtID.Text = "0";
            //                this.txtMonth1.Select();
            //                this.txtMonth1.Focus();
            //                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            //            }

            //            break;
            //        case Function.Delete:
            //            param.Add("W_DATE", this.CurrentDate.ToShortDateString());
            //            param.Add("SA_P_NO", this.txtPersonnel.Text);

            //            rslt = GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "COUNT", param);
            //            if (rslt != null && rslt.Rows.Count > 0)
            //            {
            //                this.txtMonth1.Text = rslt.Rows[0][0] == null ? "" : rslt.Rows[0][0].ToString();
            //                this.txtMonth2.Text = rslt.Rows[0][1] == null ? "" : rslt.Rows[0][1].ToString();
            //                this.DtSdate.Value = rslt.Rows[0][2];
            //                this.txtAmount.Text = rslt.Rows[0][3] == null ? "" : rslt.Rows[0][3].ToString();
            //                ID = rslt.Rows[0][5];
            //                this.txtID.Text = ID == null ? "0" : ID.ToString();

            //                DialogResult dr = MessageBox.Show("Do you want to delete the record.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //                if (dr == DialogResult.Yes)
            //                {
            //                    DataTable dt = null;
            //                    Result rsltCode;

            //                    param.Add("ID", ID);
            //                    CmnDataManager cmnDM = new CmnDataManager();
            //                    rsltCode = cmnDM.Execute("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "Delete", param);

            //                    if (rsltCode.isSuccessful)
            //                    {
            //                        //if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
            //                        //{
            //                        //    dt = rsltCode.dstResult.Tables[0];
            //                        //}
            //                    }
            //                }
            //                else
            //                {
            //                    this.Cancel();
            //                }
            //            }

            //            break;
            //        case Function.View:
            //            break;

            //    }
            //}

            #endregion
        }

        #endregion

        private void txtAmount_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    e.IsInputKey = true;
                }
            }

        }

        private void txtAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                if (!isSaved)
                {
                    if (this.txtAmount.Text == string.Empty)
                    {
                        if (global_ee < 1)
                        {
                            MessageBox.Show("VALUE SHOULD BE ENTERED");
                            global_ee = 1;
                            this.txtAmount.Focus();
                            return;
                        }
                        else
                        {
                            global_ee = 0;
                            this.Cancel();
                            this.txtOption.Select();
                            this.txtOption.Focus();
                            return;
                        }
                    }
                    else
                    {
                        Double amount = Convert.ToDouble(txtAmount.Text == "" ? "0" : txtAmount.Text);
                        if (amount > Math.Round(this.global_sa_advance, 2))
                        {
                            MessageBox.Show("VALUE HAS TO BE < " + this.global_sa_advance.ToString("00.00"));
                            this.SaveRecord();
                            return;
                        }
                        else
                        {
                            if ((this.txtMonth2.Text != "" /*|| this.txtMonth2.Text != "0"*/) && amount <= this.global_sa_advance / 2)
                            {
                                MessageBox.Show("YOU ARE TAKING 2 MONTHs ADVANCE & YOU ENTERED THE AMOUNT..\nWHICH CAN BE COVERED BY ONE MONTH ..........");
                                this.SaveRecord();
                                return;
                            }
                            else
                            {
                                this.SaveRecord();
                                return;
                            }
                            this.SaveRecord();
                            return;
                        }
                    }
                }

            }

        }
        #endregion
    }
}



