namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_ZakatEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_ZakatEntry));
            this.slPnlZakaat = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.lkpPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRefundYear2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRefundYear1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRefundAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.txtZakatAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtOptionView = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblUserNameTitle = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPnlZakaat.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 608);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // slPnlZakaat
            // 
            this.slPnlZakaat.ConcurrentPanels = null;
            this.slPnlZakaat.Controls.Add(this.slTextBox1);
            this.slPnlZakaat.Controls.Add(this.lkpPersonnel);
            this.slPnlZakaat.Controls.Add(this.label14);
            this.slPnlZakaat.Controls.Add(this.label13);
            this.slPnlZakaat.Controls.Add(this.label12);
            this.slPnlZakaat.Controls.Add(this.label11);
            this.slPnlZakaat.Controls.Add(this.label10);
            this.slPnlZakaat.Controls.Add(this.label5);
            this.slPnlZakaat.Controls.Add(this.label4);
            this.slPnlZakaat.Controls.Add(this.label3);
            this.slPnlZakaat.Controls.Add(this.label2);
            this.slPnlZakaat.Controls.Add(this.label1);
            this.slPnlZakaat.Controls.Add(this.txtRefundYear2);
            this.slPnlZakaat.Controls.Add(this.txtRefundYear1);
            this.slPnlZakaat.Controls.Add(this.txtRefundAmount);
            this.slPnlZakaat.Controls.Add(this.txtZakatAmount);
            this.slPnlZakaat.Controls.Add(this.txtFirstName);
            this.slPnlZakaat.Controls.Add(this.txtPersonnelNo);
            this.slPnlZakaat.DataManager = "iCORE.CHRIS.DATAOBJECTS.CmnDataManager";
            this.slPnlZakaat.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlZakaat.DependentPanels = null;
            this.slPnlZakaat.DisableDependentLoad = false;
            this.slPnlZakaat.EnableDelete = true;
            this.slPnlZakaat.EnableInsert = true;
            this.slPnlZakaat.EnableQuery = false;
            this.slPnlZakaat.EnableUpdate = true;
            this.slPnlZakaat.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand_Zakat";
            this.slPnlZakaat.Location = new System.Drawing.Point(48, 159);
            this.slPnlZakaat.MasterPanel = null;
            this.slPnlZakaat.Name = "slPnlZakaat";
            this.slPnlZakaat.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlZakaat.Size = new System.Drawing.Size(564, 243);
            this.slPnlZakaat.SPName = "CHRIS_SP_ZAKAT_MANAGER";
            this.slPnlZakaat.TabIndex = 10;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = false;
            this.slTextBox1.DataFieldMapping = "ID";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(404, 201);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(56, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 23;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox1.Visible = false;
            // 
            // lkpPersonnel
            // 
            this.lkpPersonnel.ActionLOVExists = "PersonnelLovExist";
            this.lkpPersonnel.ActionType = "PersonnelLov";
            this.lkpPersonnel.ConditionalFields = "";
            this.lkpPersonnel.CustomEnabled = true;
            this.lkpPersonnel.DataFieldMapping = "";
            this.lkpPersonnel.DependentLovControls = "";
            this.lkpPersonnel.HiddenColumns = "";
            this.lkpPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lkpPersonnel.Image")));
            this.lkpPersonnel.LoadDependentEntities = false;
            this.lkpPersonnel.Location = new System.Drawing.Point(256, 34);
            this.lkpPersonnel.LookUpTitle = null;
            this.lkpPersonnel.Name = "lkpPersonnel";
            this.lkpPersonnel.Size = new System.Drawing.Size(26, 21);
            this.lkpPersonnel.SkipValidationOnLeave = false;
            this.lkpPersonnel.SPName = "CHRIS_SP_ZAKAT_MANAGER";
            this.lkpPersonnel.TabIndex = 22;
            this.lkpPersonnel.TabStop = false;
            this.lkpPersonnel.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(150, 178);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(11, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = ":";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(149, 141);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(11, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(150, 106);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(11, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = ":";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(150, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(150, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(11, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = ":";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Refund For the Year";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Tax Refund Amount";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Zakat Amount";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "First Name  ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Personnel No.  ";
            // 
            // txtRefundYear2
            // 
            this.txtRefundYear2.AllowSpace = true;
            this.txtRefundYear2.AssociatedLookUpName = "";
            this.txtRefundYear2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRefundYear2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRefundYear2.ContinuationTextBox = null;
            this.txtRefundYear2.CustomEnabled = true;
            this.txtRefundYear2.DataFieldMapping = "PR_REFUND_FOR2";
            this.txtRefundYear2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefundYear2.GetRecordsOnUpDownKeys = false;
            this.txtRefundYear2.IsDate = false;
            this.txtRefundYear2.Location = new System.Drawing.Point(214, 176);
            this.txtRefundYear2.MaxLength = 4;
            this.txtRefundYear2.Name = "txtRefundYear2";
            this.txtRefundYear2.NumberFormat = "###,###,##0.00";
            this.txtRefundYear2.Postfix = "";
            this.txtRefundYear2.Prefix = "";
            this.txtRefundYear2.Size = new System.Drawing.Size(39, 20);
            this.txtRefundYear2.SkipValidation = false;
            this.txtRefundYear2.TabIndex = 5;
            this.txtRefundYear2.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtRefundYear1
            // 
            this.txtRefundYear1.AllowSpace = true;
            this.txtRefundYear1.AssociatedLookUpName = "";
            this.txtRefundYear1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRefundYear1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRefundYear1.ContinuationTextBox = null;
            this.txtRefundYear1.CustomEnabled = true;
            this.txtRefundYear1.DataFieldMapping = "PR_REFUND_FOR1";
            this.txtRefundYear1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefundYear1.GetRecordsOnUpDownKeys = false;
            this.txtRefundYear1.IsDate = false;
            this.txtRefundYear1.Location = new System.Drawing.Point(173, 176);
            this.txtRefundYear1.MaxLength = 4;
            this.txtRefundYear1.Name = "txtRefundYear1";
            this.txtRefundYear1.NumberFormat = "###,###,##0.00";
            this.txtRefundYear1.Postfix = "";
            this.txtRefundYear1.Prefix = "";
            this.txtRefundYear1.Size = new System.Drawing.Size(39, 20);
            this.txtRefundYear1.SkipValidation = false;
            this.txtRefundYear1.TabIndex = 4;
            this.txtRefundYear1.TextType = CrplControlLibrary.TextType.Integer;
            this.txtRefundYear1.Leave += new System.EventHandler(this.txtRefundYear1_Leave);
            // 
            // txtRefundAmount
            // 
            this.txtRefundAmount.AllowSpace = true;
            this.txtRefundAmount.AssociatedLookUpName = "";
            this.txtRefundAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRefundAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRefundAmount.ContinuationTextBox = null;
            this.txtRefundAmount.CustomEnabled = true;
            this.txtRefundAmount.DataFieldMapping = "PR_REFUND_AMT";
            this.txtRefundAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefundAmount.GetRecordsOnUpDownKeys = false;
            this.txtRefundAmount.IsDate = false;
            this.txtRefundAmount.Location = new System.Drawing.Point(173, 139);
            this.txtRefundAmount.MaxLength = 12;
            this.txtRefundAmount.Name = "txtRefundAmount";
            this.txtRefundAmount.NumberFormat = "###,###,##0";
            this.txtRefundAmount.Postfix = "";
            this.txtRefundAmount.Prefix = "";
            this.txtRefundAmount.Size = new System.Drawing.Size(100, 20);
            this.txtRefundAmount.SkipValidation = false;
            this.txtRefundAmount.TabIndex = 3;
            this.txtRefundAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRefundAmount.TextType = CrplControlLibrary.TextType.Amount;
            this.txtRefundAmount.TextChanged += new System.EventHandler(this.txtRefundAmount_TextChanged);
            // 
            // txtZakatAmount
            // 
            this.txtZakatAmount.AllowSpace = true;
            this.txtZakatAmount.AssociatedLookUpName = "";
            this.txtZakatAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtZakatAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtZakatAmount.ContinuationTextBox = null;
            this.txtZakatAmount.CustomEnabled = true;
            this.txtZakatAmount.DataFieldMapping = "PR_ZAKAT_AMT";
            this.txtZakatAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtZakatAmount.GetRecordsOnUpDownKeys = false;
            this.txtZakatAmount.IsDate = false;
            this.txtZakatAmount.Location = new System.Drawing.Point(173, 104);
            this.txtZakatAmount.MaxLength = 5;
            this.txtZakatAmount.Name = "txtZakatAmount";
            this.txtZakatAmount.NumberFormat = "###,###,##0";
            this.txtZakatAmount.Postfix = "";
            this.txtZakatAmount.Prefix = "";
            this.txtZakatAmount.Size = new System.Drawing.Size(52, 20);
            this.txtZakatAmount.SkipValidation = false;
            this.txtZakatAmount.TabIndex = 2;
            this.txtZakatAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZakatAmount.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "PR_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(173, 70);
            this.txtFirstName.MaxLength = 40;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(268, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "lkpPersonnel";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = true;
            this.txtPersonnelNo.DataFieldMapping = "PR_P_NO";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.Location = new System.Drawing.Point(173, 36);
            this.txtPersonnelNo.MaxLength = 8;
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(77, 20);
            this.txtPersonnelNo.SkipValidation = false;
            this.txtPersonnelNo.TabIndex = 0;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnelNo.Leave += new System.EventHandler(this.txtPersonnelNo_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(245, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "PAYROLL SYSTEM";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(216, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(182, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "ZAKAT/ TAX REFUND ENTRY";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(484, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Option :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(494, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Date :";
            // 
            // txtOptionView
            // 
            this.txtOptionView.AllowSpace = true;
            this.txtOptionView.AssociatedLookUpName = "";
            this.txtOptionView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOptionView.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOptionView.ContinuationTextBox = null;
            this.txtOptionView.CustomEnabled = true;
            this.txtOptionView.DataFieldMapping = "";
            this.txtOptionView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOptionView.GetRecordsOnUpDownKeys = false;
            this.txtOptionView.IsDate = false;
            this.txtOptionView.Location = new System.Drawing.Point(537, 90);
            this.txtOptionView.Name = "txtOptionView";
            this.txtOptionView.NumberFormat = "###,###,##0.00";
            this.txtOptionView.Postfix = "";
            this.txtOptionView.Prefix = "";
            this.txtOptionView.Size = new System.Drawing.Size(47, 20);
            this.txtOptionView.SkipValidation = false;
            this.txtOptionView.TabIndex = 15;
            this.txtOptionView.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(537, 112);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.Size = new System.Drawing.Size(78, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 16;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(539, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 110;
            this.lblUserName.Text = "User Name  :";
            // 
            // lblUserNameTitle
            // 
            this.lblUserNameTitle.AutoSize = true;
            this.lblUserNameTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblUserNameTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserNameTitle.Location = new System.Drawing.Point(464, 9);
            this.lblUserNameTitle.Name = "lblUserNameTitle";
            this.lblUserNameTitle.Size = new System.Drawing.Size(69, 13);
            this.lblUserNameTitle.TabIndex = 111;
            this.lblUserNameTitle.Text = "User Name  :";
            // 
            // CHRIS_Payroll_ZakatEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(669, 668);
            this.Controls.Add(this.lblUserNameTitle);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.txtOptionView);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.slPnlZakaat);
            this.CurrentPanelBlock = "slPnlZakaat";
            this.Name = "CHRIS_Payroll_ZakatEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Payroll_ZakatEntry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.slPnlZakaat, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.txtOptionView, 0);
            this.Controls.SetChildIndex(this.txtDate, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.lblUserNameTitle, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPnlZakaat.ResumeLayout(false);
            this.slPnlZakaat.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPnlZakaat;
        private CrplControlLibrary.SLTextBox txtRefundYear2;
        private CrplControlLibrary.SLTextBox txtRefundYear1;
        private CrplControlLibrary.SLTextBox txtRefundAmount;
        private CrplControlLibrary.SLTextBox txtZakatAmount;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private CrplControlLibrary.SLTextBox txtPersonnelNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtOptionView;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.LookupButton lkpPersonnel;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblUserNameTitle;
    }
}