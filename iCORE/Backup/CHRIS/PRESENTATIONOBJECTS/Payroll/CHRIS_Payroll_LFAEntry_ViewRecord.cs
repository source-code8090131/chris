using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_LFAEntry_ViewRecord : ChrisTabularForm
    {

        #region Declaration
        CmnDataManager objCmnDataManager;
        #endregion

        #region Constructor
        public CHRIS_Payroll_LFAEntry_ViewRecord()
        {
            InitializeComponent();
            this.txtOption.Visible = false;
            this.tlbMain.Visible = false;
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.F8 = Function.None;


            
        }
        #endregion


        public CHRIS_Payroll_LFAEntry_ViewRecord(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }



        protected override bool Query()
        {
          return false;
        }







        #region Methods

        /// <summary>
        /// override it for extra functionality
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();

                this.SetFormTitle = "";

                //Bold DataGridViewHeader
                Font newFontStyle = new Font(sldgvLFA_TAB.Font, FontStyle.Bold);
                sldgvLFA_TAB.ColumnHeadersDefaultCellStyle.Font = newFontStyle;

                objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "View_Records");

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {                      
                            this.sldgvLFA_TAB.DataSource = rslt.dstResult.Tables[0];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
          
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.F8 = Function.None;
           

        }

        #endregion

        private void CHRIS_Payroll_LFAEntry_ViewRecord_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void CHRIS_Payroll_LFAEntry_ViewRecord_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode  == Keys.F8)
            {
                //base.DoToolbarActions(this.Controls, "GetAll");
                e.Handled = false;
                this.FunctionConfig.F8 = Function.None;
                return;
                
            }

            if (e.KeyCode == Keys.F6)
            {
                CHRIS_Payroll_LFAEntry objLFA = new CHRIS_Payroll_LFAEntry();

                objLFA.slPnlLeaveFairAssistance.ResetText();
                objLFA.slPnlLeaveFairAssistance.ClearBusinessEntity();
                base.ClearForm(objLFA.slPnlLeaveFairAssistance.Controls);
                objLFA.txtOption.Focus();
              
                base.FunctionConfig.F6 = Function.Cancel;
                base.FunctionConfig.F6 = Function.Quit;
                

                objLFA.txtPersonnelNo.Text = string.Empty;
                objLFA.txtPersonnelName.Text = string.Empty;
                objLFA.txtLeaveBalance.Text = string.Empty;
                objLFA.txtLFAamount.Text = string.Empty;
                objLFA.txtApproved.Text = string.Empty;
                objLFA.txtDateofGeneration.Text = string.Empty;
                
                
                

            }
        }

    }
}