namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_IndividualsDeductionEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_IndividualsDeductionEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.slPnlMaster = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.sldpValidTo = new CrplControlLibrary.SLDatePicker(this.components);
            this.sldpValidFrom = new CrplControlLibrary.SLDatePicker(this.components);
            this.lkpValidAccount = new CrplControlLibrary.LookupButton(this.components);
            this.lkpValidDeductionCode = new CrplControlLibrary.LookupButton(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.txtAccountDescription = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAccountNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDeductionAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDeductionDescription = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDeductionCode = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.slPnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.sldgvDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.SP_P_NO = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.W_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_DED_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_REMARKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gboMaster = new System.Windows.Forms.GroupBox();
            this.gboDetail = new System.Windows.Forms.GroupBox();
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtOptionView = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPnlMaster.SuspendLayout();
            this.slPnlDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldgvDetail)).BeginInit();
            this.gboMaster.SuspendLayout();
            this.gboDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(623, 0);
            this.txtOption.Visible = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(659, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 588);
            this.panel1.Size = new System.Drawing.Size(659, 60);
            // 
            // slPnlMaster
            // 
            this.slPnlMaster.ConcurrentPanels = null;
            this.slPnlMaster.Controls.Add(this.sldpValidTo);
            this.slPnlMaster.Controls.Add(this.sldpValidFrom);
            this.slPnlMaster.Controls.Add(this.lkpValidAccount);
            this.slPnlMaster.Controls.Add(this.lkpValidDeductionCode);
            this.slPnlMaster.Controls.Add(this.label12);
            this.slPnlMaster.Controls.Add(this.txtAccountDescription);
            this.slPnlMaster.Controls.Add(this.txtAccountNo);
            this.slPnlMaster.Controls.Add(this.txtDeductionAmount);
            this.slPnlMaster.Controls.Add(this.txtDeductionDescription);
            this.slPnlMaster.Controls.Add(this.txtDeductionCode);
            this.slPnlMaster.Controls.Add(this.label11);
            this.slPnlMaster.Controls.Add(this.label10);
            this.slPnlMaster.Controls.Add(this.label9);
            this.slPnlMaster.Controls.Add(this.label8);
            this.slPnlMaster.DataManager = "iCORE.CHRIS.DATAOBJECTS.CmnDataManager";
            this.slPnlMaster.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlMaster.DependentPanels = null;
            this.slPnlMaster.DisableDependentLoad = false;
            this.slPnlMaster.EnableDelete = true;
            this.slPnlMaster.EnableInsert = true;
            this.slPnlMaster.EnableQuery = false;
            this.slPnlMaster.EnableUpdate = true;
            this.slPnlMaster.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeductionCommand";
            this.slPnlMaster.Location = new System.Drawing.Point(15, 16);
            this.slPnlMaster.MasterPanel = null;
            this.slPnlMaster.Name = "slPnlMaster";
            this.slPnlMaster.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlMaster.Size = new System.Drawing.Size(615, 92);
            this.slPnlMaster.SPName = "CHRIS_SP_DEDUCTION_MANAGER";
            this.slPnlMaster.TabIndex = 10;
            // 
            // sldpValidTo
            // 
            this.sldpValidTo.CustomEnabled = true;
            this.sldpValidTo.CustomFormat = "dd/MM/yyyy";
            this.sldpValidTo.DataFieldMapping = "SP_VALID_TO_D";
            this.sldpValidTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sldpValidTo.HasChanges = true;
            this.sldpValidTo.IsRequired = true;
            this.sldpValidTo.Location = new System.Drawing.Point(227, 37);
            this.sldpValidTo.Name = "sldpValidTo";
            this.sldpValidTo.NullValue = " ";
            this.sldpValidTo.Size = new System.Drawing.Size(98, 20);
            this.sldpValidTo.TabIndex = 4;
            this.sldpValidTo.Value = new System.DateTime(2011, 1, 26, 0, 0, 0, 0);
            this.sldpValidTo.Leave += new System.EventHandler(this.sldpValidTo_Leave);
            // 
            // sldpValidFrom
            // 
            this.sldpValidFrom.CustomEnabled = true;
            this.sldpValidFrom.CustomFormat = "dd/MM/yyyy";
            this.sldpValidFrom.DataFieldMapping = "SP_VALID_FROM_D";
            this.sldpValidFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sldpValidFrom.HasChanges = true;
            this.sldpValidFrom.IsRequired = true;
            this.sldpValidFrom.Location = new System.Drawing.Point(102, 37);
            this.sldpValidFrom.Name = "sldpValidFrom";
            this.sldpValidFrom.NullValue = " ";
            this.sldpValidFrom.Size = new System.Drawing.Size(98, 20);
            this.sldpValidFrom.TabIndex = 3;
            this.sldpValidFrom.Value = new System.DateTime(2011, 1, 26, 0, 0, 0, 0);
            // 
            // lkpValidAccount
            // 
            this.lkpValidAccount.ActionLOVExists = "ValidAccountNumberLovExist";
            this.lkpValidAccount.ActionType = "ValidAccountNumberLov";
            this.lkpValidAccount.ConditionalFields = "";
            this.lkpValidAccount.CustomEnabled = true;
            this.lkpValidAccount.DataFieldMapping = "";
            this.lkpValidAccount.DependentLovControls = "";
            this.lkpValidAccount.HiddenColumns = "";
            this.lkpValidAccount.Image = ((System.Drawing.Image)(resources.GetObject("lkpValidAccount.Image")));
            this.lkpValidAccount.LoadDependentEntities = false;
            this.lkpValidAccount.Location = new System.Drawing.Point(175, 59);
            this.lkpValidAccount.LookUpTitle = null;
            this.lkpValidAccount.Name = "lkpValidAccount";
            this.lkpValidAccount.Size = new System.Drawing.Size(26, 21);
            this.lkpValidAccount.SkipValidationOnLeave = false;
            this.lkpValidAccount.SPName = "CHRIS_SP_DEDUCTION_MANAGER";
            this.lkpValidAccount.TabIndex = 47;
            this.lkpValidAccount.TabStop = false;
            this.lkpValidAccount.UseVisualStyleBackColor = true;
            // 
            // lkpValidDeductionCode
            // 
            this.lkpValidDeductionCode.ActionLOVExists = "ValidDeductionCodeLovExist";
            this.lkpValidDeductionCode.ActionType = "ValidDeductionCodeLov";
            this.lkpValidDeductionCode.ConditionalFields = "";
            this.lkpValidDeductionCode.CustomEnabled = true;
            this.lkpValidDeductionCode.DataFieldMapping = "";
            this.lkpValidDeductionCode.DependentLovControls = "";
            this.lkpValidDeductionCode.HiddenColumns = "";
            this.lkpValidDeductionCode.Image = ((System.Drawing.Image)(resources.GetObject("lkpValidDeductionCode.Image")));
            this.lkpValidDeductionCode.LoadDependentEntities = true;
            this.lkpValidDeductionCode.Location = new System.Drawing.Point(152, 15);
            this.lkpValidDeductionCode.LookUpTitle = null;
            this.lkpValidDeductionCode.Name = "lkpValidDeductionCode";
            this.lkpValidDeductionCode.Size = new System.Drawing.Size(26, 21);
            this.lkpValidDeductionCode.SkipValidationOnLeave = false;
            this.lkpValidDeductionCode.SPName = "CHRIS_SP_DEDUCTION_MANAGER";
            this.lkpValidDeductionCode.TabIndex = 2;
            this.lkpValidDeductionCode.TabStop = false;
            this.lkpValidDeductionCode.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(208, 39);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "/";
            // 
            // txtAccountDescription
            // 
            this.txtAccountDescription.AllowSpace = true;
            this.txtAccountDescription.AssociatedLookUpName = "";
            this.txtAccountDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccountDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAccountDescription.ContinuationTextBox = null;
            this.txtAccountDescription.CustomEnabled = true;
            this.txtAccountDescription.DataFieldMapping = "SP_ACC_DESC";
            this.txtAccountDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccountDescription.GetRecordsOnUpDownKeys = false;
            this.txtAccountDescription.IsDate = false;
            this.txtAccountDescription.Location = new System.Drawing.Point(227, 59);
            this.txtAccountDescription.MaxLength = 50;
            this.txtAccountDescription.Name = "txtAccountDescription";
            this.txtAccountDescription.NumberFormat = "###,###,##0.00";
            this.txtAccountDescription.Postfix = "";
            this.txtAccountDescription.Prefix = "";
            this.txtAccountDescription.ReadOnly = true;
            this.txtAccountDescription.Size = new System.Drawing.Size(370, 20);
            this.txtAccountDescription.SkipValidation = false;
            this.txtAccountDescription.TabIndex = 7;
            this.txtAccountDescription.TabStop = false;
            this.txtAccountDescription.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.AllowSpace = true;
            this.txtAccountNo.AssociatedLookUpName = "lkpValidAccount";
            this.txtAccountNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccountNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAccountNo.ContinuationTextBox = null;
            this.txtAccountNo.CustomEnabled = true;
            this.txtAccountNo.DataFieldMapping = "SP_ACOUNT_NO_D";
            this.txtAccountNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccountNo.GetRecordsOnUpDownKeys = false;
            this.txtAccountNo.IsDate = false;
            this.txtAccountNo.Location = new System.Drawing.Point(102, 59);
            this.txtAccountNo.MaxLength = 11;
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.NumberFormat = "###,###,##0.00";
            this.txtAccountNo.Postfix = "";
            this.txtAccountNo.Prefix = "";
            this.txtAccountNo.Size = new System.Drawing.Size(72, 20);
            this.txtAccountNo.SkipValidation = false;
            this.txtAccountNo.TabIndex = 6;
            this.txtAccountNo.TextType = CrplControlLibrary.TextType.String;
            this.txtAccountNo.Leave += new System.EventHandler(this.txtAccountNo_Leave);
            // 
            // txtDeductionAmount
            // 
            this.txtDeductionAmount.AllowSpace = true;
            this.txtDeductionAmount.AssociatedLookUpName = "";
            this.txtDeductionAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDeductionAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDeductionAmount.ContinuationTextBox = null;
            this.txtDeductionAmount.CustomEnabled = true;
            this.txtDeductionAmount.DataFieldMapping = "SP_DED_AMOUNT";
            this.txtDeductionAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeductionAmount.GetRecordsOnUpDownKeys = false;
            this.txtDeductionAmount.IsDate = false;
            this.txtDeductionAmount.IsRequired = true;
            this.txtDeductionAmount.Location = new System.Drawing.Point(492, 37);
            this.txtDeductionAmount.MaxLength = 6;
            this.txtDeductionAmount.Name = "txtDeductionAmount";
            this.txtDeductionAmount.NumberFormat = "###,###,##0.00";
            this.txtDeductionAmount.Postfix = "";
            this.txtDeductionAmount.Prefix = "";
            this.txtDeductionAmount.Size = new System.Drawing.Size(105, 20);
            this.txtDeductionAmount.SkipValidation = false;
            this.txtDeductionAmount.TabIndex = 5;
            this.txtDeductionAmount.TextType = CrplControlLibrary.TextType.Integer;
            this.txtDeductionAmount.Leave += new System.EventHandler(this.txtDeductionAmount_Leave);
            this.txtDeductionAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtDeductionAmount_Validating);
            // 
            // txtDeductionDescription
            // 
            this.txtDeductionDescription.AllowSpace = true;
            this.txtDeductionDescription.AssociatedLookUpName = "";
            this.txtDeductionDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDeductionDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDeductionDescription.ContinuationTextBox = null;
            this.txtDeductionDescription.CustomEnabled = true;
            this.txtDeductionDescription.DataFieldMapping = "SP_DED_DESC";
            this.txtDeductionDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeductionDescription.GetRecordsOnUpDownKeys = false;
            this.txtDeductionDescription.IsDate = false;
            this.txtDeductionDescription.IsRequired = true;
            this.txtDeductionDescription.Location = new System.Drawing.Point(227, 15);
            this.txtDeductionDescription.MaxLength = 30;
            this.txtDeductionDescription.Name = "txtDeductionDescription";
            this.txtDeductionDescription.NumberFormat = "###,###,##0.00";
            this.txtDeductionDescription.Postfix = "";
            this.txtDeductionDescription.Prefix = "";
            this.txtDeductionDescription.Size = new System.Drawing.Size(183, 20);
            this.txtDeductionDescription.SkipValidation = false;
            this.txtDeductionDescription.TabIndex = 2;
            this.txtDeductionDescription.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDeductionCode
            // 
            this.txtDeductionCode.AllowSpace = true;
            this.txtDeductionCode.AssociatedLookUpName = "lkpValidDeductionCode";
            this.txtDeductionCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDeductionCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDeductionCode.ContinuationTextBox = null;
            this.txtDeductionCode.CustomEnabled = true;
            this.txtDeductionCode.DataFieldMapping = "SP_DED_CODE";
            this.txtDeductionCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeductionCode.GetRecordsOnUpDownKeys = false;
            this.txtDeductionCode.IsDate = false;
            this.txtDeductionCode.IsRequired = true;
            this.txtDeductionCode.Location = new System.Drawing.Point(102, 15);
            this.txtDeductionCode.MaxLength = 3;
            this.txtDeductionCode.Name = "txtDeductionCode";
            this.txtDeductionCode.NumberFormat = "###,###,##0.00";
            this.txtDeductionCode.Postfix = "";
            this.txtDeductionCode.Prefix = "";
            this.txtDeductionCode.Size = new System.Drawing.Size(48, 20);
            this.txtDeductionCode.SkipValidation = true;
            this.txtDeductionCode.TabIndex = 1;
            this.txtDeductionCode.TextType = CrplControlLibrary.TextType.String;
            this.txtDeductionCode.Leave += new System.EventHandler(this.txtDeductionCode_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(427, 39);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Amount :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(16, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Account No. :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(30, 40);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "From / To :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(53, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Code :";
            // 
            // slPnlDetail
            // 
            this.slPnlDetail.ConcurrentPanels = null;
            this.slPnlDetail.Controls.Add(this.sldgvDetail);
            this.slPnlDetail.DataManager = "iCORE.CHRIS.DATAOBJECTS.CmnDataManager";
            this.slPnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlDetail.DependentPanels = null;
            this.slPnlDetail.DisableDependentLoad = false;
            this.slPnlDetail.EnableDelete = true;
            this.slPnlDetail.EnableInsert = true;
            this.slPnlDetail.EnableQuery = false;
            this.slPnlDetail.EnableUpdate = true;
            this.slPnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeductionDetailsCommand";
            this.slPnlDetail.Location = new System.Drawing.Point(9, 19);
            this.slPnlDetail.MasterPanel = null;
            this.slPnlDetail.Name = "slPnlDetail";
            this.slPnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlDetail.Size = new System.Drawing.Size(626, 256);
            this.slPnlDetail.SPName = "CHRIS_SP_DEDUCTION_DETAILS_MANAGER";
            this.slPnlDetail.TabIndex = 11;
            // 
            // sldgvDetail
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.sldgvDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.sldgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sldgvDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SP_P_NO,
            this.W_NAME,
            this.SP_DED_AMT,
            this.SP_REMARKS});
            this.sldgvDetail.ColumnToHide = null;
            this.sldgvDetail.ColumnWidth = null;
            this.sldgvDetail.CustomEnabled = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.sldgvDetail.DefaultCellStyle = dataGridViewCellStyle3;
            this.sldgvDetail.DisplayColumnWrapper = null;
            this.sldgvDetail.GridDefaultRow = 0;
            this.sldgvDetail.Location = new System.Drawing.Point(6, 14);
            this.sldgvDetail.Name = "sldgvDetail";
            this.sldgvDetail.ReadOnlyColumns = "W_NAME";
            this.sldgvDetail.RequiredColumns = "SP_P_NO";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.sldgvDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.sldgvDetail.Size = new System.Drawing.Size(606, 231);
            this.sldgvDetail.SkippingColumns = null;
            this.sldgvDetail.TabIndex = 0;
            this.sldgvDetail.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.sldgvDetail_CellValueChanged);
            this.sldgvDetail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.sldgvDetail_KeyDown);
            // 
            // SP_P_NO
            // 
            this.SP_P_NO.ActionLOV = "ValidPersonnelNumbersLOV";
            this.SP_P_NO.ActionLOVExists = "ValidPersonnelNumbersLovEXIST";
            this.SP_P_NO.AttachParentEntity = false;
            this.SP_P_NO.DataPropertyName = "SP_P_NO";
            this.SP_P_NO.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeductionDetailsCommand";
            this.SP_P_NO.HeaderText = "P.No.";
            this.SP_P_NO.LookUpTitle = "Personnel Info";
            this.SP_P_NO.LOVFieldMapping = "SP_P_NO";
            this.SP_P_NO.MaxInputLength = 6;
            this.SP_P_NO.Name = "SP_P_NO";
            this.SP_P_NO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_P_NO.SearchColumn = "SP_P_NO";
            this.SP_P_NO.SkipValidationOnLeave = false;
            this.SP_P_NO.SpName = "CHRIS_SP_DEDUCTION_DETAILS_MANAGER";
            this.SP_P_NO.Width = 70;
            // 
            // W_NAME
            // 
            this.W_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.W_NAME.DataPropertyName = "PR_NAME";
            this.W_NAME.HeaderText = "Name";
            this.W_NAME.MaxInputLength = 40;
            this.W_NAME.Name = "W_NAME";
            // 
            // SP_DED_AMT
            // 
            this.SP_DED_AMT.DataPropertyName = "SP_DED_AMT";
            dataGridViewCellStyle2.NullValue = null;
            this.SP_DED_AMT.DefaultCellStyle = dataGridViewCellStyle2;
            this.SP_DED_AMT.HeaderText = "Ded. Amount";
            this.SP_DED_AMT.MaxInputLength = 12;
            this.SP_DED_AMT.Name = "SP_DED_AMT";
            this.SP_DED_AMT.Width = 105;
            // 
            // SP_REMARKS
            // 
            this.SP_REMARKS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SP_REMARKS.DataPropertyName = "SP_REMARKS";
            this.SP_REMARKS.HeaderText = "Remarks";
            this.SP_REMARKS.MaxInputLength = 30;
            this.SP_REMARKS.Name = "SP_REMARKS";
            // 
            // gboMaster
            // 
            this.gboMaster.Controls.Add(this.slPnlMaster);
            this.gboMaster.Location = new System.Drawing.Point(7, 141);
            this.gboMaster.Name = "gboMaster";
            this.gboMaster.Size = new System.Drawing.Size(645, 118);
            this.gboMaster.TabIndex = 12;
            this.gboMaster.TabStop = false;
            // 
            // gboDetail
            // 
            this.gboDetail.Controls.Add(this.slPnlDetail);
            this.gboDetail.Location = new System.Drawing.Point(7, 273);
            this.gboDetail.Name = "gboDetail";
            this.gboDetail.Size = new System.Drawing.Size(645, 285);
            this.gboDetail.TabIndex = 13;
            this.gboDetail.TabStop = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(89, 90);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(88, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 0;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(45, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "User :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Location :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(261, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "PAYROLL SYSTEM";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(229, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(201, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "INDIVIDUAL DEDUCTION ENTRY";
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(89, 112);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(88, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 17;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtOptionView
            // 
            this.txtOptionView.AllowSpace = true;
            this.txtOptionView.AssociatedLookUpName = "";
            this.txtOptionView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOptionView.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOptionView.ContinuationTextBox = null;
            this.txtOptionView.CustomEnabled = true;
            this.txtOptionView.DataFieldMapping = "";
            this.txtOptionView.Enabled = false;
            this.txtOptionView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOptionView.GetRecordsOnUpDownKeys = false;
            this.txtOptionView.IsDate = false;
            this.txtOptionView.Location = new System.Drawing.Point(545, 90);
            this.txtOptionView.MaxLength = 10;
            this.txtOptionView.Name = "txtOptionView";
            this.txtOptionView.NumberFormat = "###,###,##0.00";
            this.txtOptionView.Postfix = "";
            this.txtOptionView.Prefix = "";
            this.txtOptionView.ReadOnly = true;
            this.txtOptionView.Size = new System.Drawing.Size(53, 20);
            this.txtOptionView.SkipValidation = false;
            this.txtOptionView.TabIndex = 46;
            this.txtOptionView.TabStop = false;
            this.txtOptionView.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(487, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 45;
            this.label6.Text = "Option :";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Enabled = false;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(545, 112);
            this.txtDate.MaxLength = 11;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(83, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 44;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(497, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "Date :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(416, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 112;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Payroll_IndividualsDeductionEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 648);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.txtOptionView);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gboDetail);
            this.Controls.Add(this.gboMaster);
            this.CurrentPanelBlock = "slPnlMaster";
            this.CurrrentOptionTextBox = this.txtOptionView;
            this.F8OptionText = "[F8]=Add";
            this.Name = "CHRIS_Payroll_IndividualsDeductionEntry";
            this.ShowBottomBar = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Payroll_IndividualsDeductionEntry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Payroll_IndividualsDeductionEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.gboMaster, 0);
            this.Controls.SetChildIndex(this.gboDetail, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.txtUser, 0);
            this.Controls.SetChildIndex(this.txtLocation, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.txtDate, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.txtOptionView, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPnlMaster.ResumeLayout(false);
            this.slPnlMaster.PerformLayout();
            this.slPnlDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sldgvDetail)).EndInit();
            this.gboMaster.ResumeLayout(false);
            this.gboDetail.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPnlMaster;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPnlDetail;
        private System.Windows.Forms.GroupBox gboMaster;
        private System.Windows.Forms.GroupBox gboDetail;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txtLocation;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtAccountDescription;
        private CrplControlLibrary.SLTextBox txtAccountNo;
        private CrplControlLibrary.SLTextBox txtDeductionAmount;
        private CrplControlLibrary.SLTextBox txtDeductionDescription;
        private CrplControlLibrary.SLTextBox txtDeductionCode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView sldgvDetail;
        private CrplControlLibrary.SLTextBox txtOptionView;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.LookupButton lkpValidDeductionCode;
        private CrplControlLibrary.LookupButton lkpValidAccount;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.SLDatePicker sldpValidFrom;
        private CrplControlLibrary.SLDatePicker sldpValidTo;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn SP_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn W_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_DED_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_REMARKS;
    }
}