using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using System.IO;
using System.Text.RegularExpressions;
using iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_CHRSP : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        //Dictionary<string, object> BonusValues = new Dictionary<string, object>();
        Result rslt;
        string filePath = string.Empty;
        CHRIS_PayrollReports_CHRS reportObject = null;

        #region Constructors

        public CHRIS_Payroll_CHRSP()
        {
            InitializeComponent();
        }

        public CHRIS_Payroll_CHRSP(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
           
        }

        #endregion

        #region Methods
               
        protected override void CommonOnLoadMethods()
        {
            
            base.CommonOnLoadMethods();
            this.tbtAdd.Visible = false;
            this.tbtCancel.Visible = false;
            this.tbtDelete.Visible = false;
            this.tbtEdit.Visible = false;
            this.tbtList.Visible = false;
            this.tbtSave.Visible = false;
            label_Amt1_tot.Text = string.Empty;
            label_amt_tot.Text = string.Empty;
            label_l1.Text = string.Empty;
            this.txtOption.Visible = false;
            lblUserName.Text += "   " + this.UserName;
            //dgvView.ColumnHeadersDefaultCellStyle.Font.Italic = true;
            //this.FormTitleColor = Color.Red;
        }

        private bool GenerateFile()
        {

            try
            {
               filePath = txt_FilePath.Text;//"G:\fate.fate.txt._txt";
                DirectoryInfo info = new DirectoryInfo(filePath);

                //Path.GetDirectoryName(filePath)
                if (info.Root.Exists)// (Directory.Exists(filePath))
                {
                    if (info.Extension != ".txt")
                    {
                         filePath = Path.ChangeExtension(filePath, ".txt");
                    }
                    rslt = GetFileData();
                        if (rslt.isSuccessful)
                        {
                            if (rslt.dstResult != null && rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                TextWriter tw = new System.IO.StreamWriter(filePath);
                                if (rslt.dstResult.Tables[0].Rows[0]["Header"] != null)
                                    tw.WriteLine(rslt.dstResult.Tables[0].Rows[0]["Header"].ToString());

                                if (rslt.dstResult.Tables[1].Rows.Count > 0)
                                {
                                    for (int i = 0; i < rslt.dstResult.Tables[1].Rows.Count; i++)
                                    {
                                        if (rslt.dstResult.Tables[1].Rows[i]["B"] != null)
                                            tw.WriteLine(rslt.dstResult.Tables[1].Rows[i]["B"].ToString());

                                    }
                                }
                                if (rslt.dstResult.Tables[2].Rows.Count > 0)
                                {
                                    if (rslt.dstResult.Tables[2].Rows[0]["footer"] != null)
                                        tw.WriteLine(rslt.dstResult.Tables[2].Rows[0]["footer"].ToString());
                                }

                                tw.Close();
                                return true;
                            }

                        }

                        return true;
                    }
                    #region elseCase Commented Code
                    //else
                    //{
                    //    //info.Extension = ".txt";
                    //    filePath = Path.ChangeExtension(filePath, ".txt");
                    //    rslt = GetFileData();
                    //    if (rslt.isSuccessful)
                    //    {
                    //        if (rslt.dstResult != null && rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                    //        {
                    //            TextWriter tw = new System.IO.StreamWriter(filePath);
                    //            if (rslt.dstResult.Tables[0].Rows[0]["Header"] != null)
                    //                tw.WriteLine(rslt.dstResult.Tables[0].Rows[0]["Header"].ToString());
                    //            if (rslt.dstResult.Tables[1].Rows.Count > 0)
                    //            {
                    //                for (int i = 0; i < rslt.dstResult.Tables[1].Rows.Count; i++)
                    //                {
                    //                    if (rslt.dstResult.Tables[1].Rows[i]["B"] != null)
                    //                        tw.WriteLine(rslt.dstResult.Tables[1].Rows[i]["B"].ToString());

                    //                }
                    //            }
                    //            if (rslt.dstResult.Tables[2].Rows.Count > 0)
                    //            {
                    //                if (rslt.dstResult.Tables[2].Rows[i]["footer"] != null)
                    //                    tw.WriteLine(rslt.dstResult.Tables[2].Rows[i]["footer"].ToString());
                    //            }
                    //            // tw.WriteLine(DateTime.Now);
                    //            tw.Close();
                    //            return true;
                    //        }

                    //    }

                //        return true;
                //        //File has been created succesfully on '||:file||' Path  With Name 

                //    }
                //}
#endregion
                else
                {
                    MessageBox.Show("Enter Valid File Path with Name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return false;//fileGeneratedSuccfully;
                }
               
            }
            catch (Exception ioExcep)
            {
                this.panel_Chrsp.Enabled = true;
                this.panel_generateFile.Visible = false;
                this.slButton_View.Focus();
                base.LogError("GenerateFile,CHRIS_Payroll_CHRSP", ioExcep);
                return false;
            }

        }

        private Result GetFileData()
        {
            Result fileResult;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Clear();
            param.Add("VALDATE", slDatePicker_validate.Value);
            fileResult = cmnDM.GetData("CHRIS_SP_FILE_TABLE_CREATION", "", param);
            return fileResult;
            //CHRIS_SP_FILE_TABLE_CREATION
        }


        #endregion

        #region Events

        private void slButton_View_Click(object sender, EventArgs e)
        {

            label_Amt1_tot.Text = string.Empty;
            label_amt_tot.Text = string.Empty;
            label_amt_tot.TextAlign = ContentAlignment.MiddleRight;
            label_l1.Text = string.Empty;
            rslt = cmnDM.GetData("CHRIS_PR_VIEW_CHRSP", "");
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        dgvView.AutoGenerateColumns = false;
                        dgvView.DataSource = rslt.dstResult.Tables[0];
                        // dgvView.DataSource = rslt.dstResult.Tables[0];
                    }
                    if (rslt.dstResult.Tables[1].Rows.Count > 0)
                    {
                        if (rslt.dstResult.Tables[1].Rows[0]["AMT_TOT"] != null)
                            label_amt_tot.Text = rslt.dstResult.Tables[1].Rows[0]["AMT_TOT_VAL"].ToString();
                        if (rslt.dstResult.Tables[1].Rows[0]["AMT1_TOT"] != null)
                            label_Amt1_tot.Text = rslt.dstResult.Tables[1].Rows[0]["AMT1_TOT"].ToString();
                    }
                    else
                    {
                        label_amt_tot.Text = ".00";
                        label_Amt1_tot.Text = "000000000000000";

                    }
                    if (rslt.dstResult.Tables[2].Rows.Count > 0)
                    {
                        if (rslt.dstResult.Tables[2].Rows[0][0] != null)
                            label_l1.Text = rslt.dstResult.Tables[2].Rows[0][0].ToString();
                    }

                }
            }

        }

        private void slButton_Exit_Click(object sender, EventArgs e)
        {
            base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
            //this.Close();
            // base.DoToolbarActions(this.Controls, "Close");
        }

        private void slButton_Generate_Click(object sender, EventArgs e)
        {
            txt_FilePath.Text = string.Empty;
            this.panel_Chrsp.Enabled = false;
            this.panel_generateFile.Visible = true;
            this.txt_FilePath.Focus();

        }

        private void slButton_ExitFile_Click(object sender, EventArgs e)
        {
            this.panel_Chrsp.Enabled = true;
            this.panel_generateFile.Visible = false;
            this.slButton_View.Focus();

        }

        private void slButton_StartGeneration_Click(object sender, EventArgs e)
        {
            if (txt_FilePath.Text.Trim() != string.Empty)
            {
                bool fileGeneratedSuccessfully = false;
                fileGeneratedSuccessfully = GenerateFile();
                if (fileGeneratedSuccessfully)
                {
                    MessageBox.Show("File has been created succesfully on  "+filePath + "  Path With Name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    this.panel_Chrsp.Enabled = true;
                    this.panel_generateFile.Visible = false;
                    this.slButton_View.Focus();

                }
                else
                {
                    MessageBox.Show("Some Error Occured While Generating File.Please Try again Later.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    this.panel_Chrsp.Enabled = true;
                    this.panel_generateFile.Visible = false;
                    this.slButton_View.Focus();
                }

            }
            else
            {
                MessageBox.Show("Value Date or Path Must Be Entered", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void slButton_Print_Click(object sender, EventArgs e)
        {
            if (null==reportObject)
            {
                reportObject = new CHRIS_PayrollReports_CHRS(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean);
                reportObject.MdiParent = null;
                reportObject.ShowDialog();
                this.reportObject = null;
                
            }

        }
        #endregion
    }
}