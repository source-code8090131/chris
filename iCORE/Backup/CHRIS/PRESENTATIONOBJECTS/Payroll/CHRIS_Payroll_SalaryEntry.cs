using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_SalaryEntry : ChrisSimpleForm
    {

        #region Declarations
        CmnDataManager objCmnDataManager = new CmnDataManager();
        /*-------------To store data block variables--------------*/
        Dictionary<string, object> DataBlockValues = new Dictionary<string, object>();
        /*------To store input parameters of StoredProcedure------*/
        Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
        //Result rslt;
        #endregion

        #region Constructors

        public CHRIS_Payroll_SalaryEntry()
        {
            InitializeComponent();
        }
        
        public CHRIS_Payroll_SalaryEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            tbtAdd.Visible      = false;
            tbtDelete.Visible   = false;
            tbtEdit.Visible     = false;
            tbtList.Visible     = false;
            tbtSave.Visible     = false;
            txtOption.Visible   = false;
            string w_date_value     = "1/7/" + DateTime.Now.AddDays(-360).Year.ToString();
            slDatePicker_W_FROM.Value = Convert.ToDateTime(w_date_value);
            w_date_value            = "1/4/" + DateTime.Now.Year.ToString();
            slDatePicker_W_TO.Value = Convert.ToDateTime(w_date_value);
            this.txtDate.Text       = System.DateTime.Now.ToString("dd/MM/yyyy"); //yyyy/MM/dd
            this.txtLocation1.Text  = this.CurrentLocation;
            this.txtUser1.Text      = this.userID;
            this.lbl_UserName.Text  = "User Name : " + this.UserName;
            this.slDatePicker_W_FROM.Select();
            this.slDatePicker_W_FROM.Focus();
        }

        /// <summary>
        /// Override Toolbar to Skip VAlidation on Personnel No. LOV
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                
                string w_date_value         = "1/7/" + DateTime.Now.AddDays(-360).Year.ToString();
                slDatePicker_W_FROM.Value   = Convert.ToDateTime(w_date_value);
                w_date_value                = "1/4/" + DateTime.Now.Year.ToString();
                slDatePicker_W_TO.Value     = Convert.ToDateTime(w_date_value);
                this.txtDate.Text           = System.DateTime.Now.ToString("dd/MM/yyyy"); //yyyy/MM/dd
                this.txtLocation1.Text      = this.CurrentLocation;
                this.txtUser1.Text          = this.userID;
                this.lbl_UserName.Text      = "User Name : " + this.UserName;
                txt_W_BRN.IsRequired = false;
                txt_W_ANS.IsRequired = false;
                this.slDatePicker_W_FROM.Select();
                this.slDatePicker_W_FROM.Focus();
                base.DoToolbarActions(ctrlsCollection, actionType);
                this.slDatePicker_W_FROM.Select();
                this.slDatePicker_W_FROM.Focus();
                txt_W_BRN.IsRequired = true;
                txt_W_ANS.IsRequired = true;
                return;
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        private void InitializeDic()
        {

            //69 feilds.....no comments
            this.DataBlockValues.Clear();
            DataBlockValues.Add("W_FROM",null);
            DataBlockValues.Add("W_TO", null);
            DataBlockValues.Add("W_BRN", null);
            DataBlockValues.Add("W_MOLD", null);
            DataBlockValues.Add("W_COLD", null);
            DataBlockValues.Add("W_MEAL_AMT", null);
            DataBlockValues.Add("W_CONV_AMT", null);
            DataBlockValues.Add("W_LFA", null);
            DataBlockValues.Add("W_NEW_LFA", null);
            DataBlockValues.Add("W_MED", null);
            DataBlockValues.Add("W_NEW_MED", null);
            DataBlockValues.Add("PR_P_NO", null);
            DataBlockValues.Add("W_DATE", null);
            DataBlockValues.Add("W_DATE1", null);
            DataBlockValues.Add("W_CATE", null);
            DataBlockValues.Add("W_INCOME", null);
            DataBlockValues.Add("W_TAX_REC", null);
            DataBlockValues.Add("PR_PACK", null);
            DataBlockValues.Add("LEV", null);
            DataBlockValues.Add("ORG", null);
            DataBlockValues.Add("STEP", null);
            DataBlockValues.Add("ADJ", null);
            DataBlockValues.Add("AREAR", null);
            DataBlockValues.Add("W_MONTHS", null);
            DataBlockValues.Add("START1", null);         
            DataBlockValues.Add("W_JOINING", null);
            DataBlockValues.Add("W_EFF", null);
            DataBlockValues.Add("W_PF", null);
            DataBlockValues.Add("W_SD1", null);
            DataBlockValues.Add("W_SD2", null);
            DataBlockValues.Add("W_SHR", null);
            DataBlockValues.Add("W_DHR", null);
            DataBlockValues.Add("W_MEAL", null);
            DataBlockValues.Add("W_CONV", null);
            DataBlockValues.Add("W_FOUND", null);
            DataBlockValues.Add("W_SAL_AREAR", null);
            DataBlockValues.Add("W_PROMOTED", null);
            DataBlockValues.Add("W_FORCAST_OT", null);
            DataBlockValues.Add("W_ACTUAL_OT", null);
            DataBlockValues.Add("W_AVG", null);
            DataBlockValues.Add("W_AREARS", null);
            DataBlockValues.Add("W_OT_MONTHS", null);
            DataBlockValues.Add("W_FORCAST_MONTHS", null);
            DataBlockValues.Add("W_FORCAST", null);
            DataBlockValues.Add("W_OT_ASR", null);
            DataBlockValues.Add("W_PR_DATE", null);
            DataBlockValues.Add("W_TRANS_DATE", null);
            DataBlockValues.Add("W_DEPT_FT", null);
            DataBlockValues.Add("W_TAX_PACK", null);
            DataBlockValues.Add("W_10_BONUS", null);
            DataBlockValues.Add("W_ANNUAL_BASIC", null);
            DataBlockValues.Add("W_GOVT_ALL", null);
            DataBlockValues.Add("W_AN_PACK", null);
            DataBlockValues.Add("W_P_BASIC", null);
            DataBlockValues.Add("W_DESIG", null);
            DataBlockValues.Add("W_LEVEL", null);
            DataBlockValues.Add("W_OTHER_ALL", null);
            DataBlockValues.Add("W_BASIC_AREAR", null);
            DataBlockValues.Add("W_BRANCH", null);
            DataBlockValues.Add("W_VP", null);
            DataBlockValues.Add("W_ZAKAT", null);
            DataBlockValues.Add("W_PRV_INC", null);
            DataBlockValues.Add("W_TAX_USED", null);
            DataBlockValues.Add("W_TAXABLE_INC", null);
            DataBlockValues.Add("W_PRV_TAX_PD", null);
            DataBlockValues.Add("W_ANNUAL_TAX", null);
            DataBlockValues.Add("W_ITAX", null);
            DataBlockValues.Add("W_INC_EFF", null);
            DataBlockValues.Add("W_CONF", null);

            DataBlockValues.Add("W_TAX1", null);
            DataBlockValues.Add("W_TAX2", null);
            DataBlockValues.Add("W_AREAR_TAX", null);

            DataBlockValues["W_OT_ASR"] = 0;
            DataBlockValues["W_GOVT_ALL"] = 0;
            DataBlockValues["W_ITAX"] = 0;
            DataBlockValues["W_P_BASIC"] = 0;
            DataBlockValues["W_ANNUAL_TAX"] = 0;
            DataBlockValues["W_BASIC_AREAR"] = 0;
            DataBlockValues["W_SAL_AREAR"] = 0;
            DataBlockValues["W_FORCAST"] = 0;
            DataBlockValues["W_AREAR_TAX"] = 0;
            
        }

        void StartProcessing()
        {
            try
            {            
                Proc1();
                DialogResult dr = MessageBox.Show("Process Complete", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (dr == DialogResult.OK)
                {
                    this.Close();
                }
                //MessageBox.Show("Process Complete");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Proc1()
        {
            DataTable dtEmployee = null;

            //PROC_1 LOCAL VARIABLES
            float wtax1 = 0;
            float wtax2 = 0;  

            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pW_BRN", DataBlockValues["W_BRN"]);
                dicInputParameters.Add("pW_FROM", DataBlockValues["W_FROM"]);
                dicInputParameters.Add("pW_TO", DataBlockValues["W_TO"]);

                rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC1","", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dtEmployee = rslt.dstResult.Tables[0];
                                for (int EmployeeNo = 0; EmployeeNo < dtEmployee.Rows.Count; EmployeeNo++)
                                {
                                    DataBlockValues["PR_P_NO"] = dtEmployee.Rows[EmployeeNo]["pr_p_no"];
                                    DataBlockValues["W_CATE"] = dtEmployee.Rows[EmployeeNo]["pr_category"];
                                    DataBlockValues["LEV"] = dtEmployee.Rows[EmployeeNo]["pr_level"];
                                    DataBlockValues["PR_PACK"] = dtEmployee.Rows[EmployeeNo]["pr_new_annual_pack"];
                                    DataBlockValues["W_JOINING"] = dtEmployee.Rows[EmployeeNo]["pr_joining_date"];
                                    DataBlockValues["W_CONF"] = dtEmployee.Rows[EmployeeNo]["pr_conf_flag"];
                                    DataBlockValues["W_DESIG"] = dtEmployee.Rows[EmployeeNo]["pr_desig"];
                                    DataBlockValues["W_LEVEL"] = dtEmployee.Rows[EmployeeNo]["pr_level"];
                                    DataBlockValues["W_ZAKAT"] = dtEmployee.Rows[EmployeeNo]["pr_zakat_amt"];
                                    DataBlockValues["W_PRV_INC"] = dtEmployee.Rows[EmployeeNo]["pr_tax_inc"];
                                    DataBlockValues["W_PRV_TAX_PD"] = dtEmployee.Rows[EmployeeNo]["pr_tax_paid"];
                                    DataBlockValues["W_TAX_USED"] = dtEmployee.Rows[EmployeeNo]["pr_tax_used"];
                                    DataBlockValues["W_BRANCH"] = dtEmployee.Rows[EmployeeNo]["pr_new_branch"];
                                    DataBlockValues["W_PR_DATE"] = dtEmployee.Rows[EmployeeNo]["pr_promotion_date"];
                                    DataBlockValues["W_TRANS_DATE"] = dtEmployee.Rows[EmployeeNo]["pr_transfer_date"];
                                    DataBlockValues["W_AN_PACK"] = dtEmployee.Rows[EmployeeNo]["pr_new_annual_pack"];
                                    DataBlockValues["W_PROMOTED"] = null;

                                    if (DataBlockValues["W_CATE"] != DBNull.Value)
                                    {
                                        if (DataBlockValues["W_CATE"].ToString() == "C")
                                        {
                                            Proc2();
                                        }
                                        else
                                        {
                                            Proc4();
                                        }
                                    }

                                    Proc90();

                                    if (DataBlockValues["W_CATE"].ToString() == "C")
                                    {
                                        Proc97();
                                    }

                                    Proc96();

                                    Proc99();

                                    DataBlockValues["W_TAX1"] = DataBlockValues["W_ANNUAL_TAX"];

                                    Proc9();

                                    Proc99();

                                    DataBlockValues["W_TAX2"] = DataBlockValues["W_ANNUAL_TAX"];

                                    UpdateSalArear();              
                                }
                            }
                        }
                    }
                }               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc2()
        {
            try
            {
                DataTable dtPROC2 = null;
                char WFOUND = 'N';
                int sw = 0;
                DateTime w_dt = new DateTime() ;
                DateTime w_dt2 = DateTime.Parse(DataBlockValues["W_TO"].ToString());

                dicInputParameters.Clear();
               
                dicInputParameters.Add("pW_FROM", DataBlockValues["W_FROM"]);
                dicInputParameters.Add("pW_TO", DataBlockValues["W_TO"]);
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);

                rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC2","", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            #region Data return by Cursor query

                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dtPROC2 = rslt.dstResult.Tables[0];
                                int LastRecordProc2 = dtPROC2.Rows.Count - 1;//For Last record

                                for (int i = 0; i < dtPROC2.Rows.Count; i++)
                                {
                                    #region For Loop
                                    WFOUND = 'Y'; 
                                    if(sw == 0)
                                    {
                                        w_dt = DateTime.Parse(DataBlockValues["W_FROM"].ToString());
                                        sw = 1;
                                        DataBlockValues["PR_PACK"] = dtPROC2.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                        DataBlockValues["LEV"] = dtPROC2.Rows[i]["PR_LEVEL_PREVIOUS"];
                                        DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(DateTime.Parse(dtPROC2.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                        w_dt = DateTime.Parse(dtPROC2.Rows[i]["PR_EFFECTIVE"].ToString());
                                        DataBlockValues["W_EFF"] = w_dt;
                                        DataBlockValues["W_SD1"] = DataBlockValues["W_FROM"];
                                        DataBlockValues["W_SD2"] = w_dt;
                                        Proc5();
                                        //WFOUND = 'Y';
                                        continue; // loop continues to execute with the next iteration                    
                                     }

                                        DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(DateTime.Parse(dtPROC2.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                        DataBlockValues["PR_PACK"] = dtPROC2.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                        DataBlockValues["LEV"] = dtPROC2.Rows[i]["PR_LEVEL_PREVIOUS"];
                                        w_dt = DateTime.Parse(dtPROC2.Rows[i]["PR_EFFECTIVE"].ToString());
                                        DataBlockValues["W_EFF"] = w_dt;
                                        DataBlockValues["W_SD1"] = DataBlockValues["W_SD2"];
                                        DataBlockValues["W_SD2"] = w_dt;
                                        Proc5();
                                        #endregion
                                }

                                #region Last record
                                if (w_dt == null)
                                {
                                  w_dt  = DateTime.Parse(DataBlockValues["W_FROM"].ToString());
                                }
                                
                                if (WFOUND == 'Y')
                                {
                                      DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(w_dt2, w_dt); ;
                                      DataBlockValues["PR_PACK"] = dtPROC2.Rows[LastRecordProc2]["PR_ANNUAL_PRESENT"];
                                      DataBlockValues["LEV"] = dtPROC2.Rows[LastRecordProc2]["PR_LEVEL_PRESENT"];
                                      DataBlockValues["W_EFF"] = w_dt;
                                      DataBlockValues["W_SD1"] = DataBlockValues["W_SD2"];
                                      DataBlockValues["W_SD2"] = DataBlockValues["W_TO"];
                                      Proc5();
                                }
                                #endregion

                            }

                            #endregion

                            if (WFOUND == 'N')
                            {
                                Proc3();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc3()
        {
            try
            {
                DataTable dtPROC3 = null;
                char WFOUND = 'N';
                int sw = 0;
                DateTime w_dt = new DateTime();
                DateTime w_dt2 = DateTime.Parse(DataBlockValues["W_TO"].ToString());

                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);

                rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC3", "", dicInputParameters);
                
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            #region Data return by Cursor query

                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dtPROC3 = rslt.dstResult.Tables[0];
                                int LastRecordProc3 = dtPROC3.Rows.Count - 1;//For Last record

                                for (int i = 0; i < dtPROC3.Rows.Count; i++)
                                {
                                    #region For Loop 

                                    WFOUND = 'Y';

                                    if (DateTime.Parse(dtPROC3.Rows[0]["pr_effective"].ToString()) < DateTime.Parse(DataBlockValues["W_FROM"].ToString()))
                                    {
                                        DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(DateTime.Parse(DataBlockValues["W_TO"].ToString()), DateTime.Parse(DataBlockValues["W_FROM"].ToString()));
                                        DataBlockValues["PR_PACK"] = dtPROC3.Rows[i]["PR_ANNUAL_PRESENT"];
                                        DataBlockValues["LEV"] = dtPROC3.Rows[i]["PR_LEVEL_PRESENT"];
                                        DataBlockValues["W_EFF"] = null;
                                        DataBlockValues["W_SD1"] = DataBlockValues["W_FROM"];
                                        DataBlockValues["W_SD2"] = DataBlockValues["W_TO"];
                                        Proc5();
                                        WFOUND = 'C';
                                        break; //looping is broken and stop
                                    }

                                    if (sw == 0)
                                    {
                                         w_dt  = DateTime.Parse(DataBlockValues["W_FROM"].ToString());
                                         sw = 1;
                                         DataBlockValues["PR_PACK"]  =  dtPROC3.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                         DataBlockValues["LEV"]  = dtPROC3.Rows[i]["PR_LEVEL_PREVIOUS"];
                                         DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(DateTime.Parse(dtPROC3.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                         w_dt = DateTime.Parse(dtPROC3.Rows[i]["PR_EFFECTIVE"].ToString());
                                         DataBlockValues["W_EFF"] = w_dt;
                                         Proc5();
                                         continue; // loop continues to execute with the next iteration                    
                                         
                                    }

                                    DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(DateTime.Parse(dtPROC3.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                    DataBlockValues["PR_PACK"]  =  dtPROC3.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                    DataBlockValues["LEV"]  = dtPROC3.Rows[i]["PR_LEVEL_PREVIOUS"];
                                    w_dt = DateTime.Parse(dtPROC3.Rows[i]["PR_EFFECTIVE"].ToString());
                                    DataBlockValues["W_EFF"] = w_dt;
                                    DataBlockValues["W_SD1"] = DataBlockValues["W_SD2"];
                                    DataBlockValues["W_SD2"] = w_dt;
                                    Proc5();

                                    #endregion
                                } //end loop

                                #region Last record

                                if (w_dt == null)
                                {
                                    w_dt = DateTime.Parse(DataBlockValues["W_FROM"].ToString());
                                }

                                if ((dtPROC3.Rows[LastRecordProc3]["pr_promoted"] == DBNull.Value) && (WFOUND == 'Y'))
                                {
                                    DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(w_dt2, w_dt);
                                    DataBlockValues["PR_PACK"] = dtPROC3.Rows[LastRecordProc3]["PR_ANNUAL_PRESENT"];
                                    DataBlockValues["LEV"] = dtPROC3.Rows[LastRecordProc3]["PR_LEVEL_PRESENT"];
                                    DataBlockValues["W_EFF"] = w_dt;
                                    Proc5();
                                }

                                #endregion
                              
                            }

                            #endregion

                            if (WFOUND == 'N')
                            {
                                DataBlockValues["W_FOUND"] = 1;
                                DataBlockValues["W_SD1"] = DataBlockValues["W_FROM"];
                                DataBlockValues["W_SD2"] = DataBlockValues["W_TO"];
                                DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(DateTime.Parse(DataBlockValues["W_TO"].ToString()), DateTime.Parse(DataBlockValues["W_JOINING"].ToString()));
                                if (DateTime.Parse(DataBlockValues["W_JOINING"].ToString()) < DateTime.Parse(DataBlockValues["W_FROM"].ToString()))
                                {
                                    DataBlockValues["W_MONTHS"] = 9;
                                }
                                Proc5();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc4()
        {
            try
            {
                DataTable dtPROC4 = null;
                char WFOUND = 'N';
                int sw = 0;
                DateTime w_dt = new DateTime() ;
                DateTime w_dt2 = new DateTime();

                dicInputParameters.Clear();
               
                dicInputParameters.Add("pW_FROM", DataBlockValues["W_FROM"]);
                dicInputParameters.Add("pW_TO", DataBlockValues["W_TO"]);
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);

                rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC4","", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            #region Data return by Cursor query

                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dtPROC4 = rslt.dstResult.Tables[0];
                                int LastRecordProc4 = dtPROC4.Rows.Count - 1;//For Last record

                                for (int i = 0; i < dtPROC4.Rows.Count; i++)
                                {
                                    #region For Loop

                                    WFOUND = 'Y';
                                    if (sw == 0)
                                    {
                                        w_dt = DateTime.Parse(DataBlockValues["W_FROM"].ToString());
                                        sw = 1;
                                        DataBlockValues["PR_PACK"] = dtPROC4.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                        DataBlockValues["LEV"] = dtPROC4.Rows[i]["PR_LEVEL_PREVIOUS"];
                                        DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(DateTime.Parse(dtPROC4.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                        w_dt = DateTime.Parse(dtPROC4.Rows[i]["PR_EFFECTIVE"].ToString());
                                        DataBlockValues["W_EFF"] = null;
                                        DataBlockValues["W_SD1"] = DataBlockValues["W_FROM"];
                                        DataBlockValues["W_SD2"] = w_dt;
                                        Proc5();
                                        //WFOUND = 'Y';
                                        continue; // loop continues to execute with the next iteration                    
                                    }
                                        DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(DateTime.Parse(dtPROC4.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                        DataBlockValues["PR_PACK"] = dtPROC4.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                        DataBlockValues["LEV"] = dtPROC4.Rows[i]["PR_LEVEL_PREVIOUS"];
                                        w_dt = DateTime.Parse(dtPROC4.Rows[i]["PR_EFFECTIVE"].ToString());
                                        DataBlockValues["W_EFF"] = w_dt;
                                        DataBlockValues["W_SD1"] = DataBlockValues["W_SD2"];
                                        DataBlockValues["W_SD2"] = w_dt;
                                        Proc5();
                                    
                                    #endregion
                                }

                                #region Last record

                                if (dtPROC4.Rows[LastRecordProc4]["pr_promoted"] == DBNull.Value)
                                {
                                    DataBlockValues["W_MONTHS"] = base.MonthsBetweenInOracle(w_dt2, w_dt); ;
                                    DataBlockValues["PR_PACK"] = dtPROC4.Rows[LastRecordProc4]["PR_ANNUAL_PRESENT"];
                                    DataBlockValues["LEV"] = dtPROC4.Rows[LastRecordProc4]["PR_LEVEL_PRESENT"];
                                    DataBlockValues["W_EFF"] = w_dt;
                                    DataBlockValues["W_SD1"] = DataBlockValues["W_SD2"];
                                    DataBlockValues["W_SD2"] = w_dt;
                                    Proc5();
                                }

                                #endregion                              
                            }

                            #endregion

                            if (WFOUND == 'N')
                            {
                                Proc3();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc5()
        {
            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("pW_BRN", DataBlockValues["W_BRN"]);
                dicInputParameters.Add("pW_FROM", DataBlockValues["W_FROM"]);
                dicInputParameters.Add("pLEV", DataBlockValues["LEV"]);
                dicInputParameters.Add("pPR_PACK", DataBlockValues["PR_PACK"]);
                dicInputParameters.Add("pW_MONTHS", DataBlockValues["W_MONTHS"]);
                dicInputParameters.Add("pW_SD1", DataBlockValues["W_SD1"]);
                dicInputParameters.Add("pW_SD2", DataBlockValues["W_SD2"]);
                dicInputParameters.Add("pW_MOLD", DataBlockValues["W_MOLD"]);
                dicInputParameters.Add("pW_COLD", DataBlockValues["W_COLD"]);
                //dicInputParameters.Add("pORG", DataBlockValues["ORG"]);
                //dicInputParameters.Add("pSTEP", DataBlockValues["STEP"]);            
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);              
                dicInputParameters.Add("pW_TO", DataBlockValues["W_TO"]);
                //dicInputParameters.Add("pW_SHR", DataBlockValues["W_SHR"]);
                //dicInputParameters.Add("pW_DHR", DataBlockValues["W_DHR"]);                         
                dicInputParameters.Add("pW_MEAL_AMT", DataBlockValues["W_MEAL_AMT"]);
                dicInputParameters.Add("pW_CONV_AMT", DataBlockValues["W_CONV_AMT"]);       

                rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC5", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataBlockValues["ORG"] = rslt.dstResult.Tables[0].Rows[0]["ORG"];
                                DataBlockValues["STEP"] = rslt.dstResult.Tables[0].Rows[0]["STEP"];
                                DataBlockValues["ADJ"] = rslt.dstResult.Tables[0].Rows[0]["ADJ"];
                                DataBlockValues["AREAR"] = rslt.dstResult.Tables[0].Rows[0]["AREAR"];
                                DataBlockValues["W_PF"] = rslt.dstResult.Tables[0].Rows[0]["W_PF"];
                                DataBlockValues["W_EFF"] = rslt.dstResult.Tables[0].Rows[0]["W_EFF"];
                                DataBlockValues["W_FOUND"] = rslt.dstResult.Tables[0].Rows[0]["W_FOUND"];
                                DataBlockValues["W_MEAL"] = rslt.dstResult.Tables[0].Rows[0]["W_MEAL"];
                                DataBlockValues["W_CONV"] = rslt.dstResult.Tables[0].Rows[0]["W_CONV"];
                                DataBlockValues["W_SHR"] = rslt.dstResult.Tables[0].Rows[0]["W_SHR"];
                                DataBlockValues["W_DHR"] = rslt.dstResult.Tables[0].Rows[0]["W_DHR"];
                                DataBlockValues["START1"] = rslt.dstResult.Tables[0].Rows[0]["START1"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc90()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);
                dicInputParameters.Add("pW_DATE", DataBlockValues["W_DATE"]);
                dicInputParameters.Add("pW_PR_DATE", DataBlockValues["W_PR_DATE"]);             

                rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC90","", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataBlockValues["W_DESIG"] = rslt.dstResult.Tables[0].Rows[0]["W_DESIG"];
                                DataBlockValues["W_LEVEL"] = rslt.dstResult.Tables[0].Rows[0]["W_LEVEL"];
                                DataBlockValues["W_PROMOTED"] = rslt.dstResult.Tables[0].Rows[0]["W_PROMOTED"];

                                if (rslt.dstResult.Tables[0].Columns.Count > 3)
                                {
                                    DataBlockValues["W_AN_PACK"] = rslt.dstResult.Tables[0].Rows[0]["W_AN_PACK"];         

                                    if (DataBlockValues["W_DESIG"].ToString() == "CTT")
                                    {
                                        DataBlockValues["W_CATE"] = "C";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc97()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pW_BRANCH", DataBlockValues["W_BRANCH"]);
                dicInputParameters.Add("pW_DESIG", DataBlockValues["W_DESIG"]);
                dicInputParameters.Add("pW_LEVEL", DataBlockValues["W_LEVEL"]);
                dicInputParameters.Add("pW_CATE", DataBlockValues["W_CATE"]);
                dicInputParameters.Add("pW_DATE", DataBlockValues["W_DATE"]);

                rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC97","", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataBlockValues["W_GOVT_ALL"] = rslt.dstResult.Tables[0].Rows[0]["w_govt_all"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc96()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);
                dicInputParameters.Add("pW_DATE", DataBlockValues["W_DATE"]);           

                rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC96", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataBlockValues["W_INC_EFF"] = rslt.dstResult.Tables[0].Rows[0]["w_inc_eff"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc99()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);
                dicInputParameters.Add("pW_DATE", DataBlockValues["W_DATE"]);
                dicInputParameters.Add("pW_TRANS_DATE", DataBlockValues["W_TRANS_DATE"]);
                dicInputParameters.Add("pW_BRANCH", DataBlockValues["W_BRANCH"]);
                dicInputParameters.Add("pW_DESIG", DataBlockValues["W_DESIG"]);
                dicInputParameters.Add("pW_LEVEL", DataBlockValues["W_LEVEL"]);
                dicInputParameters.Add("pW_CATE", DataBlockValues["W_CATE"]);
                dicInputParameters.Add("pW_AN_PACK", DataBlockValues["W_AN_PACK"]);
                dicInputParameters.Add("pW_VP", DataBlockValues["W_VP"]);
                dicInputParameters.Add("pW_ZAKAT", DataBlockValues["W_ZAKAT"]);
                dicInputParameters.Add("pW_PRV_INC", DataBlockValues["W_PRV_INC"]);
                dicInputParameters.Add("pW_TAX_USED", DataBlockValues["W_TAX_USED"]);
                dicInputParameters.Add("pW_PRV_TAX_PD", DataBlockValues["W_PRV_TAX_PD"]);
                dicInputParameters.Add("pW_PR_DATE", DataBlockValues["W_PR_DATE"]);

                dicInputParameters.Add("oW_OT_ASR", DataBlockValues["W_OT_ASR"]);
                dicInputParameters.Add("oW_GOVT_ALL", DataBlockValues["W_GOVT_ALL"]);
                dicInputParameters.Add("oW_P_BASIC", DataBlockValues["W_P_BASIC"]);
                dicInputParameters.Add("oW_ITAX", DataBlockValues["W_ITAX"]);
                dicInputParameters.Add("oW_ANNUAL_TAX", DataBlockValues["W_ANNUAL_TAX"]);
                dicInputParameters.Add("oW_BASIC_AREAR", DataBlockValues["W_BASIC_AREAR"]);
                dicInputParameters.Add("oW_SAL_AREAR", DataBlockValues["W_SAL_AREAR"]);
                dicInputParameters.Add("oW_FORCAST", DataBlockValues["W_FORCAST"]);

                rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC99", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataBlockValues["W_DATE1"] = rslt.dstResult.Tables[0].Rows[0]["W_DATE1"];
                                DataBlockValues["GLOBAL_W_10_AMT"] = rslt.dstResult.Tables[0].Rows[0]["GLOBAL_W_10_AMT"];
                                DataBlockValues["GLOBAL_W_INC_AMT"] = rslt.dstResult.Tables[0].Rows[0]["GLOBAL_W_INC_AMT"];
                                DataBlockValues["W_SAL_AREAR"] = rslt.dstResult.Tables[0].Rows[0]["W_SAL_AREAR"];
                                DataBlockValues["GLOBAL_FLAG"] = rslt.dstResult.Tables[0].Rows[0]["GLOBAL_FLAG"];
                                DataBlockValues["W_OT_ASR"] = rslt.dstResult.Tables[0].Rows[0]["W_OT_ASR"];
                                DataBlockValues["W_PROMOTED"] = rslt.dstResult.Tables[0].Rows[0]["W_PROMOTED"];
                                DataBlockValues["W_PR_DATE"] = rslt.dstResult.Tables[0].Rows[0]["W_PR_DATE"];
                                DataBlockValues["W_FORCAST_OT"] = rslt.dstResult.Tables[0].Rows[0]["W_FORCAST_OT"];
                                DataBlockValues["W_ACTUAL_OT"] = rslt.dstResult.Tables[0].Rows[0]["W_ACTUAL_OT"];
                                DataBlockValues["W_AVG"] = rslt.dstResult.Tables[0].Rows[0]["W_AVG"];
                                DataBlockValues["W_AREARS"] = rslt.dstResult.Tables[0].Rows[0]["W_AREARS"];
                                DataBlockValues["W_OT_MONTHS"] = rslt.dstResult.Tables[0].Rows[0]["W_OT_MONTHS"];
                                DataBlockValues["W_FORCAST_MONTHS"] = rslt.dstResult.Tables[0].Rows[0]["W_FORCAST_MONTHS"];
                                DataBlockValues["W_FORCAST"] = rslt.dstResult.Tables[0].Rows[0]["W_FORCAST"];
                                DataBlockValues["W_P_BASIC"] = rslt.dstResult.Tables[0].Rows[0]["W_P_BASIC"];
                                DataBlockValues["W_OTHER_ALL"] = rslt.dstResult.Tables[0].Rows[0]["W_OTHER_ALL"];
                                DataBlockValues["W_BASIC_AREAR"] = rslt.dstResult.Tables[0].Rows[0]["W_BASIC_AREAR"];
                                DataBlockValues["W_ANNUAL_BASIC"] = rslt.dstResult.Tables[0].Rows[0]["W_ANNUAL_BASIC"];
                                DataBlockValues["W_TAX_PACK"] = rslt.dstResult.Tables[0].Rows[0]["W_TAX_PACK"];
                                DataBlockValues["W_10_BONUS"] = rslt.dstResult.Tables[0].Rows[0]["W_10_BONUS"];
                                DataBlockValues["W_VP"] = rslt.dstResult.Tables[0].Rows[0]["W_VP"];
                                DataBlockValues["W_INCOME"] = rslt.dstResult.Tables[0].Rows[0]["W_INCOME"];
                                DataBlockValues["W_TAXABLE_INC"] = rslt.dstResult.Tables[0].Rows[0]["W_TAXABLE_INC"];
                                DataBlockValues["W_ANNUAL_TAX"] = rslt.dstResult.Tables[0].Rows[0]["W_ANNUAL_TAX"];
                                DataBlockValues["W_TAX_REC"] = rslt.dstResult.Tables[0].Rows[0]["W_TAX_REC"];
                                DataBlockValues["W_ITAX"] = rslt.dstResult.Tables[0].Rows[0]["W_ITAX"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc9()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);              

                rslt = objCmnDataManager.Execute("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC9", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        //Do not return anything
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void UpdateSalArear()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);
                dicInputParameters.Add("pW_FROM", DataBlockValues["W_FROM"]);
                dicInputParameters.Add("pW_TAX1", DataBlockValues["W_TAX1"]);
                dicInputParameters.Add("pW_TAX2", DataBlockValues["W_TAX2"]);
                dicInputParameters.Add("pW_JOINING", DataBlockValues["W_JOINING"]);
                dicInputParameters.Add("pW_DATE", DataBlockValues["W_DATE"]);
                dicInputParameters.Add("pW_CONF", DataBlockValues["W_CONF"]);
                dicInputParameters.Add("pW_PR_DATE", DataBlockValues["W_PR_DATE"]);
                dicInputParameters.Add("pW_PROMOTED", DataBlockValues["W_PROMOTED"]);
                dicInputParameters.Add("pW_NEW_LFA", DataBlockValues["W_NEW_LFA"]);
                dicInputParameters.Add("pW_NEW_MED", DataBlockValues["W_NEW_MED"]);
                dicInputParameters.Add("pW_LFA", DataBlockValues["W_LFA"]);
                dicInputParameters.Add("pW_MED", DataBlockValues["W_MED"]);

                rslt = objCmnDataManager.Execute("CHRIS_SP_UNION_ARREARS_PROCESSING_UpdateSalArear", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Events

        private void txt_W_MOLD_Validating(object sender, CancelEventArgs e)
        {
            if (txt_W_MOLD.Text.Length > 0)
            {
                int oldMealValue;
                if (!(int.TryParse(txt_W_MOLD.Text, out oldMealValue) && oldMealValue > 0))
                {
                    e.Cancel = true;
                    MessageBox.Show("Enter Old Meal Amount .....!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            else
                
                e.Cancel = true;
        }

        private void txt_W_CONV_AMT_Validating(object sender, CancelEventArgs e)
        {
            if (txt_W_CONV_AMT.Text.Length > 0)
            {
                int convValue;
                if (!(int.TryParse(txt_W_CONV_AMT.Text, out convValue) && convValue > 0))
                {
                    e.Cancel = true;
                    MessageBox.Show("Enter New Conveyance Rate .....!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            else
                e.Cancel = true;
        }

        private void txt_W_COLD_Validating(object sender, CancelEventArgs e)
        {
            if (txt_W_MOLD.Text.Length > 0)
            {
                int oldConvValue;
                if (!(int.TryParse(txt_W_MOLD.Text, out oldConvValue) && oldConvValue > 0))
                {
                    e.Cancel = true;
                    MessageBox.Show("Enter Old Conveyance Amount .....!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            else
                e.Cancel = true;
        }

        private void txt_W_MEAL_AMT_Validating(object sender, CancelEventArgs e)
        {
            if (txt_W_MEAL_AMT.Text.Length > 0)
            {
                int MealValue;
                if (!(int.TryParse(txt_W_MEAL_AMT.Text, out MealValue) && MealValue > 0))
                {
                    e.Cancel = true;
                    MessageBox.Show("Enter New Meal Rate .....!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            else
                e.Cancel = true;
        }

        private void txt_W_ANS_Validating(object sender, CancelEventArgs e)
        {
            if (txt_W_ANS.Text.Length > 0)
            {
                if (txt_W_ANS.Text.ToUpper() != "Y" && txt_W_ANS.Text.ToUpper() != "N")
                {
                    MessageBox.Show("Enter [Y] Or [N] .........!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    e.Cancel = true;
                }
                else if (txt_W_ANS.Text.ToUpper() == "N")
                {
                    this.Close();
                }
            }
            else
                e.Cancel = true;
        }
        
        private void txt_W_ANS_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((this.txt_W_ANS.Text != "Y") && (this.txt_W_ANS.Text != "N") && (e.KeyChar == '\r' || e.KeyChar == '\t'))
            //{
            //    MessageBox.Show("Enter [Y]es...or ..[N]o...");
            //}
            //else if ((this.txt_W_ANS.Text.ToUpper() == "N") && (e.KeyChar == '\r' || e.KeyChar == '\t'))
            //{
            //    this.Close();
            //}
            //else if ((this.txt_W_ANS.Text.ToUpper() == "Y") && (e.KeyChar == '\r' || e.KeyChar == '\t'))
            //{

            //    //InitializeDic();

            //    //DataBlockValues["W_FROM"]   = this.slDatePicker_W_FROM.Value;
            //    //DataBlockValues["W_TO"]     = this.slDatePicker_W_TO.Value;
            //    //DataBlockValues["W_BRN"]    = this.txt_W_BRN.Text;

            //    //DataBlockValues["W_MOLD"]   = this.txt_W_MOLD.Text;
            //    //DataBlockValues["W_COLD"]   = this.txt_W_COLD.Text;

            //    //DataBlockValues["W_CONV_AMT"] = this.txt_W_CONV_AMT.Text;
            //    //DataBlockValues["W_MEAL_AMT"] = this.txt_W_MEAL_AMT.Text;

            //    //DataBlockValues["W_LFA"]        = this.txt_W_LFA.Text;
            //    //DataBlockValues["W_NEW_LFA"]    = this.txt_W_NEW_LFA.Text;

            //    //DataBlockValues["W_MED"]        = this.txt_W_MED.Text;
            //    //DataBlockValues["W_NEW_MED"]    = this.txt_W_NEW_MED.Text;

            //    //DataBlockValues["W_DATE"]   = this.txtDate.Text;
            //    //DataBlockValues["W_ANS"]    = this.txt_W_ANS.Text;

            //    //DataBlockValues["W_VP"]     = 3600;

            //    //StartProcessing();
            //}
        }

        #endregion

        private void txt_W_ANS_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            //if ((this.txt_W_ANS.Text != "Y") && (this.txt_W_ANS.Text != "N") && (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter))
            //{
            //    MessageBox.Show("Enter [Y]es...or ..[N]o...");
            //}
            //else if ((this.txt_W_ANS.Text.ToUpper() == "N") && (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter))
            //{
            //    this.Close();
            //}
            //else 
            if ((this.txt_W_ANS.Text.ToUpper() == "Y") && (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter))
            {

                InitializeDic();

                DataBlockValues["W_FROM"] = this.slDatePicker_W_FROM.Value;
                DataBlockValues["W_TO"] = this.slDatePicker_W_TO.Value;
                DataBlockValues["W_BRN"] = this.txt_W_BRN.Text;

                DataBlockValues["W_MOLD"] = this.txt_W_MOLD.Text;
                DataBlockValues["W_COLD"] = this.txt_W_COLD.Text;

                DataBlockValues["W_CONV_AMT"] = this.txt_W_CONV_AMT.Text;
                DataBlockValues["W_MEAL_AMT"] = this.txt_W_MEAL_AMT.Text;

                DataBlockValues["W_LFA"] = this.txt_W_LFA.Text;
                DataBlockValues["W_NEW_LFA"] = this.txt_W_NEW_LFA.Text;

                DataBlockValues["W_MED"] = this.txt_W_MED.Text;
                DataBlockValues["W_NEW_MED"] = this.txt_W_NEW_MED.Text;

                DataBlockValues["W_DATE"] = this.txtDate.Text;
                DataBlockValues["W_ANS"] = this.txt_W_ANS.Text;

                DataBlockValues["W_VP"] = 3600;

                StartProcessing();
            }
        }
       
        /*
        private void button1_Click(object sender, EventArgs e)
        {
            DateTime min = System.DateTime.Now.AddMonths(-1); //31st dec 2010
            min = min.AddDays(-11);
            DateTime max = System.DateTime.Now.AddDays(14); //25th feb 2011
            int Months = MonthsBetween(max, min);
        }

        /// <summary>
        /// Calculate Months between Maximum and Minimum dates
        /// </summary>
        /// <param name="MaxDate">Higher Range Date</param>
        /// <param name="MinDate">Lower Range Date</param>
        int MonthsBetween(DateTime MaxDate,DateTime MinDate)
        {
            Int32 dResult = 0;

            //Last day of Maximum Date
            DateTime FirstDayOfMonth_MaxDate = new DateTime(MaxDate.Year, MaxDate.Month, 1);
            DateTime LastDayOfMonth_MaxDate = FirstDayOfMonth_MaxDate.AddMonths(1).AddDays(-1);

            //Last day of Minimum Date
            DateTime FirstDayOfMonth_MinDate = new DateTime(MinDate.Year, MinDate.Month, 1);
            DateTime LastDayOfMonth_MinDate = FirstDayOfMonth_MinDate.AddMonths(1).AddDays(-1);

            //Condition 1: Day of Maximum date != Day of Minimum date
            //Condition 2: "Last Day of Maximum date != Day of Maximum date" And "Last Day of Minimum date != Day of Minimum date"
            if ((MaxDate.Day != MinDate.Day) || (LastDayOfMonth_MaxDate.Day != MaxDate.Day && LastDayOfMonth_MinDate.Day != MinDate.Day))
            {               
                int A = (MaxDate.AddMonths(-MinDate.Month)).Month - 1;
                int B = 32 - MinDate.Day;
                int C = MaxDate.Day - 1;

                Double D = ((A * 31) + B + C) / 31.00;
                dResult = Convert.ToInt32(Math.Round(D, 0));
            }

            return dResult;
        }
         */
    }
}