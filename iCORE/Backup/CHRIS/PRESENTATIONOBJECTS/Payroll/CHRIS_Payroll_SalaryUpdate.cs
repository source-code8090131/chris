using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using System.Reflection;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_SalaryUpdate : MasterDetailForm
    {
        CHRIS_Payroll_PR111 ObjBasecopy = null;
        public const string Fin_regex = @"^(\-(?=\d))?\d{0,6}(\.\d{1,2})?$";
        Object Mdi;
        #region Constructors

        public CHRIS_Payroll_SalaryUpdate()
        {
            InitializeComponent();
        }

        public CHRIS_Payroll_SalaryUpdate(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Payroll_PR111 menuBaseForm)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this.MdiParent = mainmenu;
            ObjBasecopy = menuBaseForm;
            ObjBasecopy.Hide();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(slPanelPayroll_Allow);
            lstDependentPanels.Add(slPanelList_Deduction);
            lstDependentPanels.Add(slPanelSimpleList_MonthOverTime);

            this.slPanelPayroll.DependentPanels = lstDependentPanels;

            lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(slPanelPayroll);
            //lstDependentPanels.Add(slPanelSimpleList_MonthOverTime);
            slPanelPersonnel.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(slPanelPersonnel);

        }

        #endregion

        #region Methods
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {

            SLPanel oCtrlPanel = this.GetCurrentPanelBlock;
            if (actionType == "Previous")
            {
                if (oCtrlPanel is SLPanelSimpleList)
                {

                    if (this.FindForm().Validate())
                    {
                        if (oCtrlPanel.Name == slPanelPayroll_Allow.Name)
                        {
                            if ((oCtrlPanel as SLPanelSimpleList).GridSource != null && (oCtrlPanel as SLPanelSimpleList).GridSource.Rows.Count > 0)
                            {
                                if (slDatePicker_PA_DATE.Value != null)
                                    if (txt_PW_ALL_CODE.Text != string.Empty && txt_PW_ALL_AMOUNT.Text != string.Empty) // if (slTxt_PW_PAY_DATE.Text != string.Empty && slTxt_PW_PAY_DATE.Text == slDatePicker_PA_DATE.Value.ToString())
                                        (oCtrlPanel as SLPanelSimpleList).GetValuesFromPanel(oCtrlPanel.Controls);
                            }
                        }
                        else if (oCtrlPanel.Name == slPanelList_Deduction.Name)
                        {
                            if ((oCtrlPanel as SLPanelSimpleList).GridSource != null && (oCtrlPanel as SLPanelSimpleList).GridSource.Rows.Count > 0)
                            {
                                if (slDatePicker_PA_DATE.Value != null)
                                    if (txt_PD_DED_CODE.Text != string.Empty && txt_PD_DED_AMOUNT.Text != string.Empty) // if (slTxt_PW_PAY_DATE.Text != string.Empty && slTxt_PW_PAY_DATE.Text == slDatePicker_PA_DATE.Value.ToString())
                                        (oCtrlPanel as SLPanelSimpleList).GetValuesFromPanel(oCtrlPanel.Controls);
                            }
                        }
                        else
                            (oCtrlPanel as SLPanelSimpleList).GetValuesFromPanel(oCtrlPanel.Controls);
                        //slPanelPayroll.GetValuesFromPanel(slPanelPayroll.Controls);
                    }
                }
            }
            else if (actionType == "Next")
            {
                if (oCtrlPanel is SLPanelSimpleList)
                {
                    if (this.FindForm().Validate())
                    {
                        if (oCtrlPanel.Name == slPanelPayroll_Allow.Name)
                        {
                            if ((oCtrlPanel as SLPanelSimpleList).GridSource != null && (oCtrlPanel as SLPanelSimpleList).GridSource.Rows.Count > 0)
                            {
                                if (slDatePicker_PA_DATE.Value != null)
                                    if (txt_PW_ALL_CODE.Text != string.Empty && txt_PW_ALL_AMOUNT.Text != string.Empty) // if (slTxt_PW_PAY_DATE.Text != string.Empty && slTxt_PW_PAY_DATE.Text == slDatePicker_PA_DATE.Value.ToString())
                                        (oCtrlPanel as SLPanelSimpleList).GetValuesFromPanel(oCtrlPanel.Controls);
                            }
                        }
                        else if (oCtrlPanel.Name == slPanelList_Deduction.Name)
                        {
                            if ((oCtrlPanel as SLPanelSimpleList).GridSource != null && (oCtrlPanel as SLPanelSimpleList).GridSource.Rows.Count > 0)
                            {
                                if (slDatePicker_PA_DATE.Value != null)
                                    if (txt_PD_DED_CODE.Text != string.Empty && txt_PD_DED_AMOUNT.Text != string.Empty) // if (slTxt_PW_PAY_DATE.Text != string.Empty && slTxt_PW_PAY_DATE.Text == slDatePicker_PA_DATE.Value.ToString())
                                        (oCtrlPanel as SLPanelSimpleList).GetValuesFromPanel(oCtrlPanel.Controls);
                            }
                        }
                        else
                            (oCtrlPanel as SLPanelSimpleList).GetValuesFromPanel(oCtrlPanel.Controls);
                        //slPanelPayroll.GetValuesFromPanel(slPanelPayroll.Controls);
                    }

                }
            }
            base.DoToolbarActions(ctrlsCollection, actionType);

            if (actionType == "Previous" || actionType == "Next")
            {
                #region Commented
                //oCtrlPanel = this.GetCurrentPanelBlock;
                //if (oCtrlPanel is SLPanelSimpleList)
                //{
                //    if (oCtrlPanel.Name == slPanelPayroll_Allow.Name)
                //    {
                //        if (this.slPanelPayroll_Allow.GridSource != null)
                //            //if (this.slPanelPayroll_Allow.GridSource.Rows[slPanelPayroll_Allow.CurrentRowIndex].RowState == DataRowState.Added)
                //            {
                //                if (slDatePicker_PA_DATE.Value != null)
                //                    slTxt_PW_PAY_DATE.Text = slDatePicker_PA_DATE.Value.ToString();
                //            }
                //    }
                //}
                #endregion
            }

            if (actionType == "Cancel")
            {
                base.errorProvider1.Clear();
                ClearPanelsAndEntities();
                SetDTPickerValues();
                txtDate.Text = DateTime.Now.ToShortDateString();
            }
        }
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            txtDate.Text = DateTime.Now.ToShortDateString();
            //this.tbtDelete.Visible = false;
            SetDTPickerValues();
            //base.DoToolbarActions(this.Controls, "save");

        }

        private void ClearPanelsAndEntities()
        {
            this.slPanelPersonnel.ClearBusinessEntity();
            this.slPanelPersonnel.ClearDependentPanels();
            this.slPanelPayroll.ClearBusinessEntity();
            this.slPanelPayroll.ClearDependentPanels();
            this.slPanelPayroll_Allow.ClearBusinessEntity();
            this.slPanelPayroll_Allow.ClearDependentPanels();
            this.slPanelList_Deduction.ClearBusinessEntity();
            this.slPanelList_Deduction.ClearDependentPanels();
            this.slPanelSimpleList_MonthOverTime.ClearBusinessEntity();
            this.slPanelSimpleList_MonthOverTime.ClearDependentPanels();
        }

        private void SetDTPickerValues()
        {
            slTxt_PW_PAY_DATE.Text = string.Empty;
            slDatePicker_PR_CONFIRM_ON.Value = null;
            slDatePicker_PR_CONFIRM_ON.Refresh();
            slDatePicker_PR_JOINING_DATE.Value = null;
            slDatePicker_PR_JOINING_DATE.Refresh();
            slDatePicker_PA_DATE.Value = null;
            slDatePicker_PA_DATE.Refresh();
            slDatePicker_PW_PAY_DATE.Value = null;
            slDatePicker_PA_DATE.Refresh();
            slDatePicker_PD_PAY_DATE.Value = null;
            slDatePicker_PD_PAY_DATE.Refresh();
            //slDatePicker_PA_DATE.Value = null;

        }

        #endregion

        #region Events

        private void txt_PD_DED_AMOUNT_Validated(object sender, EventArgs e)
        {
            //// FieldInfo fld = typeof(SLPanelSimpleList).GetField("m_oDT", BindingFlags.Instance | BindingFlags.NonPublic);
            //// object obj=fld.GetValue(slPanelList_Deduction);
            //// DataTable dt = ((DataTable)obj);
            // CHRIS_Payroll_LoanUpdate CPRLoanUpdate = new CHRIS_Payroll_LoanUpdate(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean,ref this.slPanelList_Deduction);//this.slPanelList_Deduction

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {

            base.OnFormClosing(e);
            ObjBasecopy.Show();
            //base.DoToolbarActions(this.Controls, "Close");
        }

        protected override void MasterDetailForm_KeyUp(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.F6)
            {
                this.Close();
            }
            else if (e.KeyCode == Keys.F10)
            {
                int leftSide, rightside;
                // Setting MDIParent to null and then again to mainmenu(for setting focus on form after pressing F10)
                Mdi = (Form)(this.Parent.Parent);
                leftSide = this.Left;
                rightside = this.Top;
                base.MasterDetailForm_KeyUp(sender, e);
                this.MdiParent = null;
                this.MdiParent = (Form)(Mdi);
                //this.Right = rightside;
                this.Left = leftSide;
                this.Top = rightside;
            }
            else
            {
                base.MasterDetailForm_KeyUp(sender, e);
            }

            if (e.Control)
            {
                if (e.KeyValue == 33)
                {
                    SLPanel prevBlock = GetCurrentPanelBlock;

                    if (prevBlock != null)
                    {
                        if (prevBlock.Name == slPanelPersonnel.Name)
                            return;
                        else if (prevBlock.Name == slPanelPayroll.Name)
                        {
                            this.CurrentPanelBlock = slPanelPersonnel.Name;
                            txt_PR_P_NO.Focus();//prevBlock.Focus();
                        }
                        else if (prevBlock.Name == slPanelPayroll_Allow.Name)
                        {
                            this.CurrentPanelBlock = slPanelPayroll.Name;
                            slDatePicker_PA_DATE.Select();//prevBlock.Focus();
                        }
                        else if (prevBlock.Name == slPanelList_Deduction.Name)
                        {
                            this.CurrentPanelBlock = slPanelPayroll_Allow.Name;
                            txt_PW_ALL_CODE.Focus();//prevBlock.Focus();
                        }

                    }
                }
                else if (e.KeyValue == 34)
                {
                    SLPanel nextBlock = GetCurrentPanelBlock;

                    if (nextBlock != null)
                    {
                        if (nextBlock.Name == slPanelPersonnel.Name)
                        {
                            this.CurrentPanelBlock = slPanelPayroll.Name;
                            slDatePicker_PA_DATE.Select();
                        }
                        else if (nextBlock.Name == slPanelPayroll.Name)
                        {
                            this.CurrentPanelBlock = slPanelPayroll_Allow.Name;
                            txt_PW_ALL_CODE.Focus();
                        }
                        else if (nextBlock.Name == slPanelPayroll_Allow.Name)
                        {
                            this.CurrentPanelBlock = slPanelList_Deduction.Name;
                            txt_PD_DED_CODE.Select();//prevBlock.Focus();
                        }
                        else if (nextBlock.Name == slPanelList_Deduction.Name)
                        {
                            return;

                        }


                    }
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        //these keyup;keydown were not recieving tab key...

        private void txt_PD_DED_AMOUNT_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txt_PD_DED_AMOUNT_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            //{
            //    InitializeFormObject(slPanelSimpleList_MonthOverTime);
            //    IterateFormControls(slPanelSimpleList_MonthOverTime.Controls, false, false, false);
            //    CHRIS_Payroll_SalaryUpdate_Month_OverTime ObjOvertime = new CHRIS_Payroll_SalaryUpdate_Month_OverTime(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, ref this.slPanelSimpleList_MonthOverTime);
            //    ObjOvertime.MdiParent = null;
            //    ObjOvertime.tempPrPNum = txt_PR_P_NO.Text;
            //    ObjOvertime.slPnlCopy = this.slPanelSimpleList_MonthOverTime;
            //    ObjOvertime.ObjSalBaseForm = (CHRIS_Payroll_SalaryUpdate)this;
            //    ObjOvertime.ShowDialog();
            //}

        }

        private void txt_PD_DED_AMOUNT_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == '\t' || e.KeyChar == 13)
            {
                InitializeFormObject(slPanelSimpleList_MonthOverTime);
                IterateFormControls(slPanelSimpleList_MonthOverTime.Controls, false, false, false);
                CHRIS_Payroll_SalaryUpdate_Month_OverTime ObjOvertime = new CHRIS_Payroll_SalaryUpdate_Month_OverTime(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, ref this.slPanelSimpleList_MonthOverTime);
                ObjOvertime.MdiParent = null;
                ObjOvertime.tempPrPNum = txt_PR_P_NO.Text;
                ObjOvertime.slPnlCopy = this.slPanelSimpleList_MonthOverTime;
                ObjOvertime.ObjSalBaseForm = (CHRIS_Payroll_SalaryUpdate)this;
                ObjOvertime.ShowDialog();
                base.DoToolbarActions(this.slPanelPersonnel.Controls, "Cancel");
                ClearPanelsAndEntities();
                SetDTPickerValues();
            }//if (e.KeyChar == 13)
            //{
            // {
            //        InitializeFormObject(slPanelSimpleList_MonthOverTime);
            //        IterateFormControls(slPanelSimpleList_MonthOverTime.Controls, false, false, false);
            //        CHRIS_Payroll_SalaryUpdate_Month_OverTime ObjOvertime = new CHRIS_Payroll_SalaryUpdate_Month_OverTime(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, ref this.slPanelSimpleList_MonthOverTime);
            //        ObjOvertime.MdiParent = null;
            //        ObjOvertime.tempPrPNum = txt_PR_P_NO.Text;
            //        ObjOvertime.slPnlCopy = this.slPanelSimpleList_MonthOverTime;
            //        ObjOvertime.ObjSalBaseForm = (CHRIS_Payroll_SalaryUpdate)this;
            //        ObjOvertime.ShowDialog();
            //    }
            //}
        }

        private void txt_PD_DED_AMOUNT_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (!(e.Shift))
            {
                if (e.KeyCode == Keys.Tab)
                {
                    e.IsInputKey = true;
                }
            }
        }

        private void txt_PR_CONF_FLAG_Validating(object sender, CancelEventArgs e)
        {
            if (!txt_PR_CONF_FLAG.Text.Trim().Equals(string.Empty))
            {
                if (txt_PR_CONF_FLAG.Text.ToUpper() != "N" && txt_PR_CONF_FLAG.Text.ToUpper() != "Y")
                {
                    MessageBox.Show("ENTER  [Y] OR [N] .......", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }

        }

        private void txt_PR_MONTH_AWARD_Validating(object sender, CancelEventArgs e)
        {
            if (!txt_PR_MONTH_AWARD.Text.Trim().Equals(string.Empty))
            {
                int award = 0;
                int.TryParse(txt_PR_MONTH_AWARD.Text.Trim(), out award);
                if (award > 12)
                {
                    MessageBox.Show("Monthly Award Should Be Less Then 12 ....", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //e.Cancel = true;
                }
            }
        }

        private void txt_PA_F_INTREST_Validating(object sender, CancelEventArgs e)
        {
            if (txt_PA_F_INTREST.Text != string.Empty)
            {
                bool isValid = false;
                isValid = IsValidExpression(txt_PA_F_INTREST.Text, Fin_regex);
                if (!isValid)
                {
                    MessageBox.Show("Financial Int. Amount can be in 999999.99 format.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }

        }

        private void txt_PA_INCOME_TAX_Validated(object sender, EventArgs e)
        {
            if (txt_PA_INCOME_TAX.Text != txt_PA_L_INSTALL.Text)
            {
                txt_PA_L_INSTALL.Text = txt_PA_INCOME_TAX.Text;
            }
        }

        #endregion


    }

}