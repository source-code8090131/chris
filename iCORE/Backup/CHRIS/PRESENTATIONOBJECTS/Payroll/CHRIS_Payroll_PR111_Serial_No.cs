using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_PR111_Serial_No : SimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        Result rslt;
        CHRIS_Payroll_PR111 ObjBasecopy = null;

        #region Constructors

        public CHRIS_Payroll_PR111_Serial_No()
        {
            InitializeComponent();
        }
        public CHRIS_Payroll_PR111_Serial_No(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Payroll_PR111 menuBaseForm)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            ObjBasecopy = menuBaseForm;
            ObjBasecopy.Hide();

        }

        #endregion

        #region overriden  Methods/Events.

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tlbMain.Visible = false;
            rslt = cmnDM.GetData("CHRIS_SP_SERIAL_NO_MANAGER", "List");
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.InitializeFormObject(this.slPanelSerial);
                    base.SetPanelControlWithListItem(slPanelSerial.Controls, rslt.dstResult.Tables[0].Rows[0]);
                    slPanelSerial.SetEntityObject(rslt.dstResult.Tables[0].Rows[0]);
                }
                this.operationMode = Mode.Edit;


            }
            //this.DoToolbarActions(slPanelSerial.Controls, "List");
        }

        //protected override void OnFormKeyup(object sender, KeyEventArgs e)
        protected override void SimpleForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F10)
            {
                this.IterateFormControls(slPanelSerial.Controls, false, false, false);
                if (this.IsValidPage)
                {
                    rslt = cmnDM.Update(slPanelSerial.CurrentBusinessEntity, slPanelSerial.SPName, "Update");
                    if (rslt.isSuccessful)
                    {
                        this.SetMessage("Record ", ApplicationMessages.UPDATE_SUCCESS_MESSAGE, String.Empty, MessageType.Information);
                    }
                    else
                    {
                        this.SetMessage("Record ", ApplicationMessages.UPDATE_FAILURE_MESSAGE, String.Empty, MessageType.Error);
                    }
                }
                //base.SimpleForm_KeyUp(sender, e);
                //this.OnFormKeyup(sender, e);
                //DoToolbarActions(this.slPanelSerial.Controls, "Save");
                this.Close();
            }
            else if (e.KeyCode == Keys.F6)
            {
                this.Close();
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            ObjBasecopy.Show();
            base.OnFormClosing(e);
        }

        #endregion




    }
}