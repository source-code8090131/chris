namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_LoanUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_LoanUpdate));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.slPanel_Master = new iCORE.COMMON.SLCONTROLS.SLPanelSimpleList(this.components);
            this.lookupBtnFinNum = new CrplControlLibrary.LookupButton(this.components);
            this.slDatePicker_FN_EXTRA_TIME = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_FN_MONTHLY_DED = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_FN_LIQUIDATE = new CrplControlLibrary.SLTextBox(this.components);
            this.slDatePicker_FN_START_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_FN_TYPE = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_FN_FINANCE_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_FN_AMT_AVAILED = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_FN_C_RATIO_PER = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_FN_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_FN_PAY_SCHED = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.slDatePicker_FN_END_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_FN_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.slPanelDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.col_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_FN_MDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_FN_DEBIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_FN_CREDIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_FN_PAY_LEFT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_FN_BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_FN_LOAN_BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_FN_MARKUP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_FN_LIQ_FLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_FN_M_BRANCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblUserName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanel_Master.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.slPanelDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // slPanel_Master
            // 
            this.slPanel_Master.ConcurrentPanels = null;
            this.slPanel_Master.Controls.Add(this.lookupBtnFinNum);
            this.slPanel_Master.Controls.Add(this.slDatePicker_FN_EXTRA_TIME);
            this.slPanel_Master.Controls.Add(this.txt_FN_MONTHLY_DED);
            this.slPanel_Master.Controls.Add(this.txt_FN_LIQUIDATE);
            this.slPanel_Master.Controls.Add(this.slDatePicker_FN_START_DATE);
            this.slPanel_Master.Controls.Add(this.label13);
            this.slPanel_Master.Controls.Add(this.label12);
            this.slPanel_Master.Controls.Add(this.label11);
            this.slPanel_Master.Controls.Add(this.label10);
            this.slPanel_Master.Controls.Add(this.label9);
            this.slPanel_Master.Controls.Add(this.txt_FN_TYPE);
            this.slPanel_Master.Controls.Add(this.txt_FN_FINANCE_NO);
            this.slPanel_Master.Controls.Add(this.txt_FN_AMT_AVAILED);
            this.slPanel_Master.Controls.Add(this.txt_FN_C_RATIO_PER);
            this.slPanel_Master.Controls.Add(this.txt_FN_BRANCH);
            this.slPanel_Master.Controls.Add(this.txt_FN_PAY_SCHED);
            this.slPanel_Master.Controls.Add(this.label8);
            this.slPanel_Master.Controls.Add(this.label7);
            this.slPanel_Master.Controls.Add(this.label6);
            this.slPanel_Master.Controls.Add(this.label5);
            this.slPanel_Master.Controls.Add(this.label4);
            this.slPanel_Master.Controls.Add(this.label3);
            this.slPanel_Master.Controls.Add(this.slDatePicker_FN_END_DATE);
            this.slPanel_Master.Controls.Add(this.txt_FN_P_NO);
            this.slPanel_Master.Controls.Add(this.label2);
            this.slPanel_Master.CurrentRowIndex = 0;
            this.slPanel_Master.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanel_Master.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanel_Master.DependentPanels = null;
            this.slPanel_Master.DisableDependentLoad = false;
            this.slPanel_Master.EnableDelete = false;
            this.slPanel_Master.EnableInsert = false;
            this.slPanel_Master.EnableQuery = false;
            this.slPanel_Master.EnableUpdate = true;
            this.slPanel_Master.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand_PR111";
            this.slPanel_Master.Location = new System.Drawing.Point(10, 16);
            this.slPanel_Master.MasterPanel = null;
            this.slPanel_Master.Name = "slPanel_Master";
            this.slPanel_Master.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanel_Master.Size = new System.Drawing.Size(617, 146);
            this.slPanel_Master.SPName = "CHRIS_SP_FIN_BOOKING_MPC_MANAGER_PR111";
            this.slPanel_Master.TabIndex = 1;
            // 
            // lookupBtnFinNum
            // 
            this.lookupBtnFinNum.ActionLOVExists = "FinNumLov_Exist";
            this.lookupBtnFinNum.ActionType = "FinNumLov";
            this.lookupBtnFinNum.ConditionalFields = "";
            this.lookupBtnFinNum.CustomEnabled = true;
            this.lookupBtnFinNum.DataFieldMapping = "";
            this.lookupBtnFinNum.DependentLovControls = "";
            this.lookupBtnFinNum.HiddenColumns = resources.GetString("lookupBtnFinNum.HiddenColumns");
            this.lookupBtnFinNum.Image = ((System.Drawing.Image)(resources.GetObject("lookupBtnFinNum.Image")));
            this.lookupBtnFinNum.LoadDependentEntities = true;
            this.lookupBtnFinNum.Location = new System.Drawing.Point(238, 2);
            this.lookupBtnFinNum.LookUpTitle = null;
            this.lookupBtnFinNum.Name = "lookupBtnFinNum";
            this.lookupBtnFinNum.Size = new System.Drawing.Size(26, 21);
            this.lookupBtnFinNum.SkipValidationOnLeave = false;
            this.lookupBtnFinNum.SPName = "CHRIS_SP_FIN_BOOKING_MPC_MANAGER_PR111";
            this.lookupBtnFinNum.TabIndex = 20;
            this.lookupBtnFinNum.TabStop = false;
            this.lookupBtnFinNum.UseVisualStyleBackColor = true;
            this.lookupBtnFinNum.Visible = false;
            // 
            // slDatePicker_FN_EXTRA_TIME
            // 
            this.slDatePicker_FN_EXTRA_TIME.CustomEnabled = true;
            this.slDatePicker_FN_EXTRA_TIME.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_FN_EXTRA_TIME.DataFieldMapping = "FN_EXTRA_TIME";
            this.slDatePicker_FN_EXTRA_TIME.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_FN_EXTRA_TIME.HasChanges = true;
            this.slDatePicker_FN_EXTRA_TIME.Location = new System.Drawing.Point(445, 71);
            this.slDatePicker_FN_EXTRA_TIME.Name = "slDatePicker_FN_EXTRA_TIME";
            this.slDatePicker_FN_EXTRA_TIME.NullValue = " ";
            this.slDatePicker_FN_EXTRA_TIME.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_FN_EXTRA_TIME.TabIndex = 8;
            this.slDatePicker_FN_EXTRA_TIME.Value = new System.DateTime(2011, 2, 1, 0, 0, 0, 0);
            // 
            // txt_FN_MONTHLY_DED
            // 
            this.txt_FN_MONTHLY_DED.AllowSpace = true;
            this.txt_FN_MONTHLY_DED.AssociatedLookUpName = "";
            this.txt_FN_MONTHLY_DED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FN_MONTHLY_DED.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FN_MONTHLY_DED.ContinuationTextBox = null;
            this.txt_FN_MONTHLY_DED.CustomEnabled = true;
            this.txt_FN_MONTHLY_DED.DataFieldMapping = "FN_MONTHLY_DED";
            this.txt_FN_MONTHLY_DED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FN_MONTHLY_DED.GetRecordsOnUpDownKeys = false;
            this.txt_FN_MONTHLY_DED.IsDate = false;
            this.txt_FN_MONTHLY_DED.Location = new System.Drawing.Point(445, 92);
            this.txt_FN_MONTHLY_DED.MaxLength = 10;
            this.txt_FN_MONTHLY_DED.Name = "txt_FN_MONTHLY_DED";
            this.txt_FN_MONTHLY_DED.NumberFormat = "###,###,##0.00";
            this.txt_FN_MONTHLY_DED.Postfix = "";
            this.txt_FN_MONTHLY_DED.Prefix = "";
            this.txt_FN_MONTHLY_DED.Size = new System.Drawing.Size(100, 20);
            this.txt_FN_MONTHLY_DED.SkipValidation = false;
            this.txt_FN_MONTHLY_DED.TabIndex = 10;
            this.txt_FN_MONTHLY_DED.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_FN_MONTHLY_DED.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_FN_LIQUIDATE
            // 
            this.txt_FN_LIQUIDATE.AllowSpace = true;
            this.txt_FN_LIQUIDATE.AssociatedLookUpName = "";
            this.txt_FN_LIQUIDATE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FN_LIQUIDATE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FN_LIQUIDATE.ContinuationTextBox = null;
            this.txt_FN_LIQUIDATE.CustomEnabled = true;
            this.txt_FN_LIQUIDATE.DataFieldMapping = "FN_LIQUIDATE";
            this.txt_FN_LIQUIDATE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FN_LIQUIDATE.GetRecordsOnUpDownKeys = false;
            this.txt_FN_LIQUIDATE.IsDate = false;
            this.txt_FN_LIQUIDATE.Location = new System.Drawing.Point(445, 113);
            this.txt_FN_LIQUIDATE.MaxLength = 1;
            this.txt_FN_LIQUIDATE.Name = "txt_FN_LIQUIDATE";
            this.txt_FN_LIQUIDATE.NumberFormat = "###,###,##0.00";
            this.txt_FN_LIQUIDATE.Postfix = "";
            this.txt_FN_LIQUIDATE.Prefix = "";
            this.txt_FN_LIQUIDATE.Size = new System.Drawing.Size(30, 20);
            this.txt_FN_LIQUIDATE.SkipValidation = false;
            this.txt_FN_LIQUIDATE.TabIndex = 11;
            this.txt_FN_LIQUIDATE.TextType = CrplControlLibrary.TextType.String;
            // 
            // slDatePicker_FN_START_DATE
            // 
            this.slDatePicker_FN_START_DATE.CustomEnabled = true;
            this.slDatePicker_FN_START_DATE.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_FN_START_DATE.DataFieldMapping = "FN_START_DATE";
            this.slDatePicker_FN_START_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_FN_START_DATE.HasChanges = true;
            this.slDatePicker_FN_START_DATE.Location = new System.Drawing.Point(445, 49);
            this.slDatePicker_FN_START_DATE.Name = "slDatePicker_FN_START_DATE";
            this.slDatePicker_FN_START_DATE.NullValue = " ";
            this.slDatePicker_FN_START_DATE.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_FN_START_DATE.TabIndex = 6;
            this.slDatePicker_FN_START_DATE.TabStop = false;
            this.slDatePicker_FN_START_DATE.Value = new System.DateTime(2011, 2, 1, 0, 0, 0, 0);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(299, 99);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Monthly Deduction :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(299, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Liquidation Flag     :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(299, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Extra Time            :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(299, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Start Date             :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(299, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Payment Schedule :";
            // 
            // txt_FN_TYPE
            // 
            this.txt_FN_TYPE.AllowSpace = true;
            this.txt_FN_TYPE.AssociatedLookUpName = "";
            this.txt_FN_TYPE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FN_TYPE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FN_TYPE.ContinuationTextBox = null;
            this.txt_FN_TYPE.CustomEnabled = true;
            this.txt_FN_TYPE.DataFieldMapping = "FN_TYPE";
            this.txt_FN_TYPE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FN_TYPE.GetRecordsOnUpDownKeys = false;
            this.txt_FN_TYPE.IsDate = false;
            this.txt_FN_TYPE.Location = new System.Drawing.Point(131, 25);
            this.txt_FN_TYPE.MaxLength = 6;
            this.txt_FN_TYPE.Name = "txt_FN_TYPE";
            this.txt_FN_TYPE.NumberFormat = "###,###,##0.00";
            this.txt_FN_TYPE.Postfix = "";
            this.txt_FN_TYPE.Prefix = "";
            this.txt_FN_TYPE.ReadOnly = true;
            this.txt_FN_TYPE.Size = new System.Drawing.Size(85, 20);
            this.txt_FN_TYPE.SkipValidation = false;
            this.txt_FN_TYPE.TabIndex = 3;
            this.txt_FN_TYPE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_FN_FINANCE_NO
            // 
            this.txt_FN_FINANCE_NO.AllowSpace = true;
            this.txt_FN_FINANCE_NO.AssociatedLookUpName = "";
            this.txt_FN_FINANCE_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FN_FINANCE_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FN_FINANCE_NO.ContinuationTextBox = null;
            this.txt_FN_FINANCE_NO.CustomEnabled = true;
            this.txt_FN_FINANCE_NO.DataFieldMapping = "FN_FINANCE_NO";
            this.txt_FN_FINANCE_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FN_FINANCE_NO.GetRecordsOnUpDownKeys = false;
            this.txt_FN_FINANCE_NO.IsDate = false;
            this.txt_FN_FINANCE_NO.Location = new System.Drawing.Point(131, 46);
            this.txt_FN_FINANCE_NO.MaxLength = 10;
            this.txt_FN_FINANCE_NO.Name = "txt_FN_FINANCE_NO";
            this.txt_FN_FINANCE_NO.NumberFormat = "###,###,##0.00";
            this.txt_FN_FINANCE_NO.Postfix = "";
            this.txt_FN_FINANCE_NO.Prefix = "";
            this.txt_FN_FINANCE_NO.ReadOnly = true;
            this.txt_FN_FINANCE_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_FN_FINANCE_NO.SkipValidation = false;
            this.txt_FN_FINANCE_NO.TabIndex = 5;
            this.txt_FN_FINANCE_NO.TextType = CrplControlLibrary.TextType.String;
            this.txt_FN_FINANCE_NO.Validated += new System.EventHandler(this.txt_FN_FINANCE_NO_Validated);
            // 
            // txt_FN_AMT_AVAILED
            // 
            this.txt_FN_AMT_AVAILED.AllowSpace = true;
            this.txt_FN_AMT_AVAILED.AssociatedLookUpName = "";
            this.txt_FN_AMT_AVAILED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FN_AMT_AVAILED.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FN_AMT_AVAILED.ContinuationTextBox = null;
            this.txt_FN_AMT_AVAILED.CustomEnabled = true;
            this.txt_FN_AMT_AVAILED.DataFieldMapping = "FN_AMT_AVAILED";
            this.txt_FN_AMT_AVAILED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FN_AMT_AVAILED.GetRecordsOnUpDownKeys = false;
            this.txt_FN_AMT_AVAILED.IsDate = false;
            this.txt_FN_AMT_AVAILED.Location = new System.Drawing.Point(131, 90);
            this.txt_FN_AMT_AVAILED.MaxLength = 10;
            this.txt_FN_AMT_AVAILED.Name = "txt_FN_AMT_AVAILED";
            this.txt_FN_AMT_AVAILED.NumberFormat = "###,###,##0.00";
            this.txt_FN_AMT_AVAILED.Postfix = "";
            this.txt_FN_AMT_AVAILED.Prefix = "";
            this.txt_FN_AMT_AVAILED.Size = new System.Drawing.Size(100, 20);
            this.txt_FN_AMT_AVAILED.SkipValidation = false;
            this.txt_FN_AMT_AVAILED.TabIndex = 9;
            this.txt_FN_AMT_AVAILED.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_FN_AMT_AVAILED.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_FN_C_RATIO_PER
            // 
            this.txt_FN_C_RATIO_PER.AllowSpace = true;
            this.txt_FN_C_RATIO_PER.AssociatedLookUpName = "";
            this.txt_FN_C_RATIO_PER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FN_C_RATIO_PER.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FN_C_RATIO_PER.ContinuationTextBox = null;
            this.txt_FN_C_RATIO_PER.CustomEnabled = true;
            this.txt_FN_C_RATIO_PER.DataFieldMapping = "FN_C_RATIO_PER";
            this.txt_FN_C_RATIO_PER.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FN_C_RATIO_PER.GetRecordsOnUpDownKeys = false;
            this.txt_FN_C_RATIO_PER.IsDate = false;
            this.txt_FN_C_RATIO_PER.Location = new System.Drawing.Point(131, 111);
            this.txt_FN_C_RATIO_PER.MaxLength = 3;
            this.txt_FN_C_RATIO_PER.Name = "txt_FN_C_RATIO_PER";
            this.txt_FN_C_RATIO_PER.NumberFormat = "###,###,##0.0000";
            this.txt_FN_C_RATIO_PER.Postfix = "";
            this.txt_FN_C_RATIO_PER.Prefix = "";
            this.txt_FN_C_RATIO_PER.Size = new System.Drawing.Size(85, 20);
            this.txt_FN_C_RATIO_PER.SkipValidation = false;
            this.txt_FN_C_RATIO_PER.TabIndex = 10;
            this.txt_FN_C_RATIO_PER.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_FN_C_RATIO_PER.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_FN_BRANCH
            // 
            this.txt_FN_BRANCH.AllowSpace = true;
            this.txt_FN_BRANCH.AssociatedLookUpName = "";
            this.txt_FN_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FN_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FN_BRANCH.ContinuationTextBox = null;
            this.txt_FN_BRANCH.CustomEnabled = true;
            this.txt_FN_BRANCH.DataFieldMapping = "FN_BRANCH";
            this.txt_FN_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FN_BRANCH.GetRecordsOnUpDownKeys = false;
            this.txt_FN_BRANCH.IsDate = false;
            this.txt_FN_BRANCH.Location = new System.Drawing.Point(445, 4);
            this.txt_FN_BRANCH.MaxLength = 3;
            this.txt_FN_BRANCH.Name = "txt_FN_BRANCH";
            this.txt_FN_BRANCH.NumberFormat = "###,###,##0.00";
            this.txt_FN_BRANCH.Postfix = "";
            this.txt_FN_BRANCH.Prefix = "";
            this.txt_FN_BRANCH.Size = new System.Drawing.Size(52, 20);
            this.txt_FN_BRANCH.SkipValidation = false;
            this.txt_FN_BRANCH.TabIndex = 2;
            this.txt_FN_BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_FN_PAY_SCHED
            // 
            this.txt_FN_PAY_SCHED.AllowSpace = true;
            this.txt_FN_PAY_SCHED.AssociatedLookUpName = "";
            this.txt_FN_PAY_SCHED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FN_PAY_SCHED.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FN_PAY_SCHED.ContinuationTextBox = null;
            this.txt_FN_PAY_SCHED.CustomEnabled = true;
            this.txt_FN_PAY_SCHED.DataFieldMapping = "FN_PAY_SCHED";
            this.txt_FN_PAY_SCHED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FN_PAY_SCHED.GetRecordsOnUpDownKeys = false;
            this.txt_FN_PAY_SCHED.IsDate = false;
            this.txt_FN_PAY_SCHED.Location = new System.Drawing.Point(445, 25);
            this.txt_FN_PAY_SCHED.MaxLength = 5;
            this.txt_FN_PAY_SCHED.Name = "txt_FN_PAY_SCHED";
            this.txt_FN_PAY_SCHED.NumberFormat = "###,###,##0.00";
            this.txt_FN_PAY_SCHED.Postfix = "";
            this.txt_FN_PAY_SCHED.Prefix = "";
            this.txt_FN_PAY_SCHED.Size = new System.Drawing.Size(62, 20);
            this.txt_FN_PAY_SCHED.SkipValidation = false;
            this.txt_FN_PAY_SCHED.TabIndex = 4;
            this.txt_FN_PAY_SCHED.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_FN_PAY_SCHED.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Loan Type     :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Finance No    :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Availed Amt.   :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Credit Ratio    :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "End Date       :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(299, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Branch                 :";
            // 
            // slDatePicker_FN_END_DATE
            // 
            this.slDatePicker_FN_END_DATE.CustomEnabled = true;
            this.slDatePicker_FN_END_DATE.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_FN_END_DATE.DataFieldMapping = "FN_END_DATE";
            this.slDatePicker_FN_END_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_FN_END_DATE.HasChanges = true;
            this.slDatePicker_FN_END_DATE.Location = new System.Drawing.Point(131, 69);
            this.slDatePicker_FN_END_DATE.Name = "slDatePicker_FN_END_DATE";
            this.slDatePicker_FN_END_DATE.NullValue = " ";
            this.slDatePicker_FN_END_DATE.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_FN_END_DATE.TabIndex = 7;
            this.slDatePicker_FN_END_DATE.Value = new System.DateTime(2011, 2, 1, 0, 0, 0, 0);
            // 
            // txt_FN_P_NO
            // 
            this.txt_FN_P_NO.AllowSpace = true;
            this.txt_FN_P_NO.AssociatedLookUpName = "";
            this.txt_FN_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FN_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FN_P_NO.ContinuationTextBox = null;
            this.txt_FN_P_NO.CustomEnabled = true;
            this.txt_FN_P_NO.DataFieldMapping = "PR_P_NO";
            this.txt_FN_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FN_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_FN_P_NO.IsDate = false;
            this.txt_FN_P_NO.Location = new System.Drawing.Point(131, 4);
            this.txt_FN_P_NO.MaxLength = 6;
            this.txt_FN_P_NO.Name = "txt_FN_P_NO";
            this.txt_FN_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_FN_P_NO.Postfix = "";
            this.txt_FN_P_NO.Prefix = "";
            this.txt_FN_P_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_FN_P_NO.SkipValidation = false;
            this.txt_FN_P_NO.TabIndex = 1;
            this.txt_FN_P_NO.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_FN_P_NO.Validated += new System.EventHandler(this.txt_FN_P_NO_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Personnel No :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.slPanel_Master);
            this.groupBox1.Location = new System.Drawing.Point(16, 112);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(636, 171);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(16, 80);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(636, 33);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(229, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Loan Booking Information";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.slPanelDetail);
            this.groupBox3.Location = new System.Drawing.Point(2, 289);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(667, 307);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Monthly Installment Information";
            // 
            // slPanelDetail
            // 
            this.slPanelDetail.ConcurrentPanels = null;
            this.slPanelDetail.Controls.Add(this.dgvDetail);
            this.slPanelDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelDetail.DependentPanels = null;
            this.slPanelDetail.DisableDependentLoad = false;
            this.slPanelDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.slPanelDetail.EnableDelete = true;
            this.slPanelDetail.EnableInsert = true;
            this.slPanelDetail.EnableQuery = false;
            this.slPanelDetail.EnableUpdate = true;
            this.slPanelDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FNMonthCommand_PR111";
            this.slPanelDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slPanelDetail.Location = new System.Drawing.Point(3, 16);
            this.slPanelDetail.MasterPanel = null;
            this.slPanelDetail.Name = "slPanelDetail";
            this.slPanelDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelDetail.Size = new System.Drawing.Size(661, 288);
            this.slPanelDetail.SPName = "CHRIS_SP_FN_MONTH_MANAGER_PR111";
            this.slPanelDetail.TabIndex = 2;
            // 
            // dgvDetail
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_ID,
            this.col_FN_MDATE,
            this.col_FN_DEBIT,
            this.col_FN_CREDIT,
            this.col_FN_PAY_LEFT,
            this.col_FN_BALANCE,
            this.col_FN_LOAN_BALANCE,
            this.col_FN_MARKUP,
            this.col_FN_LIQ_FLAG,
            this.col_FN_M_BRANCH});
            this.dgvDetail.ColumnToHide = null;
            this.dgvDetail.ColumnWidth = null;
            this.dgvDetail.CustomEnabled = true;
            this.dgvDetail.DisplayColumnWrapper = null;
            this.dgvDetail.GridDefaultRow = 0;
            this.dgvDetail.Location = new System.Drawing.Point(12, 12);
            this.dgvDetail.Name = "dgvDetail";
            this.dgvDetail.ReadOnlyColumns = null;
            this.dgvDetail.RequiredColumns = "COL_FN_M_BRANCH|BRANCH";
            this.dgvDetail.Size = new System.Drawing.Size(646, 273);
            this.dgvDetail.SkippingColumns = null;
            this.dgvDetail.TabIndex = 0;
            this.dgvDetail.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvDetail_CellValidating);
            this.dgvDetail.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetail_CellEnter);
            // 
            // col_ID
            // 
            this.col_ID.DataPropertyName = "ID";
            this.col_ID.HeaderText = "colID";
            this.col_ID.Name = "col_ID";
            this.col_ID.Visible = false;
            // 
            // col_FN_MDATE
            // 
            this.col_FN_MDATE.DataPropertyName = "FN_MDATE";
            dataGridViewCellStyle2.NullValue = null;
            this.col_FN_MDATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_FN_MDATE.HeaderText = "Date";
            this.col_FN_MDATE.Name = "col_FN_MDATE";
            // 
            // col_FN_DEBIT
            // 
            this.col_FN_DEBIT.DataPropertyName = "FN_DEBIT";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.col_FN_DEBIT.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_FN_DEBIT.HeaderText = "Debit";
            this.col_FN_DEBIT.MaxInputLength = 10;
            this.col_FN_DEBIT.Name = "col_FN_DEBIT";
            this.col_FN_DEBIT.Width = 70;
            // 
            // col_FN_CREDIT
            // 
            this.col_FN_CREDIT.DataPropertyName = "FN_CREDIT";
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.col_FN_CREDIT.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_FN_CREDIT.HeaderText = "Credit";
            this.col_FN_CREDIT.MaxInputLength = 10;
            this.col_FN_CREDIT.Name = "col_FN_CREDIT";
            this.col_FN_CREDIT.Width = 70;
            // 
            // col_FN_PAY_LEFT
            // 
            this.col_FN_PAY_LEFT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_FN_PAY_LEFT.DataPropertyName = "FN_PAY_LEFT";
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.col_FN_PAY_LEFT.DefaultCellStyle = dataGridViewCellStyle5;
            this.col_FN_PAY_LEFT.HeaderText = "Pay Left";
            this.col_FN_PAY_LEFT.MaxInputLength = 5;
            this.col_FN_PAY_LEFT.Name = "col_FN_PAY_LEFT";
            // 
            // col_FN_BALANCE
            // 
            this.col_FN_BALANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_FN_BALANCE.DataPropertyName = "FN_BALANCE";
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.col_FN_BALANCE.DefaultCellStyle = dataGridViewCellStyle6;
            this.col_FN_BALANCE.HeaderText = "O/S Balance";
            this.col_FN_BALANCE.MaxInputLength = 10;
            this.col_FN_BALANCE.Name = "col_FN_BALANCE";
            // 
            // col_FN_LOAN_BALANCE
            // 
            this.col_FN_LOAN_BALANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_FN_LOAN_BALANCE.DataPropertyName = "FN_LOAN_BALANCE";
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            this.col_FN_LOAN_BALANCE.DefaultCellStyle = dataGridViewCellStyle7;
            this.col_FN_LOAN_BALANCE.HeaderText = "Loan Balance";
            this.col_FN_LOAN_BALANCE.MaxInputLength = 10;
            this.col_FN_LOAN_BALANCE.Name = "col_FN_LOAN_BALANCE";
            // 
            // col_FN_MARKUP
            // 
            this.col_FN_MARKUP.DataPropertyName = "FN_MARKUP";
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = null;
            this.col_FN_MARKUP.DefaultCellStyle = dataGridViewCellStyle8;
            this.col_FN_MARKUP.HeaderText = "Markup";
            this.col_FN_MARKUP.MaxInputLength = 10;
            this.col_FN_MARKUP.Name = "col_FN_MARKUP";
            this.col_FN_MARKUP.Width = 70;
            // 
            // col_FN_LIQ_FLAG
            // 
            this.col_FN_LIQ_FLAG.DataPropertyName = "FN_LIQ_FLAG";
            this.col_FN_LIQ_FLAG.HeaderText = "Flag";
            this.col_FN_LIQ_FLAG.MaxInputLength = 1;
            this.col_FN_LIQ_FLAG.Name = "col_FN_LIQ_FLAG";
            this.col_FN_LIQ_FLAG.Width = 40;
            // 
            // col_FN_M_BRANCH
            // 
            this.col_FN_M_BRANCH.DataPropertyName = "FN_M_BRANCH";
            this.col_FN_M_BRANCH.HeaderText = "Branch";
            this.col_FN_M_BRANCH.MaxInputLength = 3;
            this.col_FN_M_BRANCH.Name = "col_FN_M_BRANCH";
            this.col_FN_M_BRANCH.Width = 50;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(376, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 57;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Payroll_LoanUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 618);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.CurrentPanelBlock = "slPanel_Master";
            this.Name = "CHRIS_Payroll_LoanUpdate";
            this.Text = "CHRIS_Payroll_LoanUpdate";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanel_Master.ResumeLayout(false);
            this.slPanel_Master.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.slPanelDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimpleList slPanel_Master;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLDatePicker slDatePicker_FN_END_DATE;
        private CrplControlLibrary.SLTextBox txt_FN_P_NO;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txt_FN_MONTHLY_DED;
        private CrplControlLibrary.SLTextBox txt_FN_LIQUIDATE;
        private CrplControlLibrary.SLDatePicker slDatePicker_FN_START_DATE;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txt_FN_TYPE;
        private CrplControlLibrary.SLTextBox txt_FN_FINANCE_NO;
        private CrplControlLibrary.SLTextBox txt_FN_AMT_AVAILED;
        private CrplControlLibrary.SLTextBox txt_FN_C_RATIO_PER;
        private CrplControlLibrary.SLTextBox txt_FN_BRANCH;
        private CrplControlLibrary.SLTextBox txt_FN_PAY_SCHED;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelDetail;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDetail;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLDatePicker slDatePicker_FN_EXTRA_TIME;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.LookupButton lookupBtnFinNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_FN_MDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_FN_DEBIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_FN_CREDIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_FN_PAY_LEFT;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_FN_BALANCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_FN_LOAN_BALANCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_FN_MARKUP;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_FN_LIQ_FLAG;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_FN_M_BRANCH;
    }
}