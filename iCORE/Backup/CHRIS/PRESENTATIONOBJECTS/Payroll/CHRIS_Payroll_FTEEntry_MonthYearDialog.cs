using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_FTEEntry_MonthYearDialog : Form
    {

        #region Properties

        bool isCompleted = false;
        public bool IsCompleted
        {
            get { return this.isCompleted; }
            set { this.isCompleted = value; }
        }

        string m_W_MY;
        public string W_MY
        {
            get { return m_W_MY; }
            set { m_W_MY = value; }
        }

        #endregion

        #region Constructor
        public CHRIS_Payroll_FTEEntry_MonthYearDialog()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void txtMonthYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((this.txtMonthYear.Text.Length == 6) && (e.KeyChar == '\r'))
            {
                string month = txtMonthYear.Text.Substring(4, 2);
                if (int.Parse(month) > 12 || int.Parse(month) < 1)
                {
                    MessageBox.Show("ENTER A VALID MONTH / YEAR  OR  [F6] TO EXIT");
                    this.txtMonthYear.Focus();
                }
                else
                {
                    W_MY = this.txtMonthYear.Text;
                    isCompleted = true;
                    this.Close();
                }
            }
            else if ((this.txtMonthYear.Text.Length < 6) && (e.KeyChar == '\r'))
            {
                MessageBox.Show("ENTER A VALID MONTH / YEAR  OR  [F6] TO EXIT");
                this.txtMonthYear.Focus();
            }
        }


        private void CHRIS_Payroll_FTEEntry_MonthYearDialog_KeyDown(object sender, KeyEventArgs e)
        {

        }


        private void txtMonthYear_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F6)
            {
                if (txtMonthYear.Text.Trim() != string.Empty)
                {
                    if (DialogResult.Yes == MessageBox.Show("Close this form?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {

                        W_MY = string.Empty;
                        isCompleted = true;
                        this.Close();
                    }
                    else
                    {
                        this.txtMonthYear.Focus();
                    }

                }
                else
                {
                    W_MY = string.Empty;
                    isCompleted = true;
                    this.Close();

                }
            }

        }
        #endregion
    }
}