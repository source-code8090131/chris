namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_SalaryUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_SalaryUpdate));
            this.slPanelPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lookup_PR_CATEGORY = new CrplControlLibrary.LookupButton(this.components);
            this.lookup_Pr_P_No = new CrplControlLibrary.LookupButton(this.components);
            this.slDatePicker_PR_CONFIRM_ON = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_ANNUAL_PACK = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_CONF_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_MONTH_AWARD = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_FIRST_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_NEW_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LEVEL = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_CATEGORY = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.slDatePicker_PR_JOINING_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.grpPersonnel = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.slPanelPayroll = new iCORE.COMMON.SLCONTROLS.SLPanelSimpleList(this.components);
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.slDatePicker_PA_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PA_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_F_INTREST = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_F_INSTALL = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_PFUND = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_INS_PREMIUM = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_WORKED_DAYS = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_HOUSE = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_L_INTREST = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_L_INSTALL = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_CONV_OFF = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_INCOME_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_SAL_ADVANCE = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PA_BASIC = new CrplControlLibrary.SLTextBox(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.slPanelPayroll_Allow = new iCORE.COMMON.SLCONTROLS.SLPanelSimpleList(this.components);
            this.slTxt_PW_PAY_DATE = new CrplControlLibrary.SLTextBox(this.components);
            this.lookup_PW_ALL_CODE = new CrplControlLibrary.LookupButton(this.components);
            this.txt_PW_ALL_AMOUNT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PW_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label31 = new System.Windows.Forms.Label();
            this.txt_PW_ALL_CODE = new CrplControlLibrary.SLTextBox(this.components);
            this.slDatePicker_PW_PAY_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.slPanelList_Deduction = new iCORE.COMMON.SLCONTROLS.SLPanelSimpleList(this.components);
            this.lookupButton_PD_DED_CODE_LOV3 = new CrplControlLibrary.LookupButton(this.components);
            this.txt_PD_DED_AMOUNT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PD_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label29 = new System.Windows.Forms.Label();
            this.txt_PD_DED_CODE = new CrplControlLibrary.SLTextBox(this.components);
            this.slDatePicker_PD_PAY_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label30 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.grp_MonthOvertime = new System.Windows.Forms.GroupBox();
            this.slPanelSimpleList_MonthOverTime = new iCORE.COMMON.SLCONTROLS.SLPanelSimpleList(this.components);
            this.txt_tempPr_p_no = new CrplControlLibrary.SLTextBox(this.components);
            this.lookup_MO_DEPT_LOV5 = new CrplControlLibrary.LookupButton(this.components);
            this.lookup_MO_SEGMENT_LOV4 = new CrplControlLibrary.LookupButton(this.components);
            this.txt_MO_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_MO_CONV_COST = new CrplControlLibrary.SLTextBox(this.components);
            this.TXT_MO_DOUBLE_COST = new CrplControlLibrary.SLTextBox(this.components);
            this.TXT_MO_DOUBLE_OT = new CrplControlLibrary.SLTextBox(this.components);
            this.TXT_MO_DEPT = new CrplControlLibrary.SLTextBox(this.components);
            this.TXT_MO_MONTH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_MO_MEAL_COST = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_MO_SINGLE_COST = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_MO_SINGLE_OT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_MO_SEGMENT = new CrplControlLibrary.SLTextBox(this.components);
            this.labely5 = new System.Windows.Forms.Label();
            this.labely7 = new System.Windows.Forms.Label();
            this.labely8 = new System.Windows.Forms.Label();
            this.labely9 = new System.Windows.Forms.Label();
            this.labely10 = new System.Windows.Forms.Label();
            this.labely6 = new System.Windows.Forms.Label();
            this.labely4 = new System.Windows.Forms.Label();
            this.labely3 = new System.Windows.Forms.Label();
            this.labely2 = new System.Windows.Forms.Label();
            this.labely1 = new System.Windows.Forms.Label();
            this.txt_MO_YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.slTxt_PD_PAY_DATE = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelPersonnel.SuspendLayout();
            this.grpPersonnel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.slPanelPayroll.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.slPanelPayroll_Allow.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.slPanelList_Deduction.SuspendLayout();
            this.grp_MonthOvertime.SuspendLayout();
            this.slPanelSimpleList_MonthOverTime.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPanelPersonnel
            // 
            this.slPanelPersonnel.ConcurrentPanels = null;
            this.slPanelPersonnel.Controls.Add(this.lookup_PR_CATEGORY);
            this.slPanelPersonnel.Controls.Add(this.lookup_Pr_P_No);
            this.slPanelPersonnel.Controls.Add(this.slDatePicker_PR_CONFIRM_ON);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_ANNUAL_PACK);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_CONF_FLAG);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_MONTH_AWARD);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_FIRST_NAME);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_NEW_BRANCH);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_LEVEL);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_DESIG);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_CATEGORY);
            this.slPanelPersonnel.Controls.Add(this.label11);
            this.slPanelPersonnel.Controls.Add(this.label10);
            this.slPanelPersonnel.Controls.Add(this.label9);
            this.slPanelPersonnel.Controls.Add(this.label8);
            this.slPanelPersonnel.Controls.Add(this.label7);
            this.slPanelPersonnel.Controls.Add(this.label6);
            this.slPanelPersonnel.Controls.Add(this.label5);
            this.slPanelPersonnel.Controls.Add(this.label4);
            this.slPanelPersonnel.Controls.Add(this.label2);
            this.slPanelPersonnel.Controls.Add(this.label3);
            this.slPanelPersonnel.Controls.Add(this.label1);
            this.slPanelPersonnel.Controls.Add(this.slDatePicker_PR_JOINING_DATE);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_P_NO);
            this.slPanelPersonnel.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelPersonnel.DependentPanels = null;
            this.slPanelPersonnel.DisableDependentLoad = false;
            this.slPanelPersonnel.EnableDelete = false;
            this.slPanelPersonnel.EnableInsert = false;
            this.slPanelPersonnel.EnableQuery = false;
            this.slPanelPersonnel.EnableUpdate = true;
            this.slPanelPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.slPanelPersonnel.Location = new System.Drawing.Point(6, 19);
            this.slPanelPersonnel.MasterPanel = null;
            this.slPanelPersonnel.Name = "slPanelPersonnel";
            this.slPanelPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelPersonnel.Size = new System.Drawing.Size(614, 155);
            this.slPanelPersonnel.SPName = "CHRIS_SP_PERSONNEL_MANAGER_FOR_SALARY_UPDATE";
            this.slPanelPersonnel.TabIndex = 9;
            // 
            // lookup_PR_CATEGORY
            // 
            this.lookup_PR_CATEGORY.ActionLOVExists = "PR_CATEGORY_LOV1_EXISTS";
            this.lookup_PR_CATEGORY.ActionType = "PR_CATEGORY_LOV1";
            this.lookup_PR_CATEGORY.ConditionalFields = "";
            this.lookup_PR_CATEGORY.CustomEnabled = true;
            this.lookup_PR_CATEGORY.DataFieldMapping = "";
            this.lookup_PR_CATEGORY.DependentLovControls = "";
            this.lookup_PR_CATEGORY.HiddenColumns = "";
            this.lookup_PR_CATEGORY.Image = ((System.Drawing.Image)(resources.GetObject("lookup_PR_CATEGORY.Image")));
            this.lookup_PR_CATEGORY.LoadDependentEntities = false;
            this.lookup_PR_CATEGORY.Location = new System.Drawing.Point(176, 77);
            this.lookup_PR_CATEGORY.LookUpTitle = null;
            this.lookup_PR_CATEGORY.Name = "lookup_PR_CATEGORY";
            this.lookup_PR_CATEGORY.Size = new System.Drawing.Size(26, 21);
            this.lookup_PR_CATEGORY.SkipValidationOnLeave = false;
            this.lookup_PR_CATEGORY.SPName = "CHRIS_SP_PERSONNEL_MANAGER_FOR_SALARY_UPDATE";
            this.lookup_PR_CATEGORY.TabIndex = 26;
            this.lookup_PR_CATEGORY.TabStop = false;
            this.lookup_PR_CATEGORY.UseVisualStyleBackColor = true;
            // 
            // lookup_Pr_P_No
            // 
            this.lookup_Pr_P_No.ActionLOVExists = "ListExist";
            this.lookup_Pr_P_No.ActionType = "List";
            this.lookup_Pr_P_No.ConditionalFields = "";
            this.lookup_Pr_P_No.CustomEnabled = true;
            this.lookup_Pr_P_No.DataFieldMapping = "";
            this.lookup_Pr_P_No.DependentLovControls = "";
            this.lookup_Pr_P_No.HiddenColumns = "PR_DESIG|PR_LEVEL|PR_CATEGORY|PR_JOINING_DATE|PR_ANNUAL_PACK|PR_CONFIRM_ON|PR_CON" +
                "F_FLAG|PR_NEW_BRANCH|PR_MONTH_AWARD";
            this.lookup_Pr_P_No.Image = ((System.Drawing.Image)(resources.GetObject("lookup_Pr_P_No.Image")));
            this.lookup_Pr_P_No.LoadDependentEntities = true;
            this.lookup_Pr_P_No.Location = new System.Drawing.Point(233, 9);
            this.lookup_Pr_P_No.LookUpTitle = null;
            this.lookup_Pr_P_No.Name = "lookup_Pr_P_No";
            this.lookup_Pr_P_No.Size = new System.Drawing.Size(26, 21);
            this.lookup_Pr_P_No.SkipValidationOnLeave = false;
            this.lookup_Pr_P_No.SPName = "CHRIS_SP_PERSONNEL_MANAGER_FOR_SALARY_UPDATE";
            this.lookup_Pr_P_No.TabIndex = 24;
            this.lookup_Pr_P_No.TabStop = false;
            this.lookup_Pr_P_No.UseVisualStyleBackColor = true;
            // 
            // slDatePicker_PR_CONFIRM_ON
            // 
            this.slDatePicker_PR_CONFIRM_ON.CustomEnabled = true;
            this.slDatePicker_PR_CONFIRM_ON.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_PR_CONFIRM_ON.DataFieldMapping = "PR_CONFIRM_ON";
            this.slDatePicker_PR_CONFIRM_ON.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_PR_CONFIRM_ON.HasChanges = true;
            this.slDatePicker_PR_CONFIRM_ON.Location = new System.Drawing.Point(127, 124);
            this.slDatePicker_PR_CONFIRM_ON.Name = "slDatePicker_PR_CONFIRM_ON";
            this.slDatePicker_PR_CONFIRM_ON.NullValue = " ";
            this.slDatePicker_PR_CONFIRM_ON.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_PR_CONFIRM_ON.TabIndex = 4;
            this.slDatePicker_PR_CONFIRM_ON.Value = new System.DateTime(2011, 1, 29, 0, 0, 0, 0);
            // 
            // txt_PR_ANNUAL_PACK
            // 
            this.txt_PR_ANNUAL_PACK.AllowSpace = true;
            this.txt_PR_ANNUAL_PACK.AssociatedLookUpName = "";
            this.txt_PR_ANNUAL_PACK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ANNUAL_PACK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ANNUAL_PACK.ContinuationTextBox = null;
            this.txt_PR_ANNUAL_PACK.CustomEnabled = true;
            this.txt_PR_ANNUAL_PACK.DataFieldMapping = "PR_ANNUAL_PACK";
            this.txt_PR_ANNUAL_PACK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ANNUAL_PACK.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ANNUAL_PACK.IsDate = false;
            this.txt_PR_ANNUAL_PACK.Location = new System.Drawing.Point(127, 100);
            this.txt_PR_ANNUAL_PACK.MaxLength = 7;
            this.txt_PR_ANNUAL_PACK.Name = "txt_PR_ANNUAL_PACK";
            this.txt_PR_ANNUAL_PACK.NumberFormat = "###,###,##0.00";
            this.txt_PR_ANNUAL_PACK.Postfix = "";
            this.txt_PR_ANNUAL_PACK.Prefix = "";
            this.txt_PR_ANNUAL_PACK.ReadOnly = true;
            this.txt_PR_ANNUAL_PACK.Size = new System.Drawing.Size(100, 20);
            this.txt_PR_ANNUAL_PACK.SkipValidation = false;
            this.txt_PR_ANNUAL_PACK.TabIndex = 22;
            this.txt_PR_ANNUAL_PACK.TabStop = false;
            this.txt_PR_ANNUAL_PACK.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PR_ANNUAL_PACK.TextType = CrplControlLibrary.TextType.Double;
            this.toolTip1.SetToolTip(this.txt_PR_ANNUAL_PACK, "Enter value for : PR_ANNUAL_PACK");
            // 
            // txt_PR_CONF_FLAG
            // 
            this.txt_PR_CONF_FLAG.AllowSpace = true;
            this.txt_PR_CONF_FLAG.AssociatedLookUpName = "";
            this.txt_PR_CONF_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_CONF_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_CONF_FLAG.ContinuationTextBox = null;
            this.txt_PR_CONF_FLAG.CustomEnabled = true;
            this.txt_PR_CONF_FLAG.DataFieldMapping = "PR_CONF_FLAG";
            this.txt_PR_CONF_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_CONF_FLAG.GetRecordsOnUpDownKeys = false;
            this.txt_PR_CONF_FLAG.IsDate = false;
            this.txt_PR_CONF_FLAG.IsRequired = true;
            this.txt_PR_CONF_FLAG.Location = new System.Drawing.Point(448, 122);
            this.txt_PR_CONF_FLAG.MaxLength = 1;
            this.txt_PR_CONF_FLAG.Name = "txt_PR_CONF_FLAG";
            this.txt_PR_CONF_FLAG.NumberFormat = "###,###,##0.00";
            this.txt_PR_CONF_FLAG.Postfix = "";
            this.txt_PR_CONF_FLAG.Prefix = "";
            this.txt_PR_CONF_FLAG.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_CONF_FLAG.SkipValidation = false;
            this.txt_PR_CONF_FLAG.TabIndex = 5;
            this.txt_PR_CONF_FLAG.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_CONF_FLAG.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_CONF_FLAG_Validating);
            // 
            // txt_PR_MONTH_AWARD
            // 
            this.txt_PR_MONTH_AWARD.AllowSpace = true;
            this.txt_PR_MONTH_AWARD.AssociatedLookUpName = "";
            this.txt_PR_MONTH_AWARD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_MONTH_AWARD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_MONTH_AWARD.ContinuationTextBox = null;
            this.txt_PR_MONTH_AWARD.CustomEnabled = true;
            this.txt_PR_MONTH_AWARD.DataFieldMapping = "PR_MONTH_AWARD";
            this.txt_PR_MONTH_AWARD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_MONTH_AWARD.GetRecordsOnUpDownKeys = false;
            this.txt_PR_MONTH_AWARD.IsDate = false;
            this.txt_PR_MONTH_AWARD.Location = new System.Drawing.Point(448, 99);
            this.txt_PR_MONTH_AWARD.MaxLength = 2;
            this.txt_PR_MONTH_AWARD.Name = "txt_PR_MONTH_AWARD";
            this.txt_PR_MONTH_AWARD.NumberFormat = "###,###,##0.00";
            this.txt_PR_MONTH_AWARD.Postfix = "";
            this.txt_PR_MONTH_AWARD.Prefix = "";
            this.txt_PR_MONTH_AWARD.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_MONTH_AWARD.SkipValidation = false;
            this.txt_PR_MONTH_AWARD.TabIndex = 3;
            this.txt_PR_MONTH_AWARD.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txt_PR_MONTH_AWARD, "Enter value for : PR_MONTH_AWARD");
            this.txt_PR_MONTH_AWARD.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_MONTH_AWARD_Validating);
            // 
            // txt_PR_FIRST_NAME
            // 
            this.txt_PR_FIRST_NAME.AllowSpace = true;
            this.txt_PR_FIRST_NAME.AssociatedLookUpName = "";
            this.txt_PR_FIRST_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_FIRST_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_FIRST_NAME.ContinuationTextBox = null;
            this.txt_PR_FIRST_NAME.CustomEnabled = true;
            this.txt_PR_FIRST_NAME.DataFieldMapping = "PR_FIRST_NAME";
            this.txt_PR_FIRST_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_FIRST_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_FIRST_NAME.IsDate = false;
            this.txt_PR_FIRST_NAME.Location = new System.Drawing.Point(127, 34);
            this.txt_PR_FIRST_NAME.Name = "txt_PR_FIRST_NAME";
            this.txt_PR_FIRST_NAME.NumberFormat = "###,###,##0.00";
            this.txt_PR_FIRST_NAME.Postfix = "";
            this.txt_PR_FIRST_NAME.Prefix = "";
            this.txt_PR_FIRST_NAME.ReadOnly = true;
            this.txt_PR_FIRST_NAME.Size = new System.Drawing.Size(200, 20);
            this.txt_PR_FIRST_NAME.SkipValidation = false;
            this.txt_PR_FIRST_NAME.TabIndex = 18;
            this.txt_PR_FIRST_NAME.TabStop = false;
            this.txt_PR_FIRST_NAME.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_PR_FIRST_NAME, "Enter value for : PR_FIRST_NAME");
            // 
            // txt_PR_NEW_BRANCH
            // 
            this.txt_PR_NEW_BRANCH.AllowSpace = true;
            this.txt_PR_NEW_BRANCH.AssociatedLookUpName = "";
            this.txt_PR_NEW_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NEW_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NEW_BRANCH.ContinuationTextBox = null;
            this.txt_PR_NEW_BRANCH.CustomEnabled = true;
            this.txt_PR_NEW_BRANCH.DataFieldMapping = "PR_NEW_BRANCH";
            this.txt_PR_NEW_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NEW_BRANCH.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NEW_BRANCH.IsDate = false;
            this.txt_PR_NEW_BRANCH.Location = new System.Drawing.Point(448, 12);
            this.txt_PR_NEW_BRANCH.MaxLength = 3;
            this.txt_PR_NEW_BRANCH.Name = "txt_PR_NEW_BRANCH";
            this.txt_PR_NEW_BRANCH.NumberFormat = "###,###,##0.00";
            this.txt_PR_NEW_BRANCH.Postfix = "";
            this.txt_PR_NEW_BRANCH.Prefix = "";
            this.txt_PR_NEW_BRANCH.ReadOnly = true;
            this.txt_PR_NEW_BRANCH.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_NEW_BRANCH.SkipValidation = false;
            this.txt_PR_NEW_BRANCH.TabIndex = 17;
            this.txt_PR_NEW_BRANCH.TabStop = false;
            this.txt_PR_NEW_BRANCH.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_PR_NEW_BRANCH, "Enter value for : PR_BRANCH");
            // 
            // txt_PR_LEVEL
            // 
            this.txt_PR_LEVEL.AllowSpace = true;
            this.txt_PR_LEVEL.AssociatedLookUpName = "";
            this.txt_PR_LEVEL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LEVEL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LEVEL.ContinuationTextBox = null;
            this.txt_PR_LEVEL.CustomEnabled = true;
            this.txt_PR_LEVEL.DataFieldMapping = "PR_LEVEL";
            this.txt_PR_LEVEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LEVEL.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LEVEL.IsDate = false;
            this.txt_PR_LEVEL.Location = new System.Drawing.Point(448, 53);
            this.txt_PR_LEVEL.Name = "txt_PR_LEVEL";
            this.txt_PR_LEVEL.NumberFormat = "###,###,##0.00";
            this.txt_PR_LEVEL.Postfix = "";
            this.txt_PR_LEVEL.Prefix = "";
            this.txt_PR_LEVEL.ReadOnly = true;
            this.txt_PR_LEVEL.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_LEVEL.SkipValidation = false;
            this.txt_PR_LEVEL.TabIndex = 16;
            this.txt_PR_LEVEL.TabStop = false;
            this.txt_PR_LEVEL.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_PR_LEVEL, "Enter value for : PR_LEVEL");
            // 
            // txt_PR_DESIG
            // 
            this.txt_PR_DESIG.AllowSpace = true;
            this.txt_PR_DESIG.AssociatedLookUpName = "";
            this.txt_PR_DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_DESIG.ContinuationTextBox = null;
            this.txt_PR_DESIG.CustomEnabled = true;
            this.txt_PR_DESIG.DataFieldMapping = "PR_DESIG";
            this.txt_PR_DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_DESIG.GetRecordsOnUpDownKeys = false;
            this.txt_PR_DESIG.IsDate = false;
            this.txt_PR_DESIG.Location = new System.Drawing.Point(127, 56);
            this.txt_PR_DESIG.MaxLength = 3;
            this.txt_PR_DESIG.Name = "txt_PR_DESIG";
            this.txt_PR_DESIG.NumberFormat = "###,###,##0.00";
            this.txt_PR_DESIG.Postfix = "";
            this.txt_PR_DESIG.Prefix = "";
            this.txt_PR_DESIG.ReadOnly = true;
            this.txt_PR_DESIG.Size = new System.Drawing.Size(43, 20);
            this.txt_PR_DESIG.SkipValidation = false;
            this.txt_PR_DESIG.TabIndex = 15;
            this.txt_PR_DESIG.TabStop = false;
            this.txt_PR_DESIG.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_PR_DESIG, "Enter value for : PR_DESIG");
            // 
            // txt_PR_CATEGORY
            // 
            this.txt_PR_CATEGORY.AllowSpace = true;
            this.txt_PR_CATEGORY.AssociatedLookUpName = "lookup_PR_CATEGORY";
            this.txt_PR_CATEGORY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_CATEGORY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_CATEGORY.ContinuationTextBox = null;
            this.txt_PR_CATEGORY.CustomEnabled = true;
            this.txt_PR_CATEGORY.DataFieldMapping = "PR_CATEGORY";
            this.txt_PR_CATEGORY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_CATEGORY.GetRecordsOnUpDownKeys = false;
            this.txt_PR_CATEGORY.IsDate = false;
            this.txt_PR_CATEGORY.IsLookUpField = true;
            this.txt_PR_CATEGORY.IsRequired = true;
            this.txt_PR_CATEGORY.Location = new System.Drawing.Point(127, 78);
            this.txt_PR_CATEGORY.MaxLength = 1;
            this.txt_PR_CATEGORY.Name = "txt_PR_CATEGORY";
            this.txt_PR_CATEGORY.NumberFormat = "###,###,##0.00";
            this.txt_PR_CATEGORY.Postfix = "";
            this.txt_PR_CATEGORY.Prefix = "";
            this.txt_PR_CATEGORY.Size = new System.Drawing.Size(43, 20);
            this.txt_PR_CATEGORY.SkipValidation = false;
            this.txt_PR_CATEGORY.TabIndex = 2;
            this.txt_PR_CATEGORY.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_PR_CATEGORY, "Enter value for : PR_CATEGORY");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(376, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Branch   :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(354, 80);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Joining Date  :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(29, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Designation   :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(45, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Category   :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Annual Package   :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(339, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Monthly Award   :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(385, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Level   :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(35, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Conf. Date   :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(339, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Confirmed Y/N   :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(61, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Name   :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Personnel No.  :";
            // 
            // slDatePicker_PR_JOINING_DATE
            // 
            this.slDatePicker_PR_JOINING_DATE.CustomEnabled = true;
            this.slDatePicker_PR_JOINING_DATE.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_PR_JOINING_DATE.DataFieldMapping = "PR_JOINING_DATE";
            this.slDatePicker_PR_JOINING_DATE.Enabled = false;
            this.slDatePicker_PR_JOINING_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_PR_JOINING_DATE.HasChanges = true;
            this.slDatePicker_PR_JOINING_DATE.Location = new System.Drawing.Point(448, 76);
            this.slDatePicker_PR_JOINING_DATE.Name = "slDatePicker_PR_JOINING_DATE";
            this.slDatePicker_PR_JOINING_DATE.NullValue = " ";
            this.slDatePicker_PR_JOINING_DATE.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_PR_JOINING_DATE.TabIndex = 1;
            this.slDatePicker_PR_JOINING_DATE.TabStop = false;
            this.slDatePicker_PR_JOINING_DATE.Value = new System.DateTime(2011, 1, 29, 0, 0, 0, 0);
            // 
            // txt_PR_P_NO
            // 
            this.txt_PR_P_NO.AllowSpace = true;
            this.txt_PR_P_NO.AssociatedLookUpName = "lookup_Pr_P_No";
            this.txt_PR_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_P_NO.ContinuationTextBox = null;
            this.txt_PR_P_NO.CustomEnabled = true;
            this.txt_PR_P_NO.DataFieldMapping = "PR_P_NO";
            this.txt_PR_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_P_NO.IsDate = false;
            this.txt_PR_P_NO.IsLookUpField = true;
            this.txt_PR_P_NO.IsRequired = true;
            this.txt_PR_P_NO.Location = new System.Drawing.Point(127, 12);
            this.txt_PR_P_NO.MaxLength = 6;
            this.txt_PR_P_NO.Name = "txt_PR_P_NO";
            this.txt_PR_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_P_NO.Postfix = "";
            this.txt_PR_P_NO.Prefix = "";
            this.txt_PR_P_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_PR_P_NO.SkipValidation = true;
            this.txt_PR_P_NO.TabIndex = 1;
            this.txt_PR_P_NO.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txt_PR_P_NO, "Enter value for : PR_P_NO");
            // 
            // grpPersonnel
            // 
            this.grpPersonnel.Controls.Add(this.slPanelPersonnel);
            this.grpPersonnel.Location = new System.Drawing.Point(6, 97);
            this.grpPersonnel.Name = "grpPersonnel";
            this.grpPersonnel.Size = new System.Drawing.Size(626, 179);
            this.grpPersonnel.TabIndex = 10;
            this.grpPersonnel.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.slPanelPayroll);
            this.groupBox2.Location = new System.Drawing.Point(3, 276);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(632, 166);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            // 
            // slPanelPayroll
            // 
            this.slPanelPayroll.ConcurrentPanels = null;
            this.slPanelPayroll.Controls.Add(this.label26);
            this.slPanelPayroll.Controls.Add(this.label24);
            this.slPanelPayroll.Controls.Add(this.label25);
            this.slPanelPayroll.Controls.Add(this.label18);
            this.slPanelPayroll.Controls.Add(this.label19);
            this.slPanelPayroll.Controls.Add(this.label20);
            this.slPanelPayroll.Controls.Add(this.label21);
            this.slPanelPayroll.Controls.Add(this.label22);
            this.slPanelPayroll.Controls.Add(this.label23);
            this.slPanelPayroll.Controls.Add(this.label12);
            this.slPanelPayroll.Controls.Add(this.label13);
            this.slPanelPayroll.Controls.Add(this.label14);
            this.slPanelPayroll.Controls.Add(this.label15);
            this.slPanelPayroll.Controls.Add(this.label16);
            this.slPanelPayroll.Controls.Add(this.label17);
            this.slPanelPayroll.Controls.Add(this.slDatePicker_PA_DATE);
            this.slPanelPayroll.Controls.Add(this.txt_PA_BRANCH);
            this.slPanelPayroll.Controls.Add(this.txt_PA_P_NO);
            this.slPanelPayroll.Controls.Add(this.txt_PA_FLAG);
            this.slPanelPayroll.Controls.Add(this.txt_PA_F_INTREST);
            this.slPanelPayroll.Controls.Add(this.txt_PA_F_INSTALL);
            this.slPanelPayroll.Controls.Add(this.txt_PA_PFUND);
            this.slPanelPayroll.Controls.Add(this.txt_PA_INS_PREMIUM);
            this.slPanelPayroll.Controls.Add(this.txt_PA_WORKED_DAYS);
            this.slPanelPayroll.Controls.Add(this.txt_PA_HOUSE);
            this.slPanelPayroll.Controls.Add(this.txt_PA_L_INTREST);
            this.slPanelPayroll.Controls.Add(this.txt_PA_L_INSTALL);
            this.slPanelPayroll.Controls.Add(this.txt_PA_CONV_OFF);
            this.slPanelPayroll.Controls.Add(this.txt_PA_INCOME_TAX);
            this.slPanelPayroll.Controls.Add(this.txt_PA_SAL_ADVANCE);
            this.slPanelPayroll.Controls.Add(this.txt_PA_BASIC);
            this.slPanelPayroll.CurrentRowIndex = 0;
            this.slPanelPayroll.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelPayroll.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelPayroll.DependentPanels = null;
            this.slPanelPayroll.DisableDependentLoad = false;
            this.slPanelPayroll.EnableDelete = false;
            this.slPanelPayroll.EnableInsert = true;
            this.slPanelPayroll.EnableQuery = false;
            this.slPanelPayroll.EnableUpdate = true;
            this.slPanelPayroll.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PAYROLLCommand";
            this.slPanelPayroll.Location = new System.Drawing.Point(4, 13);
            this.slPanelPayroll.MasterPanel = null;
            this.slPanelPayroll.Name = "slPanelPayroll";
            this.slPanelPayroll.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelPayroll.Size = new System.Drawing.Size(625, 150);
            this.slPanelPayroll.SPName = "CHRIS_SP_PAYROLL_MANAGERFOR_SALARY_UPDATE";
            this.slPanelPayroll.TabIndex = 9;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(455, 35);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(63, 13);
            this.label26.TabIndex = 53;
            this.label26.Text = "Branch   :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(456, 131);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(61, 13);
            this.label24.TabIndex = 52;
            this.label24.Text = "Pay.Flg  :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(436, 11);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 13);
            this.label25.TabIndex = 51;
            this.label25.Text = "Sal.Advance:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(264, 59);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 13);
            this.label18.TabIndex = 50;
            this.label18.Text = "P.Fund  :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(235, 82);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 13);
            this.label19.TabIndex = 49;
            this.label19.Text = "Ins.Premium   :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(235, 107);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 48;
            this.label20.Text = "Finance Int.   :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(233, 130);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(93, 13);
            this.label21.TabIndex = 47;
            this.label21.Text = "Finance Inst.  :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(264, 35);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(59, 13);
            this.label22.TabIndex = 46;
            this.label22.Text = "House   :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(231, 11);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 13);
            this.label23.TabIndex = 45;
            this.label23.Text = "Worked Days  :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(24, 59);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 44;
            this.label12.Text = "Conveyance  :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(24, 82);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 13);
            this.label13.TabIndex = 43;
            this.label13.Text = "Income Tax   :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(23, 106);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 42;
            this.label14.Text = "Loan Interest :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(27, 130);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 13);
            this.label15.TabIndex = 41;
            this.label15.Text = "Loan Install. :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(56, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 40;
            this.label16.Text = "Basic   :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(26, 11);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 13);
            this.label17.TabIndex = 39;
            this.label17.Text = "Salary Date  :";
            // 
            // slDatePicker_PA_DATE
            // 
            this.slDatePicker_PA_DATE.CustomEnabled = true;
            this.slDatePicker_PA_DATE.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_PA_DATE.DataFieldMapping = "PA_DATE";
            this.slDatePicker_PA_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_PA_DATE.HasChanges = true;
            this.slDatePicker_PA_DATE.IsRequired = true;
            this.slDatePicker_PA_DATE.Location = new System.Drawing.Point(119, 6);
            this.slDatePicker_PA_DATE.Name = "slDatePicker_PA_DATE";
            this.slDatePicker_PA_DATE.NullValue = " ";
            this.slDatePicker_PA_DATE.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_PA_DATE.TabIndex = 1;
            this.slDatePicker_PA_DATE.Value = new System.DateTime(2011, 1, 29, 0, 0, 0, 0);
            // 
            // txt_PA_BRANCH
            // 
            this.txt_PA_BRANCH.AllowSpace = true;
            this.txt_PA_BRANCH.AssociatedLookUpName = "";
            this.txt_PA_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_BRANCH.ContinuationTextBox = null;
            this.txt_PA_BRANCH.CustomEnabled = true;
            this.txt_PA_BRANCH.DataFieldMapping = "PA_BRANCH";
            this.txt_PA_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_BRANCH.GetRecordsOnUpDownKeys = false;
            this.txt_PA_BRANCH.IsDate = false;
            this.txt_PA_BRANCH.Location = new System.Drawing.Point(586, 33);
            this.txt_PA_BRANCH.MaxLength = 3;
            this.txt_PA_BRANCH.Name = "txt_PA_BRANCH";
            this.txt_PA_BRANCH.NumberFormat = "###,###,##0.00";
            this.txt_PA_BRANCH.Postfix = "";
            this.txt_PA_BRANCH.Prefix = "";
            this.txt_PA_BRANCH.ReadOnly = true;
            this.txt_PA_BRANCH.Size = new System.Drawing.Size(34, 20);
            this.txt_PA_BRANCH.SkipValidation = false;
            this.txt_PA_BRANCH.TabIndex = 8;
            this.txt_PA_BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PA_P_NO
            // 
            this.txt_PA_P_NO.AllowSpace = true;
            this.txt_PA_P_NO.AssociatedLookUpName = "";
            this.txt_PA_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_P_NO.ContinuationTextBox = null;
            this.txt_PA_P_NO.CustomEnabled = true;
            this.txt_PA_P_NO.DataFieldMapping = "PR_P_NO";
            this.txt_PA_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PA_P_NO.IsDate = false;
            this.txt_PA_P_NO.Location = new System.Drawing.Point(520, 80);
            this.txt_PA_P_NO.Name = "txt_PA_P_NO";
            this.txt_PA_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_PA_P_NO.Postfix = "";
            this.txt_PA_P_NO.Prefix = "";
            this.txt_PA_P_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_P_NO.SkipValidation = false;
            this.txt_PA_P_NO.TabIndex = 36;
            this.txt_PA_P_NO.TabStop = false;
            this.txt_PA_P_NO.TextType = CrplControlLibrary.TextType.String;
            this.txt_PA_P_NO.Visible = false;
            // 
            // txt_PA_FLAG
            // 
            this.txt_PA_FLAG.AllowSpace = true;
            this.txt_PA_FLAG.AssociatedLookUpName = "";
            this.txt_PA_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_FLAG.ContinuationTextBox = null;
            this.txt_PA_FLAG.CustomEnabled = true;
            this.txt_PA_FLAG.DataFieldMapping = "PA_FLAG";
            this.txt_PA_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_FLAG.GetRecordsOnUpDownKeys = false;
            this.txt_PA_FLAG.IsDate = false;
            this.txt_PA_FLAG.Location = new System.Drawing.Point(586, 128);
            this.txt_PA_FLAG.MaxLength = 1;
            this.txt_PA_FLAG.Name = "txt_PA_FLAG";
            this.txt_PA_FLAG.NumberFormat = "###,###,##0.00";
            this.txt_PA_FLAG.Postfix = "";
            this.txt_PA_FLAG.Prefix = "";
            this.txt_PA_FLAG.Size = new System.Drawing.Size(34, 20);
            this.txt_PA_FLAG.SkipValidation = false;
            this.txt_PA_FLAG.TabIndex = 15;
            this.txt_PA_FLAG.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PA_F_INTREST
            // 
            this.txt_PA_F_INTREST.AllowSpace = true;
            this.txt_PA_F_INTREST.AssociatedLookUpName = "";
            this.txt_PA_F_INTREST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_F_INTREST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_F_INTREST.ContinuationTextBox = null;
            this.txt_PA_F_INTREST.CustomEnabled = true;
            this.txt_PA_F_INTREST.DataFieldMapping = "PA_F_INTREST";
            this.txt_PA_F_INTREST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_F_INTREST.GetRecordsOnUpDownKeys = false;
            this.txt_PA_F_INTREST.IsDate = false;
            this.txt_PA_F_INTREST.Location = new System.Drawing.Point(334, 104);
            this.txt_PA_F_INTREST.MaxLength = 10;
            this.txt_PA_F_INTREST.Name = "txt_PA_F_INTREST";
            this.txt_PA_F_INTREST.NumberFormat = "###,###,##0.00";
            this.txt_PA_F_INTREST.Postfix = "";
            this.txt_PA_F_INTREST.Prefix = "";
            this.txt_PA_F_INTREST.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_F_INTREST.SkipValidation = false;
            this.txt_PA_F_INTREST.TabIndex = 12;
            this.txt_PA_F_INTREST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_F_INTREST.TextType = CrplControlLibrary.TextType.Double;
            this.txt_PA_F_INTREST.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PA_F_INTREST_Validating);
            // 
            // txt_PA_F_INSTALL
            // 
            this.txt_PA_F_INSTALL.AllowSpace = true;
            this.txt_PA_F_INSTALL.AssociatedLookUpName = "";
            this.txt_PA_F_INSTALL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_F_INSTALL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_F_INSTALL.ContinuationTextBox = null;
            this.txt_PA_F_INSTALL.CustomEnabled = true;
            this.txt_PA_F_INSTALL.DataFieldMapping = "PA_F_INSTALL";
            this.txt_PA_F_INSTALL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_F_INSTALL.GetRecordsOnUpDownKeys = false;
            this.txt_PA_F_INSTALL.IsDate = false;
            this.txt_PA_F_INSTALL.Location = new System.Drawing.Point(334, 128);
            this.txt_PA_F_INSTALL.MaxLength = 8;
            this.txt_PA_F_INSTALL.Name = "txt_PA_F_INSTALL";
            this.txt_PA_F_INSTALL.NumberFormat = "###,###,##0.00";
            this.txt_PA_F_INSTALL.Postfix = "";
            this.txt_PA_F_INSTALL.Prefix = "";
            this.txt_PA_F_INSTALL.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_F_INSTALL.SkipValidation = false;
            this.txt_PA_F_INSTALL.TabIndex = 14;
            this.txt_PA_F_INSTALL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_F_INSTALL.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_PA_PFUND
            // 
            this.txt_PA_PFUND.AllowSpace = true;
            this.txt_PA_PFUND.AssociatedLookUpName = "";
            this.txt_PA_PFUND.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_PFUND.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_PFUND.ContinuationTextBox = null;
            this.txt_PA_PFUND.CustomEnabled = true;
            this.txt_PA_PFUND.DataFieldMapping = "PA_PFUND";
            this.txt_PA_PFUND.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_PFUND.GetRecordsOnUpDownKeys = false;
            this.txt_PA_PFUND.IsDate = false;
            this.txt_PA_PFUND.Location = new System.Drawing.Point(334, 56);
            this.txt_PA_PFUND.MaxLength = 8;
            this.txt_PA_PFUND.Name = "txt_PA_PFUND";
            this.txt_PA_PFUND.NumberFormat = "###,###,##0.00";
            this.txt_PA_PFUND.Postfix = "";
            this.txt_PA_PFUND.Prefix = "";
            this.txt_PA_PFUND.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_PFUND.SkipValidation = false;
            this.txt_PA_PFUND.TabIndex = 7;
            this.txt_PA_PFUND.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_PFUND.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_PA_INS_PREMIUM
            // 
            this.txt_PA_INS_PREMIUM.AllowSpace = true;
            this.txt_PA_INS_PREMIUM.AssociatedLookUpName = "";
            this.txt_PA_INS_PREMIUM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_INS_PREMIUM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_INS_PREMIUM.ContinuationTextBox = null;
            this.txt_PA_INS_PREMIUM.CustomEnabled = true;
            this.txt_PA_INS_PREMIUM.DataFieldMapping = "PA_INS_PREMIUM";
            this.txt_PA_INS_PREMIUM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_INS_PREMIUM.GetRecordsOnUpDownKeys = false;
            this.txt_PA_INS_PREMIUM.IsDate = false;
            this.txt_PA_INS_PREMIUM.Location = new System.Drawing.Point(334, 80);
            this.txt_PA_INS_PREMIUM.MaxLength = 5;
            this.txt_PA_INS_PREMIUM.Name = "txt_PA_INS_PREMIUM";
            this.txt_PA_INS_PREMIUM.NumberFormat = "###,###,##0.00";
            this.txt_PA_INS_PREMIUM.Postfix = "";
            this.txt_PA_INS_PREMIUM.Prefix = "";
            this.txt_PA_INS_PREMIUM.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_INS_PREMIUM.SkipValidation = false;
            this.txt_PA_INS_PREMIUM.TabIndex = 10;
            this.txt_PA_INS_PREMIUM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_INS_PREMIUM.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_PA_WORKED_DAYS
            // 
            this.txt_PA_WORKED_DAYS.AllowSpace = true;
            this.txt_PA_WORKED_DAYS.AssociatedLookUpName = "";
            this.txt_PA_WORKED_DAYS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_WORKED_DAYS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_WORKED_DAYS.ContinuationTextBox = null;
            this.txt_PA_WORKED_DAYS.CustomEnabled = true;
            this.txt_PA_WORKED_DAYS.DataFieldMapping = "PA_WORKED_DAYS";
            this.txt_PA_WORKED_DAYS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_WORKED_DAYS.GetRecordsOnUpDownKeys = false;
            this.txt_PA_WORKED_DAYS.IsDate = false;
            this.txt_PA_WORKED_DAYS.Location = new System.Drawing.Point(334, 8);
            this.txt_PA_WORKED_DAYS.MaxLength = 2;
            this.txt_PA_WORKED_DAYS.Name = "txt_PA_WORKED_DAYS";
            this.txt_PA_WORKED_DAYS.NumberFormat = "###,###,##0.00";
            this.txt_PA_WORKED_DAYS.Postfix = "";
            this.txt_PA_WORKED_DAYS.Prefix = "";
            this.txt_PA_WORKED_DAYS.Size = new System.Drawing.Size(44, 20);
            this.txt_PA_WORKED_DAYS.SkipValidation = false;
            this.txt_PA_WORKED_DAYS.TabIndex = 2;
            this.txt_PA_WORKED_DAYS.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_PA_HOUSE
            // 
            this.txt_PA_HOUSE.AllowSpace = true;
            this.txt_PA_HOUSE.AssociatedLookUpName = "";
            this.txt_PA_HOUSE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_HOUSE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_HOUSE.ContinuationTextBox = null;
            this.txt_PA_HOUSE.CustomEnabled = true;
            this.txt_PA_HOUSE.DataFieldMapping = "PA_HOUSE";
            this.txt_PA_HOUSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_HOUSE.GetRecordsOnUpDownKeys = false;
            this.txt_PA_HOUSE.IsDate = false;
            this.txt_PA_HOUSE.Location = new System.Drawing.Point(334, 32);
            this.txt_PA_HOUSE.MaxLength = 8;
            this.txt_PA_HOUSE.Name = "txt_PA_HOUSE";
            this.txt_PA_HOUSE.NumberFormat = "###,###,##0.00";
            this.txt_PA_HOUSE.Postfix = "";
            this.txt_PA_HOUSE.Prefix = "";
            this.txt_PA_HOUSE.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_HOUSE.SkipValidation = false;
            this.txt_PA_HOUSE.TabIndex = 5;
            this.txt_PA_HOUSE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_HOUSE.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_PA_L_INTREST
            // 
            this.txt_PA_L_INTREST.AllowSpace = true;
            this.txt_PA_L_INTREST.AssociatedLookUpName = "";
            this.txt_PA_L_INTREST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_L_INTREST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_L_INTREST.ContinuationTextBox = null;
            this.txt_PA_L_INTREST.CustomEnabled = true;
            this.txt_PA_L_INTREST.DataFieldMapping = "PA_L_INTREST";
            this.txt_PA_L_INTREST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_L_INTREST.GetRecordsOnUpDownKeys = false;
            this.txt_PA_L_INTREST.IsDate = false;
            this.txt_PA_L_INTREST.Location = new System.Drawing.Point(119, 105);
            this.txt_PA_L_INTREST.MaxLength = 6;
            this.txt_PA_L_INTREST.Name = "txt_PA_L_INTREST";
            this.txt_PA_L_INTREST.NumberFormat = "###,###,##0.00";
            this.txt_PA_L_INTREST.Postfix = "";
            this.txt_PA_L_INTREST.Prefix = "";
            this.txt_PA_L_INTREST.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_L_INTREST.SkipValidation = false;
            this.txt_PA_L_INTREST.TabIndex = 11;
            this.txt_PA_L_INTREST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_L_INTREST.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_PA_L_INSTALL
            // 
            this.txt_PA_L_INSTALL.AllowSpace = true;
            this.txt_PA_L_INSTALL.AssociatedLookUpName = "";
            this.txt_PA_L_INSTALL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_L_INSTALL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_L_INSTALL.ContinuationTextBox = null;
            this.txt_PA_L_INSTALL.CustomEnabled = true;
            this.txt_PA_L_INSTALL.DataFieldMapping = "PA_L_INSTALL";
            this.txt_PA_L_INSTALL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_L_INSTALL.GetRecordsOnUpDownKeys = false;
            this.txt_PA_L_INSTALL.IsDate = false;
            this.txt_PA_L_INSTALL.Location = new System.Drawing.Point(119, 129);
            this.txt_PA_L_INSTALL.MaxLength = 8;
            this.txt_PA_L_INSTALL.Name = "txt_PA_L_INSTALL";
            this.txt_PA_L_INSTALL.NumberFormat = "###,###,##0.00";
            this.txt_PA_L_INSTALL.Postfix = "";
            this.txt_PA_L_INSTALL.Prefix = "";
            this.txt_PA_L_INSTALL.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_L_INSTALL.SkipValidation = false;
            this.txt_PA_L_INSTALL.TabIndex = 13;
            this.txt_PA_L_INSTALL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_L_INSTALL.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_PA_CONV_OFF
            // 
            this.txt_PA_CONV_OFF.AllowSpace = true;
            this.txt_PA_CONV_OFF.AssociatedLookUpName = "";
            this.txt_PA_CONV_OFF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_CONV_OFF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_CONV_OFF.ContinuationTextBox = null;
            this.txt_PA_CONV_OFF.CustomEnabled = true;
            this.txt_PA_CONV_OFF.DataFieldMapping = "PA_CONV_OFF";
            this.txt_PA_CONV_OFF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_CONV_OFF.GetRecordsOnUpDownKeys = false;
            this.txt_PA_CONV_OFF.IsDate = false;
            this.txt_PA_CONV_OFF.Location = new System.Drawing.Point(119, 57);
            this.txt_PA_CONV_OFF.MaxLength = 8;
            this.txt_PA_CONV_OFF.Name = "txt_PA_CONV_OFF";
            this.txt_PA_CONV_OFF.NumberFormat = "###,###,##0.00";
            this.txt_PA_CONV_OFF.Postfix = "";
            this.txt_PA_CONV_OFF.Prefix = "";
            this.txt_PA_CONV_OFF.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_CONV_OFF.SkipValidation = false;
            this.txt_PA_CONV_OFF.TabIndex = 6;
            this.txt_PA_CONV_OFF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_CONV_OFF.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_PA_INCOME_TAX
            // 
            this.txt_PA_INCOME_TAX.AllowSpace = true;
            this.txt_PA_INCOME_TAX.AssociatedLookUpName = "";
            this.txt_PA_INCOME_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_INCOME_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_INCOME_TAX.ContinuationTextBox = null;
            this.txt_PA_INCOME_TAX.CustomEnabled = true;
            this.txt_PA_INCOME_TAX.DataFieldMapping = "PA_INCOME_TAX";
            this.txt_PA_INCOME_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_INCOME_TAX.GetRecordsOnUpDownKeys = false;
            this.txt_PA_INCOME_TAX.IsDate = false;
            this.txt_PA_INCOME_TAX.Location = new System.Drawing.Point(119, 81);
            this.txt_PA_INCOME_TAX.MaxLength = 10;
            this.txt_PA_INCOME_TAX.Name = "txt_PA_INCOME_TAX";
            this.txt_PA_INCOME_TAX.NumberFormat = "###,###,##0.00";
            this.txt_PA_INCOME_TAX.Postfix = "";
            this.txt_PA_INCOME_TAX.Prefix = "";
            this.txt_PA_INCOME_TAX.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_INCOME_TAX.SkipValidation = false;
            this.txt_PA_INCOME_TAX.TabIndex = 9;
            this.txt_PA_INCOME_TAX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_INCOME_TAX.TextType = CrplControlLibrary.TextType.Double;
            this.txt_PA_INCOME_TAX.Validated += new System.EventHandler(this.txt_PA_INCOME_TAX_Validated);
            // 
            // txt_PA_SAL_ADVANCE
            // 
            this.txt_PA_SAL_ADVANCE.AllowSpace = true;
            this.txt_PA_SAL_ADVANCE.AssociatedLookUpName = "";
            this.txt_PA_SAL_ADVANCE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_SAL_ADVANCE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_SAL_ADVANCE.ContinuationTextBox = null;
            this.txt_PA_SAL_ADVANCE.CustomEnabled = true;
            this.txt_PA_SAL_ADVANCE.DataFieldMapping = "PA_SAL_ADVANCE";
            this.txt_PA_SAL_ADVANCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_SAL_ADVANCE.GetRecordsOnUpDownKeys = false;
            this.txt_PA_SAL_ADVANCE.IsDate = false;
            this.txt_PA_SAL_ADVANCE.Location = new System.Drawing.Point(520, 8);
            this.txt_PA_SAL_ADVANCE.MaxLength = 8;
            this.txt_PA_SAL_ADVANCE.Name = "txt_PA_SAL_ADVANCE";
            this.txt_PA_SAL_ADVANCE.NumberFormat = "###,###,##0.00";
            this.txt_PA_SAL_ADVANCE.Postfix = "";
            this.txt_PA_SAL_ADVANCE.Prefix = "";
            this.txt_PA_SAL_ADVANCE.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_SAL_ADVANCE.SkipValidation = false;
            this.txt_PA_SAL_ADVANCE.TabIndex = 3;
            this.txt_PA_SAL_ADVANCE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_SAL_ADVANCE.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_PA_BASIC
            // 
            this.txt_PA_BASIC.AllowSpace = true;
            this.txt_PA_BASIC.AssociatedLookUpName = "";
            this.txt_PA_BASIC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PA_BASIC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PA_BASIC.ContinuationTextBox = null;
            this.txt_PA_BASIC.CustomEnabled = true;
            this.txt_PA_BASIC.DataFieldMapping = "PA_BASIC";
            this.txt_PA_BASIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PA_BASIC.GetRecordsOnUpDownKeys = false;
            this.txt_PA_BASIC.IsDate = false;
            this.txt_PA_BASIC.Location = new System.Drawing.Point(119, 33);
            this.txt_PA_BASIC.MaxLength = 8;
            this.txt_PA_BASIC.Name = "txt_PA_BASIC";
            this.txt_PA_BASIC.NumberFormat = "###,###,##0.00";
            this.txt_PA_BASIC.Postfix = "";
            this.txt_PA_BASIC.Prefix = "";
            this.txt_PA_BASIC.Size = new System.Drawing.Size(100, 20);
            this.txt_PA_BASIC.SkipValidation = false;
            this.txt_PA_BASIC.TabIndex = 4;
            this.txt_PA_BASIC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PA_BASIC.TextType = CrplControlLibrary.TextType.Double;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.slPanelPayroll_Allow);
            this.groupBox3.Location = new System.Drawing.Point(6, 445);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(627, 69);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Other Allowances";
            // 
            // slPanelPayroll_Allow
            // 
            this.slPanelPayroll_Allow.ConcurrentPanels = null;
            this.slPanelPayroll_Allow.Controls.Add(this.slTxt_PW_PAY_DATE);
            this.slPanelPayroll_Allow.Controls.Add(this.lookup_PW_ALL_CODE);
            this.slPanelPayroll_Allow.Controls.Add(this.txt_PW_ALL_AMOUNT);
            this.slPanelPayroll_Allow.Controls.Add(this.txt_PW_P_NO);
            this.slPanelPayroll_Allow.Controls.Add(this.label31);
            this.slPanelPayroll_Allow.Controls.Add(this.txt_PW_ALL_CODE);
            this.slPanelPayroll_Allow.Controls.Add(this.slDatePicker_PW_PAY_DATE);
            this.slPanelPayroll_Allow.Controls.Add(this.label27);
            this.slPanelPayroll_Allow.Controls.Add(this.label28);
            this.slPanelPayroll_Allow.CurrentRowIndex = 0;
            this.slPanelPayroll_Allow.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelPayroll_Allow.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelPayroll_Allow.DependentPanels = null;
            this.slPanelPayroll_Allow.DisableDependentLoad = false;
            this.slPanelPayroll_Allow.EnableDelete = true;
            this.slPanelPayroll_Allow.EnableInsert = true;
            this.slPanelPayroll_Allow.EnableQuery = false;
            this.slPanelPayroll_Allow.EnableUpdate = true;
            this.slPanelPayroll_Allow.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PAYROLLALLOWCommand_Salary";
            this.slPanelPayroll_Allow.Location = new System.Drawing.Point(6, 12);
            this.slPanelPayroll_Allow.MasterPanel = null;
            this.slPanelPayroll_Allow.Name = "slPanelPayroll_Allow";
            this.slPanelPayroll_Allow.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelPayroll_Allow.Size = new System.Drawing.Size(608, 52);
            this.slPanelPayroll_Allow.SPName = "CHRIS_SP_PAYROLL_ALLOW_MANAGER_FOR_SALARY_UPDATE";
            this.slPanelPayroll_Allow.TabIndex = 9;
            // 
            // slTxt_PW_PAY_DATE
            // 
            this.slTxt_PW_PAY_DATE.AllowSpace = true;
            this.slTxt_PW_PAY_DATE.AssociatedLookUpName = "";
            this.slTxt_PW_PAY_DATE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxt_PW_PAY_DATE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxt_PW_PAY_DATE.ContinuationTextBox = null;
            this.slTxt_PW_PAY_DATE.CustomEnabled = true;
            this.slTxt_PW_PAY_DATE.DataFieldMapping = "PA_DATE";
            this.slTxt_PW_PAY_DATE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxt_PW_PAY_DATE.GetRecordsOnUpDownKeys = false;
            this.slTxt_PW_PAY_DATE.IsDate = false;
            this.slTxt_PW_PAY_DATE.Location = new System.Drawing.Point(127, 29);
            this.slTxt_PW_PAY_DATE.Name = "slTxt_PW_PAY_DATE";
            this.slTxt_PW_PAY_DATE.NumberFormat = "###,###,##0.00";
            this.slTxt_PW_PAY_DATE.Postfix = "";
            this.slTxt_PW_PAY_DATE.Prefix = "";
            this.slTxt_PW_PAY_DATE.ReadOnly = true;
            this.slTxt_PW_PAY_DATE.Size = new System.Drawing.Size(55, 20);
            this.slTxt_PW_PAY_DATE.SkipValidation = false;
            this.slTxt_PW_PAY_DATE.TabIndex = 47;
            this.slTxt_PW_PAY_DATE.TabStop = false;
            this.slTxt_PW_PAY_DATE.TextType = CrplControlLibrary.TextType.String;
            // 
            // lookup_PW_ALL_CODE
            // 
            this.lookup_PW_ALL_CODE.ActionLOVExists = "";
            this.lookup_PW_ALL_CODE.ActionType = "PW_ALL_CODE_LOV2";
            this.lookup_PW_ALL_CODE.ConditionalFields = "";
            this.lookup_PW_ALL_CODE.CustomEnabled = true;
            this.lookup_PW_ALL_CODE.DataFieldMapping = "";
            this.lookup_PW_ALL_CODE.DependentLovControls = "";
            this.lookup_PW_ALL_CODE.HiddenColumns = "";
            this.lookup_PW_ALL_CODE.Image = ((System.Drawing.Image)(resources.GetObject("lookup_PW_ALL_CODE.Image")));
            this.lookup_PW_ALL_CODE.LoadDependentEntities = false;
            this.lookup_PW_ALL_CODE.Location = new System.Drawing.Point(188, 6);
            this.lookup_PW_ALL_CODE.LookUpTitle = null;
            this.lookup_PW_ALL_CODE.Name = "lookup_PW_ALL_CODE";
            this.lookup_PW_ALL_CODE.Size = new System.Drawing.Size(26, 21);
            this.lookup_PW_ALL_CODE.SkipValidationOnLeave = true;
            this.lookup_PW_ALL_CODE.SPName = "CHRIS_SP_PAYROLL_ALLOW_MANAGER_FOR_SALARY_UPDATE";
            this.lookup_PW_ALL_CODE.TabIndex = 25;
            this.lookup_PW_ALL_CODE.TabStop = false;
            this.lookup_PW_ALL_CODE.UseVisualStyleBackColor = true;
            // 
            // txt_PW_ALL_AMOUNT
            // 
            this.txt_PW_ALL_AMOUNT.AllowSpace = true;
            this.txt_PW_ALL_AMOUNT.AssociatedLookUpName = "";
            this.txt_PW_ALL_AMOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PW_ALL_AMOUNT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PW_ALL_AMOUNT.ContinuationTextBox = null;
            this.txt_PW_ALL_AMOUNT.CustomEnabled = true;
            this.txt_PW_ALL_AMOUNT.DataFieldMapping = "PW_ALL_AMOUNT";
            this.txt_PW_ALL_AMOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PW_ALL_AMOUNT.GetRecordsOnUpDownKeys = false;
            this.txt_PW_ALL_AMOUNT.IsDate = false;
            this.txt_PW_ALL_AMOUNT.Location = new System.Drawing.Point(445, 6);
            this.txt_PW_ALL_AMOUNT.Name = "txt_PW_ALL_AMOUNT";
            this.txt_PW_ALL_AMOUNT.NumberFormat = "###,###,##0.00";
            this.txt_PW_ALL_AMOUNT.Postfix = "";
            this.txt_PW_ALL_AMOUNT.Prefix = "";
            this.txt_PW_ALL_AMOUNT.Size = new System.Drawing.Size(100, 20);
            this.txt_PW_ALL_AMOUNT.SkipValidation = false;
            this.txt_PW_ALL_AMOUNT.TabIndex = 2;
            this.txt_PW_ALL_AMOUNT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PW_P_NO
            // 
            this.txt_PW_P_NO.AllowSpace = true;
            this.txt_PW_P_NO.AssociatedLookUpName = "";
            this.txt_PW_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PW_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PW_P_NO.ContinuationTextBox = null;
            this.txt_PW_P_NO.CustomEnabled = true;
            this.txt_PW_P_NO.DataFieldMapping = "PR_P_NO";
            this.txt_PW_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PW_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PW_P_NO.IsDate = false;
            this.txt_PW_P_NO.Location = new System.Drawing.Point(445, 30);
            this.txt_PW_P_NO.Name = "txt_PW_P_NO";
            this.txt_PW_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_PW_P_NO.Postfix = "";
            this.txt_PW_P_NO.Prefix = "";
            this.txt_PW_P_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_PW_P_NO.SkipValidation = false;
            this.txt_PW_P_NO.TabIndex = 46;
            this.txt_PW_P_NO.TabStop = false;
            this.txt_PW_P_NO.TextType = CrplControlLibrary.TextType.String;
            this.txt_PW_P_NO.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(328, 13);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(111, 13);
            this.label31.TabIndex = 45;
            this.label31.Text = "Allowance Amount";
            // 
            // txt_PW_ALL_CODE
            // 
            this.txt_PW_ALL_CODE.AllowSpace = true;
            this.txt_PW_ALL_CODE.AssociatedLookUpName = "lookup_PW_ALL_CODE";
            this.txt_PW_ALL_CODE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PW_ALL_CODE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PW_ALL_CODE.ContinuationTextBox = null;
            this.txt_PW_ALL_CODE.CustomEnabled = true;
            this.txt_PW_ALL_CODE.DataFieldMapping = "PW_ALL_CODE";
            this.txt_PW_ALL_CODE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PW_ALL_CODE.GetRecordsOnUpDownKeys = false;
            this.txt_PW_ALL_CODE.IsDate = false;
            this.txt_PW_ALL_CODE.IsLookUpField = true;
            this.txt_PW_ALL_CODE.Location = new System.Drawing.Point(127, 6);
            this.txt_PW_ALL_CODE.Name = "txt_PW_ALL_CODE";
            this.txt_PW_ALL_CODE.NumberFormat = "###,###,##0.00";
            this.txt_PW_ALL_CODE.Postfix = "";
            this.txt_PW_ALL_CODE.Prefix = "";
            this.txt_PW_ALL_CODE.Size = new System.Drawing.Size(55, 20);
            this.txt_PW_ALL_CODE.SkipValidation = false;
            this.txt_PW_ALL_CODE.TabIndex = 1;
            this.txt_PW_ALL_CODE.TextType = CrplControlLibrary.TextType.String;
            // 
            // slDatePicker_PW_PAY_DATE
            // 
            this.slDatePicker_PW_PAY_DATE.CustomEnabled = true;
            this.slDatePicker_PW_PAY_DATE.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_PW_PAY_DATE.DataFieldMapping = "";
            this.slDatePicker_PW_PAY_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_PW_PAY_DATE.HasChanges = true;
            this.slDatePicker_PW_PAY_DATE.Location = new System.Drawing.Point(339, 28);
            this.slDatePicker_PW_PAY_DATE.Name = "slDatePicker_PW_PAY_DATE";
            this.slDatePicker_PW_PAY_DATE.NullValue = " ";
            this.slDatePicker_PW_PAY_DATE.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_PW_PAY_DATE.TabIndex = 43;
            this.slDatePicker_PW_PAY_DATE.TabStop = false;
            this.slDatePicker_PW_PAY_DATE.Value = new System.DateTime(2011, 1, 29, 0, 0, 0, 0);
            this.slDatePicker_PW_PAY_DATE.Visible = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(9, 8);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(114, 13);
            this.label27.TabIndex = 42;
            this.label27.Text = "Allowance Code   :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(69, 33);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 13);
            this.label28.TabIndex = 41;
            this.label28.Text = "Date  :";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.txtDate);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Location = new System.Drawing.Point(6, 62);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(627, 36);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(175, 14);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(276, 15);
            this.label33.TabIndex = 22;
            this.label33.Text = "Salary Updation";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(542, 14);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 21;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(495, 19);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(38, 13);
            this.label34.TabIndex = 20;
            this.label34.Text = "Date:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.slPanelList_Deduction);
            this.groupBox6.Location = new System.Drawing.Point(6, 515);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(629, 80);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Deductions";
            // 
            // slPanelList_Deduction
            // 
            this.slPanelList_Deduction.ConcurrentPanels = null;
            this.slPanelList_Deduction.Controls.Add(this.slTxt_PD_PAY_DATE);
            this.slPanelList_Deduction.Controls.Add(this.lookupButton_PD_DED_CODE_LOV3);
            this.slPanelList_Deduction.Controls.Add(this.txt_PD_DED_AMOUNT);
            this.slPanelList_Deduction.Controls.Add(this.txt_PD_P_NO);
            this.slPanelList_Deduction.Controls.Add(this.label29);
            this.slPanelList_Deduction.Controls.Add(this.txt_PD_DED_CODE);
            this.slPanelList_Deduction.Controls.Add(this.slDatePicker_PD_PAY_DATE);
            this.slPanelList_Deduction.Controls.Add(this.label30);
            this.slPanelList_Deduction.Controls.Add(this.label32);
            this.slPanelList_Deduction.CurrentRowIndex = 0;
            this.slPanelList_Deduction.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelList_Deduction.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelList_Deduction.DependentPanels = null;
            this.slPanelList_Deduction.DisableDependentLoad = false;
            this.slPanelList_Deduction.EnableDelete = true;
            this.slPanelList_Deduction.EnableInsert = true;
            this.slPanelList_Deduction.EnableQuery = false;
            this.slPanelList_Deduction.EnableUpdate = true;
            this.slPanelList_Deduction.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PAYROLLDEUCCommand_Salary";
            this.slPanelList_Deduction.Location = new System.Drawing.Point(10, 13);
            this.slPanelList_Deduction.MasterPanel = null;
            this.slPanelList_Deduction.Name = "slPanelList_Deduction";
            this.slPanelList_Deduction.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelList_Deduction.Size = new System.Drawing.Size(604, 61);
            this.slPanelList_Deduction.SPName = "CHRIS_SP_PAYROLL_DEDUC_MANAGER_FOR_SALARY";
            this.slPanelList_Deduction.TabIndex = 14;
            // 
            // lookupButton_PD_DED_CODE_LOV3
            // 
            this.lookupButton_PD_DED_CODE_LOV3.ActionLOVExists = "";
            this.lookupButton_PD_DED_CODE_LOV3.ActionType = "PD_DED_CODE_LOV3";
            this.lookupButton_PD_DED_CODE_LOV3.ConditionalFields = "";
            this.lookupButton_PD_DED_CODE_LOV3.CustomEnabled = true;
            this.lookupButton_PD_DED_CODE_LOV3.DataFieldMapping = "";
            this.lookupButton_PD_DED_CODE_LOV3.DependentLovControls = "";
            this.lookupButton_PD_DED_CODE_LOV3.HiddenColumns = "";
            this.lookupButton_PD_DED_CODE_LOV3.Image = ((System.Drawing.Image)(resources.GetObject("lookupButton_PD_DED_CODE_LOV3.Image")));
            this.lookupButton_PD_DED_CODE_LOV3.LoadDependentEntities = false;
            this.lookupButton_PD_DED_CODE_LOV3.Location = new System.Drawing.Point(184, 7);
            this.lookupButton_PD_DED_CODE_LOV3.LookUpTitle = null;
            this.lookupButton_PD_DED_CODE_LOV3.Name = "lookupButton_PD_DED_CODE_LOV3";
            this.lookupButton_PD_DED_CODE_LOV3.Size = new System.Drawing.Size(26, 21);
            this.lookupButton_PD_DED_CODE_LOV3.SkipValidationOnLeave = true;
            this.lookupButton_PD_DED_CODE_LOV3.SPName = "CHRIS_SP_PAYROLL_DEDUC_MANAGER_FOR_SALARY";
            this.lookupButton_PD_DED_CODE_LOV3.TabIndex = 49;
            this.lookupButton_PD_DED_CODE_LOV3.TabStop = false;
            this.lookupButton_PD_DED_CODE_LOV3.UseVisualStyleBackColor = true;
            // 
            // txt_PD_DED_AMOUNT
            // 
            this.txt_PD_DED_AMOUNT.AllowSpace = true;
            this.txt_PD_DED_AMOUNT.AssociatedLookUpName = "";
            this.txt_PD_DED_AMOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PD_DED_AMOUNT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PD_DED_AMOUNT.ContinuationTextBox = null;
            this.txt_PD_DED_AMOUNT.CustomEnabled = true;
            this.txt_PD_DED_AMOUNT.DataFieldMapping = "PD_DED_AMOUNT";
            this.txt_PD_DED_AMOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PD_DED_AMOUNT.GetRecordsOnUpDownKeys = false;
            this.txt_PD_DED_AMOUNT.IsDate = false;
            this.txt_PD_DED_AMOUNT.Location = new System.Drawing.Point(441, 8);
            this.txt_PD_DED_AMOUNT.Name = "txt_PD_DED_AMOUNT";
            this.txt_PD_DED_AMOUNT.NumberFormat = "###,###,##0.00";
            this.txt_PD_DED_AMOUNT.Postfix = "";
            this.txt_PD_DED_AMOUNT.Prefix = "";
            this.txt_PD_DED_AMOUNT.Size = new System.Drawing.Size(100, 20);
            this.txt_PD_DED_AMOUNT.SkipValidation = false;
            this.txt_PD_DED_AMOUNT.TabIndex = 2;
            this.txt_PD_DED_AMOUNT.TextType = CrplControlLibrary.TextType.String;
            this.txt_PD_DED_AMOUNT.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txt_PD_DED_AMOUNT_PreviewKeyDown);
            this.txt_PD_DED_AMOUNT.Validated += new System.EventHandler(this.txt_PD_DED_AMOUNT_Validated);
            this.txt_PD_DED_AMOUNT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_PD_DED_AMOUNT_KeyDown);
            this.txt_PD_DED_AMOUNT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_PD_DED_AMOUNT_KeyPress);
            // 
            // txt_PD_P_NO
            // 
            this.txt_PD_P_NO.AllowSpace = true;
            this.txt_PD_P_NO.AssociatedLookUpName = "";
            this.txt_PD_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PD_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PD_P_NO.ContinuationTextBox = null;
            this.txt_PD_P_NO.CustomEnabled = true;
            this.txt_PD_P_NO.DataFieldMapping = "PR_P_NO";
            this.txt_PD_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PD_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PD_P_NO.IsDate = false;
            this.txt_PD_P_NO.Location = new System.Drawing.Point(441, 32);
            this.txt_PD_P_NO.Name = "txt_PD_P_NO";
            this.txt_PD_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_PD_P_NO.Postfix = "";
            this.txt_PD_P_NO.Prefix = "";
            this.txt_PD_P_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_PD_P_NO.SkipValidation = false;
            this.txt_PD_P_NO.TabIndex = 54;
            this.txt_PD_P_NO.TabStop = false;
            this.txt_PD_P_NO.TextType = CrplControlLibrary.TextType.String;
            this.txt_PD_P_NO.Visible = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(324, 15);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(111, 13);
            this.label29.TabIndex = 53;
            this.label29.Text = "Deduction Amount";
            // 
            // txt_PD_DED_CODE
            // 
            this.txt_PD_DED_CODE.AllowSpace = true;
            this.txt_PD_DED_CODE.AssociatedLookUpName = "lookupButton_PD_DED_CODE_LOV3";
            this.txt_PD_DED_CODE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PD_DED_CODE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PD_DED_CODE.ContinuationTextBox = null;
            this.txt_PD_DED_CODE.CustomEnabled = true;
            this.txt_PD_DED_CODE.DataFieldMapping = "PD_DED_CODE";
            this.txt_PD_DED_CODE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PD_DED_CODE.GetRecordsOnUpDownKeys = false;
            this.txt_PD_DED_CODE.IsDate = false;
            this.txt_PD_DED_CODE.IsLookUpField = true;
            this.txt_PD_DED_CODE.Location = new System.Drawing.Point(123, 7);
            this.txt_PD_DED_CODE.Name = "txt_PD_DED_CODE";
            this.txt_PD_DED_CODE.NumberFormat = "###,###,##0.00";
            this.txt_PD_DED_CODE.Postfix = "";
            this.txt_PD_DED_CODE.Prefix = "";
            this.txt_PD_DED_CODE.Size = new System.Drawing.Size(55, 20);
            this.txt_PD_DED_CODE.SkipValidation = false;
            this.txt_PD_DED_CODE.TabIndex = 1;
            this.txt_PD_DED_CODE.TextType = CrplControlLibrary.TextType.String;
            // 
            // slDatePicker_PD_PAY_DATE
            // 
            this.slDatePicker_PD_PAY_DATE.CustomEnabled = true;
            this.slDatePicker_PD_PAY_DATE.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_PD_PAY_DATE.DataFieldMapping = "";
            this.slDatePicker_PD_PAY_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_PD_PAY_DATE.HasChanges = true;
            this.slDatePicker_PD_PAY_DATE.Location = new System.Drawing.Point(325, 34);
            this.slDatePicker_PD_PAY_DATE.Name = "slDatePicker_PD_PAY_DATE";
            this.slDatePicker_PD_PAY_DATE.NullValue = " ";
            this.slDatePicker_PD_PAY_DATE.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_PD_PAY_DATE.TabIndex = 52;
            this.slDatePicker_PD_PAY_DATE.TabStop = false;
            this.slDatePicker_PD_PAY_DATE.Value = new System.DateTime(2011, 1, 29, 0, 0, 0, 0);
            this.slDatePicker_PD_PAY_DATE.Visible = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(5, 9);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(114, 13);
            this.label30.TabIndex = 51;
            this.label30.Text = "Deduction Code   :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(66, 38);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(46, 13);
            this.label32.TabIndex = 50;
            this.label32.Text = "Date  :";
            // 
            // grp_MonthOvertime
            // 
            this.grp_MonthOvertime.CausesValidation = false;
            this.grp_MonthOvertime.Controls.Add(this.slPanelSimpleList_MonthOverTime);
            this.grp_MonthOvertime.Location = new System.Drawing.Point(24, 595);
            this.grp_MonthOvertime.Name = "grp_MonthOvertime";
            this.grp_MonthOvertime.Size = new System.Drawing.Size(508, 181);
            this.grp_MonthOvertime.TabIndex = 17;
            this.grp_MonthOvertime.TabStop = false;
            this.grp_MonthOvertime.Visible = false;
            // 
            // slPanelSimpleList_MonthOverTime
            // 
            this.slPanelSimpleList_MonthOverTime.ConcurrentPanels = null;
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_tempPr_p_no);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.lookup_MO_DEPT_LOV5);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.lookup_MO_SEGMENT_LOV4);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_P_NO);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_CONV_COST);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.TXT_MO_DOUBLE_COST);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.TXT_MO_DOUBLE_OT);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.TXT_MO_DEPT);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.TXT_MO_MONTH);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_MEAL_COST);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_SINGLE_COST);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_SINGLE_OT);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_SEGMENT);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely5);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely7);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely8);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely9);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely10);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely6);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely4);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely3);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely2);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely1);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_YEAR);
            this.slPanelSimpleList_MonthOverTime.CurrentRowIndex = 0;
            this.slPanelSimpleList_MonthOverTime.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelSimpleList_MonthOverTime.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimpleList_MonthOverTime.DependentPanels = null;
            this.slPanelSimpleList_MonthOverTime.DisableDependentLoad = false;
            this.slPanelSimpleList_MonthOverTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.slPanelSimpleList_MonthOverTime.EnableDelete = true;
            this.slPanelSimpleList_MonthOverTime.EnableInsert = true;
            this.slPanelSimpleList_MonthOverTime.EnableQuery = false;
            this.slPanelSimpleList_MonthOverTime.EnableUpdate = true;
            this.slPanelSimpleList_MonthOverTime.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MonthOverTimeCommand";
            this.slPanelSimpleList_MonthOverTime.Location = new System.Drawing.Point(3, 16);
            this.slPanelSimpleList_MonthOverTime.MasterPanel = null;
            this.slPanelSimpleList_MonthOverTime.Name = "slPanelSimpleList_MonthOverTime";
            this.slPanelSimpleList_MonthOverTime.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimpleList_MonthOverTime.Size = new System.Drawing.Size(502, 162);
            this.slPanelSimpleList_MonthOverTime.SPName = "CHRIS_SP_MONTH_OVERTIME_MANAGER";
            this.slPanelSimpleList_MonthOverTime.TabIndex = 9;
            // 
            // txt_tempPr_p_no
            // 
            this.txt_tempPr_p_no.AllowSpace = true;
            this.txt_tempPr_p_no.AssociatedLookUpName = "";
            this.txt_tempPr_p_no.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_tempPr_p_no.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_tempPr_p_no.ContinuationTextBox = null;
            this.txt_tempPr_p_no.CustomEnabled = true;
            this.txt_tempPr_p_no.DataFieldMapping = "PR_P_NO";
            this.txt_tempPr_p_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tempPr_p_no.GetRecordsOnUpDownKeys = false;
            this.txt_tempPr_p_no.IsDate = false;
            this.txt_tempPr_p_no.Location = new System.Drawing.Point(118, 132);
            this.txt_tempPr_p_no.Name = "txt_tempPr_p_no";
            this.txt_tempPr_p_no.NumberFormat = "###,###,##0.00";
            this.txt_tempPr_p_no.Postfix = "";
            this.txt_tempPr_p_no.Prefix = "";
            this.txt_tempPr_p_no.Size = new System.Drawing.Size(100, 20);
            this.txt_tempPr_p_no.SkipValidation = false;
            this.txt_tempPr_p_no.TabIndex = 25;
            this.txt_tempPr_p_no.TextType = CrplControlLibrary.TextType.String;
            this.txt_tempPr_p_no.Visible = false;
            // 
            // lookup_MO_DEPT_LOV5
            // 
            this.lookup_MO_DEPT_LOV5.ActionLOVExists = "MO_DEPT_LOV5_EXIST";
            this.lookup_MO_DEPT_LOV5.ActionType = "MO_DEPT_LOV5";
            this.lookup_MO_DEPT_LOV5.ConditionalFields = "";
            this.lookup_MO_DEPT_LOV5.CustomEnabled = true;
            this.lookup_MO_DEPT_LOV5.DataFieldMapping = "";
            this.lookup_MO_DEPT_LOV5.DependentLovControls = "";
            this.lookup_MO_DEPT_LOV5.HiddenColumns = "";
            this.lookup_MO_DEPT_LOV5.Image = ((System.Drawing.Image)(resources.GetObject("lookup_MO_DEPT_LOV5.Image")));
            this.lookup_MO_DEPT_LOV5.LoadDependentEntities = false;
            this.lookup_MO_DEPT_LOV5.Location = new System.Drawing.Point(435, 34);
            this.lookup_MO_DEPT_LOV5.LookUpTitle = null;
            this.lookup_MO_DEPT_LOV5.Name = "lookup_MO_DEPT_LOV5";
            this.lookup_MO_DEPT_LOV5.Size = new System.Drawing.Size(26, 21);
            this.lookup_MO_DEPT_LOV5.SkipValidationOnLeave = false;
            this.lookup_MO_DEPT_LOV5.SPName = "CHRIS_SP_MONTH_OVERTIME_MANAGER";
            this.lookup_MO_DEPT_LOV5.TabIndex = 23;
            this.lookup_MO_DEPT_LOV5.TabStop = false;
            this.lookup_MO_DEPT_LOV5.UseVisualStyleBackColor = true;
            // 
            // lookup_MO_SEGMENT_LOV4
            // 
            this.lookup_MO_SEGMENT_LOV4.ActionLOVExists = "MO_SEGMENT_LOV4_EXIST";
            this.lookup_MO_SEGMENT_LOV4.ActionType = "MO_SEGMENT_LOV4";
            this.lookup_MO_SEGMENT_LOV4.ConditionalFields = "txt_MO_P_NO";
            this.lookup_MO_SEGMENT_LOV4.CustomEnabled = true;
            this.lookup_MO_SEGMENT_LOV4.DataFieldMapping = "";
            this.lookup_MO_SEGMENT_LOV4.DependentLovControls = "";
            this.lookup_MO_SEGMENT_LOV4.HiddenColumns = "";
            this.lookup_MO_SEGMENT_LOV4.Image = ((System.Drawing.Image)(resources.GetObject("lookup_MO_SEGMENT_LOV4.Image")));
            this.lookup_MO_SEGMENT_LOV4.LoadDependentEntities = false;
            this.lookup_MO_SEGMENT_LOV4.Location = new System.Drawing.Point(186, 32);
            this.lookup_MO_SEGMENT_LOV4.LookUpTitle = null;
            this.lookup_MO_SEGMENT_LOV4.Name = "lookup_MO_SEGMENT_LOV4";
            this.lookup_MO_SEGMENT_LOV4.Size = new System.Drawing.Size(26, 21);
            this.lookup_MO_SEGMENT_LOV4.SkipValidationOnLeave = false;
            this.lookup_MO_SEGMENT_LOV4.SPName = "CHRIS_SP_MONTH_OVERTIME_MANAGER";
            this.lookup_MO_SEGMENT_LOV4.TabIndex = 22;
            this.lookup_MO_SEGMENT_LOV4.TabStop = false;
            this.lookup_MO_SEGMENT_LOV4.UseVisualStyleBackColor = true;
            // 
            // txt_MO_P_NO
            // 
            this.txt_MO_P_NO.AllowSpace = true;
            this.txt_MO_P_NO.AssociatedLookUpName = "";
            this.txt_MO_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_P_NO.ContinuationTextBox = null;
            this.txt_MO_P_NO.CustomEnabled = true;
            this.txt_MO_P_NO.DataFieldMapping = "PR_P_NO";
            this.txt_MO_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_MO_P_NO.IsDate = false;
            this.txt_MO_P_NO.Location = new System.Drawing.Point(368, 132);
            this.txt_MO_P_NO.Name = "txt_MO_P_NO";
            this.txt_MO_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_MO_P_NO.Postfix = "";
            this.txt_MO_P_NO.Prefix = "";
            this.txt_MO_P_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_MO_P_NO.SkipValidation = false;
            this.txt_MO_P_NO.TabIndex = 21;
            this.txt_MO_P_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_MO_CONV_COST
            // 
            this.txt_MO_CONV_COST.AllowSpace = true;
            this.txt_MO_CONV_COST.AssociatedLookUpName = "";
            this.txt_MO_CONV_COST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_CONV_COST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_CONV_COST.ContinuationTextBox = null;
            this.txt_MO_CONV_COST.CustomEnabled = true;
            this.txt_MO_CONV_COST.DataFieldMapping = "MO_CONV_COST";
            this.txt_MO_CONV_COST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_CONV_COST.GetRecordsOnUpDownKeys = false;
            this.txt_MO_CONV_COST.IsDate = false;
            this.txt_MO_CONV_COST.Location = new System.Drawing.Point(368, 107);
            this.txt_MO_CONV_COST.Name = "txt_MO_CONV_COST";
            this.txt_MO_CONV_COST.NumberFormat = "###,###,##0.00";
            this.txt_MO_CONV_COST.Postfix = "";
            this.txt_MO_CONV_COST.Prefix = "";
            this.txt_MO_CONV_COST.Size = new System.Drawing.Size(100, 20);
            this.txt_MO_CONV_COST.SkipValidation = false;
            this.txt_MO_CONV_COST.TabIndex = 10;
            this.txt_MO_CONV_COST.TextType = CrplControlLibrary.TextType.String;
            // 
            // TXT_MO_DOUBLE_COST
            // 
            this.TXT_MO_DOUBLE_COST.AllowSpace = true;
            this.TXT_MO_DOUBLE_COST.AssociatedLookUpName = "";
            this.TXT_MO_DOUBLE_COST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_MO_DOUBLE_COST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TXT_MO_DOUBLE_COST.ContinuationTextBox = null;
            this.TXT_MO_DOUBLE_COST.CustomEnabled = true;
            this.TXT_MO_DOUBLE_COST.DataFieldMapping = "MO_DOUBLE_COST";
            this.TXT_MO_DOUBLE_COST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_MO_DOUBLE_COST.GetRecordsOnUpDownKeys = false;
            this.TXT_MO_DOUBLE_COST.IsDate = false;
            this.TXT_MO_DOUBLE_COST.Location = new System.Drawing.Point(368, 83);
            this.TXT_MO_DOUBLE_COST.Name = "TXT_MO_DOUBLE_COST";
            this.TXT_MO_DOUBLE_COST.NumberFormat = "###,###,##0.00";
            this.TXT_MO_DOUBLE_COST.Postfix = "";
            this.TXT_MO_DOUBLE_COST.Prefix = "";
            this.TXT_MO_DOUBLE_COST.Size = new System.Drawing.Size(100, 20);
            this.TXT_MO_DOUBLE_COST.SkipValidation = false;
            this.TXT_MO_DOUBLE_COST.TabIndex = 8;
            this.TXT_MO_DOUBLE_COST.TextType = CrplControlLibrary.TextType.String;
            // 
            // TXT_MO_DOUBLE_OT
            // 
            this.TXT_MO_DOUBLE_OT.AllowSpace = true;
            this.TXT_MO_DOUBLE_OT.AssociatedLookUpName = "";
            this.TXT_MO_DOUBLE_OT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_MO_DOUBLE_OT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TXT_MO_DOUBLE_OT.ContinuationTextBox = null;
            this.TXT_MO_DOUBLE_OT.CustomEnabled = true;
            this.TXT_MO_DOUBLE_OT.DataFieldMapping = "MO_DOUBLE_OT";
            this.TXT_MO_DOUBLE_OT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_MO_DOUBLE_OT.GetRecordsOnUpDownKeys = false;
            this.TXT_MO_DOUBLE_OT.IsDate = false;
            this.TXT_MO_DOUBLE_OT.Location = new System.Drawing.Point(368, 59);
            this.TXT_MO_DOUBLE_OT.Name = "TXT_MO_DOUBLE_OT";
            this.TXT_MO_DOUBLE_OT.NumberFormat = "###,###,##0.00";
            this.TXT_MO_DOUBLE_OT.Postfix = "";
            this.TXT_MO_DOUBLE_OT.Prefix = "";
            this.TXT_MO_DOUBLE_OT.Size = new System.Drawing.Size(100, 20);
            this.TXT_MO_DOUBLE_OT.SkipValidation = false;
            this.TXT_MO_DOUBLE_OT.TabIndex = 6;
            this.TXT_MO_DOUBLE_OT.TextType = CrplControlLibrary.TextType.String;
            // 
            // TXT_MO_DEPT
            // 
            this.TXT_MO_DEPT.AllowSpace = true;
            this.TXT_MO_DEPT.AssociatedLookUpName = "lookup_MO_DEPT_LOV5";
            this.TXT_MO_DEPT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_MO_DEPT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TXT_MO_DEPT.ContinuationTextBox = null;
            this.TXT_MO_DEPT.CustomEnabled = true;
            this.TXT_MO_DEPT.DataFieldMapping = "MO_DEPT";
            this.TXT_MO_DEPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_MO_DEPT.GetRecordsOnUpDownKeys = false;
            this.TXT_MO_DEPT.IsDate = false;
            this.TXT_MO_DEPT.IsLookUpField = true;
            this.TXT_MO_DEPT.Location = new System.Drawing.Point(368, 35);
            this.TXT_MO_DEPT.Name = "TXT_MO_DEPT";
            this.TXT_MO_DEPT.NumberFormat = "###,###,##0.00";
            this.TXT_MO_DEPT.Postfix = "";
            this.TXT_MO_DEPT.Prefix = "";
            this.TXT_MO_DEPT.Size = new System.Drawing.Size(61, 20);
            this.TXT_MO_DEPT.SkipValidation = true;
            this.TXT_MO_DEPT.TabIndex = 4;
            this.TXT_MO_DEPT.TextType = CrplControlLibrary.TextType.String;
            // 
            // TXT_MO_MONTH
            // 
            this.TXT_MO_MONTH.AllowSpace = true;
            this.TXT_MO_MONTH.AssociatedLookUpName = "";
            this.TXT_MO_MONTH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_MO_MONTH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TXT_MO_MONTH.ContinuationTextBox = null;
            this.TXT_MO_MONTH.CustomEnabled = true;
            this.TXT_MO_MONTH.DataFieldMapping = "MO_MONTH";
            this.TXT_MO_MONTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_MO_MONTH.GetRecordsOnUpDownKeys = false;
            this.TXT_MO_MONTH.IsDate = false;
            this.TXT_MO_MONTH.IsRequired = true;
            this.TXT_MO_MONTH.Location = new System.Drawing.Point(368, 11);
            this.TXT_MO_MONTH.Name = "TXT_MO_MONTH";
            this.TXT_MO_MONTH.NumberFormat = "###,###,##0.00";
            this.TXT_MO_MONTH.Postfix = "";
            this.TXT_MO_MONTH.Prefix = "";
            this.TXT_MO_MONTH.Size = new System.Drawing.Size(61, 20);
            this.TXT_MO_MONTH.SkipValidation = false;
            this.TXT_MO_MONTH.TabIndex = 1;
            this.TXT_MO_MONTH.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_MO_MEAL_COST
            // 
            this.txt_MO_MEAL_COST.AllowSpace = true;
            this.txt_MO_MEAL_COST.AssociatedLookUpName = "";
            this.txt_MO_MEAL_COST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_MEAL_COST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_MEAL_COST.ContinuationTextBox = null;
            this.txt_MO_MEAL_COST.CustomEnabled = true;
            this.txt_MO_MEAL_COST.DataFieldMapping = "MO_MEAL_COST";
            this.txt_MO_MEAL_COST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_MEAL_COST.GetRecordsOnUpDownKeys = false;
            this.txt_MO_MEAL_COST.IsDate = false;
            this.txt_MO_MEAL_COST.Location = new System.Drawing.Point(118, 107);
            this.txt_MO_MEAL_COST.Name = "txt_MO_MEAL_COST";
            this.txt_MO_MEAL_COST.NumberFormat = "###,###,##0.00";
            this.txt_MO_MEAL_COST.Postfix = "";
            this.txt_MO_MEAL_COST.Prefix = "";
            this.txt_MO_MEAL_COST.Size = new System.Drawing.Size(100, 20);
            this.txt_MO_MEAL_COST.SkipValidation = false;
            this.txt_MO_MEAL_COST.TabIndex = 9;
            this.txt_MO_MEAL_COST.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_MO_SINGLE_COST
            // 
            this.txt_MO_SINGLE_COST.AllowSpace = true;
            this.txt_MO_SINGLE_COST.AssociatedLookUpName = "";
            this.txt_MO_SINGLE_COST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_SINGLE_COST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_SINGLE_COST.ContinuationTextBox = null;
            this.txt_MO_SINGLE_COST.CustomEnabled = true;
            this.txt_MO_SINGLE_COST.DataFieldMapping = "MO_SINGLE_COST";
            this.txt_MO_SINGLE_COST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_SINGLE_COST.GetRecordsOnUpDownKeys = false;
            this.txt_MO_SINGLE_COST.IsDate = false;
            this.txt_MO_SINGLE_COST.Location = new System.Drawing.Point(118, 83);
            this.txt_MO_SINGLE_COST.Name = "txt_MO_SINGLE_COST";
            this.txt_MO_SINGLE_COST.NumberFormat = "###,###,##0.00";
            this.txt_MO_SINGLE_COST.Postfix = "";
            this.txt_MO_SINGLE_COST.Prefix = "";
            this.txt_MO_SINGLE_COST.Size = new System.Drawing.Size(100, 20);
            this.txt_MO_SINGLE_COST.SkipValidation = false;
            this.txt_MO_SINGLE_COST.TabIndex = 7;
            this.txt_MO_SINGLE_COST.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_MO_SINGLE_OT
            // 
            this.txt_MO_SINGLE_OT.AllowSpace = true;
            this.txt_MO_SINGLE_OT.AssociatedLookUpName = "";
            this.txt_MO_SINGLE_OT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_SINGLE_OT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_SINGLE_OT.ContinuationTextBox = null;
            this.txt_MO_SINGLE_OT.CustomEnabled = true;
            this.txt_MO_SINGLE_OT.DataFieldMapping = "MO_SINGLE_OT";
            this.txt_MO_SINGLE_OT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_SINGLE_OT.GetRecordsOnUpDownKeys = false;
            this.txt_MO_SINGLE_OT.IsDate = false;
            this.txt_MO_SINGLE_OT.Location = new System.Drawing.Point(118, 59);
            this.txt_MO_SINGLE_OT.Name = "txt_MO_SINGLE_OT";
            this.txt_MO_SINGLE_OT.NumberFormat = "###,###,##0.00";
            this.txt_MO_SINGLE_OT.Postfix = "";
            this.txt_MO_SINGLE_OT.Prefix = "";
            this.txt_MO_SINGLE_OT.Size = new System.Drawing.Size(100, 20);
            this.txt_MO_SINGLE_OT.SkipValidation = false;
            this.txt_MO_SINGLE_OT.TabIndex = 5;
            this.txt_MO_SINGLE_OT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_MO_SEGMENT
            // 
            this.txt_MO_SEGMENT.AllowSpace = true;
            this.txt_MO_SEGMENT.AssociatedLookUpName = "lookup_MO_SEGMENT_LOV4";
            this.txt_MO_SEGMENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_SEGMENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_SEGMENT.ContinuationTextBox = null;
            this.txt_MO_SEGMENT.CustomEnabled = true;
            this.txt_MO_SEGMENT.DataFieldMapping = "MO_SEGMENT";
            this.txt_MO_SEGMENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_SEGMENT.GetRecordsOnUpDownKeys = false;
            this.txt_MO_SEGMENT.IsDate = false;
            this.txt_MO_SEGMENT.IsLookUpField = true;
            this.txt_MO_SEGMENT.Location = new System.Drawing.Point(118, 35);
            this.txt_MO_SEGMENT.Name = "txt_MO_SEGMENT";
            this.txt_MO_SEGMENT.NumberFormat = "###,###,##0.00";
            this.txt_MO_SEGMENT.Postfix = "";
            this.txt_MO_SEGMENT.Prefix = "";
            this.txt_MO_SEGMENT.Size = new System.Drawing.Size(61, 20);
            this.txt_MO_SEGMENT.SkipValidation = true;
            this.txt_MO_SEGMENT.TabIndex = 3;
            this.txt_MO_SEGMENT.TextType = CrplControlLibrary.TextType.String;
            // 
            // labely5
            // 
            this.labely5.AutoSize = true;
            this.labely5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely5.Location = new System.Drawing.Point(261, 42);
            this.labely5.Name = "labely5";
            this.labely5.Size = new System.Drawing.Size(76, 13);
            this.labely5.TabIndex = 11;
            this.labely5.Text = "Department:";
            // 
            // labely7
            // 
            this.labely7.AutoSize = true;
            this.labely7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely7.Location = new System.Drawing.Point(261, 114);
            this.labely7.Name = "labely7";
            this.labely7.Size = new System.Drawing.Size(81, 13);
            this.labely7.TabIndex = 10;
            this.labely7.Text = "Conveyance:";
            // 
            // labely8
            // 
            this.labely8.AutoSize = true;
            this.labely8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely8.Location = new System.Drawing.Point(261, 90);
            this.labely8.Name = "labely8";
            this.labely8.Size = new System.Drawing.Size(107, 13);
            this.labely8.TabIndex = 9;
            this.labely8.Text = "Double Hrs Cost :";
            // 
            // labely9
            // 
            this.labely9.AutoSize = true;
            this.labely9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely9.Location = new System.Drawing.Point(261, 66);
            this.labely9.Name = "labely9";
            this.labely9.Size = new System.Drawing.Size(82, 13);
            this.labely9.TabIndex = 8;
            this.labely9.Text = "Double Hrs. :";
            // 
            // labely10
            // 
            this.labely10.AutoSize = true;
            this.labely10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely10.Location = new System.Drawing.Point(261, 18);
            this.labely10.Name = "labely10";
            this.labely10.Size = new System.Drawing.Size(46, 13);
            this.labely10.TabIndex = 7;
            this.labely10.Text = "Month:";
            // 
            // labely6
            // 
            this.labely6.AutoSize = true;
            this.labely6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely6.Location = new System.Drawing.Point(23, 42);
            this.labely6.Name = "labely6";
            this.labely6.Size = new System.Drawing.Size(60, 13);
            this.labely6.TabIndex = 6;
            this.labely6.Text = "Segment:";
            // 
            // labely4
            // 
            this.labely4.AutoSize = true;
            this.labely4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely4.Location = new System.Drawing.Point(23, 114);
            this.labely4.Name = "labely4";
            this.labely4.Size = new System.Drawing.Size(67, 13);
            this.labely4.TabIndex = 4;
            this.labely4.Text = "Meal Cost:";
            // 
            // labely3
            // 
            this.labely3.AutoSize = true;
            this.labely3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely3.Location = new System.Drawing.Point(23, 90);
            this.labely3.Name = "labely3";
            this.labely3.Size = new System.Drawing.Size(102, 13);
            this.labely3.TabIndex = 3;
            this.labely3.Text = "Single Hrs Cost :";
            // 
            // labely2
            // 
            this.labely2.AutoSize = true;
            this.labely2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely2.Location = new System.Drawing.Point(23, 66);
            this.labely2.Name = "labely2";
            this.labely2.Size = new System.Drawing.Size(73, 13);
            this.labely2.TabIndex = 2;
            this.labely2.Text = "Single Hrs.:";
            // 
            // labely1
            // 
            this.labely1.AutoSize = true;
            this.labely1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely1.Location = new System.Drawing.Point(23, 18);
            this.labely1.Name = "labely1";
            this.labely1.Size = new System.Drawing.Size(37, 13);
            this.labely1.TabIndex = 1;
            this.labely1.Text = "Year:";
            // 
            // txt_MO_YEAR
            // 
            this.txt_MO_YEAR.AllowSpace = true;
            this.txt_MO_YEAR.AssociatedLookUpName = "";
            this.txt_MO_YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_YEAR.ContinuationTextBox = null;
            this.txt_MO_YEAR.CustomEnabled = true;
            this.txt_MO_YEAR.DataFieldMapping = "MO_YEAR";
            this.txt_MO_YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_YEAR.GetRecordsOnUpDownKeys = false;
            this.txt_MO_YEAR.IsDate = false;
            this.txt_MO_YEAR.IsRequired = true;
            this.txt_MO_YEAR.Location = new System.Drawing.Point(118, 11);
            this.txt_MO_YEAR.Name = "txt_MO_YEAR";
            this.txt_MO_YEAR.NumberFormat = "###,###,##0.00";
            this.txt_MO_YEAR.Postfix = "";
            this.txt_MO_YEAR.Prefix = "";
            this.txt_MO_YEAR.Size = new System.Drawing.Size(61, 20);
            this.txt_MO_YEAR.SkipValidation = false;
            this.txt_MO_YEAR.TabIndex = 0;
            this.txt_MO_YEAR.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTxt_PD_PAY_DATE
            // 
            this.slTxt_PD_PAY_DATE.AllowSpace = true;
            this.slTxt_PD_PAY_DATE.AssociatedLookUpName = "";
            this.slTxt_PD_PAY_DATE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxt_PD_PAY_DATE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxt_PD_PAY_DATE.ContinuationTextBox = null;
            this.slTxt_PD_PAY_DATE.CustomEnabled = true;
            this.slTxt_PD_PAY_DATE.DataFieldMapping = "PA_DATE";
            this.slTxt_PD_PAY_DATE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxt_PD_PAY_DATE.GetRecordsOnUpDownKeys = false;
            this.slTxt_PD_PAY_DATE.IsDate = false;
            this.slTxt_PD_PAY_DATE.Location = new System.Drawing.Point(123, 34);
            this.slTxt_PD_PAY_DATE.Name = "slTxt_PD_PAY_DATE";
            this.slTxt_PD_PAY_DATE.NumberFormat = "###,###,##0.00";
            this.slTxt_PD_PAY_DATE.Postfix = "";
            this.slTxt_PD_PAY_DATE.Prefix = "";
            this.slTxt_PD_PAY_DATE.ReadOnly = true;
            this.slTxt_PD_PAY_DATE.Size = new System.Drawing.Size(55, 20);
            this.slTxt_PD_PAY_DATE.SkipValidation = false;
            this.slTxt_PD_PAY_DATE.TabIndex = 55;
            this.slTxt_PD_PAY_DATE.TabStop = false;
            this.slTxt_PD_PAY_DATE.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_Payroll_SalaryUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 603);
            this.Controls.Add(this.grp_MonthOvertime);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.grpPersonnel);
            this.CurrentPanelBlock = "slPanelPersonnel";
            this.Name = "CHRIS_Payroll_SalaryUpdate";
            this.Text = "CHRIS_Payroll_SalaryUpdate";
            this.Controls.SetChildIndex(this.grpPersonnel, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.groupBox5, 0);
            this.Controls.SetChildIndex(this.groupBox6, 0);
            this.Controls.SetChildIndex(this.grp_MonthOvertime, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelPersonnel.ResumeLayout(false);
            this.slPanelPersonnel.PerformLayout();
            this.grpPersonnel.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.slPanelPayroll.ResumeLayout(false);
            this.slPanelPayroll.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.slPanelPayroll_Allow.ResumeLayout(false);
            this.slPanelPayroll_Allow.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.slPanelList_Deduction.ResumeLayout(false);
            this.slPanelList_Deduction.PerformLayout();
            this.grp_MonthOvertime.ResumeLayout(false);
            this.slPanelSimpleList_MonthOverTime.ResumeLayout(false);
            this.slPanelSimpleList_MonthOverTime.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpPersonnel;
        private System.Windows.Forms.GroupBox groupBox2;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimpleList slPanelPayroll;
        private System.Windows.Forms.GroupBox groupBox3;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimpleList slPanelPayroll_Allow;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLDatePicker slDatePicker_PR_JOINING_DATE;
        private CrplControlLibrary.SLTextBox txt_PR_P_NO;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txt_PR_FIRST_NAME;
        private CrplControlLibrary.SLTextBox txt_PR_NEW_BRANCH;
        private CrplControlLibrary.SLTextBox txt_PR_LEVEL;
        private CrplControlLibrary.SLTextBox txt_PR_DESIG;
        private CrplControlLibrary.SLTextBox txt_PR_CATEGORY;
        private CrplControlLibrary.SLTextBox txt_PR_CONF_FLAG;
        private CrplControlLibrary.SLTextBox txt_PR_MONTH_AWARD;
        private CrplControlLibrary.SLTextBox txt_PR_ANNUAL_PACK;
        private CrplControlLibrary.SLTextBox txt_PA_F_INTREST;
        private CrplControlLibrary.SLTextBox txt_PA_F_INSTALL;
        private CrplControlLibrary.SLTextBox txt_PA_PFUND;
        private CrplControlLibrary.SLTextBox txt_PA_INS_PREMIUM;
        private CrplControlLibrary.SLTextBox txt_PA_WORKED_DAYS;
        private CrplControlLibrary.SLTextBox txt_PA_HOUSE;
        private CrplControlLibrary.SLTextBox txt_PA_L_INTREST;
        private CrplControlLibrary.SLTextBox txt_PA_L_INSTALL;
        private CrplControlLibrary.SLTextBox txt_PA_CONV_OFF;
        private CrplControlLibrary.SLTextBox txt_PA_INCOME_TAX;
        private CrplControlLibrary.SLTextBox txt_PA_SAL_ADVANCE;
        private CrplControlLibrary.SLTextBox txt_PA_BASIC;
        private CrplControlLibrary.SLDatePicker slDatePicker_PA_DATE;
        private CrplControlLibrary.SLTextBox txt_PA_BRANCH;
        private CrplControlLibrary.SLTextBox txt_PA_P_NO;
        private CrplControlLibrary.SLTextBox txt_PA_FLAG;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private CrplControlLibrary.SLTextBox txt_PW_ALL_CODE;
        private CrplControlLibrary.SLDatePicker slDatePicker_PW_PAY_DATE;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label31;
        private CrplControlLibrary.SLTextBox txt_PW_ALL_AMOUNT;
        private CrplControlLibrary.SLTextBox txt_PW_P_NO;
        private CrplControlLibrary.SLDatePicker slDatePicker_PR_CONFIRM_ON;
        private CrplControlLibrary.LookupButton lookup_Pr_P_No;
        private CrplControlLibrary.LookupButton lookup_PR_CATEGORY;
        private CrplControlLibrary.LookupButton lookup_PW_ALL_CODE;
        private System.Windows.Forms.GroupBox groupBox6;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimpleList slPanelList_Deduction;
        private CrplControlLibrary.LookupButton lookupButton_PD_DED_CODE_LOV3;
        private CrplControlLibrary.SLTextBox txt_PD_DED_AMOUNT;
        private CrplControlLibrary.SLTextBox txt_PD_P_NO;
        private System.Windows.Forms.Label label29;
        private CrplControlLibrary.SLTextBox txt_PD_DED_CODE;
        private CrplControlLibrary.SLDatePicker slDatePicker_PD_PAY_DATE;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private CrplControlLibrary.LookupButton lookup_MO_DEPT_LOV5;
        private CrplControlLibrary.LookupButton lookup_MO_SEGMENT_LOV4;
        private CrplControlLibrary.SLTextBox txt_MO_P_NO;
        private CrplControlLibrary.SLTextBox txt_MO_CONV_COST;
        private CrplControlLibrary.SLTextBox TXT_MO_DOUBLE_COST;
        private CrplControlLibrary.SLTextBox TXT_MO_DOUBLE_OT;
        private CrplControlLibrary.SLTextBox TXT_MO_DEPT;
        private CrplControlLibrary.SLTextBox TXT_MO_MONTH;
        private CrplControlLibrary.SLTextBox txt_MO_MEAL_COST;
        private CrplControlLibrary.SLTextBox txt_MO_SINGLE_COST;
        private CrplControlLibrary.SLTextBox txt_MO_SINGLE_OT;
        private CrplControlLibrary.SLTextBox txt_MO_SEGMENT;
        private System.Windows.Forms.Label labely5;
        private System.Windows.Forms.Label labely7;
        private System.Windows.Forms.Label labely8;
        private System.Windows.Forms.Label labely9;
        private System.Windows.Forms.Label labely10;
        private System.Windows.Forms.Label labely6;
        private System.Windows.Forms.Label labely4;
        private System.Windows.Forms.Label labely3;
        private System.Windows.Forms.Label labely2;
        private System.Windows.Forms.Label labely1;
        private CrplControlLibrary.SLTextBox txt_MO_YEAR;
        public System.Windows.Forms.GroupBox grp_MonthOvertime;
        public iCORE.COMMON.SLCONTROLS.SLPanelSimpleList slPanelSimpleList_MonthOverTime;
        public iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelPersonnel;
        private CrplControlLibrary.SLTextBox txt_tempPr_p_no;
        private CrplControlLibrary.SLTextBox slTxt_PW_PAY_DATE;
        private CrplControlLibrary.SLTextBox slTxt_PD_PAY_DATE;
    }
}