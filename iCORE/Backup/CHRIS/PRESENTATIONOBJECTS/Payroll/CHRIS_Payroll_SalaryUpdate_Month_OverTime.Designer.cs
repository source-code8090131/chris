namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_SalaryUpdate_Month_OverTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_SalaryUpdate_Month_OverTime));
            this.slPanelSimpleList_MonthOverTime = new iCORE.COMMON.SLCONTROLS.SLPanelSimpleList(this.components);
            this.txt_tempPr_p_no = new CrplControlLibrary.SLTextBox(this.components);
            this.lookup_MO_DEPT_LOV5 = new CrplControlLibrary.LookupButton(this.components);
            this.lookup_MO_SEGMENT_LOV4 = new CrplControlLibrary.LookupButton(this.components);
            this.txt_MO_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_MO_CONV_COST = new CrplControlLibrary.SLTextBox(this.components);
            this.TXT_MO_DOUBLE_COST = new CrplControlLibrary.SLTextBox(this.components);
            this.TXT_MO_DOUBLE_OT = new CrplControlLibrary.SLTextBox(this.components);
            this.TXT_MO_DEPT = new CrplControlLibrary.SLTextBox(this.components);
            this.TXT_MO_MONTH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_MO_MEAL_COST = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_MO_SINGLE_COST = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_MO_SINGLE_OT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_MO_SEGMENT = new CrplControlLibrary.SLTextBox(this.components);
            this.labely5 = new System.Windows.Forms.Label();
            this.labely7 = new System.Windows.Forms.Label();
            this.labely8 = new System.Windows.Forms.Label();
            this.labely9 = new System.Windows.Forms.Label();
            this.labely10 = new System.Windows.Forms.Label();
            this.labely6 = new System.Windows.Forms.Label();
            this.labely4 = new System.Windows.Forms.Label();
            this.labely3 = new System.Windows.Forms.Label();
            this.labely2 = new System.Windows.Forms.Label();
            this.labely1 = new System.Windows.Forms.Label();
            this.txt_MO_YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.grp_MonthOvertime = new System.Windows.Forms.GroupBox();
            this.grpTitle = new System.Windows.Forms.GroupBox();
            this.labelTitle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelSimpleList_MonthOverTime.SuspendLayout();
            this.grp_MonthOvertime.SuspendLayout();
            this.grpTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPanelSimpleList_MonthOverTime
            // 
            this.slPanelSimpleList_MonthOverTime.ConcurrentPanels = null;
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_tempPr_p_no);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.lookup_MO_DEPT_LOV5);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.lookup_MO_SEGMENT_LOV4);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_P_NO);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_CONV_COST);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.TXT_MO_DOUBLE_COST);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.TXT_MO_DOUBLE_OT);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.TXT_MO_DEPT);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.TXT_MO_MONTH);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_MEAL_COST);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_SINGLE_COST);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_SINGLE_OT);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_SEGMENT);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely5);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely7);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely8);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely9);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely10);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely6);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely4);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely3);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely2);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.labely1);
            this.slPanelSimpleList_MonthOverTime.Controls.Add(this.txt_MO_YEAR);
            this.slPanelSimpleList_MonthOverTime.CurrentRowIndex = 0;
            this.slPanelSimpleList_MonthOverTime.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelSimpleList_MonthOverTime.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimpleList_MonthOverTime.DependentPanels = null;
            this.slPanelSimpleList_MonthOverTime.DisableDependentLoad = false;
            this.slPanelSimpleList_MonthOverTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.slPanelSimpleList_MonthOverTime.EnableDelete = true;
            this.slPanelSimpleList_MonthOverTime.EnableInsert = true;
            this.slPanelSimpleList_MonthOverTime.EnableQuery = false;
            this.slPanelSimpleList_MonthOverTime.EnableUpdate = true;
            this.slPanelSimpleList_MonthOverTime.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MonthOverTimeCommand";
            this.slPanelSimpleList_MonthOverTime.Location = new System.Drawing.Point(3, 16);
            this.slPanelSimpleList_MonthOverTime.MasterPanel = null;
            this.slPanelSimpleList_MonthOverTime.Name = "slPanelSimpleList_MonthOverTime";
            this.slPanelSimpleList_MonthOverTime.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimpleList_MonthOverTime.Size = new System.Drawing.Size(502, 162);
            this.slPanelSimpleList_MonthOverTime.SPName = "CHRIS_SP_MONTH_OVERTIME_MANAGER";
            this.slPanelSimpleList_MonthOverTime.TabIndex = 9;
            // 
            // txt_tempPr_p_no
            // 
            this.txt_tempPr_p_no.AllowSpace = true;
            this.txt_tempPr_p_no.AssociatedLookUpName = "";
            this.txt_tempPr_p_no.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_tempPr_p_no.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_tempPr_p_no.ContinuationTextBox = null;
            this.txt_tempPr_p_no.CustomEnabled = true;
            this.txt_tempPr_p_no.DataFieldMapping = "PR_P_NO";
            this.txt_tempPr_p_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tempPr_p_no.GetRecordsOnUpDownKeys = false;
            this.txt_tempPr_p_no.IsDate = false;
            this.txt_tempPr_p_no.Location = new System.Drawing.Point(118, 132);
            this.txt_tempPr_p_no.Name = "txt_tempPr_p_no";
            this.txt_tempPr_p_no.NumberFormat = "###,###,##0.00";
            this.txt_tempPr_p_no.Postfix = "";
            this.txt_tempPr_p_no.Prefix = "";
            this.txt_tempPr_p_no.Size = new System.Drawing.Size(100, 20);
            this.txt_tempPr_p_no.SkipValidation = false;
            this.txt_tempPr_p_no.TabIndex = 24;
            this.txt_tempPr_p_no.TextType = CrplControlLibrary.TextType.String;
            this.txt_tempPr_p_no.Visible = false;
            // 
            // lookup_MO_DEPT_LOV5
            // 
            this.lookup_MO_DEPT_LOV5.ActionLOVExists = "MO_DEPT_LOV5_EXIST";
            this.lookup_MO_DEPT_LOV5.ActionType = "MO_DEPT_LOV5";
            this.lookup_MO_DEPT_LOV5.ConditionalFields = "";
            this.lookup_MO_DEPT_LOV5.CustomEnabled = true;
            this.lookup_MO_DEPT_LOV5.DataFieldMapping = "";
            this.lookup_MO_DEPT_LOV5.DependentLovControls = "";
            this.lookup_MO_DEPT_LOV5.HiddenColumns = "";
            this.lookup_MO_DEPT_LOV5.Image = ((System.Drawing.Image)(resources.GetObject("lookup_MO_DEPT_LOV5.Image")));
            this.lookup_MO_DEPT_LOV5.LoadDependentEntities = false;
            this.lookup_MO_DEPT_LOV5.Location = new System.Drawing.Point(435, 34);
            this.lookup_MO_DEPT_LOV5.LookUpTitle = null;
            this.lookup_MO_DEPT_LOV5.Name = "lookup_MO_DEPT_LOV5";
            this.lookup_MO_DEPT_LOV5.Size = new System.Drawing.Size(26, 21);
            this.lookup_MO_DEPT_LOV5.SkipValidationOnLeave = false;
            this.lookup_MO_DEPT_LOV5.SPName = "CHRIS_SP_MONTH_OVERTIME_MANAGER";
            this.lookup_MO_DEPT_LOV5.TabIndex = 23;
            this.lookup_MO_DEPT_LOV5.TabStop = false;
            this.lookup_MO_DEPT_LOV5.UseVisualStyleBackColor = true;
            // 
            // lookup_MO_SEGMENT_LOV4
            // 
            this.lookup_MO_SEGMENT_LOV4.ActionLOVExists = "MO_SEGMENT_LOV4_EXIST";
            this.lookup_MO_SEGMENT_LOV4.ActionType = "MO_SEGMENT_LOV4";
            this.lookup_MO_SEGMENT_LOV4.ConditionalFields = "txt_tempPr_p_no";
            this.lookup_MO_SEGMENT_LOV4.CustomEnabled = true;
            this.lookup_MO_SEGMENT_LOV4.DataFieldMapping = "";
            this.lookup_MO_SEGMENT_LOV4.DependentLovControls = "";
            this.lookup_MO_SEGMENT_LOV4.HiddenColumns = "";
            this.lookup_MO_SEGMENT_LOV4.Image = ((System.Drawing.Image)(resources.GetObject("lookup_MO_SEGMENT_LOV4.Image")));
            this.lookup_MO_SEGMENT_LOV4.LoadDependentEntities = false;
            this.lookup_MO_SEGMENT_LOV4.Location = new System.Drawing.Point(186, 32);
            this.lookup_MO_SEGMENT_LOV4.LookUpTitle = null;
            this.lookup_MO_SEGMENT_LOV4.Name = "lookup_MO_SEGMENT_LOV4";
            this.lookup_MO_SEGMENT_LOV4.Size = new System.Drawing.Size(26, 21);
            this.lookup_MO_SEGMENT_LOV4.SkipValidationOnLeave = false;
            this.lookup_MO_SEGMENT_LOV4.SPName = "CHRIS_SP_MONTH_OVERTIME_MANAGER";
            this.lookup_MO_SEGMENT_LOV4.TabIndex = 22;
            this.lookup_MO_SEGMENT_LOV4.TabStop = false;
            this.lookup_MO_SEGMENT_LOV4.UseVisualStyleBackColor = true;
            // 
            // txt_MO_P_NO
            // 
            this.txt_MO_P_NO.AllowSpace = true;
            this.txt_MO_P_NO.AssociatedLookUpName = "";
            this.txt_MO_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_P_NO.ContinuationTextBox = null;
            this.txt_MO_P_NO.CustomEnabled = true;
            this.txt_MO_P_NO.DataFieldMapping = "PR_P_NO";
            this.txt_MO_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_MO_P_NO.IsDate = false;
            this.txt_MO_P_NO.Location = new System.Drawing.Point(368, 132);
            this.txt_MO_P_NO.Name = "txt_MO_P_NO";
            this.txt_MO_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_MO_P_NO.Postfix = "";
            this.txt_MO_P_NO.Prefix = "";
            this.txt_MO_P_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_MO_P_NO.SkipValidation = false;
            this.txt_MO_P_NO.TabIndex = 21;
            this.txt_MO_P_NO.TextType = CrplControlLibrary.TextType.String;
            this.txt_MO_P_NO.Visible = false;
            // 
            // txt_MO_CONV_COST
            // 
            this.txt_MO_CONV_COST.AllowSpace = true;
            this.txt_MO_CONV_COST.AssociatedLookUpName = "";
            this.txt_MO_CONV_COST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_CONV_COST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_CONV_COST.ContinuationTextBox = null;
            this.txt_MO_CONV_COST.CustomEnabled = true;
            this.txt_MO_CONV_COST.DataFieldMapping = "MO_CONV_COST";
            this.txt_MO_CONV_COST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_CONV_COST.GetRecordsOnUpDownKeys = false;
            this.txt_MO_CONV_COST.IsDate = false;
            this.txt_MO_CONV_COST.Location = new System.Drawing.Point(368, 107);
            this.txt_MO_CONV_COST.MaxLength = 5;
            this.txt_MO_CONV_COST.Name = "txt_MO_CONV_COST";
            this.txt_MO_CONV_COST.NumberFormat = "###,###,##0.00";
            this.txt_MO_CONV_COST.Postfix = "";
            this.txt_MO_CONV_COST.Prefix = "";
            this.txt_MO_CONV_COST.Size = new System.Drawing.Size(100, 20);
            this.txt_MO_CONV_COST.SkipValidation = false;
            this.txt_MO_CONV_COST.TabIndex = 10;
            this.txt_MO_CONV_COST.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_MO_CONV_COST.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txt_MO_CONV_COST_PreviewKeyDown);
            this.txt_MO_CONV_COST.Validated += new System.EventHandler(this.txt_MO_CONV_COST_Validated);
            this.txt_MO_CONV_COST.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_MO_CONV_COST_KeyPress);
            // 
            // TXT_MO_DOUBLE_COST
            // 
            this.TXT_MO_DOUBLE_COST.AllowSpace = true;
            this.TXT_MO_DOUBLE_COST.AssociatedLookUpName = "";
            this.TXT_MO_DOUBLE_COST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_MO_DOUBLE_COST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TXT_MO_DOUBLE_COST.ContinuationTextBox = null;
            this.TXT_MO_DOUBLE_COST.CustomEnabled = true;
            this.TXT_MO_DOUBLE_COST.DataFieldMapping = "MO_DOUBLE_COST";
            this.TXT_MO_DOUBLE_COST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_MO_DOUBLE_COST.GetRecordsOnUpDownKeys = false;
            this.TXT_MO_DOUBLE_COST.IsDate = false;
            this.TXT_MO_DOUBLE_COST.Location = new System.Drawing.Point(368, 83);
            this.TXT_MO_DOUBLE_COST.MaxLength = 6;
            this.TXT_MO_DOUBLE_COST.Name = "TXT_MO_DOUBLE_COST";
            this.TXT_MO_DOUBLE_COST.NumberFormat = "###,###,##0.00";
            this.TXT_MO_DOUBLE_COST.Postfix = "";
            this.TXT_MO_DOUBLE_COST.Prefix = "";
            this.TXT_MO_DOUBLE_COST.Size = new System.Drawing.Size(100, 20);
            this.TXT_MO_DOUBLE_COST.SkipValidation = false;
            this.TXT_MO_DOUBLE_COST.TabIndex = 8;
            this.TXT_MO_DOUBLE_COST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_MO_DOUBLE_COST.TextType = CrplControlLibrary.TextType.Double;
            // 
            // TXT_MO_DOUBLE_OT
            // 
            this.TXT_MO_DOUBLE_OT.AllowSpace = true;
            this.TXT_MO_DOUBLE_OT.AssociatedLookUpName = "";
            this.TXT_MO_DOUBLE_OT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_MO_DOUBLE_OT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TXT_MO_DOUBLE_OT.ContinuationTextBox = null;
            this.TXT_MO_DOUBLE_OT.CustomEnabled = true;
            this.TXT_MO_DOUBLE_OT.DataFieldMapping = "MO_DOUBLE_OT";
            this.TXT_MO_DOUBLE_OT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_MO_DOUBLE_OT.GetRecordsOnUpDownKeys = false;
            this.TXT_MO_DOUBLE_OT.IsDate = false;
            this.TXT_MO_DOUBLE_OT.Location = new System.Drawing.Point(368, 59);
            this.TXT_MO_DOUBLE_OT.MaxLength = 3;
            this.TXT_MO_DOUBLE_OT.Name = "TXT_MO_DOUBLE_OT";
            this.TXT_MO_DOUBLE_OT.NumberFormat = "###,###,##0.00";
            this.TXT_MO_DOUBLE_OT.Postfix = "";
            this.TXT_MO_DOUBLE_OT.Prefix = "";
            this.TXT_MO_DOUBLE_OT.Size = new System.Drawing.Size(100, 20);
            this.TXT_MO_DOUBLE_OT.SkipValidation = false;
            this.TXT_MO_DOUBLE_OT.TabIndex = 6;
            this.TXT_MO_DOUBLE_OT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_MO_DOUBLE_OT.TextType = CrplControlLibrary.TextType.Double;
            // 
            // TXT_MO_DEPT
            // 
            this.TXT_MO_DEPT.AllowSpace = true;
            this.TXT_MO_DEPT.AssociatedLookUpName = "lookup_MO_DEPT_LOV5";
            this.TXT_MO_DEPT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_MO_DEPT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TXT_MO_DEPT.ContinuationTextBox = null;
            this.TXT_MO_DEPT.CustomEnabled = true;
            this.TXT_MO_DEPT.DataFieldMapping = "MO_DEPT";
            this.TXT_MO_DEPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_MO_DEPT.GetRecordsOnUpDownKeys = false;
            this.TXT_MO_DEPT.IsDate = false;
            this.TXT_MO_DEPT.IsLookUpField = true;
            this.TXT_MO_DEPT.Location = new System.Drawing.Point(368, 35);
            this.TXT_MO_DEPT.MaxLength = 5;
            this.TXT_MO_DEPT.Name = "TXT_MO_DEPT";
            this.TXT_MO_DEPT.NumberFormat = "###,###,##0.00";
            this.TXT_MO_DEPT.Postfix = "";
            this.TXT_MO_DEPT.Prefix = "";
            this.TXT_MO_DEPT.Size = new System.Drawing.Size(61, 20);
            this.TXT_MO_DEPT.SkipValidation = true;
            this.TXT_MO_DEPT.TabIndex = 4;
            this.TXT_MO_DEPT.TextType = CrplControlLibrary.TextType.String;
            // 
            // TXT_MO_MONTH
            // 
            this.TXT_MO_MONTH.AllowSpace = true;
            this.TXT_MO_MONTH.AssociatedLookUpName = "";
            this.TXT_MO_MONTH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_MO_MONTH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TXT_MO_MONTH.ContinuationTextBox = null;
            this.TXT_MO_MONTH.CustomEnabled = true;
            this.TXT_MO_MONTH.DataFieldMapping = "MO_MONTH";
            this.TXT_MO_MONTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_MO_MONTH.GetRecordsOnUpDownKeys = false;
            this.TXT_MO_MONTH.IsDate = false;
            this.TXT_MO_MONTH.IsRequired = true;
            this.TXT_MO_MONTH.Location = new System.Drawing.Point(368, 11);
            this.TXT_MO_MONTH.MaxLength = 2;
            this.TXT_MO_MONTH.Name = "TXT_MO_MONTH";
            this.TXT_MO_MONTH.NumberFormat = "###,###,##0.00";
            this.TXT_MO_MONTH.Postfix = "";
            this.TXT_MO_MONTH.Prefix = "";
            this.TXT_MO_MONTH.Size = new System.Drawing.Size(61, 20);
            this.TXT_MO_MONTH.SkipValidation = false;
            this.TXT_MO_MONTH.TabIndex = 1;
            this.TXT_MO_MONTH.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_MO_MEAL_COST
            // 
            this.txt_MO_MEAL_COST.AllowSpace = true;
            this.txt_MO_MEAL_COST.AssociatedLookUpName = "";
            this.txt_MO_MEAL_COST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_MEAL_COST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_MEAL_COST.ContinuationTextBox = null;
            this.txt_MO_MEAL_COST.CustomEnabled = true;
            this.txt_MO_MEAL_COST.DataFieldMapping = "MO_MEAL_COST";
            this.txt_MO_MEAL_COST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_MEAL_COST.GetRecordsOnUpDownKeys = false;
            this.txt_MO_MEAL_COST.IsDate = false;
            this.txt_MO_MEAL_COST.Location = new System.Drawing.Point(118, 107);
            this.txt_MO_MEAL_COST.MaxLength = 5;
            this.txt_MO_MEAL_COST.Name = "txt_MO_MEAL_COST";
            this.txt_MO_MEAL_COST.NumberFormat = "###,###,##0.00";
            this.txt_MO_MEAL_COST.Postfix = "";
            this.txt_MO_MEAL_COST.Prefix = "";
            this.txt_MO_MEAL_COST.Size = new System.Drawing.Size(100, 20);
            this.txt_MO_MEAL_COST.SkipValidation = false;
            this.txt_MO_MEAL_COST.TabIndex = 9;
            this.txt_MO_MEAL_COST.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_MO_SINGLE_COST
            // 
            this.txt_MO_SINGLE_COST.AllowSpace = true;
            this.txt_MO_SINGLE_COST.AssociatedLookUpName = "";
            this.txt_MO_SINGLE_COST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_SINGLE_COST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_SINGLE_COST.ContinuationTextBox = null;
            this.txt_MO_SINGLE_COST.CustomEnabled = true;
            this.txt_MO_SINGLE_COST.DataFieldMapping = "MO_SINGLE_COST";
            this.txt_MO_SINGLE_COST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_SINGLE_COST.GetRecordsOnUpDownKeys = false;
            this.txt_MO_SINGLE_COST.IsDate = false;
            this.txt_MO_SINGLE_COST.Location = new System.Drawing.Point(118, 83);
            this.txt_MO_SINGLE_COST.MaxLength = 6;
            this.txt_MO_SINGLE_COST.Name = "txt_MO_SINGLE_COST";
            this.txt_MO_SINGLE_COST.NumberFormat = "###,###,##0.00";
            this.txt_MO_SINGLE_COST.Postfix = "";
            this.txt_MO_SINGLE_COST.Prefix = "";
            this.txt_MO_SINGLE_COST.Size = new System.Drawing.Size(100, 20);
            this.txt_MO_SINGLE_COST.SkipValidation = false;
            this.txt_MO_SINGLE_COST.TabIndex = 7;
            this.txt_MO_SINGLE_COST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_MO_SINGLE_COST.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_MO_SINGLE_OT
            // 
            this.txt_MO_SINGLE_OT.AllowSpace = true;
            this.txt_MO_SINGLE_OT.AssociatedLookUpName = "";
            this.txt_MO_SINGLE_OT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_SINGLE_OT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_SINGLE_OT.ContinuationTextBox = null;
            this.txt_MO_SINGLE_OT.CustomEnabled = true;
            this.txt_MO_SINGLE_OT.DataFieldMapping = "MO_SINGLE_OT";
            this.txt_MO_SINGLE_OT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_SINGLE_OT.GetRecordsOnUpDownKeys = false;
            this.txt_MO_SINGLE_OT.IsDate = false;
            this.txt_MO_SINGLE_OT.Location = new System.Drawing.Point(118, 59);
            this.txt_MO_SINGLE_OT.MaxLength = 3;
            this.txt_MO_SINGLE_OT.Name = "txt_MO_SINGLE_OT";
            this.txt_MO_SINGLE_OT.NumberFormat = "###,###,##0.00";
            this.txt_MO_SINGLE_OT.Postfix = "";
            this.txt_MO_SINGLE_OT.Prefix = "";
            this.txt_MO_SINGLE_OT.Size = new System.Drawing.Size(100, 20);
            this.txt_MO_SINGLE_OT.SkipValidation = false;
            this.txt_MO_SINGLE_OT.TabIndex = 5;
            this.txt_MO_SINGLE_OT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_MO_SINGLE_OT.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_MO_SEGMENT
            // 
            this.txt_MO_SEGMENT.AllowSpace = true;
            this.txt_MO_SEGMENT.AssociatedLookUpName = "lookup_MO_SEGMENT_LOV4";
            this.txt_MO_SEGMENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_SEGMENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_SEGMENT.ContinuationTextBox = null;
            this.txt_MO_SEGMENT.CustomEnabled = true;
            this.txt_MO_SEGMENT.DataFieldMapping = "MO_SEGMENT";
            this.txt_MO_SEGMENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_SEGMENT.GetRecordsOnUpDownKeys = false;
            this.txt_MO_SEGMENT.IsDate = false;
            this.txt_MO_SEGMENT.IsLookUpField = true;
            this.txt_MO_SEGMENT.Location = new System.Drawing.Point(118, 35);
            this.txt_MO_SEGMENT.Name = "txt_MO_SEGMENT";
            this.txt_MO_SEGMENT.NumberFormat = "###,###,##0.00";
            this.txt_MO_SEGMENT.Postfix = "";
            this.txt_MO_SEGMENT.Prefix = "";
            this.txt_MO_SEGMENT.Size = new System.Drawing.Size(61, 20);
            this.txt_MO_SEGMENT.SkipValidation = true;
            this.txt_MO_SEGMENT.TabIndex = 3;
            this.txt_MO_SEGMENT.TextType = CrplControlLibrary.TextType.String;
            // 
            // labely5
            // 
            this.labely5.AutoSize = true;
            this.labely5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely5.Location = new System.Drawing.Point(261, 42);
            this.labely5.Name = "labely5";
            this.labely5.Size = new System.Drawing.Size(76, 13);
            this.labely5.TabIndex = 11;
            this.labely5.Text = "Department:";
            // 
            // labely7
            // 
            this.labely7.AutoSize = true;
            this.labely7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely7.Location = new System.Drawing.Point(261, 114);
            this.labely7.Name = "labely7";
            this.labely7.Size = new System.Drawing.Size(81, 13);
            this.labely7.TabIndex = 10;
            this.labely7.Text = "Conveyance:";
            // 
            // labely8
            // 
            this.labely8.AutoSize = true;
            this.labely8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely8.Location = new System.Drawing.Point(261, 90);
            this.labely8.Name = "labely8";
            this.labely8.Size = new System.Drawing.Size(107, 13);
            this.labely8.TabIndex = 9;
            this.labely8.Text = "Double Hrs Cost :";
            // 
            // labely9
            // 
            this.labely9.AutoSize = true;
            this.labely9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely9.Location = new System.Drawing.Point(261, 66);
            this.labely9.Name = "labely9";
            this.labely9.Size = new System.Drawing.Size(82, 13);
            this.labely9.TabIndex = 8;
            this.labely9.Text = "Double Hrs. :";
            // 
            // labely10
            // 
            this.labely10.AutoSize = true;
            this.labely10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely10.Location = new System.Drawing.Point(261, 18);
            this.labely10.Name = "labely10";
            this.labely10.Size = new System.Drawing.Size(46, 13);
            this.labely10.TabIndex = 7;
            this.labely10.Text = "Month:";
            // 
            // labely6
            // 
            this.labely6.AutoSize = true;
            this.labely6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely6.Location = new System.Drawing.Point(23, 42);
            this.labely6.Name = "labely6";
            this.labely6.Size = new System.Drawing.Size(60, 13);
            this.labely6.TabIndex = 6;
            this.labely6.Text = "Segment:";
            // 
            // labely4
            // 
            this.labely4.AutoSize = true;
            this.labely4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely4.Location = new System.Drawing.Point(23, 114);
            this.labely4.Name = "labely4";
            this.labely4.Size = new System.Drawing.Size(67, 13);
            this.labely4.TabIndex = 4;
            this.labely4.Text = "Meal Cost:";
            // 
            // labely3
            // 
            this.labely3.AutoSize = true;
            this.labely3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely3.Location = new System.Drawing.Point(23, 90);
            this.labely3.Name = "labely3";
            this.labely3.Size = new System.Drawing.Size(102, 13);
            this.labely3.TabIndex = 3;
            this.labely3.Text = "Single Hrs Cost :";
            // 
            // labely2
            // 
            this.labely2.AutoSize = true;
            this.labely2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely2.Location = new System.Drawing.Point(23, 66);
            this.labely2.Name = "labely2";
            this.labely2.Size = new System.Drawing.Size(73, 13);
            this.labely2.TabIndex = 2;
            this.labely2.Text = "Single Hrs.:";
            // 
            // labely1
            // 
            this.labely1.AutoSize = true;
            this.labely1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labely1.Location = new System.Drawing.Point(23, 18);
            this.labely1.Name = "labely1";
            this.labely1.Size = new System.Drawing.Size(37, 13);
            this.labely1.TabIndex = 1;
            this.labely1.Text = "Year:";
            // 
            // txt_MO_YEAR
            // 
            this.txt_MO_YEAR.AllowSpace = true;
            this.txt_MO_YEAR.AssociatedLookUpName = "";
            this.txt_MO_YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_MO_YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_MO_YEAR.ContinuationTextBox = null;
            this.txt_MO_YEAR.CustomEnabled = true;
            this.txt_MO_YEAR.DataFieldMapping = "MO_YEAR";
            this.txt_MO_YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MO_YEAR.GetRecordsOnUpDownKeys = false;
            this.txt_MO_YEAR.IsDate = false;
            this.txt_MO_YEAR.IsRequired = true;
            this.txt_MO_YEAR.Location = new System.Drawing.Point(118, 11);
            this.txt_MO_YEAR.MaxLength = 4;
            this.txt_MO_YEAR.Name = "txt_MO_YEAR";
            this.txt_MO_YEAR.NumberFormat = "###,###,##0.00";
            this.txt_MO_YEAR.Postfix = "";
            this.txt_MO_YEAR.Prefix = "";
            this.txt_MO_YEAR.Size = new System.Drawing.Size(61, 20);
            this.txt_MO_YEAR.SkipValidation = false;
            this.txt_MO_YEAR.TabIndex = 0;
            this.txt_MO_YEAR.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // grp_MonthOvertime
            // 
            this.grp_MonthOvertime.Controls.Add(this.slPanelSimpleList_MonthOverTime);
            this.grp_MonthOvertime.Location = new System.Drawing.Point(6, 93);
            this.grp_MonthOvertime.Name = "grp_MonthOvertime";
            this.grp_MonthOvertime.Size = new System.Drawing.Size(508, 181);
            this.grp_MonthOvertime.TabIndex = 0;
            this.grp_MonthOvertime.TabStop = false;
            // 
            // grpTitle
            // 
            this.grpTitle.Controls.Add(this.labelTitle);
            this.grpTitle.Location = new System.Drawing.Point(9, 48);
            this.grpTitle.Name = "grpTitle";
            this.grpTitle.Size = new System.Drawing.Size(502, 45);
            this.grpTitle.TabIndex = 9;
            this.grpTitle.TabStop = false;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(188, 20);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(118, 15);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "Monthly Overtime";
            // 
            // CHRIS_Payroll_SalaryUpdate_Month_OverTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 285);
            this.Controls.Add(this.grpTitle);
            this.Controls.Add(this.grp_MonthOvertime);
            this.CurrentPanelBlock = "slPanelSimpleList_MonthOverTime";
            this.Name = "CHRIS_Payroll_SalaryUpdate_Month_OverTime";
            this.Text = "CHRIS_Payroll_SalaryUpdate_Month_OverTime";
            this.Controls.SetChildIndex(this.grp_MonthOvertime, 0);
            this.Controls.SetChildIndex(this.grpTitle, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelSimpleList_MonthOverTime.ResumeLayout(false);
            this.slPanelSimpleList_MonthOverTime.PerformLayout();
            this.grp_MonthOvertime.ResumeLayout(false);
            this.grpTitle.ResumeLayout(false);
            this.grpTitle.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimpleList slPanelSimpleList_MonthOverTime;
        private System.Windows.Forms.GroupBox grp_MonthOvertime;
        private System.Windows.Forms.Label labely6;
        private System.Windows.Forms.Label labely4;
        private System.Windows.Forms.Label labely3;
        private System.Windows.Forms.Label labely2;
        private System.Windows.Forms.Label labely1;
        private CrplControlLibrary.SLTextBox txt_MO_YEAR;
        private CrplControlLibrary.SLTextBox txt_MO_CONV_COST;
        private CrplControlLibrary.SLTextBox TXT_MO_DOUBLE_COST;
        private CrplControlLibrary.SLTextBox TXT_MO_DOUBLE_OT;
        private CrplControlLibrary.SLTextBox TXT_MO_DEPT;
        private CrplControlLibrary.SLTextBox TXT_MO_MONTH;
        private CrplControlLibrary.SLTextBox txt_MO_MEAL_COST;
        private CrplControlLibrary.SLTextBox txt_MO_SINGLE_COST;
        private CrplControlLibrary.SLTextBox txt_MO_SINGLE_OT;
        private CrplControlLibrary.SLTextBox txt_MO_SEGMENT;
        private System.Windows.Forms.Label labely5;
        private System.Windows.Forms.Label labely7;
        private System.Windows.Forms.Label labely8;
        private System.Windows.Forms.Label labely9;
        private System.Windows.Forms.Label labely10;
        private CrplControlLibrary.SLTextBox txt_MO_P_NO;
        private CrplControlLibrary.LookupButton lookup_MO_SEGMENT_LOV4;
        private CrplControlLibrary.LookupButton lookup_MO_DEPT_LOV5;
        private System.Windows.Forms.GroupBox grpTitle;
        private System.Windows.Forms.Label labelTitle;
        private CrplControlLibrary.SLTextBox txt_tempPr_p_no;
    }
}