using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_LFAEntry : ChrisSimpleForm
    {

        #region Declarations
        /*---------------------------To view records on another form----------------------------*/
        CHRIS_Payroll_LFAEntry_ViewRecord objCHRIS_Payroll_LFAEntry_ViewRecord;
        /*-----------------------------To store data on form level------------------------------*/
        Dictionary<string, object> dicLFA = new Dictionary<string, object>();
        /*----------------------To store input parameters of StoredProcedure--------------------*/
        Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
        /*------------------------------For custom call in database---------------------------- */
        CmnDataManager objCmnDataManager;
        float tirper = 0;
        string val="";
        string newval="";
      
        #endregion

        #region Constructor
        public CHRIS_Payroll_LFAEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Payroll_LFAEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }
        #endregion

        #region override Methods

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //this.tbtList.Visible = false;
            //this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            //lblUserName.Text = lblUserName.Text + this.UserName;
            this.txtUser.Text = this.UserName;
            //this.CurrentPanelBlock = "slPnlLeaveFairAssistance";
          
        }

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                this.FunctionConfig.EnableF8 = false;
                this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");  //System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                lblUserName.Text = lblUserName.Text + " " + this.UserName;
                this.txtUser.Text = this.userID;
                this.CurrentPanelBlock = "slPnlLeaveFairAssistance";

                this.tbtAdd.Visible = false;
                this.tbtList.Visible = false;
                this.tbtCancel.Visible = true;
                this.tbtSave.Visible = false;
                this.tbtDelete.Visible = false;

                InitializeDictionaryLFA();

                this.txtOption.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }     
        
        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override bool Add()
        {
            base.Add();

            this.txtPersonnelNo.Focus();

            dicLFA["W_DIS"] = "ADD";
            dicLFA["W_OPTION"] = "A";

            this.txtOption.Text = "A";
            this.txtOptionView.Text = "ADD";
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");  //System.DateTime.Now.Date.ToString("dd/MM/yyyy");

            this.tbtSave.Visible = false;
            this.txtUser.Text = this.userID;


            return false;
    
        }


       
        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override bool Edit()
        {
            base.Edit();

            dicLFA["W_DIS"] = "MODIFY";
            dicLFA["W_OPTION"] = "M";

            this.txtOption.Text = "M";
            this.txtOptionView.Text = "MODIFY";
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");  //System.DateTime.Now.Date.ToString("dd/MM/yyyy");

            this.tbtSave.Visible = false;
           
            return false;

        }

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override bool View()
        {
            base.View();

            dicLFA["W_DIS"] = "VIEW";
            dicLFA["W_OPTION"] = "V";

            this.txtOption.Text = "V";
            this.txtOptionView.Text = "VIEW";
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");  //System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            
            this.tbtSave.Visible = false;

            CountRecords();

            if (Convert.ToInt32(dicLFA["BLK_HEAD_W_CNT"]) > 0)
            {
                objCHRIS_Payroll_LFAEntry_ViewRecord = new CHRIS_Payroll_LFAEntry_ViewRecord();
                objCHRIS_Payroll_LFAEntry_ViewRecord.ShowDialog();
            }
            else
            {
                MessageBox.Show("NO RECORD FOUND");
            }

            this.txtOption.Focus();
            return false;

        }

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override bool Delete()
        {
            base.Delete();

            dicLFA["W_DIS"] = "DELETE";
            dicLFA["W_OPTION"] = "D";

            this.txtOption.Text = "D";
            this.txtOptionView.Text = "DELETE";
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy"); //System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            
            this.tbtSave.Visible = false;
           
            

            return false;

        }

        #endregion

        #region Events

        /// <summary>
        /// check personnel exist and perform specified operation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPersonnelNo_Leave(object sender, EventArgs e)
        {

             try
            {
                if (this.txtPersonnelNo.Text != string.Empty)
                {
                    if (CheckPersonnelExist())
                    {
                        PerformOption();
                    }
                    else
                    {
                        MessageBox.Show("NO DATA FOR THIS Per No.PRESS <F6> Exit W/O Save");
                        this.txtPersonnelNo.Focus();
                        this.txtPersonnelNo.Text = "";
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// calculate Leave amount 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtLeaveBalance_Leave(object sender, EventArgs e)
        {

            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;


            try
            {
                if (this.txtLeaveBalance.Text != "")
                {
                    decimal var = 0;
                    var = Convert.ToDecimal(txtLeaveBalance.Text);
                    txtLeaveBalanceHidden.Text = Convert.ToString(System.Math.Round(var,0));
                    
                    if ((Convert.ToDecimal(this.txtLeaveBalance.Text) < 0) || (Convert.ToDecimal(this.txtLeaveBalance.Text) > 100))
                    {
                        MessageBox.Show("INVALID VALUE RE-ENTER");
                        txtLeaveBalance.Text = "";
                        txtLeaveBalance.Focus();
                    }
                    else
                    {
                        dicLFA["LFA_TAB_LF_LEAV_BAL"] = Convert.ToInt32(this.txtLeaveBalanceHidden.Text); //Convert.ToDecimal(this.txtLeaveBalance.Text);
                        //dicLFA["LFA_TAB_LF_LEAV_BAL"] = Convert.ToDecimal(this.txtLeaveBalance.Text);
                        dicLFA["BLK_HEAD_W_YSY"] = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                        CalculateLeaveAmount();
                    }
                }
                else
                {
                    if (txtPersonnelNo.Text != "")
                    {
                        MessageBox.Show("VALUE HAS TO BE ENTERED");
                        txtLeaveBalance.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Check Leave Approve status 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtApproved_TextChanged(object sender, EventArgs e)
        {

          

        }

        /// <summary>
        /// Show LFA previous record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnView_Click(object sender, EventArgs e)
        {

            if (txtOption.Text != "V" )
            {
                return;
            }
            
            try
            {

                Reset();
                txtOption.Focus();


                CountRecords();
                if (Convert.ToInt32(dicLFA["BLK_HEAD_W_CNT"]) > 0)
                {
                    objCHRIS_Payroll_LFAEntry_ViewRecord = new CHRIS_Payroll_LFAEntry_ViewRecord();
                    objCHRIS_Payroll_LFAEntry_ViewRecord.ShowDialog();
                }
                else
                {
                    MessageBox.Show("NO RECORD FOUND");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Check Leave Approve status 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtApproved_Leave(object sender, EventArgs e)
        {

            if (txtPersonnelNo.Text != "")
            {
                if (txtApproved.Text == "")
                {
                    MessageBox.Show("Enter [Y/N] <F6> Exit W/O Save");
                    txtApproved.Focus();
                    return;
                }


                if (txtApproved.Text != "Y" && txtApproved.Text != "N" )
                {
                    MessageBox.Show("Enter [Y/N]  <F6> Exit W/O Save");
                    txtApproved.Focus();
                    return;
                }
                else if (txtApproved.Text == "N")
                {
                    Reset();
                    return;
                }



            }

            
            
            if ((this.txtApproved.Text.ToUpper() == "Y") || (this.txtApproved.Text.ToUpper() == "N"))
            {
                if ((this.FunctionConfig.CurrentOption == Function.Add) || (this.FunctionConfig.CurrentOption == Function.Modify))
                {
                    DialogResult dRes = MessageBox.Show("DO YOU WANT TO SAVE THE CHANGES [Y]es [N]o ", "Note"
                                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes == DialogResult.Yes)
                    {


                        //((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).LF_LEAV_BAL11 = Convert.ToInt32(txtLeaveBalanceHidden.Text);
                        //((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).LF_LEAV_BAL = Convert.ToInt32(txtLeaveBalanceHidden.Text);
                        //((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).LF_P_NO = Convert.ToInt32(txtPersonnelNo.Text);
                        //((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).LF_LFA_AMOUNT = Convert.ToDecimal(txtLFAamount.Text);
                        //((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).LF_APPROVED = Convert.ToString(txtApproved.Text);
                        ////((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).LF_DATE = Convert.ToDateTime(txtDateofGeneration.Text.ToString("dd/MM/yyyy") ); 
                       // ((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).ID = Convert.ToInt32(rslt.dstResult.Tables[0].Rows[0]["ID"]);

                        base.DoToolbarActions(this.Controls, "Save");
                        Reset();
                        //call save
                        return;

                    }
                    else if (dRes == DialogResult.No)
                    {
                        Reset();
                        txtOption.Focus();

                        return;
                    }
                }
            }
        }

        #endregion

        #region User define methods

        /// <summary>
        /// Initialize block label variables in dictionary
        /// </summary>
        /// 
        private void InitializeDictionaryLFA()
        {

            dicLFA.Clear();

            //-----BLK_HEAD---------16 ITEMS
            dicLFA.Add("BLK_HEAD_W_OPTION", null);
            dicLFA.Add("BLK_HEAD_W_CNT", null);
            dicLFA.Add("BLK_HEAD_W_CATEGORY", null);
            dicLFA.Add("BLK_HEAD_W_VAL1", null);
            dicLFA.Add("BLK_HEAD_W_VAL2", null);
            dicLFA.Add("BLK_HEAD_W_BASIC", null);
            dicLFA.Add("BLK_HEAD_W_SUM", null);
            dicLFA.Add("BLK_HEAD_W_YEAR", null);
            dicLFA.Add("BLK_HEAD_W_FLAG", null);
            dicLFA.Add("BLK_HEAD_W_YSY", null);
            dicLFA.Add("BLK_HEAD_W_ANSWER", null);
            dicLFA.Add("BLK_HEAD_W_ANSWER3", null);
            dicLFA.Add("BLK_HEAD_W_TOTAL", null);
            dicLFA.Add("BLK_HEAD_W_DIS", null);
            dicLFA.Add("BLK_HEAD_W_USER", null);
            dicLFA.Add("BLK_HEAD_W_LOC", null);

            //-------BLK_P-----------4 ITEMS
            dicLFA.Add("BLK_P_PR_P_NO", null);
            dicLFA.Add("BLK_P_PR_NEW_BRANCH", null);
            dicLFA.Add("BLK_P_PR_FIRST_NAME", null);
            dicLFA.Add("BLK_P_W_DATE", null);

            //-----LFA_TAB---------5 ITEMS
            dicLFA.Add("LFA_TAB_LF_P_NO", null);
            dicLFA.Add("LFA_TAB_LF_DATE", null);
            dicLFA.Add("LFA_TAB_LF_LEAV_BAL", null);
            //dicLFA.Add("LFA_TAB_LF_LEAV_BAL11",null);
            dicLFA.Add("LFA_TAB_LF_LFA_AMOUNT", null);
            dicLFA.Add("LFA_TAB_LF_APPROVED", null);

            //------PER-----------2 ITEMS
            dicLFA.Add("PER_PR_P_NO", null);
            dicLFA.Add("PER_PR_FIRST_NAME", null);

            //-----LFA_TAB1-------5 ITEMS
            dicLFA.Add("LFA_TAB1_LF_P_NO", null);
            dicLFA.Add("LFA_TAB1_LEAV_BAL", null);
            //dicLFA.Add("LFA_TAB1_LEAV_BAL11", null);
            dicLFA.Add("LFA_TAB1_LFA_AMOUNT", null);
            dicLFA.Add("LFA_TAB1_LF_APPROVED", null);
            dicLFA.Add("LFA_TAB1_W_NAME", null);
        }

        /// <summary>
        /// check personnel exist 
        /// </summary>
        /// <returns>
        /// return true if personnel exist
        /// return false if personnel not exist
        /// </returns>
        bool CheckPersonnelExist()
        {
            bool isPersonnelExist = false;

            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("inp_pr_p_no", (object)this.txtPersonnelNo.Text);

                objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "PersonnelExist", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            dicLFA["BLK_P_PR_P_NO"] = Convert.ToString(rslt.dstResult.Tables[0].Rows[0]["pr_p_no"]);
                            dicLFA["BLK_P_PR_FIRST_NAME"] = Convert.ToString(rslt.dstResult.Tables[0].Rows[0]["pr_name"]);
                            dicLFA["BLK_P_PR_NEW_BRANCH"] = Convert.ToString(rslt.dstResult.Tables[0].Rows[0]["pr_new_branch"]);
                            dicLFA["BLK_HEAD_W_CATEGORY"] = Convert.ToString(rslt.dstResult.Tables[0].Rows[0]["pr_category"]);

                            //((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).PR_P_NO = Convert.ToDouble(rslt.dstResult.Tables[0].Rows[0]["pr_p_no"]);
                            //((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).PR_NAME = Convert.ToString(rslt.dstResult.Tables[0].Rows[0]["pr_name"]);
                            //((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).PR_CATEGORY = Convert.ToString(rslt.dstResult.Tables[0].Rows[0]["pr_category"]);
                            //((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).PR_BRANCH = Convert.ToString(rslt.dstResult.Tables[0].Rows[0]["pr_new_branch"]);
                            //((LFA_TABCommand)(this.slPnlLeaveFairAssistance.CurrentBusinessEntity)).ID = Convert.ToInt32(rslt.dstResult.Tables[0].Rows[0]["ID"]);

                            this.txtPersonnelName.Text = Convert.ToString(dicLFA["BLK_P_PR_FIRST_NAME"]);

                            isPersonnelExist = true;
                        }
                        
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return isPersonnelExist;
        }

        /// <summary>
        /// Perform specified operation
        /// </summary>
        void PerformOption()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("inp_pr_p_no", (object)this.txtPersonnelNo.Text);

                Result rsltLFA = new Result();
                objCmnDataManager = new CmnDataManager();

                if (this.txtOption.Text == "A")
                {
                    #region Insertion

                    dicLFA["BLK_P_W_DATE"] = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                    this.txtDate.Text = this.Now().ToString("dd/MM/yyyy"); //System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                    
                    rsltLFA = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "Check_Insertion", dicInputParameters);
                   
                    if (rsltLFA.isSuccessful)
                    { 
                        if (rsltLFA.dstResult.Tables.Count > 0)
                        {
                            if (rsltLFA.dstResult.Tables[0].Rows.Count > 0)
                            {
                                MessageBox.Show("VALUE IS ALREADY THERE FOR THE CURRENT MONTH");
                                this.txtPersonnelNo.Text = "";
                                this.txtPersonnelName.Text = "";
                                this.txtPersonnelNo.Focus();
                            }
                            else
                            {
                                dicLFA["LFA_TAB_LF_DATE"] = System.DateTime.Now.Date.ToString("dd/MM/yyyy"); //this.Now().ToString("MM/dd/yyyy");
                                this.txtDateofGeneration.Text = this.Now().ToString("dd/MM/yyyy"); //System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                                this.txtLeaveBalance.Focus();
                            }
                        }
                    } 
                    #endregion
                }
                else if ((this.txtOption.Text == "M") || (this.txtOption.Text == "D"))
                {
                    #region Modification/Deletion
             
                    rsltLFA = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "Check_ModificationDeletion", dicInputParameters);
                    if (rsltLFA.isSuccessful)
                    {
                        if (rsltLFA.dstResult.Tables.Count > 0)
                        {
                            if (rsltLFA.dstResult.Tables[0].Rows.Count > 0)
                            {
                                if (this.FunctionConfig.CurrentOption == Function.Delete)
                                {

                                    this.txtLeaveBalance.Text = Convert.ToString(rsltLFA.dstResult.Tables[0].Rows[0]["LF_LEAV_BAL"]);
                                    this.txtLFAamount.Text = Convert.ToString(rsltLFA.dstResult.Tables[0].Rows[0]["LF_LFA_AMOUNT"]);
                                    this.txtApproved.Text = Convert.ToString(rsltLFA.dstResult.Tables[0].Rows[0]["LF_APPROVED"]);
                                    DateTime dtLFdate = DateTime.Parse(Convert.ToString(rsltLFA.dstResult.Tables[0].Rows[0]["LF_DATE"]));
                                    this.txtDateofGeneration.Text = dtLFdate.ToString("dd/MM/yyyy");
                                    
                                    
                                    base.DoToolbarActions(this.Controls, "Delete");
                                    this.txtOption.Text = "";
                                    this.txtOption.Focus();
                                }
                                else if (this.FunctionConfig.CurrentOption == Function.Modify)
                                {
                                    this.txtLeaveBalance.Text = Convert.ToString(rsltLFA.dstResult.Tables[0].Rows[0]["LF_LEAV_BAL"]);
                                    this.txtLFAamount.Text = Convert.ToString(rsltLFA.dstResult.Tables[0].Rows[0]["LF_LFA_AMOUNT"]);
                                    this.txtApproved.Text = Convert.ToString(rsltLFA.dstResult.Tables[0].Rows[0]["LF_APPROVED"]);
                                    DateTime dtLFdate = DateTime.Parse(Convert.ToString(rsltLFA.dstResult.Tables[0].Rows[0]["LF_DATE"]) );
                                    this.txtDateofGeneration.Text = dtLFdate.ToString("dd/MM/yyyy");
                                    
                                    this.txtLeaveBalance.Focus();
                                    this.txtPersonnelNo.ReadOnly = true;
                                    this.txtPersonnelName.ReadOnly = true;
                                    //this.operationMode = Mode.Edit;
                                    //base.DoToolbarActions(this.Controls, "Save");
                                }

                                #region commented code
                                //if (rsltLFA.dstResult.Tables[0].Rows[0]["NoofRecords"] != null)
                                //{
                                //    dicLFA["BLK_HEAD_W_TOTAL"] = rsltLFA.dstResult.Tables[0].Rows[0]["NoofRecords"];
                                //    if (Convert.ToInt32(rsltLFA.dstResult.Tables[0].Rows[0]["NoofRecords"]) > 0)
                                //    { 
                                //        if (this.FunctionConfig.CurrentOption == Function.Delete)
                                //        {
                                //            base.DoToolbarActions(this.Controls, "Delete");
                                //        }
                                //        else if (this.FunctionConfig.CurrentOption == Function.Modify)
                                //        {
                                            
                                //        }
                                //    }
                                //    else
                                //    {
                                //        dicLFA["BLK_HEAD_W_TOTAL"] = 0;
                                //        MessageBox.Show("NO RECORD IS THERE TO BE MODIFIED");
                                //        this.txtPersonnelNo.Text = "";
                                //        this.txtPersonnelName.Text = "";
                                //        this.txtPersonnelNo.Focus();
                                //    }
                                //}

                                #endregion
                                
                            }
                            else
                            {
                                dicLFA["BLK_HEAD_W_TOTAL"] = 0;
                                if (txtOption.Text == "M")
                                {
                                    MessageBox.Show("NO RECORD IS THERE TO BE MODIFIED");
                                }
                                else if (txtOption.Text == "D")
                                {
                                    MessageBox.Show("PLEASE ACKNOWLEDGE......");
                                }
                                                        
                                this.txtPersonnelNo.Text = "";
                                this.txtPersonnelName.Text = "";
                                this.txtPersonnelNo.Focus();
                            }  
                           
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Count Records in LFA_TAB table
        /// </summary>
        void CountRecords()
        {
            Result rsltLFA = new Result();
            objCmnDataManager = new CmnDataManager();

            try
            {
                rsltLFA = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "Count_Records");

                if (rsltLFA.isSuccessful)
                {
                    if (rsltLFA.dstResult.Tables.Count > 0)
                    {
                        if (rsltLFA.dstResult.Tables[0].Rows.Count > 0)
                        {
                            if (rsltLFA.dstResult.Tables[0].Rows[0]["NoofRecords"] != null)
                            {
                                dicLFA["BLK_HEAD_W_CNT"] = rsltLFA.dstResult.Tables[0].Rows[0]["NoofRecords"];                               
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate leave amount
        /// </summary>
        void CalculateLeaveAmount()
        {
            
            Result rsltLFA = new Result();
            objCmnDataManager = new CmnDataManager();

            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("inp_pr_new_branch", dicLFA["BLK_P_PR_NEW_BRANCH"]);

                rsltLFA = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "CalculateLeaveAmount", dicInputParameters);

                if (rsltLFA.isSuccessful)
                {
                    if (rsltLFA.dstResult.Tables.Count > 0)
                    {
                        if (rsltLFA.dstResult.Tables[0].Rows.Count > 0)
                        {
                            #region exist
                            if (rsltLFA.dstResult.Tables[0].Rows[0]["SP_ALL_AMOUNT"] != null)
                            {
                                dicLFA["BLK_HEAD_W_VAL1"] = Convert.ToString(rsltLFA.dstResult.Tables[0].Rows[0]["SP_ALL_AMOUNT"]);
                                //Double LFAamount = Convert.ToDouble(dicLFA["LFA_TAB_LF_LEAV_BAL"]) * ((Convert.ToDouble(dicLFA["BLK_HEAD_W_VAL1"]) / 30));
                                Double LFAamount = Convert.ToDouble(txtLeaveBalance.Text) * ((Convert.ToDouble(dicLFA["BLK_HEAD_W_VAL1"]) / 30));
                                LFAamount = System.Math.Round(LFAamount, 2);
                                dicLFA["LFA_TAB_LF_LFA_AMOUNT"] = LFAamount;
                                this.txtLFAamount.Text = Convert.ToString(dicLFA["LFA_TAB_LF_LFA_AMOUNT"]);
                            }
                            #endregion
                        }
                        else
                        {
                            MessageBox.Show("PLEASE ENTER LFA AMOUNT IN ALLOWANCE ENTRY ...!");
                            txtLeaveBalance.Focus();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Reset form controls
        /// </summary>
        void Reset()
        {
            this.txtPersonnelNo.Text = "";
            this.txtPersonnelNo.ReadOnly = false;
            this.txtPersonnelName.Text = "";
            this.txtLeaveBalance.Text = "";
            this.txtLeaveBalanceHidden.Text = "";
            this.txtLFAamount.Text = "";
            this.txtDateofGeneration.Text = ""; 
            this.txtApproved.Text = "";
            this.txtOption.Text = "";
            this.txtOption.Focus();
        }

        #endregion

        private void txtLeaveBalance_Validating(object sender, CancelEventArgs e)
        {
            double leaveBln = 0;
            if(double.TryParse(txtLeaveBalance.Text,out leaveBln) && leaveBln>=100)//if (Convert.ToDecimal(txtLeaveBalance.Text) >= 100 )
            {
                txtLeaveBalance.Text = "";
                txtLFAamount.Text = "";
                e.Cancel = true;

                return;

            }
           

            bool ValidTIRDec = IsValidExpression(txtLeaveBalance.Text, InputValidator.TWODECIMAL_REGEX);
            bool ValidTIRINT = IsValidExpression(txtLeaveBalance.Text, InputValidator.NUMBER_REGEX);
            bool ValidTIRONEDEC = IsValidExpression(txtLeaveBalance.Text, InputValidator.ONEDECIMAL_REGEX);
            if (ValidTIRINT)
            {
                val = txtLeaveBalance.Text;
                newval = val + ".00";
               // txtLeaveBalance.Text = newval;
                return;
            }

            if (ValidTIRONEDEC)
            {

                val = txtLeaveBalance.Text;
                newval = val + "0";
                //txtLeaveBalance.Text = newval;
                return;

            }
            //-----------------------------------------------------------//



        }

        private void txtPersonnelNo_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode ==Keys.F6 )
            {
                e.Handled = false;
                this.Cancel();
                return;
            }


        }

        private void txtLeaveBalance_KeyDown(object sender, KeyEventArgs e)
        {
            if ( e.KeyCode == Keys.F6 )
            {
                e.Handled = false;
                this.Cancel();
                return;
            }

        }

        private void txtLeaveBalance_KeyPress(object sender, KeyPressEventArgs e)
        {
          
        }

        private void txtLeaveBalance_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDateofGeneration_TextChanged(object sender, EventArgs e)
        {

        }

        private void CHRIS_Payroll_LFAEntry_Shown(object sender, EventArgs e)
        {
            this.slPnlLeaveFairAssistance.ResetText();
            this.slPnlLeaveFairAssistance.ClearBusinessEntity();
            this.ClearForm(slPnlLeaveFairAssistance.Controls);
            

            //objLFA.txtPersonnelNo.Text = string.Empty;
            //objLFA.txtPersonnelName.Text = string.Empty;
            //objLFA.txtLeaveBalance.Text = string.Empty;
            //objLFA.txtLFAamount.Text = string.Empty;
            //objLFA.txtApproved.Text = string.Empty;
            //objLFA.txtDateofGeneration.Text = string.Empty;
            this.txtOption.Focus();
                
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.F3)
            {
                this.Quit();
            }
        }
      
           
    }
}