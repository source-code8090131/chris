using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using CrplControlLibrary;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_ShiftEntry_GridForm : MasterDetailForm//ChrisTabularForm
    {

        #region Feilds

        public string pr_p_no;
        public int month;
        public int year;
        public int w_level_comp = 850;
        public DateTime Ot_Date = new DateTime();
        public string w_cate = string.Empty;


        CmnDataManager cmnDM = new CmnDataManager();
        Dictionary<string, object> paramValues = new Dictionary<string, object>();
        Result rslt;

        CHRIS_Payroll_ShiftEntry ObjShiftEntry = null;
        public const string TIMEFORMAT_REGEX = "^([0-1][0-9]|[2][0-3]):([0-5][0-9])|(99:99)$";
        public const string TIMEFORMAT_24HOURS = "^([0-1][0-9]|[2][0-3]):([0-5][0-9])$";

        //************Variables for Shift Calcutations**************
        string ShiftVal = string.Empty;
        string TimeInVal = string.Empty;


        //*************Shift TimeIn TimeOutPoPupValues**************
        string W_I1 = "08:00";
        string W_O1 = "16:00";

        string W_I2 = "15:30";
        string W_O2 = "23:30";

        string W_I3 = "23:00";
        string W_O3 = "07:00";
        //**************************Proc8************************************
        string w_fri_hol = string.Empty;
        string w_time_out = string.Empty;
        double w_in_hrs = 0;
        double w_out_hrs = 0;
        double w_ot_hrs = 0;
        double w_single_hr = 0;
        double w_double_hr = 0;
        int w_meal_5 = 0;
        int w_conv_5 = 0;
        string w_meal_c5 = string.Empty;
        string w_conv_c5 = string.Empty;

        //**************************Proc5************************************
        int w_meal = 0;
        int w_conv = 0;
        double w_meal_amt;
        double w_conv_amt;
        string w_conv_code = string.Empty;
        string w_meal_code = string.Empty;
        //***********************Proc9***************************************
        string w_desig = string.Empty;
        string w_category = string.Empty;
        string w_branch = string.Empty;
        DateTime w_promotion;

        //**********************Proc10***************************************
        string w_code = string.Empty;
        double w_amount = 0.0;

        #endregion

        #region Constructors

        public CHRIS_Payroll_ShiftEntry_GridForm()
        {
            InitializeComponent();
        }

        public CHRIS_Payroll_ShiftEntry_GridForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Payroll_ShiftEntry ObjShift)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(slPanelShift1);

            slPanelPersonnel.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(slPanelPersonnel);
            ObjShiftEntry = ObjShift;
            ObjShiftEntry.Hide();
        }

        #endregion

        #region Overridden Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //this.txtOption.Visible = false;
            this.tbtAdd.Visible = false;
            this.tbtDelete.Visible = false;
            this.tbtEdit.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtList.Visible = false;
            this.tbtCancel.Visible = false;

            //this.tlbMain.Items["tbtSearch"].Visible = false;
            FillGrids();
            //this.dgvShiftOT1.Columns["OT_TIME_IN"].DefaultCellStyle.Format = "HH:mm";
            this.dgvShiftOT1.Columns["OT_FRI_HOL"].DefaultCellStyle.DataSourceNullValue = "N";
            this.dgvShiftOT1.Columns["OT_FRI_HOL"].DefaultCellStyle.NullValue = "N";
            (this.dgvShiftOT1.DataSource as DataTable).Columns["OT_FRI_HOL"].DefaultValue = "N";



        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                if (this.dgvShiftOT1.EndEdit())
                {
                    if (MessageBox.Show("Do You Want To Give Attendance Award [Y]es or [N]o?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (MessageBox.Show("Do You Want To Save The Above Information Y/N?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            paramValues.Clear();
                            paramValues.Add("OT_P_NO", pr_p_no);
                            rslt = cmnDM.GetData("CHRIS_SP_SHIFT_OVERTIME_MANAGER", "Save_KeyNextItem", paramValues);
                            if (rslt.isSuccessful)
                            { }
                          
                        }
                        base.DoToolbarActions(ctrlsCollection, actionType);
                        paramValues.Clear();
                        paramValues.Add("OT_DATE_FILTER", Ot_Date);
                        paramValues.Add("OT_P_NO", pr_p_no);
                        rslt = cmnDM.GetData("CHRIS_SP_SHIFT_ENTRY_PROC11", "", paramValues);
                        if (rslt.isSuccessful)
                        {
                            this.Close();
                        }
                    }
                }
            }
        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            this.dgvShiftOT1.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            this.dgvShiftOT1.RowTemplate.Height = 18;
            this.dgvShiftOT1.AllowUserToResizeRows = false;
            this.dgvShiftOT1.AllowUserToAddRows = false;
            this.dgvShiftOT1.AllowUserToDeleteRows = false;

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            ObjShiftEntry.Show();
            base.OnFormClosing(e);
        }

        #endregion

        #region Methods

        private void FillGrids()
        {

            if (!string.IsNullOrEmpty(this.pr_p_no))
            {

                paramValues.Clear();
                paramValues.Add("OT_P_NO", pr_p_no);
                paramValues.Add("OT_DATE_FILTER", Ot_Date);
                rslt = cmnDM.GetData("CHRIS_SP_PERSONNEL_MANAGER_SHIFTENTRY", "List", paramValues);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        base.SetPanelControlWithListItem(slPanelPersonnel.Controls, rslt.dstResult.Tables[0].Rows[0]);
                        slPanelPersonnel.SetEntityObject(rslt.dstResult.Tables[0].Rows[0]);
                        this.slPanelPersonnel.LoadDependentPanels();
                        this.dgvShiftOT1.AllowUserToResizeRows = false;
                        this.dgvShiftOT1.AllowUserToAddRows = false;
                        this.dgvShiftOT1.AllowUserToDeleteRows = false;
                        //this.dgvShiftOT1.CurrentCell=this.dgvShiftOT1[this.dgvShiftOT1.FindFirstVisibleColumn(), 0];
                        //this.dgvShiftOT1[this.dgvShiftOT1.FindFirstVisibleColumn(), 0].Selected = true;
                        //this.dgvShiftOT1.Focus();

                    }
                }
            }

        }

        private void Proc6()
        {
            if (ShiftVal == string.Empty)
                Proc5(TimeInVal);
            else if (ShiftVal == "A")
                Proc5(W_I1);
            else if (ShiftVal == "B")
                Proc5(W_I2);
            else if (ShiftVal == "C")
                Proc5(W_I3);

        }

        /// <summary>
        /// Proc5 is called from Proc6.
        /// </summary>
        /// <param name="w_time_in"></param>
        private void Proc5(string w_time_in)
        {
            string w_fri_hol = this.dgvShiftOT1.CurrentRow.Cells["OT_FRI_HOL"].Value.ToString();
            string w_time_out = this.dgvShiftOT1.CurrentRow.Cells["OT_TIME_OUT"].Value.ToString();

            w_in_hrs = 0;
            w_out_hrs = 0;
            w_ot_hrs = 0;
            w_single_hr = 0;
            w_double_hr = 0;
            w_meal_5 = 0;
            w_conv_5 = 0;
            w_meal_c5 = string.Empty;
            w_conv_c5 = string.Empty;

            if (w_fri_hol.ToUpper() == "N")
                w_in_hrs = (double.Parse(w_time_in.Substring(0, 2)) + (double.Parse(w_time_in.Substring(3, 2)) / 60) + 7.333);
            else if (w_fri_hol.ToUpper() == "H")
                w_in_hrs = (double.Parse(w_time_in.Substring(0, 2)) + (double.Parse(w_time_in.Substring(3, 2)) / 60));

            if (int.Parse(w_time_out.Substring(0, 2)) < 12)
                w_out_hrs = (double.Parse(w_time_out.Substring(0, 2)) + (double.Parse(w_time_out.Substring(3, 2)) / 60) + 24);
            else
                w_out_hrs = (double.Parse(w_time_out.Substring(0, 2)) + (double.Parse(w_time_out.Substring(3, 2)) / 60));

            w_ot_hrs = w_out_hrs - w_in_hrs;

            if (w_ot_hrs > 2.666)
            {
                w_single_hr = 2.666;
                w_double_hr = w_ot_hrs - 2.666;
            }
            else
            {
                w_single_hr = w_ot_hrs;
                w_double_hr = 0;
            }
            if (w_fri_hol.ToUpper() == "H")
            {
                w_double_hr = w_double_hr + w_single_hr;
                w_single_hr = 0;
            }
            if (w_single_hr < 0)
                w_single_hr = 0;
            if (w_double_hr < 0)
                w_double_hr = 0;

            Proc8();

            //this.dgvShiftOT1.CurrentRow.Cells["OT_SINGLE_HR"].Value = w_single_hr;
            //this.dgvShiftOT1.CurrentRow.Cells["OT_DOUBLE_HR"].Value = w_double_hr;
            //this.dgvShiftOT1.CurrentRow.Cells["OT_MEAL_CODE"].Value = w_meal_c5;
            //this.dgvShiftOT1.CurrentRow.Cells["OT_MEAL_AMT"].Value = w_meal_5;
            //this.dgvShiftOT1.CurrentRow.Cells["OT_CONV_CODE"].Value = w_conv_c5;
            //this.dgvShiftOT1.CurrentRow.Cells["OT_CONV_AMT"].Value = w_conv_5;
        }

        /// <summary>
        /// Proc8 is called from Proc5.
        /// </summary>
        private void Proc8()
        {
            if (w_fri_hol.ToUpper() == "N")
            {
                if (w_single_hr >= 2.666)
                {
                    w_meal = 1;
                    w_conv = 0;
                    w_conv_code = string.Empty; //:= NULL;
                }
                if ((w_single_hr + w_double_hr) >= 4.666)
                {
                    w_meal = 1;
                    w_conv = 1;
                }
            }
            else if (w_fri_hol.ToUpper() == "H")
            {
                w_conv = 1;
                if ((w_single_hr + w_double_hr) >= 5.5)
                    w_meal = 1;

                if ((w_single_hr + w_double_hr) > 10)
                    w_meal = w_meal + 1;
            }

            Proc9();
            //Proc10();
            //Proc10();
            //w_meal_amt = w_meal_amt * w_meal;
            //w_conv_amt = w_conv_amt * w_conv;

            //if (w_meal == 0)
            //    w_meal_code = string.Empty;

            //if (w_conv == 0)
            //    w_conv_code = string.Empty;

        }

        private void Proc9()
        {
            paramValues.Clear();
            paramValues.Add("OT_P_NO", pr_p_no);
            rslt = cmnDM.GetData("CHRIS_SP_SHIFT_OVERTIME_MANAGER", "Proc9", paramValues);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    w_desig = rslt.dstResult.Tables[0].Rows[0]["PR_DESIG"].ToString();
                    w_category = rslt.dstResult.Tables[0].Rows[0]["PR_CATEGORY"].ToString();
                    w_branch = rslt.dstResult.Tables[0].Rows[0]["PR_BRANCH"].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0]["PR_PROMOTION_DATE"] != DBNull.Value)
                    {
                        DateTime.TryParse(rslt.dstResult.Tables[0].Rows[0]["PR_PROMOTION_DATE"].ToString(), out w_promotion);

                        paramValues.Clear();
                        paramValues.Add("OT_P_NO", pr_p_no);
                        paramValues.Add("PR_PROMOTION_DATE", rslt.dstResult.Tables[0].Rows[0]["PR_PROMOTION_DATE"]);
                        rslt = cmnDM.GetData("CHRIS_SP_PERSONNEL_MANAGER_SHIFTENTRY", "Desig_Promotion", paramValues);
                        if (rslt.isSuccessful)
                        {
                            if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                if (rslt.dstResult.Tables[0].Rows[0]["PR_DESIG"] != DBNull.Value)
                                    w_desig = rslt.dstResult.Tables[0].Rows[0]["PR_DESIG"].ToString();
                            }
                        }

                    }
                }
            }
        }

        private void Proc10()
        {

        }

        private void Proc12()
        {

        }

        private void Proc14()
        {

        }
        /// <summary>
        /// Proc11 is PostCommit PG.
        /// </summary>
        private void Proc11()
        {

        }


        #endregion

        #region Events

        private void dgvShiftOT1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex > 1 && e.RowIndex >= 0)
            {
                #region TimeIn
                if (e.ColumnIndex == this.dgvShiftOT1.Columns["OT_TIME_IN"].Index)
                {
                    if (this.dgvShiftOT1.CurrentCell.IsInEditMode)
                    {
                        if (!string.IsNullOrEmpty(this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString()))
                        {
                            bool isValid = false;
                            isValid = IsValidExpression(this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString(), TIMEFORMAT_REGEX);
                            if (!isValid)
                            {
                                MessageBox.Show("<<<<<  Invalid Time Entered >>>>>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                e.Cancel = true;
                            }

                        }
                        else
                        {
                            this.dgvShiftOT1.CurrentCell.Value = "99:99";
                            this.dgvShiftOT1.RefreshEdit();
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(this.dgvShiftOT1.CurrentCell.Value.ToString()))
                        {

                            this.dgvShiftOT1.CurrentCell.Value = "99:99";
                            this.dgvShiftOT1.RefreshEdit();

                        }

                    }

                }

                #endregion

                #region TimeOut
                else if (e.ColumnIndex == this.dgvShiftOT1.Columns["OT_TIME_OUT"].Index /*&& this.dgvShiftOT1.CurrentCell.IsInEditMode*/)
                {
                    if (this.dgvShiftOT1.CurrentCell.IsInEditMode)
                    {
                        //*******************************If TimeOutValue is Not Null********************************************
                        if (!string.IsNullOrEmpty(this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString()))
                        {
                            string TimeInVal = this.dgvShiftOT1["OT_TIME_IN", e.RowIndex].Value.ToString();
                            bool isValid = false;
                            isValid = IsValidExpression(this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString(), TIMEFORMAT_REGEX);
                            if (!isValid)
                            {
                                MessageBox.Show("<<<<<  Invalid Time Entered >>>>>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                e.Cancel = true;
                            }
                            else //********************************If TimeOut Values if Parsed*********************************
                            {
                                if (TimeInVal.Contains("99:99"))
                                {
                                    this.dgvShiftOT1.CurrentCell.Value = "99:99";
                                    this.dgvShiftOT1.RefreshEdit();
                                }
                                else
                                {
                                    if (this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString().Contains("99:99"))
                                    {
                                        MessageBox.Show("Invalid Time Because Valid Time In Is Given", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        e.Cancel = true;
                                    }
                                }
                            }
                        }
                        else //*********************************If TimeOutValue is  Null****************************************
                        {
                            string TimeInValue = this.dgvShiftOT1["OT_TIME_IN", e.RowIndex].Value.ToString();
                            if (TimeInValue.Contains("99:99"))
                            {
                                this.dgvShiftOT1.CurrentCell.Value = "99:99";
                                this.dgvShiftOT1.RefreshEdit();
                            }
                            else
                            {
                                MessageBox.Show("Invalid Time Because Valid Time In Is Given", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                e.Cancel = true;

                            }
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(this.dgvShiftOT1.CurrentCell.Value.ToString()))
                        {
                            string TimeInValue = this.dgvShiftOT1["OT_TIME_IN", e.RowIndex].Value.ToString();
                            if (IsValidExpression(TimeInValue, TIMEFORMAT_REGEX))
                            {
                                if (TimeInValue.Contains("99:99"))
                                {
                                    this.dgvShiftOT1.CurrentCell.Value = "99:99";
                                    this.dgvShiftOT1.RefreshEdit();
                                }
                                else
                                {
                                    MessageBox.Show("Invalid Time Because Valid Time In Is Given", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    e.Cancel = true;
                                    this.dgvShiftOT1.BeginEdit(false);
                                }
                            }
                        }

                    }
                }
                #endregion

                #region OT_FRI_HOLIDAY
                if (e.ColumnIndex == this.dgvShiftOT1.Columns["OT_FRI_HOL"].Index)
                {
                    if (this.dgvShiftOT1.CurrentCell.IsInEditMode)
                    {
                        if (string.IsNullOrEmpty(this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString()))
                        {
                            this.dgvShiftOT1.CurrentCell.Value = "N";
                            this.dgvShiftOT1.RefreshEdit();
                        }
                        else
                        {
                            if (this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString().ToUpper() != "N" && this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString().ToUpper() != "H")
                            {
                                this.dgvShiftOT1.CurrentCell.Value = "N";
                                this.dgvShiftOT1.RefreshEdit();
                            }
                            if (this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString().ToUpper() == "H")
                            {
                                if (w_cate.ToUpper() != "C")
                                {
                                    w_level_comp = 850;
                                    frmInput input = new frmInput("Enter Leave Compensation", CrplControlLibrary.TextType.Integer);
                                    input.TextBox.MaxLength = 4;
                                    input.TextBox.Text = w_level_comp.ToString();
                                    input.TextBox.TextAlign = HorizontalAlignment.Right;
                                    input.TextBox.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
                                    input.TextBox.PreviewKeyDown += new PreviewKeyDownEventHandler(TextBox_PreviewKeyDown);
                                    input.ShowDialog(this);
                                    int tempCompentationvalue = 0;
                                    int.TryParse(input.Value.Trim(), out tempCompentationvalue);
                                    //if (tempCompentationvalue != 0)
                                        w_level_comp = tempCompentationvalue;
                                }
                                else
                                {
                                    w_level_comp = 0;
                                }


                            }

                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(this.dgvShiftOT1.CurrentCell.Value.ToString()))
                        {
                            this.dgvShiftOT1.CurrentCell.Value = "N";
                        }
                    }
                }

                #endregion

                #region Shift
                if (e.ColumnIndex == this.dgvShiftOT1.Columns["F_SHIFT"].Index)
                {
                    ShiftVal = string.Empty;
                    if (this.dgvShiftOT1.CurrentCell.IsInEditMode)
                    {
                        if (!string.IsNullOrEmpty(this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString()))
                        {
                            ShiftVal = this.dgvShiftOT1.CurrentCell.EditedFormattedValue.ToString().ToUpper().Trim();

                            if (ShiftVal != string.Empty && ShiftVal.ToUpper() != "A" && ShiftVal.ToUpper() != "B" && ShiftVal.ToUpper() != "C" && ShiftVal.ToUpper() != "D")
                            {
                                MessageBox.Show("Valid Shifts Are [A],[B],[C],[D] or Blank For No Shift", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                e.Cancel = true;
                                return;
                            }
                            TimeInVal = string.Empty;
                            TimeInVal = this.dgvShiftOT1.CurrentRow.Cells["OT_TIME_IN"].Value.ToString();
                            if (TimeInVal != string.Empty && !TimeInVal.Equals("99:99"))
                            {
                                Proc6();
                                //*****************Proc10 raises form_trigger_failure ,thts why its implemented in validating event.**************
                                paramValues.Clear();
                                paramValues.Add("SP_MEAL_CONV", "M");
                                paramValues.Add("SP_BRANCH_A", w_branch);
                                rslt = cmnDM.GetData("CHRIS_Sp_ALLOWANCE_MANAGER", "Proc10", paramValues);
                                if (rslt.isSuccessful)
                                {
                                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        w_meal_code = rslt.dstResult.Tables[0].Rows[0][0].ToString();
                                        w_meal_amt = double.Parse(rslt.dstResult.Tables[0].Rows[0][1].ToString());
                                    }
                                    else
                                    {
                                        MessageBox.Show("Please Check Allowance Code For Meal & Conveyance", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        e.Cancel = true;
                                        return;
                                    }
                                }
                                paramValues.Clear();
                                paramValues.Add("SP_MEAL_CONV", "C");
                                paramValues.Add("SP_BRANCH_A", w_branch);
                                rslt = cmnDM.GetData("CHRIS_Sp_ALLOWANCE_MANAGER", "Proc10", paramValues);
                                if (rslt.isSuccessful)
                                {
                                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        w_conv_code = rslt.dstResult.Tables[0].Rows[0][0].ToString();
                                        w_conv_amt = double.Parse(rslt.dstResult.Tables[0].Rows[0][1].ToString());
                                    }
                                    else
                                    {
                                        MessageBox.Show("Please Check Allowance Code For Meal & Conveyance", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        e.Cancel = true;
                                        return;
                                    }
                                }
                                //******************************************code of proc8 after proc10 call ****************************************
                                w_meal_amt = w_meal_amt * w_meal;
                                w_conv_amt = w_conv_amt * w_conv;

                                if (w_meal == 0)
                                    w_meal_code = string.Empty;

                                if (w_conv == 0)
                                    w_conv_code = string.Empty;
                                //******************************************code of proc5 after proc8 call ****************************************
                                this.dgvShiftOT1.CurrentRow.Cells["OT_SINGLE_HR"].Value = w_single_hr;
                                this.dgvShiftOT1.CurrentRow.Cells["OT_DOUBLE_HR"].Value = w_double_hr;
                                this.dgvShiftOT1.CurrentRow.Cells["OT_MEAL_CODE"].Value = w_meal_c5;
                                this.dgvShiftOT1.CurrentRow.Cells["OT_MEAL_AMT"].Value = w_meal_5;
                                this.dgvShiftOT1.CurrentRow.Cells["OT_CONV_CODE"].Value = w_conv_c5;
                                this.dgvShiftOT1.CurrentRow.Cells["OT_CONV_AMT"].Value = w_conv_5;

                                //******************************************Proc12()************************************************************
                                if (ShiftVal.Trim() != string.Empty && ShiftVal.ToUpper() != "D")
                                {
                                    paramValues.Clear();
                                    paramValues.Add("SP_ALL_CODE", ShiftVal.Trim());
                                    paramValues.Add("SP_BRANCH_A", w_branch);
                                    rslt = cmnDM.GetData("CHRIS_Sp_ALLOWANCE_MANAGER", "Proc12", paramValues);
                                    if (rslt.isSuccessful)
                                    {
                                        if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            double.TryParse(rslt.dstResult.Tables[0].Rows[0]["w_amount"].ToString(), out w_amount);
                                        }
                                        else
                                        {
                                            MessageBox.Show("UPDATE ALLOWANCE TABLE FOR SHIFT  " + ShiftVal, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            e.Cancel = true;
                                            return;
                                        }
                                    }

                                }


                            }
                            if (TimeInVal.Equals("99:99") || w_cate.ToUpper() != "C")
                            {
                                this.dgvShiftOT1.CurrentRow.Cells["OT_SINGLE_HR"].Value = 0;
                                this.dgvShiftOT1.CurrentRow.Cells["OT_DOUBLE_HR"].Value = 0;
                                this.dgvShiftOT1.CurrentRow.Cells["OT_MEAL_CODE"].Value = DBNull.Value;
                                this.dgvShiftOT1.CurrentRow.Cells["OT_MEAL_AMT"].Value = 0;
                                this.dgvShiftOT1.CurrentRow.Cells["OT_CONV_CODE"].Value = DBNull.Value;
                                this.dgvShiftOT1.CurrentRow.Cells["OT_CONV_AMT"].Value = 0;
                            }
                            this.dgvShiftOT1.CurrentRow.Cells["SH_AMOUNT"].Value = w_amount + w_level_comp;
                        }
                        else
                        {
                            if (w_cate.ToUpper() != "C" && this.dgvShiftOT1.CurrentRow.Cells["OT_FRI_HOL"].Value.ToString().ToUpper() == "H")
                            {
                                MessageBox.Show("Please Enter [D] Code For Leave Compensation", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                e.Cancel = true;
                                return;
                            }

                        }

                    }
                    else
                    {

                    }

                }

                #endregion

            }

        }

        void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            SLTextBox txt = (SLTextBox)sender;
            string text = e.KeyChar.ToString();
            try
            {
                //if (txt.Text.Trim() != string.Empty)
                //{
                //    int temp = 0;

                //    if (Int32.TryParse(txt.Text, out temp))
                //    {
                //        MessageBox.Show("Please Enter a Value.........!");
                //        return;
                //    }
                //}

                if (e.KeyChar == '\r' || e.KeyChar == '\t')
                {
                    if (txt.Text.Trim() != string.Empty)
                    {
                        int temp = 0;

                        if (!Int32.TryParse(txt.Text, out temp))
                        {
                            MessageBox.Show("Please Enter a Value.........!");
                            return;
                        }
                        else
                            txt.FindForm().Close();
                    }
                    else
                    {
                        MessageBox.Show("Please Enter a Value.........!");
                        return;
                    }
                    
                }
            }
            catch (Exception ee)
            {
                base.LogError("TextBox_KeyPress", ee);
            }

        }

        void TextBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                e.IsInputKey = true;
            }
            //throw new Exception("The method or operation is not implemented.");
        }

        private void dgvShiftOT1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                if (e.Control is TextBox)
                {
                    ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
                    if (this.dgvShiftOT1.CurrentCell != null)
                    {
                        if (this.dgvShiftOT1.CurrentCell.OwningColumn.Index == this.dgvShiftOT1.Columns["OT_TIME_IN"].Index)
                        {
                            TextBox tb = e.Control as TextBox;
                            tb.KeyDown -= new KeyEventHandler(tb_KeyDown);
                            tb.KeyDown += new KeyEventHandler(tb_KeyDown);


                        }
                    }
                }
            }

        }

        void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F9)
            {
                CHRIS_Payroll_ShiftEntry_ShiftTime ObjShiftTimings = new CHRIS_Payroll_ShiftEntry_ShiftTime();
                ObjShiftTimings.WIOne = W_I1;
                ObjShiftTimings.WOOne = W_O1;

                ObjShiftTimings.WITwo = W_I2;
                ObjShiftTimings.WOTwo = W_O2;

                ObjShiftTimings.WIThree = W_I3;
                ObjShiftTimings.WOThree = W_O3;

                ObjShiftTimings.ShowDialog();

                W_I1 = ObjShiftTimings.WIOne;
                W_O1 = ObjShiftTimings.WOOne;

                W_I2 = ObjShiftTimings.WITwo;
                W_O2 = ObjShiftTimings.WOTwo;

                W_I3 = ObjShiftTimings.WIThree;
                W_O3 = ObjShiftTimings.WOThree;
                //MessageBox.Show("open");
            }
        }



        #endregion
    }
}