namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_SalaryAdvanceEntryGrid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_SalaryAdvanceEntryGrid));
            this.slPanelTabular1 = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.slDataGridView1 = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colPNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colM1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colM2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmnt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelTabular1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(472, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(508, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 386);
            this.panel1.Size = new System.Drawing.Size(508, 60);
            // 
            // slPanelTabular1
            // 
            this.slPanelTabular1.AutoSize = true;
            this.slPanelTabular1.ConcurrentPanels = null;
            this.slPanelTabular1.Controls.Add(this.slDataGridView1);
            this.slPanelTabular1.DataManager = null;
            this.slPanelTabular1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelTabular1.DependentPanels = null;
            this.slPanelTabular1.DisableDependentLoad = false;
            this.slPanelTabular1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.slPanelTabular1.EnableDelete = true;
            this.slPanelTabular1.EnableInsert = true;
            this.slPanelTabular1.EnableUpdate = true;
            this.slPanelTabular1.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SalaryAdvanceCommand";
            this.slPanelTabular1.Location = new System.Drawing.Point(0, 36);
            this.slPanelTabular1.MasterPanel = null;
            this.slPanelTabular1.Name = "slPanelTabular1";
            this.slPanelTabular1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelTabular1.Size = new System.Drawing.Size(508, 350);
            this.slPanelTabular1.SPName = "CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER";
            this.slPanelTabular1.TabIndex = 0;
            // 
            // slDataGridView1
            // 
            this.slDataGridView1.AllowUserToAddRows = false;
            this.slDataGridView1.AllowUserToDeleteRows = false;
            this.slDataGridView1.AllowUserToResizeColumns = false;
            this.slDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slDataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPNo,
            this.colName,
            this.colM1,
            this.colM2,
            this.colAmnt});
            this.slDataGridView1.ColumnToHide = null;
            this.slDataGridView1.ColumnWidth = null;
            this.slDataGridView1.CustomEnabled = true;
            this.slDataGridView1.DisplayColumnWrapper = null;
            this.slDataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.slDataGridView1.GridDefaultRow = 0;
            this.slDataGridView1.Location = new System.Drawing.Point(0, 0);
            this.slDataGridView1.Name = "slDataGridView1";
            this.slDataGridView1.ReadOnly = true;
            this.slDataGridView1.ReadOnlyColumns = null;
            this.slDataGridView1.RequiredColumns = null;
            this.slDataGridView1.RowHeadersWidth = 5;
            this.slDataGridView1.Size = new System.Drawing.Size(508, 350);
            this.slDataGridView1.SkippingColumns = null;
            this.slDataGridView1.TabIndex = 0;
            // 
            // colPNo
            // 
            this.colPNo.DataPropertyName = "SA_P_NO";
            this.colPNo.HeaderText = "Personnel No";
            this.colPNo.Name = "colPNo";
            this.colPNo.ReadOnly = true;
            this.colPNo.Width = 75;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "PR_FIRST_NAME";
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 235;
            // 
            // colM1
            // 
            this.colM1.DataPropertyName = "SA_FOR_THE_MONTH1";
            this.colM1.HeaderText = "Month (1)";
            this.colM1.Name = "colM1";
            this.colM1.ReadOnly = true;
            this.colM1.Width = 50;
            // 
            // colM2
            // 
            this.colM2.DataPropertyName = "SA_FOR_THE_MONTH2";
            this.colM2.HeaderText = "Month (2)";
            this.colM2.Name = "colM2";
            this.colM2.ReadOnly = true;
            this.colM2.Width = 50;
            // 
            // colAmnt
            // 
            this.colAmnt.DataPropertyName = "SA_ADV_AMOUNT";
            this.colAmnt.HeaderText = "Advance Amount";
            this.colAmnt.Name = "colAmnt";
            this.colAmnt.ReadOnly = true;
            this.colAmnt.Width = 70;
            // 
            // CHRIS_Payroll_SalaryAdvanceEntryGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(508, 446);
            this.Controls.Add(this.slPanelTabular1);
            this.CurrentPanelBlock = "slPanelTabular1";
            this.Name = "CHRIS_Payroll_SalaryAdvanceEntryGrid";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Payroll_SalaryAdvanceEntryGrid";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.slPanelTabular1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelTabular1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelTabular1;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView slDataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colM1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colM2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmnt;
    }
}