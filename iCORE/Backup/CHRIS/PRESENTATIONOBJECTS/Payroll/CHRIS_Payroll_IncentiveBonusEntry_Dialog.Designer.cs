namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_IncentiveBonusEntry_Dialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_IncentiveBonusEntry_Dialog));
            this.slPanelViewBonus = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvViewBonus = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelViewBonus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvViewBonus)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(533, 0);
            // 
            // slPanelViewBonus
            // 
            this.slPanelViewBonus.ConcurrentPanels = null;
            this.slPanelViewBonus.Controls.Add(this.dgvViewBonus);
            this.slPanelViewBonus.DataManager = null;
            this.slPanelViewBonus.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelViewBonus.DependentPanels = null;
            this.slPanelViewBonus.DisableDependentLoad = false;
            this.slPanelViewBonus.EnableDelete = false;
            this.slPanelViewBonus.EnableInsert = false;
            this.slPanelViewBonus.EnableUpdate = false;
            this.slPanelViewBonus.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BonusTabCommand";
            this.slPanelViewBonus.Location = new System.Drawing.Point(0, 91);
            this.slPanelViewBonus.MasterPanel = null;
            this.slPanelViewBonus.Name = "slPanelViewBonus";
            this.slPanelViewBonus.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelViewBonus.Size = new System.Drawing.Size(569, 304);
            this.slPanelViewBonus.SPName = "CHRIS_SP_BONUS_TAB_MANAGER_EXPLICIT";
            this.slPanelViewBonus.TabIndex = 10;
            // 
            // dgvViewBonus
            // 
            this.dgvViewBonus.AllowUserToAddRows = false;
            this.dgvViewBonus.AllowUserToDeleteRows = false;
            this.dgvViewBonus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvViewBonus.ColumnToHide = null;
            this.dgvViewBonus.ColumnWidth = null;
            this.dgvViewBonus.CustomEnabled = true;
            this.dgvViewBonus.DisplayColumnWrapper = null;
            this.dgvViewBonus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvViewBonus.GridDefaultRow = 0;
            this.dgvViewBonus.Location = new System.Drawing.Point(0, 0);
            this.dgvViewBonus.Name = "dgvViewBonus";
            this.dgvViewBonus.ReadOnly = true;
            this.dgvViewBonus.ReadOnlyColumns = null;
            this.dgvViewBonus.RequiredColumns = null;
            this.dgvViewBonus.Size = new System.Drawing.Size(569, 304);
            this.dgvViewBonus.TabIndex = 0;
            // 
            // CHRIS_Payroll_IncentiveBonusEntry_Dialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 471);
            this.Controls.Add(this.slPanelViewBonus);
            this.CurrentPanelBlock = "slPanelViewBonus";
            this.Name = "CHRIS_Payroll_IncentiveBonusEntry_Dialog";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Payroll_IncentiveBonusEntry_Dialog";
            this.Controls.SetChildIndex(this.slPanelViewBonus, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelViewBonus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvViewBonus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelViewBonus;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvViewBonus;
    }
}