using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_IndividualAllowanceEntry : ChrisMasterDetailForm
    {
        #region Fields

        CmnDataManager cmnDM = new CmnDataManager();
        Dictionary<string, object> paramValues = new Dictionary<string, object>();
        Result rslt;
        #endregion

        #region Constructors

        public CHRIS_Payroll_IndividualAllowanceEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Payroll_IndividualAllowanceEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(slPanelDetailAllowance);

            slPanelMasterAllowance.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(slPanelMasterAllowance);

            this.slPanelDetailAllowance.ForeColor = Color.Black;
            //this.slPanelDetailAllowance.Font.Size = 8.25;


        }

        #endregion

        #region Methods
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel" || actionType == "Close")
            {
                this.dgvAllowanceDetails.CancelEdit();
                this.dgvAllowanceDetails.EndEdit();
                txt_SP_ALL_CODE.IsRequired = false;
            }
            if (actionType == "Delete")
            {
                if (!string.IsNullOrEmpty((this.slPanelMasterAllowance.CurrentBusinessEntity as iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceCommand).SP_ALL_CODE))//if (txt_SP_ALL_CODE.Text.Trim() != string.Empty || txt_SP_ALL_CODE.Text.Trim() != "")
                {
                    this.operationMode = Mode.Edit;
                    base.DoToolbarActions(ctrlsCollection, actionType);
                    this.operationMode = Mode.Add;
                    txt_SP_ALL_CODE.Select();
                    txt_SP_ALL_CODE.Focus();
                    return;
                }
            }
            if (actionType == "Save")
            {
                //InitializeFormObject(slPanelMasterAllowance);
                //base.IterateFormControls(slPanelMasterAllowance.Controls, false, false, false);
                // if (!this.IsValidPage)
                if (string.IsNullOrEmpty(this.txt_SP_ALL_CODE.Text) || string.IsNullOrEmpty(this.txt_SP_FORECAST_TAX.Text) || string.IsNullOrEmpty(this.txt_SP_TAX.Text) || string.IsNullOrEmpty(this.txt_SP_ACOUNT_NO.Text))
                    return;//if (!this.FindForm().Validate())
                if (this.dgvAllowanceDetails.CurrentCell != null)
                {
                    if (this.dgvAllowanceDetails.CurrentCell.OwningColumn.Name == this.dgvAllowanceDetails.Columns["col_SP_P_NO"].Name)
                    {
                        if (!this.dgvAllowanceDetails.CurrentCell.OwningRow.IsNewRow)
                        {
                            if (this.dgvAllowanceDetails.CurrentCell.IsInEditMode)
                            {
                                if (string.IsNullOrEmpty(this.dgvAllowanceDetails.CurrentCell.EditedFormattedValue.ToString()))
                                { return; }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(this.dgvAllowanceDetails.CurrentCell.Value.ToString()))
                                { return; }
                            }
                        }
                    }
                }
                if (!this.dgvAllowanceDetails.EndEdit())
                    return;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);


            if (actionType == "Cancel")
            {
                txt_SP_ALL_CODE.IsRequired = false;
                this.FunctionConfig.CurrentOption = Function.None;
                this.slDatePicker_SP_VALID_FROM.Enabled = false;
                this.slDatePicker_SP_VALID_TO.Enabled = false;
                this.lookupAccounts.Enabled = false;
                this.lookupSpCodes.Enabled = false;
                txtOption.Text = "";
                txtOption.Select();
                txtOption.Focus();
                txt_SP_ALL_CODE.IsRequired = true;
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            txtOption.Select();
            txtOption.Focus();
            base.OnLoad(e);
            txtOption.Select();
            txtOption.Focus();
            tbtAdd.Visible = false;
            tbtEdit.Visible = false;
            tbtList.Visible = false;
            this.slDatePicker_SP_VALID_FROM.Enabled = false;
            this.slDatePicker_SP_VALID_TO.Enabled = false;
            // tbtClose.Visible = false;
            //tbtCancel.Visible = false;
            this.lookupAccounts.Enabled = false;
            this.lookupSpCodes.Enabled = false;
            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
            this.FunctionConfig.EnableF8 = false;
            this.F1OptionText = " START ENTRY";
            this.stsOptions.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold);
            this.stsOptions.ForeColor = System.Drawing.Color.Purple;
            ((DataGridViewLOVColumn)this.dgvAllowanceDetails.Columns["col_SP_P_NO"]).LovDataType = typeof(decimal);
            this.dgvAllowanceDetails.Columns["col_SP_P_NO"].DefaultCellStyle.NullValue = "";
            this.dgvAllowanceDetails.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            this.label10.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold);
            this.label1.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold);
            this.groupBox1.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold);
            //tbtSave.Enabled = false;
        }

        protected override bool Quit()
        {
            if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                txt_SP_ALL_CODE.IsRequired = false;
                //  base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClear"]));
                base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
                this.FunctionConfig.CurrentOption = Function.None;
                this.slDatePicker_SP_VALID_FROM.Enabled = false;
                this.slDatePicker_SP_VALID_TO.Enabled = false;
                this.lookupAccounts.Enabled = false;
                this.lookupSpCodes.Enabled = false;
                txtOption.Select();
                txtOption.Focus();
                txt_SP_ALL_CODE.IsRequired = true;
                //tbtSave.Enabled = false;
                return false;

            }
            else
            {
                base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
                return false;
            }
            //return base.View();
        }

        protected override bool Add()
        {
            bool flag = false;

            this.operationMode = Mode.Add;
            this.FunctionConfig.CurrentOption = Function.Add;
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Add";
            this.txtOption.Text = "A";
            this.slDatePicker_SP_VALID_FROM.Enabled = true;
            this.slDatePicker_SP_VALID_TO.Enabled = true;
            this.lookupAccounts.Enabled = true;
            this.lookupSpCodes.Enabled = true;
            //tbtSave.Enabled = true;
            return flag;
        }

        protected override bool Delete()
        {
            bool flag = false;
            this.operationMode = Mode.Edit;
            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Delete";
            this.txtOption.Text = "D";
            //this.FunctionConfig.CurrentOption = Function.Modify;
            this.FunctionConfig.CurrentOption = Function.Delete;
            //this.tbtDelete.PerformClick();
            this.txtOption.Text = "A";
            //this.FunctionConfig.CurrentOption = Function.Modify;
            this.FunctionConfig.CurrentOption = Function.Add;
            return flag;
        }

        protected override bool Save()
        {
            bool flag = false;

            //this.tbtSave.PerformClick();
            base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSave"]));

            this.txtOption.Text = "A";
            this.FunctionConfig.CurrentOption = Function.Add;
            this.txt_SP_ALL_CODE.Focus();
            return flag;
        }
        protected internal override void tlbMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Text == "&Close")
            {
                this.dgvAllowanceDetails.CancelEdit();
                this.dgvAllowanceDetails.EndEdit();
            }
            base.tlbMain_ItemClicked(sender, e);
        }

        #region Commented Code
        //protected override void CommonOnLoadMethods()
        //{
        //    base.CommonOnLoadMethods();
        //    txtOption.Focus();

        //   // txtOption.TabIndex = 0;

        //}
        //protected override bool Add()
        //{
        //    //Overriding Add() method,becuase we need Modify behavior in Master on Add/Insert Mode.
        //    bool flag = false;

        //    if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Modify";
        //    this.txtOption.Text = "A";//"M";
        //    this.FunctionConfig.CurrentOption = Function.Modify;

        //    return flag;
        //    //return base.Add();
        //}
        #endregion

        #endregion

        #region Events

        private void txt_SP_TAX_Validating(object sender, CancelEventArgs e)
        {
            if (txt_SP_TAX.Text.Length > 0)
            {
                if (txt_SP_TAX.Text.ToUpper() != "Y" && txt_SP_TAX.Text.ToUpper() != "N")
                {
                    MessageBox.Show("Use [Y]Yes or [N]No", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    e.Cancel = true;
                }
            }
            else
                e.Cancel = true;
        }

        private void txt_SP_FORECAST_TAX_Validating(object sender, CancelEventArgs e)
        {
            if (txt_SP_FORECAST_TAX.Text.Length > 0)
            {
                if (txt_SP_FORECAST_TAX.Text.ToUpper() != "Y" && txt_SP_FORECAST_TAX.Text.ToUpper() != "N")
                {
                    MessageBox.Show("Use [Y]Yes or [N]No", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    e.Cancel = true;
                }
            }
            else
                e.Cancel = true;
        }

        private void txt_SP_ASR_BASIC_Validating(object sender, CancelEventArgs e)
        {
            if (txt_SP_ASR_BASIC.Text.Length > 0)
            {
                if (txt_SP_ASR_BASIC.Text.ToUpper() != "A" && txt_SP_ASR_BASIC.Text.ToUpper() != "B")
                {
                    MessageBox.Show("Use [A]ASR or [B]Basic", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    e.Cancel = true;
                }
            }
            //else
            //    e.Cancel = true;

        }

        //private void dgvAllowanceDetails_CellEnter(object sender, DataGridViewCellEventArgs e)
        //{
        //    if (txt_SP_ALL_AMOUNT.Text != string.Empty)
        //    {
        //        double amountVal;
        //        if (double.TryParse(txt_SP_ALL_AMOUNT.Text, out amountVal))
        //        {
        //            if (e.RowIndex >= 0 && e.ColumnIndex > 0)
        //            {
        //                if (e.ColumnIndex == dgvAllowanceDetails.Columns["col_SP_ALL_AMT"].Index)
        //                {
        //                    if (!(dgvAllowanceDetails[e.ColumnIndex, e.RowIndex].Value != null && dgvAllowanceDetails[e.ColumnIndex, e.RowIndex].Value.ToString() != string.Empty))
        //                    {
        //                        dgvAllowanceDetails[e.ColumnIndex, e.RowIndex].Value = amountVal;
        //                    }
        //                }

        //            }
        //        }
        //    }
        //}

        private void txt_SP_ALL_CODE_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txt_SP_ALL_CODE.Text.Trim() != string.Empty)
                {
                    string all_code = txt_SP_ALL_CODE.Text.Trim();
                    paramValues.Clear();
                    paramValues.Add("SP_ALL_CODE", txt_SP_ALL_CODE.Text);
                    rslt = cmnDM.GetData("CHRIS_Sp_ALLOWANCE_MANAGER", "ListExist", paramValues);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            base.SetPanelControlWithListItem(slPanelMasterAllowance.Controls, rslt.dstResult.Tables[0].Rows[0]);
                            slPanelMasterAllowance.SetEntityObject(rslt.dstResult.Tables[0].Rows[0]);
                            this.slPanelMasterAllowance.LoadDependentPanels();
                            this.txt_SP_DESC.Focus();

                        }
                        else
                        {
                            //this.ClearForm(slPanelMasterAllowance.Controls);
                            //this.ClearForm(slPanelDetailAllowance.Controls);
                            this.txt_SP_ALL_CODE.Text = all_code;
                            this.slPanelMasterAllowance.LoadDependentPanels();
                        }

                    }
                    else
                    {
                        //this.ClearForm(slPanelMasterAllowance.Controls);
                        //this.ClearForm(slPanelDetailAllowance.Controls);
                        this.txt_SP_ALL_CODE.Text = all_code;
                        //this.IterateFormControls(slPanelMasterAllowance.Controls);
                        this.slPanelMasterAllowance.LoadDependentPanels();
                    }


                }
            }
            catch (Exception fillExcep)
            {
                base.LogError("txt_SP_ALL_CODE_Validated", fillExcep);
            }

        }

        private void dgvAllowanceDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0 && (!this.dgvAllowanceDetails.CurrentRow.IsNewRow))
            {
                if (this.dgvAllowanceDetails.CurrentRow.Cells[0].Value != DBNull.Value)
                {
                    double amountVal = 0.00;
                    double.TryParse(txt_SP_ALL_AMOUNT.Text, out amountVal);
                    this.dgvAllowanceDetails.CurrentRow.Cells["col_SP_ALL_AMT"].Value = amountVal;
                }

            }
        }

        private void dgvAllowanceDetails_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (this.dgvAllowanceDetails.CurrentRow != null && this.dgvAllowanceDetails.CurrentRow.IsNewRow)
            {
                if (this.dgvAllowanceDetails.CurrentCell.ColumnIndex == this.dgvAllowanceDetails.Columns["col_SP_REMARKS"].Index)
                    if (e.KeyData == Keys.Tab)
                    {
                        this.dgvAllowanceDetails.CurrentCell = this.dgvAllowanceDetails["col_SP_ALL_AMT", dgvAllowanceDetails.CurrentRow.Index];
                        //e.IsInputKey = false;
                    }
            }
        }

        private void slDatePicker_SP_VALID_TO_Validating(object sender, CancelEventArgs e)
        {
            if (FunctionConfig.CurrentOption != Function.Add)
                return;
            if (slDatePicker_SP_VALID_FROM.Value != null && slDatePicker_SP_VALID_TO.Value != null)
            {
                if (Convert.ToDateTime(slDatePicker_SP_VALID_TO.Value).Date < Convert.ToDateTime(slDatePicker_SP_VALID_FROM.Value).Date)
                {
                    MessageBox.Show("To date must be greater then From date.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }
        }
        /// <summary>
        /// Key press Down Event.... COM - 506
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">KeyEventArgs </param>
        private void dgvAllowanceDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4)
            {
                if (dgvAllowanceDetails.CurrentRow != null && dgvAllowanceDetails.CurrentRow.Index < dgvAllowanceDetails.Rows.Count && dgvAllowanceDetails.CurrentRow.Index != dgvAllowanceDetails.Rows.Count - 1)
                {
                    dgvAllowanceDetails.EndEdit();
                    this.Refresh();
                    dgvAllowanceDetails.Rows.RemoveAt(dgvAllowanceDetails.CurrentRow.Index);
                    this.Refresh();
                }
            }
        }
        #endregion

        

        





    }
}