using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_IncentiveBonusEntry_Dialog : ChrisTabularForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        Dictionary<string, object> BonusValues = new Dictionary<string, object>();
        Result rslt;
        public CHRIS_Payroll_IncentiveBonusEntry_Dialog()
        {
            InitializeComponent();
        }

        public CHRIS_Payroll_IncentiveBonusEntry_Dialog(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }
       
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtOption.Visible = false;
            tbtAdd.Visible = false;
            tbtCancel.Visible = false;
            tbtClose.Visible = false;
            tbtDelete.Visible = false;
            tbtEdit.Visible = false;
            tbtList.Visible = false;
            tbtSave.Visible = false;
            tlbMain.Visible = false;

            this.ShowF6Option = true;
            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F6 = Function.Quit;
            rslt = cmnDM.Get(slPanelViewBonus.SPName, "ListWithNames");
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null)
                {
                    
                    dgvViewBonus.AutoGenerateColumns = true;
                    dgvViewBonus.DataSource = rslt.dstResult.Tables[0];
                    dgvViewBonus.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    dgvViewBonus.Columns[0].HeaderText = "Personnel No.";
                    dgvViewBonus.Columns[1].HeaderText = "Name";
                    dgvViewBonus.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
                    if (dgvViewBonus.Rows.Count > 0)
                        dgvViewBonus.Select();
                    
                }
            }


        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            //base.DoToolbarActions(ctrlsCollection, actionType);
        }
    }
}