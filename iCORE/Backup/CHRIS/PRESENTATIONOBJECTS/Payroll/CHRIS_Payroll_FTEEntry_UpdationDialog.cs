using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_FTEEntry_UpdationDialog : Form
    {
        #region Properties

        bool isCompleted = false;
        public bool IsCompleted
        {
            get { return this.isCompleted; }
            set { this.isCompleted = value; }
        }

        string m_W_MY;
        public string W_MY
        {
            get { return m_W_MY; }
            set { m_W_MY = value; }
        }

        int m_W_OPT;
        public int W_OPT
        {
            get { return m_W_OPT; }
            set { m_W_OPT = value; }
        }
  
        #endregion

        #region Constructor
        public CHRIS_Payroll_FTEEntry_UpdationDialog()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void txtEmployeeType_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((this.txtEmployeeType.Text == "1") || (this.txtEmployeeType.Text == "2") || (this.txtEmployeeType.Text == "3") || (this.txtEmployeeType.Text == "4")) && (e.KeyChar == '\r'))
            {
                W_MY = W_MY;
                W_OPT = Convert.ToInt32(this.txtEmployeeType.Text);

                isCompleted = true;
                this.Close();
            }
            else if (e.KeyChar == '\r')
            {
                MessageBox.Show("PLEASE ENTER ANY OF THE GIVEN NUMBER .......");
                this.txtEmployeeType.Focus();
            }
        }
        
        private void txtEmployeeType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F6)
            {
                W_MY = W_MY;
                W_OPT = 0;

                isCompleted = false;
                this.Close();
            }

        }

        #endregion
    }
}