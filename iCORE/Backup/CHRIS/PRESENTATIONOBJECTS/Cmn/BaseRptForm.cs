#define DEV
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Globalization;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Microsoft.Office.Interop.Outlook;
using iCORE.CHRIS.DATAOBJECTS;
using iTextSharp.text.pdf;
using System.Security.AccessControl;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn
{
    public partial class BaseRptForm : BaseForm //iCORE.Common.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        #region --Variable--
        public string m_ReportPath = "C:\\iCORE-Spool\\";
        private bool m_DynamicSerchCtrl;
        public string m_rptFile;
        public string m_subName;
        private bool m_matrixRpt;
        private bool m_subRpt;
        private string m_XSLFoleder = ConfigurationManager.AppSettings["xslFolderName"];
        public string m_Prefix = "CHRIS_RPT_SP_";
        public static string __message;
#if PRODUCTION
        public string apppath = System.Windows.Forms.Application.ExecutablePath;
        public string m_ModulePath = "\\CHRIS\\PRESENTATIONOBJECTS\\Report\\";
#endif
#if DEV
        public string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
        public string m_ModulePath = "CHRIS\\PRESENTATIONOBJECTS\\Report\\";
#endif

        private bool m_IsValidPage = true;
        private bool m_IsValid = true;
        private string m_SPName;
        public bool m_Subrpt;
        public string m_rootPath = string.Empty;
        private string m_XSLPath = string.Empty;
        public string m_rptFilePath = string.Empty;
        private string ctrlval = string.Empty;
        private DataSet Ds = new DataSet();
        private SqlConnection m_ConObj;
        CommonDataManager oCmnDataMgr = new CommonDataManager();
        public DataTable dataTable = new DataTable();
        private CrystalDecisions.CrystalReports.Engine.ReportDocument crDoc;
        string SubReportName = string.Empty;
        

        #endregion
     
        #region --Properties--

        public string RptFileName
        {
            get { return m_rptFile; }
            set { m_rptFile = value; }
        }

        public bool DynamicSearchCrtl
        {
            get { return m_DynamicSerchCtrl; }
            set { m_DynamicSerchCtrl = value; }
        }

        public bool MatrixReport
        {
            get { return m_matrixRpt; }
            set { m_matrixRpt = value; }
        }


        //private string[] _colect = new string[]{};
        private string[] _colect = new string[100];
        public string[] Colect
        {
            get { return _colect; }
            set { _colect = value; }
        }

        public bool SubReport
        {
            get { return m_subRpt; }
            set { m_subRpt = value; }
        }




    #endregion

        #region --Constructor Segment--

        public BaseRptForm()
        {
            InitializeComponent();
           // base.m_ModulePath = this.m_ModulePath;
          //  base.m_Prefix = this.m_Prefix;
        }

        public BaseRptForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
           
            InitializeComponent();
           
        }

        #endregion

        #region --Methods--

        private string[] Credentials()
        {
            string[] userIdPassword = { connbean.UserId, connbean.Pwd, connbean.M_ServerName, connbean.DbName };

            return userIdPassword;
        }

        public virtual void RunReport()
        {
            //Start Code inserted by zubair 06-12-10
            apppath = apppath.ToUpper();
            
            //m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\")); // Commented by Faisal
            #region Code By Faisal
#if PRODUCTION
            m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
#endif
#if DEV
            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
#endif
            #endregion

            //End Code inserted by zubair 06-12-10
            //m_rootPath = apppath.Substring(0, apppath.Length - 12); 
            ReportViewer rptViewer = new ReportViewer();
            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";
            //MessageBox.Show(m_rptFilePath);
            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                    {
                                ComboBox cmb = (ComboBox)ctrls;
                                //c.SelectedValue
                                ctrlval = ctrls.Text;
                                //dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue;
                            }
                                //if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                //{
                                //    ctrlval = ctrls.Text;
                                //    //dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                //    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrls.Text;

                                //}
                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrlval.Length == 6)

                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                    else                                       
                                        if (ctrlval.Length == 8)

                                           dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                        else
                                           if (ctrlval.Length == 10)
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    // dataTable.Rows[0][dataTable.Columns[index].ColumnName] = Convert.ToDateTime(ctrlval).ToShortDateString();

                                }
                            }

                                //if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                //{
                                //    ctrlval = ctrls.Text;
                                //    //CrplControlLibrary.SLDatePicker lctrls = (CrplControlLibrary.SLDatePicker)ctrls;
                                //    //ctrlval = lctrls.Value == null ? "" : lctrls.Value.ToString();
                                //    //dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.Equals("") ? "" : Convert.ToDateTime(ctrlval).ToString("dd/MM/yyyy");
                                //    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            //    //    // dataTable.Rows[0][dataTable.Columns[index].ColumnName] = Convert.ToDateTime(ctrlval).ToShortDateString();
                            //    }
                            //}
                        }
                    }
                }
            }


             if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;
                 

                if (MatrixReport == true)
                {
                    CallMatrixReport();
                }
                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                    rptViewer.LoadParameters(dataTable, Credentials());
                    rptViewer.ShowDialog(this);
                }

                else if (SubReport == true && dataTable.Rows.Count > 0)
                {
                    rptViewer.ShowDialog(this);
                }
                else
                {
                    MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            }
        }

        /*CAll Matrix Report*/
        public void CallMatrixReport()
        {
            m_XSLPath = m_rootPath + m_ModulePath + m_rptFile + ".xsl";

            Ds = oCmnDataMgr.LoadReportParametersSQL(dataTable);
            //string apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            //if (!Directory.Exists(apppath + m_XSLFoleder))
            //{
            //    DirectoryInfo di = "C:\\"; 
            //}

            //Create the FileStream to write with.
            System.IO.FileStream fs = new System.IO.FileStream(apppath + m_XSLFoleder + m_rptFile + ".xml", System.IO.FileMode.Create);

            //Create an XmlTextWriter for the FileStream.
            System.Xml.XmlTextWriter xtw = new System.Xml.XmlTextWriter(fs, System.Text.Encoding.Unicode);

            //Add processing instructions to the beginning of the XML file, one of which indicates a style sheet.
            xtw.WriteProcessingInstruction("xml", "version='1.0'");
            xtw.WriteProcessingInstruction("xml-stylesheet",
                "type='text/xsl' href='" + m_XSLPath + "'");

            //Write the XML from the dataset to the file.
            Ds.WriteXml(xtw);
            System.IO.FileStream fstrm = new System.IO.FileStream(apppath + m_XSLFoleder + m_rptFile + ".htm", System.IO.FileMode.Create);

            //Create an XmlTextWriter for the FileStream.
            System.Xml.XmlTextWriter xtxtw = new System.Xml.XmlTextWriter(fstrm, System.Text.Encoding.Unicode);

            //Transform the XML using the stylesheet.
            XmlDataDocument xmlDoc = new XmlDataDocument(Ds);
            System.Xml.Xsl.XslCompiledTransform xslTran = new System.Xml.Xsl.XslCompiledTransform();
            xslTran.Load(m_XSLPath);
            xslTran.Transform(xmlDoc, null, xtxtw);

            //Open the HTML file in Excel.
            Excel.Application oExcel = new Excel.Application();
            oExcel.Visible = true;
            oExcel.UserControl = true;
            Excel.Workbooks oBooks = oExcel.Workbooks;
            object oOpt = System.Reflection.Missing.Value;
            oBooks.Open(apppath + m_XSLFoleder + m_rptFile + ".htm", oOpt, oOpt, oOpt,
                oOpt, oOpt, oOpt, oOpt, oOpt, oOpt, oOpt, oOpt,
                oOpt, oOpt, oOpt);

        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataSet RunCustomReport()
        {
            //Start Code inserted by zubair 06-12-10
            apppath = apppath.ToUpper();

            //m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\")); // Commented By Faisal

            #region Code By Faisal
#if PRODUCTION
            m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
#endif
#if DEV
            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
#endif
            #endregion

            //End Code inserted by zubair 06-12-10

            //m_rootPath = apppath.Substring(0, apppath.Length - 10);
            ReportViewer rptViewer = new ReportViewer();
            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";
            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    //ComboBox c = (ComboBox)ctrls;
                                    //c.SelectedValue
                                    ctrlval = ctrls.Text;
                                    //dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrls.Text;
                                }
                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrls.Text == " ")
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        // dataTable.Rows[0][dataTable.Columns[index].ColumnName] = Convert.ToDateTime(ctrlval).ToShortDateString();
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;


                if (MatrixReport == true)
                {
                    CallCustomMatrixReport();
                }
                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                    rptViewer.LoadParameters(dataTable,Credentials());
                    rptViewer.ShowDialog(this);
                }
                else
                {
                    MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //return;
                }

            }

            return Ds;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private DataSet CallCustomMatrixReport()
        {
            m_XSLPath = m_rootPath + m_ModulePath + m_rptFile + ".xsl";
            Ds = oCmnDataMgr.LoadReportParametersSQL(dataTable);
            //string apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            if (!Directory.Exists(apppath + m_XSLFoleder))
            {
                DirectoryInfo di = Directory.CreateDirectory(apppath + m_XSLFoleder);
            }

            return Ds;
        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            CreateToolbar();
            this.SetAcceptCancelButtons(this.Controls);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctrls"></param>
        private void SetAcceptCancelButtons(Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is CrplControlLibrary.SLButton || ctrl is Button)
                {
                    Button btn = (Button)ctrl;
                    string btnText = ctrl.Text.ToUpper().Trim();
                    if (btnText == "RUN" || btnText == "RUN REPORT" || btnText == "OK")
                    {
                        this.AcceptButton = btn;
                    }
                    if (btnText == "CLOSE" || btnText == "EXIT" || btnText == "CANCEL")
                    {
                        this.CancelButton = btn;
                    }
                }
                else
                {
                    SetAcceptCancelButtons(ctrl.Controls);
                }
            }
        }
        protected override void CreateToolbar()
        {
            base.CreateToolbar();
            tbtAdd.Visible = tbtCancel.Visible = tbtDelete.Visible = tbtEdit.Visible = tbtList.Visible = tbtSave.Visible = false;
        }

        #endregion

        #region --Events--
        protected virtual void btnCallReport_Click(object sender, EventArgs e)
        {
            /*Call Report*/
            RunReport();
        }

        protected virtual void btnCloseReport_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

//        public void PrintCustomReport()
//        {
//            //Start Code inserted by zubair 06-12-10
//            apppath = apppath.ToUpper();

//            //m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\")); // Commented By Faisal
//            #region Code By Faisal
//#if PRODUCTION
//            m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
//#endif
//#if DEV
//            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
//#endif
//            #endregion

//            //End Code inserted by zubair 06-12-10

//            //m_rootPath = apppath.Substring(0, apppath.Length - 12);
//            ReportViewer rptViewer = new ReportViewer();


//            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";

//            m_SPName = m_Prefix + RptFileName;
//            dataTable.TableName = m_SPName;

//            dataTable = SQLManager.GetSPParams(m_SPName);
//            dataTable.Rows.Add();

//            foreach (Control ctrlGrpBox in this.Controls)
//            {
//                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
//                {
//                    foreach (Control ctrls in ctrlGrpBox.Controls)
//                    {
//                        for (int index = 0; index < dataTable.Columns.Count; index++)
//                        {
//                            if ((ctrls is CrplControlLibrary.SLTextBox))
//                            {
//                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
//                                {
//                                    if (ctrls.Text != "")
//                                    {
//                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
//                                        {
//                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
//                                            {
//                                                ctrlval = ctrls.Text;
//                                                string date = ctrlval.ToString();
//                                                string m_Month = ctrlval.Substring(0, 2);
//                                                string m_Year = ctrlval.Substring(2, 4);
//                                                string m_Date = m_Month + "-01-" + m_Year;

//                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
//                                                m_IsValidPage = true;
//                                            }
//                                            else
//                                            {
//                                                m_IsValidPage = false;
//                                            }
//                                        }
//                                        else
//                                        {
//                                            ctrlval = ctrls.Text;
//                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
//                                        }
//                                    }
//                                    else
//                                    {
//                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
//                                    }
//                                }
//                            }
//                            if ((ctrls is ComboBox))
//                            {
//                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
//                                {
//                                    ComboBox cmb = (ComboBox)ctrls;

//                                    ctrlval = ctrls.Text;
//                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue;
//                                }

//                            }

//                            if ((ctrls is CrplControlLibrary.SLDatePicker))
//                            {
//                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
//                                {
//                                    ctrlval = ctrls.Text;
//                                    if (ctrlval.Length == 6)

//                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
//                                    else
//                                        if (ctrlval.Length == 8)

//                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
//                                        else
//                                            if (ctrlval.Length == 10)
//                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);

//                                }
//                            }

//                        }
//                    }
//                }
//            }


//            if (m_IsValidPage == true)
//            {
//                /*Open Report Viewer Control & Pass RprName n rptPath*/
//                rptViewer.m_RptName = m_rptFile;
//                rptViewer.m_SP = m_rptFile;
//                rptViewer.m_RptPath = m_rptFilePath;


//                if (MatrixReport == true)
//                {
//                    CallMatrixReport();
//                }
//                else if (MatrixReport == false && dataTable.Rows.Count > 0)
//                {
//                    rptViewer.LoadParameters(dataTable, Credentials());
//                    rptViewer.crystalReportViewer1.PrintReport();
//                    rptViewer = null;

//                }
//                else
//                {
//                    MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                    return;
//                }

//            }
//        }

        public void PrintCustomReport()
        {

            apppath = apppath.ToUpper();

            //m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\")); // Commented By Faisal
            #region Code By Faisal
#if PRODUCTION
            m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
#endif
#if DEV
            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
#endif
            #endregion

            //m_rootPath = apppath.Substring(0, apppath.Length - 12);
            ReportViewer rptViewer = new ReportViewer();


            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";

            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ComboBox cmb = (ComboBox)ctrls;

                                    ctrlval = ctrls.Text;
                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue;
                                }

                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrlval.Length == 6)

                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                    else
                                        if (ctrlval.Length == 8)

                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                        else
                                            if (ctrlval.Length == 10)
                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }
                            }

                        }
                    }
                }
            }


            if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;


                if (MatrixReport == true)
                {
                    CallMatrixReport();
                }
                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                    rptViewer.LoadParameters(dataTable, Credentials());
                    //rptViewer.LoadParameters(dataTable, Credentials());
                    //rptViewer.crystalReportViewer1.PrintReport();                    
                    //rptViewer = null;



                    PrintDialog ps = new PrintDialog();
                    //ps.PrinterSettings.Copies = Convert.ToInt16(no_copies);
                    ps.UseEXDialog = true;
                    DialogResult result = ps.ShowDialog();

                    if (result == DialogResult.OK)
                    {
                        //rptViewer.crDoc.PrintOptions.PrinterName=ps.
                        rptViewer.crDoc.PrintOptions.PrinterName = ps.PrinterSettings.PrinterName;

                        rptViewer.crDoc.PrintToPrinter(ps.PrinterSettings.Copies, false, 0, 0);

                    }
                    //rptViewer.ShowDialog(this);
                    rptViewer = null;

                }




                else
                {
                    MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            }
        }

        #region Code by Umair on 28 June 2011
        /// <summary>
        /// Export Report to Excel
        /// </summary>
        /// <param name="DestinationFile"></param>
        /// <param name="Dest_Format"></param>
        public void ExportCustomReport(string DestinationFile, string Dest_Format)
        {
            try
            {
                

                Dest_Format     = Dest_Format.ToUpper();
                DestinationFile = DestinationFile;// +".pdf";
                apppath         = apppath.ToUpper();

                #region Code By Faisal
#if PRODUCTION
                m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
#endif
#if DEV
            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
#endif
                #endregion

                ReportViewer rptViewer  = new ReportViewer();
                m_rptFilePath           = m_rootPath + m_ModulePath + m_rptFile + ".rpt";
                m_SPName                = m_Prefix + RptFileName;
                dataTable.TableName     = m_SPName;
                dataTable               = SQLManager.GetSPParams(m_SPName);
                dataTable.Rows.Add();

                #region Fill dtParam with Control Values
                foreach (Control ctrlGrpBox in this.Controls)
                {
                    if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                    {
                        foreach (Control ctrls in ctrlGrpBox.Controls)
                        {
                            for (int index = 0; index < dataTable.Columns.Count; index++)
                            {
                                if ((ctrls is CrplControlLibrary.SLTextBox))
                                {
                                    if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                    {
                                        if (ctrls.Text != "")
                                        {
                                            if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                            {
                                                if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                                {
                                                    ctrlval = ctrls.Text;
                                                    string date = ctrlval.ToString();
                                                    string m_Month = ctrlval.Substring(0, 2);
                                                    string m_Year = ctrlval.Substring(2, 4);
                                                    string m_Date = m_Month + "-01-" + m_Year;

                                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                    m_IsValidPage = true;
                                                }
                                                else
                                                {
                                                    m_IsValidPage = false;
                                                }
                                            }
                                            else
                                            {
                                                ctrlval = ctrls.Text;
                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                            }
                                        }
                                        else
                                        {
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                        }
                                    }
                                }
                                if ((ctrls is ComboBox))
                                {
                                    if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                    {
                                        ComboBox cmb = (ComboBox)ctrls;
                                        ctrlval = ctrls.Text;
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue;
                                    }

                                }

                                if ((ctrls is CrplControlLibrary.SLDatePicker))
                                {
                                    if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                    {
                                        ctrlval = ctrls.Text;
                                        if (ctrlval.Length == 6)

                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                        else
                                            if (ctrlval.Length == 8)

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                            else
                                                if (ctrlval.Length == 10)
                                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                {
                    if (m_IsValidPage == true)
                    {
                        /*Open Report Viewer Control & Pass RprName n rptPath*/
                        rptViewer.m_RptName = m_rptFile;
                        rptViewer.m_SP      = m_rptFile;
                        rptViewer.m_RptPath = m_rptFilePath;

                        if (MatrixReport == true)
                        {
                            CallMatrixReport();
                        }
                        else if (MatrixReport == false && dataTable.Rows.Count > 0)
                        {
                            DataSet dsSup                   = new DataSet();
                            //if (rptViewer.crDoc.Subreports.Count > 0)
                            {
                                dsSup = rptViewer.LoadParametersWithSubReports(dataTable, Credentials());
                            }
                            string MainRptName              = string.Empty;
                            MainRptName                     = (rptViewer.crDoc.Database.Tables[0].Name.Split(';'))[0];

                            saveFileDialog1.FileName        = MainRptName + ".xls";
                            saveFileDialog1.AddExtension    = true;
                            saveFileDialog1.Filter          = "xls files (*.xls)|*.xls|All files (*.*)|*.*";
                            DialogResult drs                = saveFileDialog1.ShowDialog();
                            DestinationFile                 = saveFileDialog1.FileName;

                            if (drs == DialogResult.OK)
                            {
                                //if (!Directory.Exists(DestinationFile))
                                //{
                                //    //DirectoryInfo di = "C:\\";
                                //    MessageBox.Show("Not a Valid Path. File will Save at C:\\" + MainRptName + ".xls", "Note", MessageBoxButtons.OK);
                                //    DestinationFile = "C:\\";
                                //}


                                Excel.Application oXL;
                                Excel.Workbook oWB;
                                Excel.Worksheet oSheet;
                                Excel.Range oRange;

                                // Start Excel and get Application object. 
                                oXL = new Excel.Application();

                                // Set some properties 
                                oXL.Visible = false;
                                oXL.DisplayAlerts = false;

                                // Get a new workbook. 
                                oWB = oXL.Workbooks.Add(System.Reflection.Missing.Value);

                                // Get the active sheet 
                                oSheet = (Excel.Worksheet)oWB.Sheets.Add(System.Reflection.Missing.Value,
                                                    System.Reflection.Missing.Value, System.Reflection.Missing.Value,
                                                    System.Reflection.Missing.Value);

                                oSheet = (Excel.Worksheet)oWB.ActiveSheet;
                                if (MainRptName.Length > 31)
                                {
                                    oSheet.Name = MainRptName.Substring(0, 31);
                                }
                                else
                                {
                                    oSheet.Name = MainRptName;
                                }
                                oSheet = Custom_Excel_FromDataTable(oSheet, dsSup.Tables[MainRptName], MainRptName);

                                if (dsSup.Tables[MainRptName] != null)
                                {
                                    if (dsSup.Tables[MainRptName].Rows.Count > 0)
                                    {
                                        oRange = oSheet.get_Range(oSheet.Cells[1, 1], oSheet.Cells[dsSup.Tables[MainRptName].Rows.Count, dsSup.Tables[MainRptName].Columns.Count]);
                                        oRange.EntireColumn.AutoFit();
                                    }
                                }
                                if (rptViewer.crDoc.Subreports.Count > 0)
                                {
                                    foreach (ReportDocument rpt in rptViewer.crDoc.Subreports)
                                    {
                                        ReportDocument subReport = rptViewer.crDoc.OpenSubreport(rpt.Name);
                                        SubReportName = subReport.Name;


                                        if (rptViewer.dsSubLinkParam.Tables.Count > 0)
                                        {
                                            /*Subreports have Link Parameters now Makeing thew Relationship between Main Report Datatable and Sub Report Datatable*/
                                            DataTable rsltDT = new DataTable();
                                            DataColumn dcRel1;
                                            DataColumn dcRel2;
                                            DataTable dt = new DataTable();
                                            string SubRptName = string.Empty;

                                            SubRptName = rpt.Database.Tables[0].Name.Split(';')[0];

                                            /*Link Columns Name*/
                                            dcRel1 = dsSup.Tables[MainRptName].Columns[rptViewer.dsSubLinkParam.Tables[SubReportName].Rows[0]["MainLinkParamName"].ToString()];
                                            dcRel2 = dsSup.Tables[SubRptName].Columns[rptViewer.dsSubLinkParam.Tables[SubReportName].Rows[0]["SubLinkParamName"].ToString()];

                                            int RowID = 0;
                                            rsltDT.Columns.Clear();
                                            rsltDT.TableName = SubRptName;
                                            rsltDT = dsSup.Tables[SubRptName].Copy();
                                            rsltDT.Columns.Add(dcRel1.ColumnName);
                                            rsltDT.Rows.Clear();
                                            dt.Columns.Clear();
                                            dt.Rows.Clear();
                                            dt.Columns.Add(dcRel1.ColumnName);

                                            foreach (DataRow rd in dsSup.Tables[MainRptName].Rows)
                                            {
                                                string FilterExp    = dcRel2 + " = " + (rd[dcRel1].ToString() == "" ? "0" : rd[dcRel1].ToString());
                                                DataView dv         = new DataView(dsSup.Tables[(subReport.Database.Tables[0].Name.Split(';'))[0]], FilterExp, "", DataViewRowState.CurrentRows);
                                                dv.RowStateFilter   = DataViewRowState.CurrentRows;
                                                dt = dv.ToTable().Copy();
                                                dt.Columns.Add(dcRel1.ColumnName);
                                                foreach (DataRow dr in dt.Rows)
                                                {
                                                    dr[dcRel1.ColumnName] = rd[dcRel1].ToString();
                                                    rsltDT.Rows.Add(dr.ItemArray);
                                                }
                                            }

                                            oSheet = (Excel.Worksheet)oWB.Sheets.Add(System.Reflection.Missing.Value,
                                                                System.Reflection.Missing.Value, System.Reflection.Missing.Value,
                                                                System.Reflection.Missing.Value);
                                            oSheet = (Excel.Worksheet)oWB.ActiveSheet;
                                            oSheet.Name = SubRptName;
                                            oSheet = Custom_Excel_FromDataTable(oSheet, rsltDT, SubRptName);

                                            if (rsltDT.Rows.Count > 0)
                                            {
                                                oRange = oSheet.get_Range(oSheet.Cells[1, 1], oSheet.Cells[rsltDT.Rows.Count, rsltDT.Columns.Count]);
                                                oRange.EntireColumn.AutoFit();
                                            }
                                            /*Excel Formtting*/
                                            //Excel_FromDataTable(rsltDT, SubRptName);
                                        }
                                        else
                                        {
                                            DataTable rsltDT = new DataTable();
                                            DataTable dt = new DataTable();
                                            DataColumn dcRel1;
                                            DataColumn dcRel2;
                                            string SubRptName = string.Empty;
                                            //dcRel1 = dsSup.Tables[MainRptName].Columns[rptViewer.dsSubLinkParam.Tables[SubReportName].Rows[0]["MainLinkParamName"].ToString()];
                                            
                                            if(dsSup.Tables.Contains((subReport.Database.Tables[0].Name.Split(';'))[0].ToString()))
                                                dt = dsSup.Tables[(subReport.Database.Tables[0].Name.Split(';'))[0]].Copy();

                                            SubRptName = rpt.Database.Tables[0].Name.Split(';')[0];
                                            rsltDT.Columns.Clear();
                                            rsltDT.TableName = SubRptName;

                                            if (dsSup.Tables.Contains((subReport.Database.Tables[0].Name.Split(';'))[0].ToString()))
                                                rsltDT = dsSup.Tables[SubRptName].Copy();
                                            //rsltDT.Columns.Add(dcRel1);
                                            rsltDT.Rows.Clear();

                                            foreach (DataRow dr in dt.Rows)
                                            {
                                                rsltDT.Rows.Add(dr.ItemArray);
                                            }

                                            oSheet = (Excel.Worksheet)oWB.Sheets.Add(System.Reflection.Missing.Value,
                                                            System.Reflection.Missing.Value, System.Reflection.Missing.Value,
                                                            System.Reflection.Missing.Value);
                                            oSheet = (Excel.Worksheet)oWB.ActiveSheet;
                                            oSheet.Name = SubRptName;
                                            oSheet = Custom_Excel_FromDataTable(oSheet, rsltDT, SubRptName);

                                            if (rsltDT.Rows.Count > 0)
                                            {
                                                oRange = oSheet.get_Range(oSheet.Cells[1, 1], oSheet.Cells[rsltDT.Rows.Count, rsltDT.Columns.Count]);
                                                oRange.EntireColumn.AutoFit();
                                            }
                                        }
                                    }
                                }

                                // Save the sheet and close 
                                oSheet = null;
                                oRange = null;

                                //oWB.SaveAs(DestinationFile  + MainRptName + ".xls", Excel.XlFileFormat.xlWorkbookNormal,
                                //    Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                //    Excel.XlSaveAsAccessMode.xlExclusive,
                                //    Missing.Value, Missing.Value, Missing.Value,
                                //    Missing.Value, Missing.Value);

                                oWB.SaveAs(DestinationFile, Excel.XlFileFormat.xlWorkbookNormal,
                                    Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                    Excel.XlSaveAsAccessMode.xlExclusive,
                                    Missing.Value, Missing.Value, Missing.Value,
                                    Missing.Value, Missing.Value);

                                oWB.Close(Missing.Value, Missing.Value, Missing.Value);
                                oWB = null;
                                oXL.Quit();

                                MessageBox.Show("File Save Successfully", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }

                        else
                        {
                            MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
            }
            catch (System.Exception exp)
            {
                LogError(this.Name, "ExportCustomReport", exp);
                MessageBox.Show("Error in Creating Excel File", "Note", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion
        
        public void EmailToReport(string DestinationFile, string Dest_Format)
        {
            Dest_Format = Dest_Format.ToUpper();
            DestinationFile = DestinationFile + ".pdf";
            //Start Code inserted by zubair 06-12-10
            apppath = apppath.ToUpper();
            
            //m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\")); // Commented By Faisal
            #region Code By Faisal
#if PRODUCTION
            m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
#endif
#if DEV
            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
#endif
            #endregion

            //End Code inserted by zubair 06-12-10

            //m_rootPath = apppath.Substring(0, apppath.Length - 12);
            ReportViewer rptViewer = new ReportViewer();
           
            CrystalDecisions.CrystalReports.Engine.ReportDocument Temp;
            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";
           
            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ComboBox cmb = (ComboBox)ctrls;
                                     ctrlval = ctrls.Text;
                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue;
                                }

                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrlval.Length == 6)

                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                    else
                                        if (ctrlval.Length == 8)

                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                        else
                                            if (ctrlval.Length == 10)
                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }
                            }

                        }
                    }
                }
            }


            if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;


                if (MatrixReport == true)
                {
                    CallMatrixReport();
                }
                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                    rptViewer.LoadParameters(dataTable, Credentials());
                  //  rptViewer.ShowDialog(this);
                    //rptViewer.ShowDialog(this);
                    if (Dest_Format == "pdf" || Dest_Format == "dflt" || Dest_Format == "PDF")
                    {
                        //rptViewer.Enabled = false;
                        string AttachmentName = "";
                        rptViewer.crDoc.ExportToDisk(ExportFormatType.PortableDocFormat, DestinationFile/*@"C:\crystalreport.pdf" */);
                        rptViewer.Close();
                                        
                                              
                        AttachmentName = DestinationFile;
                        Microsoft.Office.Interop.Outlook.Application app;
                        Microsoft.Office.Interop.Outlook._NameSpace appNameSpace;
                        Microsoft.Office.Interop.Outlook.MailItem memo;

                       app = new Microsoft.Office.Interop.Outlook.Application();
                        appNameSpace = app.GetNamespace("MAPI");
                        appNameSpace.Logon(null, null, false, false);
                        memo = (Microsoft.Office.Interop.Outlook.MailItem)app.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);
                        //' memo.To = "sendto@emailaddress.com"
                        // memo.Subject = "Export crystal report to email"
                        //' memo.Body = "Will go to the body of the email"
                        memo.Subject = "report";
                        // memo.Attachments.Add(AttachmentName);
                        memo.Attachments.Add(AttachmentName, Type.Missing, Type.Missing, Type.Missing);
                        //memo.Attachments.
                       
                        memo.Display(false);

                    }
                    else
                    {
                        MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

            }
        }

        /// <summary>
        /// Created by TAHNIA.Used for print no of copies to print
        /// </summary>
        /// <param name="no_copies"></param>
        
        public void PrintNoofCopiesReport(int no_copies)
        {
            
            apppath = apppath.ToUpper();
            
            //m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\")); // Commented By Faisal
            #region Code By Faisal
#if PRODUCTION
            m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
#endif
#if DEV
            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
#endif
            #endregion

            //m_rootPath = apppath.Substring(0, apppath.Length - 12);
            ReportViewer rptViewer = new ReportViewer();


            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";

            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ComboBox cmb = (ComboBox)ctrls;

                                    ctrlval = ctrls.Text;
                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue;
                                }

                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrlval.Length == 6)

                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                    else
                                        if (ctrlval.Length == 8)

                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                        else
                                            if (ctrlval.Length == 10)
                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }
                            }

                        }
                    }
                }
            }


            if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;


                if (MatrixReport == true)
                {
                    CallMatrixReport();
                }
                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                    rptViewer.LoadParameters(dataTable, Credentials());
                    //rptViewer.LoadParameters(dataTable, Credentials());
                    //rptViewer.crystalReportViewer1.PrintReport();                    
                    //rptViewer = null;



                    PrintDialog ps = new PrintDialog();
                    ps.PrinterSettings.Copies = Convert.ToInt16(no_copies);
                    ps.UseEXDialog = true;
                    DialogResult result = ps.ShowDialog();
                  
                    if (result == DialogResult.OK)
                    {
                        //rptViewer.crDoc.PrintOptions.PrinterName=ps.
                        rptViewer.crDoc.PrintOptions.PrinterName = ps.PrinterSettings.PrinterName;
                       
                        rptViewer.crDoc.PrintToPrinter(no_copies, false, 0, 0);

                    }
                    //rptViewer.ShowDialog(this);
                    rptViewer = null;

                }




                else
                {
                    MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            }
        }
        
        /// <summary>
        /// Created by TAHNIA.Used for Email Report in  PDF attachment format by using Microsoft Outlook
        /// </summary>
        /// <param name="DestinationFile"></param>
        /// <param name="Dest_Format"></param>
        /// <param name="sender"></param>
        public void EmailToReport(string DestinationFile, string Dest_Format, string sender)
        {

            Dest_Format = Dest_Format.ToUpper();
            DestinationFile = DestinationFile + ".pdf";
            //Start Code inserted by zubair 06-12-10
            apppath = apppath.ToUpper();
            //m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\")); //commneted by Faisal Iqbal

            #region Code By Faisal
#if PRODUCTION
            m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
#endif
#if DEV
            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
#endif
            #endregion

            //End Code inserted by zubair 06-12-10

            //m_rootPath = apppath.Substring(0, apppath.Length - 12);
            ReportViewer rptViewer = new ReportViewer();

            CrystalDecisions.CrystalReports.Engine.ReportDocument Temp;
            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";

            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ComboBox cmb = (ComboBox)ctrls;
                                    ctrlval = ctrls.Text;
                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue;
                                }

                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrlval.Length == 6)

                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                    else
                                        if (ctrlval.Length == 8)

                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                        else
                                            if (ctrlval.Length == 10)
                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }
                            }

                        }
                    }
                }
            }


            if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;


                if (MatrixReport == true)
                {
                    CallMatrixReport();
                }
                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                    rptViewer.LoadParameters(dataTable, Credentials());
                    //rptViewer.ShowDialog(this);
                    //rptViewer.Close();
                    if (Dest_Format == "pdf" || Dest_Format == "dflt" || Dest_Format == "PDF")
                    {
                        string AttachmentName = "";
                        rptViewer.crDoc.ExportToDisk(ExportFormatType.PortableDocFormat, DestinationFile/*@"C:\crystalreport.pdf" */);
                        rptViewer.Close();


                        AttachmentName = DestinationFile;
                        Microsoft.Office.Interop.Outlook.Application app;
                        Microsoft.Office.Interop.Outlook._NameSpace appNameSpace;
                        Microsoft.Office.Interop.Outlook.MailItem memo;

                        app = new Microsoft.Office.Interop.Outlook.Application();
                        appNameSpace = app.GetNamespace("MAPI");
                        appNameSpace.Logon(null, null, false, false);
                        memo = (Microsoft.Office.Interop.Outlook.MailItem)app.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);
                        //' memo.To = "sendto@emailaddress.com"
                        // memo.Subject = "Export crystal report to email"
                        //' memo.Body = "Will go to the body of the email"
                        memo.Subject = "report";
                        memo.To = sender;
                        // memo.Attachments.Add(AttachmentName);
                        memo.Attachments.Add(AttachmentName, Type.Missing, Type.Missing, Type.Missing);
                        //memo.Attachments.

                        memo.Display(false);

                    }
                    else
                    {
                        MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

            }
        }
        // Added by Syed Fahad Abbas 28-08-2012
        //Email Individual Salary Report based on range defined in fromPno and toPno.
        public void EmailToReport(string DestinationFile, string Dest_Format, string strProfileName,string strSubject, string strBody, string strToEmailAdd, string FilePassword)
        {
            Dest_Format = Dest_Format.ToUpper();
            //DestinationFile = DestinationFile + ".pdf";
            //Start Code inserted by zubair 06-12-10
            apppath = apppath.ToUpper();
            //m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\")); //commneted by Faisal Iqbal

            #region Code By Faisal
#if PRODUCTION
            m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
#endif
#if DEV
            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
#endif
            #endregion

            //End Code inserted by zubair 06-12-10

            //m_rootPath = apppath.Substring(0, apppath.Length - 12);
            ReportViewer rptViewer = new ReportViewer();

            CrystalDecisions.CrystalReports.Engine.ReportDocument Temp;
            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";

            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ComboBox cmb = (ComboBox)ctrls;
                                    ctrlval = ctrls.Text;
                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue;
                                }

                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrlval.Length == 6)

                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                    else
                                        if (ctrlval.Length == 8)

                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                        else
                                            if (ctrlval.Length == 10)
                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }
                            }

                        }
                    }
                }
            }

            if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;


                if (MatrixReport == true)
                {
                    CallMatrixReport();
                }
                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                                    
                    rptViewer.LoadParameters(dataTable, Credentials());
                    if (Dest_Format == "pdf" || Dest_Format == "dflt" || Dest_Format == "PDF")
                   {
                        //rptViewer.crDoc.ExportToDisk(ExportFormatType.PortableDocFormat, DestinationFile/*@"C:\crystalreport.pdf" */);
                        //rptViewer.crDoc.Close();
                        //rptViewer.crDoc.Dispose();
                        //rptViewer.crDoc = null;
                        //rptViewer.Close();
                        //rptViewer.Dispose();
                        //rptViewer = null;

                       rptViewer.crDoc.ExportToDisk(ExportFormatType.PortableDocFormat, DestinationFile);
                       rptViewer.crDoc.Close();
                       rptViewer.crDoc.Dispose();
                       rptViewer.crDoc = null;
                       rptViewer.Close();
                       rptViewer.Dispose();
                       rptViewer = null;
                        string path = DestinationFile.Replace(".pdf", "_.pdf");
                        Stream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
                        PdfReader pdfReader = new PdfReader(DestinationFile);
                        PdfEncryptor.Encrypt(pdfReader, stream, true, FilePassword, null, 2052);
                        stream.Close();
                        stream.Dispose();
                        pdfReader.Close();
                        pdfReader.Dispose();
                        FileSecurity accessControl = File.GetAccessControl(DestinationFile);
                        File.Delete(DestinationFile);

                        
                        
                        //if (SendMail(DestinationFile, strProfileName, strSubject, strBody, strToEmailAdd))
                        //{
                        //    File.Delete(DestinationFile);
                        //}
                    }
                }
            }
        }

        private static bool SendMail(string DestinationFile, string strProfileName, string strSubject, string strBody, string strToEmailAdd)
        {
            try
            {
                CmnDataManager cmnDM = new CmnDataManager();
                
                Dictionary<string,object> dictParam = new Dictionary<string,object>();
                
                dictParam.Add("profile_name1", strProfileName);
                dictParam.Add("file_attachments1", DestinationFile);
                dictParam.Add("recipients1", strToEmailAdd);
                dictParam.Add("subject1", strSubject);
                dictParam.Add("body1", strBody);
                
                Result res = cmnDM.GetData("CHRIS_SP_SEND_EMAIL","",dictParam);
                if (res.isSuccessful)
                {
                    return true;
                }
                return false;
                //Microsoft.Office.Interop.Outlook.Application app;
                //Microsoft.Office.Interop.Outlook._NameSpace appNameSpace;
                //Microsoft.Office.Interop.Outlook.MailItem memo;

                //app = new Microsoft.Office.Interop.Outlook.Application();
                //appNameSpace = app.GetNamespace("MAPI");
                //appNameSpace.Logon(null, null, false, false);
                //memo = (Microsoft.Office.Interop.Outlook.MailItem)app.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);
                ////' memo.To = "sendto@emailaddress.com"
                //// memo.Subject = "Export crystal report to email"
                ////' memo.Body = "Will go to the body of the email"
                //memo.Subject = strSubject;
                //memo.To = strToEmailAdd;
                //memo.Attachments.Add(DestinationFile, Type.Missing, Type.Missing, Type.Missing);
                //memo.Body = strBody;
                //memo.Send();
                
                //MailMessage msg = new MailMessage();
                //msg.From = new MailAddress(strFrom);
                //msg.To.Add(new MailAddress(strToEmailAdd));
                //msg.Subject = strSubject;
                //msg.Body = strBody;
                //msg.Attachments.Add((new System.Net.Mail.Attachment(DestinationFile)));
                //SmtpClient objSMTP = new SmtpClient(strSMTPServer);
                //objSMTP.Send(msg);
                //msg.Dispose();
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }
        static bool FileInUse(string path)
        {
            try
            {
                //Just opening the file as open/create
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    //If required we can check for read/write by using fs.CanRead or fs.CanWrite
                }
                return false;
            }
            catch (IOException ex)
            {
                //check if message is for a File IO
                __message = ex.Message.ToString();
                if (__message.Contains("The process cannot access the file"))
                    return true;
                else
                    throw;
            }
        }
        /// <summary>
        /// Created by Nida Nazir.Used for export to text file format 
        /// </summary>
        /// <param name="DestinationFile"></param>
        /// <param name="Dest_Format"></param>
        /// <param name="sender"></param>
        public void ExportCustomReportToTXT(string DestinationFile, string Dest_Format)
        {
            int pindex = 0;
            string Path = "";
            DestinationFile = DestinationFile + ".rtf";
            if (DestinationFile != string.Empty)
            {
                
                pindex = DestinationFile.LastIndexOf(@"\");
                Path = DestinationFile.Substring(0, pindex);
            }
            apppath = apppath.ToUpper();

            // m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\")); Commented by Faisal Iqbal

            #region Code By Faisal
#if PRODUCTION
            m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
#endif
#if DEV
            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
#endif
            #endregion


       
            ReportViewer rptViewer = new ReportViewer();


            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";

            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ComboBox cmb = (ComboBox)ctrls;

                                    ctrlval = ctrls.Text;
                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue;
                                }

                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrlval.Length == 6)

                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                    else
                                        if (ctrlval.Length == 8)

                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                        else
                                            if (ctrlval.Length == 10)
                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }
                            }

                        }
                    }
                }
            }


            if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;


                if (MatrixReport == true)
                {
                    CallMatrixReport();
                }

                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                    rptViewer.LoadParameters(dataTable, Credentials());
                   // rptViewer.crystalReportViewer1.PrintReport();
                    // rptViewer.crystalReportViewer1.PrintReport();

                    if (Dest_Format == "txt" || Dest_Format == "TXT")
                    {
                        if(!Directory.Exists(Path))//we r checking the folder "gg",already available in drive c: or not
                        {
                        System.IO.Directory.CreateDirectory(Path);
                        //System.IO.File.Create("C:\\gg//textbox1.text");//textbox1 having the file name.
                        }
                        //else
                        //{
                        //response.write("ALREADY FOLDER EXIST");
                        //}
                      



                        ExportOptions CrExportOptions;
                        DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                        PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                        CrDiskFileDestinationOptions.DiskFileName = DestinationFile;

                        CrExportOptions = rptViewer.crDoc.ExportOptions;
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.RichText;

                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;
                        rptViewer.crDoc.ExportToDisk(ExportFormatType.RichText, CrDiskFileDestinationOptions.DiskFileName);
                        rptViewer.Refresh();

                    }


                }

                else
                {
                    MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            }
        }

        public void PrintCustomReport(string noCop)
        {

            int noCopies;
            if (noCop == string.Empty)
                noCopies = 1;
            else
            {
                noCopies = Convert.ToInt16(noCop); 
            }
            apppath = apppath.ToUpper();

            //m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\")); // Commented By Faisal
            #region Code By Faisal
                #if PRODUCTION
                            m_rootPath = apppath.Substring(0, apppath.LastIndexOf("\\"));
                #endif
                #if DEV
                            m_rootPath = apppath.Substring(0, apppath.IndexOf("BIN\\"));
                #endif
            #endregion

            //m_rootPath = apppath.Substring(0, apppath.Length - 12);
            ReportViewer rptViewer = new ReportViewer();


            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";

            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ComboBox cmb = (ComboBox)ctrls;

                                    ctrlval = ctrls.Text;
                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue;
                                }

                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrlval.Length == 6)

                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                    else
                                        if (ctrlval.Length == 8)

                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                        else
                                            if (ctrlval.Length == 10)
                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }
                            }

                        }
                    }
                }
            }


            if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;


                if (MatrixReport == true)
                {
                    CallMatrixReport();
                }
                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                    rptViewer.LoadParameters(dataTable, Credentials());
                    //rptViewer.LoadParameters(dataTable, Credentials());
                    //rptViewer.crystalReportViewer1.PrintReport();                    
                    //rptViewer = null;



                    PrintDialog ps = new PrintDialog();
                    ps.PrinterSettings.Copies = Convert.ToInt16 ( noCopies);
                    ps.UseEXDialog = true;
                    DialogResult result = ps.ShowDialog();

                    if (result == DialogResult.OK)
                    {
                        //rptViewer.crDoc.PrintOptions.PrinterName=ps.
                        rptViewer.crDoc.PrintOptions.PrinterName = ps.PrinterSettings.PrinterName;

                        rptViewer.crDoc.PrintToPrinter(noCopies, false, 0, 0);

                    }
                    //rptViewer.ShowDialog(this);
                    rptViewer = null;

                }




                else
                {
                    MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            }
        }


        /*Added By Umair on 23 Jun 2011--  Export Report To Excel-- It create the Excel Sheet on C:\ ReportName.Xls*/
        private void Excel_FromDataTable(DataTable dt, string ReportName)
        {
            // Create an Excel object and add workbook...
            Excel.ApplicationClass excel    = new Excel.ApplicationClass();
            Excel.Workbook workbook         = excel.Application.Workbooks.Add(true);

            // Add column headings...
            int iCol = 0;
            foreach (DataColumn c in dt.Columns)
            {
                iCol++;
                excel.Cells[1, iCol] = c.ColumnName;
            }
            // for each row of data...
            int iRow = 0;
            foreach (DataRow r in dt.Rows)
            {
                iRow++;

                // add each row's cell data...
                iCol = 0;
                foreach (DataColumn c in dt.Columns)
                {
                    iCol++;
                    excel.Cells[iRow + 1, iCol] = r[c.ColumnName];
                }
            }

            // Global missing reference for objects we are not defining...
            object missing = System.Reflection.Missing.Value;

            //string RptDestination =   base.m_rptFile + "xls";
            // If wanting to Save the workbook...
            workbook.SaveAs("C:\\iCORE-Spool\\" + ReportName + ".xls",
                Excel.XlFileFormat.xlXMLSpreadsheet, missing, missing,
                false, false, Excel.XlSaveAsAccessMode.xlNoChange,
                missing, missing, missing, missing, missing);

            // If wanting to make Excel visible and activate the worksheet...
            excel.Visible = false;
            Excel.Worksheet worksheet = (Excel.Worksheet)excel.ActiveSheet;
            ((Excel._Worksheet)worksheet).Activate();

            // If wanting excel to shutdown...
            ((Excel._Application)excel).Quit();
        }

        private Excel.Worksheet Custom_Excel_FromDataTable(Excel.Worksheet oSheet,  DataTable dt, string ReportName)
        {
            try
            {
                // Create an Excel object and add workbook...
                //Excel.ApplicationClass excel = new Excel.ApplicationClass();
                //Excel.Workbook workbook = excel.Application.Workbooks.Add(true);

                // Add column headings...

                if (dt != null)
                {
                    int iCol = 0;
                    foreach (DataColumn c in dt.Columns)
                    {
                        iCol++;
                        oSheet.Cells[1, iCol] = c.ColumnName;
                    }
                    // for each row of data...
                    int iRow = 0;
                    foreach (DataRow r in dt.Rows)
                    {
                        iRow++;

                        // add each row's cell data...
                        iCol = 0;
                        foreach (DataColumn c in dt.Columns)
                        {
                            iCol++;
                            oSheet.Cells[iRow + 1, iCol] = r[c.ColumnName];
                        }
                    }
                }
                return oSheet;
            }
            catch (System.Exception exp)
            {
                //LogError(this, "Custom_Excel_FromDataTable", exp);
                return oSheet;
            }
        }
    }
}



