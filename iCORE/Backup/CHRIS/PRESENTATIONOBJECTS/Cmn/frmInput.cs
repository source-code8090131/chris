using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn
{
    public partial class frmInput : Form
    {
        #region Constructors

        private frmInput()
        {
            InitializeComponent();
        }
        public frmInput(string lableCaption, CrplControlLibrary.TextType textType)
        : this()
        {
            this.lblCaption.Text = lableCaption;
            this.txtVal.TextType = textType;
        }

        #endregion

        #region Code to intercept the Close Box (X button), which will call Validating routines

        protected override void WndProc(ref Message msg)
        {
            if (msg.Msg == (int)0x0112)
            {
                if (msg.WParam.ToInt32() == (int)0xf060)
                {
                    this.AutoValidate = AutoValidate.Disable;
                }
            }
            base.WndProc(ref msg);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.AutoValidate = AutoValidate.Disable;
            base.OnClosing(e);
        }

        #endregion

        #region Properties

        public String Value
        {
            get
            {
                return this.txtVal.Text;
            }
        }
        public CrplControlLibrary.SLTextBox TextBox
        {
           get
            {
                return this.txtVal;
            }
        }

        #endregion

        
    }
}