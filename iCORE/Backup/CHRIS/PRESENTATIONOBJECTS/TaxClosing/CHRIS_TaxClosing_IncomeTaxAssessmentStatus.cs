using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
/*data is not entered and data is slow*/
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    public partial class CHRIS_TaxClosing_IncomeTaxAssessmentStatus : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_TaxClosing_IncomeTaxAssessmentStatus()
        {
            InitializeComponent();
        }
        public CHRIS_TaxClosing_IncomeTaxAssessmentStatus(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }
        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.Copies.Text = "1";
            this.BRANCH.Text = "ALL";
            this.DestFormat.Text = "dflt";
            this.W_CHOICE.Text = "I";
            //Output_mod.Items.RemoveAt(3);
        }

        private void Run_Click_1(object sender, EventArgs e)
        {
            base.RptFileName = "Atr10";

            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (Dest_Type.Text == "Printer")
            {
                base.PrintCustomReport(this.Copies.Text );
            }
            else if (Dest_Type.Text == "File")
            {

                string Dest_name;
                string DestFormat;

                Dest_name = @"C:\iCORE-Spool\Report";

                if (Dest_name != string.Empty)
                {
                    base.ExportCustomReport(Dest_name, "PDF");
                }
            }
            else if (Dest_Type.Text == "Mail")
            {
                string Dest_name = @"C:\iCORE-Spool\Report";
                string DestFormat;
                string RecipentName;
                if (Dest_name != string.Empty)
                {
                    base.EmailToReport(Dest_name, "PDF");
                }
            }
        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }
        #endregion 


    }
}