using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    public partial class CHRIS_TaxClosing_DeductionLetterReport : BaseRptForm
    {
        public CHRIS_TaxClosing_DeductionLetterReport()
        {
            InitializeComponent();
        }
        public CHRIS_TaxClosing_DeductionLetterReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }
        string DestFormat;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(Dest_Type.Items.Count-1);
            
            this.copies.Text = "1";
            this.Dest_format.Text = "dflt";
            this.FNAME.Text = "Asad Ali";
            this.W_SPNO.Text = "41";
            this.W_EPNO.Text = "41";
            this.FDESIG.Text = "Residence Vice President";
            this.W_BRN.Text = "ALL";
            this.fn_from_date.Value = new DateTime(2010, 7, 1);
            this.fn_to_date.Value = new DateTime(2011, 6, 30);


        }

        private void RUN_Click(object sender, EventArgs e)
        {
            base.RptFileName = "ATR03";
            if (this.Dest_format.Text == String.Empty)
            {
                DestFormat = @"PDF";
            }
            else
            {
                DestFormat = this.Dest_format.Text;
            }
            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (Dest_Type.Text == "Printer")
            {
                base.PrintCustomReport(this.copies.Text );
            }
            else if (Dest_Type.Text == "File")
            {

                string DestName;

                if (Dest_Type.Text == string.Empty)
                {
                    DestName = @"C:\iCORE-Spool\Report";
                }

                else
                {
                    DestName = this.Dest_name.Text;
                }

                if (DestName != string.Empty)
                {
                    base.ExportCustomReport(DestName, DestFormat);
                }
            }
            else if (Dest_Type.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";

                string RecipentName;
                if (this.Dest_name.Text == string.Empty)
                {
                    RecipentName = "";
                }

                else
                {
                    RecipentName = this.Dest_name.Text;
                }

                if (DestName != string.Empty)
                {
                    base.EmailToReport(DestName, DestFormat, RecipentName);
                }
            }


        }

        private void CLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }

}