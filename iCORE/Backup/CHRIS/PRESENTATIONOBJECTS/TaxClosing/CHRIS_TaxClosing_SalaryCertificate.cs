using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    public partial class CHRIS_TaxClosing_SalaryCertificate : BaseRptForm
    {
        public CHRIS_TaxClosing_SalaryCertificate()
        {
            InitializeComponent();
        }
        public CHRIS_TaxClosing_SalaryCertificate(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            MODE.Items.RemoveAt(3);
            this.copies.Text = "1";
            this.Dest_format.Text = "dflt";
            this.BRANCH.Text = "ALL";
            this.FPNO.Text = "1";
            this.TPNO.Text = "999999";
            this.SIGN.Text = "Nisar Abbas Thaverani";
            this.DESIG.Text = "Manager";
            //Output_mod.Items.RemoveAt(3);
        }

        private void RUN_Click(object sender, EventArgs e)
        {
            {
                base.RptFileName = "ATR04";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    //if (Dest_Format.Text!= string.Empty)
                    //{
                    //   base.ExportCustomReport();
                    //}
                }
                if (Dest_Type.Text == "Printer")
                {
                    //base.MatrixReport = true;
                    ///if (Dest_Format.Text!= string.Empty)
                    ///{
                    ///base.ExportCustomReport();
                    /// }
                    base.PrintCustomReport(this.copies.Text );
                    //base.ExportCustomReport();
                    //DataSet ds = base.PrintCustomReport();

                }

                if (Dest_Type.Text == "File")
                {
                    if (Dest_format.Text != string.Empty || Dest_name.Text != string.Empty)
                    {


                        base.ExportCustomReport(Dest_name.Text, Dest_format.Text);



                    }

                }

                if (Dest_Type.Text == "Mail")
                {
                    base.EmailToReport("C:\\iCORE-Spool\\Report", "PDF");
                }
                //if (Dest_Format.Text != string.Empty && Dest_Format.Text == "PDF")
                //{
                //    string Path =base.ExportReportInGivenFormat("PDF");

                //}


            }


        }

        private void CLOSE_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }



        #endregion 



    }
}