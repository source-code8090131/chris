namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    partial class CHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gboProcessingTaxFor = new System.Windows.Forms.GroupBox();
            this.slPnlProcessingTaxFor = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.sltxtPR_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.sltxtPR_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.gboProcessingTaxFor.SuspendLayout();
            this.slPnlProcessingTaxFor.SuspendLayout();
            this.SuspendLayout();
            // 
            // gboProcessingTaxFor
            // 
            this.gboProcessingTaxFor.Controls.Add(this.slPnlProcessingTaxFor);
            this.gboProcessingTaxFor.Location = new System.Drawing.Point(12, 61);
            this.gboProcessingTaxFor.Name = "gboProcessingTaxFor";
            this.gboProcessingTaxFor.Size = new System.Drawing.Size(368, 60);
            this.gboProcessingTaxFor.TabIndex = 11;
            this.gboProcessingTaxFor.TabStop = false;
            // 
            // slPnlProcessingTaxFor
            // 
            this.slPnlProcessingTaxFor.ConcurrentPanels = null;
            this.slPnlProcessingTaxFor.Controls.Add(this.sltxtPR_NAME);
            this.slPnlProcessingTaxFor.Controls.Add(this.sltxtPR_P_NO);
            this.slPnlProcessingTaxFor.DataManager = null;
            this.slPnlProcessingTaxFor.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlProcessingTaxFor.DependentPanels = null;
            this.slPnlProcessingTaxFor.DisableDependentLoad = false;
            this.slPnlProcessingTaxFor.EnableDelete = true;
            this.slPnlProcessingTaxFor.EnableInsert = true;
            this.slPnlProcessingTaxFor.EnableQuery = false;
            this.slPnlProcessingTaxFor.EnableUpdate = true;
            this.slPnlProcessingTaxFor.EntityName = null;
            this.slPnlProcessingTaxFor.Location = new System.Drawing.Point(6, 14);
            this.slPnlProcessingTaxFor.MasterPanel = null;
            this.slPnlProcessingTaxFor.Name = "slPnlProcessingTaxFor";
            this.slPnlProcessingTaxFor.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlProcessingTaxFor.Size = new System.Drawing.Size(356, 37);
            this.slPnlProcessingTaxFor.SPName = null;
            this.slPnlProcessingTaxFor.TabIndex = 10;
            // 
            // sltxtPR_NAME
            // 
            this.sltxtPR_NAME.AllowSpace = true;
            this.sltxtPR_NAME.AssociatedLookUpName = "";
            this.sltxtPR_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sltxtPR_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.sltxtPR_NAME.ContinuationTextBox = null;
            this.sltxtPR_NAME.CustomEnabled = true;
            this.sltxtPR_NAME.DataFieldMapping = "PR_NAME";
            this.sltxtPR_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sltxtPR_NAME.GetRecordsOnUpDownKeys = false;
            this.sltxtPR_NAME.IsDate = false;
            this.sltxtPR_NAME.Location = new System.Drawing.Point(84, 9);
            this.sltxtPR_NAME.Name = "sltxtPR_NAME";
            this.sltxtPR_NAME.NumberFormat = "###,###,##0.00";
            this.sltxtPR_NAME.Postfix = "";
            this.sltxtPR_NAME.Prefix = "";
            this.sltxtPR_NAME.ReadOnly = true;
            this.sltxtPR_NAME.Size = new System.Drawing.Size(264, 20);
            this.sltxtPR_NAME.SkipValidation = false;
            this.sltxtPR_NAME.TabIndex = 1;
            this.sltxtPR_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // sltxtPR_P_NO
            // 
            this.sltxtPR_P_NO.AllowSpace = true;
            this.sltxtPR_P_NO.AssociatedLookUpName = "";
            this.sltxtPR_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sltxtPR_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.sltxtPR_P_NO.ContinuationTextBox = null;
            this.sltxtPR_P_NO.CustomEnabled = true;
            this.sltxtPR_P_NO.DataFieldMapping = "PR_P_NO";
            this.sltxtPR_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sltxtPR_P_NO.GetRecordsOnUpDownKeys = false;
            this.sltxtPR_P_NO.IsDate = false;
            this.sltxtPR_P_NO.Location = new System.Drawing.Point(8, 9);
            this.sltxtPR_P_NO.Name = "sltxtPR_P_NO";
            this.sltxtPR_P_NO.NumberFormat = "###,###,##0.00";
            this.sltxtPR_P_NO.Postfix = "";
            this.sltxtPR_P_NO.Prefix = "";
            this.sltxtPR_P_NO.ReadOnly = true;
            this.sltxtPR_P_NO.Size = new System.Drawing.Size(73, 20);
            this.sltxtPR_P_NO.SkipValidation = false;
            this.sltxtPR_P_NO.TabIndex = 0;
            this.sltxtPR_P_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(112, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "PROCESSING TAX FOR";
            // 
            // CHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 126);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gboProcessingTaxFor);
            this.Name = "CHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor";
            this.Text = "Tax Year End Closing Process";
            this.Shown += new System.EventHandler(this.CHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor_Shown);
            this.Controls.SetChildIndex(this.gboProcessingTaxFor, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.gboProcessingTaxFor.ResumeLayout(false);
            this.slPnlProcessingTaxFor.ResumeLayout(false);
            this.slPnlProcessingTaxFor.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPnlProcessingTaxFor;
        private System.Windows.Forms.GroupBox gboProcessingTaxFor;
        private CrplControlLibrary.SLTextBox sltxtPR_NAME;
        private CrplControlLibrary.SLTextBox sltxtPR_P_NO;
        private System.Windows.Forms.Label label1;

    }
}