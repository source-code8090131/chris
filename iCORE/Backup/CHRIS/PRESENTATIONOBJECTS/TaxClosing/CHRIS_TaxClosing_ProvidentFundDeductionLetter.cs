using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
/*data is not entered and data is slow*/
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    public partial class CHRIS_TaxClosing_ProvidentFundDeductionLetter : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_TaxClosing_ProvidentFundDeductionLetter()
        {
            InitializeComponent();
        }
        public CHRIS_TaxClosing_ProvidentFundDeductionLetter(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }


        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.Dest_format.Text = "dflt";
            this.copies.Text = "1";
            this.wf_pno.Text = "1";
            this.wt_pno.Text = "9999";
            this.w_sig.Text = "Asad Ali";
            this.w_desig.Text = "Resident Vice President";
            this.w_brn.Text = "ALL";
            this.hr.Text = "Human Resources";
            //Output_mod.Items.RemoveAt(3);
        }

              

        private void run_Click(object sender, EventArgs e)
        {
            base.RptFileName = "Atr02";

            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (Dest_Type.Text == "Printer")
            {
                base.PrintCustomReport(this.copies.Text );
            }
            else if (Dest_Type.Text == "File")
            {

                string Dest_name;
                string DestFormat;

                Dest_name = @"C:\iCORE-Spool\Report";

                if (Dest_name != string.Empty)
                {
                    base.ExportCustomReport(Dest_name, "PDF");
                }
            }
            else if (Dest_Type.Text == "Mail")
            {
                string Dest_name = @"C:\iCORE-Spool\Report";
                string DestFormat;
                string RecipentName;
                if (Dest_name != string.Empty)
                {
                    base.EmailToReport(Dest_name, "PDF");
                }
            }
        }

        private void close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        #endregion 


    }
}