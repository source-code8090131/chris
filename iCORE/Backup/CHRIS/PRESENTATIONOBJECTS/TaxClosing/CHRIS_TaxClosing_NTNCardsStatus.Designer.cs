namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    partial class CHRIS_TaxClosing_NTNCardsStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_TaxClosing_NTNCardsStatus));
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.Close = new System.Windows.Forms.Button();
            this.Run = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.W_CERT = new CrplControlLibrary.SLTextBox(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DestFormat = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.Copies = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(225, 123);
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(152, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 2;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // YEAR
            // 
            this.YEAR.AllowSpace = true;
            this.YEAR.AssociatedLookUpName = "";
            this.YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.YEAR.ContinuationTextBox = null;
            this.YEAR.CustomEnabled = true;
            this.YEAR.DataFieldMapping = "";
            this.YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YEAR.GetRecordsOnUpDownKeys = false;
            this.YEAR.IsDate = false;
            this.YEAR.Location = new System.Drawing.Point(225, 225);
            this.YEAR.MaxLength = 4;
            this.YEAR.Name = "YEAR";
            this.YEAR.NumberFormat = "###,###,##0.00";
            this.YEAR.Postfix = "";
            this.YEAR.Prefix = "";
            this.YEAR.Size = new System.Drawing.Size(152, 20);
            this.YEAR.SkipValidation = false;
            this.YEAR.TabIndex = 6;
            this.YEAR.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // BRANCH
            // 
            this.BRANCH.AllowSpace = true;
            this.BRANCH.AssociatedLookUpName = "";
            this.BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.BRANCH.ContinuationTextBox = null;
            this.BRANCH.CustomEnabled = true;
            this.BRANCH.DataFieldMapping = "";
            this.BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BRANCH.GetRecordsOnUpDownKeys = false;
            this.BRANCH.IsDate = false;
            this.BRANCH.Location = new System.Drawing.Point(225, 199);
            this.BRANCH.MaxLength = 3;
            this.BRANCH.Name = "BRANCH";
            this.BRANCH.NumberFormat = "###,###,##0.00";
            this.BRANCH.Postfix = "";
            this.BRANCH.Prefix = "";
            this.BRANCH.Size = new System.Drawing.Size(152, 20);
            this.BRANCH.SkipValidation = false;
            this.BRANCH.TabIndex = 5;
            this.BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // Close
            // 
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(306, 282);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 9;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Run
            // 
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(225, 282);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 8;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(29, 253);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(167, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "NTN Card Recieved (Y/N) ?";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(29, 227);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "Year";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(29, 201);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "Branch";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 42;
            this.label3.Text = "Number of Copies";
            // 
            // W_CERT
            // 
            this.W_CERT.AllowSpace = true;
            this.W_CERT.AssociatedLookUpName = "";
            this.W_CERT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_CERT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_CERT.ContinuationTextBox = null;
            this.W_CERT.CustomEnabled = true;
            this.W_CERT.DataFieldMapping = "";
            this.W_CERT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_CERT.GetRecordsOnUpDownKeys = false;
            this.W_CERT.IsDate = false;
            this.W_CERT.Location = new System.Drawing.Point(225, 251);
            this.W_CERT.MaxLength = 1;
            this.W_CERT.Name = "W_CERT";
            this.W_CERT.NumberFormat = "###,###,##0.00";
            this.W_CERT.Postfix = "";
            this.W_CERT.Prefix = "";
            this.W_CERT.Size = new System.Drawing.Size(152, 20);
            this.W_CERT.SkipValidation = false;
            this.W_CERT.TabIndex = 7;
            this.W_CERT.TextType = CrplControlLibrary.TextType.String;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.DestFormat);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.YEAR);
            this.groupBox1.Controls.Add(this.BRANCH);
            this.groupBox1.Controls.Add(this.Close);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.W_CERT);
            this.groupBox1.Controls.Add(this.Copies);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(6, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 356);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(380, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 65);
            this.pictureBox1.TabIndex = 133;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "Destination Type";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 60;
            this.label1.Text = "Destination Name";
            // 
            // DestFormat
            // 
            this.DestFormat.AllowSpace = true;
            this.DestFormat.AssociatedLookUpName = "";
            this.DestFormat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DestFormat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.DestFormat.ContinuationTextBox = null;
            this.DestFormat.CustomEnabled = true;
            this.DestFormat.DataFieldMapping = "";
            this.DestFormat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DestFormat.GetRecordsOnUpDownKeys = false;
            this.DestFormat.IsDate = false;
            this.DestFormat.Location = new System.Drawing.Point(225, 149);
            this.DestFormat.Name = "DestFormat";
            this.DestFormat.NumberFormat = "###,###,##0.00";
            this.DestFormat.Postfix = "";
            this.DestFormat.Prefix = "";
            this.DestFormat.Size = new System.Drawing.Size(152, 20);
            this.DestFormat.SkipValidation = false;
            this.DestFormat.TabIndex = 3;
            this.DestFormat.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(29, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Destination Format";
            // 
            // Copies
            // 
            this.Copies.AllowSpace = true;
            this.Copies.AssociatedLookUpName = "";
            this.Copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Copies.ContinuationTextBox = null;
            this.Copies.CustomEnabled = true;
            this.Copies.DataFieldMapping = "";
            this.Copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Copies.GetRecordsOnUpDownKeys = false;
            this.Copies.IsDate = false;
            this.Copies.Location = new System.Drawing.Point(225, 175);
            this.Copies.MaxLength = 2;
            this.Copies.Name = "Copies";
            this.Copies.NumberFormat = "###,###,##0.00";
            this.Copies.Postfix = "";
            this.Copies.Prefix = "";
            this.Copies.Size = new System.Drawing.Size(152, 20);
            this.Copies.SkipValidation = false;
            this.Copies.TabIndex = 4;
            this.Copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(225, 96);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(152, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(91, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(225, 16);
            this.label9.TabIndex = 12;
            this.label9.Text = "Enter values for the Parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(133, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 16);
            this.label8.TabIndex = 11;
            this.label8.Text = "Report Parameters";
            // 
            // CHRIS_TaxClosing_NTNCardsStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 410);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_TaxClosing_NTNCardsStatus";
            this.Text = "iCORE CHRIS - NTN Cards Status";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox Dest_name;
        private CrplControlLibrary.SLTextBox YEAR;
        private CrplControlLibrary.SLTextBox BRANCH;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button Run;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox W_CERT;
        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLTextBox Copies;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox DestFormat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}