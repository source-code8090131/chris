namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    partial class CHRIS_TaxClosing_ProvidentFundEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_TaxClosing_ProvidentFundEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gboBalanceUpdation = new System.Windows.Forms.GroupBox();
            this.slPnlTabularBalanceUpdation = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.slDgvBalanceUpdation = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.F_P_NO = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.F_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AN_PF_BAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AN_PFUND = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.gboBalanceUpdation.SuspendLayout();
            this.slPnlTabularBalanceUpdation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDgvBalanceUpdation)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 608);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // gboBalanceUpdation
            // 
            this.gboBalanceUpdation.Controls.Add(this.slPnlTabularBalanceUpdation);
            this.gboBalanceUpdation.Location = new System.Drawing.Point(17, 175);
            this.gboBalanceUpdation.Name = "gboBalanceUpdation";
            this.gboBalanceUpdation.Size = new System.Drawing.Size(634, 427);
            this.gboBalanceUpdation.TabIndex = 10;
            this.gboBalanceUpdation.TabStop = false;
            // 
            // slPnlTabularBalanceUpdation
            // 
            this.slPnlTabularBalanceUpdation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPnlTabularBalanceUpdation.ConcurrentPanels = null;
            this.slPnlTabularBalanceUpdation.Controls.Add(this.slDgvBalanceUpdation);
            this.slPnlTabularBalanceUpdation.DataManager = null;
            this.slPnlTabularBalanceUpdation.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlTabularBalanceUpdation.DependentPanels = null;
            this.slPnlTabularBalanceUpdation.DisableDependentLoad = false;
            this.slPnlTabularBalanceUpdation.EnableDelete = true;
            this.slPnlTabularBalanceUpdation.EnableInsert = true;
            this.slPnlTabularBalanceUpdation.EnableQuery = false;
            this.slPnlTabularBalanceUpdation.EnableUpdate = true;
            this.slPnlTabularBalanceUpdation.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AnnualTaxCommand";
            this.slPnlTabularBalanceUpdation.Location = new System.Drawing.Point(6, 19);
            this.slPnlTabularBalanceUpdation.MasterPanel = null;
            this.slPnlTabularBalanceUpdation.Name = "slPnlTabularBalanceUpdation";
            this.slPnlTabularBalanceUpdation.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlTabularBalanceUpdation.Size = new System.Drawing.Size(622, 402);
            this.slPnlTabularBalanceUpdation.SPName = "CHRIS_SP_BALANCEUPDATION_ANNUAL_TAX_MANAGER";
            this.slPnlTabularBalanceUpdation.TabIndex = 0;
            // 
            // slDgvBalanceUpdation
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.slDgvBalanceUpdation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.slDgvBalanceUpdation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slDgvBalanceUpdation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F_P_NO,
            this.F_NAME,
            this.AN_PF_BAL,
            this.AN_PFUND,
            this.ID});
            this.slDgvBalanceUpdation.ColumnToHide = null;
            this.slDgvBalanceUpdation.ColumnWidth = null;
            this.slDgvBalanceUpdation.CustomEnabled = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.slDgvBalanceUpdation.DefaultCellStyle = dataGridViewCellStyle6;
            this.slDgvBalanceUpdation.DisplayColumnWrapper = null;
            this.slDgvBalanceUpdation.GridDefaultRow = 0;
            this.slDgvBalanceUpdation.Location = new System.Drawing.Point(3, 3);
            this.slDgvBalanceUpdation.Name = "slDgvBalanceUpdation";
            this.slDgvBalanceUpdation.ReadOnlyColumns = null;
            this.slDgvBalanceUpdation.RequiredColumns = null;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.slDgvBalanceUpdation.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.slDgvBalanceUpdation.Size = new System.Drawing.Size(614, 394);
            this.slDgvBalanceUpdation.SkippingColumns = null;
            this.slDgvBalanceUpdation.TabIndex = 0;
            this.slDgvBalanceUpdation.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.slDgvBalanceUpdation_CellValidating);
            // 
            // F_P_NO
            // 
            this.F_P_NO.ActionLOV = "AnnualTaxLOV";
            this.F_P_NO.ActionLOVExists = "AnnualTaxLOVExist";
            this.F_P_NO.AttachParentEntity = false;
            this.F_P_NO.DataPropertyName = "AN_P_NO";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.NullValue = null;
            this.F_P_NO.DefaultCellStyle = dataGridViewCellStyle2;
            this.F_P_NO.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AnnualTaxCommand";
            this.F_P_NO.HeaderText = "P.NO.";
            this.F_P_NO.LookUpTitle = "Personnel No.";
            this.F_P_NO.LOVFieldMapping = "AN_P_NO";
            this.F_P_NO.MaxInputLength = 6;
            this.F_P_NO.Name = "F_P_NO";
            this.F_P_NO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.F_P_NO.SearchColumn = "AN_P_NO";
            this.F_P_NO.SkipValidationOnLeave = false;
            this.F_P_NO.SpName = "CHRIS_SP_BALANCEUPDATION_ANNUAL_TAX_MANAGER";
            this.F_P_NO.Width = 70;
            // 
            // F_NAME
            // 
            this.F_NAME.DataPropertyName = "AN_P_NAME";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F_NAME.DefaultCellStyle = dataGridViewCellStyle3;
            this.F_NAME.HeaderText = "Name";
            this.F_NAME.MaxInputLength = 50;
            this.F_NAME.Name = "F_NAME";
            this.F_NAME.ReadOnly = true;
            this.F_NAME.Width = 360;
            // 
            // AN_PF_BAL
            // 
            this.AN_PF_BAL.DataPropertyName = "AN_PF_BAL";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.AN_PF_BAL.DefaultCellStyle = dataGridViewCellStyle4;
            this.AN_PF_BAL.HeaderText = "P.Fund C/F Amount";
            this.AN_PF_BAL.MaxInputLength = 14;
            this.AN_PF_BAL.Name = "AN_PF_BAL";
            this.AN_PF_BAL.Width = 120;
            // 
            // AN_PFUND
            // 
            this.AN_PFUND.DataPropertyName = "AN_PFUND";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.AN_PFUND.DefaultCellStyle = dataGridViewCellStyle5;
            this.AN_PFUND.HeaderText = "P.Fund This Year";
            this.AN_PFUND.MaxInputLength = 14;
            this.AN_PFUND.Name = "AN_PFUND";
            this.AN_PFUND.ReadOnly = true;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(44, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 103;
            this.label5.Text = "Location :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(44, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 102;
            this.label4.Text = "User :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(468, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 105;
            this.label8.Text = "Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(261, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 107;
            this.label1.Text = "Annual Tax System";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(253, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 13);
            this.label2.TabIndex = 108;
            this.label2.Text = "P.F. Balance Updation";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(520, 108);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(100, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 106;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "W_LOC";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(113, 136);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(100, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 101;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "W_USER";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(113, 113);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(100, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 104;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(455, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 109;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_TaxClosing_ProvidentFundEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 668);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.gboBalanceUpdation);
            this.CurrentPanelBlock = "slPnlTabularBalanceUpdation";
            this.Name = "CHRIS_TaxClosing_ProvidentFundEntry";
            this.ShowBottomBar = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_TaxClosing_ProvidentFundEntry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.gboBalanceUpdation, 0);
            this.Controls.SetChildIndex(this.txtUser, 0);
            this.Controls.SetChildIndex(this.txtLocation, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.txtDate, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.gboBalanceUpdation.ResumeLayout(false);
            this.slPnlTabularBalanceUpdation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slDgvBalanceUpdation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gboBalanceUpdation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPnlTabularBalanceUpdation;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView slDgvBalanceUpdation;
        private System.Windows.Forms.Label lblUserName;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn F_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn AN_PF_BAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn AN_PFUND;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}