using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
/*data is not entered and data is slow*/
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    public partial class CHRIS_TaxClosing_NTNCertification : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_TaxClosing_NTNCertification()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "dflt";
        public CHRIS_TaxClosing_NTNCertification(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }
      

      #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.Dest_Format.Text = "dflt";
            this.nocopies.Text = "1";
            this.BRANCH.Text = "ALL";
            //Output_mod.Items.RemoveAt(3);
        }
       
      

        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }

            no_of_copies = Convert.ToInt16(nocopies.Text);
           
            {
                base.RptFileName = "Atr07";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    base.PrintNoofCopiesReport(no_of_copies);


                }


                if (Dest_Type.Text == "File")
                {
                    if (DestFormat != string.Empty && Dest_name.Text != string.Empty)
                    {
                        base.ExportCustomReport(Dest_name.Text, DestFormat);
                    }

                }


                if (Dest_Type.Text == "Mail")
                {

                    if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                    {
                        base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF");
                    }
                }


            }
      #region commented code 

            //base.RptFileName = "Atr07";

            //if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            //{
            //    base.btnCallReport_Click(sender, e);
            //}
            //else if (Dest_Type.Text == "Printer")
            //{
            //    base.PrintCustomReport();
            //}
            //else if (Dest_Type.Text == "File")
            //{

            //    string Dest_name;
            //    string DestFormat;

            //    Dest_name = @"C:\Report";

            //    if (Dest_name != string.Empty)
            //    {
            //        base.ExportCustomReport(Dest_name, "PDF");
            //    }
            //}
            //else if (Dest_Type.Text == "Mail")
            //{
            //    string Dest_name = @"C:\Report";
            //    string DestFormat;
            //    string RecipentName;
            //    if (Dest_name != string.Empty)
            //    {
            //        base.EmailToReport(Dest_name, "PDF");
            //    }
            //}
      #endregion commented code
        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }
      #endregion

       

       

        
    }
}