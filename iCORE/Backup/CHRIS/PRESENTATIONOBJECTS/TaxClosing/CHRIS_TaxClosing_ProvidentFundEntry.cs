using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    public partial class CHRIS_TaxClosing_ProvidentFundEntry : ChrisTabularForm
    {

        #region Declaration
        CmnDataManager objCmnDataManager;
        #endregion

        #region Constructor
        public CHRIS_TaxClosing_ProvidentFundEntry()
        {
            InitializeComponent();
        }
        public CHRIS_TaxClosing_ProvidentFundEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
           
        }
        #endregion

        #region Methods
        
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();

                this.SetFormTitle = "";
                this.txtOption.Visible = false;
                this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");

                lblUserName.Text = lblUserName.Text + this.UserName;
                this.txtUser.Text = this.userID;
              
                //Bold DataGridViewHeader
                Font newFontStyle = new Font(slDgvBalanceUpdation.Font, FontStyle.Bold);
                slDgvBalanceUpdation.ColumnHeadersDefaultCellStyle.Font = newFontStyle;

                ((DataGridViewLOVColumn)slDgvBalanceUpdation.Columns["F_P_NO"]).LovDataType = typeof(double);
                
                //slDgvBalanceUpdation.Columns["F_P_NO"].ValueType = typeof(int);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                this.slDgvBalanceUpdation.CancelEdit();
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
            
            if (actionType == "Cancel")
            {
                this.slDgvBalanceUpdation.Select();
                this.slDgvBalanceUpdation.Focus();
                this.slDgvBalanceUpdation.Rows[0].Cells[0].Selected = true;
            }
        }

        void EnableControls(bool isEnable)
        {
            this.tbtList.Enabled = isEnable;
            this.tbtSave.Visible = !isEnable;
            this.tbtDelete.Visible = !isEnable;
        }
        #endregion

        #region Events    

        /// <summary>
        /// Handles DataGridView Cell validation event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slDgvBalanceUpdation_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (slDgvBalanceUpdation.CurrentCell.OwningColumn.Name.ToUpper().Equals("AN_PF_BAL") && slDgvBalanceUpdation.CurrentCell.IsInEditMode)
            {
                if (!slDgvBalanceUpdation.CurrentCell.EditedFormattedValue.ToString().Trim().Equals(String.Empty))
                {
                    if (!base.IsValidExpression(slDgvBalanceUpdation.CurrentCell.EditedFormattedValue.ToString().Replace(",", ""), "([-|+](?=\\d))?(\\d{0,10})(\\.\\d{1,2})?"))
                    {
                        MessageBox.Show("Field must be of form 999,999,999.99.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Cancel = true;
                    }
                }
            }
        }

        //private void slDgvBalanceUpdation_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        //{
        //    #region Commented Code
        //    //    //e.ColumnIndex == 0 && this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View
        ////    if (e.ColumnIndex == 0)
        ////    {
        ////        if (?["F_P_NO"].Value != null)
        ////        {
        ////            #region Fill Annual Tax Detail
        ////            Dictionary<string, object> d = new Dictionary<string, object>();
        ////            d.Add("AN_P_NO", this.slDgvBalanceUpdation.CurrentRow.Cells["F_P_NO"].Value);

        ////            objCmnDataManager = new CmnDataManager();
        ////            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BALANCEUPDATION_ANNUAL_TAX_MANAGER", "AnnualTaxDetail", d);

        ////            if (rsltCode.isSuccessful)
        ////            {

        ////                if (rsltCode.dstResult.Tables.Count > 0)
        ////                {
        ////                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
        ////                    {
        ////                        #region Exist
        ////                        if (rsltCode.dstResult.Tables[0].Rows[0]["an_pf_bal"] != System.DBNull.Value)
        ////                        {
        ////                            this.slDgvBalanceUpdation.CurrentRow.Cells["an_pf_bal"].Value = rsltCode.dstResult.Tables[0].Rows[0]["an_pf_bal"].ToString();
        ////                        }
        ////                        if (rsltCode.dstResult.Tables[0].Rows[0]["an_pfund"] != System.DBNull.Value)
        ////                        {
        ////                            this.slDgvBalanceUpdation.CurrentRow.Cells["an_pfund"].Value = rsltCode.dstResult.Tables[0].Rows[0]["an_pfund"].ToString();
        ////                        }
        ////                        #endregion
        ////                    }
        ////                    else
        ////                    {
        ////                        MessageBox.Show("Invalid Personnel No. .......");
        ////                    }
        ////                }

        ////            }

        ////            #endregion
        ////        }
        //    //    }
        //    #endregion

        //    //slDgvBalanceUpdation.Rows[e.RowIndex].ErrorText = "";
        //    //int newInteger;

        //    //// Don't try to validate the 'new row' until finished 
        //    //// editing since there
        //    //// is not any point in validating its initial value.
        //    //if (slDgvBalanceUpdation.Rows[e.RowIndex].IsNewRow) { return; }
        //    //if (!int.TryParse(e.FormattedValue.ToString(),out newInteger) || newInteger < 0)
        //    //{
        //    //    e.Cancel = true;
        //    //    slDgvBalanceUpdation.Rows[e.RowIndex].ErrorText = "the value must be a non-negative integer";
        //    //}

        //}   
        #endregion

    }
}