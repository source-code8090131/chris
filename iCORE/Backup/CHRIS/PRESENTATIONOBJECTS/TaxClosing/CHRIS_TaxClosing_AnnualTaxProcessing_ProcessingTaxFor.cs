using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    public partial class CHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor : SimpleForm
    {

        #region Constructor
        public CHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor()
        {
            InitializeComponent();
            //this.tlbMain.Visible = false;
        }
        #endregion

        #region Declarations

        /*------For custom call in database------- */
        CmnDataManager objCmnDataManager;

        /*------To store input parameters of StoredProcedure------*/
        Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();

        /*------To store data on form level-------------*/
        Dictionary<string, object> dicEmployeeDetail = new Dictionary<string, object>();

        /*------To contain personnel records,fetched via cursor in oracle-------------*/
        DataTable dtEmployeeDetail = null;

        #endregion

        #region Properties
        DateTime m_W_SYS;
        public DateTime W_SYS
        {
            get { return m_W_SYS; }
            set { m_W_SYS = value; }
        }

        DateTime m_W_DATE;
        public DateTime W_DATE
        {
            get { return m_W_DATE; }
            set { m_W_DATE = value; }
        }

        int m_W_TAX_TO;
        public int W_TAX_TO
        {
            get { return m_W_TAX_TO; }
            set { m_W_TAX_TO = value; }
        }

        int m_W_TAX_FROM;
        public int W_TAX_FROM
        {
            get { return m_W_TAX_FROM; }
            set { m_W_TAX_FROM = value; }
        }
        #endregion

        #region Methods

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            this.tlbMain.Visible = false;
            //StartTaxProcessing();
        }      

        /// <summary>
        /// Initialize block label variables in dictionary
        /// </summary>
        private void InitializeEmployeeDetailDictionary()
        {

            dicEmployeeDetail.Clear();

            dicEmployeeDetail.Add("W_PR_DATE", null);
            dicEmployeeDetail.Add("W_PFUND", 0);
            dicEmployeeDetail.Add("W_ITAX", 0);
            dicEmployeeDetail.Add("W_P_BASIC", 0);
            dicEmployeeDetail.Add("W_FORCAST", 0);
            dicEmployeeDetail.Add("W_DEPT_FT", null);
            dicEmployeeDetail.Add("W_ANS", null);
            dicEmployeeDetail.Add("W_GOVT_ALL", 0);
            dicEmployeeDetail.Add("W_INC_BONUS", 0);
            dicEmployeeDetail.Add("W_TAX_NO", null);
            dicEmployeeDetail.Add("W_ADD2", null);
            dicEmployeeDetail.Add("W_LEVEL", null);
            dicEmployeeDetail.Add("W_CATE", null);
            dicEmployeeDetail.Add("W_CONV_AMT", 0);
            dicEmployeeDetail.Add("W_TAX_PAID", 0);
            dicEmployeeDetail.Add("W_OTHER_ALL", 0);
            dicEmployeeDetail.Add("W_TAXABLE_INC", 0);
            dicEmployeeDetail.Add("W_REBATE", 0);
            dicEmployeeDetail.Add("W_TAX_FRM_AMT", 0);
            dicEmployeeDetail.Add("W_FIX_TAX", 0);
            dicEmployeeDetail.Add("W_REMAIN_AMT", 0);
            dicEmployeeDetail.Add("W_TAX_REMAIN", 0);
            dicEmployeeDetail.Add("W_SUR", 0);
            dicEmployeeDetail.Add("W_FLAG", 0);
            dicEmployeeDetail.Add("W_OT_ASR", 0);
            dicEmployeeDetail.Add("W_PROMOTED", null);
            dicEmployeeDetail.Add("W_SAL_AREAR", 0);
            dicEmployeeDetail.Add("W_BRANCH", null);
            dicEmployeeDetail.Add("W_ANNUAL_TAX", 0);
            dicEmployeeDetail.Add("W_BASIC_AREAR", 0);
            dicEmployeeDetail.Add("W_10_BONUS", 0);
            dicEmployeeDetail.Add("W_ANNUAL_INCOME", 0);
            dicEmployeeDetail.Add("W_ADD1", null);
            dicEmployeeDetail.Add("W_DESIG", null);
            dicEmployeeDetail.Add("W_HOUSE_RENT", 0);
            dicEmployeeDetail.Add("W_ZAKAT", 0);
            dicEmployeeDetail.Add("W_REFUND_AMT", 0);
            dicEmployeeDetail.Add("W_REFUND_FOR", null);
            dicEmployeeDetail.Add("W_GHA_FORCAST", 0);
            dicEmployeeDetail.Add("W_VP", 0);
            dicEmployeeDetail.Add("W_SEX", null);
            dicEmployeeDetail.Add("W_GHA", 0);


            //Not included in "INITIAL" program Unit
            dicEmployeeDetail.Add("PR_P_NO", 0);
            dicEmployeeDetail.Add("PR_NAME", null);
            dicEmployeeDetail.Add("W_AN_PACK", 0);
            dicEmployeeDetail.Add("W_PRV_INC", 0);
            dicEmployeeDetail.Add("W_PRV_TAX_PD", 0);
            dicEmployeeDetail.Add("W_TAX_USED", 0);
            dicEmployeeDetail.Add("W_JOIN_DATE", null);
            dicEmployeeDetail.Add("W_TRANS_DATE", null);
            dicEmployeeDetail.Add("W_TERMIN_DATE", null);
            dicEmployeeDetail.Add("W_TRANSFER", null);
            dicEmployeeDetail.Add("W_TAX_FROM_DATE", null);
            dicEmployeeDetail.Add("W_TAX_TO_DATE", null);
            dicEmployeeDetail.Add("W_SYS", null);
            dicEmployeeDetail.Add("W_DATE", null);
            dicEmployeeDetail.Add("W_INC_EFF", null);
            dicEmployeeDetail.Add("W_TAX_FROM", null);
            dicEmployeeDetail.Add("W_TAX_TO", null);

            dicEmployeeDetail.Add("W_ACTUAL_OT", 0);          //NUMBER
            dicEmployeeDetail.Add("W_AVG", 0);                //NUMBER
            dicEmployeeDetail.Add("W_AREARS", 0);             //NUMBER
            dicEmployeeDetail.Add("W_OT_MONTHS", null);       //CHARACTER
            dicEmployeeDetail.Add("W_FORCAST_MONTHS", null);  //CHARACTER
            dicEmployeeDetail.Add("W_FORCAST_OT", 0);         //NUMBER
            dicEmployeeDetail.Add("GLOBAL_W_10_BONUS", 0);
            dicEmployeeDetail.Add("W_CONV_ADDED", 0);
            dicEmployeeDetail.Add("W_SUBS_LOAN_TAX_AMT", 0);

            dicEmployeeDetail["W_SYS"] = W_SYS;
            dicEmployeeDetail["W_DATE"] = W_DATE;
            dicEmployeeDetail["W_TAX_TO"] = W_TAX_TO;
            dicEmployeeDetail["W_TAX_FROM"] = W_TAX_FROM;


            dicEmployeeDetail.Add("W_forcast_all", 0);
        }

        /// <summary>
        /// Fetch all employees with condition described in cursor in oracle form
        /// </summary>
        private void GetAllEmployees()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pW_TAX_FROM", dicEmployeeDetail["W_TAX_FROM"]);
                dicInputParameters.Add("pW_TAX_TO", dicEmployeeDetail["W_TAX_TO"]);

                objCmnDataManager = new CmnDataManager();
                Result rsltAllEmployees = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_VALID_PNO", "VALID_PNO", dicInputParameters);

                if (rsltAllEmployees.isSuccessful)
                {
                    if (rsltAllEmployees.dstResult!=null)
                    {
                        if (rsltAllEmployees.dstResult.Tables.Count > 0)
                        {
                            dtEmployeeDetail = rsltAllEmployees.dstResult.Tables[0];
                            if (rsltAllEmployees.dstResult.Tables[0].Rows.Count.Equals(0))
                            {
                                MessageBox.Show("TAX PROCESSING ALREADY DONE FOR " + dicEmployeeDetail["W_TAX_FROM"].ToString() + "-" + dicEmployeeDetail["W_TAX_TO"].ToString() + ".", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //dtEmployeeDetail = rsltAllEmployees.dstResult.Tables[0];
                            }
                        }
                    }            
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }     

        /// <summary>
        /// Fill EmployeeDetailDictionary with EmployeeDetailDataTable
        /// </summary>
        private void FillEmployeeDetailDictionary(int pRow)
        {
            try
            {

                #region Fill EmployeeDetailDictionary with EmployeeDetailDataTable

                dicEmployeeDetail["PR_P_NO"] = dtEmployeeDetail.Rows[pRow]["PR_P_NO"];
                dicEmployeeDetail["PR_NAME"] = dtEmployeeDetail.Rows[pRow]["PR_NAME"];
                dicEmployeeDetail["W_TAX_NO"] = dtEmployeeDetail.Rows[pRow]["pr_national_tax"];
                dicEmployeeDetail["W_ADD1"] = dtEmployeeDetail.Rows[pRow]["pr_add1"];
                dicEmployeeDetail["W_ADD2"] = dtEmployeeDetail.Rows[pRow]["pr_add2"];
                dicEmployeeDetail["W_CATE"] = dtEmployeeDetail.Rows[pRow]["pr_category"];
                dicEmployeeDetail["W_DESIG"] = dtEmployeeDetail.Rows[pRow]["pr_desig"];
                dicEmployeeDetail["W_LEVEL"] = dtEmployeeDetail.Rows[pRow]["pr_level"];
                dicEmployeeDetail["W_AN_PACK"] = dtEmployeeDetail.Rows[pRow]["pr_new_annual_pack"];
                dicEmployeeDetail["W_ZAKAT"] = dtEmployeeDetail.Rows[pRow]["pr_zakat_amt"];
                dicEmployeeDetail["W_PRV_INC"] = dtEmployeeDetail.Rows[pRow]["pr_tax_inc"];
                dicEmployeeDetail["W_PRV_TAX_PD"] = dtEmployeeDetail.Rows[pRow]["pr_tax_paid"];
                dicEmployeeDetail["W_TAX_USED"] = dtEmployeeDetail.Rows[pRow]["pr_tax_used"];
                dicEmployeeDetail["W_JOIN_DATE"] = dtEmployeeDetail.Rows[pRow]["pr_joining_date"];
                dicEmployeeDetail["W_BRANCH"] = dtEmployeeDetail.Rows[pRow]["pr_new_branch"];
                dicEmployeeDetail["W_PR_DATE"] = dtEmployeeDetail.Rows[pRow]["pr_promotion_date"];
                dicEmployeeDetail["W_PROMOTED"] = null;
                dicEmployeeDetail["W_TRANS_DATE"] = dtEmployeeDetail.Rows[pRow]["pr_transfer_date"];
                dicEmployeeDetail["W_TERMIN_DATE"] = dtEmployeeDetail.Rows[pRow]["pr_termin_date"];
                dicEmployeeDetail["W_TRANSFER"] = dtEmployeeDetail.Rows[pRow]["pr_transfer"];
                dicEmployeeDetail["W_SEX"] = dtEmployeeDetail.Rows[pRow]["pr_SEX"];
                dicEmployeeDetail["W_REFUND_AMT"] = dtEmployeeDetail.Rows[pRow]["pr_refund_amt"];
                dicEmployeeDetail["W_REFUND_FOR"] = dtEmployeeDetail.Rows[pRow]["pr_refund_for"];
                dicEmployeeDetail["W_GHA"] = 0;

                
                this.sltxtPR_P_NO.Text = dtEmployeeDetail.Rows[pRow]["PR_P_NO"].ToString();
                this.sltxtPR_NAME.Text = dtEmployeeDetail.Rows[pRow]["PR_NAME"].ToString();

                Application.DoEvents();

                #endregion

                #region From date Calculation
                if ((dicEmployeeDetail["W_JOIN_DATE"] != System.DBNull.Value) && (!String.IsNullOrEmpty(dicEmployeeDetail["W_JOIN_DATE"].ToString()))) //1989-03-04 00:00:00.000
                {
                  
                    DateTime dtJoiningDate = DateTime.Parse(dicEmployeeDetail["W_JOIN_DATE"].ToString());
                    DateTime dtTaxFromDate = DateTime.Parse(dicEmployeeDetail["W_TAX_FROM"].ToString() + "-06-30 00:00:00.000");    //30th june              

                    if (dtJoiningDate > dtTaxFromDate)
                    {
    
                        dicEmployeeDetail["W_TAX_FROM_DATE"] = dtJoiningDate.ToString("dd-MMM-yyyy");
                        dicEmployeeDetail["W_FLAG"] = 1;                      
                    }
                    else
                    {
                        dtTaxFromDate = DateTime.Parse(dicEmployeeDetail["W_TAX_FROM"].ToString() + "-07-01 00:00:00.000");        //1st july
                        dicEmployeeDetail["W_TAX_FROM_DATE"] = dtTaxFromDate.ToString("dd-MMM-yyyy");
                        dicEmployeeDetail["W_FLAG"] = 1;
                    }
                    
                }
                #endregion

                #region To date Calculation

                if ((dicEmployeeDetail["W_TERMIN_DATE"] != System.DBNull.Value) && (!String.IsNullOrEmpty(dicEmployeeDetail["W_TERMIN_DATE"].ToString())))
                {
                    DateTime dtTerminationDate = DateTime.Parse(dicEmployeeDetail["W_TERMIN_DATE"].ToString());
                    dicEmployeeDetail["W_TAX_TO_DATE"] = dtTerminationDate.ToString("dd-MMM-yyyy");
                    dicEmployeeDetail["W_FLAG"] = 2;
                }
                else if ((dicEmployeeDetail["W_TRANSFER"] != System.DBNull.Value) && (!String.IsNullOrEmpty(dicEmployeeDetail["W_TRANSFER"].ToString())))
                {
                    if (Convert.ToInt32(dicEmployeeDetail["W_TRANSFER"]) > 5)
                    {
                        if ((dicEmployeeDetail["W_TRANS_DATE"] != System.DBNull.Value) && (!String.IsNullOrEmpty(dicEmployeeDetail["W_TRANS_DATE"].ToString())))
                        {
                            DateTime dtTransferDate = DateTime.Parse(dicEmployeeDetail["W_TRANS_DATE"].ToString());
                            dicEmployeeDetail["W_TAX_TO_DATE"] = dtTransferDate.ToString("dd-MMM-yyyy");
                        }
                        dicEmployeeDetail["W_FLAG"] = 2;
                    }
                    else
                    {
                        DateTime dtTaxToDate = DateTime.Parse(dicEmployeeDetail["W_TAX_TO"].ToString() + "-06-30 00:00:00.000");
                        dicEmployeeDetail["W_TAX_TO_DATE"] = dtTaxToDate.ToString("dd-MMM-yyyy");
                    }
                }
                else
                {
                    DateTime dtTaxToDate = DateTime.Parse(dicEmployeeDetail["W_TAX_TO"].ToString() + "-06-30 00:00:00.000");
                    dicEmployeeDetail["W_TAX_TO_DATE"] = dtTaxToDate.ToString("dd-MMM-yyyy");
                }
                            
                #endregion

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate Promotion
        /// </summary>
        private void GetPromotionProc()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", dicEmployeeDetail["PR_P_NO"]);
                dicInputParameters.Add("pW_SYS", dicEmployeeDetail["W_SYS"]);
                dicInputParameters.Add("pW_PR_DATE", dicEmployeeDetail["W_PR_DATE"]);

                objCmnDataManager = new CmnDataManager();
                Result rsltPromotionProc = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_PROMOTION_PROC", "PROMOTION_PROC", dicInputParameters);

                if (rsltPromotionProc.isSuccessful)
                {
                    if (rsltPromotionProc.dstResult != null)
                    {
                        if (rsltPromotionProc.dstResult.Tables.Count > 0)
                        {
                            if (rsltPromotionProc.dstResult.Tables[0].Rows.Count > 0)
                            {
                                
                                dicEmployeeDetail["W_DESIG"] = rsltPromotionProc.dstResult.Tables[0].Rows[0]["W_DESIG"];
                                dicEmployeeDetail["W_LEVEL"] = rsltPromotionProc.dstResult.Tables[0].Rows[0]["W_LEVEL"];
                                dicEmployeeDetail["W_PROMOTED"] = rsltPromotionProc.dstResult.Tables[0].Rows[0]["W_PROMOTED"];
                                
                               
                                
                                if (rsltPromotionProc.dstResult.Tables[0].Columns.Count > 3)
                                {
                                    dicEmployeeDetail["W_AN_PACK"] = rsltPromotionProc.dstResult.Tables[0].Rows[0]["W_AN_PACK"];
                                    dicEmployeeDetail["W_CATE"] = rsltPromotionProc.dstResult.Tables[0].Rows[0]["W_CATE"];

                                    if (dicEmployeeDetail["W_CATE"].ToString() == "C")
                                    {
                                        GetGovernmentAllowance();
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate Government allowance
        /// </summary>
        private void GetGovernmentAllowance()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pW_DESIG", dicEmployeeDetail["W_DESIG"]);
                dicInputParameters.Add("pW_LEVEL", dicEmployeeDetail["W_LEVEL"]);
                dicInputParameters.Add("pW_CATE", dicEmployeeDetail["W_CATE"]);
                dicInputParameters.Add("pW_BRANCH", dicEmployeeDetail["W_BRANCH"]);
                dicInputParameters.Add("pW_DATE", dicEmployeeDetail["W_DATE"]);

                objCmnDataManager = new CmnDataManager();
                Result rsltGovernmentAllowance = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_GOVT_ALL", "GOVT_ALL", dicInputParameters);

                if (rsltGovernmentAllowance.isSuccessful)
                {
                    if (rsltGovernmentAllowance.dstResult != null)
                    {
                        if (rsltGovernmentAllowance.dstResult.Tables.Count > 0)
                        {
                            if (rsltGovernmentAllowance.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dicEmployeeDetail["W_GOVT_ALL"] = rsltGovernmentAllowance.dstResult.Tables[0].Rows[0]["W_GOVT_ALL"];
                            }
                        }
                        
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate last increase
        /// </summary>
        private void GetLastIncrease()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", dicEmployeeDetail["PR_P_NO"]);
                dicInputParameters.Add("pW_SYS", dicEmployeeDetail["W_SYS"]);

                objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_LAST_INCREASE", "LAST_INCREASE", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dicEmployeeDetail["W_INC_EFF"] = rslt.dstResult.Tables[0].Rows[0]["W_INC_EFF"];
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate Z Tax
        /// </summary>
        private void GetZ_Tax()
        {
            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("pPR_P_NO", dicEmployeeDetail["PR_P_NO"]);
                dicInputParameters.Add("pW_SYS", dicEmployeeDetail["W_SYS"]);
                dicInputParameters.Add("pW_DATE", dicEmployeeDetail["W_DATE"]);
                dicInputParameters.Add("pW_PR_DATE", dicEmployeeDetail["W_PR_DATE"]);
                dicInputParameters.Add("pW_BRANCH", dicEmployeeDetail["W_BRANCH"]);
                dicInputParameters.Add("pW_DESIG", dicEmployeeDetail["W_DESIG"]);
                dicInputParameters.Add("pW_LEVEL", dicEmployeeDetail["W_LEVEL"]);
                dicInputParameters.Add("pW_CATE", dicEmployeeDetail["W_CATE"]);
                dicInputParameters.Add("pW_AN_PACK", dicEmployeeDetail["W_AN_PACK"]);
                dicInputParameters.Add("pGLOBAL_W_10_BONUS", dicEmployeeDetail["GLOBAL_W_10_BONUS"]);
                dicInputParameters.Add("pW_ZAKAT", dicEmployeeDetail["W_ZAKAT"]);
                dicInputParameters.Add("pW_PRV_INC", dicEmployeeDetail["W_PRV_INC"]);
                dicInputParameters.Add("pW_TAX_USED", dicEmployeeDetail["W_TAX_USED"]);
                dicInputParameters.Add("pW_JOIN_DATE", dicEmployeeDetail["W_JOIN_DATE"]);
                dicInputParameters.Add("pW_TAX_TO_DATE", dicEmployeeDetail["W_TAX_TO_DATE"]);
                dicInputParameters.Add("pW_SEX", dicEmployeeDetail["W_SEX"]);
                dicInputParameters.Add("pW_REFUND_AMT", dicEmployeeDetail["W_REFUND_AMT"]);
                dicInputParameters.Add("W_forcast_all", dicEmployeeDetail["W_forcast_all"]);

                //if (dicEmployeeDetail["PR_P_NO"].ToString() == "2085" || dicEmployeeDetail["PR_P_NO"].ToString() == "2069" || dicEmployeeDetail["PR_P_NO"].ToString() == "2047")
                //{
                //    MessageBox.Show("pr_p_no=" + dicEmployeeDetail["PR_P_NO"] + ", w_forcast_all=" + dicEmployeeDetail["W_forcast_all"]);
                //}
                

                objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.Execute("CHRIS_SP_TAXYEARENDCLOSING_Z_TAX", " ", dicInputParameters, 0);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {

                                dicEmployeeDetail["W_FIX_TAX"] = rslt.dstResult.Tables[0].Rows[0]["W_FIX_TAX"];
                                dicEmployeeDetail["W_TAX_REMAIN"] = rslt.dstResult.Tables[0].Rows[0]["W_TAX_REMAIN"];
                                dicEmployeeDetail["W_TAXABLE_INC"] = rslt.dstResult.Tables[0].Rows[0]["W_TAXABLE_INC"];
                                dicEmployeeDetail["W_INC_BONUS"] = rslt.dstResult.Tables[0].Rows[0]["W_INC_BONUS"];
                                dicEmployeeDetail["W_ANNUAL_INCOME"] = rslt.dstResult.Tables[0].Rows[0]["W_ANNUAL_INCOME"];
                                dicEmployeeDetail["W_HOUSE_RENT"] = rslt.dstResult.Tables[0].Rows[0]["W_HOUSE_RENT"];
                                dicEmployeeDetail["W_CONV_ADDED"] = rslt.dstResult.Tables[0].Rows[0]["W_CONV_ADDED"];
                                dicEmployeeDetail["W_TAX_PAID"] = rslt.dstResult.Tables[0].Rows[0]["W_TAX_PAID"];
                                dicEmployeeDetail["W_OTHER_ALL"] = rslt.dstResult.Tables[0].Rows[0]["W_OTHER_ALL"];
                                dicEmployeeDetail["W_PFUND"] = rslt.dstResult.Tables[0].Rows[0]["W_PFUND"];
                                dicEmployeeDetail["W_TAX_FRM_AMT"] = rslt.dstResult.Tables[0].Rows[0]["W_TAX_FRM_AMT"];
                                dicEmployeeDetail["W_REMAIN_AMT"] = rslt.dstResult.Tables[0].Rows[0]["W_REMAIN_AMT"];
                                dicEmployeeDetail["W_SUR"] = rslt.dstResult.Tables[0].Rows[0]["W_SUR"];
                                dicEmployeeDetail["W_SUBS_LOAN_TAX_AMT"] = rslt.dstResult.Tables[0].Rows[0]["SUBS_LOAN_TAX_AMT"];

                                dicEmployeeDetail["W_forcast_all"] = rslt.dstResult.Tables[0].Rows[0]["W_forcast_all"];
                                dicEmployeeDetail["W_GHA"] = rslt.dstResult.Tables[0].Rows[0]["w_gha"];
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate Rebate
        /// </summary>
        private void Rebate()
        {
            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("pw_fix_tax", dicEmployeeDetail["W_FIX_TAX"]);
                dicInputParameters.Add("pw_tax_remain", dicEmployeeDetail["W_TAX_REMAIN"]);
                dicInputParameters.Add("pw_taxable_inc", dicEmployeeDetail["W_TAXABLE_INC"]);

                objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_REBATE", "Rebate", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dicEmployeeDetail["W_REBATE"] = rslt.dstResult.Tables[0].Rows[0]["w_REBATE"];
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert into Annual Tax
        /// </summary>
        private void AddProc()
        {
            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("pPR_P_NO", dicEmployeeDetail["PR_P_NO"]);
                dicInputParameters.Add("pPR_NAME", dicEmployeeDetail["PR_NAME"]);
                dicInputParameters.Add("pW_TAX_NO", dicEmployeeDetail["W_TAX_NO"]);
                dicInputParameters.Add("pW_ADD1", dicEmployeeDetail["W_ADD1"]);
                dicInputParameters.Add("pW_ADD2", dicEmployeeDetail["W_ADD2"]);
                dicInputParameters.Add("pW_DESIG", dicEmployeeDetail["W_DESIG"]);
                dicInputParameters.Add("pW_LEVEL", dicEmployeeDetail["W_LEVEL"]);
                dicInputParameters.Add("pW_BRANCH", dicEmployeeDetail["W_BRANCH"]);
                dicInputParameters.Add("pW_CATE", dicEmployeeDetail["W_CATE"]);
                dicInputParameters.Add("pW_TAX_FROM", dicEmployeeDetail["W_TAX_FROM"]);
                dicInputParameters.Add("pW_TAX_TO", dicEmployeeDetail["W_TAX_TO"]);
                dicInputParameters.Add("pW_TAX_FROM_DATE", dicEmployeeDetail["W_TAX_FROM_DATE"]);
                dicInputParameters.Add("pW_TAX_TO_DATE", dicEmployeeDetail["W_TAX_TO_DATE"]);
                dicInputParameters.Add("pW_10_BONUS", dicEmployeeDetail["W_10_BONUS"]);
                dicInputParameters.Add("pW_INC_BONUS", dicEmployeeDetail["W_INC_BONUS"]);
                dicInputParameters.Add("pW_ANNUAL_INCOME", dicEmployeeDetail["W_ANNUAL_INCOME"]);
                dicInputParameters.Add("pW_HOUSE_RENT", dicEmployeeDetail["W_HOUSE_RENT"]);
                dicInputParameters.Add("pW_CONV_ADDED", dicEmployeeDetail["W_CONV_ADDED"]);
                dicInputParameters.Add("pW_ZAKAT", dicEmployeeDetail["W_ZAKAT"]);
                dicInputParameters.Add("pW_FORCAST", dicEmployeeDetail["W_FORCAST"]);
                dicInputParameters.Add("pW_TAX_PAID", dicEmployeeDetail["W_TAX_PAID"]);
                dicInputParameters.Add("pW_OTHER_ALL", dicEmployeeDetail["W_OTHER_ALL"]);
                dicInputParameters.Add("pW_GHA", dicEmployeeDetail["W_GHA"]);
                dicInputParameters.Add("pW_TAXABLE_INC", dicEmployeeDetail["W_TAXABLE_INC"]);
                dicInputParameters.Add("pW_PFUND", dicEmployeeDetail["W_PFUND"]);
                dicInputParameters.Add("pW_REBATE", dicEmployeeDetail["W_REBATE"]);
                dicInputParameters.Add("pW_TAX_FRM_AMT", dicEmployeeDetail["W_TAX_FRM_AMT"]);
                dicInputParameters.Add("pW_FIX_TAX", dicEmployeeDetail["W_FIX_TAX"]);
                dicInputParameters.Add("pW_REMAIN_AMT", dicEmployeeDetail["W_REMAIN_AMT"]);
                dicInputParameters.Add("pW_TAX_REMAIN", dicEmployeeDetail["W_TAX_REMAIN"]);
                dicInputParameters.Add("pW_SUR", dicEmployeeDetail["W_SUR"]);
                dicInputParameters.Add("pW_REFUND_AMT", dicEmployeeDetail["W_REFUND_AMT"]);
                dicInputParameters.Add("pW_REFUND_FOR", dicEmployeeDetail["W_REFUND_FOR"]);
                dicInputParameters.Add("pW_FLAG", dicEmployeeDetail["W_FLAG"]);
                dicInputParameters.Add("pSUBS_LOAN_TAX_AMT", dicEmployeeDetail["W_SUBS_LOAN_TAX_AMT"]);

                objCmnDataManager = new CmnDataManager();
                Result rsltPromotionProc = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_ADD_PROC", "", dicInputParameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Start year End closing process
        /// </summary>
        public void StartTaxProcessing()
        {
            DateTime wIncEff;
            DateTime wPrDate;
            DateTime wSys;

            try
            {              

                #region INITIAL
                InitializeEmployeeDetailDictionary();
                #endregion             

                #region VALID_PNO
                GetAllEmployees();
                #endregion

                //If employee exist
                if (dtEmployeeDetail != null)
                {
                    for (int EmployeeNo = 0; EmployeeNo < dtEmployeeDetail.Rows.Count; EmployeeNo++)
                    {
                        //dtEmployeeDetail.Rows.Count
                        //int EmployeeNo = 0; // for testing purrpose
                        FillEmployeeDetailDictionary(EmployeeNo);

                        #region PROMOTION_PROC
                        GetPromotionProc();
                        #endregion

                        #region LAST_INCREASE
                        GetLastIncrease();
                        #endregion

                        #region Z_TAX
                        GetZ_Tax();
                        #endregion

                        #region VALID_PNO Part II

                        DateTime.TryParse(dicEmployeeDetail["W_PR_DATE"].ToString(), out wPrDate);
                        DateTime.TryParse(dicEmployeeDetail["W_SYS"].ToString(), out wSys);
                        DateTime.TryParse(dicEmployeeDetail["W_INC_EFF"].ToString(), out wIncEff);

                        if(wPrDate > wSys)
                            dicEmployeeDetail["W_PR_DATE"] = null;

                        if(wIncEff > wSys)
                             dicEmployeeDetail["W_INC_EFF"] = null;
                               
                        #endregion 

                        #region REBATE
                        Rebate();
                        #endregion

                        #region ADD_PROC
                        AddProc();
                        #endregion

                        #region VALID_PNO Part III
                        try
                        {
                            dicInputParameters.Clear();
                            dicInputParameters.Add("pW_TAX_FROM", dicEmployeeDetail["W_TAX_FROM"]);
                            dicInputParameters.Add("pW_TAX_TO", dicEmployeeDetail["W_TAX_TO"]);

                            objCmnDataManager = new CmnDataManager();
                            //Result rsltAllEmployees = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_VALID_PNO", "UPDATE_ANNUAL_TAX", dicInputParameters);
                            objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_VALID_PNO", "UPDATE_ANNUAL_TAX", dicInputParameters);

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                        #endregion
                    }

                    MessageBox.Show("PROCESSING COMPLETED");

                    this.Close();
                
                }
            }
            catch (Exception ex)
            {
                //string str = dicEmployeeDetail["PR_P_NO"].ToString();
                MessageBox.Show(ex.Message);
            }
        }

        #endregion      

        #region Event Handlers
        private void CHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor_Shown(object sender, EventArgs e)
        {
            StartTaxProcessing();
        }
        #endregion

    }
}