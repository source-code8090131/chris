namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    partial class CHRIS_TaxClosing_TaxAssessmentStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_TaxClosing_TaxAssessmentStatus));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.slTBOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.txt_location = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_username = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.slPanelMasterQuery = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.dtpJoining = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtCardRcvd = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNtnCertRcvd = new CrplControlLibrary.SLTextBox(this.components);
            this.lookup_Pr_P_No = new CrplControlLibrary.LookupButton(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_PR_LAST_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.PR_NATIONAL_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_PR_FIRST_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.slPanelDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvDetails = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.col_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ETS_PR_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ETS_ASST_YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ETS_ASST_IT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ETS_ASST_WT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.slPanelMasterQuery.SuspendLayout();
            this.slPanelDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(658, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(694, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 463);
            this.panel1.Size = new System.Drawing.Size(694, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.slTBOption);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.txt_location);
            this.pnlHead.Controls.Add(this.txt_username);
            this.pnlHead.Controls.Add(this.label33);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label34);
            this.pnlHead.Controls.Add(this.label35);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label36);
            this.pnlHead.Controls.Add(this.label37);
            this.pnlHead.Location = new System.Drawing.Point(25, 82);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(645, 52);
            this.pnlHead.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(497, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 15);
            this.label3.TabIndex = 56;
            this.label3.Text = "Option:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(29, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "User :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTBOption
            // 
            this.slTBOption.AllowSpace = true;
            this.slTBOption.AssociatedLookUpName = "";
            this.slTBOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTBOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTBOption.ContinuationTextBox = null;
            this.slTBOption.CustomEnabled = true;
            this.slTBOption.DataFieldMapping = "";
            this.slTBOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTBOption.GetRecordsOnUpDownKeys = false;
            this.slTBOption.IsDate = false;
            this.slTBOption.Location = new System.Drawing.Point(556, 3);
            this.slTBOption.MaxLength = 10;
            this.slTBOption.Name = "slTBOption";
            this.slTBOption.NumberFormat = "###,###,##0.00";
            this.slTBOption.Postfix = "";
            this.slTBOption.Prefix = "";
            this.slTBOption.ReadOnly = true;
            this.slTBOption.Size = new System.Drawing.Size(80, 20);
            this.slTBOption.SkipValidation = false;
            this.slTBOption.TabIndex = 54;
            this.slTBOption.TabStop = false;
            this.slTBOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(7, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 53;
            this.label2.Text = "Location :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_location
            // 
            this.txt_location.AllowSpace = true;
            this.txt_location.AssociatedLookUpName = "";
            this.txt_location.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_location.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_location.ContinuationTextBox = null;
            this.txt_location.CustomEnabled = false;
            this.txt_location.DataFieldMapping = "";
            this.txt_location.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_location.GetRecordsOnUpDownKeys = false;
            this.txt_location.IsDate = false;
            this.txt_location.Location = new System.Drawing.Point(89, 26);
            this.txt_location.MaxLength = 10;
            this.txt_location.Name = "txt_location";
            this.txt_location.NumberFormat = "###,###,##0.00";
            this.txt_location.Postfix = "";
            this.txt_location.Prefix = "";
            this.txt_location.ReadOnly = true;
            this.txt_location.Size = new System.Drawing.Size(87, 20);
            this.txt_location.SkipValidation = false;
            this.txt_location.TabIndex = 21;
            this.txt_location.TabStop = false;
            this.txt_location.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_username
            // 
            this.txt_username.AllowSpace = true;
            this.txt_username.AssociatedLookUpName = "";
            this.txt_username.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_username.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_username.ContinuationTextBox = null;
            this.txt_username.CustomEnabled = false;
            this.txt_username.DataFieldMapping = "";
            this.txt_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_username.GetRecordsOnUpDownKeys = false;
            this.txt_username.IsDate = false;
            this.txt_username.Location = new System.Drawing.Point(89, 3);
            this.txt_username.MaxLength = 10;
            this.txt_username.Name = "txt_username";
            this.txt_username.NumberFormat = "###,###,##0.00";
            this.txt_username.Postfix = "";
            this.txt_username.Prefix = "";
            this.txt_username.ReadOnly = true;
            this.txt_username.Size = new System.Drawing.Size(87, 20);
            this.txt_username.SkipValidation = false;
            this.txt_username.TabIndex = 13;
            this.txt_username.TabStop = false;
            this.txt_username.TextType = CrplControlLibrary.TextType.String;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(210, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(276, 30);
            this.label33.TabIndex = 19;
            this.label33.Text = "PAYROLL SYSTEM \r\nTAX ASSESSMENT STATUS ENTRY";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(556, 27);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(501, 77);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(509, 31);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 15);
            this.label34.TabIndex = 16;
            this.label34.Text = "Date:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(450, 79);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 15);
            this.label35.TabIndex = 15;
            this.label35.Text = "Option:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label35.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(83, 87);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(83, 61);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(7, 87);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 20);
            this.label36.TabIndex = 2;
            this.label36.Text = "Location:";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label36.Visible = false;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(16, 61);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 20);
            this.label37.TabIndex = 1;
            this.label37.Text = "User:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label37.Visible = false;
            // 
            // slPanelMasterQuery
            // 
            this.slPanelMasterQuery.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPanelMasterQuery.ConcurrentPanels = null;
            this.slPanelMasterQuery.Controls.Add(this.dtpJoining);
            this.slPanelMasterQuery.Controls.Add(this.txtCardRcvd);
            this.slPanelMasterQuery.Controls.Add(this.txtNtnCertRcvd);
            this.slPanelMasterQuery.Controls.Add(this.lookup_Pr_P_No);
            this.slPanelMasterQuery.Controls.Add(this.label11);
            this.slPanelMasterQuery.Controls.Add(this.label10);
            this.slPanelMasterQuery.Controls.Add(this.label4);
            this.slPanelMasterQuery.Controls.Add(this.txt_PR_LAST_NAME);
            this.slPanelMasterQuery.Controls.Add(this.label8);
            this.slPanelMasterQuery.Controls.Add(this.PR_NATIONAL_TAX);
            this.slPanelMasterQuery.Controls.Add(this.label9);
            this.slPanelMasterQuery.Controls.Add(this.label7);
            this.slPanelMasterQuery.Controls.Add(this.txt_PR_FIRST_NAME);
            this.slPanelMasterQuery.Controls.Add(this.txt_PR_P_NO);
            this.slPanelMasterQuery.Controls.Add(this.label6);
            this.slPanelMasterQuery.Controls.Add(this.label5);
            this.slPanelMasterQuery.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelMasterQuery.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelMasterQuery.DependentPanels = null;
            this.slPanelMasterQuery.DisableDependentLoad = false;
            this.slPanelMasterQuery.EnableDelete = true;
            this.slPanelMasterQuery.EnableInsert = true;
            this.slPanelMasterQuery.EnableQuery = false;
            this.slPanelMasterQuery.EnableUpdate = true;
            this.slPanelMasterQuery.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand_Tax";
            this.slPanelMasterQuery.Location = new System.Drawing.Point(12, 143);
            this.slPanelMasterQuery.MasterPanel = null;
            this.slPanelMasterQuery.Name = "slPanelMasterQuery";
            this.slPanelMasterQuery.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelMasterQuery.Size = new System.Drawing.Size(670, 120);
            this.slPanelMasterQuery.SPName = "CHRIS_SP_TAXCLOSING_PERSONNEL_MANAGER";
            this.slPanelMasterQuery.TabIndex = 70;
            // 
            // dtpJoining
            // 
            this.dtpJoining.CustomEnabled = false;
            this.dtpJoining.CustomFormat = "dd/MM/yyyy";
            this.dtpJoining.DataFieldMapping = "PR_JOINING_DATE";
            this.dtpJoining.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpJoining.HasChanges = false;
            this.dtpJoining.Location = new System.Drawing.Point(177, 38);
            this.dtpJoining.Name = "dtpJoining";
            this.dtpJoining.NullValue = " ";
            this.dtpJoining.Size = new System.Drawing.Size(90, 20);
            this.dtpJoining.TabIndex = 73;
            this.dtpJoining.Value = new System.DateTime(2011, 3, 25, 0, 0, 0, 0);
            // 
            // txtCardRcvd
            // 
            this.txtCardRcvd.AllowSpace = true;
            this.txtCardRcvd.AssociatedLookUpName = "";
            this.txtCardRcvd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCardRcvd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCardRcvd.ContinuationTextBox = null;
            this.txtCardRcvd.CustomEnabled = true;
            this.txtCardRcvd.DataFieldMapping = "PR_NTN_CARD_REC";
            this.txtCardRcvd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCardRcvd.GetRecordsOnUpDownKeys = false;
            this.txtCardRcvd.IsDate = false;
            this.txtCardRcvd.Location = new System.Drawing.Point(177, 83);
            this.txtCardRcvd.MaxLength = 1;
            this.txtCardRcvd.Name = "txtCardRcvd";
            this.txtCardRcvd.NumberFormat = "###,###,##0.00";
            this.txtCardRcvd.Postfix = "";
            this.txtCardRcvd.Prefix = "";
            this.txtCardRcvd.Size = new System.Drawing.Size(48, 20);
            this.txtCardRcvd.SkipValidation = false;
            this.txtCardRcvd.TabIndex = 3;
            this.txtCardRcvd.TextType = CrplControlLibrary.TextType.String;
            this.txtCardRcvd.Validated += new System.EventHandler(this.txtCardRcvd_Validated);
            this.txtCardRcvd.Validating += new System.ComponentModel.CancelEventHandler(this.txtCardRcvd_Validating);
            // 
            // txtNtnCertRcvd
            // 
            this.txtNtnCertRcvd.AllowSpace = true;
            this.txtNtnCertRcvd.AssociatedLookUpName = "";
            this.txtNtnCertRcvd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNtnCertRcvd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNtnCertRcvd.ContinuationTextBox = null;
            this.txtNtnCertRcvd.CustomEnabled = true;
            this.txtNtnCertRcvd.DataFieldMapping = "PR_NTN_CERT_REC";
            this.txtNtnCertRcvd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNtnCertRcvd.GetRecordsOnUpDownKeys = false;
            this.txtNtnCertRcvd.IsDate = false;
            this.txtNtnCertRcvd.Location = new System.Drawing.Point(177, 61);
            this.txtNtnCertRcvd.MaxLength = 1;
            this.txtNtnCertRcvd.Name = "txtNtnCertRcvd";
            this.txtNtnCertRcvd.NumberFormat = "###,###,##0.00";
            this.txtNtnCertRcvd.Postfix = "";
            this.txtNtnCertRcvd.Prefix = "";
            this.txtNtnCertRcvd.Size = new System.Drawing.Size(48, 20);
            this.txtNtnCertRcvd.SkipValidation = false;
            this.txtNtnCertRcvd.TabIndex = 2;
            this.txtNtnCertRcvd.TextType = CrplControlLibrary.TextType.String;
            this.txtNtnCertRcvd.Validating += new System.ComponentModel.CancelEventHandler(this.txtNtnCertRcvd_Validating);
            // 
            // lookup_Pr_P_No
            // 
            this.lookup_Pr_P_No.ActionLOVExists = "Personnel_LovExists";
            this.lookup_Pr_P_No.ActionType = "Personnel_Lov";
            this.lookup_Pr_P_No.ConditionalFields = "";
            this.lookup_Pr_P_No.CustomEnabled = true;
            this.lookup_Pr_P_No.DataFieldMapping = "";
            this.lookup_Pr_P_No.DependentLovControls = "";
            this.lookup_Pr_P_No.HiddenColumns = "";
            this.lookup_Pr_P_No.Image = ((System.Drawing.Image)(resources.GetObject("lookup_Pr_P_No.Image")));
            this.lookup_Pr_P_No.LoadDependentEntities = true;
            this.lookup_Pr_P_No.Location = new System.Drawing.Point(233, 15);
            this.lookup_Pr_P_No.LookUpTitle = "Personnel No. Name";
            this.lookup_Pr_P_No.Name = "lookup_Pr_P_No";
            this.lookup_Pr_P_No.Size = new System.Drawing.Size(26, 21);
            this.lookup_Pr_P_No.SkipValidationOnLeave = false;
            this.lookup_Pr_P_No.SPName = "CHRIS_SP_TAXCLOSING_PERSONNEL_MANAGER";
            this.lookup_Pr_P_No.TabIndex = 72;
            this.lookup_Pr_P_No.TabStop = false;
            this.lookup_Pr_P_No.UseVisualStyleBackColor = true;
            this.lookup_Pr_P_No.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lookup_Pr_P_No_MouseDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(231, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 71;
            this.label11.Text = "[Y]es Or [N]o";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(231, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 70;
            this.label10.Text = "[Y]es Or [N]o";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(77, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 58;
            this.label4.Text = "Personnel No.  :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_PR_LAST_NAME
            // 
            this.txt_PR_LAST_NAME.AllowSpace = true;
            this.txt_PR_LAST_NAME.AssociatedLookUpName = "";
            this.txt_PR_LAST_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LAST_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LAST_NAME.ContinuationTextBox = null;
            this.txt_PR_LAST_NAME.CustomEnabled = true;
            this.txt_PR_LAST_NAME.DataFieldMapping = "PR_LAST_NAME";
            this.txt_PR_LAST_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LAST_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LAST_NAME.IsDate = false;
            this.txt_PR_LAST_NAME.Location = new System.Drawing.Point(479, 16);
            this.txt_PR_LAST_NAME.Name = "txt_PR_LAST_NAME";
            this.txt_PR_LAST_NAME.NumberFormat = "###,###,##0.00";
            this.txt_PR_LAST_NAME.Postfix = "";
            this.txt_PR_LAST_NAME.Prefix = "";
            this.txt_PR_LAST_NAME.ReadOnly = true;
            this.txt_PR_LAST_NAME.Size = new System.Drawing.Size(167, 20);
            this.txt_PR_LAST_NAME.SkipValidation = false;
            this.txt_PR_LAST_NAME.TabIndex = 69;
            this.txt_PR_LAST_NAME.TabStop = false;
            this.txt_PR_LAST_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(31, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 13);
            this.label8.TabIndex = 62;
            this.label8.Text = "N.T.N. Card Received    :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PR_NATIONAL_TAX
            // 
            this.PR_NATIONAL_TAX.AllowSpace = true;
            this.PR_NATIONAL_TAX.AssociatedLookUpName = "";
            this.PR_NATIONAL_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PR_NATIONAL_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PR_NATIONAL_TAX.ContinuationTextBox = null;
            this.PR_NATIONAL_TAX.CustomEnabled = true;
            this.PR_NATIONAL_TAX.DataFieldMapping = "PR_NATIONAL_TAX";
            this.PR_NATIONAL_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PR_NATIONAL_TAX.GetRecordsOnUpDownKeys = false;
            this.PR_NATIONAL_TAX.IsDate = false;
            this.PR_NATIONAL_TAX.Location = new System.Drawing.Point(479, 40);
            this.PR_NATIONAL_TAX.Name = "PR_NATIONAL_TAX";
            this.PR_NATIONAL_TAX.NumberFormat = "###,###,##0.00";
            this.PR_NATIONAL_TAX.Postfix = "";
            this.PR_NATIONAL_TAX.Prefix = "";
            this.PR_NATIONAL_TAX.ReadOnly = true;
            this.PR_NATIONAL_TAX.Size = new System.Drawing.Size(120, 20);
            this.PR_NATIONAL_TAX.SkipValidation = false;
            this.PR_NATIONAL_TAX.TabIndex = 68;
            this.PR_NATIONAL_TAX.TabStop = false;
            this.PR_NATIONAL_TAX.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(290, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 63;
            this.label9.Text = "Name :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(395, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 61;
            this.label7.Text = "N.T.N. No. :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_PR_FIRST_NAME
            // 
            this.txt_PR_FIRST_NAME.AllowSpace = true;
            this.txt_PR_FIRST_NAME.AssociatedLookUpName = "";
            this.txt_PR_FIRST_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_FIRST_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_FIRST_NAME.ContinuationTextBox = null;
            this.txt_PR_FIRST_NAME.CustomEnabled = true;
            this.txt_PR_FIRST_NAME.DataFieldMapping = "PR_FIRST_NAME";
            this.txt_PR_FIRST_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_FIRST_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_FIRST_NAME.IsDate = false;
            this.txt_PR_FIRST_NAME.Location = new System.Drawing.Point(343, 15);
            this.txt_PR_FIRST_NAME.Name = "txt_PR_FIRST_NAME";
            this.txt_PR_FIRST_NAME.NumberFormat = "###,###,##0.00";
            this.txt_PR_FIRST_NAME.Postfix = "";
            this.txt_PR_FIRST_NAME.Prefix = "";
            this.txt_PR_FIRST_NAME.ReadOnly = true;
            this.txt_PR_FIRST_NAME.Size = new System.Drawing.Size(130, 20);
            this.txt_PR_FIRST_NAME.SkipValidation = false;
            this.txt_PR_FIRST_NAME.TabIndex = 67;
            this.txt_PR_FIRST_NAME.TabStop = false;
            this.txt_PR_FIRST_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_P_NO
            // 
            this.txt_PR_P_NO.AllowSpace = true;
            this.txt_PR_P_NO.AssociatedLookUpName = "lookup_Pr_P_No";
            this.txt_PR_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_P_NO.ContinuationTextBox = null;
            this.txt_PR_P_NO.CustomEnabled = true;
            this.txt_PR_P_NO.DataFieldMapping = "PR_P_NO";
            this.txt_PR_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_P_NO.IsDate = false;
            this.txt_PR_P_NO.IsLookUpField = true;
            this.txt_PR_P_NO.Location = new System.Drawing.Point(177, 16);
            this.txt_PR_P_NO.MaxLength = 6;
            this.txt_PR_P_NO.Name = "txt_PR_P_NO";
            this.txt_PR_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_P_NO.Postfix = "";
            this.txt_PR_P_NO.Prefix = "";
            this.txt_PR_P_NO.Size = new System.Drawing.Size(50, 20);
            this.txt_PR_P_NO.SkipValidation = false;
            this.txt_PR_P_NO.TabIndex = 1;
            this.txt_PR_P_NO.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(177, 13);
            this.label6.TabIndex = 60;
            this.label6.Text = "N.T.N Certificate Received   :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(71, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Date of Joining  :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // slPanelDetail
            // 
            this.slPanelDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPanelDetail.ConcurrentPanels = null;
            this.slPanelDetail.Controls.Add(this.dgvDetails);
            this.slPanelDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelDetail.DependentPanels = null;
            this.slPanelDetail.DisableDependentLoad = false;
            this.slPanelDetail.EnableDelete = true;
            this.slPanelDetail.EnableInsert = true;
            this.slPanelDetail.EnableQuery = false;
            this.slPanelDetail.EnableUpdate = true;
            this.slPanelDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.EmpTaxStatusCommand";
            this.slPanelDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slPanelDetail.Location = new System.Drawing.Point(12, 261);
            this.slPanelDetail.MasterPanel = null;
            this.slPanelDetail.Name = "slPanelDetail";
            this.slPanelDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelDetail.Size = new System.Drawing.Size(670, 195);
            this.slPanelDetail.SPName = "CHRIS_SP_EMP_TAX_STATUS_MANAGER";
            this.slPanelDetail.TabIndex = 72;
            // 
            // dgvDetails
            // 
            this.dgvDetails.AllowUserToAddRows = false;
            this.dgvDetails.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_ID,
            this.col_ETS_PR_P_NO,
            this.col_ETS_ASST_YEAR,
            this.col_ETS_ASST_IT,
            this.col_ETS_ASST_WT});
            this.dgvDetails.ColumnToHide = null;
            this.dgvDetails.ColumnWidth = null;
            this.dgvDetails.CustomEnabled = true;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDetails.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvDetails.DisplayColumnWrapper = null;
            this.dgvDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetails.GridDefaultRow = 0;
            this.dgvDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvDetails.Name = "dgvDetails";
            this.dgvDetails.ReadOnlyColumns = null;
            this.dgvDetails.RequiredColumns = null;
            this.dgvDetails.RowHeadersWidth = 25;
            this.dgvDetails.Size = new System.Drawing.Size(668, 193);
            this.dgvDetails.SkippingColumns = null;
            this.dgvDetails.TabIndex = 0;
            this.dgvDetails.Enter += new System.EventHandler(this.dgvDetails_Enter);
            this.dgvDetails.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvDetails_CellValidating);
            this.dgvDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvDetails_EditingControlShowing);
            // 
            // col_ID
            // 
            this.col_ID.DataPropertyName = "ID";
            this.col_ID.HeaderText = "ID";
            this.col_ID.Name = "col_ID";
            this.col_ID.Visible = false;
            this.col_ID.Width = 5;
            // 
            // col_ETS_PR_P_NO
            // 
            this.col_ETS_PR_P_NO.DataPropertyName = "PR_P_NO";
            this.col_ETS_PR_P_NO.HeaderText = "ETS_PR_P_NO";
            this.col_ETS_PR_P_NO.Name = "col_ETS_PR_P_NO";
            this.col_ETS_PR_P_NO.Visible = false;
            this.col_ETS_PR_P_NO.Width = 5;
            // 
            // col_ETS_ASST_YEAR
            // 
            this.col_ETS_ASST_YEAR.DataPropertyName = "ETS_ASST_YEAR";
            this.col_ETS_ASST_YEAR.HeaderText = "Income Year";
            this.col_ETS_ASST_YEAR.MaxInputLength = 9;
            this.col_ETS_ASST_YEAR.Name = "col_ETS_ASST_YEAR";
            this.col_ETS_ASST_YEAR.Width = 150;
            // 
            // col_ETS_ASST_IT
            // 
            this.col_ETS_ASST_IT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_ETS_ASST_IT.DataPropertyName = "ETS_ASST_IT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_ETS_ASST_IT.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_ETS_ASST_IT.HeaderText = "Asst. Order Income Tax";
            this.col_ETS_ASST_IT.MaxInputLength = 3;
            this.col_ETS_ASST_IT.Name = "col_ETS_ASST_IT";
            // 
            // col_ETS_ASST_WT
            // 
            this.col_ETS_ASST_WT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_ETS_ASST_WT.DataPropertyName = "ETS_ASST_WT";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_ETS_ASST_WT.DefaultCellStyle = dataGridViewCellStyle9;
            this.col_ETS_ASST_WT.HeaderText = "Asst. Order Wealth Tax";
            this.col_ETS_ASST_WT.MaxInputLength = 3;
            this.col_ETS_ASST_WT.Name = "col_ETS_ASST_WT";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(417, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 55;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_TaxClosing_TaxAssessmentStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 523);
            this.Controls.Add(this.slPanelMasterQuery);
            this.Controls.Add(this.slPanelDetail);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "slPanelMasterQuery";
            this.CurrrentOptionTextBox = this.slTBOption;
            this.Name = "CHRIS_TaxClosing_TaxAssessmentStatus";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF1Option = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS Tax Closing-Tax Assessment Status";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.slPanelDetail, 0);
            this.Controls.SetChildIndex(this.slPanelMasterQuery, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.slPanelMasterQuery.ResumeLayout(false);
            this.slPanelMasterQuery.PerformLayout();
            this.slPanelDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private CrplControlLibrary.SLTextBox txt_location;
        private CrplControlLibrary.SLTextBox txt_username;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox slTBOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txt_PR_LAST_NAME;
        private CrplControlLibrary.SLTextBox PR_NATIONAL_TAX;
        private CrplControlLibrary.SLTextBox txt_PR_FIRST_NAME;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txt_PR_P_NO;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelMasterQuery;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.LookupButton lookup_Pr_P_No;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelDetail;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDetails;
        private CrplControlLibrary.SLTextBox txtNtnCertRcvd;
        private CrplControlLibrary.SLTextBox txtCardRcvd;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ETS_PR_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ETS_ASST_YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ETS_ASST_IT;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ETS_ASST_WT;
        private CrplControlLibrary.SLDatePicker dtpJoining;
    }
}