using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_RegularStaffHiringEnt_Page2 : Form
    {
        #region
        string lblText = "";
        #endregion


        #region Constructor
        public CHRIS_Personnel_RegularStaffHiringEnt_Page2(string LabelName)
        {
            InitializeComponent();
            lblText = LabelName;
        }

        //public CHRIS_Personnel_RegularStaffHiringEnt_Page2(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, string LabelName)
        //: base(mainmenu, connbean_obj)
        //{
        //    InitializeComponent();
        //    lblText = LabelName;

        //}
        #endregion

        #region Properties

        public String Value
        {
            get
            {
                return this.txt_W_VIEW_ANS.Text;
            }
        }
        public TextBox TextBox
        {
            get
            {
                return this.txt_W_VIEW_ANS;
            }
        }

        #endregion

        private void CHRIS_Personnel_RegularStaffHiringEnt_Page2_Load(object sender, EventArgs e)
        {
            lblPopUP.Text = lblText;
        }


        //#region --Events--
        ///// <summary>
        ///// REmove the Add Update Save Cancel Button
        ///// </summary>
        //protected override void CommonOnLoadMethods()
        //{
        //    try
        //    {
        //        base.CommonOnLoadMethods();
        //        this.tbtAdd.Visible = false;
        //        this.tbtList.Visible = false;
        //        this.tbtSave.Visible = false;
        //        this.tbtCancel.Visible = false;
        //        this.tlbMain.Visible = false;
        //    }
        //    catch (Exception exp)
        //    {
        //        LogException(this.Name, "CommonOnLoadMethods", exp);
        //    }
        //}

        ///// <summary>
        ///// Open the Desired panels on Form
        ///// </summary>
        ///// <param name="e"></param>
        //protected override void OnLoad(EventArgs e)
        //{
        //    try
        //    {
        //        base.OnLoad(e);
        //        //if (pnlViewPopUP.Name == pnlName)
        //        //{
        //        //    pnlViewPopUP.Visible = true;
        //        //}
        //        //else if (pnlSavePopUP.Name == pnlName)
        //        //{
        //        //    pnlSavePopUP.Visible = true;
        //        //}
        //        //else if (pnlDeletePopUp.Name == pnlName)
        //        //{
        //        //    pnlDeletePopUp.Visible = true;
        //        //}

        //        lblPopUP.Text = lblText;

        //        //this.KeyPreview = true;
        //        //this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
        //    }
        //    catch (Exception exp)
        //    {
        //        LogException(this.Name, "OnLoad", exp);
        //    }
        //}
        
        //#endregion

    }
}