using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_FastOrISTransfer : ChrisSimpleForm
    {

        #region Declarations
        string globalType;
        public DataTable DtTransfer;
        #endregion
      
        # region constructor
        public CHRIS_Personnel_FastOrISTransfer()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_FastOrISTransfer(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #endregion

        #region Methods
        public bool QuitNew()
        {
           
            if (txtTransferType.Text != string.Empty)
            {
                this.DoToolbarActions(this.Controls, "Cancel");
                return false;
            }
            else
            {
                base.Quit();
                return false;
            }
          
        }
        protected override bool Quit()
        {

            if (txtTransferType.Text != string.Empty)
            {
                this.DoToolbarActions(this.Controls, "Cancel");
                return false;
            }
            else
            {
                base.Quit();
                return false;
            }

        }
        //public void CallQuitFromChild(CHRIS_Personnel_FastOrISTransfer_TransferPage frm)
        //{
        //    frm.Close();

        //    this.Quit();
        
        //}

        protected override bool Add()
        {


            this.lbtnPNo.ActionType = "PR_P_ADD_LOV";
            //this.lbtnPNo.ActionLOVExists = "PR_P_ADD_LOV_Exists";
            //this.lbtnPNo.ActionLOVExists = "PR_P_NO_LOV_EXISTS";

            base.Add();
            this.txtTransferType.Text = globalType;
            if (txtTransferType.Text != string.Empty)
            {
                if (txtTransferType.Text == "LOCAL")
                {
                    prTransferGlobal.Text = "6";

                    txtPRTransferTypeGlobal.Text = "6";

                }

                if (txtTransferType.Text == "IS")
                {
                    prTransferGlobal.Text = "7";

                    txtPRTransferTypeGlobal.Text = "7";

                }




            }
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            return false;
        }
        protected override bool Edit()
        {

            this.lbtnPNo.ActionType = "PR_P_EDIT_LOV";
            //this.lbtnPNo.ActionLOVExists = "PR_P_EDIT_LOV_Exists";
          
            base.Edit();
            this.txtTransferType.Text = globalType;
            if (txtTransferType.Text != string.Empty)
            {
                if (txtTransferType.Text == "LOCAL")
                {
                    prTransferGlobal.Text = "6";

                    txtPRTransferTypeGlobal.Text = "6";

                }

                if (txtTransferType.Text == "IS")
                {
                    prTransferGlobal.Text = "7";

                    txtPRTransferTypeGlobal.Text = "7";

                }




            }
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            return false;
        }
        protected override bool View()
        {

            this.lbtnPNo.ActionType = "PR_P_EDIT_LOV";
            //this.lbtnPNo.ActionLOVExists = "PR_P_EDIT_LOV_Exists";
            base.View();
            this.txtTransferType.Text = globalType;
            if (txtTransferType.Text != string.Empty)
            {
                if (txtTransferType.Text == "LOCAL")
                {
                    prTransferGlobal.Text = "6";

                    txtPRTransferTypeGlobal.Text = "6";

                }

                if (txtTransferType.Text == "IS")
                {
                    prTransferGlobal.Text = "7";

                    txtPRTransferTypeGlobal.Text = "7";

                }




            }
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            return false;
        }
        protected override bool Delete()
        {
            this.lbtnPNo.ActionType = "PR_P_EDIT_LOV";
            //this.lbtnPNo.ActionLOVExists = "PR_P_EDIT_LOV_Exists";
            base.Delete();
            this.txtTransferType.Text = globalType;
            if (txtTransferType.Text != string.Empty)
            {
                if (txtTransferType.Text == "LOCAL")
                {
                    prTransferGlobal.Text = "6";

                    txtPRTransferTypeGlobal.Text = "6";

                }

                if (txtTransferType.Text == "IS")
                {
                    prTransferGlobal.Text = "7";

                    txtPRTransferTypeGlobal.Text = "7";

                }




            }
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            return false;
        }

        public void SetID(int IDM)
        {
            this.m_intPKID = IDM;
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtDelete.Enabled = false;
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtAdd.Visible = false;
            tbtDelete.Visible = false;
            txtUserName.Text = "User Name: " + this.UserName;
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF5 = false;
            this.FunctionConfig.EnableF2 = false;
        }
        
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
           
         
                if (actionType == "Cancel")
            {

                this.AutoValidate = AutoValidate.Disable;
                this.FunctionConfig.CurrentOption = Function.None;
                this.txtOption.Select();
                this.txtOption.Focus();
                base.DoToolbarActions(ctrlsCollection, actionType);
                this.txtOption.Select();
                this.txtOption.Focus();
                this.BindLOVLookupButton(lbtnTransferType);
               
           /* this for testing nida
                this.txtOption.Visible = false;

                this.lbtnTransferType.PerformClick();

                this.txtOption.Visible = true;*/

                //this.txtOption.Select();
                //this.txtOption.Focus();
                //this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                //this.ClearForm(pnlDetail.Controls);

               return;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);

        }

        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }

        public void AddMoreRecords()
        {
            AddYN();
            txtPersNo.Select();
            txtPersNo.Focus();
          
        }
        public void ViewMoreRecords()
        {
           // this.AutoValidate = AutoValidate.Disable;
            txtPersNo.Select();
            txtPersNo.Focus();
          this.ClearForm(pnlDetail.Controls);
          
           View();
         
        }

        #endregion

        #region Events

        private void txtPersNo_Validating(object sender, CancelEventArgs e)
        {
            int GGTransfer = 0;
            if (txtPersNo.Text != string.Empty)
            {
                DataTable DtPersonal;
                
               
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                colsNVals.Clear();
                colsNVals.Add("PR_TR_NO", txtPersNo.Text);

                Dictionary<string, object> colsNVals1 = new Dictionary<string, object>();
                colsNVals1.Clear();

                int Transfer=0;
                
                    DtPersonal = GetData("CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER", "ExecuteQueryBlkPersonal", colsNVals);

                 if (DtPersonal != null)
                 {
                     if (DtPersonal.Rows.Count > 0)
                     {
                         if (DtPersonal.Rows[0].ItemArray[1] != null)
                         {
                             if (DtPersonal.Rows[0].ItemArray[1].ToString() != string.Empty)
                             {
                                 txtJoiningDate.Text = Convert.ToDateTime(DtPersonal.Rows[0].ItemArray[1]).ToString();
                             }
                             else
                             {
                                 txtJoiningDate.Text = "";

                             }
                         }
                         if (DtPersonal.Rows[0].ItemArray[10] != null)
                         {
                             txtPR_CLOSE_FLAG.Text = DtPersonal.Rows[0].ItemArray[10].ToString();
                         }
                         if (DtPersonal.Rows[0].ItemArray[7] != null)
                         {
                             txtPRTransfer.Text = DtPersonal.Rows[0].ItemArray[7].ToString();
                         }

                         if (DtPersonal.Rows[0].ItemArray[8] != null)
                         {
                             if (DtPersonal.Rows[0].ItemArray[8].ToString() != string.Empty)
                             {
                                // dtPR_TERMIN_DATE.Value = Convert.ToDateTime(DtPersonal.Rows[0].ItemArray[8]);
                                 dtTerminDate.Text = DtPersonal.Rows[0].ItemArray[8].ToString();
                             }
                             else
                             {
                                 //dtPR_TERMIN_DATE.Value = null;
                                 dtTerminDate.Text = "";

                             }
                         }

                         if (DtPersonal.Rows[0].ItemArray[6] != null)
                         {
                             if (DtPersonal.Rows[0].ItemArray[6].ToString() != string.Empty)
                             {
                                // dtPRTransferDate.Value = Convert.ToDateTime(DtPersonal.Rows[0].ItemArray[6]);
                                 dtTransferDate.Text = DtPersonal.Rows[0].ItemArray[6].ToString();
                             }
                             else
                             {
                                 dtTransferDate.Text = "";
                                 //dtPRTransferDate.Value = null;

                             }

                         }



                         txtPR_DESG.Text = DtPersonal.Rows[0].ItemArray[2].ToString();
                         txtLevel.Text = DtPersonal.Rows[0].ItemArray[3].ToString();
                         txtPR_Func1.Text = DtPersonal.Rows[0].ItemArray[4].ToString();
                         txtPR_Func2.Text = DtPersonal.Rows[0].ItemArray[5].ToString();

                         txtName.Text = DtPersonal.Rows[0].ItemArray[12].ToString();
                         slTextBox1.Text = DtPersonal.Rows[0].ItemArray[13].ToString();
                         if (dtTransferDate.Text != string.Empty)
                         {
                             colsNVals1.Add("PR_TR_NO", txtPersNo.Text);
                             colsNVals1.Add("PR_TRANSFER_DATE", Convert.ToDateTime(dtTransferDate.Text));
                             colsNVals1.Add("PR_TRANSFER_TYPE", txtPRTransferTypeGlobal.Text);

                             DtTransfer = GetData("CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER", "ExecuteQueryTransfer", colsNVals1);

                         }
                      

                         if (DtPersonal.Rows[0].ItemArray[8].ToString() == string.Empty && txtPR_CLOSE_FLAG.Text != "C")
                         {
                             if (this.FunctionConfig.CurrentOption == Function.Add)
                             {
                                 #region Add
                                 if (txtPRTransfer.Text !=string.Empty)
                                {
                                    Transfer=Convert.ToInt32(txtPRTransfer.Text);
                                }
                                 if(prTransferGlobal.Text !=string.Empty)
                                 {
                                     GGTransfer = Convert.ToInt32(prTransferGlobal.Text);
                                 }
                                 if (txtPRTransfer.Text == string.Empty || ((txtPRTransfer.Text != string.Empty) && Transfer < GGTransfer))
                                 {
                                    
                                     string _Pr_P_No = "";
                                     string _Tr_Type = "";
                                     string _frmOption;
                                     string _TypeOf = "";
                                     DateTime _joiningDate = new DateTime();
                                    DateTime _dtPRTransferDate = new DateTime();
                                     _Pr_P_No = txtPersNo.Text;
                                     _Tr_Type=txtPRTransferTypeGlobal.Text;
                                     _TypeOf = txtTransferType.Text;
                                     // if(JoiningDate.Value != null)
                                     //{
                                     //_joiningDate =Convert.ToDateTime(JoiningDate.Value);
                                     //}
                                     if (txtJoiningDate.Text != string.Empty)
                                     {
                                         _joiningDate = Convert.ToDateTime(txtJoiningDate.Text);
                                     }

                                     
                                    // if (dtPRTransferDate.Value != null)
                                    // {
                                     //    _dtPRTransferDate =Convert.ToDateTime(dtPRTransferDate.Value);
                                    // }

                                     if (dtTransferDate.Text != string.Empty)
                                     {
                                         _dtPRTransferDate = Convert.ToDateTime(dtTransferDate.Text);
                                     }
                                  
                                     _frmOption = txtOption.Text;

                                     //go_block('blktwo');
                                     //   go_field('blktwo.pr_country');

                                     CHRIS_Personnel_FastOrISTransfer_TransferPage processnew = new CHRIS_Personnel_FastOrISTransfer_TransferPage(null, this.connbean,this);
                                    
                                     processnew.ShowDialog();
                                 }
                                
                                 else
                                 {
                                     MessageBox.Show("NO DATA FOUND FOR THIS OPTION");

                                     e.Cancel = true;

                                     this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                                     //txtOption.Focus();
                                     return;

                                 }

                                 #endregion
                             }
                             else if (this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete)
                             {
                                 #region View/Modify
                                 if (txtPRTransfer.Text != string.Empty)
                                 {

                                     if (Convert.ToInt32(txtPRTransfer.Text) == Convert.ToInt32(prTransferGlobal.Text))
                                     {

                                         //go_block('blktwo');
                                         //   execute_query;
                                         //    show_page(7);
                                         if (DtTransfer != null)
                                         {
                                             CHRIS_Personnel_FastOrISTransfer_TransferPage processnew = new CHRIS_Personnel_FastOrISTransfer_TransferPage(null, this.connbean, this);

                                             processnew.ShowDialog();
                                         }
                                         else if (this.FunctionConfig.CurrentOption == Function.View)
                                         {
                                             if (MessageBox.Show("Do You Want To View More Record [Y]es or [N]o", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                             {
                                                 this.ViewMoreRecords();
                                             }
                                             else
                                             {
                                                 this.DoToolbarActions(this.Controls, "Cancel");
                                             }
                              

                                         }
                                         else if (this.FunctionConfig.CurrentOption == Function.Modify)
                                         {
                                             MessageBox.Show("RECORD NOT FOUND");
                                             e.Cancel = true;
                                             txtOption.Select();
                                             txtOption.Focus();
                                             this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                                            
                                             return;

                                         }
                                         else if (this.FunctionConfig.CurrentOption == Function.Delete)
                                         {
                                             MessageBox.Show("RECORD NOT FOUND");
                                             e.Cancel = true;

                                             this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                                             txtOption.Select();
                                             txtOption.Focus();
                                             return;

                                         }

                                         return;


                                        

                                     }
                                     else
                                     {
                                         MessageBox.Show("NO DATA FOUND FOR THIS OPTION");

                                         e.Cancel = true;

                                         this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                                         //txtOption.Focus();
                                         return;
                                     }

                                 }
                                 else
                                 {
                                     MessageBox.Show("NO DATA FOUND FOR THIS OPTION");

                                     e.Cancel = true;

                                     this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                                     //txtOption.Focus();
                                     return;
                                 }

                                 #endregion
                             }

                         }

                     }
                     else
                     {
                         MessageBox.Show("RECORD NOT FOUND");
                         e.Cancel = true;

                         this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                         txtOption.Select();
                         txtOption.Focus();
                         return;
                     }


                 }
                 else
                 {
                     MessageBox.Show("RECORD NOT FOUND");
                     e.Cancel = true;
                     
                     
                     this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                     txtOption.Select();
                     txtOption.Focus();
                    // return;
                 }


            }
        }
        private void CHRIS_Personnel_FastOrISTransfer_Shown(object sender, EventArgs e)
        {


            //this.txtOption.Visible = false;

            //lbtnTransferType.PerformClick();

            //this.txtOption.Visible = true;
            this.BindLOVLookupButton(lbtnTransferType);
            this.txtOption.Select();
            this.txtOption.Focus();
          
            if (txtTransferType.Text == "Exit" || txtTransferType.Text == "EXIT")
            {
                this.Close();
            }

           
        }
        private void CHRIS_Personnel_FastOrISTransfer_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "TransferType_LOV")
            {
                globalType = this.txtTransferType.Text;

            }
            if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            }




        }

        protected  bool AddYN()
        {


            this.lbtnPNo.ActionType = "PR_P_ADD_LOV";
            //this.lbtnPNo.ActionLOVExists = "PR_P_ADD_LOV_Exists";
            //this.lbtnPNo.ActionLOVExists = "PR_P_NO_LOV_EXISTS";

            base.Add();
            this.txtTransferType.Text = globalType;
            if (txtTransferType.Text != string.Empty)
            {
                if (txtTransferType.Text == "LOCAL")
                {
                    prTransferGlobal.Text = "6";

                    txtPRTransferTypeGlobal.Text = "6";

                }

                if (txtTransferType.Text == "IS")
                {
                    prTransferGlobal.Text = "7";

                    txtPRTransferTypeGlobal.Text = "7";

                }




            }
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            txtPersNo.Focus();
            return false;
        }

        private void txtPersNo_Leave(object sender, EventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;
            if (this.txtPersNo.Text == "")
            {
                //MessageBox.Show("RECORD NOT FOUND");
                this.txtPersNo.Focus();
                return;
            }
        }

        #endregion

      

      
      
    }
}