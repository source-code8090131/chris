using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_LettersFollowupEntry : ChrisMasterDetailForm
    {
        public CHRIS_Personnel_LettersFollowupEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_LettersFollowupEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(slPanelDetailReferences);
            lstDependentPanels.Add(slPanelDetailEmployment);
            this.pnlDetail.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlDetail);
          


        }


        protected override bool Add()
        {
            bool flag = false;

            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            ///this.FunctionConfig.CurrentOption = Function.Add;
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "ENTRY";
            this.txtOption.Text = "S";

            return flag;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("MM/dd/yyyy");
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = false;
            tbtDelete.Enabled = false;
            tbtDelete.Visible = false;
            tbtList.Visible = false;

       

        }
          /// <summary>
        /// Handles Key Press Event to identify the current charachter as Y or N
        /// </summary>
        /// <param name="object sender"></param>
        /// <param name="KeyPressEventArgs e"></param>
        private void txtPR_MED_FLAG_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            TextBox txt = (TextBox)sender;
            if (txt != null)
            {
                switch (e.KeyChar)
                {
                    case 'Y':
                    case 'y':
                    case 'N':
                    case 'n':
                        e.Handled = false;
                        break;
                    case '\b':
                        e.Handled = false;
                        break;
                    default:
                        e.Handled = true;
                        break;
                }
            }
        }

        private void dgvReferenceDetails_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 1 || e.ColumnIndex == 2)
            {
                if (dgvReferenceDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].IsInEditMode)
                {
                    if (((dgvReferenceDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().ToUpper() != "Y")
                      || (dgvReferenceDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().ToUpper() == "N")))
                      
                    {
                        MessageBox.Show("Please Enter [Y]es or [N]o......");

                    }

                }


            }

        }
        



    }
}