namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_TerminationSettHold
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_TerminationSettHold));
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.dtpTransferDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.pnlTblDept = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.slDataGridView1 = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.Seg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contrib = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pr_transfer_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtflag = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.dtEffectiveDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCategory = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnalName = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPersonnal = new CrplControlLibrary.LookupButton(this.components);
            this.txtRemarks = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesignation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnalNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.W_LOC = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlDetail.SuspendLayout();
            this.pnlTblDept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(479, 0);
            this.txtOption.TabStop = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.None;
            this.pnlBottom.Location = new System.Drawing.Point(28, 4);
            this.pnlBottom.Size = new System.Drawing.Size(517, 26);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.None;
            this.panel1.Location = new System.Drawing.Point(0, 390);
            this.panel1.Size = new System.Drawing.Size(578, 60);
            // 
            // pnlDetail
            // 
            this.pnlDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.dtpTransferDate);
            this.pnlDetail.Controls.Add(this.pnlTblDept);
            this.pnlDetail.Controls.Add(this.txtflag);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.dtEffectiveDate);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.label6);
            this.pnlDetail.Controls.Add(this.txtCategory);
            this.pnlDetail.Controls.Add(this.txtLevel);
            this.pnlDetail.Controls.Add(this.txtPersonnalName);
            this.pnlDetail.Controls.Add(this.lbtnPersonnal);
            this.pnlDetail.Controls.Add(this.txtRemarks);
            this.pnlDetail.Controls.Add(this.txtBranch);
            this.pnlDetail.Controls.Add(this.txtDesignation);
            this.pnlDetail.Controls.Add(this.txtPersonnalNo);
            this.pnlDetail.Controls.Add(this.label5);
            this.pnlDetail.Controls.Add(this.label4);
            this.pnlDetail.Controls.Add(this.label3);
            this.pnlDetail.Controls.Add(this.label2);
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.Controls.Add(this.label16);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.DataManager = null;
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TransferCommand";
            this.pnlDetail.Location = new System.Drawing.Point(28, 141);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(517, 249);
            this.pnlDetail.SPName = "CHRIS_SP_TRANSFER_MANAGER";
            this.pnlDetail.TabIndex = 12;
            // 
            // dtpTransferDate
            // 
            this.dtpTransferDate.CustomEnabled = true;
            this.dtpTransferDate.CustomFormat = "dd/MM/yyyy";
            this.dtpTransferDate.DataFieldMapping = "pr_transfer_date";
            this.dtpTransferDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransferDate.HasChanges = true;
            this.dtpTransferDate.Location = new System.Drawing.Point(211, 113);
            this.dtpTransferDate.Name = "dtpTransferDate";
            this.dtpTransferDate.NullValue = " ";
            this.dtpTransferDate.Size = new System.Drawing.Size(93, 20);
            this.dtpTransferDate.TabIndex = 36;
            this.dtpTransferDate.Value = new System.DateTime(2010, 9, 28, 0, 0, 0, 0);
            this.dtpTransferDate.Visible = false;
            // 
            // pnlTblDept
            // 
            this.pnlTblDept.ConcurrentPanels = null;
            this.pnlTblDept.Controls.Add(this.slDataGridView1);
            this.pnlTblDept.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblDept.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblDept.DependentPanels = null;
            this.pnlTblDept.DisableDependentLoad = false;
            this.pnlTblDept.EnableDelete = false;
            this.pnlTblDept.EnableInsert = false;
            this.pnlTblDept.EnableQuery = false;
            this.pnlTblDept.EnableUpdate = false;
            this.pnlTblDept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptCommand";
            this.pnlTblDept.Location = new System.Drawing.Point(312, 102);
            this.pnlTblDept.MasterPanel = this.pnlDetail;
            this.pnlTblDept.Name = "pnlTblDept";
            this.pnlTblDept.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblDept.Size = new System.Drawing.Size(192, 105);
            this.pnlTblDept.SPName = "CHRIS_SP_DEPT_CONT_MANAGER";
            this.pnlTblDept.TabIndex = 13;
            // 
            // slDataGridView1
            // 
            this.slDataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seg,
            this.Dept,
            this.Contrib,
            this.pr_transfer_date});
            this.slDataGridView1.ColumnToHide = null;
            this.slDataGridView1.ColumnWidth = null;
            this.slDataGridView1.CustomEnabled = true;
            this.slDataGridView1.DisplayColumnWrapper = null;
            this.slDataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.slDataGridView1.GridDefaultRow = 0;
            this.slDataGridView1.Location = new System.Drawing.Point(0, 0);
            this.slDataGridView1.Name = "slDataGridView1";
            this.slDataGridView1.ReadOnlyColumns = null;
            this.slDataGridView1.RequiredColumns = null;
            this.slDataGridView1.RowHeadersWidth = 5;
            this.slDataGridView1.Size = new System.Drawing.Size(192, 105);
            this.slDataGridView1.SkippingColumns = null;
            this.slDataGridView1.TabIndex = 0;
            this.slDataGridView1.TabStop = false;
            this.slDataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.slDataGridView1_EditingControlShowing);
            // 
            // Seg
            // 
            this.Seg.DataPropertyName = "PR_SEGMENT";
            this.Seg.HeaderText = "Seg.";
            this.Seg.Name = "Seg";
            this.Seg.ReadOnly = true;
            this.Seg.Width = 44;
            // 
            // Dept
            // 
            this.Dept.DataPropertyName = "PR_DEPT";
            this.Dept.HeaderText = "Dept.";
            this.Dept.Name = "Dept";
            this.Dept.ReadOnly = true;
            this.Dept.Width = 80;
            // 
            // Contrib
            // 
            this.Contrib.DataPropertyName = "PR_CONTRIB";
            this.Contrib.HeaderText = "Contrib.";
            this.Contrib.Name = "Contrib";
            this.Contrib.ReadOnly = true;
            this.Contrib.Width = 60;
            // 
            // pr_transfer_date
            // 
            this.pr_transfer_date.DataPropertyName = "pr_transfer_date";
            this.pr_transfer_date.HeaderText = "pr_transfer_date";
            this.pr_transfer_date.Name = "pr_transfer_date";
            this.pr_transfer_date.Visible = false;
            // 
            // txtflag
            // 
            this.txtflag.AllowSpace = true;
            this.txtflag.AssociatedLookUpName = "";
            this.txtflag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtflag.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtflag.ContinuationTextBox = null;
            this.txtflag.CustomEnabled = true;
            this.txtflag.DataFieldMapping = "pr_flag_HR";
            this.txtflag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtflag.GetRecordsOnUpDownKeys = false;
            this.txtflag.IsDate = false;
            this.txtflag.Location = new System.Drawing.Point(197, 278);
            this.txtflag.MaxLength = 30;
            this.txtflag.Name = "txtflag";
            this.txtflag.NumberFormat = "###,###,##0.00";
            this.txtflag.Postfix = "";
            this.txtflag.Prefix = "";
            this.txtflag.Size = new System.Drawing.Size(40, 20);
            this.txtflag.SkipValidation = false;
            this.txtflag.TabIndex = 32;
            this.txtflag.TabStop = false;
            this.txtflag.TextType = CrplControlLibrary.TextType.String;
            this.txtflag.Visible = false;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(21, 164);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 20);
            this.label13.TabIndex = 31;
            this.label13.Text = "(DD/MM/YYYY)";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtEffectiveDate
            // 
            this.dtEffectiveDate.CustomEnabled = true;
            this.dtEffectiveDate.CustomFormat = "dd/MM/yyyy";
            this.dtEffectiveDate.DataFieldMapping = "PR_EFFECTIVE";
            this.dtEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEffectiveDate.HasChanges = true;
            this.dtEffectiveDate.Location = new System.Drawing.Point(115, 141);
            this.dtEffectiveDate.Name = "dtEffectiveDate";
            this.dtEffectiveDate.NullValue = " ";
            this.dtEffectiveDate.Size = new System.Drawing.Size(93, 20);
            this.dtEffectiveDate.TabIndex = 2;
            this.dtEffectiveDate.Value = new System.DateTime(2010, 9, 28, 0, 0, 0, 0);
            this.dtEffectiveDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtEffectiveDate_Validating);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(182, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Level :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(306, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Category :";
            // 
            // txtCategory
            // 
            this.txtCategory.AllowSpace = true;
            this.txtCategory.AssociatedLookUpName = "";
            this.txtCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCategory.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCategory.ContinuationTextBox = null;
            this.txtCategory.CustomEnabled = true;
            this.txtCategory.DataFieldMapping = "pr_category";
            this.txtCategory.Enabled = false;
            this.txtCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategory.GetRecordsOnUpDownKeys = false;
            this.txtCategory.IsDate = false;
            this.txtCategory.Location = new System.Drawing.Point(374, 55);
            this.txtCategory.MaxLength = 40;
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.NumberFormat = "###,###,##0.00";
            this.txtCategory.Postfix = "";
            this.txtCategory.Prefix = "";
            this.txtCategory.ReadOnly = true;
            this.txtCategory.Size = new System.Drawing.Size(27, 20);
            this.txtCategory.SkipValidation = false;
            this.txtCategory.TabIndex = 23;
            this.txtCategory.TabStop = false;
            this.txtCategory.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "PR_LEVEL";
            this.txtLevel.Enabled = false;
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(234, 55);
            this.txtLevel.MaxLength = 40;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(27, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 22;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnalName
            // 
            this.txtPersonnalName.AllowSpace = true;
            this.txtPersonnalName.AssociatedLookUpName = "";
            this.txtPersonnalName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnalName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnalName.ContinuationTextBox = null;
            this.txtPersonnalName.CustomEnabled = true;
            this.txtPersonnalName.DataFieldMapping = "pr_name";
            this.txtPersonnalName.Enabled = false;
            this.txtPersonnalName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnalName.GetRecordsOnUpDownKeys = false;
            this.txtPersonnalName.IsDate = false;
            this.txtPersonnalName.Location = new System.Drawing.Point(201, 32);
            this.txtPersonnalName.MaxLength = 3;
            this.txtPersonnalName.Name = "txtPersonnalName";
            this.txtPersonnalName.NumberFormat = "###,###,##0.00";
            this.txtPersonnalName.Postfix = "";
            this.txtPersonnalName.Prefix = "";
            this.txtPersonnalName.ReadOnly = true;
            this.txtPersonnalName.Size = new System.Drawing.Size(227, 20);
            this.txtPersonnalName.SkipValidation = false;
            this.txtPersonnalName.TabIndex = 21;
            this.txtPersonnalName.TabStop = false;
            this.txtPersonnalName.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbtnPersonnal
            // 
            this.lbtnPersonnal.ActionLOVExists = "PersonnelLovListExists";
            this.lbtnPersonnal.ActionType = "PersonnelLovList";
            this.lbtnPersonnal.ConditionalFields = "";
            this.lbtnPersonnal.CustomEnabled = true;
            this.lbtnPersonnal.DataFieldMapping = "";
            this.lbtnPersonnal.DependentLovControls = "";
            this.lbtnPersonnal.HiddenColumns = "PR_DESG|PR_LEVEL|pr_category|PR_OLD_BRANCH|pr_transfer_date";
            this.lbtnPersonnal.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnal.Image")));
            this.lbtnPersonnal.LoadDependentEntities = true;
            this.lbtnPersonnal.Location = new System.Drawing.Point(169, 32);
            this.lbtnPersonnal.LookUpTitle = null;
            this.lbtnPersonnal.Name = "lbtnPersonnal";
            this.lbtnPersonnal.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnal.SkipValidationOnLeave = false;
            this.lbtnPersonnal.SPName = "CHRIS_SP_TRANSFER_MANAGER";
            this.lbtnPersonnal.TabIndex = 1;
            this.lbtnPersonnal.TabStop = false;
            this.lbtnPersonnal.Tag = "";
            this.lbtnPersonnal.UseVisualStyleBackColor = true;
            // 
            // txtRemarks
            // 
            this.txtRemarks.AllowSpace = true;
            this.txtRemarks.AssociatedLookUpName = "";
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRemarks.ContinuationTextBox = null;
            this.txtRemarks.CustomEnabled = true;
            this.txtRemarks.DataFieldMapping = "PR_REMARKS";
            this.txtRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.GetRecordsOnUpDownKeys = false;
            this.txtRemarks.IsDate = false;
            this.txtRemarks.Location = new System.Drawing.Point(115, 213);
            this.txtRemarks.MaxLength = 30;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.NumberFormat = "###,###,##0.00";
            this.txtRemarks.Postfix = "";
            this.txtRemarks.Prefix = "";
            this.txtRemarks.Size = new System.Drawing.Size(239, 20);
            this.txtRemarks.SkipValidation = false;
            this.txtRemarks.TabIndex = 3;
            this.txtRemarks.TextType = CrplControlLibrary.TextType.String;
            this.txtRemarks.Validated += new System.EventHandler(this.txtRemarks_Validated);
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "PR_OLD_BRANCH";
            this.txtBranch.Enabled = false;
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(115, 77);
            this.txtBranch.MaxLength = 30;
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.ReadOnly = true;
            this.txtBranch.Size = new System.Drawing.Size(27, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 7;
            this.txtBranch.TabStop = false;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesignation
            // 
            this.txtDesignation.AllowSpace = true;
            this.txtDesignation.AssociatedLookUpName = "";
            this.txtDesignation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesignation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesignation.ContinuationTextBox = null;
            this.txtDesignation.CustomEnabled = true;
            this.txtDesignation.DataFieldMapping = "PR_DESG";
            this.txtDesignation.Enabled = false;
            this.txtDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesignation.GetRecordsOnUpDownKeys = false;
            this.txtDesignation.IsDate = false;
            this.txtDesignation.Location = new System.Drawing.Point(115, 55);
            this.txtDesignation.MaxLength = 40;
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.NumberFormat = "###,###,##0.00";
            this.txtDesignation.Postfix = "";
            this.txtDesignation.Prefix = "";
            this.txtDesignation.ReadOnly = true;
            this.txtDesignation.Size = new System.Drawing.Size(27, 20);
            this.txtDesignation.SkipValidation = false;
            this.txtDesignation.TabIndex = 6;
            this.txtDesignation.TabStop = false;
            this.txtDesignation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnalNo
            // 
            this.txtPersonnalNo.AllowSpace = true;
            this.txtPersonnalNo.AssociatedLookUpName = "lbtnPersonnal";
            this.txtPersonnalNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnalNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnalNo.ContinuationTextBox = null;
            this.txtPersonnalNo.CustomEnabled = true;
            this.txtPersonnalNo.DataFieldMapping = "PR_TR_NO";
            this.txtPersonnalNo.Enabled = false;
            this.txtPersonnalNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnalNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnalNo.IsDate = false;
            this.txtPersonnalNo.Location = new System.Drawing.Point(115, 33);
            this.txtPersonnalNo.MaxLength = 6;
            this.txtPersonnalNo.Name = "txtPersonnalNo";
            this.txtPersonnalNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnalNo.Postfix = "";
            this.txtPersonnalNo.Prefix = "";
            this.txtPersonnalNo.Size = new System.Drawing.Size(52, 20);
            this.txtPersonnalNo.SkipValidation = false;
            this.txtPersonnalNo.TabIndex = 1;
            this.txtPersonnalNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersonnalNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersonnalNo, "[F9] = Help [F6] = W/O Save Exit");
            this.txtPersonnalNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonnalNo_Validating);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Remarks ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Termination Dt:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Branch ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Designation";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Personnel No. :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(103, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 20);
            this.label16.TabIndex = 35;
            this.label16.Text = ":";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(103, 77);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 20);
            this.label15.TabIndex = 34;
            this.label15.Text = ":";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(103, 213);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 20);
            this.label14.TabIndex = 33;
            this.label14.Text = ":";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(199, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Personnel System";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(189, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Hold / Regular Entry";
            // 
            // W_LOC
            // 
            this.W_LOC.AllowSpace = true;
            this.W_LOC.AssociatedLookUpName = "";
            this.W_LOC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_LOC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_LOC.ContinuationTextBox = null;
            this.W_LOC.CustomEnabled = true;
            this.W_LOC.DataFieldMapping = "";
            this.W_LOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_LOC.GetRecordsOnUpDownKeys = false;
            this.W_LOC.IsDate = false;
            this.W_LOC.Location = new System.Drawing.Point(46, 29);
            this.W_LOC.Name = "W_LOC";
            this.W_LOC.NumberFormat = "###,###,##0.00";
            this.W_LOC.Postfix = "";
            this.W_LOC.Prefix = "";
            this.W_LOC.ReadOnly = true;
            this.W_LOC.Size = new System.Drawing.Size(86, 20);
            this.W_LOC.SkipValidation = false;
            this.W_LOC.TabIndex = 20;
            this.W_LOC.TabStop = false;
            this.W_LOC.TextType = CrplControlLibrary.TextType.String;
            this.W_LOC.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(46, 4);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(86, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 19;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(4, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Loc :";
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(4, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "User :";
            this.label12.Visible = false;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(418, 25);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(84, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 16;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(370, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Date :";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtDate);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtUser);
            this.panel2.Controls.Add(this.W_LOC);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(28, 87);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(517, 54);
            this.panel2.TabIndex = 119;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(372, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 120;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Personnel_TerminationSettHold
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 450);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlDetail);
            this.CurrentPanelBlock = "pnlDetail";
            this.F6OptionText = "[F6]=Exit Without Save";
            this.Name = "CHRIS_Personnel_TerminationSettHold";
            this.ShowBottomBar = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "iCORE CHRISTOP - Termination Set Hold";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.pnlTblDept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox W_LOC;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.LookupButton lbtnPersonnal;
        private CrplControlLibrary.SLTextBox txtRemarks;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtDesignation;
        private CrplControlLibrary.SLTextBox txtPersonnalNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtCategory;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtPersonnalName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLDatePicker dtEffectiveDate;
        private CrplControlLibrary.SLTextBox txtflag;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblDept;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView slDataGridView1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.SLDatePicker dtpTransferDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Seg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dept;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contrib;
        private System.Windows.Forms.DataGridViewTextBoxColumn pr_transfer_date;
    }
}