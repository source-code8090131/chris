using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_OnePagerPersonalEntry_Save : SimpleForm
    {

        #region Declarations
        private CHRIS_Personnel_OnePagerPersonalEntry _mainForm = null;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_CitiRef frm_CitiRef;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Save frm_Save;
        string lblText = "";
        #endregion

        #region Constructor
        public CHRIS_Personnel_OnePagerPersonalEntry_Save()
        {
            InitializeComponent();
            
            txtOption.Visible = false;
          
        }

        public CHRIS_Personnel_OnePagerPersonalEntry_Save(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_OnePagerPersonalEntry mainForm, string LabelName)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            //this._mainForm = mainForm;

            lblText = LabelName;

        }
        #endregion

        #region Properties
        public String Value
        {
            get
            {
                return this.txtStatus.Text;
            }
        }
        public TextBox TextBox
        {
            get
            {
                return this.txtStatus;
            }
        }
        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);

                this.txtOption.Visible = false;
                this.tbtList.Visible = false;
                this.tbtSave.Visible = false;
                this.tbtCancel.Visible = false;
                this.tlbMain.Visible = false;
                this.tbtAdd.Visible = false;

                txtOption.Visible = false;
                this.txtStatus.Focus();
               
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }
        internal void CloseSaveDialog()
        {
            this.Close();
        }

        #endregion

        #region Events

        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Modifiers == Keys.Control)
                {
                    switch (e.KeyValue)
                    {
                        case 33:

                            /// <summary>
                            /// Commented By Anila on 25th March 2011
                            /// Description: User can not go back to previous form at this position,
                            /// This functionality is not provided by oracle form so
                            /// no need to implement it on Icore
                            /// </summary>
                            /// 
                            //this.Hide();
                            //frm_CitiRef.Show();
                            //this.KeyPreview = true;
                            break;

                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "ShortCutKey_Press", exp);
            }
        }
        
        private void txtStatus_Leave(object sender, EventArgs e)
        {
            if (txtStatus.Text != "Y" && txtStatus.Text != "N")
            {
                MessageBox.Show("Enter Only Y or N");
                txtStatus.Focus();
                return;
            }
            else
            {
                this.Hide();
            }
        }

        private void txtStatus_Validating(object sender, CancelEventArgs e)
        {

            //if (txtStatus.Text != "Y" && txtStatus.Text != "N")
            //{
            //    MessageBox.Show("Enter Only  Y or N");
            //    return;

            //}
            //else
            //{
            //    //Main Window//
            //    CHRIS_Personnel_OnePagerPersonalEntry OnePagerPersonalDialog = new CHRIS_Personnel_OnePagerPersonalEntry();
            //    OnePagerPersonalDialog.ShowDialog();
            //    this.Close();

            //}

        }
       
        #endregion

    }
}