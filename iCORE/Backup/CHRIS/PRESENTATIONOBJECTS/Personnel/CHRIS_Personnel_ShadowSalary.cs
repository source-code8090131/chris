using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_ShadowSalary : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();

        #region Variables
        bool flag = false;
        #endregion 
        #region Constructor
        public CHRIS_Personnel_ShadowSalary()
        {
            InitializeComponent();
        }
        public CHRIS_Personnel_ShadowSalary(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #endregion
        #region Methods
        void saveRec()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("shs_loc_salary", this.txtReviseShadowSal.Text);
            param.Add("shs_pr_no", this.txtShs_pr_No.Text);

            rslt = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "save_record", param);

            //if (rslt.isSuccessful)
            //{
            //    MessageBox.Show("Record Saved Sucessfully"); 

            //}      
        }
        bool Proc_delete()
        {
            try
            {
                proc_update_del();
                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("shs_pr_no", this.txtShs_pr_No.Text);
                param.Add("shs_inc_last_date", Convert.ToDateTime(DtLastInc.Value));
                rslt = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "Proc_delete", param);
                return true;
            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc_delete",exp);
                return false;
            }
         
        }
        void proc_update_del()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("shs_pr_no", txtShs_pr_No.Text);
            rslt = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "proc_update_del", param);

        }
        void proc_view_add()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("shs_pr_no", this.txtShs_pr_No.Text);
            param.Add("shs_inc_last_date", Convert.ToDateTime(DtLastInc.Value).ToShortDateString());
            rslt = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "proc_view_add", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    DtTransferDate.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    txtCity.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    txtcountry.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    txtDesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    txtLevel.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                    txtAnnualPackage.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    DtIncrement.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());
                    txtIs_TirPercent.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();
                    txtAnnualPackage.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    TxtRankinginHC.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtIncrementPercent.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();
                    txtIncremntType.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[11].ToString();
                    txtRevisePackgeDolar.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[12].ToString();
                    txtRanking.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[13].ToString();
                    dtRankingDate.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[14].ToString());
                    txtReviseShadowSal.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[15].ToString();
                    txtRemarks.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[16].ToString();
                    txtAnnualShadowSal.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[17].ToString();
                }
            }
        }
        void proc_Modify()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("shs_pr_no", txtShs_pr_No.Text);
            param.Add("shs_inc_last_date", Convert.ToDateTime(DtLastInc.Value).ToShortDateString());
            param.Add("shs_inc_crnt_date", Convert.ToDateTime(DtIncrement.Value).ToShortDateString());
            param.Add("shs_is_tir", txtIs_TirPercent.Text);
            param.Add("shs_anl_pkg", txtAnnualPackage.Text);
            param.Add("shs_rank_hc", TxtRankinginHC.Text);
            param.Add("shs_inc_percent", txtIncrementPercent.Text);
            param.Add("shs_inc_type", txtIncremntType.Text);
            param.Add("shs_rev_pkg", txtRevisePackgeDolar.Text);
            param.Add("shs_loc_salary", txtReviseShadowSal.Text);
            param.Add("SHS_LOC_DATE", Convert.ToDateTime(dtRankingDate.Value).ToShortDateString());
            param.Add("SHS_LOC_RANK", txtRanking.Text);
            param.Add("SHS_LEVEL", txtLevel.Text);
            param.Add("shs_remarks", txtRemarks.Text);



            rslt = cmnDM.Execute("CHRIS_SP_SHADOW_SALARY_MANAGER", "proc_modify", param);




        }
        void proc_mod_Del()
        {
            //  proc_mod_Del
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("shs_pr_no", txtShs_pr_No.Text);
            param.Add("shs_inc_last_date", Convert.ToDateTime(DtLastInc.Value).ToShortDateString());
            // param.Add("SHS_LOC_DATE", Convert.ToDateTime(dtRankingDate.Value).ToShortDateString());
            // param.Add("SHS_LOC_RANK",txtRanking.Text);
            //   param.Add("@SHS_LEVEL", txtLevel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "proc_mod_Del", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    DtTransferDate.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    txtCity.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    txtcountry.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    txtDesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    txtLevel.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                    txtAnnualPackage.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    DtIncrement.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());
                    txtIs_TirPercent.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();
                    txtAnnualPackage.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    TxtRankinginHC.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtIncrementPercent.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();
                    txtIncremntType.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[11].ToString();
                    txtRevisePackgeDolar.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[12].ToString();
                    txtRanking.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[13].ToString();
                    dtRankingDate.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[14].ToString());
                    txtReviseShadowSal.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[15].ToString();
                    txtRemarks.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[16].ToString();
                    txtAnnualShadowSal.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[17].ToString();

                }
            }
        }
        void Proc_mod_DEl_Ext()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("SHS_LOC_DATE", Convert.ToDateTime(dtRankingDate.Value).ToShortDateString());
            param.Add("SHS_LOC_RANK", txtRanking.Text);
            param.Add("@SHS_LEVEL", txtLevel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "proc_mod_Del_ext", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtLocalTIRPercent.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtIncrementPerRs.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();

                }
            }


        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            label30.Text = "User Name :    " + this.UserName;
            this.txtUser.Text = this.userID;
            this.txtLocation.Text = this.CurrentLocation;
            txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");


            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF5 = false;
            this.FunctionConfig.EnableF2 = false;
            //DtLastInc.Value = null;
            //DtIncrement.Value = null;
            // dtRankingDate.Value = null;
            lbPr_p_no.Enabled = false;
            this.tbtList.Visible = false;
            this.ShowTextOption = true;
            this.tbtAdd.Visible = false;
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {

            if (actionType == "Save")
            {
                if (flag == false || txtShs_pr_No.Text != string.Empty)
                {
                    return;
                }
                else
                {
                    base.DoToolbarActions(this.Controls, actionType);
                    return;
                }
            }


            if (actionType == "List")
            {
                if (txtOption.Text == string.Empty)
                {
                    return;
                }

                else
                {
                    base.DoToolbarActions(this.Controls, actionType);
                }


            }

            if (actionType == "Cancel")
            {
                base.ClearForm(PnlShadowSalary.Controls);


            }
            base.DoToolbarActions(this.Controls, actionType);

        }
        #endregion
        #region Events
        private void txtShs_pr_No_Validated(object sender, EventArgs e)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("shs_pr_no", txtShs_pr_No.Text);
            rslt = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "SHS_PR_PostChange", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtFirstname.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    // txtShs_pr_No.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                }
            }

            Result rslt1;
            Dictionary<string, object> param1 = new Dictionary<string, object>();
            param1.Add("shs_pr_no", txtShs_pr_No.Text);
            rslt1 = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "Execute_trigger", param1);
            if (rslt1.isSuccessful)
            {
                if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 0)
                {
                    DtTransferDate.Value = Convert.ToDateTime(rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    txtCity.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    txtcountry.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();

                    if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[3].ToString() != string.Empty)
                    { dtPromotion.Value = Convert.ToDateTime(rslt1.dstResult.Tables[0].Rows[0].ItemArray[3].ToString()); }
                    txtDesignation.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                    txtLevel.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    txtAnnualShadowSal.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[6].ToString();



                    if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[3].ToString() != string.Empty)
                    {

                        rslt1 = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "Execute_triggerIfdatenotnull", param1);

                        if (rslt1.isSuccessful)
                        {
                            if (rslt1.dstResult.Tables[0].Rows.Count > 0 && rslt1.dstResult != null)
                            {
                                txtDesignation.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(); ;
                                txtLevel.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[1].ToString(); ;
                            }

                        }
                    }
                }

            }



        }
        private void DtIncrement_Validating(object sender, CancelEventArgs e)
        {

            if (DtIncrement.Value != null && DtLastInc.Value != null)
            {
                DateTime curr_date = Convert.ToDateTime(DtIncrement.Value);
                DateTime lastdate = Convert.ToDateTime(DtLastInc.Value);

                TimeSpan ts = curr_date.Subtract(lastdate);
                int days = ts.Days;


                if (days < 0)
                {

                    MessageBox.Show("Cruent increment date Must be grater then last increment date");
                    e.Cancel = true;

                }
            }
        }
        private void txtIncrementPercent_Validating(object sender, CancelEventArgs e)
        {
            double shs_anl_pkg = 0;
            double shs_inc_percent = 0;
            double shs_rev_pkg = 0;

            if (!double.TryParse(txtIncrementPercent.Text, out shs_inc_percent))
            {
                txtIncrementPercent.Text = "";            
            }

            float tirper = 0;
            if (float.TryParse(txtIncrementPercent.Text, out tirper))
            {
                string str = tirper.ToString();
                //if (str.IndexOf(".") != -1)
                //{
                //    //Console.Write("string contains 'Vader'");
                //    if (str.IndexOf(".") > 2)
                //    {
                //        MessageBox.Show("Field must be of form 99.99.");
                //        e.Cancel = true;
                //    }


                bool ValidTIRDec = IsValidExpression(txtIncrementPercent.Text, InputValidator.TWODECIMAL_REGEX);
                bool ValidTIRINT = IsValidExpression(txtIncrementPercent.Text, InputValidator.NUMBER_REGEX);
                bool ValidTIRONEDEC = IsValidExpression(txtIncrementPercent.Text, InputValidator.ONEDECIMAL_REGEX);
                if (ValidTIRINT)
                {
                    string val = txtIncrementPercent.Text;
                    string newval = val + ".00";
                    txtIncrementPercent.Text = newval;

                    if ((double.TryParse(txtAnnualPackage.Text, out shs_anl_pkg)) && (double.TryParse(txtIncrementPercent.Text, out shs_inc_percent)))
                    {
                        shs_anl_pkg = double.Parse(txtAnnualPackage.Text == "" ? "0" : txtAnnualPackage.Text);
                        shs_inc_percent = double.Parse(txtIncrementPercent.Text == "" ? "0" : txtIncrementPercent.Text);
                        shs_rev_pkg = (shs_anl_pkg / 100) * (shs_inc_percent) + shs_anl_pkg;
                        txtRevisePackgeDolar.Text = shs_rev_pkg.ToString();
                    }



                    return;
                }

                if (ValidTIRONEDEC)
                {

                    string val = txtIncrementPercent.Text;
                    string newval = val + "0";
                    txtIncrementPercent.Text = newval;
                    if ((double.TryParse(txtAnnualPackage.Text, out shs_anl_pkg)) && (double.TryParse(txtIncrementPercent.Text, out shs_inc_percent)))
                    {
                        shs_anl_pkg = double.Parse(txtAnnualPackage.Text == "" ? "0" : txtAnnualPackage.Text);
                        shs_inc_percent = double.Parse(txtIncrementPercent.Text == "" ? "0" : txtIncrementPercent.Text);
                        shs_rev_pkg = (shs_anl_pkg / 100) * (shs_inc_percent) + shs_anl_pkg;
                        txtRevisePackgeDolar.Text = shs_rev_pkg.ToString();
                    }
                    return;



                }

                if (!ValidTIRDec)
                {
                    MessageBox.Show("Field must be of form 99.99.");
                    e.Cancel = true;
                }
                



                    if ((double.TryParse(txtAnnualPackage.Text, out shs_anl_pkg)) && (double.TryParse(txtIncrementPercent.Text, out shs_inc_percent)))
                    {
                        shs_anl_pkg = double.Parse(txtAnnualPackage.Text == "" ? "0" : txtAnnualPackage.Text);
                        shs_inc_percent = double.Parse(txtIncrementPercent.Text == "" ? "0" : txtIncrementPercent.Text);
                        shs_rev_pkg = (shs_anl_pkg / 100) * (shs_inc_percent) + shs_anl_pkg;
                        txtRevisePackgeDolar.Text = shs_rev_pkg.ToString();
                    }

              //  }

                //else
                //{

                //    MessageBox.Show("Field must be of form 99.99.");
                //    e.Cancel = true;

                //}



            }
            else
            {
                txtIncrementPercent.Text = "";
                txtIncrementPercent.Select();
                txtIncrementPercent.Focus();

            }


            //if (txtAnnualPackage.Text != null && txtAnnualPackage.Text.ToString() != string.Empty && txtAnnualPackage.Text != "")
            //{
           

           

        }
        private void txtRanking_Validating(object sender, CancelEventArgs e)
        {
            if ((txtRanking.Text == "") && (txtRanking.Text.ToString() == string.Empty) && (txtRanking.Text == null))
            {
                MessageBox.Show("Please Enter Rank .......!");
                e.Cancel = true;
            }
        }
        private void dtRankingDate_Validating(object sender, CancelEventArgs e)
        {

            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("SHS_ANL_SHD_SAL", txtAnnualShadowSal.Text);
            param.Add("SHS_LOC_DATE", Convert.ToDateTime(dtRankingDate.Value).ToShortDateString());
            param.Add("SHS_LOC_RANK", txtRanking.Text);
            param.Add("SHS_LEVEL", txtLevel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "LocDate_Postchng", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult != null)
                {
                    txtLocalTIRPercent.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtIncrementPerRs.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    txtReviseShadowSal.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    flag = true;
                }
                
                else
                {
                MessageBox.Show("TIR % for This Year Not Updated ........");
                e.Cancel = true;
                }

            }
            


            if ((FunctionConfig.CurrentOption == Function.View) && (FunctionConfig.CurrentOption == Function.Delete))
            {
                Result rslt2;
                Dictionary<string, object> param2 = new Dictionary<string, object>();
                param2.Add("SHS_PR_NO", txtShs_pr_No.Text);
                param2.Add("SHS_INC_LAST_DATE", Convert.ToDateTime(DtLastInc.Value).ToShortDateString());
                rslt2 = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "Loc_date_MV", param);
                if (rslt2.isSuccessful)
                {
                    if (rslt2.dstResult.Tables[0].Rows.Count > 0 && rslt2.dstResult != null)
                    {
                        txtReviseShadowSal.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    }
                }


            }
        }
        private void txtRemarks_Validating(object sender, CancelEventArgs e)
        {
            if (FunctionConfig.CurrentOption == Function.Modify)
            {
                if (MessageBox.Show("Do You Want To Update this Record[Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    proc_Modify();
                    MessageBox.Show("Record Updated Sucessfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Cancel();
                }

                else
                { return; }
            }
            else
                if (FunctionConfig.CurrentOption == Function.Add)
                {
                    int number = 0;
                    if (MessageBox.Show("Do You Want To Save the Record[Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (txtReviseShadowSal.Text != null && txtReviseShadowSal.Text != "" && txtReviseShadowSal.Text.ToString() != string.Empty)
                        {
                            txtAnnualShadowSal.Text = txtReviseShadowSal.Text;
                            saveRec();
                            base.DoToolbarActions(this.Controls, "Save");
                            base.IterateFormToEnableControls(PnlShadowSalary.Controls, false);
                           
                                               

                        }

                        else
                        {
                            txtReviseShadowSal.Text = System.Convert.ToString(number);
                            saveRec();
                            base.DoToolbarActions(this.Controls, "Save");
                            base.IterateFormToEnableControls(PnlShadowSalary.Controls, false);
                        }

                    }

                    else
                    {
                        base.ClearForm(PnlShadowSalary.Controls);

                    }

                }


        }
        private void DtLastInc_Validating(object sender, CancelEventArgs e)
        {

            # region proc_mod_Del
            if (FunctionConfig.CurrentOption == Function.Modify)
            {
                proc_mod_Del();

                Proc_mod_DEl_Ext();
            }
            #endregion
            #region proc_view_add
            if ((FunctionConfig.CurrentOption == Function.Add) || (FunctionConfig.CurrentOption == Function.View))
            {
                proc_view_add();
                if (FunctionConfig.CurrentOption == Function.View)
                {
                    Result rslt5;
                    Dictionary<string, object> param5 = new Dictionary<string, object>();
                    param5.Add("SHS_LOC_RANK", txtRanking.Text);
                    param5.Add("SHS_LEVEL",txtLevel.Text);
                    param5.Add("SHS_LOC_DATE", Convert.ToDateTime(dtRankingDate.Value).ToShortDateString());
                    rslt5 = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "View_Tir", param5);
                    if (rslt5.isSuccessful)
                    {
                        if (rslt5.dstResult.Tables[0].Rows.Count > 0 && rslt5.dstResult != null)
                        {
                            txtLocalTIRPercent.Text = rslt5.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                            txtIncrementPerRs.Text = rslt5.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                        }
                    }
                
                }

            }
             #endregion
            #region chk Wheter record Already Exists
            if (FunctionConfig.CurrentOption == Function.Add)
            {
                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("shs_pr_no", txtShs_pr_No.Text);
                param.Add("shs_inc_last_date", Convert.ToDateTime(DtLastInc.Value).ToShortDateString());
                rslt = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "Total_count", param);
                if (rslt.isSuccessful)
                {
                    if ((rslt.dstResult != null) && (rslt.dstResult.Tables[0].Rows.Count > 0))
                    {
                        int count = 0;
                        count = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                        if (count > 0)
                        {
                            MessageBox.Show("Record Already Entered");
                            // e.Cancel = true;
                            base.ClearForm(this.Controls);
                        }

                    }
                }

            }

             #endregion
            #region View Record

            if (FunctionConfig.CurrentOption == Function.View)
            {
                if (MessageBox.Show("Do You Want To View More Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    base.ClearForm(PnlShadowSalary.Controls);
                    txtShs_pr_No.Select();
                    txtShs_pr_No.Focus();
                }


                else
                {
                    this.Cancel();
                    // base.ClearForm(PnlShadowSalary.Controls);
                }
            }

             #endregion
            #region Delete record

            if (FunctionConfig.CurrentOption == Function.Delete)
            {
                if (MessageBox.Show("Do You Want To Delete This Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Result rslt;
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("SHS_ANL_SHD_SAL", txtAnnualShadowSal.Text);
                    param.Add("shs_pr_no", txtShs_pr_No.Text);
                    rslt = cmnDM.GetData("CHRIS_SP_SHADOW_SALARY_MANAGER", "Delete_rcord", param);
                    
                    if (Proc_delete())
                    {
                        MessageBox.Show("Record Deleted Sucessfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    this.Cancel();

                }


                else
                {
                    // base.ClearForm(PnlShadowSalary.Controls);
                    this.Cancel();
                }
            }
             #endregion
        }
        private void txtOption_TextChanged(object sender, EventArgs e)
        {
            //if ((txtOption.Text != "A") && (txtOption.Text != "V") && (txtOption.Text != "M") && (txtOption.Text != "D") && (txtOption.Text == string.Empty))
            //{
            //    lbPr_p_no.Enabled = false;

            //}
            //else
            //{
            //    lbPr_p_no.Enabled = true;
            //}
        }
        private void txtIs_TirPercent_Validating(object sender, CancelEventArgs e)
            {

            float tirper = 0;

            bool ValidTIRDec = IsValidExpression(txtIs_TirPercent.Text, InputValidator.TWODECIMAL_REGEX);
            bool ValidTIRINT = IsValidExpression(txtIs_TirPercent.Text, InputValidator.NUMBER_REGEX);
            bool ValidTIRONEDEC = IsValidExpression(txtIs_TirPercent.Text, InputValidator.ONEDECIMAL_REGEX);
            if (ValidTIRINT)
            {
                string val              = txtIs_TirPercent.Text;
                string newval           = val + ".00";
                txtIs_TirPercent.Text   = newval;
                return;
            }

            if (ValidTIRONEDEC)
            {

                string val = txtIs_TirPercent.Text;
                string newval = val + "0";
                txtIs_TirPercent.Text = newval;
                return;
            
            
            
            }

            if (!ValidTIRDec)
            {
                MessageBox.Show("Field must be of form 99.99.");
                e.Cancel = true;
            }

            //if (float.TryParse(txtIs_TirPercent.Text, out tirper))
            //{
            //    string str = tirper.ToString();
            //    if  (str.IndexOf(".") != -1)
            //    {
            //        //Console.Write("string contains 'Vader'");
            //        if (str.IndexOf(".") > 2)
            //        {
            //            MessageBox.Show("Field must be of form 99.99.");
            //            e.Cancel = true;
            //        }                 



            //    }

            //    else
            //    {

            //        MessageBox.Show("Field must be of form 99.99.");
            //        e.Cancel = true;

            //    }



            //}
            //else
            //{
            //    txtIs_TirPercent.Text = "";
            //    txtIs_TirPercent.Select();
            //    txtIs_TirPercent.Focus();

            //}

        }
        #endregion
    }
}