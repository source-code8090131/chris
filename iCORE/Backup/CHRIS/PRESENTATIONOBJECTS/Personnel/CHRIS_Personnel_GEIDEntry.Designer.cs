namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_GEIDEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_GEIDEntry));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnExit = new CrplControlLibrary.SLButton();
            this.btnClear = new CrplControlLibrary.SLButton();
            this.btnDelete = new CrplControlLibrary.SLButton();
            this.btnQuery = new CrplControlLibrary.SLButton();
            this.btnModify = new CrplControlLibrary.SLButton();
            this.btnSave = new CrplControlLibrary.SLButton();
            this.btnAdd = new CrplControlLibrary.SLButton();
            this.dtpGDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtpDOB = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtPersNo2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtRecType = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLastName = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtGeidNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCountPayLoc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblHeader = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 348);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pnlDetail);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(40, 100);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(560, 250);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.groupBox2);
            this.pnlDetail.Controls.Add(this.dtpGDate);
            this.pnlDetail.Controls.Add(this.dtpDOB);
            this.pnlDetail.Controls.Add(this.txtPersNo2);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.txtRecType);
            this.pnlDetail.Controls.Add(this.txtLastName);
            this.pnlDetail.Controls.Add(this.label6);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtGeidNo);
            this.pnlDetail.Controls.Add(this.txtCountPayLoc);
            this.pnlDetail.Controls.Add(this.txtFirstName);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label5);
            this.pnlDetail.Controls.Add(this.label4);
            this.pnlDetail.Controls.Add(this.label3);
            this.pnlDetail.Controls.Add(this.label2);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GEIDCommand";
            this.pnlDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F);
            this.pnlDetail.Location = new System.Drawing.Point(3, 16);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(554, 231);
            this.pnlDetail.SPName = "CHRIS_SP_GEID_MANAGER";
            this.pnlDetail.TabIndex = 0;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "";
            this.lbtnPNo.ActionType = "";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = false;
            this.lbtnPNo.Location = new System.Drawing.Point(246, 33);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = true;
            this.lbtnPNo.SPName = "CHRIS_SP_GEID_MANAGER";
            this.lbtnPNo.TabIndex = 22;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            this.lbtnPNo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtnPNo_MouseDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnExit);
            this.groupBox2.Controls.Add(this.btnClear);
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnQuery);
            this.groupBox2.Controls.Add(this.btnModify);
            this.groupBox2.Controls.Add(this.btnSave);
            this.groupBox2.Controls.Add(this.btnAdd);
            this.groupBox2.Location = new System.Drawing.Point(31, 165);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(490, 47);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            // 
            // btnExit
            // 
            this.btnExit.ActionType = "";
            this.btnExit.Location = new System.Drawing.Point(416, 15);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(65, 23);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnClear
            // 
            this.btnClear.ActionType = "";
            this.btnClear.Location = new System.Drawing.Point(348, 15);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(65, 23);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.ActionType = "";
            this.btnDelete.Location = new System.Drawing.Point(280, 15);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(65, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.ActionType = "";
            this.btnQuery.Location = new System.Drawing.Point(212, 15);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(65, 23);
            this.btnQuery.TabIndex = 3;
            this.btnQuery.Text = "Query";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnModify
            // 
            this.btnModify.ActionType = "";
            this.btnModify.Location = new System.Drawing.Point(144, 15);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(65, 23);
            this.btnModify.TabIndex = 2;
            this.btnModify.Text = "Modify";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnSave
            // 
            this.btnSave.ActionType = "";
            this.btnSave.Location = new System.Drawing.Point(76, 15);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(65, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ActionType = "";
            this.btnAdd.Location = new System.Drawing.Point(9, 15);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(65, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btn_Click);
            // 
            // dtpGDate
            // 
            this.dtpGDate.CustomEnabled = false;
            this.dtpGDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpGDate.DataFieldMapping = "GEI_DATE";
            this.dtpGDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpGDate.HasChanges = false;
            this.dtpGDate.Location = new System.Drawing.Point(412, 113);
            this.dtpGDate.Name = "dtpGDate";
            this.dtpGDate.NullValue = " ";
            this.dtpGDate.Size = new System.Drawing.Size(100, 20);
            this.dtpGDate.TabIndex = 7;
            this.dtpGDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            // 
            // dtpDOB
            // 
            this.dtpDOB.CustomEnabled = false;
            this.dtpDOB.CustomFormat = "dd-MMM-yyyy";
            this.dtpDOB.DataFieldMapping = "GEI_BIRTH_DATE";
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOB.HasChanges = false;
            this.dtpDOB.Location = new System.Drawing.Point(412, 61);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.NullValue = " ";
            this.dtpDOB.Size = new System.Drawing.Size(100, 20);
            this.dtpDOB.TabIndex = 3;
            this.dtpDOB.TabStop = false;
            this.dtpDOB.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            // 
            // txtPersNo2
            // 
            this.txtPersNo2.AllowSpace = true;
            this.txtPersNo2.AssociatedLookUpName = "";
            this.txtPersNo2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo2.ContinuationTextBox = null;
            this.txtPersNo2.CustomEnabled = false;
            this.txtPersNo2.DataFieldMapping = "GEI_PR_P_NO";
            this.txtPersNo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo2.GetRecordsOnUpDownKeys = false;
            this.txtPersNo2.IsDate = false;
            this.txtPersNo2.Location = new System.Drawing.Point(412, 139);
            this.txtPersNo2.Name = "txtPersNo2";
            this.txtPersNo2.NumberFormat = "###,###,##0.00";
            this.txtPersNo2.Postfix = "";
            this.txtPersNo2.Prefix = "";
            this.txtPersNo2.Size = new System.Drawing.Size(100, 20);
            this.txtPersNo2.SkipValidation = false;
            this.txtPersNo2.TabIndex = 9;
            this.txtPersNo2.TextType = CrplControlLibrary.TextType.String;
            this.txtPersNo2.Visible = false;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(302, 139);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 20);
            this.label10.TabIndex = 16;
            this.label10.Text = "Personnel No";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label10.Visible = false;
            // 
            // txtRecType
            // 
            this.txtRecType.AllowSpace = true;
            this.txtRecType.AssociatedLookUpName = "";
            this.txtRecType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRecType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRecType.ContinuationTextBox = null;
            this.txtRecType.CustomEnabled = true;
            this.txtRecType.DataFieldMapping = "GEI_REC_TYPE";
            this.txtRecType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRecType.GetRecordsOnUpDownKeys = false;
            this.txtRecType.IsDate = false;
            this.txtRecType.Location = new System.Drawing.Point(412, 87);
            this.txtRecType.MaxLength = 1;
            this.txtRecType.Name = "txtRecType";
            this.txtRecType.NumberFormat = "###,###,##0.00";
            this.txtRecType.Postfix = "";
            this.txtRecType.Prefix = "";
            this.txtRecType.Size = new System.Drawing.Size(31, 20);
            this.txtRecType.SkipValidation = false;
            this.txtRecType.TabIndex = 5;
            this.txtRecType.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLastName
            // 
            this.txtLastName.AllowSpace = true;
            this.txtLastName.AssociatedLookUpName = "";
            this.txtLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastName.ContinuationTextBox = null;
            this.txtLastName.CustomEnabled = true;
            this.txtLastName.DataFieldMapping = "GEI_LAST_NAME";
            this.txtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastName.GetRecordsOnUpDownKeys = false;
            this.txtLastName.IsDate = false;
            this.txtLastName.Location = new System.Drawing.Point(412, 35);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.NumberFormat = "###,###,##0.00";
            this.txtLastName.Postfix = "";
            this.txtLastName.Prefix = "";
            this.txtLastName.ReadOnly = true;
            this.txtLastName.Size = new System.Drawing.Size(130, 20);
            this.txtLastName.SkipValidation = false;
            this.txtLastName.TabIndex = 1;
            this.txtLastName.TabStop = false;
            this.txtLastName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(302, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Gei Date";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(302, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 20);
            this.label7.TabIndex = 10;
            this.label7.Text = "Rec Type";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(302, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Date Of Birth";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(302, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Last Name";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtGeidNo
            // 
            this.txtGeidNo.AllowSpace = true;
            this.txtGeidNo.AssociatedLookUpName = "";
            this.txtGeidNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGeidNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGeidNo.ContinuationTextBox = null;
            this.txtGeidNo.CustomEnabled = true;
            this.txtGeidNo.DataFieldMapping = "GEI_GEID_NO";
            this.txtGeidNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGeidNo.GetRecordsOnUpDownKeys = false;
            this.txtGeidNo.IsDate = false;
            this.txtGeidNo.Location = new System.Drawing.Point(140, 113);
            this.txtGeidNo.MaxLength = 10;
            this.txtGeidNo.Name = "txtGeidNo";
            this.txtGeidNo.NumberFormat = "###,###,##0.00";
            this.txtGeidNo.Postfix = "";
            this.txtGeidNo.Prefix = "";
            this.txtGeidNo.Size = new System.Drawing.Size(100, 20);
            this.txtGeidNo.SkipValidation = false;
            this.txtGeidNo.TabIndex = 6;
            this.txtGeidNo.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtCountPayLoc
            // 
            this.txtCountPayLoc.AllowSpace = true;
            this.txtCountPayLoc.AssociatedLookUpName = "";
            this.txtCountPayLoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountPayLoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountPayLoc.ContinuationTextBox = null;
            this.txtCountPayLoc.CustomEnabled = true;
            this.txtCountPayLoc.DataFieldMapping = "GEI_COUNT_PAY_LOC";
            this.txtCountPayLoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountPayLoc.GetRecordsOnUpDownKeys = false;
            this.txtCountPayLoc.IsDate = false;
            this.txtCountPayLoc.Location = new System.Drawing.Point(140, 87);
            this.txtCountPayLoc.MaxLength = 2;
            this.txtCountPayLoc.Name = "txtCountPayLoc";
            this.txtCountPayLoc.NumberFormat = "###,###,##0.00";
            this.txtCountPayLoc.Postfix = "";
            this.txtCountPayLoc.Prefix = "";
            this.txtCountPayLoc.Size = new System.Drawing.Size(30, 20);
            this.txtCountPayLoc.SkipValidation = false;
            this.txtCountPayLoc.TabIndex = 4;
            this.txtCountPayLoc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "GEI_FIRST_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(140, 61);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(149, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 2;
            this.txtFirstName.TabStop = false;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "GEI_PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.Location = new System.Drawing.Point(140, 35);
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.ReadOnly = true;
            this.txtPersNo.Size = new System.Drawing.Size(100, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 0;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.String;
            this.txtPersNo.TextChanged += new System.EventHandler(this.txtPersNo_TextChanged);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(32, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Geid No";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(32, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Count Pay Loc";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(32, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "First Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(32, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Personnel No";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblHeader
            // 
            this.lblHeader.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(34, 51);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(574, 34);
            this.lblHeader.TabIndex = 11;
            this.lblHeader.Text = "GEID Entry Form";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(333, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(44, 15);
            this.lblUserName.TabIndex = 13;
            this.lblUserName.Text = "CHRIS";
            // 
            // CHRIS_Personnel_GEIDEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 408);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.groupBox1);
            this.CurrentPanelBlock = "pnlDetail";
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F);
            this.Name = "CHRIS_Personnel_GEIDEntry";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRISTOP - GEID Entry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_GEIDEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.lblHeader, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txtGeidNo;
        private CrplControlLibrary.SLTextBox txtCountPayLoc;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private CrplControlLibrary.SLTextBox txtRecType;
        private CrplControlLibrary.SLTextBox txtLastName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtPersNo2;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLDatePicker dtpGDate;
        private CrplControlLibrary.SLDatePicker dtpDOB;
        private System.Windows.Forms.GroupBox groupBox2;
        private CrplControlLibrary.SLButton btnExit;
        private CrplControlLibrary.SLButton btnClear;
        private CrplControlLibrary.SLButton btnDelete;
        private CrplControlLibrary.SLButton btnQuery;
        private CrplControlLibrary.SLButton btnModify;
        private CrplControlLibrary.SLButton btnSave;
        private CrplControlLibrary.SLButton btnAdd;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private System.Windows.Forms.Label lblUserName;
    }
}