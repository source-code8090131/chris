namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_IncrementOfficers_Level
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_IncrementOfficers_Level));
            this.PnlIncPromotionClerical = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.LBDesig = new CrplControlLibrary.LookupButton(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtfunctitl2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfunctitle1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlIncPromotionClerical.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(309, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(345, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 176);
            this.panel1.Size = new System.Drawing.Size(345, 60);
            // 
            // PnlIncPromotionClerical
            // 
            this.PnlIncPromotionClerical.ConcurrentPanels = null;
            this.PnlIncPromotionClerical.Controls.Add(this.LBDesig);
            this.PnlIncPromotionClerical.Controls.Add(this.label3);
            this.PnlIncPromotionClerical.Controls.Add(this.label2);
            this.PnlIncPromotionClerical.Controls.Add(this.txtfunctitl2);
            this.PnlIncPromotionClerical.Controls.Add(this.txtfunctitle1);
            this.PnlIncPromotionClerical.Controls.Add(this.txtLevel);
            this.PnlIncPromotionClerical.DataManager = null;
            this.PnlIncPromotionClerical.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlIncPromotionClerical.DependentPanels = null;
            this.PnlIncPromotionClerical.DisableDependentLoad = false;
            this.PnlIncPromotionClerical.EnableDelete = true;
            this.PnlIncPromotionClerical.EnableInsert = true;
            this.PnlIncPromotionClerical.EnableQuery = false;
            this.PnlIncPromotionClerical.EnableUpdate = true;
            this.PnlIncPromotionClerical.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncrementPromotionClericalCommand";
            this.PnlIncPromotionClerical.Location = new System.Drawing.Point(12, 40);
            this.PnlIncPromotionClerical.MasterPanel = null;
            this.PnlIncPromotionClerical.Name = "PnlIncPromotionClerical";
            this.PnlIncPromotionClerical.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlIncPromotionClerical.Size = new System.Drawing.Size(322, 134);
            this.PnlIncPromotionClerical.SPName = "CHRIS_SP_INC_PROMOTION_MANAGER";
            this.PnlIncPromotionClerical.TabIndex = 14;
            // 
            // LBDesig
            // 
            this.LBDesig.ActionLOVExists = "Lov_desigExists";
            this.LBDesig.ActionType = "CheckLevel";
            this.LBDesig.ConditionalFields = "";
            this.LBDesig.CustomEnabled = true;
            this.LBDesig.DataFieldMapping = "";
            this.LBDesig.DependentLovControls = "";
            this.LBDesig.HiddenColumns = "";
            this.LBDesig.Image = ((System.Drawing.Image)(resources.GetObject("LBDesig.Image")));
            this.LBDesig.LoadDependentEntities = false;
            this.LBDesig.Location = new System.Drawing.Point(196, 22);
            this.LBDesig.LookUpTitle = null;
            this.LBDesig.Name = "LBDesig";
            this.LBDesig.Size = new System.Drawing.Size(26, 21);
            this.LBDesig.SkipValidationOnLeave = false;
            this.LBDesig.SPName = "CHRIS_SP_INC_PROMOTION_Increment_MANAGER";
            this.LBDesig.TabIndex = 29;
            this.LBDesig.TabStop = false;
            this.LBDesig.UseVisualStyleBackColor = true;
            this.LBDesig.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Function Title :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(101, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Level ";
            // 
            // txtfunctitl2
            // 
            this.txtfunctitl2.AllowSpace = true;
            this.txtfunctitl2.AssociatedLookUpName = "";
            this.txtfunctitl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfunctitl2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfunctitl2.ContinuationTextBox = null;
            this.txtfunctitl2.CustomEnabled = false;
            this.txtfunctitl2.DataFieldMapping = "";
            this.txtfunctitl2.Enabled = false;
            this.txtfunctitl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfunctitl2.GetRecordsOnUpDownKeys = false;
            this.txtfunctitl2.IsDate = false;
            this.txtfunctitl2.Location = new System.Drawing.Point(32, 105);
            this.txtfunctitl2.Name = "txtfunctitl2";
            this.txtfunctitl2.NumberFormat = "###,###,##0.00";
            this.txtfunctitl2.Postfix = "";
            this.txtfunctitl2.Prefix = "";
            this.txtfunctitl2.Size = new System.Drawing.Size(265, 20);
            this.txtfunctitl2.SkipValidation = false;
            this.txtfunctitl2.TabIndex = 24;
            this.txtfunctitl2.TabStop = false;
            this.txtfunctitl2.TextType = CrplControlLibrary.TextType.String;
            this.txtfunctitl2.Validating += new System.ComponentModel.CancelEventHandler(this.txtfunctitl2_Validating);
            // 
            // txtfunctitle1
            // 
            this.txtfunctitle1.AllowSpace = true;
            this.txtfunctitle1.AssociatedLookUpName = "";
            this.txtfunctitle1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfunctitle1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfunctitle1.ContinuationTextBox = null;
            this.txtfunctitle1.CustomEnabled = false;
            this.txtfunctitle1.DataFieldMapping = "";
            this.txtfunctitle1.Enabled = false;
            this.txtfunctitle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfunctitle1.GetRecordsOnUpDownKeys = false;
            this.txtfunctitle1.IsDate = false;
            this.txtfunctitle1.Location = new System.Drawing.Point(32, 79);
            this.txtfunctitle1.Name = "txtfunctitle1";
            this.txtfunctitle1.NumberFormat = "###,###,##0.00";
            this.txtfunctitle1.Postfix = "";
            this.txtfunctitle1.Prefix = "";
            this.txtfunctitle1.Size = new System.Drawing.Size(265, 20);
            this.txtfunctitle1.SkipValidation = false;
            this.txtfunctitle1.TabIndex = 23;
            this.txtfunctitle1.TabStop = false;
            this.txtfunctitle1.TextType = CrplControlLibrary.TextType.String;
            this.txtfunctitle1.Validating += new System.ComponentModel.CancelEventHandler(this.txtfunctitle1_Validating);
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = false;
            this.txtLevel.DataFieldMapping = "PR_LEVEL_PRESENT";
            this.txtLevel.Enabled = false;
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(146, 22);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.Size = new System.Drawing.Size(44, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 22;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            this.txtLevel.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel_Validating);
            // 
            // CHRIS_Personnel_IncrementOfficers_Level
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 236);
            this.Controls.Add(this.PnlIncPromotionClerical);
            this.Location = new System.Drawing.Point(250, 250);
            this.Name = "CHRIS_Personnel_IncrementOfficers_Level";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Personnel_IncrementOfficers_Level";
            this.Shown += new System.EventHandler(this.CHRIS_Personnel_IncrementOfficers_Level_Shown);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlIncPromotionClerical, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlIncPromotionClerical.ResumeLayout(false);
            this.PnlIncPromotionClerical.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlIncPromotionClerical;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtfunctitl2;
        private CrplControlLibrary.SLTextBox txtfunctitle1;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.LookupButton LBDesig;
    }
}