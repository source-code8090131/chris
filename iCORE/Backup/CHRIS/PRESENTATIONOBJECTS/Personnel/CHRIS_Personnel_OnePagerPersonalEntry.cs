using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_OnePagerPersonalEntry : ChrisMasterDetailForm
    {

        #region Declarations

        Dictionary<string, object> objVals = new Dictionary<string, object>();
        public DataTable dtPreEmp = null;
        public DataTable dtEdu = null;
        public DataTable dtTraining = null;
        public DataTable dtCitiEmpRef = null;
        public DataTable dtStatus = null;

        DataTable dtDummy = new DataTable();

        public int prNum;
        TextBox txt = new TextBox();
             

        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory frm_EmpHistory;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Education frm_Education;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Training frm_Training;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_CitiRef frm_CitiRef;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Save frm_Save;    

        CmnDataManager cmnDM = new CmnDataManager();
        Result rslt, rsltDummy;

        #endregion

        #region Constructor
        public CHRIS_Personnel_OnePagerPersonalEntry()
        {
            InitializeComponent();

            this.txtPersonnelNO.Focus();
        }
        public CHRIS_Personnel_OnePagerPersonalEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(this.pnlPersonal);
            lstDependentPanels.Add(this.pnlChildren);
            
            lstDependentPanels.Add(this.pnlEmpHistory);
            lstDependentPanels.Add(this.pnlEducation);
            lstDependentPanels.Add(this.pnlTraining);
            lstDependentPanels.Add(this.pnlCitiReference);
            
            
            this.PnlPersonnel.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(PnlPersonnel);
            
            this.ShowBottomBar.Equals(false);
            this.txtOption.Visible = false;
            

            lblUserName.Text += "   " + this.UserName;
            dgvChild.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.CurrentPanelBlock = this.PnlPersonnel.Name;
            txtPersonnelNO.Focus();

        }
        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                this.txtPersonnelNO.Focus();
                this.KeyPreview = true;
                this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
                tbtList.Visible = false;
                tbtDelete.Visible = false;

                dgvChild.Enabled = true;

            }
            catch (Exception exp)
            {
                LogError(this.Name, "OnLoad", exp);
            }


        }
        public void ClearForm(Control.ControlCollection ctrlList)
        {
            TextBox txt = null;
            DateTimePicker dtp = null;
            ComboBox cmb = null;
            CheckBox chk = null;
            DataGridView dgv = null;

            foreach (Control ctrl in ctrlList)
            {
                if (ctrl.Controls.Count > 0)
                {
                    if ((dgv = ctrl as DataGridView) != null)
                    {
                        dgv.DataSource = null;
                    }
                    else
                    {
                        ClearForm(ctrl.Controls);
                    }
                }
                else
                {
                    if (ctrl.Tag == null || ctrl.Tag.ToString() != "1")
                    {
                        if ((txt = ctrl as TextBox) != null)
                        {
                            txt.Text = "";
                        }
                        else if ((dtp = ctrl as DateTimePicker) != null)
                        {
                            dtp.Value = DateTime.Now;
                        }
                        else if ((cmb = ctrl as ComboBox) != null)
                        {
                            if (cmb.Items.Count > 0)
                                cmb.SelectedIndex = 0;
                        }
                        else if ((chk = ctrl as CheckBox) != null)
                        {
                            chk.Checked = false;
                        }
                    }
                }
            }
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                lbpersonnel.SkipValidationOnLeave = true;
                lkupBtnRepToNum.SkipValidationOnLeave = true;
                base.DoToolbarActions(this.Controls, "Save");
                lbpersonnel.SkipValidationOnLeave = false;
                lkupBtnRepToNum.SkipValidationOnLeave = false;

                //return;
            }
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                this.ClearForm(PnlPersonnel.Controls);
                this.ClearForm(pnlPersonal.Controls);
                this.ClearForm(pnlChildren.Controls);

                this.ClearForm(pnlCitiReference.Controls);
                this.ClearForm(pnlEmpHistory.Controls);
                this.ClearForm(pnlTraining.Controls);
                this.ClearForm(pnlEducation.Controls);
                this.txtPersonnelNO.Select();
                this.txtPersonnelNO.Focus();
                return;
            }

        }
        public void CallSave(string status)
        {
                                                                    
            try
            {

                CHRIS_Personnel_OnePagerPersonalEntry_Save frmSave = new CHRIS_Personnel_OnePagerPersonalEntry_Save();
            
                if (status == "N")
                {                   
                    //frmSave.CloseSaveDialog();
                    //frmSave.Close();
                    frmSave.Hide();
                    //this.Show();
                    ClearForm(PnlPersonnel.Controls);
                    ClearForm(pnlPersonal.Controls);
                    ClearForm(pnlChildren.Controls);
                }
                else if (status == "Y")
                {                   
                    lbpersonnel.SkipValidationOnLeave = true;
                    lkupBtnRepToNum.SkipValidationOnLeave = true;
                    base.DoToolbarActions(this.Controls, "Save");
                    lbpersonnel.SkipValidationOnLeave = false;
                    lkupBtnRepToNum.SkipValidationOnLeave = false;
                                       
               
                    //this.Show();
                    this.Hide();
                    //frmSave.MdiParent = null;
                    //frmSave.CloseSaveDialog();
                    //frmSave.TopMost = false;
                    frmSave.Visible = false;
                    frmSave.Hide();
                    //frmSave.Close();

                    //calling CITI_EMPAD Report//
                    CallReport();

                    ClearForm(PnlPersonnel.Controls);
                    ClearForm(pnlPersonal.Controls);
                    ClearForm(pnlChildren.Controls);
                   
                    
                }
                else
                {
                    ClearForm(PnlPersonnel.Controls);
                    ClearForm(pnlPersonal.Controls);
                    ClearForm(pnlChildren.Controls);
                }

            }
            catch (Exception exp)
            {
                LogException(this.Name, "CallSave", exp);
            }

            this.ShowDialog(this);
            this.txtPersonnelNO.Focus(); 

        }
        private void CallReport()
        {

            try
            {


                CHRIS_Personnel_OnePagerPersonalEntry_Save frmSave = new CHRIS_Personnel_OnePagerPersonalEntry_Save();
                frmSave.MdiParent = null;
                frmSave.Hide();
                frmSave.Close();



                iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                 new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

                System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

                #region Set Input Parameters to report

                GroupBox pnl = new GroupBox();

                


                //CrplControlLibrary.SLTextBox txtDESTYPE = new CrplControlLibrary.SLTextBox(comp);
                //txtDESTYPE.Name = "PF_DESTYPE";
                //txtDESTYPE.Text = "FILE";
                //pnl.Controls.Add(txtDESTYPE);

                //CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
                //txtDESNAME.Name = "PF_DESNAME";
                //txtDESNAME.Text = "";
                //pnl.Controls.Add(txtDESNAME);

                //CrplControlLibrary.SLTextBox txtFORMAT = new CrplControlLibrary.SLTextBox(comp);
                //txtFORMAT.Name = "PF_DESFORMAT";
                //txtFORMAT.Text = "CHARACTER";
                //pnl.Controls.Add(txtFORMAT);

                //CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
                //txtMODE.Name = "txtMODE";
                //txtMODE.Text = "PF_MODE1";
                //pnl.Controls.Add(txtMODE);

                CrplControlLibrary.SLTextBox txtBRANCH = new CrplControlLibrary.SLTextBox(comp);
                txtBRANCH.Name = "BRN";
                txtBRANCH.Text = this.txtBranch.Text;
                pnl.Controls.Add(txtBRANCH);

                CrplControlLibrary.SLTextBox txtSPNO = new CrplControlLibrary.SLTextBox(comp);
                txtSPNO.Name = "SPNO";
                txtSPNO.Text = Convert.ToString(txtPersonnelNO.Text);
                pnl.Controls.Add(txtSPNO);

                CrplControlLibrary.SLTextBox txtTPNO = new CrplControlLibrary.SLTextBox(comp);
                txtTPNO.Name = "TPNO";
                txtTPNO.Text = Convert.ToString(txtPersonnelNO.Text);
                pnl.Controls.Add(txtTPNO);            

                CrplControlLibrary.SLTextBox txtSEG = new CrplControlLibrary.SLTextBox(comp);
                txtSEG.Name = "SEG";
                txtSEG.Text = null;
                pnl.Controls.Add(txtSEG);

                CrplControlLibrary.SLTextBox txtDEPT = new CrplControlLibrary.SLTextBox(comp);
                txtDEPT.Name = "DPT";
                txtDEPT.Text = this.txtCurrDept.Text;
                pnl.Controls.Add(txtDEPT);

                CrplControlLibrary.SLTextBox txtLVL = new CrplControlLibrary.SLTextBox(comp);
                txtLVL.Name = "LEV";
                txtLVL.Text = this.txtLevel.Text;
                pnl.Controls.Add(txtLVL);

                CrplControlLibrary.SLTextBox txtDESIG = new CrplControlLibrary.SLTextBox(comp);
                txtDESIG.Name = "P_DESIG";
                txtDESIG.Text = this.txtDesignation.Text;
                pnl.Controls.Add(txtDESIG);

                CrplControlLibrary.SLTextBox txtGRP = new CrplControlLibrary.SLTextBox(comp);
                txtGRP.Name = "P_GRP";
                txtGRP.Text = this.txtCurrGrp.Text;
                pnl.Controls.Add(txtGRP);

                #endregion

                frm.Controls.Add(pnl);
                frm.RptFileName = "CITI_EMPAD";
                frm.Owner = this;
                frm.RunReport();

            }
            catch (Exception exp)
            {
                LogException(this.Name, "CallReport", exp);
            }
            
            txtPersonnelNO.Focus();


        }

        #endregion

        #region Events

        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {

            if (slDatePicker2.Text == "" || slDatePicker2.Text==string.Empty || slDatePicker2.Value==null )
            {
                slDatePicker1.Focus();
                return;
            }
                            
            
            try
            {

    
                if (e.Modifiers == Keys.Control)
                {
                    switch (e.KeyValue)
                    {
                        case 34:

                            frm_EmpHistory = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvEmpHistory.GridSource, dgvEducation.GridSource, dgvTraining.GridSource);
                            //frm_EmpHistory = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean,this);
                            frm_EmpHistory.MdiParent = null;
                            frm_EmpHistory.Enabled = true;
                            frm_EmpHistory.Select();
                            frm_EmpHistory.Focus();
                            frm_EmpHistory.ShowDialog(this);

                            this.KeyPreview = true;
                            txtPersonnelNO.Select();
                            txtPersonnelNO.Focus();
                            break;
                   }


                }
                if (e.Modifiers == Keys.Shift)
                {

                    if (e.KeyCode == Keys.F7)
                    {
                        //switch (e.KeyValue)
                        //{
                            //Refreshing the Fields [SHIFT+F7]//
                           // case 16:

                                this.ResetText();
                                ClearForm(PnlPersonnel.Controls);
                                ClearForm(pnlPersonal.Controls);
                                ClearForm(pnlChildren.Controls);
                                this.KeyPreview = true;
                              //  break;
                      //  }
                    }

                }

            }
            catch (Exception exp)
            {
                LogError(this.Name, "ShortCutKey_Press", exp);
            }
        }
    
        private void dgvChild_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            
           
            //if (!this.dgvChild.CurrentCell.IsInEditMode)
            //{
            //    return;
            //}
            //if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
           // {
                if (e.ColumnIndex == 2)
                {
                    
                    if (dgvChild.CurrentRow.Cells[2].EditedFormattedValue.ToString() != "" || dgvChild.CurrentRow.Cells[2].EditedFormattedValue.ToString() != null)
                    {
                        
                        string cSx = dgvChild.CurrentRow.Cells[2].EditedFormattedValue.ToString();
                        if (cSx.ToString() != String.Empty)
                        {
                            if (cSx.ToUpper() == "M" || cSx.ToUpper() == "F" || cSx.ToUpper() == "m" || cSx.ToUpper() == "f")
                            {
                                this.dgvChild.CurrentCell.Value = cSx.ToUpper();
                            }
                            else
                            {
                                MessageBox.Show("Valid Value is : F OR M");
                                e.Cancel = true;
                            }
                        }
                    }
                }

            //}




        }

        private void CHRIS_Personnel_OnePagerPersonalEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {

            //if (txtPersonnelNO.Text != "")
            //{

            //    this.objVals.Clear();
            //    this.objVals.Add("PR_P_NO", txtPersonnelNO.Text);
                
            //    rslt = cmnDM.GetData("CHRIS_SP_ONEPAGERPERSONALENTRY_MANAGER", "DUMMY", objVals);

            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult.Tables.Count > 0)
            //        {
            //            dtDummy = rslt.dstResult.Tables[0];
            //            if (dtDummy.Rows.Count > 0)
            //            {

            //                txtFirstName.Text = Convert.ToString(dtDummy.Rows[0]["PR_FIRST_NAME"]);
            //                txtLastName.Text = Convert.ToString(dtDummy.Rows[0]["PR_LAST_NAME"]);
            //                txtLevel.Text = Convert.ToString(dtDummy.Rows[0]["LVL"]);
            //                txtCurrGrp.Text = Convert.ToString(dtDummy.Rows[0]["GRP"]);
            //                txtCurrDept.Text = Convert.ToString(dtDummy.Rows[0]["DEPT"]);
            //                txtDesignation.Text = Convert.ToString(dtDummy.Rows[0]["DESIG"]);
            //                txtGEID.Text = Convert.ToString(dtDummy.Rows[0]["GEI_GEID_NO"]);
            //            }
            //            else
            //            {
            //                txtLevel.Text = "";
            //                txtCurrGrp.Text = "";
            //                txtCurrDept.Text = "";
            //                txtDesignation.Text = "";
            //                txtGEID.Text = "";
            //            }

            //        }
            //    }

            //}

        }
              
        private void txtStatus_Validating(object sender, CancelEventArgs e)
        {

            //try
            //{
            //    if ((txtStatus.Text == "Y" && txtStatus.Text == "N") || txtStatus.Text == null)
            //    {
            //        e.Cancel = true;
            //    }
            //    else if (txtStatus.Text == "Y")
            //    {
                  
            //        //Without IP
            //        //CallReport();

            //        //With IP
            //        //CallReport();
                   
            //    }
            //    else
            //    {
            //        ClearForm(PnlPersonnel.Controls);
              
            //    }

                
            //    if (txtStatus.Text == "Y")
            //    {
            //        this.Save();
            //    }
             

            //}
            //catch (Exception exp)
            //{
            //    LogException(this.Name, "txtStatus_Validating", exp);
            //}



        }      
      
        private void txtPersonnelNO_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }


        }

        private void txtPersonnelNO_Leave(object sender, EventArgs e)
        {


            if (txtPersonnelNO.Text != "")
            {

                this.objVals.Clear();
                this.objVals.Add("PR_P_NO", txtPersonnelNO.Text);

                rslt = cmnDM.GetData("CHRIS_SP_ONEPAGERPERSONALENTRY_MANAGER", "DUMMY", objVals);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        dtDummy = rslt.dstResult.Tables[0];
                        if (dtDummy.Rows.Count > 0)
                        {

                            txtFirstName.Text = Convert.ToString(dtDummy.Rows[0]["PR_FIRST_NAME"]);
                            txtLastName.Text = Convert.ToString(dtDummy.Rows[0]["PR_LAST_NAME"]);
                            txtLevel.Text = Convert.ToString(dtDummy.Rows[0]["LVL"]);
                            txtCurrGrp.Text = Convert.ToString(dtDummy.Rows[0]["GRP"]);
                            txtCurrDept.Text = Convert.ToString(dtDummy.Rows[0]["DEPT"]);
                            txtDesignation.Text = Convert.ToString(dtDummy.Rows[0]["DESIG"]);
                            txtGEID.Text = Convert.ToString(dtDummy.Rows[0]["GEI_GEID_NO"]);
                            this.txtCurrBus.Text = Convert.ToString(dtDummy.Rows[0]["SEG"]);
                        }
                        else
                        {
                            txtLevel.Text = "";
                            txtCurrGrp.Text = "";
                            txtCurrDept.Text = "";
                            txtDesignation.Text = "";
                            txtGEID.Text = "";
                            this.txtCurrBus.Text = "";
                        }

                    }
                }

            }

            if (txtPersonnelNO.Text != "")
            {
                objVals.Clear();
                objVals.Add("PR_P_NO", txtPersonnelNO.Text);

                this.ClearForm(pnlCitiReference.Controls);
                this.ClearForm(pnlEducation.Controls);
                this.ClearForm(pnlEmpHistory.Controls);
                this.ClearForm(pnlTraining.Controls);
                
                
                rslt = cmnDM.GetData("CHRIS_SP_ONEPAGERPERSONALENTRY_MANAGER", "PP_VERIFY", objVals);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "0")
                    {
                        MessageBox.Show(txtPersonnelNO.Text + "    -- is Invalid Personnel No.");
                        this.ClearForm(PnlPersonnel.Controls);
                        this.ClearForm(pnlChildren.Controls);
                        this.ClearForm(pnlPersonal.Controls);
                       
                        
                        txtPersonnelNO.Text = "";
                        txtPersonnelNO.Focus();
                        return;

                    }
                }
            }

        }

        private void dgvChild_KeyPress(object sender, KeyPressEventArgs e)
        {
             //if (dgvChild.CurrentRow.Cells[2].EditedFormattedValue.ToString() !="")
             //   dgvChild.CurrentRow.Cells[2].EditedFormattedValue.ToString().ToUpper();

        }

        private void dgvChild_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.Value != null)
            {
                e.Value = e.Value.ToString().ToUpper();
                e.FormattingApplied = true;

            }

        }        

        private void txtReligion_Leave(object sender, EventArgs e)
        {
            this.slDatePicker2.Focus();
        }

        private void slTextBox11_Leave(object sender, EventArgs e)
        {
            this.slDatePicker1.Focus();
        }

        #endregion

        
        private void txtRName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
               
            }
            else
            {
                e.Handled = false;
                
            }
        }

        private void slDatePicker1_Leave(object sender, EventArgs e)
        {
            dgvChild.Enabled = true;
           
            dgvChild.Focus();
            dgvChild.CurrentCell = dgvChild.Rows[0].Cells[0];
            dgvChild.BeginEdit(true);
           
        }

        private void txtRepTo_Leave(object sender, EventArgs e)
        {
            if (txtPersonnelNO.Text != "")
            {
                if (txtRepTo.Text == "")
                {
                    txtRepTo.Focus();
                    return;
                }

                if (txtRepTo.Text != "")
                {
                 
                    if (txtPersonnelNO.Text == txtRepTo.Text)
                    {
                        MessageBox.Show("Personnel No. And Reporting No. Cannot be Same");
                        txtRepTo.Focus();
                        return;
                    }


                    objVals.Clear();
                    objVals.Add("PR_P_NO", txtRepTo.Text);
                    rslt = cmnDM.GetData("CHRIS_SP_ONEPAGERPERSONALENTRY_MANAGER", "CITIEMPREF", objVals);
                    if (rslt.isSuccessful)
                    {
                         if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString()  == "0")
                         {
                             MessageBox.Show(txtRepTo.Text + "  Not Exists ....");
                             txtRepTo.Focus();
                             return;
                         }

                    }


                    if (txtRName.Text == "")
                    {
                        txtRName.Text = "PR_FIRST_NAMEPR_LAST_NAME";

                    }



                }
            }


        }

        private void txtRepTo_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;

            }
            else
            {
                e.Handled = true;

            }


        }

        private void CHRIS_Personnel_OnePagerPersonalEntry_Shown(object sender, EventArgs e)
        {
            txtPersonnelNO.TabIndex = 0;
            txtPersonnelNO.Focus();
        }

        private void CHRIS_Personnel_OnePagerPersonalEntry_MdiChildActivate(object sender, EventArgs e)
        {
            txtPersonnelNO.TabIndex = 0;
            txtPersonnelNO.Focus();
        }


    }
}