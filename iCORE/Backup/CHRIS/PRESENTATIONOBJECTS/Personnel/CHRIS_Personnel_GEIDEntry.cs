using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;



using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_GEIDEntry : ChrisSimpleForm
    {
        #region Fields

        #endregion

        #region Constructors

        public CHRIS_Personnel_GEIDEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Personnel_GEIDEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            //this.ShowOptionTextBox = false;
            this.lblHeader.SendToBack();
            this.tbtList.Available = false;

            this.dtpDOB.Value = null;
            this.dtpGDate.Value = null;

            this.lblUserName.Text = "User Name:  "+this.UserName;

            this.btnAdd.Select();
            this.btnAdd.Focus();
            
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                if (this.IsValidate())
                {
                    base.DoToolbarActions(ctrlsCollection, actionType);
                    this.ResetForm();
                    return;
                }

                if (dtpGDate.Value == null && dtpDOB.Value == null)
                {
                    return;
                }
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
            switch (actionType)
            {
                case "Add":
                    this.FunctionConfig.CurrentOption = Function.Add;
                    this.lbtnPNo.ActionType = "LOV_PER";
                    break;
                case "Edit":
                    this.FunctionConfig.CurrentOption = Function.Modify;
                    this.lbtnPNo.ActionType = "LOV_GEID";
                    break;
                case "Cancel":
                    this.ResetForm();
                    break;
                case "List":
                    this.FunctionConfig.CurrentOption = Function.Modify;
                    this.lbtnPNo.ActionType = "LOV_GEID";
                    break;
                case "Delete":
                    //DoFunction(Keys.F1, Function.Delete);
                    this.FunctionConfig.CurrentOption = Function.None;
                    break;
            }
            
        }

        private void ResetForm()
        {
            this.txtPersNo.Text = "";
            this.txtFirstName.Text = "";
            this.txtLastName.Text = "";
            this.txtCountPayLoc.Text = "";
            this.txtGeidNo.Text = "";
            this.txtRecType.Text = "";

            this.dtpDOB.Value = null;
            this.dtpGDate.Value = null;

            this.btnSave.Enabled = false;

            this.FunctionConfig.CurrentOption = Function.None;

        }
        private bool IsValidate()
        {
            bool flag = true;

            if (this.txtPersNo.Text == "")
            {
                flag = false;
            }
            if (this.dtpDOB.Value == null)
            {
                flag = false;
            }
            if (this.dtpGDate.Value == null)
            {
                flag = false;
            }

            return flag;
        }

        
        #endregion

        #region Event Handlers

        private void btn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn == null)
            {
                return;
            }
            string caption = btn.Text;

            this.txtRecType.ReadOnly = false;
            this.txtPersNo2.ReadOnly = false;
            this.txtGeidNo.ReadOnly = false;
            this.txtCountPayLoc.ReadOnly = false;
            this.dtpGDate.CustomEnabled = false;
            
            switch(caption)
            {
                case"Add":
                    DoFunction(Function.Cancel);
                    this.dtpGDate.CustomEnabled = true;
                    DoFunction(Function.Add);
                    this.lbtnPNo.ActionType = "LOV_PER";
                    this.lbtnPNo.HiddenColumns = "GEI_PR_P_NO|GEI_FIRST_NAME|GEI_LAST_NAME|GEI_BIRTH_DATE";
                    //btnSave.Enabled = false;
                    //OnKeyDown(new KeyEventArgs(Keys.F1));
                    this.dtpGDate.CustomEnabled = true;
                    this.txtCountPayLoc.Text = "PK";
                    this.txtRecType.Text = "F";
                    this.dtpGDate.CustomEnabled = true;
                    this.dtpGDate.Enabled = true;
                    this.dtpGDate.Value = Now();

                    this.btnSave.Enabled = false;
                    this.tbtSave.Enabled = false;

                    break;
                case "Save":
                    this.dtpGDate.CustomEnabled = true;
                    //DoFunction(Function.Save);
                    Save(this.FunctionConfig.CurrentOption);
                    this.FunctionConfig.CurrentOption = Function.None;
                    break;
                case "Modify":
                    //btnSave.Enabled = false;
                    DoFunction(Function.Cancel);
                    this.dtpGDate.CustomEnabled = true;
                    DoFunction(Function.Modify);
                    this.lbtnPNo.ActionType = "LOV_GEID";
                    this.lbtnPNo.HiddenColumns = "GEI_COUNT_PAY_LOC|GEI_GEID_NO|GEI_PR_P_NO|GEI_FIRST_NAME|GEI_LAST_NAME|GEI_BIRTH_DATE|GEI_COUNT_PAY_LOC";
                    //if ((txtGeidNo.Text != string.Empty) || (txtCountPayLoc.Text != string.Empty))
                    //{ btnSave.Enabled = true; }

                    this.dtpGDate.CustomEnabled = true;
                    this.txtCountPayLoc.Text = "PK";
                    this.txtRecType.Text = "F";
                    this.dtpGDate.CustomEnabled = true;
                    this.dtpGDate.Enabled = true;
                    this.dtpGDate.Value = Now();

                    this.btnSave.Enabled = false;

                    break;
                case "Query":
                    DoFunction(Function.Cancel);
                    DoFunction(Function.Query);
                    this.lbtnPNo.ActionType = "LOV_GEID";
                    this.lbtnPNo.HiddenColumns = "GEI_COUNT_PAY_LOC|GEI_GEID_NO|GEI_PR_P_NO|GEI_FIRST_NAME|GEI_LAST_NAME|GEI_BIRTH_DATE|GEI_COUNT_PAY_LOC";
                    //if ((txtGeidNo.Text !=string.Empty) && (txtCountPayLoc.Text != string.Empty))
                    //{ MessageBox.Show("You cannot update this record"); }
                    this.dtpGDate.CustomEnabled = true;
                    this.txtCountPayLoc.Text = "PK";
                    this.txtRecType.Text = "F";
                    //this.dtpGDate.CustomEnabled = true;
                    //this.dtpGDate.Enabled = true;
                    this.dtpGDate.Value = Now();

                    //dtpGDate.Enabled = false;
                    this.txtRecType.ReadOnly = true;
                    this.txtPersNo2.ReadOnly = true;
                    this.txtGeidNo.ReadOnly = true;
                    this.txtCountPayLoc.ReadOnly = true;
                    this.dtpGDate.CustomEnabled = false;

                    break;
                case "Delete":
                    DoFunction(Function.Cancel);
                    this.dtpGDate.CustomEnabled = true;
                    //DoFunction(Keys.F1, Function.Delete);
                    this.FunctionConfig.CurrentOption = Function.Delete;
                    this.lbtnPNo.ActionType = "LOV_GEID";
                    this.lbtnPNo.HiddenColumns = "GEI_COUNT_PAY_LOC|GEI_GEID_NO|GEI_PR_P_NO|GEI_FIRST_NAME|GEI_LAST_NAME|GEI_BIRTH_DATE|GEI_COUNT_PAY_LOC";
                    break;
                case "Clear":
                    this.dtpGDate.CustomEnabled = true;
                    DoFunction(Function.Cancel);
                    this.FunctionConfig.CurrentOption = Function.None;
                    break;
                case "Exit":
                    this.dtpGDate.CustomEnabled = true;
                    //DoFunction(Function.Quit);
                    this.Close();
                    break;
                                    
            }
        }
        private void Save(Function f)
        {
            switch (f)
            {
                case Function.Add:
                    this.DoFunction(Function.Save);
                    this.DoFunction(Function.Cancel);
                    break;

                case Function.Modify:
                    this.DoFunction(Function.Save);
                    this.DoFunction(Function.Cancel);
                    break;

                case Function.Delete:
                    //this.DoFunction(Function.Delete);
                    this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));
           
                    break;

            }
        }
        private void lbtnPNo_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                //lbtnPNo.Enabled = false;
                //this.SetMessage("CHRIS","FIRST SELECT YOUR DESIRED OPTION.", "Note", iCORE.Common.PRESENTATIONOBJECTS.Cmn.MessageType.Null);
                //this.Message.ShowMessage();
                //lbtnPNo.Enabled = true;

                MessageBox.Show("FIRST SELECT YOUR DESIRED OPTION.", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCountPayLoc.Text = "PK";
                txtRecType.Text = "F";
                dtpGDate.Value = Now();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtOption.Visible = false;
            btnSave.Enabled = false;
            tbtAdd.Visible = false;
            tbtSave.Visible = false;
            tbtDelete.Visible = false;

        }
        private void txtPersNo_TextChanged(object sender, EventArgs e)
        {
            if (txtPersNo.Text != "")
            {
                this.btnSave.Enabled = true;
            }
            else
            {
                this.btnSave.Enabled = false;
            }
        }

        #endregion

        private void CHRIS_Personnel_GEIDEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (this.FunctionConfig.CurrentOption == Function.Query)
            {
                MessageBox.Show("You cannot update this record"); 
                this.btnSave.Enabled = false;
            }
            if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                this.btnSave.Enabled = true;
                this.tbtSave.Enabled = true;
            }
            
            if (this.FunctionConfig.CurrentOption == Function.Modify)
            {
                this.txtRecType.Text = "F";
                this.dtpGDate.Value = Now();
            }
            if (actionType == "")
            {

            }
        }

       
    
    }
}