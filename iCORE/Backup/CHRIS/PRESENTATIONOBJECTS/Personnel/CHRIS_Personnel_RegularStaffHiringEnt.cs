using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using CrplControlLibrary;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_RegularStaffHiringEnt : ChrisMasterDetailForm
    {
        #region --Global Variable--
        string tem_W_ADD_ANS = string.Empty;
        bool valSaved = false;
        int g_Pno;
        public string strCurrentPopUP = string.Empty;
        public DateTime g_w_Date_To;
        public string g_up;
        public int varPR_P_NO;
        string lblViewPopUP = "[N]ext P. No.     Next [P]age         [O]ptions";
        string lblDeletePopUP = "Do You Want To Delete The Above Information [Y/N]";
        CmnDataManager cmnDM = new CmnDataManager();
        TextBox txt = new TextBox();
        public string option = string.Empty;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frm_Page2;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT frm_Dpt;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP frm_Emp;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU eduFrm;
        public DataTable BlkDept = null;
        public DataTable BlkEMP = null;
        public DataTable BlkChild = null;
        public DataTable BlkEdu = null;
        public DataTable BlkRef = null;
        string NewPr_P_No;
        string g_Location;
        string g_IP;
        double g_cl;
        double g_ml;
        double g_sl;
        double g_slh;
        double g_pl;
        double g_w_cl_month;
        double g_w_cl_day;
        double g_w_ml_month;
        double g_w_ml_day;
        double g_w_sl_month;
        double g_w_slh_month;
        double g_w_sl_day;
        double g_w_slh_day;
        double g_w_pl_month;
        double g_w_pl_day;
        double g_w_months;
        double g_w_days;
        string g_w_year;
        string g_dummy;
        string AUDIT_PATH = "G:\\SPOOL\\";
        string PATH_REPORT = "G:\\";
        public string NewTxtOption = string.Empty;
        string strDesg = string.Empty;
        string strLevel = string.Empty;


        #endregion

        # region --Constructor--
        public CHRIS_Personnel_RegularStaffHiringEnt()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_RegularStaffHiringEnt(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(this.pnlBlkDept);
            lstDependentPanels.Add(this.pnlTblBlkEmp);
            lstDependentPanels.Add(this.pnlTblBlkRef);
            lstDependentPanels.Add(this.pnlSplBlkPersMain);
            //lstDependentPanels.Add(this.pnlSplBlkPersConc);
            lstDependentPanels.Add(this.pnlTblBLKChild);
            lstDependentPanels.Add(this.pnlTblBlkEdu);

            this.pnlPersonnelMain.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlPersonnelMain);

        }
        #endregion

        #region --Method--
        /// <summary>
        /// 
        /// </summary>
        private void ProcViewAdd()
        {
            if (txt_PR_RELIGION.Text == "MUSLIM")
                txt_W_RELIGION.Text = "M";
            else if (txt_PR_RELIGION.Text == "NON-MUSLIM")
                txt_W_RELIGION.Text = "N";

            //if (txtOption.Text == "A" && txt_PR_P_NO.Text == string.Empty)
            //{
            //    ClearForm(pnlPersonnelMain.Controls);
            //    txtOption.Select();
            //    txtOption.Focus();
            //}
            //else if (txtOption.Text == "V")
            //{
            //    MessageBox.Show("No Information Found On The Given Personal No.", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        /// <summary>
        /// IF RESULT SET IS EMPTY THAN SET THE txt_PR_NEW_ANNUAL_PACK to  txt_PR_ANNUAL_PACK
        /// </summary>
        private void Proc_Inc()
        {
            string dummy = string.Empty;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txt_PR_P_NO.Text);

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "PROC_INC", param);

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                dummy = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

            if (dummy == string.Empty)
            {
                txt_PR_NEW_ANNUAL_PACK.Text = txt_PR_ANNUAL_PACK.Text;
            }
        }

        /// <summary>
        /// Verify the Account Number
        /// </summary>
        /// <returns></returns>
        private bool Account_Check()
        {
            try
            {
                int one, two, three, four, five, six, seven, eight, nine, ten;
                int x, v;
                string acc = txt_PR_ACCOUNT_NO.Text;
                string AccNo;
                string newAccNo;
                one = Convert.ToInt32(acc.Substring(0, 1));
                two = Convert.ToInt32(acc.Substring(2, 1));
                three = Convert.ToInt32(acc.Substring(3, 1));
                four = Convert.ToInt32(acc.Substring(4, 1));
                five = Convert.ToInt32(acc.Substring(5, 1));
                six = Convert.ToInt32(acc.Substring(6, 1));
                seven = Convert.ToInt32(acc.Substring(7, 1));
                eight = Convert.ToInt32(acc.Substring(9, 1));
                nine = Convert.ToInt32(acc.Substring(10, 1));
                ten = Convert.ToInt32(acc.Substring(11, 1));

                x = (nine + three) * 2;
                x = x + ((eight + two) * 3);
                x = x + ((seven + one) * 4);
                x = x + (six * 5);
                x = x + (five * 6);
                x = x + (four * 7);
                //v = trunc( (x/11), 0);
                v = (x / 11);
                x = 11 - (x - (11 * v));


                if (x > 9 && one == 9)
                    x = x - 3;

                if (ten != x)
                {
                    MessageBox.Show("Invalid Account Number ....  Retry.", "Forms", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                else
                {
                    //acc.Insert(1, "6");

                    //AccNo = acc.Insert(1, "-");
                    //newAccNo = AccNo.Insert(8, "-");
                    //txt_PR_ACCOUNT_NO.Text = newAccNo;
                }
                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
        }


        /// <summary>
        /// Ctrl+Down PopUP Open
        /// </summary>
        private void CtrlDownPopUPOpen()
        {
            if (strCurrentPopUP == "Page2")
            {
                frm_Page2.Close();
                frm_Dpt = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, null , dgvBlkDept.GridSource, dgvBlkEmp.GridSource, dgvBlkRef.GridSource, dgvBlkChild.GridSource, dgvBlkEdu.GridSource);
                //frm_Dpt = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this);
                frm_Dpt.MdiParent = null;
                frm_Dpt.ShowDialog();
                strCurrentPopUP = "BLKDEPT";
            }
            else if (strCurrentPopUP == "BLKDEPT")
            {
                //frm_Dpt.Close();
                //frm_Emp = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, dgvBlkEmp.GridSource);
                //frm_Emp.MdiParent = null;
                //frm_Emp.ShowDialog();
                //strCurrentPopUP = "BLKEMP";
            }
            else if (strCurrentPopUP == "BLKEMP")
            {
            }
            else if (strCurrentPopUP == "BLKEMP")
            {
            }
            else if (strCurrentPopUP == "BLKPERS")
            {
            }
            else if (strCurrentPopUP == "BLKREF")
            {
            }
        }


        /// <summary>
        /// Ctrl+Up PopUP Open
        /// </summary>
        private void CtrlUpPopUPOpen()
        {
            if (strCurrentPopUP == "Page2")
            {
                //frm_Page2.Close();
                //frm_Dpt = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, dgvDept.GridSource);
                //frm_Dpt.MdiParent = null;
                //frm_Dpt.ShowDialog();
                //strCurrentPopUP = "BLKDEPT";
            }
            else if (strCurrentPopUP == "BLKDEPT")
            {
                //frm_Page2                   = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2();
                //frm_Page2.TextBox.MaxLength = 1;
                //frm_Page2.TextBox.Leave     += new EventHandler(TextBox_Leave);
                //frm_Page2.ShowDialog();
                //strCurrentPopUP             = "Page2";
            }
            else if (strCurrentPopUP == "BLKEDU")
            {
            }
            else if (strCurrentPopUP == "BLKEMP")
            {
            }
            else if (strCurrentPopUP == "BLKPERS")
            {
            }
            else if (strCurrentPopUP == "BLKREF")
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Pro_Rate_Leaves()
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", txt_PR_P_NO.Text);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                //rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "ADDMONTH", param);
                rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "Dummy", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count == 0)
                {
                    //g_dummy                 = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    g_w_year = Convert.ToString(DateTime.Now.Year - 1);


                    DateTime joiningDate = Convert.ToDateTime(txt_PR_JOINING_DATE.Value.ToString());

                    if (txt_PR_CATEGORY.Text == "C" && Convert.ToInt32(joiningDate.Year) == Convert.ToInt32(g_w_year.ToString()))
                    {
                        DateTime date = Convert.ToDateTime("12/31/" + g_w_year);
                        g_w_months = MonthsBetweenInOracle(date, joiningDate);
                        DateTime dtmonthadded = joiningDate.AddMonths(1);
                        DateTime LastDayOfMonth = new DateTime(dtmonthadded.Year, dtmonthadded.Month, 1).AddDays(-1);
                        TimeSpan ts = LastDayOfMonth - joiningDate;
                        g_w_days = ts.Days;


                        g_w_cl_month = 12;
                        g_w_sl_month = 30;
                        g_w_slh_month = 30;
                        g_w_ml_month = 60;
                        g_w_pl_day = (30 / 12 / 30) * g_w_days;
                        g_w_pl_month = (30 / 12) * g_w_months;

                    }
                    else if (joiningDate.Year == Now().Year)
                    {
                        DateTime date = Convert.ToDateTime("31/12/" + Now().Year);
                        g_w_months = MonthsBetweenInOracle(date, joiningDate);
                        DateTime dtmonthadded = joiningDate.AddMonths(1);
                        DateTime LastDayOfMonth = new DateTime(dtmonthadded.Year, dtmonthadded.Month, 1).AddDays(-1);
                        TimeSpan ts = LastDayOfMonth - joiningDate;
                        g_w_days = ts.Days;



                        g_w_cl_day = (12.0 / 12.0 / 30.0) * g_w_days;
                        g_w_cl_month = (12.0 / 12.0) * g_w_months;
                        g_w_sl_day = (30.0 / 12.0 / 30.0) * g_w_days;
                        g_w_sl_month = (30.0 / 12.0) * g_w_months;
                        g_w_slh_day = (30.0 / 12.0 / 30.0) * g_w_days;
                        g_w_slh_month = (30.0 / 12.0) * g_w_months;
                        g_w_ml_day = (60.0 / 12.0 / 30.0) * g_w_days;
                        g_w_ml_month = (60.0 / 12.0) * g_w_months;
                        g_w_pl_day = (30.0 / 12.0 / 30.0) * g_w_days;
                        g_w_pl_month = (30.0 / 12.0) * g_w_months;

                    }


                    g_cl = Math.Round(((g_w_cl_month) + (g_w_cl_day)));
                    g_sl = Math.Round(((g_w_sl_month) + (g_w_sl_day)));
                    g_slh = Math.Round(((g_w_slh_month) + (g_w_slh_day)));
                    g_ml = Math.Round(((g_w_ml_month) + (g_w_ml_day)));
                    g_pl = Math.Round(((g_w_pl_month) + (g_w_pl_day)));


                    if (txt_PR_SEX.Text == "M")
                    {
                        g_ml = 0;
                    }

                    Dictionary<string, object> param1 = new Dictionary<string, object>();
                    param1.Add("PR_P_NO", txt_PR_P_NO.Text);
                    param1.Add("CL", g_cl);
                    param1.Add("ML", g_ml);
                    param1.Add("SL", g_sl);
                    param1.Add("PL", g_pl);
                    param1.Add("SLH", g_slh);


                    Result rsltCode1;
                    CmnDataManager cmnDM1 = new CmnDataManager();
                    rsltCode1 = cmnDM1.Execute("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "Insert_Lev_Status", param1);
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "Pro_Rate_Leaves", exp);
            }
        }

        /// <summary>
        /// Report before Commiting Record
        /// </summary>
        private void CallReportOLD()
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();

            string FN1 = "AUPR" + (DateTime.Now.ToString("yyyyMMddhhmiss")) + ".LIS";

            CrplControlLibrary.SLTextBox txtDESTYPE = new CrplControlLibrary.SLTextBox(comp);
            txtDESTYPE.Name = "txtDESTYPE";
            txtDESTYPE.Text = "FILE";
            pnl.Controls.Add(txtDESTYPE);

            CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            txtDESNAME.Name = "txtDESNAME";
            txtDESNAME.Text = AUDIT_PATH + FN1;
            pnl.Controls.Add(txtDESNAME);

            CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            txtMODE.Name = "txtMODE";
            txtMODE.Text = "CHARACTER";
            pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "PNO";
            txtPNO.Text = txt_PR_P_NO.Text;
            //txtPNO.Text = g_Pno.ToString();
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "USER";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "ST";
            txtST.Text = "OLD";
            pnl.Controls.Add(txtST);

            CrplControlLibrary.SLTextBox txtIP = new CrplControlLibrary.SLTextBox(comp);
            txtIP.Name = "IP";
            txtIP.Text = "192.168.1.1";
            pnl.Controls.Add(txtIP);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "txtDT";
            txtDT.Text = Now().ToString("dd/MM/yyyy");
            pnl.Controls.Add(txtDT);

            frm.Controls.Add(pnl);
            frm.RptFileName = "audit08A";
            frm.Owner = this;

            frm.ExportCustomReportToTXT(frm.m_ReportPath + FN1, "txt");
            //frm.RunReport();
        }

        /// <summary>
        /// Report After Commiting Record
        /// </summary>
        private void CallReportNEW()
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();

            string FN1 = "AUPO" + (DateTime.Now.ToString("yyyyMMddhhmiss")) + ".LIS";

            CrplControlLibrary.SLTextBox txtDESTYPE = new CrplControlLibrary.SLTextBox(comp);
            txtDESTYPE.Name = "txtDESTYPE";
            txtDESTYPE.Text = "FILE";
            pnl.Controls.Add(txtDESTYPE);

            CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            txtDESNAME.Name = "txtDESNAME";
            txtDESNAME.Text = AUDIT_PATH + FN1;
            pnl.Controls.Add(txtDESNAME);

            CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            txtMODE.Name = "txtMODE";
            txtMODE.Text = "CHARACTER";
            pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "PNO";
            txtPNO.Text = txt_PR_P_NO.Text;
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "USER";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "ST";
            txtST.Text = "NEW";
            pnl.Controls.Add(txtST);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "txtDT";
            txtDT.Text = Now().ToString("dd/MM/yyyy");
            pnl.Controls.Add(txtDT);

            CrplControlLibrary.SLTextBox txtIP = new CrplControlLibrary.SLTextBox(comp);
            txtIP.Name = "IP";
            txtIP.Text = g_IP;
            pnl.Controls.Add(txtDT);

            frm.Controls.Add(pnl);
            frm.RptFileName = "audit08A";
            frm.Owner = this;
            frm.ExportCustomReportToTXT(frm.m_ReportPath + FN1, "txt");
            //frm.RunReport();
        }

        /// <summary>
        /// Open Report and Call the From Save from Education Panel
        /// </summary>
        /// <param name="W_ADD_ANS"></param>
        public void CallSave(string W_ADD_ANS)
        {
            try
            {
                g_w_Date_To = DateTime.MinValue;
                g_Pno = int.MinValue;
                tem_W_ADD_ANS = W_ADD_ANS;

                if (W_ADD_ANS == "Y")
                {
                    Pro_Rate_Leaves();

                    //Call OLD Report
                    CallReportOLD();

                    //Save Records 
                    this.Save();

                    //Call NEW Report
                     CallReportNEW();

                    if (txtOption.Text == "M")
                    {
                        //ClearForm(pnlPersonnelMain.Controls);
                        //FormClear();
                        txt_W_USER.Text     = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                        txtLocation.Text    = g_Location;
                        base.Cancel();
                        valSaved            = true;
                        txtOption.Text      = "M";
                        txt_PR_P_NO.Select();
                        txt_PR_P_NO.Focus();
                    }
                    else if (txtOption.Text == "A")
                    {
                        base.Cancel();
                        this.Add();
                        txt_W_USER.Text         = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                        txtLocation.Text        = g_Location;
                        //txt_PR_P_NO.CustomEnabled = false;
                        //txtOption.Text          = "A";
                        //txt_W_OPTION_DIS.Text   = "ADD";
                        //txt_PR_EMP_TYPE.Text    = "FT";
                        //txt_PR_BANK_ID.Text     = "Y";
                        //txt_PR_TAX_INC.Text     = "0";
                        //txt_PR_TAX_PAID.Text    = "0";
                        //txt_PR_BRANCH.Select();
                        //txt_PR_BRANCH.Focus();
                        valSaved = true;
                    }

                    base.IterateFormToEnableControls(pnlPersonnelMain.Controls, true);
                    //txt_PR_EMP_TYPE.Text    = "FT";
                    //txt_PR_BANK_ID.Text     = "Y";
                    //txt_PR_TAX_INC.Text     = "0";
                    //txt_PR_TAX_PAID.Text    = "0";
                }
                else
                {
                    base.Cancel();
                }

            }
            catch (Exception exp)
            {
                LogException(this.Name, "CallSave", exp);
            }
        }

        /// <summary>
        /// Open Report and Call the From Save from Education Panel
        /// </summary>
        /// <param name="W_DEL_ANS"></param>
        public void CallDelete(string W_DEL_ANS)
        {
            try
            {
                if (W_DEL_ANS == "Y")
                {
                    txt_PR_CLOSE_FLAG.Text = "C";
                    g_Pno = Convert.ToInt32(txt_PR_P_NO.Text);
                    CallReportNEW();
                    txtOption.Text = "D";
                    this.Delete();
                    base.m_intPKID = Convert.ToInt32(txtID.Text);
                    base.PerformAction(pnlPersonnelMain, "Delete");
                    //this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CallDelete", exp);
            }
        }

        public void CallView(string W_VIEW_ANS)
        {
            try
            {
                if (W_VIEW_ANS == "N")
                {
                    FormClear();
                    base.Cancel();
                    txtDate.Text = Now().ToString("dd/MM/yyyy");
                    txtOption.Text = "V";
                    txt_W_OPTION_DIS.Text = "VIEW";
                    txt_PR_P_NO.Select();
                    txt_PR_P_NO.Focus();

                    txt_PR_EMP_TYPE.Text = "FT";
                    txt_PR_BANK_ID.Text = "Y";
                    txt_PR_TAX_INC.Text = "0";
                    txt_PR_TAX_PAID.Text = "0";
                }
                else if (W_VIEW_ANS == "P")
                {
                    frm_Dpt = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, null, dgvBlkDept.GridSource, dgvBlkEmp.GridSource, dgvBlkRef.GridSource, dgvBlkChild.GridSource, dgvBlkEdu.GridSource);
                    frm_Dpt.MdiParent = null;
                    frm_Page2.Hide();
                    frm_Dpt.Enabled = true;
                    frm_Dpt.ShowDialog();
                    strCurrentPopUP = "BLKDEPT";
                }
                else if (W_VIEW_ANS == "O")
                {
                    FormClear();
                    base.Cancel();
                    txtOption.Select();
                    txtOption.Focus();
                }

            }
            catch (Exception exp)
            {
                LogError(this.Name, "CallView", exp);
            }
        }

        public void FormClear()
        {
            ClearForm(pnlPersonnelMain.Controls);
            txt_PR_JOINING_DATE.Value = null;
            txt_PR_CONFIRM.Value = null;
            txt_PR_ID_ISSUE.Value = null;
            txt_PR_EXPIRY.Value = null;


            //ClearForm(pnlSplBlkPersConc.Controls);
            ClearForm(pnlSplBlkPersMain.Controls);
            ClearForm(pnlTblBlkEdu.Controls);
            ClearForm(pnlTblBlkEmp.Controls);
            ClearForm(pnlTblBlkRef.Controls);
            ClearForm(pnlHead.Controls);
            txtDate.Text = null;
            txt_W_OPTION_DIS.Text = null;
            txtLocation.Text = null;
            txtOption.Text = null;
        }

        /// <summary>
        /// Fill the Form with the current PR_P_NO
        /// </summary>
        /// <param name="ds"></param>
        private void FillFrom(DataSet ds)
        {
            txt_PR_BRANCH.Text = ds.Tables[0].Rows[0]["PR_BRANCH"].ToString();
            txt_PR_FIRST_NAME.Text = ds.Tables[0].Rows[0]["FIRST NAME"].ToString();
            txt_PR_LAST_NAME.Text = ds.Tables[0].Rows[0]["LAST NAME"].ToString();
            txt_PR_DESIG.Text = ds.Tables[0].Rows[0]["PR_DESIG"].ToString();
            txt_PR_LEVEL.Text = ds.Tables[0].Rows[0]["PR_LEVEL"].ToString();
            txt_PR_FUNC_Title1.Text = ds.Tables[0].Rows[0]["PR_FUNC_TITTLE1"].ToString();
            txt_PR_FUNC_Title2.Text = ds.Tables[0].Rows[0]["PR_FUNC_TITTLE2"].ToString();
            txt_PR_EMP_TYPE.Text = ds.Tables[0].Rows[0]["PR_EMP_TYPE"].ToString();
            txt_W_RELIGION.Text = ds.Tables[0].Rows[0]["PR_RELIGION"].ToString();
            txt_PR_ANNUAL_PACK.Text = ds.Tables[0].Rows[0]["PR_ANNUAL_PACK"].ToString();
            txt_PR_NATIONAL_TAX.Text = ds.Tables[0].Rows[0]["PR_NATIONAL_TAX"].ToString();
            txt_PR_ACCOUNT_NO.Text = ds.Tables[0].Rows[0]["PR_ACCOUNT_NO"].ToString();
            txt_PR_BANK_ID.Text = ds.Tables[0].Rows[0]["PR_BANK_ID"].ToString();
            txt_PR_TAX_INC.Text = ds.Tables[0].Rows[0]["PR_TAX_INC"].ToString();
            txt_PR_TAX_PAID.Text = ds.Tables[0].Rows[0]["PR_TAX_PAID"].ToString();
            txtID.Text = ds.Tables[0].Rows[0]["ID"].ToString();

            if (ds.Tables[0].Rows[0]["PR_JOINING_DATE"].ToString() != string.Empty)
            {
                txt_PR_JOINING_DATE.Value = DateTime.Parse(ds.Tables[0].Rows[0]["PR_JOINING_DATE"].ToString());
            }
            else
            {
                txt_PR_JOINING_DATE.Value = DateTime.Parse(ds.Tables[0].Rows[0]["PR_JOINING_DATE"].ToString());
            }

            if (ds.Tables[0].Rows[0]["PR_CONFIRM"].ToString() != string.Empty)
            {
                txt_PR_CONFIRM.Value = DateTime.Parse(ds.Tables[0].Rows[0]["PR_CONFIRM"].ToString());
            }
            else
            {
                txt_PR_CONFIRM.Value = DateTime.Parse(ds.Tables[0].Rows[0]["PR_CONFIRM"].ToString());
            }
            if (ds.Tables[0].Rows[0]["PR_ID_ISSUE"].ToString() != string.Empty)
            {
                txt_PR_ID_ISSUE.Value = DateTime.Parse(ds.Tables[0].Rows[0]["PR_ID_ISSUE"].ToString());
            }
            else
            {
                txt_PR_ID_ISSUE.Value = null;
            }
            if (ds.Tables[0].Rows[0]["PR_EXPIRY"].ToString() != string.Empty)
            {
                txt_PR_EXPIRY.Value = DateTime.Parse(ds.Tables[0].Rows[0]["PR_EXPIRY"].ToString());
            }
            else
            {
                txt_PR_EXPIRY.Value = null;
            }
        }

        private bool FromValidate()
        {
            if (txt_PR_FIRST_NAME.Text == string.Empty)
            {
                txt_PR_FIRST_NAME.Select();
                txt_PR_FIRST_NAME.Focus();

                return false;
            }
            if (txt_PR_DESIG.Text == string.Empty)
            {
                txt_PR_DESIG.Select();
                txt_PR_DESIG.Focus();
                return false;
            }
            if (txt_PR_EMP_TYPE.Text == string.Empty)
            {
                txt_PR_EMP_TYPE.Select();
                txt_PR_EMP_TYPE.Focus();
                return false;
            }
            if (txt_W_RELIGION.Text == string.Empty)
            {
                txt_W_RELIGION.Select();
                txt_W_RELIGION.Focus();
                return false;
            }
            if (txt_PR_JOINING_DATE.Value == null)
            {
                txt_PR_JOINING_DATE.Select();
                txt_PR_JOINING_DATE.Focus();
                return false;
            }
            if (txt_PR_CONFIRM.Value == null)
            {
                txt_PR_CONFIRM.Select();
                txt_PR_CONFIRM.Focus();
                return false;
            }
            if (txt_PR_BANK_ID.Text == string.Empty)
            {
                txt_PR_BANK_ID.Select();
                txt_PR_BANK_ID.Focus();
                return false;
            }

            return true;
        }

        #endregion

        # region --Events--

        protected override bool Add()
        {
            base.Add();
            txtOption.Text = "A";
            
            Dictionary<string, object> param = new Dictionary<string, object>();
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "SERIALNO", param);

            if (rsltCode.isSuccessful)
                txt_PR_P_NO.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

            txt_PR_P_NO.AssociatedLookUpName = "as";
            lbtnPersonnelNO.Visible = false;
            txt_PR_BRANCH.Select();
            txt_PR_BRANCH.Focus();
            txt_W_OPTION_DIS.Text = "ADD";
            valSaved = false;


            txt_W_OPTION_DIS.Text = "ADD";
            txt_PR_EMP_TYPE.Text = "FT";
            txt_PR_BANK_ID.Text = "Y";
            txt_PR_TAX_INC.Text = "0";
            txt_PR_TAX_PAID.Text = "0";



            txt_PR_EMP_TYPE.Text = "FT";
            txt_PR_BANK_ID.Text = "Y";
            txt_PR_TAX_INC.Text = "0";
            txt_PR_TAX_PAID.Text = "0";

            return false;
        }


        /// <summary>
        /// Override Toolbar to Skip VAlidation on Personnel No. LOV
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                lbtnPersonnelNO.SkipValidationOnLeave = true;
            }
            else if (actionType == "Close")
            {
                FormClear();
            }
            else if (actionType == "Cancel")
            {
                txt_PR_P_NO.Text = string.Empty;
                txt_PR_BRANCH.Text = string.Empty;
                txtReportTo.Text = string.Empty;
                txt_PR_DESIG.Text = string.Empty;
                txt_PR_LEVEL.Text = string.Empty;
                txt_PR_JOINING_DATE.Value = null;
                //txt_PR_CONFIRM.Value = null;
                //txt_PR_ID_ISSUE.Value = null;
                //txt_PR_EXPIRY.Value = null;
                base.DoToolbarActions(ctrlsCollection, actionType);
                this.pnlPersonnelMain.LoadDependentPanels();
                return;

            }

            base.DoToolbarActions(ctrlsCollection, actionType);

            lbtnPersonnelNO.SkipValidationOnLeave = false;
        }

        /// <summary>
        /// Shows the User Name and Time on FormLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                txtDate.Enabled             = true;
                txt_W_OPTION_DIS.Enabled    = true;
                txt_W_USER.Enabled          = true;
                txtLocation.Enabled         = true;

                tbtAdd.Visible      = false;
                tbtDelete.Visible   = false;
                tbtSave.Visible     = false;
                tbtList.Visible     = false;
                tbtEdit.Visible     = false;
                pnlSave.Visible     = false;
                pnlDelete.Visible   = false;
                this.FunctionConfig.EnableF8 = false;
                this.FunctionConfig.EnableF5 = false;
                this.FunctionConfig.EnableF2 = false;
                lblUserName.Text = UserName.ToString();
                this.KeyPreview = true;
                this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);

                //DataTable dt = (DataTable)(dgvBlkDept.DataSource);
                //dt.TableNewRow += new DataTableNewRowEventHandler(dt_TableNewRow);

            }
            catch (Exception exp)
            {
                LogError(this.Name, "OnLoad", exp);
            }
        }

        /// <summary>
        /// On Ctrl+Down/Ctrl+UP open new POPUPs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                //if (FromValidate())
                {
                    if (e.Modifiers == Keys.Control)
                    {
                        switch (e.KeyValue)
                        {
                            case 34:
                                if (FromValidate())
                                {
                                    frm_Dpt             = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, null, dgvBlkDept.GridSource, dgvBlkEmp.GridSource, dgvBlkRef.GridSource, dgvBlkChild.GridSource, dgvBlkEdu.GridSource);
                                    frm_Dpt.MdiParent   = null;
                                    frm_Dpt.Enabled     = true;
                                    frm_Dpt.ShowDialog();

                                    this.KeyPreview     = true;
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "ShortCutKey_Press", exp);
            }
        }

        /// <summary>
        /// txt_PR_P_NO_Leave open PopUP if Mode is VIEW else move next Textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_P_NO_Leave(object sender, EventArgs e)
        {
            if (option == "O")
            {
                txtOption.Select();
                txtOption.Focus();
                option = string.Empty;
            }
            else if (option == "N")
            {
                base.IterateFormToEnableControls(pnlPersonnelMain.Controls, true);
                txt_PR_P_NO.Select();
                txt_PR_P_NO.Focus();
                option = string.Empty;
                txt_PR_P_NO.Select();
                txt_PR_P_NO.Focus();


            }
        }

        /// <summary>
        /// Validate the PopUP Text Box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TextBox_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (txtOption.Text == "V")
            {
                if (txt.Text != null && txt.Text != "N" && txt.Text != "P" && txt.Text != "O")
                {
                    e.Cancel = true;
                }

            }
        }

        //<summary>
        //POPUP DepartmentGrid on Writing P in Textbox. In View Mode Only
        //</summary>
        //<param name="sender"></param>
        //<param name="e"></param>
        void TextBox_Validated(object sender, EventArgs e)
        {
            try
            {
                TextBox txt = (TextBox)sender;
                frm_Page2.Hide();
                //if (txtOption.Text == "V")
                //{
                //    if (txt.Text == "N")
                //    {
                //        ClearForm(pnlPersonnelMain.Controls);
                //        FormClear();
                //        base.Cancel();
                //        txtOption.Text = "V";
                //        txt_PR_FUNC_Title1.Text = "testing";

                //        frm_Page2.Hide();

                //        base.IterateFormToEnableControls(pnlPersonnelMain.Controls, true);
                //        txt_PR_EMP_TYPE.Text = "FT";
                //        txt_PR_BANK_ID.Text = "Y";
                //        txt_PR_TAX_INC.Text = "0";
                //        txt_PR_TAX_PAID.Text = "0";

                //        txt_PR_P_NO.Select();
                //        txt_PR_P_NO.Focus();

                //    }
                //    else if (txt.Text == "P")
                //    {
                //        frm_Dpt             = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvBlkDept.GridSource, dgvBlkEmp.GridSource, dgvBlkRef.GridSource, dgvBlkChild.GridSource, dgvBlkEdu.GridSource);
                //        frm_Dpt.MdiParent   = null;
                //        frm_Page2.Hide();
                //        frm_Dpt.Enabled     = true;
                //        frm_Dpt.ShowDialog();
                //        strCurrentPopUP     = "BLKDEPT";
                //    }
                //    else if (txt.Text == "O")
                //    {
                //        ClearForm(pnlPersonnelMain.Controls);
                //        FormClear();
                //        base.Cancel();
                //        txtOption.Select();
                //        txtOption.Focus();
                //        frm_Page2.Hide();
                //    }
                //}
            }
            catch (Exception exp)
            {
                LogError(this.Name, "TextBox_Validated", exp);
            }
        }

        /// <summary>
        /// txt_PR_P_NO VAlidation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_P_NO_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txt_PR_P_NO.Text == string.Empty || txt_PR_P_NO.Text == null || txt_PR_P_NO.Text == "")
                {
                    if ((txtOption.Text == "V" || txtOption.Text == "M") && valSaved == false)
                    {
                        MessageBox.Show("You Can`t Leave the Field Empty", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        
                        return;
                    }
                    ClearForm(pnlPersonnelMain.Controls);
                    this.txt_W_USER.Text = this.userID;
                    this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
                    base.IterateFormToEnableControls(pnlPersonnelMain.Controls, false);
                    txtOption.Select();
                    txtOption.Focus();
                    return;
                }
                else if (txtOption.Text == "V" || txtOption.Text == "A")
                {
                    ProcViewAdd();

                    if (txt_PR_CLOSE_FLAG.Text == "C".ToUpper() && txtOption.Text == "V")
                    {
                        MessageBox.Show("This Is A ******** Deleted Record ********", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                if (txtOption.Text == "A" && (txt_PR_BRANCH.Text == string.Empty || txt_PR_BRANCH.Text == null))
                {
                    //MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //ClearForm(pnlPersonnelMain.Controls);
                    //txtOption.Select();
                    //txtOption.Focus();
                }
                else if ((txtOption.Text == "M" || txtOption.Text == "D") && (txt_PR_FIRST_NAME.Text == string.Empty))
                {
                    ClearForm(pnlPersonnelMain.Controls);
                    txtOption.Select();
                    txtOption.Focus();
                }
                else if (txtOption.Text == "M" || (txt_PR_P_NO.Text == string.Empty || txt_PR_P_NO.Text == null))
                {
                    //txt_PR_EMP_TYPE.Select();
                    //txt_PR_EMP_TYPE.Focus();
                }
                else if (txtOption.Text == "V")
                {
                    txt_W_ADD_ANS.Select();
                    txt_W_ADD_ANS.Focus();
                }
                else if (txtOption.Text == "D")
                {
                    txt_W_DEL_ANS.Select();
                    txt_W_DEL_ANS.Focus();
                }
                //}
                //else
                {
                    if (txtOption.Text == "V")
                    {
                        BlkDept = dgvBlkDept.GridSource;
                        BlkEMP = dgvBlkEmp.GridSource;
                        BlkChild = dgvBlkChild.GridSource;
                        BlkEdu = dgvBlkEdu.GridSource;
                        BlkRef = dgvBlkRef.GridSource;

                        varPR_P_NO = Convert.ToInt32(txt_PR_P_NO.Text);

                        frm_Page2 = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2(lblViewPopUP);
                        frm_Page2.TextBox.MaxLength = 1;
                        frm_Page2.TextBox.Validating += new CancelEventHandler(TextBox_Validating);
                        frm_Page2.TextBox.Validated += new EventHandler(TextBox_Validated);
                        frm_Page2.MdiParent = null;
                        frm_Page2.ShowDialog();
                        strCurrentPopUP = "Page2";
                        txt.Text = frm_Page2.Value;
                        frm_Page2.Close();
                        if (txt.Text == "O")
                        {
                            ClearForm(pnlPersonnelMain.Controls);
                            FormClear();
                            base.Cancel();
                            txtOption.Select();
                            txtOption.Focus();
                            txtOption.Select();
                            txtOption.Focus();
                        }
                        else if (txt.Text == "P")
                        {
                            NewTxtOption = string.Empty;
                            frm_Dpt = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, null, dgvBlkDept.GridSource, dgvBlkEmp.GridSource, dgvBlkRef.GridSource, dgvBlkChild.GridSource, dgvBlkEdu.GridSource);
                            frm_Dpt.MdiParent = null;
                            //frm_Page2.Hide();
                            frm_Dpt.Enabled = true;
                            frm_Dpt.ShowDialog();
                            //NewTxtOption = this.NewTxtOption;
                            strCurrentPopUP = "BLKDEPT";
                            {
                                if (NewTxtOption == "N")//(tempControl.Name== txt_PR_P_NO.Name)
                                    base.IterateFormToEnableControls(pnlPersonnelMain.Controls, true);
                                else if (NewTxtOption == "O")
                                {//(tempControl.Name==txtOption.Name)
                                    this.Cancel();
                                    base.IterateFormToEnableControls(pnlPersonnelMain.Controls, false);
                                }
                            }

                        }
                        else if (txt.Text == "N")
                        {
                            base.IterateFormToEnableControls(pnlPersonnelMain.Controls, true);
                            txt_PR_EMP_TYPE.Text = "FT";
                            txt_PR_BANK_ID.Text = "Y";
                            txt_PR_TAX_INC.Text = "0";
                            txt_PR_TAX_PAID.Text = "0";
                            txt_PR_P_NO.Select();
                            txt_PR_P_NO.Focus();
                        }
                    }
                    else if (txtOption.Text == "D")
                    {
                        if (txtID.Text == string.Empty && txt_PR_P_NO.Text != string.Empty)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("PR_P_NO", txt_PR_P_NO.Text);
                            Result rsltCode;
                            CmnDataManager cmnDM = new CmnDataManager();
                            rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "PERSONNELSINFOEXIST", param);
                            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                            {
                                FillFrom(rsltCode.dstResult);
                            }
                        }

                        if (txtID.Text != string.Empty)
                        {
                            DialogResult dr = MessageBox.Show("Do You Want To Delete The Above Information [Y/N]", "Form", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (dr == DialogResult.Yes)
                            {
                                operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                                CallDelete("Y");
                                FormClear();
                                txtOption.Text = "D";
                                txt_W_OPTION_DIS.Text = "DELETE";
                                txtDate.Text = Now().ToString("dd/MM/yyyy");
                                txt_W_USER.Text = this.UserName;
                                txt_PR_P_NO.Select();
                                txt_PR_P_NO.Focus();
                            }
                            else
                            {
                                FormClear();
                                txtOption.Select();
                                txtOption.Focus();
                            }
                        }
                    }

                    //if (option == "O")
                    if (frm_Page2 != null && txtOption.Text == "V")
                    {
                        if (frm_Page2.Value == "O")
                        {
                            txtOption.Select();
                            txtOption.Focus();
                            option = string.Empty;
                        }
                        //else if (option == "N")
                        else if (frm_Page2.Value == "N")
                        {
                            base.IterateFormToEnableControls(pnlPersonnelMain.Controls, true);
                            txt_PR_P_NO.Select();
                            txt_PR_P_NO.Focus();
                            option = string.Empty;
                            e.Cancel = true;
                        }
                    }
                }
                //frm_Page2.Value = string.Empty;
                strDesg     = txt_PR_DESIG.Text;
                strLevel    = txt_PR_LEVEL.Text;
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_PR_P_NO_Validating", exp);
            }
        }

        /// <summary>
        /// txt_PR_BRANCH Leave Update the Values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_BRANCH_Leave(object sender, EventArgs e)
        {
            try
            {
                //if (txt_PR_BRANCH.Text == string.Empty || txt_PR_BRANCH.Text == null)
                //{
                //    //ClearForm(pnlPersonnelMain.Controls);
                //    //txtOption.Select();
                //    //txtOption.Focus();
                //}
                //else
                //{
                //    g_Pno = Convert.ToInt32(txt_PR_P_NO.Text);

                //    //if (txtOption.Text == "A")
                //    //{
                //    //    //Dictionary<string, object> param = new Dictionary<string, object>();
                //    //    //param.Add("PR_SNO", txt_PR_P_NO.Text);
                //    //    //Result rsltCode;
                //    //    //CmnDataManager cmnDM = new CmnDataManager();
                //    //    //rsltCode = cmnDM.Execute("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "SERIAL_NO", param);
                //    //}

                txt_PR_NEW_BRANCH.Text = txt_PR_BRANCH.Text;
                txtDate.Text = this.Now().ToString("dd/MM/yyyy");
                //}
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_BRANCH_Leave", exp);
            }
        }

        /// <summary>
        /// txt_PR_BRANCH = txt_PR_NEW_BRANCH
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_BRANCH_Validating(object sender, CancelEventArgs e)
        {
            txt_PR_NEW_BRANCH.Text = txt_PR_BRANCH.Text;
            txtDate.Text = this.Now().ToString("dd/MM/yyyy");
        }

        /// <summary>
        /// Set the VAlues After LOV Selection
        /// </summary>
        /// <param name="selectedRow"></param>
        /// <param name="actionType"></param>
        private void CHRIS_Personnel_RegularStaffHiringEnt_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            try
            {
                if (actionType == "PersonnelsInfo")
                {
                    if (txt_PR_RELIGION.Text == "MUSLIM")
                    {
                        txt_W_RELIGION.Text = "M";
                    }
                    else if (txt_PR_RELIGION.Text == "NON-MUSLIM")
                    {
                        txt_W_RELIGION.Text = "N";
                    }

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("PR_P_NO", txt_PR_P_NO.Text);
                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "GEID", param);
                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        txt_F_GEID_NO.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                else if (actionType == "DESIGNATION")
                {
                    if (txt_PR_LEVEL.Text != null)
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_DESIG", txt_PR_DESIG.Text);
                        param.Add("PR_LEVEL", txt_PR_LEVEL.Text);
                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();
                        rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "SP_CATEGORY", param);
                        if (rsltCode.isSuccessful)
                            txt_PR_CATEGORY.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CHRIS_Personnel_RegularStaffHiringEnt_AfterLOVSelection", exp);
            }
        }

        /// <summary>
        /// VALIDATING PR_DESIG TEXTBOX: FILL THE DESIG, LEVEL, ANNUAL_PACK AND CATEGORY TEXTBOXS 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_DESIG_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txt_PR_DESIG.Modified)
                {
                    if (txtOption.Text != "V" && lbtnDesignation.Focused != true)
                    {

                        if (txt_PR_LEVEL.Text != string.Empty)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("PR_DESIG", txt_PR_DESIG.Text);
                            param.Add("PR_LEVEL", txt_PR_LEVEL.Text);
                            param.Add("PR_NEW_BRANCH", txt_PR_NEW_BRANCH.Text);
                            Result rsltCode;
                            CmnDataManager cmnDM = new CmnDataManager();
                            rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "DESG_VALIDATE", param);

                            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                            {
                                txt_PR_DESIG.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                                txt_PR_LEVEL.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();

                                if (txt_PR_DESIG.Text != strDesg && strLevel != txt_PR_LEVEL.Text)
                                    txt_PR_ANNUAL_PACK.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                            }
                            else
                            {
                                MessageBox.Show("Invalid Designation Entered Press [F9] Key For Help", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                e.Cancel = true;
                            }
                        }
                        else
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("PR_DESIG", txt_PR_DESIG.Text);
                            param.Add("PR_NEW_BRANCH", txt_PR_NEW_BRANCH.Text);
                            Result rsltCode;
                            CmnDataManager cmnDM = new CmnDataManager();
                            rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "DESG_VALIDATE_WO_LEVEL", param);

                            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                            {
                                txt_PR_DESIG.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                                txt_PR_LEVEL.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();

                                if (txt_PR_DESIG.Text != strDesg && strLevel != txt_PR_LEVEL.Text)
                                    txt_PR_ANNUAL_PACK.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();

                                txt_PR_CATEGORY.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                            }
                            else
                            {
                                MessageBox.Show("Invalid Designation Entered Press [F9] Key For Help", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                e.Cancel = true;
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_DESIG_Validating", exp);
            }
        }

        /// <summary>
        /// Validate PR_LEVEL Textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_LEVEL_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txt_PR_LEVEL.Modified)
                {
                    if (txtOption.Text != "V")
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_DESIG", txt_PR_DESIG.Text);
                        param.Add("PR_LEVEL", txt_PR_LEVEL.Text);
                        param.Add("PR_NEW_BRANCH", txt_PR_NEW_BRANCH.Text);
                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();
                        rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "LEVEL_VALIDATE", param);

                        if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            txt_PR_LEVEL.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                            if (strLevel != txt_PR_LEVEL.Text)
                                txt_PR_ANNUAL_PACK.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_LEVEL_Validating", exp);
            }
        }

        /// <summary>
        /// on Leave event check if input is null or other than FT or PT then popup message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_EMP_TYPE_Leave(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// on Validating event check if input is null or other than FT or PT then popup message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_EMP_TYPE_Validating(object sender, CancelEventArgs e)
        {
            if (txt_PR_EMP_TYPE.Text == null || (txt_PR_EMP_TYPE.Text != "FT" && txt_PR_EMP_TYPE.Text != "PT"))
            {
                MessageBox.Show("Field Must Be Enter [FT] Full Time Or [PT] Part Time..", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void txt_W_RELIGION_Validating(object sender, CancelEventArgs e)
        {
            if (txt_W_RELIGION.Text == string.Empty || txt_W_RELIGION.Text == " ")
            {
                MessageBox.Show("Enter [M] For Muslim [N] For Non-Muslim", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_W_RELIGION.Text == "M")
            {
                txt_PR_RELIGION.Text = "MUSLIM";
            }
            else if (txt_W_RELIGION.Text == "N")
            {
                txt_PR_RELIGION.Text = "NON-MUSLIM";
            }

            if (txtOption.Text == "A")
            {
                txt_PR_CONF_FLAG.Text = "N";
            }
        }

        /// <summary>
        /// VALIDATING TEXTBOX AND SET THE PR_CONF_FLAG VALUE TO 'N'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_W_RELIGION_Leave(object sender, EventArgs e)
        {
            //if (txt_W_RELIGION.Text == string.Empty || txt_W_RELIGION.Text == " ")
            //{
            //    MessageBox.Show("Enter [M] For Muslim [N] For Non-Muslim", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);


            //}
            //else if (txt_W_RELIGION.Text == "M")
            //{
            //    txt_PR_RELIGION.Text = "MUSLIM";
            //}
            //else if (txt_W_RELIGION.Text == "N")
            //{
            //    txt_PR_RELIGION.Text = "NON-MUSLIM";
            //}

            //if (txtOption.Text == "A")
            //{
            //    txt_PR_CONF_FLAG.Text = "N";
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_JOINING_DATE_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txt_PR_JOINING_DATE.Value != null)
                {
                    //form trigger failure
                    //if (txt_PR_CONFIRM.Value == null)
                    if (txtOption.Text == "A")
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_DESIG", txt_PR_DESIG.Text);
                        param.Add("PR_LEVEL", txt_PR_LEVEL.Text);
                        param.Add("PR_JOINING_DATE", txt_PR_JOINING_DATE.Value);

                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();
                        rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "ADDMONTH", param);

                        if (rsltCode.isSuccessful)
                        {
                            txt_PR_CONFIRM.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_JOINING_DATE_Leave", exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_CONFIRM_Validating(object sender, CancelEventArgs e)
        {
            if (txt_PR_CONFIRM.Value == null || DateTime.Compare(Convert.ToDateTime(txt_PR_CONFIRM.Value), Convert.ToDateTime(txt_PR_JOINING_DATE.Value)) < 0)
            {
                MessageBox.Show("Invalid Date Entered...", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }

            if (txt_PR_CONFIRM.Value == txt_PR_JOINING_DATE.Value)
            {
                txt_PR_CONF_FLAG.Text = "Y";
            }
        }

        /// <summary>
        /// PR_ANNUAL_PACK Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_ANNUAL_PACK_Validating(object sender, CancelEventArgs e)
        {
           

            
            try
            {
                if (txt_PR_ANNUAL_PACK.Modified)
                {
                    decimal sp_main = 0;

                    if (txtOption.Text != "V" && txt_PR_ANNUAL_PACK.Text != string.Empty)
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_DESIG", txt_PR_DESIG.Text);
                        param.Add("PR_LEVEL", txt_PR_LEVEL.Text);
                        param.Add("PR_NEW_BRANCH", txt_PR_NEW_BRANCH.Text);

                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();
                        rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "SP_MIN", param);

                        if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            if (rsltCode.dstResult.Tables[0].Rows.Count == 1)
                                sp_main = Convert.ToInt32(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                            else if (rsltCode.dstResult.Tables[0].Rows.Count > 1)
                                e.Cancel = true;
                        }
                        if (Convert.ToDecimal(txt_PR_ANNUAL_PACK.Text) >= sp_main)
                        {
                            txt_PR_NEW_ANNUAL_PACK.Text = txt_PR_ANNUAL_PACK.Text;
                        }
                        else
                        {
                            MessageBox.Show("Annual Package Should Be Greater then Minimum Salary", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_ANNUAL_PACK_Validating", exp);
            }
        }

        /// <summary>
        /// ON PR_Annual_PAck Leave: If Modify then Call Proc_Inc Method -- else -- move to next Textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_ANNUAL_PACK_Leave(object sender, EventArgs e)
        {
          
            try
            {
                if (txtOption.Text == "M")
                {
                    Proc_Inc();
                }
                else
                {
                    txt_PR_NEW_ANNUAL_PACK.Text = txt_PR_ANNUAL_PACK.Text;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_ANNUAL_PACK_Leave", exp);
            }
        }

        /// <summary>
        /// On National TAx LEave Set the W_NAME Textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_NATIONAL_TAX_Leave(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// On National Tax Validation Set the W_NAME Textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_NATIONAL_TAX_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                string w_name = string.Empty;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", txt_PR_P_NO.Text);
                param.Add("PR_NATIONAL_TAX", txt_PR_NATIONAL_TAX.Text);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "NAT_TAX_LEAVE", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    w_name = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                if (w_name != string.Empty)
                {
                    MessageBox.Show("This Tax Is Given To " + w_name, "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_NATIONAL_TAX_Validating", exp);
            }
        }


        /// <summary>
        /// VAlidate the Current ACcount Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_ACCOUNT_NO_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                //if (txt_PR_ACCOUNT_NO.Text != string.Empty)
                {
                    bool verify_Acc;
                    //if (txt_PR_ACCOUNT_NO.Text.Length > 0)
                    //{
                        //MessageBox.Show("Account No. Is Left Blank", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                   // }
                    if (txt_PR_ACCOUNT_NO.Text.Trim().Length > 24)
                    {
                        MessageBox.Show("Account No. length cannot be greater than 24", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                    if (txt_PR_ACCOUNT_NO.Text.Trim().Length > 0)
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_P_NO", txt_PR_P_NO.Text);
                        param.Add("PR_ACCOUNT_NO", txt_PR_ACCOUNT_NO.Text);
                        Result rsltCode;
                        String PR_P_NO;
                        CmnDataManager cmnDM = new CmnDataManager();
                        rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "ACCOUNT_VALIDATE", param);

                        if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
                        {
                            PR_P_NO = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                            MessageBox.Show("Account number already used for Personnel Number: " + PR_P_NO, "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }
                    }

                    //if (txt_PR_ACCOUNT_NO.Text.Length == 10)
                    //{
                    //    string AccountNo        = txt_PR_ACCOUNT_NO.Text.Insert(1, "-");
                    //    AccountNo               = AccountNo.Insert(8, "-");
                    //    txt_PR_ACCOUNT_NO.Text  = AccountNo;
                    //}
                    //9-340180-008

                    //bool ValidAccount = IsValidExpression(txt_PR_ACCOUNT_NO.Text, InputValidator.ACCOUNTNO_REGEX);

                    //if (ValidAccount)
                    //{
                    //    verify_Acc = Account_Check();
                    //}
                    //else
                    //{
                    //    verify_Acc = false;
                    //}

                    //if (verify_Acc == false)
                    //{
                    //    MessageBox.Show("Enter Account Number In The Format 9-999999-999", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    e.Cancel = true;
                    //}
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_ACCOUNT_NO_Validating", exp);
            }
        }

        /// <summary>
        /// PR_BANK_ID VAlidation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_BANK_ID_Validating(object sender, CancelEventArgs e)
        {
            if (txt_PR_BANK_ID.Text == null || (txt_PR_BANK_ID.Text != "Y" && txt_PR_BANK_ID.Text != "N"))
            {
                MessageBox.Show("Valid Reply is [Y] or [N]  <F6>= Exit W/O Save <Ctrl+Page Down>=Next Screen", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_PR_BANK_ID.Text == "N")
            {
                txt_PR_TAX_INC.Focus();
                txt_PR_ID_ISSUE.Value       = null;
                txt_PR_EXPIRY.Value         = null;
                txt_PR_ID_ISSUE.IsRequired  = false;
                txt_PR_EXPIRY.IsRequired    = false;
                txt_PR_ID_ISSUE.Enabled     = false;
                txt_PR_EXPIRY.Enabled       = false;
            }
            else
            {
                txt_PR_ID_ISSUE.Enabled     = true;
                txt_PR_EXPIRY.Enabled       = true;
                txt_PR_ID_ISSUE.IsRequired  = true;
                txt_PR_EXPIRY.IsRequired    = true;
                txt_PR_ID_ISSUE.Focus();
                txt_PR_ID_ISSUE.IsRequired  = true;
                txt_PR_EXPIRY.IsRequired    = true;

                if (txt_PR_ID_ISSUE.Value == null)
                {
                    txt_PR_ID_ISSUE.Value = Now();
                }

                if (txt_PR_EXPIRY.Value == null)
                {
                    txt_PR_EXPIRY.Value = Now();
                }
            }
        }

        /// <summary>
        /// PR_ID_ISsue Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_ID_ISSUE_Validating(object sender, CancelEventArgs e)
        {
            if ((DateTime.Compare(Convert.ToDateTime(txt_PR_JOINING_DATE.Value), Convert.ToDateTime(txt_PR_ID_ISSUE.Value)) > 0) && txt_PR_BANK_ID.Text == "Y")
            {
                MessageBox.Show("Issue Date Cant Not Be Less Then Joining Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }

            else if (txt_PR_ID_ISSUE.Value == null)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// On moving back to previous control remove the ID_Issue and Expiry dates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_ID_ISSUE_LostFocus(object sender, System.EventArgs e)
        {
            if (txt_PR_BANK_ID.Focused == true)
            {
                txt_PR_ID_ISSUE.Value = null;
                txt_PR_EXPIRY.Value = null;
            }
        }

        /// <summary>
        /// txt_PR_EXPIRY Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_EXPIRY_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (DateTime.Compare(Convert.ToDateTime(txt_PR_ID_ISSUE.Value), Convert.ToDateTime(txt_PR_EXPIRY.Value)) > 0)
                {
                    MessageBox.Show("Expiry Date Can not Be Less Then Issue Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (txt_PR_EXPIRY.Value == null)
                {
                    e.Cancel = true;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_EXPIRY_Validating", exp);
            }
        }

        /// <summary>
        /// txt_PR_TAX_PAID Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_TAX_PAID_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txt_PR_TAX_PAID.Text != string.Empty)
                {
                    double intTaxPaid = Convert.ToDouble(txt_PR_TAX_PAID.Text == "" ? "0.0" : txt_PR_TAX_PAID.Text);
                    double intTaxxPaidInc = Convert.ToDouble(txt_PR_TAX_INC.Text == "" ? "0.0" : txt_PR_TAX_INC.Text);
                    int TaxPaid = Convert.ToInt32(intTaxPaid);
                    int TaxxPaidInc = Convert.ToInt32(intTaxxPaidInc);

                    if (TaxPaid > TaxxPaidInc)
                    {
                        //MessageBox.Show("Tax Paid Can Not Be Greater Then Taxable Income", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }

                    if (txtOption.Text == "M")
                    {
                        //ClearForm(pnlPersonnelMain.Controls);
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_TAX_PAID_Validating", exp);
            }
        }

        /// <summary>
        /// Check the Mode: Different Flows for different Modes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtOption_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txtOption.Text != "A" && txtOption.Text != "M" && txtOption.Text != "V" && txtOption.Text != "D" && txtOption.Text != "E")
                {
                    return;
                }
                else if (txtOption.Text == "E")
                {
                    ClearForm(pnlPersonnelMain.Controls);
                    this.Close();
                }
                else if (txtOption.Text == "A")
                {
                    //SerialNO
                    //e.Cancel = false;

                    
                   
                }
                else if (txtOption.Text == "M")
                {
                    //next_block; enter_query(For_Update, no_wait);
                    txt_PR_P_NO.AssociatedLookUpName= "lbtnPersonnelNO";
                    lbtnPersonnelNO.Visible         = true;
                    txt_W_OPTION_DIS.Text           = "MODIFY";
                    txt_PR_P_NO.Select();
                    txt_PR_P_NO.Focus();
                    valSaved                        = false;
                }
                else if (txtOption.Text == "V")
                {
                    txt_W_OPTION_DIS.Text = "VIEW";
                    txt_PR_P_NO.AssociatedLookUpName = "lbtnPersonnelNO";
                    lbtnPersonnelNO.Visible = true;
                }
                else if (txtOption.Text == "D")
                {
                    //next_block;enter_query(For_Update, NO_WAIT);
                    txt_W_OPTION_DIS.Text = "DELETE";
                    txt_PR_P_NO.AssociatedLookUpName = "lbtnPersonnelNO";
                    lbtnPersonnelNO.Visible = true;
                }

                txtDate.Text = Now().ToString("dd/MM/yyyy");
                //txtLocation.Text                      = this.Location;
                txt_W_USER.Text = this.UserName;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txtOption_Validating", exp);
            }

        }

        /// <summary>
        /// Before SAve 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_W_ADD_ANS_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                g_w_Date_To = DateTime.MinValue;
                g_Pno = int.MinValue;

                if ((txt_W_ADD_ANS.Text == "Y" && txt_W_ADD_ANS.Text == "N") || txt_W_ADD_ANS.Text == null)
                {
                    e.Cancel = true;
                }
                else if (txt_W_ADD_ANS.Text == "Y")
                {
                    Pro_Rate_Leaves();

                    //Without IP
                    //CallReport();

                    //With IP
                    //CallReport();


                    if (txtOption.Text == "A")
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_P_NO", txt_PR_P_NO.Text);
                        param.Add("PR_NATIONAL_TAX", txt_PR_NATIONAL_TAX.Text);

                        Result rsltCode;

                        rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "AddSerialNum", param);

                        if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            txt_PR_P_NO.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                            txt_W_OPTION_DIS.Text = "ADD";
                            //txt_PR_BRANCH.Select();
                            txt_PR_BRANCH.Focus();
                        }
                    }
                }
                else
                {
                    ClearForm(pnlPersonnelMain.Controls);

                    txt_W_USER.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                    txtLocation.Text = g_Location;

                    ClearForm(pnlPersonnelMain.Controls);
                    txtOption.Text = "M";

                }

                //pnlHead.Visible = false;

                if (txt_W_ADD_ANS.Text == "Y")
                {
                    this.Save();
                }
                else if (txt_W_ADD_ANS.Text == "N")
                {

                }

            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_W_ADD_ANS_Validating", exp);
            }
        }

        /// <summary>
        /// Open Department Popup if the P in textBox, else do move focus to other controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void txt_W_VIEW_ANS_Validating(object sender, CancelEventArgs e)
        //{
        //    try
        //    {
        //        if ((txt_W_ADD_ANS.Text == null) || txt_W_ADD_ANS.Text == "N" && txt_W_ADD_ANS.Text == "P" && txt_W_ADD_ANS.Text == "O")
        //        {
        //            e.Cancel = true;
        //        }
        //        else
        //        {
        //            if (txt_W_ADD_ANS.Text == "N")
        //            {
        //                ClearForm(pnlPersonnelMain.Controls);
        //                txtOption.Text = "V";
        //                txt_PR_P_NO.Select();
        //                txt_PR_P_NO.Focus();
        //            }
        //            else if (txt_W_ADD_ANS.Text == "P")
        //            {
        //                //go_block('blkdept');execute_query;
        //                //Open Pop of Department Grid
        //            }
        //            else
        //            {
        //                ClearForm(pnlPersonnelMain.Controls);
        //                txtOption.Select();
        //                txtOption.Focus();
        //            }
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        LogException(this.Name, "txt_W_VIEW_ANS_Validating", exp);
        //    }
        //}

        /// <summary>
        /// On TAx_Paid Text Box keypress: Check if tab i spressed than move to next popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_TAX_PAID_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == '\t')
                {
                    frm_Dpt = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, null, dgvBlkDept.GridSource, dgvBlkEmp.GridSource, dgvBlkRef.GridSource, dgvBlkChild.GridSource, dgvBlkEdu.GridSource);
                    frm_Dpt.MdiParent = null;
                    frm_Dpt.Enabled = true;
                    frm_Dpt.ShowDialog();
                    strCurrentPopUP = "BLKDEPT";
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "ShortCutKey_Press", exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_W_DEL_ANS_Validating(object sender, CancelEventArgs e)
        {
            //try
            //{

            //    if (txt_W_DEL_ANS.Text != "Y" && txt_W_DEL_ANS.Text != "N")
            //    {
            //        e.Cancel = true;
            //    }
            //    else if (txt_W_DEL_ANS.Text == "Y")
            //    {
            //        txt_PR_CLOSE_FLAG.Text = "C";
            //        g_Pno = Convert.ToInt32(txt_PR_P_NO.Text);

            //        CallReport();

            //        txtOption.Text = "D";
            //        txt_PR_P_NO.Select();
            //        txt_PR_P_NO.Focus();

            //    }

            //    this.Delete();
            //    txtOption.Select();
            //    txtOption.Focus();
            //}
            //catch (Exception exp)
            //{
            //    LogException(this.Name, "txt_W_DEL_ANS_Validating", exp);
            //}
        }

        /// <summary>
        /// After Compelte successfull validation close the SAve Panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_W_ADD_ANS_Validated(object sender, EventArgs e)
        {
            //pnlSave.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_W_VIEW_ANS_Validating(object sender, CancelEventArgs e)
        {
            //try
            //{
            //    //TextBox txt = (TextBox)sender;
            //    if ((txt_W_VIEW_ANS.Text == null) || txt_W_VIEW_ANS.Text == "N" && txt_W_VIEW_ANS.Text == "P" && txt_W_VIEW_ANS.Text == "O")
            //    {
            //        //e.Cancel = true;
            //    }
            //    else
            //    {
            //        if (txt_W_VIEW_ANS.Text == "N")
            //        {
            //            base.Cancel();
            //            txt_PR_JOINING_DATE.Value = null;
            //            txt_PR_CONFIRM.Value = null;
            //            txt_PR_ID_ISSUE.Value = null;
            //            txt_PR_EXPIRY.Value = null;
            //            txtOption.Text = "V";
            //            txt_PR_P_NO.Select();
            //            txt_PR_P_NO.Focus();
            //        }
            //        else if (txt_W_VIEW_ANS.Text == "P")
            //        {

            //            //go_block('blkdept');execute_query;
            //            //frm_Page2.Close();  
            //            //frm_Dpt = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvBlkDept.GridSource, dgvBlkEmp.GridSource, dgvBlkRef.GridSource, dgvBlkChild.GridSource, dgvBlkEdu.GridSource);
            //            //frm_Dpt.MdiParent = null;
            //            //frm_Dpt.Enabled = true;
            //            //frm_Page2.Hide();
            //            //frm_Dpt.ShowDialog();
            //            //strCurrentPopUP = "BLKDEPT";
            //        }
            //        else
            //        {
            //            base.Cancel();
            //            txt_PR_JOINING_DATE.Value = null;
            //            txt_PR_CONFIRM.Value = null;
            //            txt_PR_ID_ISSUE.Value = null;
            //            txt_PR_EXPIRY.Value = null;

            //            txtOption.Select();
            //            txtOption.Focus();
            //        }
            //    }
            //}
            //catch (Exception exp)
            //{
            //    LogError(this.Name, "TextBox_Leave", exp);
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_W_VIEW_ANS_Validated(object sender, EventArgs e)
        {
            //pnlView.Visible = false;
            //if (txt_W_VIEW_ANS.Text == "P")
            //{
            //    frm_Dpt = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvBlkDept.GridSource, dgvBlkEmp.GridSource, dgvBlkRef.GridSource, dgvBlkChild.GridSource, dgvBlkEdu.GridSource);
            //    frm_Dpt.MdiParent = null;
            //    frm_Dpt.Enabled = true;
            //    frm_Dpt.Select();
            //    frm_Dpt.Focus();
            //    frm_Dpt.ShowDialog();
            //}
        }

        private void txt_PR_TAX_PAID_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

            if (txt_PR_TAX_PAID.Text != string.Empty)
            {
                double intTaxPaid = Convert.ToDouble(txt_PR_TAX_PAID.Text == "" ? "0.0" : txt_PR_TAX_PAID.Text);
                double intTaxxPaidInc = Convert.ToDouble(txt_PR_TAX_INC.Text == "" ? "0.0" : txt_PR_TAX_INC.Text);
                int TaxPaid = Convert.ToInt32(intTaxPaid);
                int TaxxPaidInc = Convert.ToInt32(intTaxxPaidInc);

                if (TaxPaid > TaxxPaidInc)
                {
                    MessageBox.Show("Tax Paid Can Not Be Greater Then Taxable Income", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txt_PR_TAX_PAID.Select();
                    txt_PR_TAX_PAID.Focus();
                    return;
                }

                if (txtOption.Text == "M")
                {
                    //ClearForm(pnlPersonnelMain.Controls);
                }
            }

            if (e.KeyCode == Keys.Tab)
            {
                if (FromValidate())
                {
                    frm_Dpt = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, null, dgvBlkDept.GridSource, dgvBlkEmp.GridSource, dgvBlkRef.GridSource, dgvBlkChild.GridSource, dgvBlkEdu.GridSource);
                    frm_Dpt.MdiParent = null;
                    frm_Dpt.Enabled = true;
                    frm_Dpt.ShowDialog(this);

                    if (tem_W_ADD_ANS == "Y")
                    {
                        if (txtOption.Text == "M")
                        {
                            txt_PR_P_NO.Select();
                            txt_PR_P_NO.Focus();
                        }
                        else if (txtOption.Text == "A")
                        {
                            
                            //txtOption.Text = "A";
                            //txt_PR_BRANCH.CustomEnabled = false;
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            Result rsltCode;
                            CmnDataManager cmnDM = new CmnDataManager();
                            rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "SERIALNO", param);

                            if (rsltCode.isSuccessful)
                                txt_PR_P_NO.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                            lbtnPersonnelNO.SkipValidationOnLeave = true;
                            txt_PR_P_NO.Select();
                            txt_PR_P_NO.Focus();
                            e.IsInputKey = false;
                            return;
                        }
                    }
                }
            }
        }

        private void txt_PR_RELIGION_TextChanged(object sender, EventArgs e)
        {
            if (txt_W_RELIGION.Text == string.Empty)
            {
                if (txt_PR_RELIGION.Text == "MUSLIM")
                {
                    txt_W_RELIGION.Text = "M";
                }
                else if (txt_PR_RELIGION.Text == "NON-MUSLIM")
                {
                    txt_W_RELIGION.Text = "N";
                }
            }
        }

        private void txtLoc_Leave(object sender, EventArgs e)
        {
            txt_PR_FIRST_NAME.Focus();
        }

        #endregion

        private void txtReportTo_Validating(object sender, CancelEventArgs e)
        {
            //if (txtOption.Text == "M")
            //{
            //    lbtnRepNo.ActionLOVExists = "as";
            //}
            //else
            //{
            //    lbtnRepNo.ActionLOVExists = "REP_NO_EXIST";
            //}
        }

        private void txtReportTo_Enter(object sender, EventArgs e)
        {
            if (txtOption.Text == "M")
            {
                lbtnRepNo.SkipValidationOnLeave = true;
            }
            else
            {
                lbtnRepNo.SkipValidationOnLeave = false;
            }
        }

        private void txt_PR_ACCOUNT_NO_Enter(object sender, EventArgs e)
        {
            try
            {
                if (txt_PR_ACCOUNT_NO.Text != string.Empty)
                {
                    if (txt_PR_ACCOUNT_NO.Text.Contains("-"))
                    {
                        string Account = txt_PR_ACCOUNT_NO.Text;
                        txt_PR_ACCOUNT_NO.Text = Account.Replace("-", "");
                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_PR_ACCOUNT_NO_Enter", exp);
            }
        }

        private void txt_PR_DESIG_Leave(object sender, EventArgs e)
        {
            if (txt_PR_DESIG.Modified)
            {
                lbtnDesignation.SkipValidationOnLeave = false;
            }
            else
            {
                lbtnDesignation.SkipValidationOnLeave = true;
            }
        }

        private void txt_PR_LEVEL_Leave(object sender, EventArgs e)
        {
            if (txt_PR_LEVEL.Modified)
            {
                lbtnDesgLevel.SkipValidationOnLeave = false;
            }
            else
            {
                lbtnDesgLevel.SkipValidationOnLeave = true;
            }
        }

        private void slTxtbEmail_Leave(object sender, EventArgs e)
        {
            if (slTxtbEmail.Text != String.Empty)
            {
                try
                {
                    System.Net.Mail.MailAddress m = new System.Net.Mail.MailAddress(slTxtbEmail.Text);
                }
                catch (FormatException)
                {
                    slTxtbEmail.Select();
                    MessageBox.Show("Please enter a valid email address");
                }
            }
        }
        //public void dgvBlkDept_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        //{
        //    SLDataGridView dgv = (SLDataGridView)sender;

        //    if (dgv == null || e.RowIndex == 0 )//|| e.RowIndex == 1)
        //            return;

        //        if (dgv.Rows[e.RowIndex - 1].Cells[0].EditedFormattedValue == null || dgv.Rows[e.RowIndex - 1].Cells[0].EditedFormattedValue.ToString() == ""
        //        || dgv.Rows[e.RowIndex - 1].Cells[1].EditedFormattedValue == null || dgv.Rows[e.RowIndex - 1].Cells[1].EditedFormattedValue.ToString() == ""
        //        || dgv.Rows[e.RowIndex - 1].Cells[2].EditedFormattedValue == null || dgv.Rows[e.RowIndex - 1].Cells[2].EditedFormattedValue.ToString() == "")
        //    {
        //        dgv.CancelEdit();
        //    }
        //}

        //void dt_TableNewRow(object sender, DataTableNewRowEventArgs e)
        //{
        //    List<DataRow> rows = new List<DataRow>();
        //    foreach (DataRow r in e.Row.Table.Rows)
        //    {
        //        if (r[2] == null || r.ToString() == "" || r[3] == null || r[3].ToString() == "" && r[4] != null || r[4].ToString() == "")
        //        {
        //            rows.Add(r);
        //            //((DataTable)(sender)).Rows.RemoveAt(1);
        //            //e.Row.Delete();
        //        }
        //    }
        //    foreach (DataRow r in rows)
        //    {
        //        e.Row.Table.Rows.Remove(r);
        //    }

        //    //DataTable DT = ((DataTable)(sender));

        //    //if (e.Row[2] == null || e.Row[2].ToString() == "" || e.Row[3] == null || e.Row[3].ToString() == "" && e.Row[4] != null || e.Row[4].ToString() == "")
        //    //{
        //    //    //((DataTable)(sender)).Rows.RemoveAt(1);
        //    //    e.Row.Delete();
        //    //}
        //    //throw new Exception("The method or operation is not implemented.");
        //}
    }
}