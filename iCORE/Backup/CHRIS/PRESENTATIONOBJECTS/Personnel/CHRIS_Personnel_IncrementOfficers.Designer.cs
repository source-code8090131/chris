namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_IncrementOfficers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_IncrementOfficers));
            this.PnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAnnualPresnt = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_DESIG_PRESENT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnel = new CrplControlLibrary.SLTextBox(this.components);
            this.PnlIncPromotion = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_PR_EFFECTIVE = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelID = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnIncType = new CrplControlLibrary.LookupButton(this.components);
            this.txtPNo = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnDate = new CrplControlLibrary.LookupButton(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncPr_p_no = new CrplControlLibrary.SLTextBox(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.dtLastApp = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtNextApp = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtPR_ANNUAL_PREVIOUS1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_DESIG_PREVIOUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_LEVEL_PREVIOUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtnew_branch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_FINAL = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_CURRENT_PREVIOUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_FUNCT_2_PREVIOUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_FUNCT_1_PREVIOUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_FUNCT_2_PRESENT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_FUNCT_1_PRESENT = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtPR_RANKING_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtPR_RANK = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_REMARKS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtWP_pre = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncrementPer = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_ANNUAL_PRESENT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncAmonut = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_DESIG_PRESENT1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_LEVEL_PRESENT1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncremnttype = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlPersonnel.SuspendLayout();
            this.PnlIncPromotion.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(627, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(663, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 549);
            this.panel1.Size = new System.Drawing.Size(663, 60);
            // 
            // PnlPersonnel
            // 
            this.PnlPersonnel.ConcurrentPanels = null;
            this.PnlPersonnel.Controls.Add(this.lbPersonnel);
            this.PnlPersonnel.Controls.Add(this.label22);
            this.PnlPersonnel.Controls.Add(this.label21);
            this.PnlPersonnel.Controls.Add(this.label20);
            this.PnlPersonnel.Controls.Add(this.label2);
            this.PnlPersonnel.Controls.Add(this.label1);
            this.PnlPersonnel.Controls.Add(this.txtAnnualPresnt);
            this.PnlPersonnel.Controls.Add(this.txtPR_DESIG_PRESENT);
            this.PnlPersonnel.Controls.Add(this.txtLevel);
            this.PnlPersonnel.Controls.Add(this.txtName);
            this.PnlPersonnel.Controls.Add(this.txtPersonnel);
            this.PnlPersonnel.DataManager = null;
            this.PnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPersonnel.DependentPanels = null;
            this.PnlPersonnel.DisableDependentLoad = false;
            this.PnlPersonnel.EnableDelete = true;
            this.PnlPersonnel.EnableInsert = true;
            this.PnlPersonnel.EnableQuery = false;
            this.PnlPersonnel.EnableUpdate = true;
            this.PnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.PnlPersonnel.Location = new System.Drawing.Point(12, 193);
            this.PnlPersonnel.MasterPanel = null;
            this.PnlPersonnel.Name = "PnlPersonnel";
            this.PnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPersonnel.Size = new System.Drawing.Size(645, 62);
            this.PnlPersonnel.SPName = "CHRIS_SP_personnel_IncrementOfficer_MANAGER";
            this.PnlPersonnel.TabIndex = 1;
            // 
            // lbPersonnel
            // 
            this.lbPersonnel.ActionLOVExists = "PR_P_NO_exist";
            this.lbPersonnel.ActionType = "PR_P_NO";
            this.lbPersonnel.ConditionalFields = "";
            this.lbPersonnel.CustomEnabled = true;
            this.lbPersonnel.DataFieldMapping = "";
            this.lbPersonnel.DependentLovControls = "";
            this.lbPersonnel.HiddenColumns = "";
            this.lbPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lbPersonnel.Image")));
            this.lbPersonnel.LoadDependentEntities = false;
            this.lbPersonnel.Location = new System.Drawing.Point(276, 14);
            this.lbPersonnel.LookUpTitle = null;
            this.lbPersonnel.Name = "lbPersonnel";
            this.lbPersonnel.Size = new System.Drawing.Size(26, 21);
            this.lbPersonnel.SkipValidationOnLeave = false;
            this.lbPersonnel.SPName = "CHRIS_SP_personnel_IncrementOfficer_MANAGER";
            this.lbPersonnel.TabIndex = 10;
            this.lbPersonnel.TabStop = false;
            this.lbPersonnel.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(539, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 9;
            this.label22.Text = "Level :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(425, 18);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(43, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "Desig:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(308, 18);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 13);
            this.label20.TabIndex = 7;
            this.label20.Text = "ASR:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "2)  Name                          :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "1)  Personnel No.              :";
            // 
            // txtAnnualPresnt
            // 
            this.txtAnnualPresnt.AllowSpace = true;
            this.txtAnnualPresnt.AssociatedLookUpName = "";
            this.txtAnnualPresnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnualPresnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnualPresnt.ContinuationTextBox = null;
            this.txtAnnualPresnt.CustomEnabled = true;
            this.txtAnnualPresnt.DataFieldMapping = "PR_ANNUAL_PRESENT";
            this.txtAnnualPresnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnualPresnt.GetRecordsOnUpDownKeys = false;
            this.txtAnnualPresnt.IsDate = false;
            this.txtAnnualPresnt.Location = new System.Drawing.Point(350, 15);
            this.txtAnnualPresnt.MaxLength = 11;
            this.txtAnnualPresnt.Name = "txtAnnualPresnt";
            this.txtAnnualPresnt.NumberFormat = "###,###,##0.00";
            this.txtAnnualPresnt.Postfix = "";
            this.txtAnnualPresnt.Prefix = "";
            this.txtAnnualPresnt.ReadOnly = true;
            this.txtAnnualPresnt.Size = new System.Drawing.Size(65, 20);
            this.txtAnnualPresnt.SkipValidation = false;
            this.txtAnnualPresnt.TabIndex = 41;
            this.txtAnnualPresnt.TabStop = false;
            this.txtAnnualPresnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAnnualPresnt.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtPR_DESIG_PRESENT
            // 
            this.txtPR_DESIG_PRESENT.AllowSpace = true;
            this.txtPR_DESIG_PRESENT.AssociatedLookUpName = "";
            this.txtPR_DESIG_PRESENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESIG_PRESENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESIG_PRESENT.ContinuationTextBox = null;
            this.txtPR_DESIG_PRESENT.CustomEnabled = true;
            this.txtPR_DESIG_PRESENT.DataFieldMapping = "PR_DESIG_PRESENT";
            this.txtPR_DESIG_PRESENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESIG_PRESENT.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESIG_PRESENT.IsDate = false;
            this.txtPR_DESIG_PRESENT.Location = new System.Drawing.Point(474, 15);
            this.txtPR_DESIG_PRESENT.Name = "txtPR_DESIG_PRESENT";
            this.txtPR_DESIG_PRESENT.NumberFormat = "###,###,##0.00";
            this.txtPR_DESIG_PRESENT.Postfix = "";
            this.txtPR_DESIG_PRESENT.Prefix = "";
            this.txtPR_DESIG_PRESENT.ReadOnly = true;
            this.txtPR_DESIG_PRESENT.Size = new System.Drawing.Size(56, 20);
            this.txtPR_DESIG_PRESENT.SkipValidation = false;
            this.txtPR_DESIG_PRESENT.TabIndex = 31;
            this.txtPR_DESIG_PRESENT.TabStop = false;
            this.txtPR_DESIG_PRESENT.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_DESIG_PRESENT.TextChanged += new System.EventHandler(this.txtPR_DESIG_PRESENT_TextChanged);
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "PR_LEVEL_PRESENT";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(591, 15);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(24, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 11;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(191, 41);
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(284, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 11;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnel
            // 
            this.txtPersonnel.AllowSpace = true;
            this.txtPersonnel.AssociatedLookUpName = "lbPersonnel";
            this.txtPersonnel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnel.ContinuationTextBox = null;
            this.txtPersonnel.CustomEnabled = true;
            this.txtPersonnel.DataFieldMapping = "PR_P_NO";
            this.txtPersonnel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnel.GetRecordsOnUpDownKeys = false;
            this.txtPersonnel.IsDate = false;
            this.txtPersonnel.IsRequired = true;
            this.txtPersonnel.Location = new System.Drawing.Point(191, 15);
            this.txtPersonnel.MaxLength = 6;
            this.txtPersonnel.Name = "txtPersonnel";
            this.txtPersonnel.NumberFormat = "###,###,##0.00";
            this.txtPersonnel.Postfix = "";
            this.txtPersonnel.Prefix = "";
            this.txtPersonnel.Size = new System.Drawing.Size(82, 20);
            this.txtPersonnel.SkipValidation = false;
            this.txtPersonnel.TabIndex = 2;
            this.txtPersonnel.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnel.TextChanged += new System.EventHandler(this.txtPersonnel_TextChanged);
            // 
            // PnlIncPromotion
            // 
            this.PnlIncPromotion.ConcurrentPanels = null;
            this.PnlIncPromotion.Controls.Add(this.txt_PR_EFFECTIVE);
            this.PnlIncPromotion.Controls.Add(this.txtPersonnelID);
            this.PnlIncPromotion.Controls.Add(this.lbtnIncType);
            this.PnlIncPromotion.Controls.Add(this.txtPNo);
            this.PnlIncPromotion.Controls.Add(this.lbtnDate);
            this.PnlIncPromotion.Controls.Add(this.label24);
            this.PnlIncPromotion.Controls.Add(this.txtID);
            this.PnlIncPromotion.Controls.Add(this.txtIncPr_p_no);
            this.PnlIncPromotion.Controls.Add(this.label23);
            this.PnlIncPromotion.Controls.Add(this.dtLastApp);
            this.PnlIncPromotion.Controls.Add(this.dtNextApp);
            this.PnlIncPromotion.Controls.Add(this.txtPR_ANNUAL_PREVIOUS1);
            this.PnlIncPromotion.Controls.Add(this.txtPR_DESIG_PREVIOUS);
            this.PnlIncPromotion.Controls.Add(this.txtPR_LEVEL_PREVIOUS);
            this.PnlIncPromotion.Controls.Add(this.txtnew_branch);
            this.PnlIncPromotion.Controls.Add(this.txtPR_FINAL);
            this.PnlIncPromotion.Controls.Add(this.txtPR_CURRENT_PREVIOUS);
            this.PnlIncPromotion.Controls.Add(this.txtPR_FUNCT_2_PREVIOUS);
            this.PnlIncPromotion.Controls.Add(this.txtPR_FUNCT_1_PREVIOUS);
            this.PnlIncPromotion.Controls.Add(this.txtPR_FUNCT_2_PRESENT);
            this.PnlIncPromotion.Controls.Add(this.txtPR_FUNCT_1_PRESENT);
            this.PnlIncPromotion.Controls.Add(this.label10);
            this.PnlIncPromotion.Controls.Add(this.label12);
            this.PnlIncPromotion.Controls.Add(this.label11);
            this.PnlIncPromotion.Controls.Add(this.label9);
            this.PnlIncPromotion.Controls.Add(this.label8);
            this.PnlIncPromotion.Controls.Add(this.label7);
            this.PnlIncPromotion.Controls.Add(this.label6);
            this.PnlIncPromotion.Controls.Add(this.label5);
            this.PnlIncPromotion.Controls.Add(this.label4);
            this.PnlIncPromotion.Controls.Add(this.label3);
            this.PnlIncPromotion.Controls.Add(this.dtPR_RANKING_DATE);
            this.PnlIncPromotion.Controls.Add(this.txtPR_RANK);
            this.PnlIncPromotion.Controls.Add(this.txtPR_REMARKS);
            this.PnlIncPromotion.Controls.Add(this.txtWP_pre);
            this.PnlIncPromotion.Controls.Add(this.txtIncrementPer);
            this.PnlIncPromotion.Controls.Add(this.txtPR_ANNUAL_PRESENT);
            this.PnlIncPromotion.Controls.Add(this.txtIncAmonut);
            this.PnlIncPromotion.Controls.Add(this.txtPR_DESIG_PRESENT1);
            this.PnlIncPromotion.Controls.Add(this.txtPR_LEVEL_PRESENT1);
            this.PnlIncPromotion.Controls.Add(this.txtIncremnttype);
            this.PnlIncPromotion.DataManager = null;
            this.PnlIncPromotion.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlIncPromotion.DependentPanels = null;
            this.PnlIncPromotion.DisableDependentLoad = false;
            this.PnlIncPromotion.EnableDelete = true;
            this.PnlIncPromotion.EnableInsert = true;
            this.PnlIncPromotion.EnableQuery = false;
            this.PnlIncPromotion.EnableUpdate = true;
            this.PnlIncPromotion.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.Inc_Promotion_officerCommand";
            this.PnlIncPromotion.Location = new System.Drawing.Point(12, 258);
            this.PnlIncPromotion.MasterPanel = null;
            this.PnlIncPromotion.Name = "PnlIncPromotion";
            this.PnlIncPromotion.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlIncPromotion.Size = new System.Drawing.Size(645, 284);
            this.PnlIncPromotion.SPName = "CHRIS_SP_INC_PROMOTION_Increment_MANAGER";
            this.PnlIncPromotion.TabIndex = 3;
            // 
            // txt_PR_EFFECTIVE
            // 
            this.txt_PR_EFFECTIVE.AllowSpace = true;
            this.txt_PR_EFFECTIVE.AssociatedLookUpName = "lbtnDate";
            this.txt_PR_EFFECTIVE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_EFFECTIVE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_EFFECTIVE.ContinuationTextBox = null;
            this.txt_PR_EFFECTIVE.CustomEnabled = true;
            this.txt_PR_EFFECTIVE.DataFieldMapping = "PR_EFFECTIVE";
            this.txt_PR_EFFECTIVE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_EFFECTIVE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_EFFECTIVE.IsDate = false;
            this.txt_PR_EFFECTIVE.Location = new System.Drawing.Point(191, 28);
            this.txt_PR_EFFECTIVE.Name = "txt_PR_EFFECTIVE";
            this.txt_PR_EFFECTIVE.NumberFormat = "dd/MM/yyyy";
            this.txt_PR_EFFECTIVE.Postfix = "";
            this.txt_PR_EFFECTIVE.Prefix = "";
            this.txt_PR_EFFECTIVE.Size = new System.Drawing.Size(100, 20);
            this.txt_PR_EFFECTIVE.SkipValidation = false;
            this.txt_PR_EFFECTIVE.TabIndex = 4;
            this.txt_PR_EFFECTIVE.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_EFFECTIVE.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_EFFECTIVE_Validating);
            // 
            // txtPersonnelID
            // 
            this.txtPersonnelID.AllowSpace = true;
            this.txtPersonnelID.AssociatedLookUpName = "lbPersonnel";
            this.txtPersonnelID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelID.ContinuationTextBox = null;
            this.txtPersonnelID.CustomEnabled = true;
            this.txtPersonnelID.DataFieldMapping = "PR_P_NO";
            this.txtPersonnelID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelID.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelID.IsDate = false;
            this.txtPersonnelID.IsRequired = true;
            this.txtPersonnelID.Location = new System.Drawing.Point(191, 251);
            this.txtPersonnelID.MaxLength = 6;
            this.txtPersonnelID.Name = "txtPersonnelID";
            this.txtPersonnelID.NumberFormat = "###,###,##0.00";
            this.txtPersonnelID.Postfix = "";
            this.txtPersonnelID.Prefix = "";
            this.txtPersonnelID.Size = new System.Drawing.Size(82, 20);
            this.txtPersonnelID.SkipValidation = false;
            this.txtPersonnelID.TabIndex = 44;
            this.txtPersonnelID.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnelID.Visible = false;
            // 
            // lbtnIncType
            // 
            this.lbtnIncType.ActionLOVExists = "";
            this.lbtnIncType.ActionType = "INC_TYPE";
            this.lbtnIncType.ConditionalFields = "txtPersonnelID";
            this.lbtnIncType.CustomEnabled = true;
            this.lbtnIncType.DataFieldMapping = "";
            this.lbtnIncType.DependentLovControls = "";
            this.lbtnIncType.HiddenColumns = "";
            this.lbtnIncType.Image = ((System.Drawing.Image)(resources.GetObject("lbtnIncType.Image")));
            this.lbtnIncType.LoadDependentEntities = false;
            this.lbtnIncType.Location = new System.Drawing.Point(240, 2);
            this.lbtnIncType.LookUpTitle = null;
            this.lbtnIncType.Name = "lbtnIncType";
            this.lbtnIncType.Size = new System.Drawing.Size(26, 21);
            this.lbtnIncType.SkipValidationOnLeave = true;
            this.lbtnIncType.SPName = "CHRIS_SP_INC_PROMOTION_INCREMENT_MANAGER";
            this.lbtnIncType.TabIndex = 43;
            this.lbtnIncType.TabStop = false;
            this.lbtnIncType.UseVisualStyleBackColor = true;
            // 
            // txtPNo
            // 
            this.txtPNo.AllowSpace = true;
            this.txtPNo.AssociatedLookUpName = "lbPersonnel";
            this.txtPNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPNo.ContinuationTextBox = null;
            this.txtPNo.CustomEnabled = false;
            this.txtPNo.DataFieldMapping = "PR_P_NO";
            this.txtPNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPNo.GetRecordsOnUpDownKeys = false;
            this.txtPNo.IsDate = false;
            this.txtPNo.Location = new System.Drawing.Point(500, 104);
            this.txtPNo.Name = "txtPNo";
            this.txtPNo.NumberFormat = "###,###,##0.00";
            this.txtPNo.Postfix = "";
            this.txtPNo.Prefix = "";
            this.txtPNo.Size = new System.Drawing.Size(64, 20);
            this.txtPNo.SkipValidation = false;
            this.txtPNo.TabIndex = 42;
            this.txtPNo.TextType = CrplControlLibrary.TextType.String;
            this.txtPNo.Visible = false;
            // 
            // lbtnDate
            // 
            this.lbtnDate.ActionLOVExists = "";
            this.lbtnDate.ActionType = "DateLOV";
            this.lbtnDate.ConditionalFields = "txtPNo|txtIncremnttype";
            this.lbtnDate.CustomEnabled = true;
            this.lbtnDate.DataFieldMapping = "";
            this.lbtnDate.DependentLovControls = "";
            this.lbtnDate.HiddenColumns = "";
            this.lbtnDate.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDate.Image")));
            this.lbtnDate.LoadDependentEntities = false;
            this.lbtnDate.Location = new System.Drawing.Point(297, 27);
            this.lbtnDate.LookUpTitle = null;
            this.lbtnDate.Name = "lbtnDate";
            this.lbtnDate.Size = new System.Drawing.Size(26, 21);
            this.lbtnDate.SkipValidationOnLeave = true;
            this.lbtnDate.SPName = "CHRIS_SP_personnel_IncrementOfficer_MANAGER";
            this.lbtnDate.TabIndex = 41;
            this.lbtnDate.TabStop = false;
            this.lbtnDate.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(339, 56);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(107, 13);
            this.label24.TabIndex = 40;
            this.label24.Text = "(Before Effective)";
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(425, 105);
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.Size = new System.Drawing.Size(63, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 39;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // txtIncPr_p_no
            // 
            this.txtIncPr_p_no.AllowSpace = true;
            this.txtIncPr_p_no.AssociatedLookUpName = "";
            this.txtIncPr_p_no.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncPr_p_no.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncPr_p_no.ContinuationTextBox = null;
            this.txtIncPr_p_no.CustomEnabled = true;
            this.txtIncPr_p_no.DataFieldMapping = "PR_IN_NO";
            this.txtIncPr_p_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncPr_p_no.GetRecordsOnUpDownKeys = false;
            this.txtIncPr_p_no.IsDate = false;
            this.txtIncPr_p_no.Location = new System.Drawing.Point(354, 105);
            this.txtIncPr_p_no.Name = "txtIncPr_p_no";
            this.txtIncPr_p_no.NumberFormat = "###,###,##0.00";
            this.txtIncPr_p_no.Postfix = "";
            this.txtIncPr_p_no.Prefix = "";
            this.txtIncPr_p_no.Size = new System.Drawing.Size(64, 20);
            this.txtIncPr_p_no.SkipValidation = false;
            this.txtIncPr_p_no.TabIndex = 38;
            this.txtIncPr_p_no.TextType = CrplControlLibrary.TextType.String;
            this.txtIncPr_p_no.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(282, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(217, 13);
            this.label23.TabIndex = 37;
            this.label23.Text = "[I]ncrement [A]djustment [P]romotion ";
            // 
            // dtLastApp
            // 
            this.dtLastApp.CustomEnabled = true;
            this.dtLastApp.CustomFormat = "dd/MM/yyyy";
            this.dtLastApp.DataFieldMapping = "PR_LAST_APP";
            this.dtLastApp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtLastApp.HasChanges = true;
            this.dtLastApp.Location = new System.Drawing.Point(191, 158);
            this.dtLastApp.Name = "dtLastApp";
            this.dtLastApp.NullValue = " ";
            this.dtLastApp.Size = new System.Drawing.Size(100, 20);
            this.dtLastApp.TabIndex = 36;
            this.dtLastApp.Value = new System.DateTime(2011, 1, 17, 0, 0, 0, 0);
            // 
            // dtNextApp
            // 
            this.dtNextApp.CustomEnabled = true;
            this.dtNextApp.CustomFormat = "dd/MM/yyyy";
            this.dtNextApp.DataFieldMapping = "PR_NEXT_APP";
            this.dtNextApp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNextApp.HasChanges = true;
            this.dtNextApp.Location = new System.Drawing.Point(191, 185);
            this.dtNextApp.Name = "dtNextApp";
            this.dtNextApp.NullValue = " ";
            this.dtNextApp.Size = new System.Drawing.Size(100, 20);
            this.dtNextApp.TabIndex = 35;
            this.dtNextApp.Value = new System.DateTime(2011, 1, 15, 0, 0, 0, 0);
            this.dtNextApp.Validating += new System.ComponentModel.CancelEventHandler(this.dtNextApp_Validating);
            // 
            // txtPR_ANNUAL_PREVIOUS1
            // 
            this.txtPR_ANNUAL_PREVIOUS1.AllowSpace = true;
            this.txtPR_ANNUAL_PREVIOUS1.AssociatedLookUpName = "";
            this.txtPR_ANNUAL_PREVIOUS1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_ANNUAL_PREVIOUS1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_ANNUAL_PREVIOUS1.ContinuationTextBox = null;
            this.txtPR_ANNUAL_PREVIOUS1.CustomEnabled = false;
            this.txtPR_ANNUAL_PREVIOUS1.DataFieldMapping = "PR_ANNUAL_PREVIOUS";
            this.txtPR_ANNUAL_PREVIOUS1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_ANNUAL_PREVIOUS1.GetRecordsOnUpDownKeys = false;
            this.txtPR_ANNUAL_PREVIOUS1.IsDate = false;
            this.txtPR_ANNUAL_PREVIOUS1.Location = new System.Drawing.Point(494, 52);
            this.txtPR_ANNUAL_PREVIOUS1.Name = "txtPR_ANNUAL_PREVIOUS1";
            this.txtPR_ANNUAL_PREVIOUS1.NumberFormat = "###,###,##0.00";
            this.txtPR_ANNUAL_PREVIOUS1.Postfix = "";
            this.txtPR_ANNUAL_PREVIOUS1.Prefix = "";
            this.txtPR_ANNUAL_PREVIOUS1.Size = new System.Drawing.Size(72, 20);
            this.txtPR_ANNUAL_PREVIOUS1.SkipValidation = false;
            this.txtPR_ANNUAL_PREVIOUS1.TabIndex = 34;
            this.txtPR_ANNUAL_PREVIOUS1.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_ANNUAL_PREVIOUS1.Visible = false;
            // 
            // txtPR_DESIG_PREVIOUS
            // 
            this.txtPR_DESIG_PREVIOUS.AllowSpace = true;
            this.txtPR_DESIG_PREVIOUS.AssociatedLookUpName = "";
            this.txtPR_DESIG_PREVIOUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESIG_PREVIOUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESIG_PREVIOUS.ContinuationTextBox = null;
            this.txtPR_DESIG_PREVIOUS.CustomEnabled = false;
            this.txtPR_DESIG_PREVIOUS.DataFieldMapping = "PR_DESIG_PREVIOUS";
            this.txtPR_DESIG_PREVIOUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESIG_PREVIOUS.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESIG_PREVIOUS.IsDate = false;
            this.txtPR_DESIG_PREVIOUS.Location = new System.Drawing.Point(494, 26);
            this.txtPR_DESIG_PREVIOUS.Name = "txtPR_DESIG_PREVIOUS";
            this.txtPR_DESIG_PREVIOUS.NumberFormat = "###,###,##0.00";
            this.txtPR_DESIG_PREVIOUS.Postfix = "";
            this.txtPR_DESIG_PREVIOUS.Prefix = "";
            this.txtPR_DESIG_PREVIOUS.Size = new System.Drawing.Size(72, 20);
            this.txtPR_DESIG_PREVIOUS.SkipValidation = false;
            this.txtPR_DESIG_PREVIOUS.TabIndex = 33;
            this.txtPR_DESIG_PREVIOUS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_DESIG_PREVIOUS.Visible = false;
            // 
            // txtPR_LEVEL_PREVIOUS
            // 
            this.txtPR_LEVEL_PREVIOUS.AllowSpace = true;
            this.txtPR_LEVEL_PREVIOUS.AssociatedLookUpName = "";
            this.txtPR_LEVEL_PREVIOUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_LEVEL_PREVIOUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_LEVEL_PREVIOUS.ContinuationTextBox = null;
            this.txtPR_LEVEL_PREVIOUS.CustomEnabled = false;
            this.txtPR_LEVEL_PREVIOUS.DataFieldMapping = "PR_LEVEL_PREVIOUS";
            this.txtPR_LEVEL_PREVIOUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_LEVEL_PREVIOUS.GetRecordsOnUpDownKeys = false;
            this.txtPR_LEVEL_PREVIOUS.IsDate = false;
            this.txtPR_LEVEL_PREVIOUS.Location = new System.Drawing.Point(494, 0);
            this.txtPR_LEVEL_PREVIOUS.Name = "txtPR_LEVEL_PREVIOUS";
            this.txtPR_LEVEL_PREVIOUS.NumberFormat = "###,###,##0.00";
            this.txtPR_LEVEL_PREVIOUS.Postfix = "";
            this.txtPR_LEVEL_PREVIOUS.Prefix = "";
            this.txtPR_LEVEL_PREVIOUS.Size = new System.Drawing.Size(72, 20);
            this.txtPR_LEVEL_PREVIOUS.SkipValidation = false;
            this.txtPR_LEVEL_PREVIOUS.TabIndex = 32;
            this.txtPR_LEVEL_PREVIOUS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_LEVEL_PREVIOUS.Visible = false;
            // 
            // txtnew_branch
            // 
            this.txtnew_branch.AllowSpace = true;
            this.txtnew_branch.AssociatedLookUpName = "";
            this.txtnew_branch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnew_branch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnew_branch.ContinuationTextBox = null;
            this.txtnew_branch.CustomEnabled = false;
            this.txtnew_branch.DataFieldMapping = "pr_new_branch";
            this.txtnew_branch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnew_branch.GetRecordsOnUpDownKeys = false;
            this.txtnew_branch.IsDate = false;
            this.txtnew_branch.Location = new System.Drawing.Point(424, 78);
            this.txtnew_branch.Name = "txtnew_branch";
            this.txtnew_branch.NumberFormat = "###,###,##0.00";
            this.txtnew_branch.Postfix = "";
            this.txtnew_branch.Prefix = "";
            this.txtnew_branch.Size = new System.Drawing.Size(64, 20);
            this.txtnew_branch.SkipValidation = false;
            this.txtnew_branch.TabIndex = 31;
            this.txtnew_branch.TextType = CrplControlLibrary.TextType.String;
            this.txtnew_branch.Visible = false;
            // 
            // txtPR_FINAL
            // 
            this.txtPR_FINAL.AllowSpace = true;
            this.txtPR_FINAL.AssociatedLookUpName = "";
            this.txtPR_FINAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_FINAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_FINAL.ContinuationTextBox = null;
            this.txtPR_FINAL.CustomEnabled = false;
            this.txtPR_FINAL.DataFieldMapping = "PR_FINAL";
            this.txtPR_FINAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_FINAL.GetRecordsOnUpDownKeys = false;
            this.txtPR_FINAL.IsDate = false;
            this.txtPR_FINAL.Location = new System.Drawing.Point(500, 78);
            this.txtPR_FINAL.Name = "txtPR_FINAL";
            this.txtPR_FINAL.NumberFormat = "###,###,##0.00";
            this.txtPR_FINAL.Postfix = "";
            this.txtPR_FINAL.Prefix = "";
            this.txtPR_FINAL.Size = new System.Drawing.Size(64, 20);
            this.txtPR_FINAL.SkipValidation = false;
            this.txtPR_FINAL.TabIndex = 30;
            this.txtPR_FINAL.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_FINAL.Visible = false;
            // 
            // txtPR_CURRENT_PREVIOUS
            // 
            this.txtPR_CURRENT_PREVIOUS.AllowSpace = true;
            this.txtPR_CURRENT_PREVIOUS.AssociatedLookUpName = "";
            this.txtPR_CURRENT_PREVIOUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_CURRENT_PREVIOUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_CURRENT_PREVIOUS.ContinuationTextBox = null;
            this.txtPR_CURRENT_PREVIOUS.CustomEnabled = false;
            this.txtPR_CURRENT_PREVIOUS.DataFieldMapping = "PR_CURRENT_PREVIOUS";
            this.txtPR_CURRENT_PREVIOUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_CURRENT_PREVIOUS.GetRecordsOnUpDownKeys = false;
            this.txtPR_CURRENT_PREVIOUS.IsDate = false;
            this.txtPR_CURRENT_PREVIOUS.Location = new System.Drawing.Point(354, 78);
            this.txtPR_CURRENT_PREVIOUS.Name = "txtPR_CURRENT_PREVIOUS";
            this.txtPR_CURRENT_PREVIOUS.NumberFormat = "###,###,##0.00";
            this.txtPR_CURRENT_PREVIOUS.Postfix = "";
            this.txtPR_CURRENT_PREVIOUS.Prefix = "";
            this.txtPR_CURRENT_PREVIOUS.Size = new System.Drawing.Size(64, 20);
            this.txtPR_CURRENT_PREVIOUS.SkipValidation = false;
            this.txtPR_CURRENT_PREVIOUS.TabIndex = 29;
            this.txtPR_CURRENT_PREVIOUS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_CURRENT_PREVIOUS.Visible = false;
            // 
            // txtPR_FUNCT_2_PREVIOUS
            // 
            this.txtPR_FUNCT_2_PREVIOUS.AllowSpace = true;
            this.txtPR_FUNCT_2_PREVIOUS.AssociatedLookUpName = "";
            this.txtPR_FUNCT_2_PREVIOUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_FUNCT_2_PREVIOUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_FUNCT_2_PREVIOUS.ContinuationTextBox = null;
            this.txtPR_FUNCT_2_PREVIOUS.CustomEnabled = false;
            this.txtPR_FUNCT_2_PREVIOUS.DataFieldMapping = "PR_FUNCT_2_PREVIOUS";
            this.txtPR_FUNCT_2_PREVIOUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_FUNCT_2_PREVIOUS.GetRecordsOnUpDownKeys = false;
            this.txtPR_FUNCT_2_PREVIOUS.IsDate = false;
            this.txtPR_FUNCT_2_PREVIOUS.Location = new System.Drawing.Point(354, 52);
            this.txtPR_FUNCT_2_PREVIOUS.Name = "txtPR_FUNCT_2_PREVIOUS";
            this.txtPR_FUNCT_2_PREVIOUS.NumberFormat = "###,###,##0.00";
            this.txtPR_FUNCT_2_PREVIOUS.Postfix = "";
            this.txtPR_FUNCT_2_PREVIOUS.Prefix = "";
            this.txtPR_FUNCT_2_PREVIOUS.Size = new System.Drawing.Size(64, 20);
            this.txtPR_FUNCT_2_PREVIOUS.SkipValidation = false;
            this.txtPR_FUNCT_2_PREVIOUS.TabIndex = 28;
            this.txtPR_FUNCT_2_PREVIOUS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_FUNCT_2_PREVIOUS.Visible = false;
            // 
            // txtPR_FUNCT_1_PREVIOUS
            // 
            this.txtPR_FUNCT_1_PREVIOUS.AllowSpace = true;
            this.txtPR_FUNCT_1_PREVIOUS.AssociatedLookUpName = "";
            this.txtPR_FUNCT_1_PREVIOUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_FUNCT_1_PREVIOUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_FUNCT_1_PREVIOUS.ContinuationTextBox = null;
            this.txtPR_FUNCT_1_PREVIOUS.CustomEnabled = false;
            this.txtPR_FUNCT_1_PREVIOUS.DataFieldMapping = "PR_FUNCT_1_PREVIOUS";
            this.txtPR_FUNCT_1_PREVIOUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_FUNCT_1_PREVIOUS.GetRecordsOnUpDownKeys = false;
            this.txtPR_FUNCT_1_PREVIOUS.IsDate = false;
            this.txtPR_FUNCT_1_PREVIOUS.Location = new System.Drawing.Point(354, 3);
            this.txtPR_FUNCT_1_PREVIOUS.Name = "txtPR_FUNCT_1_PREVIOUS";
            this.txtPR_FUNCT_1_PREVIOUS.NumberFormat = "###,###,##0.00";
            this.txtPR_FUNCT_1_PREVIOUS.Postfix = "";
            this.txtPR_FUNCT_1_PREVIOUS.Prefix = "";
            this.txtPR_FUNCT_1_PREVIOUS.Size = new System.Drawing.Size(64, 20);
            this.txtPR_FUNCT_1_PREVIOUS.SkipValidation = false;
            this.txtPR_FUNCT_1_PREVIOUS.TabIndex = 27;
            this.txtPR_FUNCT_1_PREVIOUS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_FUNCT_1_PREVIOUS.Visible = false;
            // 
            // txtPR_FUNCT_2_PRESENT
            // 
            this.txtPR_FUNCT_2_PRESENT.AllowSpace = true;
            this.txtPR_FUNCT_2_PRESENT.AssociatedLookUpName = "";
            this.txtPR_FUNCT_2_PRESENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_FUNCT_2_PRESENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_FUNCT_2_PRESENT.ContinuationTextBox = null;
            this.txtPR_FUNCT_2_PRESENT.CustomEnabled = false;
            this.txtPR_FUNCT_2_PRESENT.DataFieldMapping = "PR_FUNCT_2_PRESENT";
            this.txtPR_FUNCT_2_PRESENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_FUNCT_2_PRESENT.GetRecordsOnUpDownKeys = false;
            this.txtPR_FUNCT_2_PRESENT.IsDate = false;
            this.txtPR_FUNCT_2_PRESENT.Location = new System.Drawing.Point(424, 3);
            this.txtPR_FUNCT_2_PRESENT.Name = "txtPR_FUNCT_2_PRESENT";
            this.txtPR_FUNCT_2_PRESENT.NumberFormat = "###,###,##0.00";
            this.txtPR_FUNCT_2_PRESENT.Postfix = "";
            this.txtPR_FUNCT_2_PRESENT.Prefix = "";
            this.txtPR_FUNCT_2_PRESENT.Size = new System.Drawing.Size(64, 20);
            this.txtPR_FUNCT_2_PRESENT.SkipValidation = false;
            this.txtPR_FUNCT_2_PRESENT.TabIndex = 24;
            this.txtPR_FUNCT_2_PRESENT.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_FUNCT_2_PRESENT.Visible = false;
            // 
            // txtPR_FUNCT_1_PRESENT
            // 
            this.txtPR_FUNCT_1_PRESENT.AllowSpace = true;
            this.txtPR_FUNCT_1_PRESENT.AssociatedLookUpName = "";
            this.txtPR_FUNCT_1_PRESENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_FUNCT_1_PRESENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_FUNCT_1_PRESENT.ContinuationTextBox = null;
            this.txtPR_FUNCT_1_PRESENT.CustomEnabled = false;
            this.txtPR_FUNCT_1_PRESENT.DataFieldMapping = "PR_FUNCT_1_PRESENT";
            this.txtPR_FUNCT_1_PRESENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_FUNCT_1_PRESENT.GetRecordsOnUpDownKeys = false;
            this.txtPR_FUNCT_1_PRESENT.IsDate = false;
            this.txtPR_FUNCT_1_PRESENT.Location = new System.Drawing.Point(354, 26);
            this.txtPR_FUNCT_1_PRESENT.Name = "txtPR_FUNCT_1_PRESENT";
            this.txtPR_FUNCT_1_PRESENT.NumberFormat = "###,###,##0.00";
            this.txtPR_FUNCT_1_PRESENT.Postfix = "";
            this.txtPR_FUNCT_1_PRESENT.Prefix = "";
            this.txtPR_FUNCT_1_PRESENT.Size = new System.Drawing.Size(64, 20);
            this.txtPR_FUNCT_1_PRESENT.SkipValidation = false;
            this.txtPR_FUNCT_1_PRESENT.TabIndex = 23;
            this.txtPR_FUNCT_1_PRESENT.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_FUNCT_1_PRESENT.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 192);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(167, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "10) Next Appraisal             :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(7, 218);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(166, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "11) Remarks                     :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(357, 140);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Previous Package   :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(166, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "9) Last Appraisal               :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 138);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(168, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "8) New Annual Package     :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(167, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "7) Increment %                  :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(168, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "6) Increment Amount          :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(170, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "5) Ranking && Ranking Date :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "4) Effective from               :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "3) Increment Type             :";
            // 
            // dtPR_RANKING_DATE
            // 
            this.dtPR_RANKING_DATE.CustomEnabled = true;
            this.dtPR_RANKING_DATE.CustomFormat = "dd/MM/yyyy";
            this.dtPR_RANKING_DATE.DataFieldMapping = "PR_RANKING_DATE";
            this.dtPR_RANKING_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPR_RANKING_DATE.HasChanges = true;
            this.dtPR_RANKING_DATE.Location = new System.Drawing.Point(191, 52);
            this.dtPR_RANKING_DATE.Name = "dtPR_RANKING_DATE";
            this.dtPR_RANKING_DATE.NullValue = " ";
            this.dtPR_RANKING_DATE.Size = new System.Drawing.Size(100, 20);
            this.dtPR_RANKING_DATE.TabIndex = 5;
            this.dtPR_RANKING_DATE.Value = new System.DateTime(2011, 1, 14, 0, 0, 0, 0);
            this.dtPR_RANKING_DATE.Validating += new System.ComponentModel.CancelEventHandler(this.dtPR_RANKING_DATE_Validating);
            // 
            // txtPR_RANK
            // 
            this.txtPR_RANK.AllowSpace = true;
            this.txtPR_RANK.AssociatedLookUpName = "";
            this.txtPR_RANK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_RANK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_RANK.ContinuationTextBox = null;
            this.txtPR_RANK.CustomEnabled = true;
            this.txtPR_RANK.DataFieldMapping = "PR_RANK";
            this.txtPR_RANK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_RANK.GetRecordsOnUpDownKeys = false;
            this.txtPR_RANK.IsDate = false;
            this.txtPR_RANK.Location = new System.Drawing.Point(297, 52);
            this.txtPR_RANK.MaxLength = 1;
            this.txtPR_RANK.Name = "txtPR_RANK";
            this.txtPR_RANK.NumberFormat = "###,###,##0.00";
            this.txtPR_RANK.Postfix = "";
            this.txtPR_RANK.Prefix = "";
            this.txtPR_RANK.Size = new System.Drawing.Size(26, 20);
            this.txtPR_RANK.SkipValidation = false;
            this.txtPR_RANK.TabIndex = 6;
            this.txtPR_RANK.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_RANK.Validating += new System.ComponentModel.CancelEventHandler(this.txtPR_RANK_Validating);
            // 
            // txtPR_REMARKS
            // 
            this.txtPR_REMARKS.AllowSpace = true;
            this.txtPR_REMARKS.AssociatedLookUpName = "";
            this.txtPR_REMARKS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_REMARKS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_REMARKS.ContinuationTextBox = null;
            this.txtPR_REMARKS.CustomEnabled = true;
            this.txtPR_REMARKS.DataFieldMapping = "PR_REMARKS";
            this.txtPR_REMARKS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_REMARKS.GetRecordsOnUpDownKeys = false;
            this.txtPR_REMARKS.IsDate = false;
            this.txtPR_REMARKS.IsRequired = true;
            this.txtPR_REMARKS.Location = new System.Drawing.Point(191, 211);
            this.txtPR_REMARKS.MaxLength = 50;
            this.txtPR_REMARKS.Name = "txtPR_REMARKS";
            this.txtPR_REMARKS.NumberFormat = "###,###,##0.00";
            this.txtPR_REMARKS.Postfix = "";
            this.txtPR_REMARKS.Prefix = "";
            this.txtPR_REMARKS.Size = new System.Drawing.Size(398, 20);
            this.txtPR_REMARKS.SkipValidation = false;
            this.txtPR_REMARKS.TabIndex = 38;
            this.txtPR_REMARKS.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_REMARKS, "<F10> To Save <F6> Exit W/o Save");
            this.txtPR_REMARKS.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPR_REMARKS_KeyDown);
            this.txtPR_REMARKS.Validating += new System.ComponentModel.CancelEventHandler(this.txtPR_REMARKS_Validating);
            // 
            // txtWP_pre
            // 
            this.txtWP_pre.AllowSpace = true;
            this.txtWP_pre.AssociatedLookUpName = "";
            this.txtWP_pre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWP_pre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtWP_pre.ContinuationTextBox = null;
            this.txtWP_pre.CustomEnabled = true;
            this.txtWP_pre.DataFieldMapping = "";
            this.txtWP_pre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWP_pre.GetRecordsOnUpDownKeys = false;
            this.txtWP_pre.IsDate = false;
            this.txtWP_pre.Location = new System.Drawing.Point(489, 133);
            this.txtWP_pre.MaxLength = 11;
            this.txtWP_pre.Name = "txtWP_pre";
            this.txtWP_pre.NumberFormat = "###,###,###";
            this.txtWP_pre.Postfix = "";
            this.txtWP_pre.Prefix = "";
            this.txtWP_pre.ReadOnly = true;
            this.txtWP_pre.Size = new System.Drawing.Size(100, 20);
            this.txtWP_pre.SkipValidation = false;
            this.txtWP_pre.TabIndex = 6;
            this.txtWP_pre.TabStop = false;
            this.txtWP_pre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWP_pre.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtIncrementPer
            // 
            this.txtIncrementPer.AllowSpace = true;
            this.txtIncrementPer.AssociatedLookUpName = "";
            this.txtIncrementPer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncrementPer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncrementPer.ContinuationTextBox = null;
            this.txtIncrementPer.CustomEnabled = true;
            this.txtIncrementPer.DataFieldMapping = "PR_INC_PER";
            this.txtIncrementPer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncrementPer.GetRecordsOnUpDownKeys = false;
            this.txtIncrementPer.IsDate = false;
            this.txtIncrementPer.Location = new System.Drawing.Point(191, 107);
            this.txtIncrementPer.MaxLength = 7;
            this.txtIncrementPer.Name = "txtIncrementPer";
            this.txtIncrementPer.NumberFormat = "0.00";
            this.txtIncrementPer.Postfix = "";
            this.txtIncrementPer.Prefix = "";
            this.txtIncrementPer.Size = new System.Drawing.Size(100, 20);
            this.txtIncrementPer.SkipValidation = false;
            this.txtIncrementPer.TabIndex = 8;
            this.txtIncrementPer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIncrementPer.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtPR_ANNUAL_PRESENT
            // 
            this.txtPR_ANNUAL_PRESENT.AllowSpace = true;
            this.txtPR_ANNUAL_PRESENT.AssociatedLookUpName = "";
            this.txtPR_ANNUAL_PRESENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_ANNUAL_PRESENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_ANNUAL_PRESENT.ContinuationTextBox = null;
            this.txtPR_ANNUAL_PRESENT.CustomEnabled = true;
            this.txtPR_ANNUAL_PRESENT.DataFieldMapping = "PR_ANNUAL_PRESENT";
            this.txtPR_ANNUAL_PRESENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_ANNUAL_PRESENT.GetRecordsOnUpDownKeys = false;
            this.txtPR_ANNUAL_PRESENT.IsDate = false;
            this.txtPR_ANNUAL_PRESENT.Location = new System.Drawing.Point(191, 133);
            this.txtPR_ANNUAL_PRESENT.MaxLength = 11;
            this.txtPR_ANNUAL_PRESENT.Name = "txtPR_ANNUAL_PRESENT";
            this.txtPR_ANNUAL_PRESENT.NumberFormat = "###,###,###";
            this.txtPR_ANNUAL_PRESENT.Postfix = "";
            this.txtPR_ANNUAL_PRESENT.Prefix = "";
            this.txtPR_ANNUAL_PRESENT.Size = new System.Drawing.Size(100, 20);
            this.txtPR_ANNUAL_PRESENT.SkipValidation = false;
            this.txtPR_ANNUAL_PRESENT.TabIndex = 9;
            this.txtPR_ANNUAL_PRESENT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPR_ANNUAL_PRESENT.TextType = CrplControlLibrary.TextType.Amount;
            this.txtPR_ANNUAL_PRESENT.Validating += new System.ComponentModel.CancelEventHandler(this.txtPR_ANNUAL_PRESENT_Validating);
            // 
            // txtIncAmonut
            // 
            this.txtIncAmonut.AllowSpace = true;
            this.txtIncAmonut.AssociatedLookUpName = "";
            this.txtIncAmonut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncAmonut.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncAmonut.ContinuationTextBox = null;
            this.txtIncAmonut.CustomEnabled = true;
            this.txtIncAmonut.DataFieldMapping = "PR_INC_AMT";
            this.txtIncAmonut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncAmonut.GetRecordsOnUpDownKeys = false;
            this.txtIncAmonut.IsDate = false;
            this.txtIncAmonut.IsRequired = true;
            this.txtIncAmonut.Location = new System.Drawing.Point(191, 81);
            this.txtIncAmonut.MaxLength = 7;
            this.txtIncAmonut.Name = "txtIncAmonut";
            this.txtIncAmonut.NumberFormat = "###,###,##0.00";
            this.txtIncAmonut.Postfix = "";
            this.txtIncAmonut.Prefix = "";
            this.txtIncAmonut.Size = new System.Drawing.Size(100, 20);
            this.txtIncAmonut.SkipValidation = false;
            this.txtIncAmonut.TabIndex = 7;
            this.txtIncAmonut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIncAmonut.TextType = CrplControlLibrary.TextType.Amount;
            this.txtIncAmonut.Validating += new System.ComponentModel.CancelEventHandler(this.txtIncAmonut_Validating);
            // 
            // txtPR_DESIG_PRESENT1
            // 
            this.txtPR_DESIG_PRESENT1.AllowSpace = true;
            this.txtPR_DESIG_PRESENT1.AssociatedLookUpName = "";
            this.txtPR_DESIG_PRESENT1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESIG_PRESENT1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESIG_PRESENT1.ContinuationTextBox = null;
            this.txtPR_DESIG_PRESENT1.CustomEnabled = false;
            this.txtPR_DESIG_PRESENT1.DataFieldMapping = "PR_DESIG_PRESENT";
            this.txtPR_DESIG_PRESENT1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESIG_PRESENT1.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESIG_PRESENT1.IsDate = false;
            this.txtPR_DESIG_PRESENT1.Location = new System.Drawing.Point(424, 52);
            this.txtPR_DESIG_PRESENT1.Name = "txtPR_DESIG_PRESENT1";
            this.txtPR_DESIG_PRESENT1.NumberFormat = "###,###,##0.00";
            this.txtPR_DESIG_PRESENT1.Postfix = "";
            this.txtPR_DESIG_PRESENT1.Prefix = "";
            this.txtPR_DESIG_PRESENT1.Size = new System.Drawing.Size(64, 20);
            this.txtPR_DESIG_PRESENT1.SkipValidation = false;
            this.txtPR_DESIG_PRESENT1.TabIndex = 21;
            this.txtPR_DESIG_PRESENT1.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_DESIG_PRESENT1.Visible = false;
            // 
            // txtPR_LEVEL_PRESENT1
            // 
            this.txtPR_LEVEL_PRESENT1.AllowSpace = true;
            this.txtPR_LEVEL_PRESENT1.AssociatedLookUpName = "";
            this.txtPR_LEVEL_PRESENT1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_LEVEL_PRESENT1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_LEVEL_PRESENT1.ContinuationTextBox = null;
            this.txtPR_LEVEL_PRESENT1.CustomEnabled = false;
            this.txtPR_LEVEL_PRESENT1.DataFieldMapping = "PR_LEVEL_PRESENT";
            this.txtPR_LEVEL_PRESENT1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_LEVEL_PRESENT1.GetRecordsOnUpDownKeys = false;
            this.txtPR_LEVEL_PRESENT1.IsDate = false;
            this.txtPR_LEVEL_PRESENT1.Location = new System.Drawing.Point(424, 26);
            this.txtPR_LEVEL_PRESENT1.Name = "txtPR_LEVEL_PRESENT1";
            this.txtPR_LEVEL_PRESENT1.NumberFormat = "###,###,##0.00";
            this.txtPR_LEVEL_PRESENT1.Postfix = "";
            this.txtPR_LEVEL_PRESENT1.Prefix = "";
            this.txtPR_LEVEL_PRESENT1.Size = new System.Drawing.Size(64, 20);
            this.txtPR_LEVEL_PRESENT1.SkipValidation = false;
            this.txtPR_LEVEL_PRESENT1.TabIndex = 11;
            this.txtPR_LEVEL_PRESENT1.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_LEVEL_PRESENT1.Visible = false;
            // 
            // txtIncremnttype
            // 
            this.txtIncremnttype.AllowSpace = true;
            this.txtIncremnttype.AssociatedLookUpName = "lbtnIncType";
            this.txtIncremnttype.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncremnttype.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncremnttype.ContinuationTextBox = null;
            this.txtIncremnttype.CustomEnabled = true;
            this.txtIncremnttype.DataFieldMapping = "PR_INC_TYPE";
            this.txtIncremnttype.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncremnttype.GetRecordsOnUpDownKeys = false;
            this.txtIncremnttype.IsDate = false;
            this.txtIncremnttype.IsRequired = true;
            this.txtIncremnttype.Location = new System.Drawing.Point(191, 3);
            this.txtIncremnttype.MaxLength = 1;
            this.txtIncremnttype.Name = "txtIncremnttype";
            this.txtIncremnttype.NumberFormat = "###,###,##0.00";
            this.txtIncremnttype.Postfix = "";
            this.txtIncremnttype.Prefix = "";
            this.txtIncremnttype.Size = new System.Drawing.Size(46, 20);
            this.txtIncremnttype.SkipValidation = false;
            this.txtIncremnttype.TabIndex = 3;
            this.txtIncremnttype.TextType = CrplControlLibrary.TextType.String;
            this.txtIncremnttype.Validating += new System.ComponentModel.CancelEventHandler(this.txtIncremnttype_Validating);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label19);
            this.pnlHead.Controls.Add(this.label13);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(12, 94);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(645, 93);
            this.pnlHead.TabIndex = 54;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(188, 58);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(235, 20);
            this.label19.TabIndex = 21;
            this.label19.Text = "(OFFICERS     STAFF)";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(169, 34);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(273, 18);
            this.label13.TabIndex = 20;
            this.label13.Text = "INCREMENTS / PROMOTIONS";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(188, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(235, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "PERSONNEL SYSTEM";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(500, 61);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(500, 36);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(457, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label15.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(441, 36);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label16.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(15, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(403, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 55;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Personnel_IncrementOfficers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(663, 609);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlPersonnel);
            this.Controls.Add(this.PnlIncPromotion);
            this.CurrentPanelBlock = "PnlIncPromotion";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Personnel_IncrementOfficers";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Personnel_IncrementOfficers";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_IncrementOfficers_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlIncPromotion, 0);
            this.Controls.SetChildIndex(this.PnlPersonnel, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlPersonnel.ResumeLayout(false);
            this.PnlPersonnel.PerformLayout();
            this.PnlIncPromotion.ResumeLayout(false);
            this.PnlIncPromotion.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlPersonnel;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlIncPromotion;
        private CrplControlLibrary.SLTextBox txtAnnualPresnt;
        private CrplControlLibrary.SLTextBox txtPR_DESIG_PRESENT;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtName;
        private CrplControlLibrary.SLTextBox txtPersonnel;
        private CrplControlLibrary.SLDatePicker dtPR_RANKING_DATE;
        private CrplControlLibrary.SLTextBox txtPR_RANK;
        private CrplControlLibrary.SLTextBox txtPR_REMARKS;
        private CrplControlLibrary.SLTextBox txtWP_pre;
        private CrplControlLibrary.SLTextBox txtIncrementPer;
        private CrplControlLibrary.SLTextBox txtPR_ANNUAL_PRESENT;
        private CrplControlLibrary.SLTextBox txtIncAmonut;
        private CrplControlLibrary.SLTextBox txtPR_DESIG_PRESENT1;
        private CrplControlLibrary.SLTextBox txtPR_LEVEL_PRESENT1;
        private CrplControlLibrary.SLTextBox txtIncremnttype;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtPR_FUNCT_2_PRESENT;
        private CrplControlLibrary.SLTextBox txtPR_FUNCT_1_PRESENT;
        private CrplControlLibrary.SLTextBox txtPR_ANNUAL_PREVIOUS1;
        private CrplControlLibrary.SLTextBox txtPR_DESIG_PREVIOUS;
        private CrplControlLibrary.SLTextBox txtPR_LEVEL_PREVIOUS;
        private CrplControlLibrary.SLTextBox txtnew_branch;
        private CrplControlLibrary.SLTextBox txtPR_FINAL;
        private CrplControlLibrary.SLTextBox txtPR_CURRENT_PREVIOUS;
        private CrplControlLibrary.SLTextBox txtPR_FUNCT_2_PREVIOUS;
        private CrplControlLibrary.SLTextBox txtPR_FUNCT_1_PREVIOUS;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.LookupButton lbPersonnel;
        private CrplControlLibrary.SLDatePicker dtNextApp;
        private CrplControlLibrary.SLDatePicker dtLastApp;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox txtIncPr_p_no;
        private CrplControlLibrary.SLTextBox txtID;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.LookupButton lbtnDate;
        private CrplControlLibrary.SLTextBox txtPNo;
        private CrplControlLibrary.LookupButton lbtnIncType;
        private CrplControlLibrary.SLTextBox txtPersonnelID;
        private CrplControlLibrary.SLTextBox txt_PR_EFFECTIVE;
    }
}