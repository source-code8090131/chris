namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_LeavesCarryForward
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_LeavesCarryForward));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlLeaveStatus = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVLEAVE_STATUS = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.LS_P_NO = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LC_PL_BAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LS_BAL_CF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LS_C_FORWARD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LS_CF_APPROVED = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LS_PL_BAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LS_CL_BAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LS_ML_BAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LS_SL_BAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlLeaveStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVLEAVE_STATUS)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(634, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(670, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 495);
            this.panel1.Size = new System.Drawing.Size(670, 60);
            // 
            // PnlLeaveStatus
            // 
            this.PnlLeaveStatus.ConcurrentPanels = null;
            this.PnlLeaveStatus.Controls.Add(this.DGVLEAVE_STATUS);
            this.PnlLeaveStatus.DataManager = null;
            this.PnlLeaveStatus.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlLeaveStatus.DependentPanels = null;
            this.PnlLeaveStatus.DisableDependentLoad = false;
            this.PnlLeaveStatus.EnableDelete = true;
            this.PnlLeaveStatus.EnableInsert = true;
            this.PnlLeaveStatus.EnableQuery = false;
            this.PnlLeaveStatus.EnableUpdate = true;
            this.PnlLeaveStatus.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LEAVE_STATUSCommand";
            this.PnlLeaveStatus.Location = new System.Drawing.Point(4, 178);
            this.PnlLeaveStatus.MasterPanel = null;
            this.PnlLeaveStatus.Name = "PnlLeaveStatus";
            this.PnlLeaveStatus.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlLeaveStatus.Size = new System.Drawing.Size(654, 314);
            this.PnlLeaveStatus.SPName = "CHRIS_SP_LEAVE_STATUS_MANAGER";
            this.PnlLeaveStatus.TabIndex = 0;
            // 
            // DGVLEAVE_STATUS
            // 
            this.DGVLEAVE_STATUS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVLEAVE_STATUS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LS_P_NO,
            this.FirstName,
            this.LC_PL_BAL,
            this.LS_BAL_CF,
            this.LS_C_FORWARD,
            this.LS_CF_APPROVED,
            this.LS_PL_BAL,
            this.LS_CL_BAL,
            this.LS_ML_BAL,
            this.LS_SL_BAL});
            this.DGVLEAVE_STATUS.ColumnToHide = null;
            this.DGVLEAVE_STATUS.ColumnWidth = null;
            this.DGVLEAVE_STATUS.CustomEnabled = true;
            this.DGVLEAVE_STATUS.DisplayColumnWrapper = null;
            this.DGVLEAVE_STATUS.GridDefaultRow = 0;
            this.DGVLEAVE_STATUS.Location = new System.Drawing.Point(6, 12);
            this.DGVLEAVE_STATUS.Name = "DGVLEAVE_STATUS";
            this.DGVLEAVE_STATUS.ReadOnlyColumns = null;
            this.DGVLEAVE_STATUS.RequiredColumns = "LS_P_NO";
            this.DGVLEAVE_STATUS.Size = new System.Drawing.Size(647, 302);
            this.DGVLEAVE_STATUS.SkippingColumns = null;
            this.DGVLEAVE_STATUS.TabIndex = 0;
            this.DGVLEAVE_STATUS.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVLEAVE_STATUS_CellLeave);
            this.DGVLEAVE_STATUS.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVLEAVE_STATUS_CellValidating);
            this.DGVLEAVE_STATUS.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVLEAVE_STATUS_CellClick);
            this.DGVLEAVE_STATUS.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DGVLEAVE_STATUS_EditingControlShowing);
            this.DGVLEAVE_STATUS.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVLEAVE_STATUS_CellEnter);
            // 
            // LS_P_NO
            // 
            this.LS_P_NO.ActionLOV = "Leave_Pr_p_no";
            this.LS_P_NO.ActionLOVExists = "Leave_Pr_p_no_Exists";
            this.LS_P_NO.AttachParentEntity = false;
            this.LS_P_NO.DataPropertyName = "LS_P_NO";
            dataGridViewCellStyle1.NullValue = null;
            this.LS_P_NO.DefaultCellStyle = dataGridViewCellStyle1;
            this.LS_P_NO.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LEAVE_STATUSCommand";
            this.LS_P_NO.HeaderText = "Personnel No.";
            this.LS_P_NO.LookUpTitle = null;
            this.LS_P_NO.LOVFieldMapping = "LS_P_NO";
            this.LS_P_NO.MaxInputLength = 6;
            this.LS_P_NO.Name = "LS_P_NO";
            this.LS_P_NO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LS_P_NO.SearchColumn = "LS_P_NO";
            this.LS_P_NO.SkipValidationOnLeave = false;
            this.LS_P_NO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LS_P_NO.SpName = "CHRIS_SP_LEAVE_STATUS_MANAGER";
            // 
            // FirstName
            // 
            this.FirstName.DataPropertyName = "FirstName";
            this.FirstName.HeaderText = "Name";
            this.FirstName.MaxInputLength = 20;
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            this.FirstName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FirstName.Width = 210;
            // 
            // LC_PL_BAL
            // 
            this.LC_PL_BAL.DataPropertyName = "LC_PL_BAL";
            this.LC_PL_BAL.HeaderText = "Prv.Year      PL Balance";
            this.LC_PL_BAL.MaxInputLength = 2;
            this.LC_PL_BAL.Name = "LC_PL_BAL";
            this.LC_PL_BAL.ReadOnly = true;
            this.LC_PL_BAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LS_BAL_CF
            // 
            this.LS_BAL_CF.DataPropertyName = "LS_BAL_CF";
            this.LS_BAL_CF.HeaderText = "Carry Forward (Y/N)";
            this.LS_BAL_CF.MaxInputLength = 1;
            this.LS_BAL_CF.Name = "LS_BAL_CF";
            this.LS_BAL_CF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LS_C_FORWARD
            // 
            this.LS_C_FORWARD.DataPropertyName = "LS_C_FORWARD";
            this.LS_C_FORWARD.HeaderText = "No. Of Leaves  Carry Forward";
            this.LS_C_FORWARD.MaxInputLength = 2;
            this.LS_C_FORWARD.Name = "LS_C_FORWARD";
            this.LS_C_FORWARD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LS_CF_APPROVED
            // 
            this.LS_CF_APPROVED.DataPropertyName = "LS_CF_APPROVED";
            this.LS_CF_APPROVED.HeaderText = "Approved (Y/N)";
            this.LS_CF_APPROVED.MaxInputLength = 1;
            this.LS_CF_APPROVED.Name = "LS_CF_APPROVED";
            this.LS_CF_APPROVED.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LS_PL_BAL
            // 
            this.LS_PL_BAL.DataPropertyName = "LS_PL_BAL";
            this.LS_PL_BAL.HeaderText = "LS_PL_BAL";
            this.LS_PL_BAL.MaxInputLength = 2;
            this.LS_PL_BAL.Name = "LS_PL_BAL";
            this.LS_PL_BAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LS_PL_BAL.Visible = false;
            // 
            // LS_CL_BAL
            // 
            this.LS_CL_BAL.DataPropertyName = "LS_CL_BAL";
            this.LS_CL_BAL.HeaderText = "LS_CL_BAL";
            this.LS_CL_BAL.MaxInputLength = 2;
            this.LS_CL_BAL.Name = "LS_CL_BAL";
            this.LS_CL_BAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LS_CL_BAL.Visible = false;
            // 
            // LS_ML_BAL
            // 
            this.LS_ML_BAL.DataPropertyName = "LS_ML_BAL";
            this.LS_ML_BAL.HeaderText = "LS_ML_BAL";
            this.LS_ML_BAL.MaxInputLength = 2;
            this.LS_ML_BAL.Name = "LS_ML_BAL";
            this.LS_ML_BAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LS_ML_BAL.Visible = false;
            // 
            // LS_SL_BAL
            // 
            this.LS_SL_BAL.DataPropertyName = "LS_SL_BAL";
            this.LS_SL_BAL.HeaderText = "LS_SL_BAL";
            this.LS_SL_BAL.MaxInputLength = 2;
            this.LS_SL_BAL.Name = "LS_SL_BAL";
            this.LS_SL_BAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LS_SL_BAL.Visible = false;
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(4, 85);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(654, 75);
            this.pnlHead.TabIndex = 52;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(169, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "LEAVES APPROVALS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(188, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(235, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Personnel System";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(500, 52);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(500, 27);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(457, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(449, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label16.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 52);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 26);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 52);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(15, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label18.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(436, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "User Name :";
            // 
            // CHRIS_Personnel_LeavesCarryForward
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 555);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlLeaveStatus);
            this.CurrentPanelBlock = "PnlLeaveStatus";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Personnel_LeavesCarryForward";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF2Option = true;
            this.ShowF5Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Personnel_Leaves_Carry_Forward";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlLeaveStatus, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlLeaveStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVLEAVE_STATUS)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlLeaveStatus;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVLEAVE_STATUS;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label1;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn LS_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LC_PL_BAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn LS_BAL_CF;
        private System.Windows.Forms.DataGridViewTextBoxColumn LS_C_FORWARD;
        private System.Windows.Forms.DataGridViewTextBoxColumn LS_CF_APPROVED;
        private System.Windows.Forms.DataGridViewTextBoxColumn LS_PL_BAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn LS_CL_BAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn LS_ML_BAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn LS_SL_BAL;
    }
}