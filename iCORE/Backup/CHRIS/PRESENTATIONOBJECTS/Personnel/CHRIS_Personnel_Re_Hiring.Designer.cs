namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_Re_Hiring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_Re_Hiring));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVPersonnel = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.REH_PR_NO = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.REH_OLD_PR_NO = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.LC_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REH_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LC_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlPersonnel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPersonnel)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(590, 0);
            this.txtOption.Validating += new System.ComponentModel.CancelEventHandler(this.txtOption_Validating);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(626, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 513);
            this.panel1.Size = new System.Drawing.Size(626, 60);
            // 
            // PnlPersonnel
            // 
            this.PnlPersonnel.ConcurrentPanels = null;
            this.PnlPersonnel.Controls.Add(this.DGVPersonnel);
            this.PnlPersonnel.DataManager = null;
            this.PnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPersonnel.DependentPanels = null;
            this.PnlPersonnel.DisableDependentLoad = false;
            this.PnlPersonnel.EnableDelete = true;
            this.PnlPersonnel.EnableInsert = true;
            this.PnlPersonnel.EnableQuery = false;
            this.PnlPersonnel.EnableUpdate = true;
            this.PnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.REHAIRCommand";
            this.PnlPersonnel.Location = new System.Drawing.Point(12, 192);
            this.PnlPersonnel.MasterPanel = null;
            this.PnlPersonnel.Name = "PnlPersonnel";
            this.PnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPersonnel.Size = new System.Drawing.Size(602, 279);
            this.PnlPersonnel.SPName = "CHRIS_SP_REHAIR_MANAGER";
            this.PnlPersonnel.TabIndex = 0;
            // 
            // DGVPersonnel
            // 
            this.DGVPersonnel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVPersonnel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.REH_PR_NO,
            this.REH_OLD_PR_NO,
            this.LC_NAME,
            this.REH_DATE,
            this.LC_DATE,
            this.ID});
            this.DGVPersonnel.ColumnToHide = null;
            this.DGVPersonnel.ColumnWidth = null;
            this.DGVPersonnel.CustomEnabled = true;
            this.DGVPersonnel.DisplayColumnWrapper = null;
            this.DGVPersonnel.GridDefaultRow = 1;
            this.DGVPersonnel.Location = new System.Drawing.Point(6, 3);
            this.DGVPersonnel.Name = "DGVPersonnel";
            this.DGVPersonnel.ReadOnlyColumns = null;
            this.DGVPersonnel.RequiredColumns = "REH_OLD_PR_NO|REH_PR_NO|REH_DATE|LC_DATE";
            this.DGVPersonnel.Size = new System.Drawing.Size(593, 276);
            this.DGVPersonnel.SkippingColumns = null;
            this.DGVPersonnel.TabIndex = 0;
            this.DGVPersonnel.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVPersonnel_RowLeave);
            this.DGVPersonnel.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVPersonnel_CellValidating);
            this.DGVPersonnel.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVPersonnel_CellEndEdit);
            this.DGVPersonnel.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVPersonnel_RowValidated);
            // 
            // REH_PR_NO
            // 
            this.REH_PR_NO.ActionLOV = "Pr_No_Lov";
            this.REH_PR_NO.ActionLOVExists = "Pr_No_Lov_exists";
            this.REH_PR_NO.AttachParentEntity = false;
            this.REH_PR_NO.DataPropertyName = "REH_PR_NO";
            this.REH_PR_NO.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.REHAIRCommand";
            this.REH_PR_NO.HeaderText = "Personnel New";
            this.REH_PR_NO.LookUpTitle = null;
            this.REH_PR_NO.LOVFieldMapping = "REH_PR_NO";
            this.REH_PR_NO.Name = "REH_PR_NO";
            this.REH_PR_NO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.REH_PR_NO.SearchColumn = "REH_PR_NO";
            this.REH_PR_NO.SkipValidationOnLeave = false;
            this.REH_PR_NO.SpName = "CHRIS_SP_REHAIR_MANAGER";
            // 
            // REH_OLD_PR_NO
            // 
            this.REH_OLD_PR_NO.ActionLOV = "Pr_Old_Lov";
            this.REH_OLD_PR_NO.ActionLOVExists = "Pr_Old_Lov_Exists";
            this.REH_OLD_PR_NO.AttachParentEntity = false;
            this.REH_OLD_PR_NO.DataPropertyName = "REH_OLD_PR_NO";
            this.REH_OLD_PR_NO.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.REHAIRCommand";
            this.REH_OLD_PR_NO.HeaderText = "Number Old";
            this.REH_OLD_PR_NO.LookUpTitle = null;
            this.REH_OLD_PR_NO.LOVFieldMapping = "REH_OLD_PR_NO";
            this.REH_OLD_PR_NO.Name = "REH_OLD_PR_NO";
            this.REH_OLD_PR_NO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.REH_OLD_PR_NO.SearchColumn = "REH_OLD_PR_NO";
            this.REH_OLD_PR_NO.SkipValidationOnLeave = false;
            this.REH_OLD_PR_NO.SpName = "CHRIS_SP_REHAIR_MANAGER";
            // 
            // LC_NAME
            // 
            this.LC_NAME.DataPropertyName = "LC_NAME";
            this.LC_NAME.HeaderText = "Name";
            this.LC_NAME.Name = "LC_NAME";
            this.LC_NAME.Width = 150;
            // 
            // REH_DATE
            // 
            this.REH_DATE.DataPropertyName = "REH_DATE";
            dataGridViewCellStyle1.Format = "dd/MM/yyyy";
            dataGridViewCellStyle1.NullValue = null;
            this.REH_DATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.REH_DATE.HeaderText = "Rehire Date";
            this.REH_DATE.MinimumWidth = 10;
            this.REH_DATE.Name = "REH_DATE";
            // 
            // LC_DATE
            // 
            this.LC_DATE.DataPropertyName = "LC_DATE";
            dataGridViewCellStyle2.NullValue = null;
            this.LC_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.LC_DATE.HeaderText = "Confirmation Date";
            this.LC_DATE.MinimumWidth = 10;
            this.LC_DATE.Name = "LC_DATE";
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(12, 93);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(602, 93);
            this.pnlHead.TabIndex = 51;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(168, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "REHIRE SCREEN";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(188, 34);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(235, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Personnel System";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(499, 62);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(499, 37);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(456, 64);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(448, 38);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(15, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label18.Visible = false;
            // 
            // CHRIS_Personnel_Re_Hiring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 573);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlPersonnel);
            this.CurrentPanelBlock = "PnlPersonnel";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Personnel_Re_Hiring";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF8Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Personnel_Re_Hiring";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlPersonnel, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlPersonnel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVPersonnel)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlPersonnel;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVPersonnel;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn REH_PR_NO;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn REH_OLD_PR_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn LC_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn REH_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn LC_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}