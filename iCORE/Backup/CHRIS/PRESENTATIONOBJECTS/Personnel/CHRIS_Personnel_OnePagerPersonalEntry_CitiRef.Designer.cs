namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_OnePagerPersonalEntry_CitiRef
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlCitiReference = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtCity = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.cmbOvrLocal = new CrplControlLibrary.SLComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDesig = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbFTE = new CrplControlLibrary.SLComboBox();
            this.txtCountry = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.txtGroup = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDept = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRelation = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlCitiReference.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(638, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(674, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 165);
            this.panel1.Size = new System.Drawing.Size(674, 60);
            // 
            // pnlCitiReference
            // 
            this.pnlCitiReference.ConcurrentPanels = null;
            this.pnlCitiReference.Controls.Add(this.txtCity);
            this.pnlCitiReference.Controls.Add(this.label9);
            this.pnlCitiReference.Controls.Add(this.txtBranch);
            this.pnlCitiReference.Controls.Add(this.label8);
            this.pnlCitiReference.Controls.Add(this.cmbOvrLocal);
            this.pnlCitiReference.Controls.Add(this.label7);
            this.pnlCitiReference.Controls.Add(this.txtDesig);
            this.pnlCitiReference.Controls.Add(this.label3);
            this.pnlCitiReference.Controls.Add(this.label6);
            this.pnlCitiReference.Controls.Add(this.cmbFTE);
            this.pnlCitiReference.Controls.Add(this.txtCountry);
            this.pnlCitiReference.Controls.Add(this.label17);
            this.pnlCitiReference.Controls.Add(this.txtGroup);
            this.pnlCitiReference.Controls.Add(this.txtDept);
            this.pnlCitiReference.Controls.Add(this.label5);
            this.pnlCitiReference.Controls.Add(this.label4);
            this.pnlCitiReference.Controls.Add(this.label2);
            this.pnlCitiReference.Controls.Add(this.label1);
            this.pnlCitiReference.Controls.Add(this.txtName);
            this.pnlCitiReference.Controls.Add(this.txtRelation);
            this.pnlCitiReference.DataManager = null;
            this.pnlCitiReference.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlCitiReference.DependentPanels = null;
            this.pnlCitiReference.DisableDependentLoad = false;
            this.pnlCitiReference.EnableDelete = true;
            this.pnlCitiReference.EnableInsert = true;
            this.pnlCitiReference.EnableQuery = false;
            this.pnlCitiReference.EnableUpdate = true;
            this.pnlCitiReference.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CitiEmpRefPersonnelCommand";
            this.pnlCitiReference.Location = new System.Drawing.Point(12, 8);
            this.pnlCitiReference.MasterPanel = null;
            this.pnlCitiReference.Name = "pnlCitiReference";
            this.pnlCitiReference.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlCitiReference.Size = new System.Drawing.Size(650, 147);
            this.pnlCitiReference.SPName = "CHRIS_CITI_EMP_REF_ONEPAGER_MANAGER";
            this.pnlCitiReference.TabIndex = 10;
            // 
            // txtCity
            // 
            this.txtCity.AllowSpace = true;
            this.txtCity.AssociatedLookUpName = "";
            this.txtCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.ContinuationTextBox = null;
            this.txtCity.CustomEnabled = true;
            this.txtCity.DataFieldMapping = "CR_CITY";
            this.txtCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCity.GetRecordsOnUpDownKeys = false;
            this.txtCity.IsDate = false;
            this.txtCity.Location = new System.Drawing.Point(323, 102);
            this.txtCity.MaxLength = 30;
            this.txtCity.Name = "txtCity";
            this.txtCity.NumberFormat = "###,###,##0.00";
            this.txtCity.Postfix = "";
            this.txtCity.Prefix = "";
            this.txtCity.Size = new System.Drawing.Size(207, 20);
            this.txtCity.SkipValidation = false;
            this.txtCity.TabIndex = 9;
            this.txtCity.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(285, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 137;
            this.label9.Text = "City";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "CR_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(507, 76);
            this.txtBranch.MaxLength = 40;
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.Size = new System.Drawing.Size(120, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 7;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(451, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 135;
            this.label8.Text = "Branch";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbOvrLocal
            // 
            this.cmbOvrLocal.BusinessEntity = "";
            this.cmbOvrLocal.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbOvrLocal.CustomEnabled = true;
            this.cmbOvrLocal.DataFieldMapping = "CR_OVS_LCL";
            this.cmbOvrLocal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOvrLocal.FormattingEnabled = true;
            this.cmbOvrLocal.Items.AddRange(new object[] {
            "Local",
            "Overseas"});
            this.cmbOvrLocal.Location = new System.Drawing.Point(323, 73);
            this.cmbOvrLocal.LOVType = "";
            this.cmbOvrLocal.Name = "cmbOvrLocal";
            this.cmbOvrLocal.Size = new System.Drawing.Size(120, 21);
            this.cmbOvrLocal.SPName = "";
            this.cmbOvrLocal.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(216, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 133;
            this.label7.Text = "Overseas/Local";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDesig
            // 
            this.txtDesig.AllowSpace = true;
            this.txtDesig.AssociatedLookUpName = "";
            this.txtDesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesig.ContinuationTextBox = null;
            this.txtDesig.CustomEnabled = true;
            this.txtDesig.DataFieldMapping = "CR_DESIG";
            this.txtDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesig.GetRecordsOnUpDownKeys = false;
            this.txtDesig.IsDate = false;
            this.txtDesig.Location = new System.Drawing.Point(323, 45);
            this.txtDesig.MaxLength = 40;
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.NumberFormat = "###,###,##0.00";
            this.txtDesig.Postfix = "";
            this.txtDesig.Prefix = "";
            this.txtDesig.Size = new System.Drawing.Size(207, 20);
            this.txtDesig.SkipValidation = false;
            this.txtDesig.TabIndex = 4;
            this.txtDesig.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(274, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 130;
            this.label3.Text = "Desig";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(459, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 129;
            this.label6.Text = "Name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbFTE
            // 
            this.cmbFTE.BusinessEntity = "";
            this.cmbFTE.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbFTE.CustomEnabled = true;
            this.cmbFTE.DataFieldMapping = "CR_FTE_CONT";
            this.cmbFTE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFTE.FormattingEnabled = true;
            this.cmbFTE.Items.AddRange(new object[] {
            "FTE",
            "Contract"});
            this.cmbFTE.Location = new System.Drawing.Point(90, 18);
            this.cmbFTE.LOVType = "";
            this.cmbFTE.Name = "cmbFTE";
            this.cmbFTE.Size = new System.Drawing.Size(120, 21);
            this.cmbFTE.SPName = "";
            this.cmbFTE.TabIndex = 0;
            this.toolTip1.SetToolTip(this.cmbFTE, "Field Must be Entered .....");
            this.cmbFTE.Validating += new System.ComponentModel.CancelEventHandler(this.cmbFTE_Validating);
            // 
            // txtCountry
            // 
            this.txtCountry.AllowSpace = true;
            this.txtCountry.AssociatedLookUpName = "";
            this.txtCountry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountry.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountry.ContinuationTextBox = null;
            this.txtCountry.CustomEnabled = true;
            this.txtCountry.DataFieldMapping = "CR_COUNTRY";
            this.txtCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountry.GetRecordsOnUpDownKeys = false;
            this.txtCountry.IsDate = false;
            this.txtCountry.Location = new System.Drawing.Point(90, 100);
            this.txtCountry.MaxLength = 20;
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.NumberFormat = "###,###,##0.00";
            this.txtCountry.Postfix = "";
            this.txtCountry.Prefix = "";
            this.txtCountry.Size = new System.Drawing.Size(120, 20);
            this.txtCountry.SkipValidation = false;
            this.txtCountry.TabIndex = 8;
            this.txtCountry.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(34, 104);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 13);
            this.label17.TabIndex = 126;
            this.label17.Text = "Country";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtGroup
            // 
            this.txtGroup.AllowSpace = true;
            this.txtGroup.AssociatedLookUpName = "";
            this.txtGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGroup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGroup.ContinuationTextBox = null;
            this.txtGroup.CustomEnabled = true;
            this.txtGroup.DataFieldMapping = "CR_GRP";
            this.txtGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGroup.GetRecordsOnUpDownKeys = false;
            this.txtGroup.IsDate = false;
            this.txtGroup.Location = new System.Drawing.Point(90, 46);
            this.txtGroup.MaxLength = 40;
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.NumberFormat = "###,###,##0.00";
            this.txtGroup.Postfix = "";
            this.txtGroup.Prefix = "";
            this.txtGroup.Size = new System.Drawing.Size(120, 20);
            this.txtGroup.SkipValidation = false;
            this.txtGroup.TabIndex = 3;
            this.txtGroup.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDept
            // 
            this.txtDept.AllowSpace = true;
            this.txtDept.AssociatedLookUpName = "";
            this.txtDept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDept.ContinuationTextBox = null;
            this.txtDept.CustomEnabled = true;
            this.txtDept.DataFieldMapping = "CR_DEPT";
            this.txtDept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDept.GetRecordsOnUpDownKeys = false;
            this.txtDept.IsDate = false;
            this.txtDept.Location = new System.Drawing.Point(90, 73);
            this.txtDept.MaxLength = 40;
            this.txtDept.Name = "txtDept";
            this.txtDept.NumberFormat = "###,###,##0.00";
            this.txtDept.Postfix = "";
            this.txtDept.Prefix = "";
            this.txtDept.Size = new System.Drawing.Size(120, 20);
            this.txtDept.SkipValidation = false;
            this.txtDept.TabIndex = 5;
            this.txtDept.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(50, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 122;
            this.label5.Text = "Dept";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(43, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 121;
            this.label4.Text = "Group";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(259, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 119;
            this.label2.Text = "Relation";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 119;
            this.label1.Text = "FTE/CONT";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "lbpersonnel";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "CR_CONT_NAME";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(507, 17);
            this.txtName.MaxLength = 40;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.Size = new System.Drawing.Size(120, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 2;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtRelation
            // 
            this.txtRelation.AllowSpace = true;
            this.txtRelation.AssociatedLookUpName = "";
            this.txtRelation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRelation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRelation.ContinuationTextBox = null;
            this.txtRelation.CustomEnabled = true;
            this.txtRelation.DataFieldMapping = "CR_PR_RELATION";
            this.txtRelation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRelation.GetRecordsOnUpDownKeys = false;
            this.txtRelation.IsDate = false;
            this.txtRelation.Location = new System.Drawing.Point(323, 17);
            this.txtRelation.MaxLength = 20;
            this.txtRelation.Name = "txtRelation";
            this.txtRelation.NumberFormat = "###,###,##0.00";
            this.txtRelation.Postfix = "";
            this.txtRelation.Prefix = "";
            this.txtRelation.Size = new System.Drawing.Size(120, 20);
            this.txtRelation.SkipValidation = false;
            this.txtRelation.TabIndex = 1;
            this.txtRelation.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_Personnel_OnePagerPersonalEntry_CitiRef
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(674, 225);
            this.Controls.Add(this.pnlCitiReference);
            this.Name = "CHRIS_Personnel_OnePagerPersonalEntry_CitiRef";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iCore CHRIS - One Pager Personnel Entry [Citi-Ref]";
            this.TopMost = true;
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlCitiReference, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlCitiReference.ResumeLayout(false);
            this.pnlCitiReference.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlCitiReference;
        private CrplControlLibrary.SLComboBox cmbFTE;
        private CrplControlLibrary.SLTextBox txtCountry;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLTextBox txtGroup;
        private CrplControlLibrary.SLTextBox txtDept;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtRelation;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtName;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLComboBox cmbOvrLocal;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtDesig;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txtCity;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtBranch;
        private System.Windows.Forms.Label label8;
    }
}