namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlEmpHistory = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvEmpHistory = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.prPeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.From = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.To = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Organization_Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Designation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlEmpHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpHistory)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(638, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(674, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 265);
            this.panel1.Size = new System.Drawing.Size(674, 60);
            // 
            // pnlEmpHistory
            // 
            this.pnlEmpHistory.ConcurrentPanels = null;
            this.pnlEmpHistory.Controls.Add(this.dgvEmpHistory);
            this.pnlEmpHistory.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlEmpHistory.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlEmpHistory.DependentPanels = null;
            this.pnlEmpHistory.DisableDependentLoad = false;
            this.pnlEmpHistory.EnableDelete = true;
            this.pnlEmpHistory.EnableInsert = true;
            this.pnlEmpHistory.EnableQuery = false;
            this.pnlEmpHistory.EnableUpdate = true;
            this.pnlEmpHistory.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PreEmpPersonnelCommand";
            this.pnlEmpHistory.Location = new System.Drawing.Point(12, 15);
            this.pnlEmpHistory.MasterPanel = null;
            this.pnlEmpHistory.Name = "pnlEmpHistory";
            this.pnlEmpHistory.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlEmpHistory.Size = new System.Drawing.Size(650, 245);
            this.pnlEmpHistory.SPName = "CHRIS_PRE_EMP_ONEPAGER_MANAGER";
            this.pnlEmpHistory.TabIndex = 0;
            this.pnlEmpHistory.TabStop = true;
            // 
            // dgvEmpHistory
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmpHistory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEmpHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmpHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.prPeNo,
            this.From,
            this.To,
            this.Organization_Address,
            this.Designation});
            this.dgvEmpHistory.ColumnToHide = null;
            this.dgvEmpHistory.ColumnWidth = null;
            this.dgvEmpHistory.CustomEnabled = true;
            this.dgvEmpHistory.DisplayColumnWrapper = null;
            this.dgvEmpHistory.GridDefaultRow = 0;
            this.dgvEmpHistory.Location = new System.Drawing.Point(3, 3);
            this.dgvEmpHistory.Name = "dgvEmpHistory";
            this.dgvEmpHistory.ReadOnlyColumns = null;
            this.dgvEmpHistory.RequiredColumns = null;
            this.dgvEmpHistory.Size = new System.Drawing.Size(644, 242);
            this.dgvEmpHistory.SkippingColumns = null;
            this.dgvEmpHistory.TabIndex = 0;
            this.dgvEmpHistory.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvEmpHistory_CellFormatting);
            this.dgvEmpHistory.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvEmpHistory_CellValidating);
            // 
            // prPeNo
            // 
            this.prPeNo.DataPropertyName = "PR_P_NO";
            this.prPeNo.HeaderText = "";
            this.prPeNo.Name = "prPeNo";
            this.prPeNo.Visible = false;
            this.prPeNo.Width = 20;
            // 
            // From
            // 
            this.From.DataPropertyName = "PR_JOB_FROM";
            dataGridViewCellStyle2.Format = "dd/MM/yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.From.DefaultCellStyle = dataGridViewCellStyle2;
            this.From.HeaderText = "From Date";
            this.From.MaxInputLength = 10;
            this.From.Name = "From";
            this.From.ToolTipText = "Date Format : dd/MM/yyyy";
            // 
            // To
            // 
            this.To.DataPropertyName = "PR_JOB_TO";
            dataGridViewCellStyle3.Format = "dd/MM/yyyy";
            this.To.DefaultCellStyle = dataGridViewCellStyle3;
            this.To.HeaderText = "To Date";
            this.To.MaxInputLength = 10;
            this.To.Name = "To";
            this.To.ToolTipText = "Date Format : dd/MM/yyyy";
            // 
            // Organization_Address
            // 
            this.Organization_Address.DataPropertyName = "PR_ORGANIZ";
            this.Organization_Address.HeaderText = "Organization ";
            this.Organization_Address.MaxInputLength = 40;
            this.Organization_Address.Name = "Organization_Address";
            this.Organization_Address.Width = 220;
            // 
            // Designation
            // 
            this.Designation.DataPropertyName = "PR_DESIG_PR";
            this.Designation.HeaderText = "Designation/Position";
            this.Designation.MaxInputLength = 30;
            this.Designation.Name = "Designation";
            this.Designation.Width = 180;
            // 
            // CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(674, 325);
            this.Controls.Add(this.pnlEmpHistory);
            this.Name = "CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iCore CHRIS - One Pager Personnel Entry [Employee History]";
            this.TopMost = true;
            this.Shown += new System.EventHandler(this.CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory_Shown);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlEmpHistory, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlEmpHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpHistory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlEmpHistory;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvEmpHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn prPeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn From;
        private System.Windows.Forms.DataGridViewTextBoxColumn To;
        private System.Windows.Forms.DataGridViewTextBoxColumn Organization_Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn Designation;
    }
}