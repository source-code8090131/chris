namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_OnePagerPersonalEntry_Education
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_OnePagerPersonalEntry_Education));
            this.pnlEducation = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvEducation = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PrEYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Degree = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_College = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_City = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_DEG_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlEducation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEducation)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 272);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // pnlEducation
            // 
            this.pnlEducation.ConcurrentPanels = null;
            this.pnlEducation.Controls.Add(this.dgvEducation);
            this.pnlEducation.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlEducation.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlEducation.DependentPanels = null;
            this.pnlEducation.DisableDependentLoad = false;
            this.pnlEducation.EnableDelete = true;
            this.pnlEducation.EnableInsert = true;
            this.pnlEducation.EnableQuery = false;
            this.pnlEducation.EnableUpdate = true;
            this.pnlEducation.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.EducationPersonnelCommand";
            this.pnlEducation.Location = new System.Drawing.Point(12, 15);
            this.pnlEducation.MasterPanel = null;
            this.pnlEducation.Name = "pnlEducation";
            this.pnlEducation.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlEducation.Size = new System.Drawing.Size(650, 249);
            this.pnlEducation.SPName = "CHRIS_EDUCATION_ONEPAGER_MANAGER";
            this.pnlEducation.TabIndex = 11;
            // 
            // dgvEducation
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEducation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEducation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEducation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PrEYear,
            this.Pr_Degree,
            this.Pr_Grade,
            this.Pr_College,
            this.Pr_City,
            this.PR_DEG_TYPE,
            this.Pr_Country,
            this.PR_P_NO});
            this.dgvEducation.ColumnToHide = null;
            this.dgvEducation.ColumnWidth = null;
            this.dgvEducation.CustomEnabled = true;
            this.dgvEducation.DisplayColumnWrapper = null;
            this.dgvEducation.GridDefaultRow = 0;
            this.dgvEducation.Location = new System.Drawing.Point(3, 3);
            this.dgvEducation.Name = "dgvEducation";
            this.dgvEducation.ReadOnlyColumns = null;
            this.dgvEducation.RequiredColumns = null;
            this.dgvEducation.Size = new System.Drawing.Size(644, 243);
            this.dgvEducation.SkippingColumns = null;
            this.dgvEducation.TabIndex = 11;
            this.dgvEducation.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvEducation_CellFormatting);
            // 
            // PrEYear
            // 
            this.PrEYear.DataPropertyName = "PR_E_YEAR";
            this.PrEYear.HeaderText = "Year";
            this.PrEYear.MaxInputLength = 4;
            this.PrEYear.Name = "PrEYear";
            this.PrEYear.Width = 45;
            // 
            // Pr_Degree
            // 
            this.Pr_Degree.DataPropertyName = "PR_DEGREE";
            this.Pr_Degree.HeaderText = "Degree";
            this.Pr_Degree.MaxInputLength = 10;
            this.Pr_Degree.Name = "Pr_Degree";
            this.Pr_Degree.Width = 70;
            // 
            // Pr_Grade
            // 
            this.Pr_Grade.DataPropertyName = "PR_GRADE";
            this.Pr_Grade.HeaderText = "Grade";
            this.Pr_Grade.MaxInputLength = 7;
            this.Pr_Grade.Name = "Pr_Grade";
            this.Pr_Grade.Width = 45;
            // 
            // Pr_College
            // 
            this.Pr_College.DataPropertyName = "PR_COLLAGE";
            this.Pr_College.HeaderText = "College";
            this.Pr_College.MaxInputLength = 50;
            this.Pr_College.Name = "Pr_College";
            this.Pr_College.Width = 160;
            // 
            // Pr_City
            // 
            this.Pr_City.DataPropertyName = "PR_CITY";
            this.Pr_City.HeaderText = "City";
            this.Pr_City.MaxInputLength = 10;
            this.Pr_City.Name = "Pr_City";
            this.Pr_City.Width = 90;
            // 
            // PR_DEG_TYPE
            // 
            this.PR_DEG_TYPE.DataPropertyName = "PR_DEG_TYPE";
            this.PR_DEG_TYPE.HeaderText = "Full Time/ PartTime ";
            this.PR_DEG_TYPE.MaxInputLength = 10;
            this.PR_DEG_TYPE.Name = "PR_DEG_TYPE";
            // 
            // Pr_Country
            // 
            this.Pr_Country.DataPropertyName = "PR_COUNTRY";
            this.Pr_Country.HeaderText = "Country";
            this.Pr_Country.MaxInputLength = 10;
            this.Pr_Country.Name = "Pr_Country";
            this.Pr_Country.Width = 90;
            // 
            // PR_P_NO
            // 
            this.PR_P_NO.DataPropertyName = "PR_P_NO";
            this.PR_P_NO.HeaderText = "Pr_E_No";
            this.PR_P_NO.Name = "PR_P_NO";
            this.PR_P_NO.Visible = false;
            // 
            // CHRIS_Personnel_OnePagerPersonalEntry_Education
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(669, 332);
            this.Controls.Add(this.pnlEducation);
            this.Name = "CHRIS_Personnel_OnePagerPersonalEntry_Education";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iCore CHRIS - One Pager Personnel Entry [Education]";
            this.TopMost = true;
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlEducation, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlEducation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEducation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlEducation;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvEducation;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrEYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Degree;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_College;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_City;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DEG_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_P_NO;

    }
}