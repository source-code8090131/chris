using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_FastOrISConversion : ChrisSimpleForm
    {
        public CHRIS_Personnel_FastOrISConversion()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_FastOrISConversion(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();




        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF5 = false;
            this.FunctionConfig.EnableF2 = false;
            tbtDelete.Enabled = false;
            txtUserName.Text = "User Name: " + this.UserName;
            this.FunctionConfig.F10 = Function.Save;
            this.FunctionConfig.EnableF10 = true;
            tbtAdd.Available = false;
            tbtDelete.Available = false;
            tbtSave.Available = false;
            tbtList.Available = false;


        }
        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }

        protected override bool Save()
        {

            if (!this.IsValidated())
                return false;
            this.lbtnPNo.SkipValidationOnLeave = true;

            DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {


                this.DoToolbarActions(this.pnlDetail.Controls, "Save");
                this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");

                #region Continue The Process For More Records

                DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Y]es or [N]o", "Note"
                               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dRes1 == DialogResult.Yes)
                {
                    if (this.txtMode.Text == "A")
                    {
                        this.Add();
                        txtPersNo.Select();
                        txtPersNo.Focus();

                    }
                    else if (this.txtMode.Text == "M")
                    {
                        this.Edit();
                        txtPersNo.Select();
                        txtPersNo.Focus();

                    }
                }


                #endregion

                return false;

            }
            else if (dRes == DialogResult.No)
            {

                this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");

                return false;
            }


            return false;
        }


        private bool IsValidated()
        {
            bool validated = true;

            if (this.txtPersNo.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid Country Code.");
            }

            if (FunctionConfig.CurrentOption != Function.Delete)
            {
                if (this.txtCountry.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
                }
                if (this.txtCity.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryAc, "Mark as Authenticat/.");
                }
                if (this.txtDesg.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCitiMail, "Mark as Authenticat/.");
                }
                if (this.txtLevel.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryCode, "Select valid Country Code.");
                }





            }


            return validated;
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {


            if (actionType == "Cancel")
            {
                Control skipControl = this.ActiveControl;
                bool skip = false;
                if (skipControl != null)
                {
                    if (skipControl is CrplControlLibrary.SLTextBox)
                        skip = ((CrplControlLibrary.SLTextBox)skipControl).SkipValidation;
                }
                // this.AutoValidate = AutoValidate.Disable;
                this.txtPersNo.IsRequired = false;
                this.txtPersNo.Text = "";
                this.FunctionConfig.CurrentOption = Function.None;
                this.txtOption.Select();
                this.txtOption.Focus();
                base.DoToolbarActions(ctrlsCollection, actionType);
                this.txtPersNo.IsRequired = true;
                 if (skipControl != null)
                {
                    if (skipControl is CrplControlLibrary.SLTextBox)
                        ((CrplControlLibrary.SLTextBox)skipControl).SkipValidation = skip;
                }
               
                //this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                return;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);

        }
        protected override bool Add()
        {

            this.lbtnPNo.ActionType = "Pr_P_NO_LOV_Add";
            this.lbtnPNo.ActionLOVExists = "Pr_P_NO_LOV_Add_Exists";
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.HiddenColumns = "P_PR_DESIG|P_PR_LEVEL|PR_FUNC_TITTLE1|PR_FUNC_TITTLE2|PR_TRANSFER|PR_TERMIN_DATE|PR_CLOSE_FLAG|PR_TRANSFER_DATE|PR_JOINING_DATE";
            base.Add();
            txtMode.Text = "A";

            return false;
        }

        protected override bool View()
        {

            this.lbtnPNo.ActionType = "Pr_P_NO_LOV_Edit";
            this.lbtnPNo.ActionLOVExists = "Pr_P_NO_LOV_Edit_Exists";
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.HiddenColumns = "P_PR_DESIG|P_PR_LEVEL|PR_FUNC_TITTLE1|PR_FUNC_TITTLE2|PR_TRANSFER|PR_TERMIN_DATE|PR_CLOSE_FLAG|PR_TRANSFER_DATE|PR_COUNTRY|PR_CITY|PR_DESG|PR_LEVEL|PR_DEPARTMENT_HC|PR_ASR_DOL|PR_FAST_CONVER|PR_IS_COORDINAT|PR_FURLOUGH|PR_REMARKS|PR_JOINING_DATE";
            base.View();

            return false;
        }

        protected override bool Delete()
        {

            this.lbtnPNo.ActionType = "Pr_P_NO_LOV_Edit";
            this.lbtnPNo.ActionLOVExists = "Pr_P_NO_LOV_Edit_Exists";
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.HiddenColumns = "P_PR_DESIG|P_PR_LEVEL|PR_FUNC_TITTLE1|PR_FUNC_TITTLE2|PR_TRANSFER|PR_TERMIN_DATE|PR_CLOSE_FLAG|PR_TRANSFER_DATE|PR_COUNTRY|PR_CITY|PR_DESG|PR_LEVEL|PR_DEPARTMENT_HC|PR_ASR_DOL|PR_FAST_CONVER|PR_IS_COORDINAT|PR_FURLOUGH|PR_REMARKS|PR_JOINING_DATE";
            base.Delete();

            return false;
        }

        protected override bool Quit()
        {
            return base.Quit();
        }
        protected override bool Edit()
        {

            this.lbtnPNo.ActionType = "Pr_P_NO_LOV_Edit";
            this.lbtnPNo.ActionLOVExists = "Pr_P_NO_LOV_Edit_Exists";
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.HiddenColumns = "P_PR_DESIG|P_PR_LEVEL|PR_FUNC_TITTLE1|PR_FUNC_TITTLE2|PR_TRANSFER|PR_TERMIN_DATE|PR_CLOSE_FLAG|PR_TRANSFER_DATE|PR_COUNTRY|PR_CITY|PR_DESG|PR_LEVEL|PR_DEPARTMENT_HC|PR_ASR_DOL|PR_FAST_CONVER|PR_IS_COORDINAT|PR_FURLOUGH|PR_REMARKS|PR_JOINING_DATE";
            base.Edit();
            txtMode.Text = "M";

            return false;
        }
        private void txtPersNo_Validating(object sender, CancelEventArgs e)
        {
            if (txtPersNo.Text != string.Empty)
            {
                if (terminDate.Value != null)
                {
                    dtDate.Text = terminDate.Value.ToString();
                }
                if (transferDate.Value != null)
                {
                    slTextBox2.Text = transferDate.Value.ToString();
                }
                if (dtDate.Text == string.Empty && txtprcloseFlag.Text != "C")
                {
                    if (this.FunctionConfig.CurrentOption == Function.Add)
                    {
                        if (txtPrTransfer.Text == "6")
                        {

                            txtCountry.Focus();
                        }
                        else
                        {
                            MessageBox.Show("NO DATA FOUND FOR THIS OPTION");
                            e.Cancel = true;
                            this.Cancel();
                            txtOption.Focus();
                            return;

                        }

                    }

                    else if (this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete || this.FunctionConfig.CurrentOption == Function.View)
                    {
                        if (txtPrTransfer.Text == "8")
                        {
                            if (this.FunctionConfig.CurrentOption == Function.Delete)
                            {

                                DialogResult dRes = MessageBox.Show("Do You Want To Delete The Record [Y]es or [N]o", "Note"
                                                , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (dRes == DialogResult.Yes)
                                {


                                    CustomDelete();
                                    base.ClearForm(this.pnlDetail.Controls);

                                    #region Continue The Process For More Records

                                    DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Y]es or [N]o", "Note"
                                                   , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                    if (dRes1 == DialogResult.Yes)
                                    {
                                        this.Delete();
                                        txtPersNo.Select();
                                        txtPersNo.Focus();

                                    }
                                    else
                                    {
                                        base.DoToolbarActions(this.Controls, "Cancel");
                                    }


                                    #endregion






                                }
                                else if (dRes == DialogResult.No)
                                {

                                    base.DoToolbarActions(this.Controls, "Cancel");

                                }

                                return;

                            }
                            if (this.FunctionConfig.CurrentOption == Function.Modify)
                            {
                                txtCountry.Focus();
                                return;

                            }
                            if (this.FunctionConfig.CurrentOption == Function.View)
                            {

                                if (MessageBox.Show("Do You Want To View More Record [Y]es or [N]o", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {



                                    base.ClearForm(pnlDetail.Controls);
                                    txtPersNo.Select();
                                    txtPersNo.Focus();


                                    return;

                                }
                                else
                                {

                                    this.DoToolbarActions(this.Controls, "Cancel");

                                    return;

                                }
                            }

                        }

                        else
                        {
                            MessageBox.Show("NO DATA FOUND FOR THIS OPTION");

                            this.Cancel();
                            txtOption.Focus();
                        }


                    }
                }
            }

            //else
            //{
            //    MessageBox.Show("Record Not Found");

            //    //e.Cancel = true;
            //    this.Cancel();

            //    txtOption.Select();
            //    txtOption.Focus();
            //    return;

            //}



        }

        private void txtPRDept_Validating(object sender, CancelEventArgs e)
        {
            DataTable DtPersonal;

            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            colsNVals.Clear();
            colsNVals.Add("PR_TR_NO", txtPersNo.Text);

            DtPersonal = GetData("CHRIS_SP_CONVERSION_TRANSFER_MANAGER", "Pr_Annual", colsNVals);

            if (DtPersonal != null)
            {
                if (DtPersonal.Rows.Count > 0)
                {
                    txtCurrAnnual.Text = DtPersonal.Rows[0].ItemArray[0].ToString();
                }
            }
        }

        private void txtPR_REMARKS_Validating(object sender, CancelEventArgs e)
        {
            //if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
            //{

            //    DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
            //                                         , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    if (dRes == DialogResult.Yes)
            //    {
            //        this.txtOption.Focus();

            //        base.DoToolbarActions(this.Controls, "Save");

            //        //call save
            //        return;

            //    }
            //    else if (dRes == DialogResult.No)
            //    {

            //        txtOption.Focus();

            //        return;
            //    }
            //}
        }

        private void CHRIS_Personnel_FastOrISConversion_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "Pr_P_NO_LOV_Add")
            {
                if (this.FunctionConfig.CurrentOption == Function.Add)
                {
                    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                }
            }


        }

        private void txtPR_REMARKS_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    MessageBox.Show(" PRESS <F10> TO SAVE  <F6>  EXIT W/O SAVE");
                    e.IsInputKey = true;

                    txtPR_REMARKS.Focus();
                    return;


                }

            }
        }

        private void txtPR_EFFECTIVE_Validating(object sender, CancelEventArgs e)
        {
            //if (this.FunctionConfig.CurrentOption == Function.Add)
            //{
            DateTime date2 = new DateTime();
            DateTime date3 = new DateTime();
            if (joinDate.Value != null)
            {
                date2 = Convert.ToDateTime(joinDate.Value);
            }


            if (transferDate.Value != null)
            {
                date3 = Convert.ToDateTime(transferDate.Value);
            }

            if (txtPR_EFFECTIVE.Value != null)
            {
                DateTime date1 = new DateTime();


                date1 = Convert.ToDateTime(txtPR_EFFECTIVE.Value);

                if (DateTime.Compare(date1.Date, date2.Date) < 0)
                {

                    MessageBox.Show("DATE HAS TO BE GREATER THAN JOINING DATE :" + date2.ToString("dd-MMM-yy"));
                    txtPR_EFFECTIVE.Focus();
                    e.Cancel = true;
                    return;
                }

                if (DateTime.Compare(date1.Date, date3.Date) < 0)
                {

                    MessageBox.Show("YOU HAVE ENTERED AN INVALID DATE.REENTER THE DATE WHICH IS GREATER THAN " + date3.ToString("dd-MMM-yy"));
                    txtPR_EFFECTIVE.Focus();
                    e.Cancel = true;
                    return;
                }

            }
            else
            {
                MessageBox.Show("YOU HAVE ENTERED AN INVALID DATE.REENTER THE DATE WHICH IS GREATER THAN " + date3.ToString("dd-MMM-yy"));
                txtPR_EFFECTIVE.Focus();
                e.Cancel = true;
                return;
            }

            //}

        }
        private void CustomDelete()
        {
            int ID = 0;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();


            colsNVals.Clear();
            colsNVals.Add("PR_TR_NO", txtPersNo.Text);
            colsNVals.Add("PR_FAST_CONVER", txtPR_EFFECTIVE.Value);

            rsltCode = cmnDM.Execute("CHRIS_SP_CONVERSION_TRANSFER_MANAGER", "DELETE", colsNVals);

            if (rsltCode.isSuccessful)
            {


                MessageBox.Show("Record deleted successfully");
                return;
            }



        }



    }
}