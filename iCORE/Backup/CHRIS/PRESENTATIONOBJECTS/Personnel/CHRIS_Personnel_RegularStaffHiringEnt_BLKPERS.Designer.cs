namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlSplBlkPersMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txt_w_date_2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_NO_OF_CHILD = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_BIRTH_SP = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_MARRIAGE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_PR_D_BIRTH = new CrplControlLibrary.SLDatePicker(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.txt_PR_ADD2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label53 = new System.Windows.Forms.Label();
            this.txt_PR_PHONE1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label52 = new System.Windows.Forms.Label();
            this.txt_PR_PHONE2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.txt_PR_ID_CARD_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label49 = new System.Windows.Forms.Label();
            this.txt_PR_SEX = new CrplControlLibrary.SLTextBox(this.components);
            this.label48 = new System.Windows.Forms.Label();
            this.txt_PR_OLD_ID_CARD_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label47 = new System.Windows.Forms.Label();
            this.txt_PR_MARITAL = new CrplControlLibrary.SLTextBox(this.components);
            this.label46 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txt_PR_LANG1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label37 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txt_PR_LANG2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label38 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txt_PR_LANG3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_ADD1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label42 = new System.Windows.Forms.Label();
            this.txt_PR_LANG4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LANG6 = new CrplControlLibrary.SLTextBox(this.components);
            this.label41 = new System.Windows.Forms.Label();
            this.txt_PR_LANG5 = new CrplControlLibrary.SLTextBox(this.components);
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txt_PR_SPOUSE = new CrplControlLibrary.SLTextBox(this.components);
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_PR_PA_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_USER_IS_PRIME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_GROUP_HOSP = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_OPD = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_GROUP_LIFE = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlTblBlkChild = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvBlkChild = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChildName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date_of_Birth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlSplBlkPersMain.SuspendLayout();
            this.slPanelSimple1.SuspendLayout();
            this.pnlTblBlkChild.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkChild)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSplBlkPersMain
            // 
            this.pnlSplBlkPersMain.ConcurrentPanels = null;
            this.pnlSplBlkPersMain.Controls.Add(this.label8);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_w_date_2);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_NO_OF_CHILD);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_BIRTH_SP);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_MARRIAGE);
            this.pnlSplBlkPersMain.Controls.Add(this.label3);
            this.pnlSplBlkPersMain.Controls.Add(this.label2);
            this.pnlSplBlkPersMain.Controls.Add(this.label1);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_D_BIRTH);
            this.pnlSplBlkPersMain.Controls.Add(this.label34);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_ADD2);
            this.pnlSplBlkPersMain.Controls.Add(this.label53);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_PHONE1);
            this.pnlSplBlkPersMain.Controls.Add(this.label52);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_PHONE2);
            this.pnlSplBlkPersMain.Controls.Add(this.label51);
            this.pnlSplBlkPersMain.Controls.Add(this.label50);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_ID_CARD_NO);
            this.pnlSplBlkPersMain.Controls.Add(this.label49);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_SEX);
            this.pnlSplBlkPersMain.Controls.Add(this.label48);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_OLD_ID_CARD_NO);
            this.pnlSplBlkPersMain.Controls.Add(this.label47);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_MARITAL);
            this.pnlSplBlkPersMain.Controls.Add(this.label46);
            this.pnlSplBlkPersMain.Controls.Add(this.label35);
            this.pnlSplBlkPersMain.Controls.Add(this.label45);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG1);
            this.pnlSplBlkPersMain.Controls.Add(this.label37);
            this.pnlSplBlkPersMain.Controls.Add(this.label44);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG2);
            this.pnlSplBlkPersMain.Controls.Add(this.label38);
            this.pnlSplBlkPersMain.Controls.Add(this.label43);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG3);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_ADD1);
            this.pnlSplBlkPersMain.Controls.Add(this.label42);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG4);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG6);
            this.pnlSplBlkPersMain.Controls.Add(this.label41);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG5);
            this.pnlSplBlkPersMain.Controls.Add(this.label39);
            this.pnlSplBlkPersMain.Controls.Add(this.label40);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_SPOUSE);
            this.pnlSplBlkPersMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlSplBlkPersMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlSplBlkPersMain.DependentPanels = null;
            this.pnlSplBlkPersMain.DisableDependentLoad = false;
            this.pnlSplBlkPersMain.EnableDelete = true;
            this.pnlSplBlkPersMain.EnableInsert = true;
            this.pnlSplBlkPersMain.EnableQuery = false;
            this.pnlSplBlkPersMain.EnableUpdate = true;
            this.pnlSplBlkPersMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntPERSONALCommand";
            this.pnlSplBlkPersMain.Location = new System.Drawing.Point(12, 39);
            this.pnlSplBlkPersMain.MasterPanel = null;
            this.pnlSplBlkPersMain.Name = "pnlSplBlkPersMain";
            this.pnlSplBlkPersMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlSplBlkPersMain.Size = new System.Drawing.Size(610, 246);
            this.pnlSplBlkPersMain.SPName = " ";
            this.pnlSplBlkPersMain.TabIndex = 10;
            this.pnlSplBlkPersMain.Leave += new System.EventHandler(this.pnlSplBlkPersMain_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(436, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 131;
            this.label8.Text = "Date :";
            this.label8.Visible = false;
            // 
            // txt_w_date_2
            // 
            this.txt_w_date_2.AllowSpace = true;
            this.txt_w_date_2.AssociatedLookUpName = "";
            this.txt_w_date_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_w_date_2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_w_date_2.ContinuationTextBox = null;
            this.txt_w_date_2.CustomEnabled = true;
            this.txt_w_date_2.DataFieldMapping = "w_date_2";
            this.txt_w_date_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_w_date_2.GetRecordsOnUpDownKeys = false;
            this.txt_w_date_2.IsDate = false;
            this.txt_w_date_2.Location = new System.Drawing.Point(482, 11);
            this.txt_w_date_2.Name = "txt_w_date_2";
            this.txt_w_date_2.NumberFormat = "###,###,##0.00";
            this.txt_w_date_2.Postfix = "";
            this.txt_w_date_2.Prefix = "";
            this.txt_w_date_2.Size = new System.Drawing.Size(96, 20);
            this.txt_w_date_2.SkipValidation = false;
            this.txt_w_date_2.TabIndex = 130;
            this.txt_w_date_2.TextType = CrplControlLibrary.TextType.String;
            this.txt_w_date_2.Visible = false;
            // 
            // txt_PR_NO_OF_CHILD
            // 
            this.txt_PR_NO_OF_CHILD.AllowSpace = true;
            this.txt_PR_NO_OF_CHILD.AssociatedLookUpName = "";
            this.txt_PR_NO_OF_CHILD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NO_OF_CHILD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NO_OF_CHILD.ContinuationTextBox = null;
            this.txt_PR_NO_OF_CHILD.CustomEnabled = true;
            this.txt_PR_NO_OF_CHILD.DataFieldMapping = "PR_NO_OF_CHILD";
            this.txt_PR_NO_OF_CHILD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NO_OF_CHILD.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NO_OF_CHILD.IsDate = false;
            this.txt_PR_NO_OF_CHILD.Location = new System.Drawing.Point(386, 223);
            this.txt_PR_NO_OF_CHILD.Name = "txt_PR_NO_OF_CHILD";
            this.txt_PR_NO_OF_CHILD.NumberFormat = "###,###,##0.00";
            this.txt_PR_NO_OF_CHILD.Postfix = "";
            this.txt_PR_NO_OF_CHILD.Prefix = "";
            this.txt_PR_NO_OF_CHILD.Size = new System.Drawing.Size(52, 20);
            this.txt_PR_NO_OF_CHILD.SkipValidation = false;
            this.txt_PR_NO_OF_CHILD.TabIndex = 19;
            this.txt_PR_NO_OF_CHILD.TabStop = false;
            this.txt_PR_NO_OF_CHILD.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_BIRTH_SP
            // 
            this.txt_PR_BIRTH_SP.CustomEnabled = true;
            this.txt_PR_BIRTH_SP.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_BIRTH_SP.DataFieldMapping = "PR_BIRTH_SP";
            this.txt_PR_BIRTH_SP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_BIRTH_SP.HasChanges = true;
            this.txt_PR_BIRTH_SP.Location = new System.Drawing.Point(386, 197);
            this.txt_PR_BIRTH_SP.Name = "txt_PR_BIRTH_SP";
            this.txt_PR_BIRTH_SP.NullValue = " ";
            this.txt_PR_BIRTH_SP.Size = new System.Drawing.Size(121, 20);
            this.txt_PR_BIRTH_SP.TabIndex = 18;
            this.txt_PR_BIRTH_SP.Value = new System.DateTime(2011, 1, 20, 0, 0, 0, 0);
            this.txt_PR_BIRTH_SP.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_BIRTH_SP_Validating);
            // 
            // txt_PR_MARRIAGE
            // 
            this.txt_PR_MARRIAGE.CustomEnabled = true;
            this.txt_PR_MARRIAGE.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_MARRIAGE.DataFieldMapping = "PR_MARRIAGE";
            this.txt_PR_MARRIAGE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_MARRIAGE.HasChanges = true;
            this.txt_PR_MARRIAGE.Location = new System.Drawing.Point(125, 223);
            this.txt_PR_MARRIAGE.Name = "txt_PR_MARRIAGE";
            this.txt_PR_MARRIAGE.NullValue = " ";
            this.txt_PR_MARRIAGE.Size = new System.Drawing.Size(96, 20);
            this.txt_PR_MARRIAGE.TabIndex = 16;
            this.txt_PR_MARRIAGE.Value = new System.DateTime(2011, 1, 20, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(266, 225);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 126;
            this.label3.Text = "No. of Childern`s";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(287, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 125;
            this.label2.Text = "Date Of Birth";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(283, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 124;
            this.label1.Text = "Spouse Name";
            // 
            // txt_PR_D_BIRTH
            // 
            this.txt_PR_D_BIRTH.CustomEnabled = true;
            this.txt_PR_D_BIRTH.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_D_BIRTH.DataFieldMapping = "";
            this.txt_PR_D_BIRTH.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_D_BIRTH.HasChanges = true;
            this.txt_PR_D_BIRTH.Location = new System.Drawing.Point(92, 115);
            this.txt_PR_D_BIRTH.Name = "txt_PR_D_BIRTH";
            this.txt_PR_D_BIRTH.NullValue = " ";
            this.txt_PR_D_BIRTH.Size = new System.Drawing.Size(96, 20);
            this.txt_PR_D_BIRTH.TabIndex = 5;
            this.txt_PR_D_BIRTH.Value = new System.DateTime(2011, 1, 20, 0, 0, 0, 0);
            this.txt_PR_D_BIRTH.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_D_BIRTH_Validating);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(10, 13);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(47, 13);
            this.label34.TabIndex = 86;
            this.label34.Text = "Page 2";
            this.label34.Visible = false;
            // 
            // txt_PR_ADD2
            // 
            this.txt_PR_ADD2.AllowSpace = true;
            this.txt_PR_ADD2.AssociatedLookUpName = "";
            this.txt_PR_ADD2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ADD2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ADD2.ContinuationTextBox = null;
            this.txt_PR_ADD2.CustomEnabled = true;
            this.txt_PR_ADD2.DataFieldMapping = "";
            this.txt_PR_ADD2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ADD2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ADD2.IsDate = false;
            this.txt_PR_ADD2.Location = new System.Drawing.Point(77, 54);
            this.txt_PR_ADD2.MaxLength = 30;
            this.txt_PR_ADD2.Name = "txt_PR_ADD2";
            this.txt_PR_ADD2.NumberFormat = "###,###,##0.00";
            this.txt_PR_ADD2.Postfix = "";
            this.txt_PR_ADD2.Prefix = "";
            this.txt_PR_ADD2.Size = new System.Drawing.Size(263, 20);
            this.txt_PR_ADD2.SkipValidation = false;
            this.txt_PR_ADD2.TabIndex = 2;
            this.txt_PR_ADD2.TextType = CrplControlLibrary.TextType.AllCharacters;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(12, 225);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(110, 13);
            this.label53.TabIndex = 106;
            this.label53.Text = "Date of Marriage :";
            // 
            // txt_PR_PHONE1
            // 
            this.txt_PR_PHONE1.AllowSpace = true;
            this.txt_PR_PHONE1.AssociatedLookUpName = "";
            this.txt_PR_PHONE1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_PHONE1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_PHONE1.ContinuationTextBox = null;
            this.txt_PR_PHONE1.CustomEnabled = true;
            this.txt_PR_PHONE1.DataFieldMapping = "";
            this.txt_PR_PHONE1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_PHONE1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_PHONE1.IsDate = false;
            this.txt_PR_PHONE1.Location = new System.Drawing.Point(77, 77);
            this.txt_PR_PHONE1.MaxLength = 15;
            this.txt_PR_PHONE1.Name = "txt_PR_PHONE1";
            this.txt_PR_PHONE1.NumberFormat = "###,###,##0.00";
            this.txt_PR_PHONE1.Postfix = "";
            this.txt_PR_PHONE1.Prefix = "";
            this.txt_PR_PHONE1.Size = new System.Drawing.Size(111, 20);
            this.txt_PR_PHONE1.SkipValidation = false;
            this.txt_PR_PHONE1.TabIndex = 3;
            this.txt_PR_PHONE1.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(13, 200);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(93, 13);
            this.label52.TabIndex = 105;
            this.label52.Text = "Marital Status :";
            // 
            // txt_PR_PHONE2
            // 
            this.txt_PR_PHONE2.AllowSpace = true;
            this.txt_PR_PHONE2.AssociatedLookUpName = "";
            this.txt_PR_PHONE2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_PHONE2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_PHONE2.ContinuationTextBox = null;
            this.txt_PR_PHONE2.CustomEnabled = true;
            this.txt_PR_PHONE2.DataFieldMapping = "";
            this.txt_PR_PHONE2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_PHONE2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_PHONE2.IsDate = false;
            this.txt_PR_PHONE2.Location = new System.Drawing.Point(219, 77);
            this.txt_PR_PHONE2.MaxLength = 15;
            this.txt_PR_PHONE2.Name = "txt_PR_PHONE2";
            this.txt_PR_PHONE2.NumberFormat = "###,###,##0.00";
            this.txt_PR_PHONE2.Postfix = "";
            this.txt_PR_PHONE2.Prefix = "";
            this.txt_PR_PHONE2.Size = new System.Drawing.Size(121, 20);
            this.txt_PR_PHONE2.SkipValidation = false;
            this.txt_PR_PHONE2.TabIndex = 4;
            this.txt_PR_PHONE2.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_PR_PHONE2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_PHONE2_Validating);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(13, 168);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(77, 13);
            this.label51.TabIndex = 104;
            this.label51.Text = "Old ID Card.";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(13, 144);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(74, 13);
            this.label50.TabIndex = 103;
            this.label50.Text = "ID Card No.";
            // 
            // txt_PR_ID_CARD_NO
            // 
            this.txt_PR_ID_CARD_NO.AllowSpace = true;
            this.txt_PR_ID_CARD_NO.AssociatedLookUpName = "";
            this.txt_PR_ID_CARD_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ID_CARD_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ID_CARD_NO.ContinuationTextBox = null;
            this.txt_PR_ID_CARD_NO.CustomEnabled = true;
            this.txt_PR_ID_CARD_NO.DataFieldMapping = "PR_ID_CARD_NO";
            this.txt_PR_ID_CARD_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ID_CARD_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ID_CARD_NO.IsDate = false;
            this.txt_PR_ID_CARD_NO.Location = new System.Drawing.Point(92, 141);
            this.txt_PR_ID_CARD_NO.MaxLength = 15;
            this.txt_PR_ID_CARD_NO.Name = "txt_PR_ID_CARD_NO";
            this.txt_PR_ID_CARD_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_ID_CARD_NO.Postfix = "";
            this.txt_PR_ID_CARD_NO.Prefix = "";
            this.txt_PR_ID_CARD_NO.Size = new System.Drawing.Size(96, 20);
            this.txt_PR_ID_CARD_NO.SkipValidation = false;
            this.txt_PR_ID_CARD_NO.TabIndex = 7;
            this.txt_PR_ID_CARD_NO.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txt_PR_ID_CARD_NO, "Enter ID Card in the Format 999-99-999999  <F6>=Exit W/O Save <Ctrl+Page Down>=Ne" +
                    "xt Ssreen");
            this.txt_PR_ID_CARD_NO.Enter += new System.EventHandler(this.txt_PR_ID_CARD_NO_Enter);
            this.txt_PR_ID_CARD_NO.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_ID_CARD_NO_Validating);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(196, 121);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(28, 13);
            this.label49.TabIndex = 102;
            this.label49.Text = "Sex";
            // 
            // txt_PR_SEX
            // 
            this.txt_PR_SEX.AllowSpace = true;
            this.txt_PR_SEX.AssociatedLookUpName = "";
            this.txt_PR_SEX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_SEX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_SEX.ContinuationTextBox = null;
            this.txt_PR_SEX.CustomEnabled = true;
            this.txt_PR_SEX.DataFieldMapping = "";
            this.txt_PR_SEX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_SEX.GetRecordsOnUpDownKeys = false;
            this.txt_PR_SEX.IsDate = false;
            this.txt_PR_SEX.Location = new System.Drawing.Point(230, 115);
            this.txt_PR_SEX.MaxLength = 1;
            this.txt_PR_SEX.Name = "txt_PR_SEX";
            this.txt_PR_SEX.NumberFormat = "###,###,##0.00";
            this.txt_PR_SEX.Postfix = "";
            this.txt_PR_SEX.Prefix = "";
            this.txt_PR_SEX.Size = new System.Drawing.Size(30, 20);
            this.txt_PR_SEX.SkipValidation = false;
            this.txt_PR_SEX.TabIndex = 6;
            this.txt_PR_SEX.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_SEX.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_SEX_Validating);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(13, 117);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(79, 13);
            this.label48.TabIndex = 101;
            this.label48.Text = "Date of Birth";
            // 
            // txt_PR_OLD_ID_CARD_NO
            // 
            this.txt_PR_OLD_ID_CARD_NO.AllowSpace = true;
            this.txt_PR_OLD_ID_CARD_NO.AssociatedLookUpName = "";
            this.txt_PR_OLD_ID_CARD_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_OLD_ID_CARD_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_OLD_ID_CARD_NO.ContinuationTextBox = null;
            this.txt_PR_OLD_ID_CARD_NO.CustomEnabled = true;
            this.txt_PR_OLD_ID_CARD_NO.DataFieldMapping = "PR_OLD_ID_CARD_NO";
            this.txt_PR_OLD_ID_CARD_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_OLD_ID_CARD_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_OLD_ID_CARD_NO.IsDate = false;
            this.txt_PR_OLD_ID_CARD_NO.Location = new System.Drawing.Point(92, 165);
            this.txt_PR_OLD_ID_CARD_NO.MaxLength = 14;
            this.txt_PR_OLD_ID_CARD_NO.Name = "txt_PR_OLD_ID_CARD_NO";
            this.txt_PR_OLD_ID_CARD_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_OLD_ID_CARD_NO.Postfix = "";
            this.txt_PR_OLD_ID_CARD_NO.Prefix = "";
            this.txt_PR_OLD_ID_CARD_NO.Size = new System.Drawing.Size(96, 20);
            this.txt_PR_OLD_ID_CARD_NO.SkipValidation = false;
            this.txt_PR_OLD_ID_CARD_NO.TabIndex = 8;
            this.txt_PR_OLD_ID_CARD_NO.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txt_PR_OLD_ID_CARD_NO, "Enter ID Card in the Format 999-99-999999  <F6>=Exit W/O Save <Ctrl+Page Down>=Ne" +
                    "xt Ssreen");
            this.txt_PR_OLD_ID_CARD_NO.Enter += new System.EventHandler(this.txt_PR_OLD_ID_CARD_NO_Enter);
            this.txt_PR_OLD_ID_CARD_NO.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_OLD_ID_CARD_NO_Validating);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(201, 81);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(18, 13);
            this.label47.TabIndex = 100;
            this.label47.Text = "2)";
            // 
            // txt_PR_MARITAL
            // 
            this.txt_PR_MARITAL.AllowSpace = true;
            this.txt_PR_MARITAL.AssociatedLookUpName = "";
            this.txt_PR_MARITAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_MARITAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_MARITAL.ContinuationTextBox = null;
            this.txt_PR_MARITAL.CustomEnabled = true;
            this.txt_PR_MARITAL.DataFieldMapping = "PR_MARITAL";
            this.txt_PR_MARITAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_MARITAL.GetRecordsOnUpDownKeys = false;
            this.txt_PR_MARITAL.IsDate = false;
            this.txt_PR_MARITAL.Location = new System.Drawing.Point(125, 199);
            this.txt_PR_MARITAL.MaxLength = 1;
            this.txt_PR_MARITAL.Name = "txt_PR_MARITAL";
            this.txt_PR_MARITAL.NumberFormat = "###,###,##0.00";
            this.txt_PR_MARITAL.Postfix = "";
            this.txt_PR_MARITAL.Prefix = "";
            this.txt_PR_MARITAL.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_MARITAL.SkipValidation = false;
            this.txt_PR_MARITAL.TabIndex = 15;
            this.txt_PR_MARITAL.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_MARITAL.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_MARITAL_Validating);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(13, 81);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(58, 13);
            this.label46.TabIndex = 99;
            this.label46.Text = "Phone 1)";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(95, 1);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(107, 13);
            this.label35.TabIndex = 88;
            this.label35.Text = "Personnel System";
            this.label35.Visible = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(458, 146);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(18, 13);
            this.label45.TabIndex = 98;
            this.label45.Text = "6)";
            // 
            // txt_PR_LANG1
            // 
            this.txt_PR_LANG1.AllowSpace = true;
            this.txt_PR_LANG1.AssociatedLookUpName = "";
            this.txt_PR_LANG1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG1.ContinuationTextBox = null;
            this.txt_PR_LANG1.CustomEnabled = true;
            this.txt_PR_LANG1.DataFieldMapping = "PR_LANG1";
            this.txt_PR_LANG1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG1.IsDate = false;
            this.txt_PR_LANG1.Location = new System.Drawing.Point(482, 34);
            this.txt_PR_LANG1.MaxLength = 10;
            this.txt_PR_LANG1.Name = "txt_PR_LANG1";
            this.txt_PR_LANG1.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG1.Postfix = "";
            this.txt_PR_LANG1.Prefix = "";
            this.txt_PR_LANG1.Size = new System.Drawing.Size(96, 20);
            this.txt_PR_LANG1.SkipValidation = false;
            this.txt_PR_LANG1.TabIndex = 9;
            this.txt_PR_LANG1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(206, 3);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(180, 13);
            this.label37.TabIndex = 90;
            this.label37.Text = "PERSONNELS INFORMATION";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(458, 124);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(18, 13);
            this.label44.TabIndex = 97;
            this.label44.Text = "5)";
            // 
            // txt_PR_LANG2
            // 
            this.txt_PR_LANG2.AllowSpace = true;
            this.txt_PR_LANG2.AssociatedLookUpName = "";
            this.txt_PR_LANG2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG2.ContinuationTextBox = null;
            this.txt_PR_LANG2.CustomEnabled = true;
            this.txt_PR_LANG2.DataFieldMapping = "PR_LANG2";
            this.txt_PR_LANG2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG2.IsDate = false;
            this.txt_PR_LANG2.Location = new System.Drawing.Point(482, 56);
            this.txt_PR_LANG2.MaxLength = 10;
            this.txt_PR_LANG2.Name = "txt_PR_LANG2";
            this.txt_PR_LANG2.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG2.Postfix = "";
            this.txt_PR_LANG2.Prefix = "";
            this.txt_PR_LANG2.Size = new System.Drawing.Size(96, 20);
            this.txt_PR_LANG2.SkipValidation = false;
            this.txt_PR_LANG2.TabIndex = 10;
            this.txt_PR_LANG2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(13, 36);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(60, 13);
            this.label38.TabIndex = 91;
            this.label38.Text = "Address :";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(458, 103);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(18, 13);
            this.label43.TabIndex = 96;
            this.label43.Text = "4)";
            // 
            // txt_PR_LANG3
            // 
            this.txt_PR_LANG3.AllowSpace = true;
            this.txt_PR_LANG3.AssociatedLookUpName = "";
            this.txt_PR_LANG3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG3.ContinuationTextBox = null;
            this.txt_PR_LANG3.CustomEnabled = true;
            this.txt_PR_LANG3.DataFieldMapping = "PR_LANG3";
            this.txt_PR_LANG3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG3.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG3.IsDate = false;
            this.txt_PR_LANG3.Location = new System.Drawing.Point(482, 78);
            this.txt_PR_LANG3.MaxLength = 10;
            this.txt_PR_LANG3.Name = "txt_PR_LANG3";
            this.txt_PR_LANG3.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG3.Postfix = "";
            this.txt_PR_LANG3.Prefix = "";
            this.txt_PR_LANG3.Size = new System.Drawing.Size(96, 20);
            this.txt_PR_LANG3.SkipValidation = false;
            this.txt_PR_LANG3.TabIndex = 11;
            this.txt_PR_LANG3.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_ADD1
            // 
            this.txt_PR_ADD1.AllowSpace = true;
            this.txt_PR_ADD1.AssociatedLookUpName = "";
            this.txt_PR_ADD1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ADD1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ADD1.ContinuationTextBox = null;
            this.txt_PR_ADD1.CustomEnabled = true;
            this.txt_PR_ADD1.DataFieldMapping = "";
            this.txt_PR_ADD1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ADD1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ADD1.IsDate = false;
            this.txt_PR_ADD1.IsRequired = true;
            this.txt_PR_ADD1.Location = new System.Drawing.Point(77, 31);
            this.txt_PR_ADD1.MaxLength = 30;
            this.txt_PR_ADD1.Name = "txt_PR_ADD1";
            this.txt_PR_ADD1.NumberFormat = "###,###,##0.00";
            this.txt_PR_ADD1.Postfix = "";
            this.txt_PR_ADD1.Prefix = "";
            this.txt_PR_ADD1.Size = new System.Drawing.Size(263, 20);
            this.txt_PR_ADD1.SkipValidation = false;
            this.txt_PR_ADD1.TabIndex = 1;
            this.txt_PR_ADD1.TextType = CrplControlLibrary.TextType.AllCharacters;
            this.txt_PR_ADD1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_ADD1_Validating);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(458, 82);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(18, 13);
            this.label42.TabIndex = 95;
            this.label42.Text = "3)";
            // 
            // txt_PR_LANG4
            // 
            this.txt_PR_LANG4.AllowSpace = true;
            this.txt_PR_LANG4.AssociatedLookUpName = "";
            this.txt_PR_LANG4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG4.ContinuationTextBox = null;
            this.txt_PR_LANG4.CustomEnabled = true;
            this.txt_PR_LANG4.DataFieldMapping = "PR_LANG4";
            this.txt_PR_LANG4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG4.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG4.IsDate = false;
            this.txt_PR_LANG4.Location = new System.Drawing.Point(482, 100);
            this.txt_PR_LANG4.MaxLength = 10;
            this.txt_PR_LANG4.Name = "txt_PR_LANG4";
            this.txt_PR_LANG4.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG4.Postfix = "";
            this.txt_PR_LANG4.Prefix = "";
            this.txt_PR_LANG4.Size = new System.Drawing.Size(96, 20);
            this.txt_PR_LANG4.SkipValidation = false;
            this.txt_PR_LANG4.TabIndex = 12;
            this.txt_PR_LANG4.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_LANG6
            // 
            this.txt_PR_LANG6.AllowSpace = true;
            this.txt_PR_LANG6.AssociatedLookUpName = "";
            this.txt_PR_LANG6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG6.ContinuationTextBox = null;
            this.txt_PR_LANG6.CustomEnabled = true;
            this.txt_PR_LANG6.DataFieldMapping = "PR_LANG6";
            this.txt_PR_LANG6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG6.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG6.IsDate = false;
            this.txt_PR_LANG6.Location = new System.Drawing.Point(482, 144);
            this.txt_PR_LANG6.MaxLength = 10;
            this.txt_PR_LANG6.Name = "txt_PR_LANG6";
            this.txt_PR_LANG6.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG6.Postfix = "";
            this.txt_PR_LANG6.Prefix = "";
            this.txt_PR_LANG6.Size = new System.Drawing.Size(96, 20);
            this.txt_PR_LANG6.SkipValidation = false;
            this.txt_PR_LANG6.TabIndex = 14;
            this.txt_PR_LANG6.TextType = CrplControlLibrary.TextType.String;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(458, 60);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(18, 13);
            this.label41.TabIndex = 94;
            this.label41.Text = "2)";
            // 
            // txt_PR_LANG5
            // 
            this.txt_PR_LANG5.AllowSpace = true;
            this.txt_PR_LANG5.AssociatedLookUpName = "";
            this.txt_PR_LANG5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG5.ContinuationTextBox = null;
            this.txt_PR_LANG5.CustomEnabled = true;
            this.txt_PR_LANG5.DataFieldMapping = "PR_LANG5";
            this.txt_PR_LANG5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG5.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG5.IsDate = false;
            this.txt_PR_LANG5.Location = new System.Drawing.Point(482, 122);
            this.txt_PR_LANG5.MaxLength = 10;
            this.txt_PR_LANG5.Name = "txt_PR_LANG5";
            this.txt_PR_LANG5.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG5.Postfix = "";
            this.txt_PR_LANG5.Prefix = "";
            this.txt_PR_LANG5.Size = new System.Drawing.Size(96, 20);
            this.txt_PR_LANG5.SkipValidation = false;
            this.txt_PR_LANG5.TabIndex = 13;
            this.txt_PR_LANG5.TextType = CrplControlLibrary.TextType.String;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(383, 38);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(69, 13);
            this.label39.TabIndex = 92;
            this.label39.Text = "Languages";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(458, 38);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(18, 13);
            this.label40.TabIndex = 93;
            this.label40.Text = "1)";
            // 
            // txt_PR_SPOUSE
            // 
            this.txt_PR_SPOUSE.AllowSpace = true;
            this.txt_PR_SPOUSE.AssociatedLookUpName = "";
            this.txt_PR_SPOUSE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_SPOUSE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_SPOUSE.ContinuationTextBox = null;
            this.txt_PR_SPOUSE.CustomEnabled = true;
            this.txt_PR_SPOUSE.DataFieldMapping = "PR_SPOUSE";
            this.txt_PR_SPOUSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_SPOUSE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_SPOUSE.IsDate = false;
            this.txt_PR_SPOUSE.Location = new System.Drawing.Point(386, 171);
            this.txt_PR_SPOUSE.MaxLength = 45;
            this.txt_PR_SPOUSE.Name = "txt_PR_SPOUSE";
            this.txt_PR_SPOUSE.NumberFormat = "###,###,##0.00";
            this.txt_PR_SPOUSE.Postfix = "";
            this.txt_PR_SPOUSE.Prefix = "";
            this.txt_PR_SPOUSE.Size = new System.Drawing.Size(192, 20);
            this.txt_PR_SPOUSE.SkipValidation = false;
            this.txt_PR_SPOUSE.TabIndex = 17;
            this.txt_PR_SPOUSE.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_SPOUSE.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_SPOUSE_Validating);
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.txt_PR_PA_NO);
            this.slPanelSimple1.Controls.Add(this.txt_PR_USER_IS_PRIME);
            this.slPanelSimple1.Controls.Add(this.txt_PR_GROUP_HOSP);
            this.slPanelSimple1.Controls.Add(this.txt_PR_OPD);
            this.slPanelSimple1.Controls.Add(this.txt_PR_GROUP_LIFE);
            this.slPanelSimple1.Controls.Add(this.label7);
            this.slPanelSimple1.Controls.Add(this.label6);
            this.slPanelSimple1.Controls.Add(this.label5);
            this.slPanelSimple1.Controls.Add(this.label4);
            this.slPanelSimple1.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = true;
            this.slPanelSimple1.EnableInsert = true;
            this.slPanelSimple1.EnableQuery = false;
            this.slPanelSimple1.EnableUpdate = true;
            this.slPanelSimple1.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntPERSONALCommand";
            this.slPanelSimple1.Location = new System.Drawing.Point(12, 286);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(260, 122);
            this.slPanelSimple1.SPName = "CHRIS_SP_RegStHiEnt_PERSONAL_MANAGER";
            this.slPanelSimple1.TabIndex = 11;
            // 
            // txt_PR_PA_NO
            // 
            this.txt_PR_PA_NO.AllowSpace = true;
            this.txt_PR_PA_NO.AssociatedLookUpName = "";
            this.txt_PR_PA_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_PA_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_PA_NO.ContinuationTextBox = null;
            this.txt_PR_PA_NO.CustomEnabled = true;
            this.txt_PR_PA_NO.DataFieldMapping = "PR_P_NO";
            this.txt_PR_PA_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_PA_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_PA_NO.IsDate = false;
            this.txt_PR_PA_NO.Location = new System.Drawing.Point(154, 96);
            this.txt_PR_PA_NO.Name = "txt_PR_PA_NO";
            this.txt_PR_PA_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_PA_NO.Postfix = "";
            this.txt_PR_PA_NO.Prefix = "";
            this.txt_PR_PA_NO.Size = new System.Drawing.Size(70, 20);
            this.txt_PR_PA_NO.SkipValidation = false;
            this.txt_PR_PA_NO.TabIndex = 137;
            this.txt_PR_PA_NO.TabStop = false;
            this.txt_PR_PA_NO.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_PA_NO.Visible = false;
            // 
            // txt_PR_USER_IS_PRIME
            // 
            this.txt_PR_USER_IS_PRIME.AllowSpace = true;
            this.txt_PR_USER_IS_PRIME.AssociatedLookUpName = "";
            this.txt_PR_USER_IS_PRIME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_USER_IS_PRIME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_USER_IS_PRIME.ContinuationTextBox = null;
            this.txt_PR_USER_IS_PRIME.CustomEnabled = true;
            this.txt_PR_USER_IS_PRIME.DataFieldMapping = "PR_USER_IS_PRIME";
            this.txt_PR_USER_IS_PRIME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_USER_IS_PRIME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_USER_IS_PRIME.IsDate = false;
            this.txt_PR_USER_IS_PRIME.Location = new System.Drawing.Point(154, 70);
            this.txt_PR_USER_IS_PRIME.MaxLength = 10;
            this.txt_PR_USER_IS_PRIME.Name = "txt_PR_USER_IS_PRIME";
            this.txt_PR_USER_IS_PRIME.NumberFormat = "###,###,##0.00";
            this.txt_PR_USER_IS_PRIME.Postfix = "";
            this.txt_PR_USER_IS_PRIME.Prefix = "";
            this.txt_PR_USER_IS_PRIME.Size = new System.Drawing.Size(70, 20);
            this.txt_PR_USER_IS_PRIME.SkipValidation = false;
            this.txt_PR_USER_IS_PRIME.TabIndex = 23;
            this.txt_PR_USER_IS_PRIME.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_USER_IS_PRIME.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txt_PR_USER_IS_PRIME_PreviewKeyDown);
            this.txt_PR_USER_IS_PRIME.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_PR_USER_IS_PRIME_KeyPress);
            // 
            // txt_PR_GROUP_HOSP
            // 
            this.txt_PR_GROUP_HOSP.AllowSpace = true;
            this.txt_PR_GROUP_HOSP.AssociatedLookUpName = "";
            this.txt_PR_GROUP_HOSP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_GROUP_HOSP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_GROUP_HOSP.ContinuationTextBox = null;
            this.txt_PR_GROUP_HOSP.CustomEnabled = true;
            this.txt_PR_GROUP_HOSP.DataFieldMapping = "PR_GROUP_HOSP";
            this.txt_PR_GROUP_HOSP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_GROUP_HOSP.GetRecordsOnUpDownKeys = false;
            this.txt_PR_GROUP_HOSP.IsDate = false;
            this.txt_PR_GROUP_HOSP.Location = new System.Drawing.Point(154, 47);
            this.txt_PR_GROUP_HOSP.MaxLength = 1;
            this.txt_PR_GROUP_HOSP.Name = "txt_PR_GROUP_HOSP";
            this.txt_PR_GROUP_HOSP.NumberFormat = "###,###,##0.00";
            this.txt_PR_GROUP_HOSP.Postfix = "";
            this.txt_PR_GROUP_HOSP.Prefix = "";
            this.txt_PR_GROUP_HOSP.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_GROUP_HOSP.SkipValidation = false;
            this.txt_PR_GROUP_HOSP.TabIndex = 22;
            this.txt_PR_GROUP_HOSP.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_GROUP_HOSP.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_GROUP_HOSP_Validating);
            // 
            // txt_PR_OPD
            // 
            this.txt_PR_OPD.AllowSpace = true;
            this.txt_PR_OPD.AssociatedLookUpName = "";
            this.txt_PR_OPD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_OPD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_OPD.ContinuationTextBox = null;
            this.txt_PR_OPD.CustomEnabled = true;
            this.txt_PR_OPD.DataFieldMapping = "PR_OPD";
            this.txt_PR_OPD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_OPD.GetRecordsOnUpDownKeys = false;
            this.txt_PR_OPD.IsDate = false;
            this.txt_PR_OPD.Location = new System.Drawing.Point(154, 25);
            this.txt_PR_OPD.MaxLength = 1;
            this.txt_PR_OPD.Name = "txt_PR_OPD";
            this.txt_PR_OPD.NumberFormat = "###,###,##0.00";
            this.txt_PR_OPD.Postfix = "";
            this.txt_PR_OPD.Prefix = "";
            this.txt_PR_OPD.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_OPD.SkipValidation = false;
            this.txt_PR_OPD.TabIndex = 21;
            this.txt_PR_OPD.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_OPD.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_OPD_Validating);
            // 
            // txt_PR_GROUP_LIFE
            // 
            this.txt_PR_GROUP_LIFE.AllowSpace = true;
            this.txt_PR_GROUP_LIFE.AssociatedLookUpName = "";
            this.txt_PR_GROUP_LIFE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_GROUP_LIFE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_GROUP_LIFE.ContinuationTextBox = null;
            this.txt_PR_GROUP_LIFE.CustomEnabled = true;
            this.txt_PR_GROUP_LIFE.DataFieldMapping = "PR_GROUP_LIFE";
            this.txt_PR_GROUP_LIFE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_GROUP_LIFE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_GROUP_LIFE.IsDate = false;
            this.txt_PR_GROUP_LIFE.Location = new System.Drawing.Point(154, 3);
            this.txt_PR_GROUP_LIFE.MaxLength = 1;
            this.txt_PR_GROUP_LIFE.Name = "txt_PR_GROUP_LIFE";
            this.txt_PR_GROUP_LIFE.NumberFormat = "###,###,##0.00";
            this.txt_PR_GROUP_LIFE.Postfix = "";
            this.txt_PR_GROUP_LIFE.Prefix = "";
            this.txt_PR_GROUP_LIFE.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_GROUP_LIFE.SkipValidation = false;
            this.txt_PR_GROUP_LIFE.TabIndex = 20;
            this.txt_PR_GROUP_LIFE.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_GROUP_LIFE.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_GROUP_LIFE_Validating);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(34, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 133;
            this.label7.Text = "User ID on PRIME :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 13);
            this.label6.TabIndex = 132;
            this.label6.Text = "Enrolled in Group Hosp. :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 13);
            this.label5.TabIndex = 131;
            this.label5.Text = "Enrolled in Out Patient :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 13);
            this.label4.TabIndex = 130;
            this.label4.Text = "Enrolled in Group Life :";
            // 
            // pnlTblBlkChild
            // 
            this.pnlTblBlkChild.ConcurrentPanels = null;
            this.pnlTblBlkChild.Controls.Add(this.dgvBlkChild);
            this.pnlTblBlkChild.DataManager = null;
            this.pnlTblBlkChild.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBlkChild.DependentPanels = null;
            this.pnlTblBlkChild.DisableDependentLoad = false;
            this.pnlTblBlkChild.EnableDelete = true;
            this.pnlTblBlkChild.EnableInsert = true;
            this.pnlTblBlkChild.EnableQuery = false;
            this.pnlTblBlkChild.EnableUpdate = true;
            this.pnlTblBlkChild.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntCHILDERNCommand";
            this.pnlTblBlkChild.Location = new System.Drawing.Point(298, 286);
            this.pnlTblBlkChild.MasterPanel = null;
            this.pnlTblBlkChild.Name = "pnlTblBlkChild";
            this.pnlTblBlkChild.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBlkChild.Size = new System.Drawing.Size(324, 120);
            this.pnlTblBlkChild.SPName = "CHRIS_SP_RegStHiEnt_CHILDREN_MANAGER";
            this.pnlTblBlkChild.TabIndex = 12;
            // 
            // dgvBlkChild
            // 
            this.dgvBlkChild.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkChild.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_P_NO,
            this.ChildName,
            this.Date_of_Birth});
            this.dgvBlkChild.ColumnToHide = null;
            this.dgvBlkChild.ColumnWidth = null;
            this.dgvBlkChild.CustomEnabled = true;
            this.dgvBlkChild.DisplayColumnWrapper = null;
            this.dgvBlkChild.GridDefaultRow = 0;
            this.dgvBlkChild.Location = new System.Drawing.Point(3, 3);
            this.dgvBlkChild.Name = "dgvBlkChild";
            this.dgvBlkChild.ReadOnlyColumns = null;
            this.dgvBlkChild.RequiredColumns = null;
            this.dgvBlkChild.Size = new System.Drawing.Size(294, 113);
            this.dgvBlkChild.SkippingColumns = null;
            this.dgvBlkChild.TabIndex = 24;
            this.dgvBlkChild.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvBlkChild_CellValidating);
            this.dgvBlkChild.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvBlkChild_EditingControlShowing);
            // 
            // PR_P_NO
            // 
            this.PR_P_NO.DataPropertyName = "PR_P_NO";
            this.PR_P_NO.HeaderText = "PR_P_NO";
            this.PR_P_NO.Name = "PR_P_NO";
            this.PR_P_NO.Visible = false;
            // 
            // ChildName
            // 
            this.ChildName.DataPropertyName = "PR_CHILD_NAME";
            this.ChildName.HeaderText = "Child Name";
            this.ChildName.MaxInputLength = 30;
            this.ChildName.Name = "ChildName";
            this.ChildName.Width = 130;
            // 
            // Date_of_Birth
            // 
            this.Date_of_Birth.DataPropertyName = "PR_DATE_BIRTH";
            dataGridViewCellStyle1.Format = "dd/MM/yyyy";
            dataGridViewCellStyle1.NullValue = null;
            this.Date_of_Birth.DefaultCellStyle = dataGridViewCellStyle1;
            this.Date_of_Birth.HeaderText = "Date of Birth";
            this.Date_of_Birth.MaxInputLength = 10;
            this.Date_of_Birth.Name = "Date_of_Birth";
            this.Date_of_Birth.Width = 120;
            // 
            // CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 414);
            this.Controls.Add(this.pnlSplBlkPersMain);
            this.Controls.Add(this.slPanelSimple1);
            this.Controls.Add(this.pnlTblBlkChild);
            this.Name = "CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS";
            this.Text = "CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS";
            this.Controls.SetChildIndex(this.pnlTblBlkChild, 0);
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            this.Controls.SetChildIndex(this.pnlSplBlkPersMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlSplBlkPersMain.ResumeLayout(false);
            this.pnlSplBlkPersMain.PerformLayout();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.pnlTblBlkChild.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkChild)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlSplBlkPersMain;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblBlkChild;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBlkChild;
        private System.Windows.Forms.Label label34;
        private CrplControlLibrary.SLTextBox txt_PR_ADD2;
        private System.Windows.Forms.Label label53;
        private CrplControlLibrary.SLTextBox txt_PR_PHONE1;
        private System.Windows.Forms.Label label52;
        private CrplControlLibrary.SLTextBox txt_PR_PHONE2;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private CrplControlLibrary.SLTextBox txt_PR_ID_CARD_NO;
        private System.Windows.Forms.Label label49;
        private CrplControlLibrary.SLTextBox txt_PR_SEX;
        private System.Windows.Forms.Label label48;
        private CrplControlLibrary.SLTextBox txt_PR_OLD_ID_CARD_NO;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label45;
        private CrplControlLibrary.SLTextBox txt_PR_LANG1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label44;
        private CrplControlLibrary.SLTextBox txt_PR_LANG2;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label43;
        private CrplControlLibrary.SLTextBox txt_PR_LANG3;
        private CrplControlLibrary.SLTextBox txt_PR_ADD1;
        private System.Windows.Forms.Label label42;
        private CrplControlLibrary.SLTextBox txt_PR_LANG4;
        private CrplControlLibrary.SLTextBox txt_PR_LANG6;
        private System.Windows.Forms.Label label41;
        private CrplControlLibrary.SLTextBox txt_PR_LANG5;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private CrplControlLibrary.SLTextBox txt_PR_SPOUSE;
        private CrplControlLibrary.SLDatePicker txt_PR_D_BIRTH;
        private CrplControlLibrary.SLTextBox txt_PR_NO_OF_CHILD;
        private CrplControlLibrary.SLDatePicker txt_PR_BIRTH_SP;
        private CrplControlLibrary.SLDatePicker txt_PR_MARRIAGE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txt_PR_GROUP_HOSP;
        private CrplControlLibrary.SLTextBox txt_PR_OPD;
        private CrplControlLibrary.SLTextBox txt_PR_GROUP_LIFE;
        private CrplControlLibrary.SLTextBox txt_PR_PA_NO;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txt_w_date_2;
        public CrplControlLibrary.SLTextBox txt_PR_MARITAL;
        public CrplControlLibrary.SLTextBox txt_PR_USER_IS_PRIME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChildName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date_of_Birth;

    }
}