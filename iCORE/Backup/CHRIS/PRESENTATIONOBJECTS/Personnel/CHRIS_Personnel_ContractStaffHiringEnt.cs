using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using CrplControlLibrary;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_ContractStaffHiringEnt : ChrisMasterDetailForm
    {
        #region --Variable--

        int TotalContribution   = 0;
        string PersNumber       = string.Empty;
        bool validateFrom       = true;
        bool frmSave            = false;
        #endregion

        #region Constructor

        public CHRIS_Personnel_ContractStaffHiringEnt()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_ContractStaffHiringEnt(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(pnlDept);
            pnlDetail.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlDetail);
            this.CurrentPanelBlock = pnlDetail.Name;

            //this.tbtCancel.Available = false;
            this.tbtDelete.Available = false;
            //this.tbtEdit.Available = false;
            this.tbtList.Available = false;
            this.tbtSave.Available = false;

            this.txtOption.Enabled = true;
            this.txtOption.Select();

            //this.dtpFromDate.Value = null;
            //this.dtpToDate.Value = null;
            //this.dtpDOB.Value = null;

            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
            this.FunctionConfig.OptionLetterF10 = "";
            this.dgvDept.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
            this.FunctionConfig.OptionLetterF10 = "";
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF5 = false;
            this.FunctionConfig.EnableF2 = false;
            this.lblUserName.Text = this.UserName;
            this.txtUser.Text = this.userID;
            this.txtLocation.Text = this.CurrentLocation;
            this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.dgvDept.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            tbtSave.Visible = true;
            this.txtBranch.SkipValidation = true;
            this.txtBranch.IsRequired = false;

            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;

            this.KeyPreview = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
            EnableControls(false);
            this.tbtList.Visible = false;
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save" || actionType == "Delete")
            {
                if (IsValidated() && this.FindForm().Validate())
                {
                    PersNumber = string.Empty;
                    PersNumber = txtPersNo.Text;
                    double contrib = 0;
                    if (dgvDept.DataSource as DataTable != null)
                        double.TryParse((dgvDept.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out contrib);
                    if (contrib == 100)
                    {
                        if (this.FunctionConfig.CurrentOption != Function.Add)
                        {
                            //CallReport("AUPR", "OLD");
                            //CallReport("AUP0", "NEW");
                        }
                        CallReport("AUPR", "OLD");

                        //if (!this.FindForm().Validate())
                        //    return;

                        base.DoToolbarActions(ctrlsCollection, actionType);

                        if (actionType == "Save" && this.FunctionConfig.CurrentOption == Function.Add)
                        {
                            this.txtPersNo.Text = (this.pnlDetail.CurrentBusinessEntity as iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ContractCommand).PR_P_NO.ToString();
                            PersNumber = txtPersNo.Text;//CallReport("AUPR", "OLD");
                            //CallReport("AUP0", "NEW");
                            lbtnPNo.Enabled = false;
                        }

                        if (true)/*this.FindForm().Validate()*/
                        {
                            CallReport("AUP0", "NEW");
                            this.Cancel();
                            PersNumber = string.Empty;
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("CONTRIBUTION MARGIN SHOULD BE LESS THAN OR EQUAL TO 100 ...", "Note", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    validateFrom = false;
                }
            }
            else
                base.DoToolbarActions(ctrlsCollection, actionType);
            if (actionType == "Cancel")
            {
                EnableControls(false);
                //this.dtpToDate.Value    = null;
                //this.dtpFromDate.Value  = null;
                //this.dtpDOB.Value       = null;
                //this.dtpFromDate.Value  = null;
                this.txtOption.Select();
                this.txtOption.Focus();
            }

            this.lblUserName.Text = this.UserName;
            this.txtUser.Text = this.UserName;
            this.txtLocation.Text = this.CurrentLocation;
            this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
        }

        private void ResetForm()
        {

            this.txtPersNo.Text = "";
            this.txtFirstName.Text = "";
            this.txtLastName.Text = "";
            this.txtBranch.Text = "";

            this.txtDesg.Text = "";
            this.txtContractor1.Text = "";
            this.txtContractorName.Text = "";
            //this.dtpFromDate.Value = null;
            //this.dtpToDate.Value = null;

            this.txtAddress.Text = "";
            this.txtPh1.Text = "";
            this.txtPh2.Text = "";
            //this.dtpDOB.Value = null;
            this.txtMarital.Text = "";
            this.txtSex.Text = "";
            this.txtNID.Text = "";
            this.txtSalary.Text = "0.00";

            this.txtPersNo.IsRequired = false;
            this.txtFirstName.IsRequired = false;
            this.txtBranch.IsRequired = false;

            this.txtDesg.IsRequired = false;
            this.txtContractor1.IsRequired = false;
            this.dtpFromDate.IsRequired = false;
            this.dtpToDate.IsRequired = false;

            this.dtpDOB.IsRequired = false;
            this.txtMarital.IsRequired = false;
            this.txtSex.IsRequired = false;
            this.txtNID.IsRequired = false;
            this.txtSalary.IsRequired = false;
            lbtnPNo.SkipValidationOnLeave = true;
            this.Cancel();
            lbtnPNo.SkipValidationOnLeave = false;
            this.txtUser.Text = this.UserName;
            this.txtLocation.Text = this.CurrentLocation;
            this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            this.CurrrentOptionTextBox = this.txtCurrOption;

            this.txtPersNo.IsRequired = true;
            this.txtFirstName.IsRequired = true;
            this.txtBranch.IsRequired = true;

            // this.txtDesg.IsRequired = true;
            this.txtContractor1.IsRequired = true;
            this.dtpFromDate.IsRequired = true;
            this.dtpToDate.IsRequired = true;

            this.dtpDOB.IsRequired = true;
            this.txtMarital.IsRequired = true;
            this.txtSex.IsRequired = true;
            this.txtNID.IsRequired = true;
            this.txtSalary.IsRequired = true;

            //DataTable dt = (DataTable)this.dgvDept.DataSource;
            //if (dt != null)
            //{
            //    dt.Rows.Clear();
            //}
        }

        private bool IsValidated()
        {
            bool validated = true;

            if (this.txtPersNo.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid Country Code.");
            }
            if (FunctionConfig.CurrentOption != Function.Delete)
            {
                if (this.txtFirstName.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
                }
                if (this.txtBranch.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryAc, "Mark as Authenticat/.");
                }
                if (this.txtDesg.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCitiMail, "Mark as Authenticat/.");
                }
                if (this.txtContractor1.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryCode, "Select valid Country Code.");
                }
                if (this.dtpFromDate.Value == null)
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
                }
                if (this.dtpToDate.Value == null)
                {
                    //validated = false;
                    //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
                }
                if (this.dtpDOB.Value == null)
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
                }
                if (this.txtMarital.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryAc, "Mark as Authenticat/.");
                }
                if (this.txtSex.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCitiMail, "Mark as Authenticat/.");
                }
                if (this.txtNID.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryAc, "Mark as Authenticat/.");
                }
                if (this.txtSalary.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCitiMail, "Mark as Authenticat/.");
                }

            }


            return validated;
        }

        protected override bool Add()
        {
            //this.ResetForm();
            base.Add();
            EnableControls(true);
            this.lbtnPNo.Enabled = false;
            this.lbtnPNo.ActionType = "PR_P_NO_LOV";
            this.lbtnPNo.ActionLOVExists = "PR_P_NO_LOV_EXISTS";
            this.lbtnPNo.SkipValidationOnLeave = true;

            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_CONTRACT_MANAGER", "SER_NO");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.txtPersNo.Text = rsltCode.dstResult.Tables[0].Rows[0][0] == null ? "" : rsltCode.dstResult.Tables[0].Rows[0][0].ToString();
                }
            }

            this.txtBranch.Select();
            this.txtBranch.Focus();

            // this.FunctionConfig.OptionLetterF10 = this.FunctionConfig.OptionLetterF1;

            return false;
        }

        protected override bool Edit()
        {
            //this.ResetForm();
            this.lbtnPNo.ActionType = "List";

            base.Edit();

            EnableControls(true);
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.txtPersNo.Select();

            // this.FunctionConfig.OptionLetterF10 = this.FunctionConfig.OptionLetterF7;

            return false;

        }
        protected override bool View()
        {
            //this.ResetForm(); ;
            this.lbtnPNo.ActionType = "List";
            this.lbtnPNo.SkipValidationOnLeave = false;

            base.View();
            EnableControls(true);
            this.txtOption.Text = "V";
            this.txtPersNo.Select();

            // this.FunctionConfig.OptionLetterF10 = this.FunctionConfig.OptionLetterF3;

            return false;
        }
        protected override bool Delete()
        {
            //this.Cancel();
            //this.ResetForm();
            this.lbtnPNo.ActionType = "List";

            base.Delete();
            EnableControls(true);
            this.txtPersNo.Select();
            this.FunctionConfig.CurrentOption = Function.Delete;

            //this.FunctionConfig.OptionLetterF10 = this.FunctionConfig.OptionLetterF4;

            return false;
        }
        protected override bool Save()
        {
            //DateTime SaveDate = new DateTime(1900, 1, 1);
            //if (dtpToDate.Value == null)
            //{
            //    dtpToDate.Value = SaveDate;
            //}
            if (!this.IsValidated())
                return false;

            bool flag = false;
            if (this.dgvDept.Rows.Count > 1)
            {
                if (!this.IsValidated())
                    return false;

                this.dgvDept.EndEdit();

                double totalCon;
                double.TryParse((dgvDept.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalCon);

                if (totalCon == 100)
                {
                    flag = base.Save();

                    if (validateFrom)
                    {
                        this.ResetForm();
                        this.txtOption.Select();
                    }
                }
                else
                {
                    MessageBox.Show("YOUR CONTRIBUTION % SUM FOR THE CURRENT ID IS EXCEEDING 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //flag = base.Save();

                //if (validateFrom)
                //{
                //    this.ResetForm();
                //    this.txtOption.Select();
                //}
            }
            else
            {
                this.FunctionConfig.CurrentOption = this.FunctionConfig.CurrentOption;
                this.ActiveControl.Select();
                this.ActiveControl.Focus();
            }
            //  this.lbtnPNo.Enabled = true;
            return flag;
        }

        protected override bool Cancel()
        {
            base.Cancel();
            EnableControls(false);
            //this.dtpToDate.Value = null;
            //this.dtpFromDate.Value = null;
            //this.dtpDOB.Value = null;
            //this.dtpFromDate.Value = null;
            this.txtOption.Select();
            this.txtOption.Focus();


            return false;
        }

        protected override bool Quit()
        {
            bool flag = false;
            this.txtPersNo.IsRequired = false;
            this.txtPersNo.SkipValidation = true;
            this.lbtnPNo.SkipValidationOnLeave = true;

            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                if (base.tlbMain.Items.Count > 0)
                {
                    base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));

                    //base.Close();
                    //this.Dispose(true);
                }
                //else

            }
            else
            {
                if (this.dgvDept.CurrentCell != null)
                {
                    this.dgvDept.CancelEdit();
                    this.FunctionConfig.CurrentOption = Function.None;
                }

                this.FunctionConfig.CurrentOption = Function.None;
                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
                EnableControls(false);

                //this.dtpToDate.Value = null;
                //this.dtpFromDate.Value = null;
                //this.dtpDOB.Value = null;
                //this.dtpFromDate.Value = null;
                this.txtOption.Select();
                this.txtOption.Focus();

            }
            //this.txtPersNo.IsRequired = true;
            //this.txtPersNo.SkipValidation = false;
            this.lbtnPNo.SkipValidationOnLeave = false;
            return flag;
        }

        private void EnableControls(bool yesOrNo)
        {
            this.lbtnBranch.Enabled = yesOrNo;
            this.lbtnContractor.Enabled = yesOrNo;
            this.lbtnDesg.Enabled = yesOrNo;
            this.lbtnPNo.Enabled = yesOrNo;
            this.dtpDOB.Enabled = yesOrNo;
            this.dtpFromDate.Enabled = yesOrNo;
            this.dtpToDate.Enabled = yesOrNo;

        }

        protected override void BindLOVShortCut(string pstrSPName, SLTextBox poSLTextBox)
        {
            if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                if (poSLTextBox.AssociatedLookUpName == lbtnPNo.Name)
                    return;
            }
            base.BindLOVShortCut(pstrSPName, poSLTextBox);
        }

        private void CallReport(string AuditPreOrAuditPo, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = this.GlobalReportPath;//@"C:\SPOOL\CHRIS\AUDIT\";
            string auditStatus = AuditPreOrAuditPo + DateTime.Now.ToString("yyyyMMddHms");
            //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            string FullPath = globalPath + auditStatus;
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "dt";
            txtDT.Text = DateTime.Now.ToString();//this.Now().ToShortDateString();
            pnl.Controls.Add(txtDT);


            CrplControlLibrary.SLTextBox txtCNo = new CrplControlLibrary.SLTextBox(comp);
            txtCNo.Name = "CNO";
            txtCNo.Text = PersNumber;//txtPersNo.Text;
            pnl.Controls.Add(txtCNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "user";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "st";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);




            frm.Controls.Add(pnl);
            frm.RptFileName = "AUDIT09A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }



        #endregion

        #region Event Handlers

        private void dgvDept_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int Sum1 = 0;
            int WVal1 = 0;

            try
            {
                if (dgvDept.CurrentRow.Index < dgvDept.Rows.Count && dgvDept.Rows.Count > 1)
                {
                    if (dgvDept.CurrentCell.OwningColumn.Name == "colSeg" && (dgvDept.CurrentCell.EditedFormattedValue.ToString() == ""))
                    {
                        MessageBox.Show("Enter 'GF' or 'GCB'", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                    else if (dgvDept.CurrentCell.OwningColumn.Name == "colDept" && (dgvDept.CurrentCell.EditedFormattedValue.ToString() == ""))
                    {
                        MessageBox.Show("Invalid Department Entered Press [F9] Key For Help", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                    else if (dgvDept.CurrentCell.OwningColumn.Name == "colContribution" && (dgvDept.CurrentCell.EditedFormattedValue.ToString() == "" || dgvDept.CurrentCell.EditedFormattedValue.ToString() == "0"))
                    {
                        e.Cancel = true;
                        return;
                    }
                }

                if (this.FunctionConfig.CurrentOption != Function.None)
                {
                    if (e.ColumnIndex == 0 && dgvDept.CurrentCell.IsInEditMode)
                    {
                        string txt = dgvDept.CurrentCell.EditedFormattedValue == null ? null : dgvDept.CurrentCell.EditedFormattedValue.ToString();
                        if (txt.ToUpper() == "GF" || txt.ToUpper() == "GCB")
                        {
                            dgvDept.CurrentCell.Value = txt.ToUpper();
                        }
                        else
                        {
                            MessageBox.Show("Enter 'GF' or 'GCB'", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                        }
                    }
                    if (dgvDept.CurrentCell.OwningColumn.Name == "colDept" && dgvDept.CurrentCell.IsInEditMode)
                    {
                        string txt1 = dgvDept.CurrentCell.EditedFormattedValue == null ? null : dgvDept.CurrentCell.EditedFormattedValue.ToString();
                        string txt2 = dgvDept.CurrentRow.Cells["colSeg"].Value.ToString();
                        Dictionary<string, object> paramDept = new Dictionary<string, object>();
                        paramDept.Add("PR_SEGMENT", txt2);
                        paramDept.Add("PR_DEPT", txt1);


                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();
                        rsltCode = cmnDM.GetData("CHRIS_SP_CONTR_HIRING_DEPT_CONT_MANAGER", "DEPT_VALID", paramDept);
                        if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {

                        }
                        else
                        {
                            MessageBox.Show("Invalid Department Entered Press [F9] Key For Help", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }

                    }

                    if (dgvDept.CurrentCell.OwningColumn.Name == "colContribution" && dgvDept.CurrentCell.IsInEditMode)
                    {

                        if (dgvDept.CurrentCell.EditedFormattedValue != null && dgvDept.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                        {
                            Double GlobalSum = 0;
                            Double Contribution = 0;
                            Double PreviousValue = 0;
                            if (dgvDept[e.ColumnIndex, e.RowIndex].Value != null)
                                double.TryParse(dgvDept[e.ColumnIndex, e.RowIndex].Value.ToString(), out PreviousValue);

                            Double.TryParse(dgvDept[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString(), out Contribution);
                            Contribution = Math.Round(Contribution, 0, MidpointRounding.AwayFromZero);
                            double totalCon = 0;

                            if (Contribution > 100)
                            {
                                MessageBox.Show("CONTRIBUTION MARGIN SHOULD BE LESS THAN OR EQUAL TO 100 ...", "Note", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                //MessageBox.Show("If The Contribution Is Equal To 100 Then Press SAVE Button To Save The Record", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                e.Cancel = true;

                                return;
                            }
                            else if (Contribution <= 100)
                            {
                                if (dgvDept.DataSource as DataTable != null)
                                    double.TryParse((dgvDept.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalCon);
                                totalCon = totalCon + Contribution - PreviousValue;

                                if (totalCon < 100)
                                {
                                    dgvDept.CurrentCell.Value = Contribution;
                                }
                                if (totalCon == 100)
                                {
                                    dgvDept.CurrentCell.Value = Contribution;

                                    //txtTRemarks.Focus();
                                }
                                if (totalCon > 100)
                                {
                                    MessageBox.Show("YOUR CONTRIBUTION % SUM FOR THE CURRENT ID IS EXCEEDING 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    totalCon = totalCon - Contribution;
                                    e.Cancel = true;
                                }

                                if (dgvDept.CurrentRow.Index == (dgvDept.Rows.Count - 2))
                                {
                                    if (totalCon == 100 && frmSave == false)
                                    {
                                        dgvDept.CurrentCell.Value = Contribution;
                                        frmSave = true;
                                        AddModifyFunc();
                                        frmSave = false;
                                        return;
                                    }
                                }
                                totalCon = 0;
                            }

                        }
                    }
                    dgvDept.EndEdit();
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dgvDept_CellValidating", exp);
            }
        }

        private void CHRIS_Personnel_ContractStafHiringEnt_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            //if (actionType != "List")
            //    return;

            try
            {
                switch (this.FunctionConfig.CurrentOption)
                {
                    case Function.Delete:
                        if (actionType == "List" || actionType == "PR_P_NO_LOV_EXISTS")
                        {
                            DialogResult dr = MessageBox.Show("Do you want to delete the record.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dr == DialogResult.Yes)
                            {
                                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));
                                this.Delete();
                                //this.ResetForm();
                                //txtOption.Text = "D";
                                //txtPersNo.Select();
                                //txtPersNo.Focus();
                            }
                            else
                            {

                                this.Quit();

                                //this.ResetForm();
                                txtOption.Select();
                                txtOption.Focus();
                            }
                        }
                        break;

                    case Function.Modify:
                        break;

                    case Function.View:
                        if (actionType == "List")
                        {
                            DialogResult dr1 = MessageBox.Show("Do you want to View more records.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dr1 == DialogResult.Yes)
                            {
                                //txtOption.Text = "V";
                                this.Quit();
                                //this.ResetForm();
                                this.View();
                                txtPersNo.Select();
                                txtPersNo.Focus();
                            }
                            else
                            {
                                this.ResetForm();
                                txtOption.Select();
                                txtOption.Focus();
                            }
                        }
                        else if (actionType.ToUpper() == "PR_P_NO_LOV_EXISTS")
                        {
                            if (selectedRow != null)
                            {
                                DialogResult dr1 = MessageBox.Show("Do you want to View more records.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (dr1 == DialogResult.Yes)
                                {
                                    //txtOption.Text = "V";
                                    this.Quit();
                                    //this.ResetForm();
                                    this.View();
                                    txtPersNo.Select();
                                    txtPersNo.Focus();
                                }
                                else
                                {
                                    this.Quit();
                                    //this.ResetForm();
                                    txtOption.Select();
                                    txtOption.Focus();
                                }
                            }

                        }
                        break;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CHRIS_Personnel_ContractStafHiringEnt_AfterLOVSelection", exp);
            }
        }

        private void txtPersNo_Enter(object sender, EventArgs e)
        {
            //if (this.FunctionConfig.CurrentOption == Function.Add)
            //{
            //    this.txtBranch.Select();
            //    this.txtBranch.Focus();
            //}
        }

        /// <summary>
        /// DateofBirth should be greater than or equal to 180 months diff
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpDOB_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                int DateDiff;

                if (dtpDOB.Value == null)
                    e.Cancel = true;


                if (DateTime.Compare(Convert.ToDateTime(dtpDOB.Value), (Convert.ToDateTime(dtpFromDate.Value))) > 0)//|| dtpFromDate.Value == null)
                {
                    MessageBox.Show("Birth Date can not be Greater Than From Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_FROM_DATE", dtpFromDate.Value);
                param.Add("PR_DATE_BIRTH", dtpDOB.Value);


                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_CONTRACT_MANAGER", "MONTH_BETWEEN", param);
                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    DateDiff = int.Parse(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                    if (DateDiff <= 180)
                    {
                        MessageBox.Show("Age Should be 15 years Less than From Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dtpDOB_Validating", exp);
            }
        }

        /// <summary>
        /// To Date Must Be Greater Than From Date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpToDate_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (dtpToDate.Value != null)
                {
                    DateTime dt1 = DateTime.Parse(dtpFromDate.Value.ToString()).Date;
                    DateTime dt2 = DateTime.Parse(dtpToDate.Value.ToString()).Date;

                    if (DateTime.Compare(dt1, dt2) >= 0)
                    {
                        MessageBox.Show("To Date Must Be Greater Then From Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dtpToDate_Validating", exp);
            }
        }


        /// <summary>
        /// Both Telephone Number Cannot be same
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPh2_Validating(object sender, CancelEventArgs e)
        {
            if (txtPh1.Text == txtPh2.Text && txtPh1.Text != string.Empty && txtPh2.Text != string.Empty)
            {
                MessageBox.Show("Both Telephone No. Cannot Be Same", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void txtMarital_Validating(object sender, CancelEventArgs e)
        {
            if (txtMarital.Text != "M" && txtMarital.Text != "S")
            {
                MessageBox.Show("Please [M]arried or [S]ingle", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void txtSex_Validating(object sender, CancelEventArgs e)
        {
            if (txtSex.Text != "M" && txtSex.Text != "F")
            {
                MessageBox.Show("Press [M]ale or [F]emale", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void txtBranch_Validating(object sender, CancelEventArgs e)
        {
            //if (this.txtBranch.Text == "")
            //{
            //    this.ResetForm();
            //    this.txtOption.Select();
            //    this.txtOption.Focus();
            //}
        }

        private void lbtn_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                Button btn = (Button)sender;
                if (btn != null)
                {
                    btn.Enabled = false;
                    btn.Enabled = true;
                }
            }
        }

        private void txtBranch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' && this.txtBranch.Text == "")
            {
                this.ResetForm();
                this.txtOption.Select();
                this.txtOption.Focus();
            }
        }

        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Modifiers == Keys.Control)
                {
                    switch (e.KeyValue)
                    {
                        case 34:
                            AddModifyFunc();
                            this.KeyPreview = true;
                            break;
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "ShortCutKey_Press", exp);
            }
        }

        public void AddModifyFunc()
        {
            if (txtOption.Text == "A" || txtOption.Text == "M")
            {

                double totalCon;
                double.TryParse((dgvDept.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalCon);

                if (totalCon == 100)
                {
                    DialogResult dRslt = MessageBox.Show("Do You Want To Save The Record [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (dRslt == DialogResult.Yes)
                    {
                        base.Save();
                        if (validateFrom)
                        {
                            ClearForm(pnlDept.Controls);
                            ClearForm(pnlDetail.Controls);
                            //dtpDOB.Value = null;
                            //dtpFromDate.Value = null;
                            //dtpToDate.Value = null;
                        }
                        else
                        {
                            validateFrom = true;
                            return;
                        }
                    }
                    else if (dRslt == DialogResult.No)
                    {
                        ClearForm(pnlDept.Controls);
                        ClearForm(pnlDetail.Controls);
                        //dtpDOB.Value = null;
                        //dtpFromDate.Value = null;
                        //dtpToDate.Value = null;
                        txtOption.Select();
                        txtOption.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("YOUR CONTRIBUTION % SUM FOR THE CURRENT ID IS EXCEEDING 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void txtNID_Validating(object sender, CancelEventArgs e)
        {
            bool ValidNIC = true;
            if (txtNID.Text != string.Empty)
            {
                ValidNIC = IsValidExpression(txtNID.Text, InputValidator.OldNICNO_REGEX);

                if (!ValidNIC)
                {
                    MessageBox.Show("Please Enter NID Card Number in 999-99-999999 Format.", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }


        }

        private void dgvDept_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (this.dgvDept.CurrentRow != null && this.dgvDept.CurrentRow.IsNewRow)
            {
                if (this.dgvDept.CurrentCell.ColumnIndex == this.dgvDept.Columns["colContribution"].Index && !this.dgvDept.CurrentCell.IsInEditMode)
                    if (e.KeyData == Keys.Tab)
                    {
                        this.dgvDept.CurrentCell = this.dgvDept["colDept", dgvDept.CurrentRow.Index];
                        //e.IsInputKey = false;
                    }
            }
        }

        private void dgvDept_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }

        #endregion

        private void dgvDept_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (!this.dgvDept.ContainsFocus)
                return;
            dgvDept.BeginEdit(true);
        }
    }
}