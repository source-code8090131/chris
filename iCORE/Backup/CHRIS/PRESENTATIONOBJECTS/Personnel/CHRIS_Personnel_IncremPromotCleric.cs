using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using System.Threading;
using iCORE.Common;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.CHRIS.DATAOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_IncremPromotCleric : ChrisMasterDetailForm
    {
        #region Data Members
        CHRIS_Personnel_IncrementOfficers_Designation Staff_Desig;
        CHRIS_Personnel_IncrementOfficers_Level Staff_Level;
        CHRIS_Personnel_IncrementOfficersClerical_Course Staff_Course;
        string globalPNo = "";
        string globalOption = "";

        string strType = string.Empty;
        string strOption = string.Empty;
        int NOInc = 0;
 
        #endregion

        #region Constructors
        public CHRIS_Personnel_IncremPromotCleric()
        {
            InitializeComponent();
        }


        public CHRIS_Personnel_IncremPromotCleric(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            //lstDependentPanels.Add(PnlDetails);
            lstDependentPanels.Add(pnlCourseDtl);
            this.pnlDetail.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlDetail);



        }
        #endregion

        #region Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);


            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
          
            tbtDelete.Enabled = false;
           
            tbtAdd.Visible = false;
            tbtSave.Visible = false;
            tbtDelete.Visible = false;

            tbtList.Available = false;

            this.txtUserName.Text = "User Name: " + this.UserName;

            this.FunctionConfig.F6 = Function.Quit;
        
            this.FunctionConfig.F10 = Function.Save;
            this.FunctionConfig.EnableF10 = true;

            this.FunctionConfig.EnableF8 = false;
            this.CurrrentOptionTextBox  = this.txtCurrOption;
            dtpPR_LAST_APP.Value        = null;
            dtpNextAppraisalDate.Value  = null;
            dtpEffectiveDate.Value      = null;
            dtpToDate.Value             = null;

            txtDate.Text = Now().ToString("dd/MM/yyyy");

        }
        
        protected override bool Add()
        {

            
            base.Add();
            txttextMode.Text = "A";
            this.txtOption.Text = "A";
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            lbtnIncrementType.SkipValidationOnLeave = true;
            lbtnEffectiveDate.SkipValidationOnLeave = true;
            return false;
        }

        protected override bool Edit()
        {
            base.Edit();
            txttextMode.Text = "M";
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            this.viewoption.Text = "Y";
            return false;

        }

        protected override bool Delete()
        {
            base.Delete();
            txttextMode.Text = "D";
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            return false;
        }

        protected override bool View()
        {
            base.View();
            txtOption.Text = "V";
            txttextMode.Text = "V";
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View;
            this.viewoption.Text = "Y";
            return false;
        }

        public void CallQuit()
        {
            base.Quit();
        }

        /// <summary>
        /// Check the all the required Form field.
        /// </summary>
        /// <returns></returns>
        public bool frmValidation()
        {
            bool flag = true;

            if (txtPersNo.Text == string.Empty)
                flag = false;
            else if (txtType.Text == string.Empty)
                flag = false;
            else if (dtpEffectiveDate.Value == null)
                flag = false;
            else if (txtNoINCR.Text == string.Empty && txtType.Text != "P" && txtType.Text != "T")
                flag = false;
            else if (txtPR_REMARKS.Text == string.Empty)
                flag = false;

            return flag;
        }


        protected override bool Save()
        {
            if (this.FindForm().Validate() && frmValidation())
            {
                DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                       , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dRes == DialogResult.Yes)
                {
                    preCommit();
                    strType = txtType.Text;
                    globalOption = txttextMode.Text;
                    lbtnPNo.SkipValidationOnLeave = true;
                    base.DoToolbarActions(this.Controls, "Save");
                    this.DoToolbarActions(this.PnlDetails.Controls, "Cancel");
                    this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                    lbtnPNo.SkipValidationOnLeave = false;
                    #region Continue The Process For More Records

                    DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Y]es or [N]o..", "Note"
                                   , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes1 == DialogResult.Yes)
                    {

                        if (this.globalOption == "A")
                        {
                            base.IterateFormToEnableControls(pnlDetail.Controls, true);
                            this.Add();
                            txtPersNo.Select();
                            txtPersNo.Focus();

                        }
                        else if (this.globalOption == "M")
                        {
                            base.IterateFormToEnableControls(pnlDetail.Controls, true);
                            this.Edit();
                            txtPersNo.Select();
                            txtPersNo.Focus();

                        }

                    }
                    else if (dRes1 == DialogResult.No)
                    {
                        strOption = "No";
                        this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                        return false;
                    }

                    #endregion

                    //call save
                }
                else if (dRes == DialogResult.No)
                {
                    globalOption = txtOption.Text;
                    base.DoToolbarActions(this.Controls, "Cancel");
                    //txtOption.Focus();
                    #region Continue The Process For More Records

                    DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Y]es or [N]o..", "Note"
                                   , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes1 == DialogResult.Yes)
                    {

                        if (this.globalOption == "A")
                        {
                            base.IterateFormToEnableControls(pnlDetail.Controls, true);
                            this.Add();
                            txtPersNo.Select();
                            txtPersNo.Focus();

                        }
                        else if (this.globalOption == "M")
                        {
                            base.IterateFormToEnableControls(pnlDetail.Controls, true);
                            this.Edit();
                            txtPersNo.Select();
                            txtPersNo.Focus();

                        }

                    }
                    else if (dRes1 == DialogResult.No)
                    {
                        this.DoToolbarActions(this.Controls, "Cancel");
                        txtOption.Focus();
                    }

                    #endregion
                }
            }
            return false;
        }

        public void CallSave(CHRIS_Personnel_IncrementOfficersClerical_Course frmDialog)
        {
            this.Save();
            frmDialog.Close();
            //base.Cancel();
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                this.dtpEffectiveDate.Value     = null;
                this.dtpNextAppraisalDate.Value = null;
                this.dtpToDate.Value            = null;
                this.dtpPR_LAST_APP.Value       = null;

                this.AutoValidate = AutoValidate.Disable;
                this.txtPersNo.IsRequired = false;
                this.FunctionConfig.CurrentOption = Function.None;
                this.txtOption.Select();
                this.txtOption.Focus();
                base.DoToolbarActions(ctrlsCollection, actionType);
                this.txtOption.Select();
                this.txtOption.Focus();
                this.txtPersNo.IsRequired = true;
                txtDate.Text = Now().ToString("dd/MM/yyyy");
                return;
            }
            if (actionType == "Save")
            {
                base.IterateFormToEnableControls(pnlDetail.Controls, false);
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
            txtDate.Text    = Now().ToString("dd/MM/yyyy");

            if (actionType == "Save")
            {
                base.IterateFormToEnableControls(pnlDetail.Controls, false);
            }

        }

        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }

        private void Reset()
        {
           



        }

        #endregion

        #region Event Handlers

        private void txtPersNo_Validating(object sender, CancelEventArgs e)
        {
            if (!lbtnPNo.Focused)
            {
                if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                    return;
                if (this.FunctionConfig.CurrentOption == Function.None)
                    return;

                
                if (txtPersNo.Text != string.Empty)
                {
                    NOInc = 0;
                    DataTable dtmvd_count;

                    Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                    colsNVals.Clear();
                    colsNVals.Add("PR_IN_NO", txtPersNo.Text);
                    if (this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete)
                    {
                        dtmvd_count = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "PROMOTION_COUNT", colsNVals);
                        if (dtmvd_count != null)
                        {
                            if (dtmvd_count.Rows.Count <= 0)
                            {
                                this.Reset();
                                txtOption.Focus();
                                e.Cancel = true;
                                MessageBox.Show("NO DATA EXISTS FOR THIS PERSONNEL NO.");
                            }
                        }
                        else
                        {
                            this.Reset();
                            txtOption.Focus();
                            e.Cancel = true;
                            MessageBox.Show("NO DATA EXISTS FOR THIS PERSONNEL NO.");
                        }
                    }
                }
            }
            else
            {
                txtPersNo.Select();
                txtPersNo.Focus();
            }
        }

        private void txtType_Validating(object sender, CancelEventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed )
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;
            
            if (txtType.Text != string.Empty)
            {
                DataTable dtProc7;
                DataTable dtProc1;

                Dictionary<string, object> colsNVals1 = new Dictionary<string, object>();
                colsNVals1.Clear();
                colsNVals1.Add("PR_IN_NO", txtPersNo.Text);
                colsNVals1.Add("PR_INC_TYPE", txtType.Text);
                if ((txtType.Text != "C" && txtType.Text != "T" && txtType.Text != "Y" && txtType.Text != "P") || txtType.Text == string.Empty)
                {
                    txtType.Focus();
                    MessageBox.Show("YOU HAVE TO ENTER T,C , Y or P'");
                    e.Cancel = true;
                }
                else
                {
                    if (this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete)
                    {
                        // PROC_7;
                        dtProc7 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "PROC7", colsNVals1);
                        if (dtProc7 != null)
                        {
                            if (dtProc7.Rows.Count == 0)
                            {
                                this.Reset();
                                txtOption.Focus();
                                MessageBox.Show("NO DATA EXISTS FOR THIS PERSONNEL NO.");
                            }
                            else if (dtProc7.Rows.Count > 1)
                            {
                                this.Reset();
                                txtOption.Focus();
                            }
                        }
                    }
                    else if (this.FunctionConfig.CurrentOption == Function.View)
                    {
                        //  PROC_1;
                        dtProc1 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "PROC1", colsNVals1);
                        if (dtProc1 != null)
                        {
                            if (dtProc1.Rows.Count <= 0)
                            {
                                this.Reset();
                                txtOption.Focus();
                                MessageBox.Show("NO DATA EXISTS FOR THIS PERSONNEL NO.");
                            }
                            else if (dtProc1.Rows.Count > 1)
                            {
                                this.Reset();
                                txtOption.Focus();
                            }
                        }
                    }
                    else
                    {
                        if (txtType.Text == "P")
                        {
                            // GO_FIELD('BLK_HEAD.W_ANS5');
                            DialogResult dRes = MessageBox.Show("Is The Promotion To Officer [Y]es or [N]o", "Note"
                                            , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dRes == DialogResult.Yes)
                            {
                                txtAnsW5.Text = "YES";
                                //Umair
                                //show_page(10);
                                //SHOW_PAGE(12);
                                
                                return;
                            }
                            else if (dRes == DialogResult.No)
                            {
                                // show_page(6);
                                txtAnsW5.Text = "NO";
                                return;
                            }
                        }
                    }
                }
            }
        }

        private void dtpEffectiveDate_Validating(object sender, CancelEventArgs e)
        {
            if (!lbtnEffectiveDate.Focused)
            {
                if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                    return;
                if (this.FunctionConfig.CurrentOption == Function.None)
                    return;

                DataTable dtCheck;
                DataTable dtList;
                DataTable dtCheckAdd;
                DataTable dtCheckIfAdd;
                if (dtpEffectiveDate.Value == null)
                {

                    MessageBox.Show("VALUE HAS TO BE ENTERED WHICH SHOULD BE OF THE CURRENT YEAR");
                    dtpEffectiveDate.Focus();
                    e.Cancel = true;
                    return;


                }
                else
                {
                    DateTime date1 = Convert.ToDateTime(dtpEffectiveDate.Value);
                    DateTime date2 = Convert.ToDateTime(dtjoinDate.Value);
                    int result = DateTime.Compare(date1.Date, date2.Date);
                    if (result < 0)
                    {
                        MessageBox.Show("DATE HAS TO BE GREATER THAN JOINING DATE :" + date2.ToString("dd/MM/yyyy"));
                        dtpEffectiveDate.Focus();
                        e.Cancel = true;
                        return;
                    }

                    // These need to check after Add
                    if (this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete || this.FunctionConfig.CurrentOption == Function.View)
                    {
                        //Umair
                        if (txtType.Text == "C")
                        {
                            FillGrid();
                        }

                        Dictionary<string, object> colsNVals2 = new Dictionary<string, object>();
                        colsNVals2.Clear();
                        colsNVals2.Add("PR_IN_NO",      txtPersNo.Text);
                        colsNVals2.Add("PR_INC_TYPE",   txtType.Text);
                        colsNVals2.Add("PR_EFFECTIVE",  Convert.ToDateTime(dtpEffectiveDate.Value));
                        dtCheck = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "CheckIfExists", colsNVals2);
                        
                        if (dtCheck != null)
                        {
                            if (dtCheck.Rows.Count > 0)
                            {
                                Dictionary<string, object> colsNVals3 = new Dictionary<string, object>();
                                colsNVals3.Clear();
                                colsNVals3.Add("PR_IN_NO",      txtPersNo.Text);
                                colsNVals3.Add("PR_INC_TYPE",   txtType.Text);
                                colsNVals3.Add("PR_EFFECTIVE",  Convert.ToDateTime(dtpEffectiveDate.Value));
                                dtList = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "List", colsNVals3);
                                
                                if (dtList != null)
                                {
                                    if (dtList.Rows.Count == 1)
                                    {
                                        SetFields(dtList);
                                        if (txtAnsW5.Text == "YES")
                                        {
                                            //go_block('blktwo');
                                            //execute_query;
                                            Staff_Desig = new CHRIS_Personnel_IncrementOfficers_Designation("Desination");
                                            Staff_Desig.ShowDialog();
                                            txtPR_DESIG_PRESENT.Text    = Staff_Desig.desig;
                                            txtPR_LEVEL_PRESENT.Text    = Staff_Desig.level;
                                            txtPR_FUNCT_1_PRESENT.Text  = Staff_Desig.functit1;
                                            txtPR_FUNCT_2_PRESENT.Text  = Staff_Desig.functit2;
                                            //Staff_Desig.Close();
                                            txtPR_INC_AMT.Select();
                                            txtPR_INC_AMT.Focus();
                                        }
                                        else
                                        {
                                            //go_field('blkone.pr_level_present');
                                            //show_page(6);

                                            //----- changed by umair 
                                            //if (this.FunctionConfig.CurrentOption != Function.Modify && (this.FunctionConfig.CurrentOption != Function.View && txtType.Text!="C"))
                                            {
                                                if (txtType.Text == "C" && this.FunctionConfig.CurrentOption == Function.View && this.FunctionConfig.CurrentOption != Function.Modify)
                                                {
                                                    Staff_Course = new CHRIS_Personnel_IncrementOfficersClerical_Course(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvCourseDtl.GridSource);
                                                    Staff_Course.MdiParent = null;
                                                    Staff_Course.ShowDialog();
                                                }
                                                else
                                                {
                                                    Staff_Level                 = new CHRIS_Personnel_IncrementOfficers_Level(this);
                                                    Staff_Level.StartPosition   = FormStartPosition.CenterScreen;
                                                    Staff_Level.ShowDialog();

                                                }
                                                
                                                if (this.FunctionConfig.CurrentOption == Function.None)
                                                {
                                                    txtOption.Select();
                                                    txtOption.Focus();
                                                }
                                                return;
                                            }
                                        }


                                        if (this.FunctionConfig.CurrentOption == Function.Modify)
                                        {
                                            //show_page(6);
                                            //:w_no := :pr_no_incr;
                                            //go_field('blkone.pr_rank');
                                            txtWNo.Text = txtNoINCR.Text;
                                            txtRank.Focus();
                                            Staff_Level = new CHRIS_Personnel_IncrementOfficers_Level(this);
                                            Staff_Level.ShowDialog();
                                            txtRank.Focus();

                                        }
                                        if (this.FunctionConfig.CurrentOption == Function.Delete)
                                        {
                                            //GO_FIELD('BLK_HEAD.W_ANS3');

                                        }

                                        if (this.FunctionConfig.CurrentOption == Function.View)
                                        {
                                            /*** Display course block if type = 'C' */
                                            // if :blkone.pr_inc_type='C' then
                                            // go_block('blkcourse');
                                            // execute_query;
                                            // message('PRESS ENTER TO PROCEED ...');PAUSE;
                                            // end if;
                                            ///** end of check */
                                            //GO_FIELD('BLK_HEAD.W_ANS');
                                            IncrementPromotionClericalCommand ent = (IncrementPromotionClericalCommand)pnlDetail.CurrentBusinessEntity;
                                            if (txtPersNo.Text != string.Empty)
                                            {
                                                ent.PR_IN_NO = Convert.ToDecimal(txtPersNo.Text);
                                            }
                                            if (dtpEffectiveDate.Value != null)
                                            {
                                                ent.PR_EFFECTIVE = (DateTime)dtpEffectiveDate.Value;
                                            }
                                            pnlDetail.LoadDependentPanels();
                                            if (txtType.Text == "C")
                                            {
                                                Staff_Course = new CHRIS_Personnel_IncrementOfficersClerical_Course(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, DGVCourse.GridSource);

                                                Staff_Course.MdiParent = null;

                                                Staff_Course.ShowDialog();
                                                if (this.FunctionConfig.CurrentOption == Function.None)
                                                {
                                                    this.txtOption.Select();
                                                    this.txtOption.Focus();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (dtCheck.Rows.Count == 0)
                            {
                                MessageBox.Show("NO DATA FOUND PRESS [F9] KEY");

                                dtpEffectiveDate.Focus();
                                e.Cancel = true;
                                return;
                            }
                            else
                            {
                                this.Reset();
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("NO DATA FOUND PRESS [F9] KEY");
                            dtpEffectiveDate.Focus();
                            e.Cancel = true;
                            return;
                        }
                    }
                    // These need to check after Add

                    if (this.FunctionConfig.CurrentOption == Function.Add)
                    {
                        DateTime showDate1 = new DateTime();
                        Dictionary<string, object> colsNVals3 = new Dictionary<string, object>();
                        colsNVals3.Clear();
                        colsNVals3.Add("PR_IN_NO", txtPersNo.Text);
                        colsNVals3.Add("PR_EFFECTIVE", Convert.ToDateTime(dtpEffectiveDate.Value));
                        dtCheckAdd = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "CheckIfExistsForADD", colsNVals3);
                        if (dtCheckAdd != null)
                        {
                            if (dtCheckAdd.Rows.Count == 1)
                            {
                                if (dtCheckAdd.Rows[0].ItemArray[0].ToString() != string.Empty)
                                {
                                    showDate1 = Convert.ToDateTime(dtCheckAdd.Rows[0].ItemArray[0]);
                                    MessageBox.Show("ALREADY EXISTS.DATE HAS TO BE GREATER THAN " + showDate1.ToString("dd/MM/yyyy"));
                                    dtpEffectiveDate.Focus();
                                    e.Cancel = true;
                                    return;
                                }
                            }
                            if (dtCheckAdd.Rows.Count == 0)
                            {
                                //    IF :BLKONE.PR_INC_TYPE = 'P' THEN
                                //    IF :W_ANS = 'Y' THEN
                                //       SHOW_PAGE(10);
                                //       SHOW_PAGE(12);
                                //    ELSE
                                //       SHOW_PAGE(6);
                                //       NEXT_FIELD;
                                //   END IF;
                                //ELSE
                                //   NEXT_FIELD;
                                //END IF;
                            }
                            else
                            {
                                DateTime showDate = new DateTime();
                                Dictionary<string, object> colsNVals4 = new Dictionary<string, object>();
                                colsNVals4.Clear();
                                colsNVals4.Add("PR_IN_NO", txtPersNo.Text);
                                dtCheckIfAdd = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "CheckIfADD", colsNVals4);
                                if (dtCheckIfAdd != null)
                                {
                                    if (dtCheckIfAdd.Rows.Count > 0)
                                    {
                                        if (dtCheckIfAdd.Rows[0].ItemArray[0].ToString() != string.Empty)
                                        {
                                            showDate = Convert.ToDateTime(dtCheckIfAdd.Rows[0].ItemArray[0]);
                                            MessageBox.Show("ALREADY EXISTS.DATE HAS TO BE GREATER THAN " + showDate.ToString("dd/MM/yyyy"));
                                            dtpEffectiveDate.Focus();
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void SetFields(DataTable ListFields)
        {
            if (ListFields != null)
            {
                if (ListFields.Rows.Count == 1)
                {
                    txtRank.Text = ListFields.Rows[0].ItemArray[3].ToString();//"PR_RANK"
                    if (ListFields.Rows[0].ItemArray[4].ToString() != string.Empty)//PR_RANKING_DATE
                    {
                        dtpToDate.Value = Convert.ToDateTime(ListFields.Rows[0].ItemArray[4]);
                    }
                    txtNoINCR.Text      = ListFields.Rows[0].ItemArray[22].ToString();//PR_NO_INCR
                    txtPR_INC_AMT.Text  = ListFields.Rows[0].ItemArray[6].ToString();//PR_INC_AMT
                    txtStepAmount.Text  = ListFields.Rows[0].ItemArray[24].ToString();//PR_STEP_AMT
                    txtNoOfMonths.Text  = ListFields.Rows[0].ItemArray[23].ToString();//PR_NO_MONTHS

                    if (ListFields.Rows[0].ItemArray[8].ToString() != string.Empty) //PR_LAST_APP
                        dtpPR_LAST_APP.Value = Convert.ToDateTime(ListFields.Rows[0].ItemArray[8]);

                    txtPRAnnualPresent.Text = ListFields.Rows[0].ItemArray[11].ToString();//PR_ANNUAL_PRESENT
                    
                    if (ListFields.Rows[0].ItemArray[9].ToString() != string.Empty)  ///PR_NEXT_APP
                        dtpNextAppraisalDate.Value = Convert.ToDateTime(ListFields.Rows[0].ItemArray[9]);
                    
                    txtPR_REMARKS.Text          = ListFields.Rows[0].ItemArray[10].ToString(); ///PR_REMARKS
                    txtPR_FUNCT_1_PRESENT.Text  = ListFields.Rows[0]["PR_FUNCT_1_PRESENT"].ToString(); 
                    txtPR_FUNCT_2_PRESENT.Text  = ListFields.Rows[0]["PR_FUNCT_2_PRESENT"].ToString(); 
                    txtPR_DESIG_PRESENT.Text    = ListFields.Rows[0]["PR_DESIG_PRESENT"].ToString(); 
                    txtPR_LEVEL_PRESENT.Text    = ListFields.Rows[0]["PR_LEVEL_PRESENT"].ToString(); 
                    txtPR_PROMOTED.Text         = ListFields.Rows[0]["PR_PROMOTED"].ToString(); 
                    txtPR_LEVEL_PREVIOUS.Text   = ListFields.Rows[0]["PR_LEVEL_PREVIOUS"].ToString(); 
                    txtPR_DESIG_PREVIOUS.Text   = ListFields.Rows[0]["PR_DESIG_PREVIOUS"].ToString(); 
                    txtPR_FUNCT_1_PREVIOUS.Text = ListFields.Rows[0]["PR_FUNCT_1_PREVIOUS"].ToString(); 
                    txtPR_FUNCT_2_PREVIOUS.Text = ListFields.Rows[0]["PR_FUNCT_2_PREVIOUS"].ToString(); 
                    txtAnnualPrevious.Text      = ListFields.Rows[0]["PR_ANNUAL_PREVIOUS"].ToString(); 
                    txtprCurrentPrevious.Text   = ListFields.Rows[0]["PR_CURRENT_PREVIOUS"].ToString(); 
                                                                                                        
                    IncrementPromotionClericalCommand ent = (IncrementPromotionClericalCommand)pnlDetail.CurrentBusinessEntity;
                   ent.ID =int.Parse(ListFields.Rows[0]["ID"].ToString());
                }
            }
        }

        private void dtpToDate_Validating(object sender, CancelEventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;

            if (dtpToDate != null)
            {
                DateTime date1 = Convert.ToDateTime(dtpToDate.Value);
                DateTime date2 = Convert.ToDateTime(dtpEffectiveDate.Value);
                DateTime date3 = Convert.ToDateTime(dtjoinDate.Value);

                int result1 = DateTime.Compare(date1, date3);
                if (result1 < 0)
                {
                    MessageBox.Show("DATE HAS TO BE GREATER THAN JOINING DATE :" + date3.ToString("dd-MMM-yyyy"));
                    dtpToDate.Focus();
                    return;
                }
                else
                {
                    

                 int result = DateTime.Compare(date1, date2);
                if (result > 0)
                {
                    MessageBox.Show("DATE HAS TO BE SMALLER THAN EFFECTIVE DATE");
                    dtpToDate.Focus();
                    return;
                }


                }
                if (txtType.Text == "P")
                {
                    if (txtAnsW5.Text == "NO")
                    {
                        // Open Level PopUP
                        Staff_Desig                 = new CHRIS_Personnel_IncrementOfficers_Designation("Level");
                        Staff_Desig.ShowDialog();
                        //txtPR_INC_AMT.Enabled       = false;
                        txtPR_DESIG_PRESENT.Text    = Staff_Desig.desig;
                        txtPR_LEVEL_PRESENT.Text    = Staff_Desig.level;
                        txtPR_FUNCT_1_PRESENT.Text  = Staff_Desig.functit1;
                        txtPR_FUNCT_2_PRESENT.Text  = Staff_Desig.functit2;
                        txtNoINCR.Select();
                        txtNoINCR.Focus();
                        return;
                    }
                    else
                    {
                        if (this.FunctionConfig.CurrentOption == Function.Add)
                        {
                            //  if :w_option = 'A' then
                            //   GO_FIELD('BLKTWO.PR_DESIG_PRESENT');
                            //  else
                            //   go_field('blkone.pr_level_present');
                            //  end if;
                            // END IF;
                            Staff_Desig = new CHRIS_Personnel_IncrementOfficers_Designation("Designation");
                            Staff_Desig.ShowDialog();

                            txtNoINCR.Enabled = false;
                            txtPR_DESIG_PRESENT.Text        = Staff_Desig.desig;
                            txtPR_LEVEL_PRESENT.Text        = Staff_Desig.level;
                            txtPR_FUNCT_1_PRESENT.Text      = Staff_Desig.functit1;
                            txtPR_FUNCT_2_PRESENT.Text      = Staff_Desig.functit2;
                            txtPR_INC_AMT.Select();
                            txtPR_INC_AMT.Focus();
                            return;

                        }

                        else
                        {
                            Staff_Level = new CHRIS_Personnel_IncrementOfficers_Level(this);
                            Staff_Level.ShowDialog();
                            return;
                           

                        }
                    }

                }
                else if (txtType.Text == "T")
                {
                    txtNoOfMonths.Focus();
                    txtNoINCR.Enabled = false;
                    txtPR_INC_AMT.Enabled = false;
                    txtStepAmount.Enabled = false;

                    return;
                }

                else
                {
                    txtNoINCR.Focus();
                    return;
                }

            }
           
        }

        private void txtNoINCR_Validating(object sender, CancelEventArgs e)
        {
            bool flag = false;
            if (txtNoINCR.Text != string.Empty)
            {
                #region Key Next for No of Increment
                flag = keyNextForNo_of_Increments();
                if (flag)
                {
                    e.Cancel = true;
                    return;
                }
                if (txtType.Text == "C")
                {
                    txtPR_INC_AMT.Enabled       = false;
                    txtStepAmount.Enabled       = false;
                    txtNoOfMonths.Enabled       = false;
                    dtpPR_LAST_APP.Enabled      = false;
                    txtPRAnnualPresent.Enabled  = false;
                    txtwBasic.Enabled           = false;
                }
                else if (txtType.Text == "Y")
                {
                    txtPR_INC_AMT.Enabled       = false;
                    txtStepAmount.Enabled       = false;
                    txtNoOfMonths.Enabled       = false;
                    dtpPR_LAST_APP.Enabled      = false;
                    txtPRAnnualPresent.Enabled  = false;
                    txtwBasic.Enabled           = false;
                }
                else if (txtType.Text == "P" && txtAnsW5.Text == "NO" )
                {
                    txtPR_INC_AMT.Enabled       = false;
                    txtStepAmount.Enabled       = false;
                    txtNoOfMonths.Enabled       = false;
                    dtpPR_LAST_APP.Enabled      = false;
                    txtPRAnnualPresent.Enabled  = false;
                    txtwBasic.Enabled           = false;     
                }
                #endregion
            }
            else
            {
                MessageBox.Show("YOU HAVE ENTERED A VERY LARGE INCREMENT");
                e.Cancel = true;
                return;
            }
        }


        /// <summary>
        /// // Key NextItem for No_of_Increments
        /// </summary>
        private bool keyNextForNo_of_Increments()
        {
            DataTable dtIncrementCount;
            DataTable dtExistingIncrement;
            DataTable dtExistingDesig;
            DataTable dtExistingIncrementPersonnel;
            DataTable dtExistingDesigPersonnel;
            DataTable dtproc5;
            DataTable dtproc6;
            int CntNo=0;
            bool flag = false;
            Dictionary<string, object> paramkeyNextIncrement1 = new Dictionary<string, object>();
            paramkeyNextIncrement1.Clear();
            paramkeyNextIncrement1.Add("PR_IN_NO", txtPersNo.Text);

            dtIncrementCount = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "CountIncrements", paramkeyNextIncrement1);
            if (dtIncrementCount != null)
            {
                if (dtIncrementCount.Rows[0]["CNT"].ToString() != string.Empty)
                {
                    int.TryParse(dtIncrementCount.Rows[0]["CNT"].ToString(), out CntNo);
                }
            }
            if (txtType.Text != "P")
            {
               
                //////////////////////////////////////////

                if (CntNo > 0)
                {
                    Dictionary<string, object> paramkeyNextIncrement2 = new Dictionary<string, object>();
                    paramkeyNextIncrement2.Clear();
                    paramkeyNextIncrement2.Add("PR_IN_NO", txtPersNo.Text);

                    dtExistingIncrement = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "IncrementExistLevel", paramkeyNextIncrement2);

                    if (dtExistingIncrement != null)
                    {
                        if (dtExistingIncrement.Rows.Count > 0)
                        {
                            if (dtExistingIncrement.Rows[0]["PR_ANNUAL_PRESENT"].ToString() != string.Empty)
                            {
                                //PR_ANNUAL_PREVIOUS
                                    txtAnnualPrevious.Text = dtExistingIncrement.Rows[0]["PR_ANNUAL_PREVIOUS"].ToString();
                                //txtAnnualPrevious.Text = dtExistingIncrement.Rows[0]["PR_ANNUAL_PRESENT"].ToString();
                            }
                            if (dtExistingIncrement.Rows[0]["PR_LEVEL_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PRESENT.Text = dtExistingIncrement.Rows[0]["PR_LEVEL_PRESENT"].ToString();
                            }
                            if (dtExistingIncrement.Rows[0]["PR_FUNCT_1_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PRESENT.Text = dtExistingIncrement.Rows[0]["PR_FUNCT_1_PRESENT"].ToString();
                            }

                            if (dtExistingIncrement.Rows[0]["PR_FUNCT_2_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PRESENT.Text = dtExistingIncrement.Rows[0]["PR_FUNCT_2_PRESENT"].ToString();
                            }
                        }
                    }


                    //////////////////////////////////////////////////////////

                    Dictionary<string, object> paramkeyNextIncrement3 = new Dictionary<string, object>();
                    paramkeyNextIncrement3.Clear();
                    paramkeyNextIncrement3.Add("PR_IN_NO", txtPersNo.Text);

                    dtExistingDesig = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "IncrementExistDesig", paramkeyNextIncrement3);


                    if (dtExistingDesig != null)
                    {
                        if (dtExistingDesig.Rows.Count > 0)
                        {
                            if (dtExistingDesig.Rows[0]["pr_DESIG_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_DESIG_PREVIOUS.Text = dtExistingDesig.Rows[0]["pr_DESIG_PRESENT"].ToString();
                            }
                            if (dtExistingDesig.Rows[0]["PR_LEVEL_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PREVIOUS.Text = dtExistingDesig.Rows[0]["PR_LEVEL_PRESENT"].ToString();
                            }
                            if (dtExistingDesig.Rows[0]["PR_FUNCT_1_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PREVIOUS.Text = dtExistingDesig.Rows[0]["PR_FUNCT_1_PRESENT"].ToString();
                            }

                            if (dtExistingDesig.Rows[0]["PR_FUNCT_2_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PREVIOUS.Text = dtExistingDesig.Rows[0]["PR_FUNCT_2_PRESENT"].ToString();
                            }
                        }
                    }

                }

                if (CntNo == 0)
                {
                    Dictionary<string, object> paramPersonnel1 = new Dictionary<string, object>();
                    paramPersonnel1.Clear();
                    paramPersonnel1.Add("PR_IN_NO", txtPersNo.Text);

                    dtExistingIncrementPersonnel = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "PersonnelExistLevel", paramPersonnel1);

                    if (dtExistingIncrementPersonnel != null)
                    {
                        if (dtExistingIncrementPersonnel.Rows.Count > 0)
                        {
                            if (dtExistingIncrementPersonnel.Rows[0]["PR_NEW_ANNUAL_PACK"].ToString() != string.Empty)
                            {

                                txtAnnualPrevious.Text = dtExistingIncrementPersonnel.Rows[0]["PR_NEW_ANNUAL_PACK"].ToString();
                            }
                            if (dtExistingIncrementPersonnel.Rows[0]["PR_LEVEL"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PRESENT.Text = dtExistingIncrementPersonnel.Rows[0]["PR_LEVEL"].ToString();
                            }
                            if (dtExistingIncrementPersonnel.Rows[0]["PR_FUNC_TITTLE1"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PRESENT.Text = dtExistingIncrementPersonnel.Rows[0]["PR_FUNC_TITTLE1"].ToString();
                            }

                            if (dtExistingIncrementPersonnel.Rows[0]["PR_FUNC_TITTLE2"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PRESENT.Text = dtExistingIncrementPersonnel.Rows[0]["PR_FUNC_TITTLE2"].ToString();
                            }
                        }
                    }
                    //////////////////////////////////////////////////////////
                    Dictionary<string, object> paramPersonnel2 = new Dictionary<string, object>();
                    paramPersonnel2.Clear();
                    paramPersonnel2.Add("PR_IN_NO", txtPersNo.Text);

                    dtExistingDesigPersonnel = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "PersonnelExistDesig", paramPersonnel2);


                    if (dtExistingDesigPersonnel != null)
                    {
                        if (dtExistingDesigPersonnel.Rows.Count > 0)
                        {
                            if (dtExistingDesigPersonnel.Rows[0]["PR_DESIG"].ToString() != string.Empty)
                            {

                                txtPR_DESIG_PREVIOUS.Text = dtExistingDesigPersonnel.Rows[0]["PR_DESIG"].ToString();
                            }
                            if (dtExistingDesigPersonnel.Rows[0]["PR_LEVEL"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PREVIOUS.Text = dtExistingDesigPersonnel.Rows[0]["PR_LEVEL"].ToString();
                            }
                            if (dtExistingDesigPersonnel.Rows[0]["PR_FUNC_TITTLE1"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PREVIOUS.Text = dtExistingDesigPersonnel.Rows[0]["PR_FUNC_TITTLE1"].ToString();
                            }

                            if (dtExistingDesigPersonnel.Rows[0]["PR_FUNC_TITTLE2"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PREVIOUS.Text = dtExistingDesigPersonnel.Rows[0]["PR_FUNC_TITTLE2"].ToString();
                            }
                        }
                    }



                }


               flag= PROC_5();
               

            }


            else
            {
                if (CntNo == 0)
                {
                    Dictionary<string, object> paramP1 = new Dictionary<string, object>();
                    paramP1.Clear();
                    paramP1.Add("PR_IN_NO", txtPersNo.Text);

                    dtproc5 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "Proc5_4", paramP1);
                    if (dtproc5 != null)
                    {
                        if (dtproc5.Rows.Count > 0)
                        {
                            if (dtproc5.Rows[0]["PR_NEW_ANNUAL_PACK"].ToString() != string.Empty)
                            {

                                txtAnnualPrevious.Text = dtproc5.Rows[0]["PR_NEW_ANNUAL_PACK"].ToString();
                            }
                            if (dtproc5.Rows[0]["PR_DESIG"].ToString() != string.Empty)
                            {

                                txtPR_DESIG_PREVIOUS.Text = dtproc5.Rows[0]["PR_DESIG"].ToString();
                            }

                            if (dtproc5.Rows[0]["PR_LEVEL"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PREVIOUS.Text = dtproc5.Rows[0]["PR_LEVEL"].ToString();
                            }
                            if (dtproc5.Rows[0]["PR_FUNC_TITTLE1"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PREVIOUS.Text = dtproc5.Rows[0]["PR_FUNC_TITTLE1"].ToString();
                            }

                            if (dtproc5.Rows[0]["PR_FUNC_TITTLE2"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PREVIOUS.Text = dtproc5.Rows[0]["PR_FUNC_TITTLE2"].ToString();
                            }
                        }

                    }


                }

                else
                {


                    Dictionary<string, object> paramP2 = new Dictionary<string, object>();
                    paramP2.Clear();
                    paramP2.Add("PR_IN_NO", txtPersNo.Text);

                    dtproc6 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "Proc5_5", paramP2);
                    if (dtproc6 != null)
                    {
                        if (dtproc6.Rows.Count > 0)
                        {
                            if (dtproc6.Rows[0]["PR_ANNUAL_PRESENT"].ToString() != string.Empty)
                            {

                                txtAnnualPrevious.Text = dtproc6.Rows[0]["PR_ANNUAL_PRESENT"].ToString();
                            }
                            if (dtproc6.Rows[0]["PR_DESIG_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_DESIG_PREVIOUS.Text = dtproc6.Rows[0]["PR_DESIG_PRESENT"].ToString();
                            }

                            if (dtproc6.Rows[0]["PR_LEVEL_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PREVIOUS.Text = dtproc6.Rows[0]["PR_LEVEL_PRESENT"].ToString();
                            }
                            if (dtproc6.Rows[0]["PR_FUNCT_1_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PREVIOUS.Text = dtproc6.Rows[0]["PR_FUNCT_1_PRESENT"].ToString();
                            }

                            if (dtproc6.Rows[0]["PR_FUNCT_2_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PREVIOUS.Text = dtproc6.Rows[0]["PR_FUNCT_2_PRESENT"].ToString();
                            }
                        }

                    }

                }

                flag=PROC_5();
            }

            PROC_9();

            return flag;

        }

        /// <summary>
        /// // Proc 5 Implement prgm unit PROC_5
        /// </summary>
        private bool PROC_5()
        {
            DataTable dtProc5_Number;
            DataTable dtProc5_Num1;
            DataTable dtProc5_Num2;
            double NUMB1=0;
            double NUMB2=0;
            double NUMB3=0;
            double NUMB4=0;
            double NUMB5=0;
            double NUMB6=0;
            double NUMB7=0;
            double NUMB8=0;
            int pr_noINCR = 0;
            double PRANNUAL_PREVIOUS=0;
            double w_No=0;
            double stepAmt=0;
            DateTime DateAppNext = new DateTime();
            int curNoInc = 0;
            
            if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                txtWNo.Text = "0";
            }
            
            //////////////////////////////////////////////
            Dictionary<string, object> param1 = new Dictionary<string, object>();
            param1.Clear();
            param1.Add("pr_new_branch", txtprBranch.Text);
            
            //param1.Add("PR_LEVEL_PRESENT", txtPR_LEVEL_PRESENT.Text);
            param1.Add("PR_LEVEL_PRESENT", txtPR_LEVEL_PREVIOUS.Text); 

            dtProc5_Number = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "Proc5_1", param1);

            if (dtProc5_Number != null)
            {
                if (dtProc5_Number.Rows.Count > 0)
                {
                    if (dtProc5_Number.Rows[0]["SP_MIN"].ToString() != string.Empty)
                    {
                        double.TryParse(dtProc5_Number.Rows[0]["SP_MIN"].ToString(), out NUMB1);
                    }
                    if (dtProc5_Number.Rows[0]["SP_MAX"].ToString() != string.Empty)
                    {
                        double.TryParse(dtProc5_Number.Rows[0]["SP_MAX"].ToString(), out NUMB2);
                    }

                    if (dtProc5_Number.Rows[0]["SP_INCREMENT"].ToString() != string.Empty)
                    {
                        double.TryParse(dtProc5_Number.Rows[0]["SP_INCREMENT"].ToString(), out NUMB3);
                    }

                    //////// In case of txtType != "p"
                    if (txtAnnualPrevious.Text != string.Empty)
                    {
                        double.TryParse(txtAnnualPrevious.Text, out PRANNUAL_PREVIOUS);
                    }

                    if (txtNoINCR.Text != string.Empty)
                    {
                        int.TryParse(txtNoINCR.Text, out pr_noINCR);
                    }
                        if (txtType.Text != "P")
                        {
                            DataTable dtnoInc = new DataTable();
                            Result rsltCode;
                            Dictionary<string, object> paramInc = new Dictionary<string, object>();
                            CmnDataManager cmnDM = new CmnDataManager();
                            paramInc.Clear();
                            paramInc.Add("PR_IN_NO", txtPersNo.Text);
                            paramInc.Add("PR_EFFECTIVE", dtpEffectiveDate.Value);
                            rsltCode = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "CHK_NO_OF_INC", paramInc);

                            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables.Count > 0 && NOInc.Equals(0))
                            {
                                NOInc = Convert.ToInt32(rsltCode.dstResult.Tables[0].Rows[0]["PR_NO_INCR"].ToString());
                            }
                                
                            curNoInc = Convert.ToInt32(txtNoINCR.Text);
                            paramInc.Clear();


                            if (txtWNo.Text != string.Empty)
                            {
                                double.TryParse(txtWNo.Text, out w_No);
                            }
                            NUMB4 = pr_noINCR * NUMB3 * 12;
                            //// NUMB5 := :BLKONE.PR_ANNUAL_PREVIOUS + NUMB4 - (:w_no * numb3 * 12);

                            NUMB5 = PRANNUAL_PREVIOUS + NUMB4 - (w_No * NUMB3 * 12);

                            if (NUMB5 <= NUMB2)
                            {
                                if (curNoInc != NOInc) //&& this.txtOption.Text == "M")
                                {
                                    txtPRAnnualPresent.Text = NUMB5.ToString();
                                    txtPR_INC_AMT.Text      = NUMB4.ToString();
                                }

                                if (this.FunctionConfig.CurrentOption != Function.Modify)
                                {
                                    Dictionary<string, object> param2 = new Dictionary<string, object>();
                                    param2.Clear();
                                    param2.Add("PR_IN_NO", txtPersNo.Text);

                                    dtProc5_Num1 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "Proc5_2", param2);
                                    if (dtProc5_Num1 != null)
                                    {
                                        if (dtProc5_Num1.Rows.Count == 1)
                                        {
                                            if (dtProc5_Num1.Rows[0]["PR_EFFECTIVE"].ToString() != string.Empty)
                                            {
                                                if (DateTime.TryParse(dtProc5_Num1.Rows[0]["PR_EFFECTIVE"].ToString(), out DateAppNext))
                                                {
                                                    dtpPR_LAST_APP.Value = DateAppNext;
                                                }
                                            }
                                        }
                                    }
                                }
                                dtpNextAppraisalDate.Focus();
                            }
                            else
                            {
                                //  CLEAR_FIELD;
                                // MESSAGE('YOU HAVE ENTERED A VERY LARGE INCREMENT');
                                MessageBox.Show("YOU HAVE ENTERED A VERY LARGE INCREMENT");
                                return true;
                            }
                        }
                        else
                        {
                            if (PRANNUAL_PREVIOUS > NUMB1)
                            {
                                NUMB4 = PRANNUAL_PREVIOUS / 12;
                                NUMB5 = NUMB1 / 12;
                                NUMB6 = 0;
                                NUMB8 = 0;

                                while (NUMB4 > NUMB5)
                                {
                                    NUMB5 = NUMB5 + NUMB3;
                                    NUMB8 = NUMB8 + 1;
                                }

                                NUMB6 = NUMB3 * pr_noINCR;
                                NUMB7 = NUMB6 + NUMB5;
                                NUMB7 = NUMB7 * 12;

                                if (NUMB7 <= NUMB2)
                                {
                                    txtPRAnnualPresent.Text = NUMB7.ToString();
                                    txtPR_INC_AMT.Text = NUMB6.ToString();
                                    stepAmt = NUMB8 * NUMB3;
                                    txtStepAmount.Text = stepAmt.ToString();
                                    //////////// App Date
                                    Dictionary<string, object> param3 = new Dictionary<string, object>();
                                    param3.Clear();
                                    param3.Add("PR_IN_NO", txtPersNo.Text);

                                    dtProc5_Num2 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "Proc5_3", param3);
                                    if (dtProc5_Num2 != null)
                                    {
                                        if (dtProc5_Num2.Rows.Count == 1)
                                        {
                                            if (dtProc5_Num2.Rows[0]["PR_EFFECTIVE"].ToString() != string.Empty)
                                            {

                                                if (DateTime.TryParse(dtProc5_Num2.Rows[0]["PR_EFFECTIVE"].ToString(), out DateAppNext))
                                                {
                                                    dtpPR_LAST_APP.Value = DateAppNext;
                                                }
                                            }
                                        }
                                    }
                                   dtpNextAppraisalDate.Focus();
                                }
                                else
                                {
                                    MessageBox.Show("YOU HAVE ENTERED A VERY LARGE INCREMENT");
                                    return true;
                                }
                            }
                            else
                            {
                                NUMB4 = pr_noINCR * (NUMB3 * 12);
                                NUMB5 = NUMB1 + NUMB4;
                                if (NUMB5 <= NUMB2)
                                {
                                    txtPRAnnualPresent.Text = NUMB5.ToString();
                                    txtPR_INC_AMT.Text = NUMB4.ToString();

                                    //// Date App
                                    Dictionary<string, object> param4 = new Dictionary<string, object>();
                                    param4.Clear();
                                    param4.Add("PR_IN_NO", txtPersNo.Text);

                                    dtProc5_Num2 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "Proc5_3", param4);
                                    if (dtProc5_Num2 != null)
                                    {
                                        if (dtProc5_Num2.Rows.Count == 1)
                                        {
                                            if (dtProc5_Num2.Rows[0]["PR_EFFECTIVE"].ToString() != string.Empty)
                                            {
                                                if (DateTime.TryParse(dtProc5_Num2.Rows[0]["PR_EFFECTIVE"].ToString(), out DateAppNext))
                                                {
                                                    dtpPR_LAST_APP.Value = DateAppNext;
                                                }
                                            }
                                        }
                                    }
                                    dtpNextAppraisalDate.Focus();
                                }
                                else
                                {
                                    MessageBox.Show("YOU HAVE ENTERED A VERY LARGE INCREMENT");
                                    return true;
                                }

                            }
                        }
                }
            }
            NOInc = Convert.ToInt32(txtNoINCR.Text);
            return false;
        }


        /// <summary>
        /// // Proc 9 Implement prgm unit PROC_9
        /// </summary>

        private void PROC_9()
        {
            double WBasic = 0;
            double AnnualPresent=0;
           //IF :W_ANS5 = 'Y' THEN
           //   :W_BASIC := (NVL(:PR_ANNUAL_PRESENT,0) / 150 * 100) / 12;
           //ELSE
           //   :W_BASIC := NVL(:PR_ANNUAL_PRESENT,0) / 12 ;
           //END IF;
            double.TryParse(txtPRAnnualPresent.Text, out AnnualPresent);

            if (txtAnsW5.Text == "YES")
            {
                WBasic = (AnnualPresent / 150 * 100) / 12;
            }
            else
            {
                WBasic = (AnnualPresent  / 12);
            }

            WBasic = Math.Round(WBasic, 3);
            txtwBasic.Text = WBasic.ToString();
        }

        private void txtPR_INC_AMT_Validating(object sender, CancelEventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;

            DataTable dtCount;
            DataTable dtproc5a;
             DataTable dtproc5b;
            DataTable dtprocNew;
            DataTable dtProcNew1;
             double NUMB9=0;
             double NUMB10=0;
             double NUMB11=0;
             double NUMB12=0;
             double NUMB13=0;
            double PrIncAmt=0;
            double PrAnnual=0;
            DateTime DateAppNext = new DateTime();
            if (txtPR_INC_AMT.Text != string.Empty)
            {

                int CntNo = 0;

                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Clear();
                param1.Add("PR_IN_NO", txtPersNo.Text);

                dtCount = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "CountIncrements", param1);
                if (dtCount != null)
                {
                    if (dtCount.Rows[0]["CNT"].ToString() != string.Empty)
                    {
                        int.TryParse(dtCount.Rows[0]["CNT"].ToString(), out CntNo);
                    }
                }

                if (CntNo == 0)
                {
                    Dictionary<string, object> paramP1 = new Dictionary<string, object>();
                    paramP1.Clear();
                    paramP1.Add("PR_IN_NO", txtPersNo.Text);

                    dtproc5a = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "Proc5_4", paramP1);
                    if (dtproc5a != null)
                    {
                        if (dtproc5a.Rows.Count > 0)
                        {
                            if (dtproc5a.Rows[0]["PR_NEW_ANNUAL_PACK"].ToString() != string.Empty)
                            {

                                txtAnnualPrevious.Text = dtproc5a.Rows[0]["PR_NEW_ANNUAL_PACK"].ToString();
                            }
                            if (dtproc5a.Rows[0]["PR_DESIG"].ToString() != string.Empty)
                            {

                                txtPR_DESIG_PREVIOUS.Text = dtproc5a.Rows[0]["PR_DESIG"].ToString();
                            }

                            if (dtproc5a.Rows[0]["PR_LEVEL"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PREVIOUS.Text = dtproc5a.Rows[0]["PR_LEVEL"].ToString();
                            }
                            if (dtproc5a.Rows[0]["PR_FUNC_TITTLE1"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PREVIOUS.Text = dtproc5a.Rows[0]["PR_FUNC_TITTLE1"].ToString();
                            }

                            if (dtproc5a.Rows[0]["PR_FUNC_TITTLE2"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PREVIOUS.Text = dtproc5a.Rows[0]["PR_FUNC_TITTLE2"].ToString();
                            }
                        }

                    }
                }

                else
                {
                     Dictionary<string, object> paramP2 = new Dictionary<string, object>();
                    paramP2.Clear();
                    paramP2.Add("PR_IN_NO", txtPersNo.Text);

                    dtproc5b = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "Proc5_5", paramP2);
                    if (dtproc5b != null)
                    {
                        if (dtproc5b.Rows.Count > 0)
                        {
                            if (dtproc5b.Rows[0]["PR_ANNUAL_PRESENT"].ToString() != string.Empty)
                            {

                                txtAnnualPrevious.Text = dtproc5b.Rows[0]["PR_ANNUAL_PRESENT"].ToString();
                            }

                            if (dtproc5b.Rows[0]["PR_LEVEL_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PREVIOUS.Text = dtproc5b.Rows[0]["PR_LEVEL_PRESENT"].ToString();
                            }
                            if (dtproc5b.Rows[0]["PR_FUNCT_1_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PREVIOUS.Text = dtproc5b.Rows[0]["PR_FUNCT_1_PRESENT"].ToString();
                            }

                            if (dtproc5b.Rows[0]["PR_FUNCT_2_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PREVIOUS.Text = dtproc5b.Rows[0]["PR_FUNCT_2_PRESENT"].ToString();
                            }
                        }
                    }



                }

                 Dictionary<string, object> paramP3= new Dictionary<string, object>();
                    paramP3.Clear();
                    paramP3.Add("PR_DESIG_PRESENT", txtPR_DESIG_PRESENT.Text);
                    paramP3.Add("PR_LEVEL_PRESENT", txtPR_LEVEL_PRESENT.Text);
                    paramP3.Add("pr_new_branch", txtprBranch.Text);

                    dtprocNew = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "INC_AMT_1", paramP3);
                    if (dtprocNew != null)
                    {
                        if (dtprocNew.Rows[0]["SP_MAX"].ToString() != string.Empty)
                        {

                            double.TryParse(dtprocNew.Rows[0]["SP_MAX"].ToString(), out NUMB9);

                        }

                        if (dtprocNew.Rows[0]["SP_MIN"].ToString() != string.Empty)
                        {

                            double.TryParse(dtprocNew.Rows[0]["SP_MIN"].ToString(), out NUMB12);

                        }

                    }
                if(txtPR_INC_AMT.Text !=string.Empty)
                {
                double.TryParse(txtPR_INC_AMT.Text,out PrIncAmt);
                }

                if(txtAnnualPrevious.Text !=string.Empty)
                {
                    double.TryParse(txtAnnualPrevious.Text, out PrAnnual );

                }


                NUMB11 = PrIncAmt + PrAnnual;



                if (NUMB11 > NUMB9)
                {
                    NUMB10 = NUMB9 - PrAnnual;
                    MessageBox.Show("INCREMENT AMOUNT HAS TO BE <=" + NUMB10.ToString());



                }
                if (NUMB11 < NUMB12)
                {
                    NUMB13 = NUMB12 - PrAnnual;
                    MessageBox.Show("INCREMENT AMOUNT HAS TO BE >=" + NUMB13.ToString());


                }

                //// Date App

                Dictionary<string, object> param4 = new Dictionary<string, object>();
                param4.Clear();
                param4.Add("PR_IN_NO", txtPersNo.Text);


                dtProcNew1 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "Proc5_3", param4);
                if (dtProcNew1 != null)
                {
                    if (dtProcNew1.Rows.Count == 1)
                    {
                        if (dtProcNew1.Rows[0]["PR_EFFECTIVE"].ToString() != string.Empty)
                        {

                            if (DateTime.TryParse(dtProcNew1.Rows[0]["PR_EFFECTIVE"].ToString(), out DateAppNext))
                            {
                                dtpPR_LAST_APP.Value = DateAppNext;
                            }
                        }

                    }

                }

                txtPRAnnualPresent.Text = NUMB11.ToString();

                DateTime lastdate = Convert.ToDateTime("31-Dec-" + Now().Year.ToString());

                PROC_9();
               
                dtpNextAppraisalDate.Focus();

                if (txtType.Text == "P" && this.FunctionConfig.CurrentOption == Function.Add && txtAnsW5.Text == "YES")
                {
                    //var1                        = DateTime.Now.Month;
                    //var2                        = 12 - var1;
                    //AfterAddMonths              = DateTime.Now.AddMonths(var2);
                    //NewDate                     = GetLastDayOfMonth(AfterAddMonths);
                    //dtpNextAppraisalDate.Value  = NewDate;
                    dtpNextAppraisalDate.Value = lastdate;

                    if (txtNoINCR.Text == string.Empty)
                    {
                        txtNoINCR.Text = "0";
                    }
                    if (txtStepAmount.Text == string.Empty)
                    {
                        txtStepAmount.Text = "0";
                    }
                    
                    txtStepAmount.Enabled       = false;
                    txtNoOfMonths.Enabled       = false;
                    dtpPR_LAST_APP.Enabled      = false;
                    txtPRAnnualPresent.Enabled  = false;
                    txtwBasic.Enabled           = false;                    
                }
            }
        }

        private void txtNoOfMonths_Validating(object sender, CancelEventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;
            DataTable dtCount;
            DataTable dtno_of_months1;
            DataTable dtno_of_months2;
            DataTable dtno_of_months3;
            DataTable dtno_of_months4;
            if (txtNoOfMonths.Text != string.Empty)
            {
                int CntNo = 0;

                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Clear();
                param1.Add("PR_IN_NO", txtPersNo.Text);

                dtCount = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "CountIncrements", param1);
                if (dtCount != null)
                {
                    if (dtCount.Rows[0]["CNT"].ToString() != string.Empty)
                    {
                        int.TryParse(dtCount.Rows[0]["CNT"].ToString(), out CntNo);
                    }
                }

                if (CntNo > 0)
                {
                    Dictionary<string, object> paramNew2 = new Dictionary<string, object>();
                    paramNew2.Clear();
                    paramNew2.Add("PR_IN_NO", txtPersNo.Text);

                    dtno_of_months1 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "No_Of_Months1", paramNew2);
                    if (dtno_of_months1 != null)
                    {
                        if (dtno_of_months1.Rows.Count > 0)
                        {
                            if (dtno_of_months1.Rows[0]["PR_ANNUAL_PRESENT"].ToString() != string.Empty)
                            {

                                txtAnnualPrevious.Text = dtno_of_months1.Rows[0]["PR_ANNUAL_PRESENT"].ToString();
                            }

                            if (dtno_of_months1.Rows[0]["PR_ANNUAL_PRESENT"].ToString() != string.Empty)
                            {

                                txtPRAnnualPresent.Text = dtno_of_months1.Rows[0]["PR_ANNUAL_PRESENT"].ToString();
                            }
                            if (dtno_of_months1.Rows[0]["PR_DESIG_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_DESIG_PRESENT.Text = dtno_of_months1.Rows[0]["PR_DESIG_PRESENT"].ToString();
                            }

                            if (dtno_of_months1.Rows[0]["PR_LEVEL_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PRESENT.Text = dtno_of_months1.Rows[0]["PR_LEVEL_PRESENT"].ToString();
                            }

                            if (dtno_of_months1.Rows[0]["PR_FUNCT_1_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PRESENT.Text = dtno_of_months1.Rows[0]["PR_FUNCT_1_PRESENT"].ToString();
                            }
                            if (dtno_of_months1.Rows[0]["PR_FUNCT_2_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PRESENT.Text = dtno_of_months1.Rows[0]["PR_FUNCT_2_PRESENT"].ToString();
                            }
                        }
                    }
                    ////////////////

                     Dictionary<string, object> paramNew3 = new Dictionary<string, object>();
                    paramNew3.Clear();
                    paramNew3.Add("PR_IN_NO", txtPersNo.Text);

                    dtno_of_months2 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "No_Of_Months2", paramNew3);
                    if (dtno_of_months2 != null)
                    {
                        if (dtno_of_months2.Rows.Count > 0)
                        {
                            
                            if (dtno_of_months2.Rows[0]["PR_DESIG_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_DESIG_PREVIOUS.Text = dtno_of_months2.Rows[0]["PR_DESIG_PRESENT"].ToString();
                            }

                            if (dtno_of_months2.Rows[0]["PR_LEVEL_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PREVIOUS.Text = dtno_of_months2.Rows[0]["PR_LEVEL_PRESENT"].ToString();
                            }

                            if (dtno_of_months2.Rows[0]["PR_FUNCT_1_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PREVIOUS.Text = dtno_of_months2.Rows[0]["PR_FUNCT_1_PRESENT"].ToString();
                            }
                            if (dtno_of_months2.Rows[0]["PR_FUNCT_2_PRESENT"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PREVIOUS.Text = dtno_of_months2.Rows[0]["PR_FUNCT_2_PRESENT"].ToString();
                            }
                        }


                    }



                }
                else if (CntNo == 0)
                {

                   // No_Of_Months3

                    Dictionary<string, object> paramNew3 = new Dictionary<string, object>();
                    paramNew3.Clear();
                    paramNew3.Add("PR_IN_NO", txtPersNo.Text);

                    dtno_of_months3 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "No_Of_Months3", paramNew3);
                    if (dtno_of_months3 != null)
                    {
                        if (dtno_of_months3.Rows.Count > 0)
                        {
                            if (dtno_of_months3.Rows[0]["PR_NEW_ANNUAL_PACK"].ToString() != string.Empty)
                            {

                                txtAnnualPrevious.Text = dtno_of_months3.Rows[0]["PR_NEW_ANNUAL_PACK"].ToString();
                            }

                            if (dtno_of_months3.Rows[0]["PR_NEW_ANNUAL_PACK"].ToString() != string.Empty)
                            {

                                txtPRAnnualPresent.Text = dtno_of_months3.Rows[0]["PR_NEW_ANNUAL_PACK"].ToString();
                            }
                            if (dtno_of_months3.Rows[0]["PR_LEVEL"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PRESENT.Text = dtno_of_months3.Rows[0]["PR_LEVEL"].ToString();
                            }

                            if (dtno_of_months3.Rows[0]["PR_FUNC_TITTLE1"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PRESENT.Text = dtno_of_months3.Rows[0]["PR_FUNC_TITTLE1"].ToString();
                            }
                            if (dtno_of_months3.Rows[0]["PR_FUNC_TITTLE2"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PRESENT.Text = dtno_of_months3.Rows[0]["PR_FUNC_TITTLE2"].ToString();
                            }
                        }
                    }


                    Dictionary<string, object> paramNew4 = new Dictionary<string, object>();
                    paramNew4.Clear();
                    paramNew4.Add("PR_IN_NO", txtPersNo.Text);

                    dtno_of_months4 = GetData("CHRIS_SP_INC_PROMOTION_MANAGER", "No_Of_Months4", paramNew4);
                    if (dtno_of_months4 != null)
                    {
                        if (dtno_of_months4.Rows.Count > 0)
                        {

                            if (dtno_of_months4.Rows[0]["PR_DESIG"].ToString() != string.Empty)
                            {

                                txtPR_DESIG_PREVIOUS.Text = dtno_of_months4.Rows[0]["PR_DESIG"].ToString();
                            }

                            if (dtno_of_months4.Rows[0]["PR_LEVEL"].ToString() != string.Empty)
                            {

                                txtPR_LEVEL_PREVIOUS.Text = dtno_of_months4.Rows[0]["PR_LEVEL"].ToString();
                            }

                            if (dtno_of_months4.Rows[0]["PR_FUNC_TITTLE1"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_1_PREVIOUS.Text = dtno_of_months4.Rows[0]["PR_FUNC_TITTLE1"].ToString();
                            }
                            if (dtno_of_months4.Rows[0]["PR_FUNC_TITTLE2"].ToString() != string.Empty)
                            {

                                txtPR_FUNCT_2_PREVIOUS.Text = dtno_of_months4.Rows[0]["PR_FUNC_TITTLE2"].ToString();
                            }
                        }
                    }
                }
                if (txtType.Text == "T")
                {


                    txtPR_REMARKS.Focus();
                    dtpPR_LAST_APP.Enabled = false;
                    txtwBasic.Enabled = false;
                    txtPRAnnualPresent.Enabled = false;
                    dtpNextAppraisalDate.Enabled = false;
                }

            }

        }

        /// <summary> 

        /// Get the last day of the month for any 

        /// full date 

        /// </summary> 

        /// <param name="dtDate"></param> 

        /// <returns></returns> 
        /// 
        private DateTime GetLastDayOfMonth(DateTime dtDate)
        {

            // set return value to the last day of the month 

            // for any date passed in to the method 



            // create a datetime variable set to the passed in date 

            DateTime dtTo = dtDate;



            // overshoot the date by a month 

            dtTo = dtTo.AddMonths(1);



            // remove all of the days in the next month 

            // to get bumped down to the last day of the 

            // previous month 

            dtTo = dtTo.AddDays(-(dtTo.Day));



            // return the last day of the month 

            return dtTo;

        } 

        private void dtpNextAppraisalDate_Validating(object sender, CancelEventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;
            int var1 = 0;
            int var2 = 0;
            DateTime AfterAddMonths = new DateTime();
            DateTime NewDate = new DateTime();

            //if (dtpNextAppraisalDate.Value != null)
            //{


            if (dtpNextAppraisalDate.Value != null)
            {


              
                DateTime date2 = Convert.ToDateTime(dtpEffectiveDate.Value);
                DateTime date3 = Convert.ToDateTime(dtpNextAppraisalDate.Value);

                int result1 = DateTime.Compare(date2.Date, date3.Date);
                if (result1 > 0)
                {
                    MessageBox.Show("VALUE IS TO BE ENTERED WHICH SHOULD BE MORE THAN EFFECTIVE DATE");
              

                }
            }
            else
            {
                MessageBox.Show("VALUE IS TO BE ENTERED WHICH SHOULD BE MORE THAN EFFECTIVE DATE");
              

            }
                

            //}

        }

        public void ShowViewMsg(CHRIS_Personnel_IncrementOfficers_Level frm)
        {
            if (MessageBox.Show("Do You Want To View More Record [Y]es or [N]o", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {

                dtpEffectiveDate.Value      = null;
                dtpNextAppraisalDate.Value  = null;
                dtpPR_LAST_APP.Value        = null;
                dtpToDate.Value             = null;
                frm.Hide();
                frm.Close();
                CallView();
                return;

                #region Continue The Process For More Records
                //DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Y]es or [N]o", "Note"
                //               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //if (dRes1 == DialogResult.Yes)
                //{
                //    frm.Hide();
                //    frm.Close();
                //    CallView();
                //    return;
                //}
                //else
                //{
                //    frm.Hide();
                //    frm.Close();
                //    this.DoToolbarActions(this.Controls, "Cancel");
                //    this.txtOption.Select();
                //    this.txtOption.Focus();
                //}
                #endregion
            }
            else
            {
                //frm.Hide();
                this.txtOption.Select();
                this.txtOption.Focus();
                this.DoToolbarActions(this.Controls, "Cancel");
                this.txtOption.Select();
                this.txtOption.Focus();
                frm.Close();
                this.txtOption.Select();
                this.txtOption.Focus();
            }
        }

        public void CallView()
        {
            txtPersNo.Select();
            txtPersNo.Focus();
            base.ClearForm(pnlDetail.Controls);
            base.ClearForm(PnlDetails.Controls);

            this.View();

           
        }

        public void ShowViewMsgCourse(CHRIS_Personnel_IncrementOfficers_Level frm,CHRIS_Personnel_IncrementOfficersClerical_Course frm2)
        {
            if (MessageBox.Show("Do You Want To View More Record [Y]es or [N]o", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                base.Cancel();
                base.IterateFormToEnableControls(pnlDetail.Controls, true);
                txtpersonal.Select();
                txtpersonal.Focus();
                txtOption.Text = "A";
                frm.Hide();
                frm.Close();
                Staff_Course.Hide();
                Staff_Course.Close();
                
                if (frm2 != null)
                {
                    frm2.Hide();
                    frm2.Close();
                }
                txtPersNo.IsRequired = false;
                txtPersNo.Select();
                txtPersNo.Focus();
                base.ClearForm(this.Controls);
                txtPersNo.Select();
                txtPersNo.Focus();
                this.View();
                txtPersNo.IsRequired = true;
                return;


                #region Continue The Process For More Records

                //DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Y]es or [N]o", "Note"
                //               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //if (dRes1 == DialogResult.Yes)
                //{
                //    frm.Hide();
                //    frm.Close();
                //    if (frm2 != null)
                //    {
                //        frm2.Hide();
                //        frm2.Close();
                //    }
                //    txtPersNo.IsRequired = false;
                //    txtPersNo.Select();
                //    txtPersNo.Focus();
                //    base.ClearForm(this.Controls);
                //    txtPersNo.Select();
                //    txtPersNo.Focus();
                //    this.View();
                //    txtPersNo.IsRequired = true;
                //    return;
                //}
                //else
                //{
                //    frm.Hide();
                //    frm.Close();
                //    if (frm2 != null)
                //    {
                //        frm2.Hide();
                //        frm2.Close();
                //    }
                //    this.DoToolbarActions(this.Controls, "Cancel");
                //    this.txtOption.Select();
                //    this.txtOption.Focus();
                //}
                #endregion
            }
            else
            {

                frm.Hide();

                frm.Close();
                if (frm2 != null)
                {
                    frm2.Hide();
                    frm2.Close();
                }
                this.txtOption.Select();
                this.txtOption.Focus();
                this.DoToolbarActions(this.Controls, "Cancel");
                return;







            }

        }

        private void CustomDelete()
        {
            int ID = 0;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncrementPromotionClericalCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncrementPromotionClericalCommand)this.pnlDetail.CurrentBusinessEntity;
            ID = ent.ID;
            if (ID > 0)
            {
                //ID = ID;
                colsNVals.Clear();
                colsNVals.Add("PR_IN_NO", txtPersNo.Text);
                colsNVals.Add("PR_INC_TYPE", txtType.Text);
                colsNVals.Add("PR_EFFECTIVE", dtpEffectiveDate.Value);

                rsltCode = cmnDM.Execute("CHRIS_SP_INC_PROMOTION_MANAGER", "Delete", colsNVals);

                if (rsltCode.isSuccessful)
                {


                    MessageBox.Show("Record deleted successfully");
                    return;
                }
            }


        }

        public void ShowDeleteMsg(CHRIS_Personnel_IncrementOfficers_Level frm)
        {
            DialogResult dRes = MessageBox.Show("Do You Want To Delete The Record [Y]es or [N]o", "Note"
                                                 , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {

                //base.DoToolbarActions(this.Controls, "Delete");
                frm.Close();
                globalPNo = txtPersNo.Text;
                CallReport("AUPR", "OLD");

                CustomDelete();

                CallReport("AUP0", "OLD");

                //this.Reset();
                base.ClearForm(this.Controls);
               

                #region Continue The Process For More Records

                DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Y]es or [N]o", "Note"
                               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dRes1 == DialogResult.Yes)
                {
                   
                  
                    txtPersNo.Select();
                    txtPersNo.Focus();
                 

                    this.Delete();
                    return;


                }
                else
                {
                   
                    this.DoToolbarActions(this.Controls, "Cancel");
                    this.txtOption.Select();
                    this.txtOption.Focus();


                }


                #endregion

                //call save
                return;

            }
            else if (dRes == DialogResult.No)
            {
                frm.Close();
                
                this.DoToolbarActions(this.Controls, "Cancel");

                //txtOption.Select();
                //txtOption.Focus();

                return;
            }
        }

        private void preCommit()
        {

            //if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
            //{
                CmnDataManager cmnDM = new CmnDataManager();
                if (txtType.Text != "T")
                {
                    Result rsltCode;
                    
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Clear();
                    param.Add("PR_IN_NO", txtPersNo.Text);
                    param.Add("PR_ANNUAL_PRESENT", txtPRAnnualPresent.Text);
                    param.Add("PR_LAST_APP", dtpPR_LAST_APP.Value);
                    param.Add("PR_NEXT_APP", dtpNextAppraisalDate.Value);
                    rsltCode = cmnDM.Execute("CHRIS_SP_INC_PROMOTION_MANAGER", "preCommit1", param);


                }

                if (txtType.Text == "P")
                {
                    Result rsltCode1;
                    
                    Dictionary<string, object> param1 = new Dictionary<string, object>();
                    param1.Clear();
                    param1.Add("PR_IN_NO", txtPersNo.Text);
                    param1.Add("PR_EFFECTIVE", dtpEffectiveDate.Value);

                    rsltCode1 = cmnDM.Execute("CHRIS_SP_INC_PROMOTION_MANAGER", "preCommit2", param1);



                }
                //UMAIR
                if (txtOption.Text == "A")
                {
                    Result rsltCode2;

                    Dictionary<string, object> param2 = new Dictionary<string, object>();
                    param2.Clear();
                    param2.Add("PR_IN_NO", txtPersNo.Text);
                    param2.Add("PR_INC_TYPE", txtType.Text);

                    rsltCode2 = cmnDM.Execute("CHRIS_SP_INC_PROMOTION_MANAGER", "preCommit3", param2);
                }



                txtprCurrentPrevious.Text = "C";


                if (txtAnsW5.Text != "YES")
                {
                    txtPR_DESIG_PRESENT.Text = "CTT";
                }
                else{
                Result rsltCode3;

                Dictionary<string, object> param3 = new Dictionary<string, object>();
                param3.Clear();
                param3.Add("PR_IN_NO", txtPersNo.Text);
                param3.Add("pr_new_branch", txtprBranch.Text);
                param3.Add("PR_DESIG_PRESENT", txtPR_DESIG_PRESENT.Text);
                param3.Add("PR_LEVEL_PRESENT", txtPR_LEVEL_PRESENT.Text);

                rsltCode3 = cmnDM.Execute("CHRIS_SP_INC_PROMOTION_MANAGER", "preCommit4", param3);


                txtPR_PROMOTED.Text = "P";
                }



            //}
        }

        private void txtPR_REMARKS_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (txtPR_REMARKS.Text != string.Empty)
            {
                if (e.Modifiers != Keys.Shift)
                {
                    if (e.KeyCode == Keys.Tab)
                    {
                        if (txtType.Text == "C")
                        {
                            //if (txtOption.Text == "A")
                            //{
                            //    Staff_Course = new CHRIS_Personnel_IncrementOfficersClerical_Course(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, DGVCourse.GridSource);
                            //}
                            //else
                            //{
                            //    Staff_Course = new CHRIS_Personnel_IncrementOfficersClerical_Course(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvCourseDtl.GridSource);
                            //}

                            Staff_Course = new CHRIS_Personnel_IncrementOfficersClerical_Course(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvCourseDtl.GridSource);
                            Staff_Course.MdiParent = null;
                            Staff_Course.ShowDialog();
                            return;
                        }
                        if (txtType.Text != "C")
                        {
                            MessageBox.Show("PRESS <F10> Exit With Save  <F6> Exit W/O Save");
                            e.IsInputKey = true;
                            txtPR_REMARKS.Focus();
                            return;
                        }
                    }
                }
            }
            else
            {
                txtPR_REMARKS.Select();
                txtPR_REMARKS.Focus();
                txtPR_REMARKS.IsRequired = true;
            }
        }
        
        /// </summary>
//        private void CallReport(string FN, string Status)
//        {
//            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
//                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

//            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

//            GroupBox pnl = new GroupBox();
//            string globalPath = @"C:\SPOOL\CHRIS\AUDIT\";
//            string FN1 = FN + DateTime.Now.ToString("yyyyMMddHms");
//            string FullPath = frm.m_ReportPath + FN1;

//            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
//            txtDT.Name = "DT";
//            txtDT.Text = this.Now().ToString();
//            pnl.Controls.Add(txtDT);

//            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
//            txtPNO.Name = "PNO";
//            txtPNO.Text = globalPNo;
//            pnl.Controls.Add(txtPNO);

          
//            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
//            txtuser.Name = "user";
//            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
//            pnl.Controls.Add(txtuser);

//            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
//            txtST.Name = "st";
//            txtST.Text = Status;
//            pnl.Controls.Add(txtST);




//            frm.Controls.Add(pnl);
//            frm.RptFileName = "audit14A";
//            frm.Owner = this;
//            //frm.ExportCustomReportToTXT(FullPath, "TXT");
////            frm.ExportCustomReportToTXT(FullPath, "txt");
//            frm.ExportCustomReportToTXT(FullPath, "TXT");
//        }

        private void CallReport(string FN, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();

            string FN1 = FN + (DateTime.Now.ToString("yyyyMMddhhmiss")) + ".LIS";

            CrplControlLibrary.SLTextBox txtDESTYPE = new CrplControlLibrary.SLTextBox(comp);
            txtDESTYPE.Name = "txtDESTYPE";
            txtDESTYPE.Text = "FILE";
            pnl.Controls.Add(txtDESTYPE);

            CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            txtDESNAME.Name = "txtDESNAME";
            txtDESNAME.Text = frm.m_ReportPath + FN1;
            pnl.Controls.Add(txtDESNAME);

            CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            txtMODE.Name = "txtMODE";
            txtMODE.Text = "CHARACTER";
            pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "PNO";
            txtPNO.Text = globalPNo;
            //txtPNO.Text = g_Pno.ToString();
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "USER";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "ST";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);


            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "txtDT";
            txtDT.Text = Now().ToString("dd/MM/yyyy");
            pnl.Controls.Add(txtDT);

            frm.Controls.Add(pnl);
            frm.RptFileName = "audit14A";
            frm.Owner = this;

            frm.ExportCustomReportToTXT(frm.m_ReportPath + FN1, "txt");
            //frm.RunReport();
        }

        private void CHRIS_Personnel_IncremPromotCleric_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "Effective_Date_LOV")
            {
                dtpEffectiveDate.Select();
                dtpEffectiveDate.Focus();
            }
        }

        private void FillGrid()
        {
            try
            {
                DataTable dtCheckIfAdd;
                Dictionary<string, object> colsNVals4 = new Dictionary<string, object>();
                colsNVals4.Clear();
                colsNVals4.Add("PR_IN_NO", txtPersNo.Text);
                colsNVals4.Add("PR_EFFECTIVE", dtpEffectiveDate.Value);
                dtCheckIfAdd = GetData("CHRIS_SP_INCREMENT_PROMOTION_CTT_COURSE_MANAGER", "List", colsNVals4);
                if (dtCheckIfAdd != null)
                {
                    if (dtCheckIfAdd.Rows.Count > 0)
                    {
                        if (dtCheckIfAdd.Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            dgvCourseDtl.GridSource = dtCheckIfAdd;
                            dgvCourseDtl.DataSource = dgvCourseDtl.GridSource;
                        }
                    }
                }

            }
            catch (Exception exp)
            {
                LogError(this.Name, "", exp);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (strOption == "No" && strType == "P" && e.KeyCode == Keys.F10)
            {
                base.IterateFormToEnableControls(pnlDetail.Controls, false);
                strOption = string.Empty;
                strType = string.Empty;
            }
            
        }

        #endregion
    }
}