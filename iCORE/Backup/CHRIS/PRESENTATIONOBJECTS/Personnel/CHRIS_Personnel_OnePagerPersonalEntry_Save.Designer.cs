namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_OnePagerPersonalEntry_Save
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtStatus = new CrplControlLibrary.SLTextBox(this.components);
            this.txtOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelSimple1.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.slTextBox1);
            this.slPanelSimple1.Controls.Add(this.txtStatus);
            this.slPanelSimple1.Controls.Add(this.txtOption);
            this.slPanelSimple1.Controls.Add(this.label1);
            this.slPanelSimple1.DataManager = null;
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = true;
            this.slPanelSimple1.EnableInsert = true;
            this.slPanelSimple1.EnableQuery = false;
            this.slPanelSimple1.EnableUpdate = true;
            this.slPanelSimple1.EntityName = null;
            this.slPanelSimple1.Location = new System.Drawing.Point(24, 8);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(461, 53);
            this.slPanelSimple1.SPName = null;
            this.slPanelSimple1.TabIndex = 0;
            this.slPanelSimple1.TabStop = true;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(144, 32);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(0, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 1;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtStatus
            // 
            this.txtStatus.AllowSpace = true;
            this.txtStatus.AssociatedLookUpName = "";
            this.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStatus.ContinuationTextBox = null;
            this.txtStatus.CustomEnabled = true;
            this.txtStatus.DataFieldMapping = "";
            this.txtStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatus.GetRecordsOnUpDownKeys = false;
            this.txtStatus.IsDate = false;
            this.txtStatus.Location = new System.Drawing.Point(328, 15);
            this.txtStatus.MaxLength = 1;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.NumberFormat = "###,###,##0.00";
            this.txtStatus.Postfix = "";
            this.txtStatus.Prefix = "";
            this.txtStatus.Size = new System.Drawing.Size(85, 20);
            this.txtStatus.SkipValidation = false;
            this.txtStatus.TabIndex = 0;
            this.txtStatus.Text = "   ";
            this.txtStatus.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtStatus, "YES/NO REQUIRED");
            this.txtStatus.Leave += new System.EventHandler(this.txtStatus_Leave);
            this.txtStatus.Validating += new System.ComponentModel.CancelEventHandler(this.txtStatus_Validating);
            // 
            // txtOption
            // 
            this.txtOption.AllowSpace = true;
            this.txtOption.AssociatedLookUpName = "";
            this.txtOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOption.ContinuationTextBox = null;
            this.txtOption.CustomEnabled = true;
            this.txtOption.DataFieldMapping = "";
            this.txtOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOption.GetRecordsOnUpDownKeys = false;
            this.txtOption.IsDate = false;
            this.txtOption.Location = new System.Drawing.Point(272, 15);
            this.txtOption.MaxLength = 1;
            this.txtOption.Name = "txtOption";
            this.txtOption.NumberFormat = "###,###,##0.00";
            this.txtOption.Postfix = "";
            this.txtOption.Prefix = "";
            this.txtOption.Size = new System.Drawing.Size(50, 20);
            this.txtOption.SkipValidation = false;
            this.txtOption.TabIndex = 12;
            this.txtOption.Text = "A";
            this.txtOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(59, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 19);
            this.label1.TabIndex = 11;
            this.label1.Text = "Save All Information Y/N :";
            // 
            // CHRIS_Personnel_OnePagerPersonalEntry_Save
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(510, 71);
            this.Controls.Add(this.slPanelSimple1);
            this.Name = "CHRIS_Personnel_OnePagerPersonalEntry_Save";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iCore CHRIS - One Pager Personnel Entry [Save]";
            this.TopMost = true;
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
        private CrplControlLibrary.SLTextBox txtOption;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtStatus;
        private System.Windows.Forms.Label lblPopUP;
        private CrplControlLibrary.SLTextBox slTextBox1;

    }
}