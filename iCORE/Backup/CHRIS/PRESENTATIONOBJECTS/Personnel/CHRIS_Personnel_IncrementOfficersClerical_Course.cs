using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel  
{
    public partial class CHRIS_Personnel_IncrementOfficersClerical_Course :  ChrisTabularForm
    {
        private CHRIS_Personnel_IncremPromotCleric _mainForm = null;
        CHRIS_Personnel_IncrementOfficers_Level Staff_Level;
        private DataTable dtCourse;


        #region Constructors
        public CHRIS_Personnel_IncrementOfficersClerical_Course()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_IncrementOfficersClerical_Course(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_IncremPromotCleric mainForm, DataTable _dtCourse)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            dtCourse = _dtCourse;
            this._mainForm = mainForm;


        }
        #endregion


        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {

            base.CommonOnLoadMethods();
            this.tbtAdd.Visible = false;
            this.tbtList.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtCancel.Visible = false;
            this.tlbMain.Visible = false;
            this.txtOption.Visible = false;
            DGVCourse.Focus();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {

            base.OnLoad(e);

            DGVCourse.GridSource = dtCourse;
            DGVCourse.DataSource = DGVCourse.GridSource;
            DGVCourse.Select();
            DGVCourse.Focus();
            this.FunctionConfig.CurrentOption = this._mainForm.FunctionConfig.CurrentOption;
            this.FunctionConfig.EnableF8 = true;
            this.FunctionConfig.F8 = Function.Save;

            this.FunctionConfig.F6 = Function.Quit;
            //iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncrementCourse_Command ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncrementCourse_Command)PnlDetails.CurrentBusinessEntity;
            //ent.PR_IN_NO = decimal.Parse(_mainForm.txtPersNo.Text);
            DGVCourse.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.CTT_INSTITUTE.Width = 195;
            this.CTT_TITLE.Width =175;

         


        }

        public bool frmValidate()
        {
            bool flag = true;

            if(DGVCourse.Rows[0].Cells[0].EditedFormattedValue.ToString() == "" )
                flag = false;
            else if (DGVCourse.Rows[0].Cells[0].Value.ToString() == "" )
                flag = false;

            return flag;

        }

        protected override bool Save()
        {
            if (frmValidate())
            {
                _mainForm.CallSave(this);
            }
            this.CurrentPanelBlock = PnlDetails.Name;
            return false;

        }

        private void DGVCourse_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                DGVCourse.Rows[e.RowIndex].Cells["PR_IN_NO"].Value = _mainForm.txtPersNo.Text;
                if(_mainForm.dtpEffectiveDate.Value != null)
                {
                    DGVCourse.Rows[e.RowIndex].Cells["PR_EFFECTIVE"].Value = _mainForm.dtpEffectiveDate.Value;
                }
            }
        }
        protected override bool Quit()
        {
            this.FunctionConfig.CurrentOption = Function.None;
            base.Quit();
            _mainForm.CallQuit();
            return false;

        }

        private void CHRIS_Personnel_IncrementOfficersClerical_Course_Shown(object sender, EventArgs e)
        {
            if (this._mainForm.txtOption.Text == "V")
            {
                Staff_Level = new CHRIS_Personnel_IncrementOfficers_Level(_mainForm,this);
                Staff_Level.StartPosition = FormStartPosition.CenterScreen;
                Staff_Level.ShowDialog();

            }
        }

        private void DGVCourse_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }
    }
}