using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;

using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_FastOrISTransfer_FastEndDatePage : Form
    {

        #region Properties
        private Nullable<DateTime> _DateValue;
        CHRIS_Personnel_FastOrISTransfer_TransferPage mainTransferPage;
        private string _OperationMode;

        public Nullable<DateTime> DateValue
        {
            get
            {
                return _DateValue;
            }

            set
            {
                _DateValue = value;
            }
        }

        public string OperationMode
        {
            get
            {
                return _OperationMode;
            }

            set
            {
                _OperationMode = value;
            }
        }
        #endregion

        #region Constructor

        public CHRIS_Personnel_FastOrISTransfer_FastEndDatePage(DateTime date1,DateTime SaveDate,string option,CHRIS_Personnel_FastOrISTransfer_TransferPage _mainTransferPage)
        {

            InitializeComponent();
            dtPrEffective.Value = date1;
            mainTransferPage = _mainTransferPage;
            //DateValue = SaveDate;

           

        }

        private void dtEndDate_Validating(object sender, CancelEventArgs e)
        {

            if (dtEndDate.Value != null)
            {
                DateTime date1 = new DateTime();
                DateTime date2 = new DateTime();
                date1 = Convert.ToDateTime(dtEndDate.Value);
                date2 = Convert.ToDateTime(dtPrEffective.Value);

                if (DateTime.Compare(date1, date2) < 0)
                {

                    //this.dtEndDate.Value = null;
                    MessageBox.Show("THE FAST END DATE WHICH YOU HAVE ENTERED > EFFECTIVE DATE. RE-ENTER");
                    dtEndDate.Focus();
                    return;

                }
                else
                {

                    DateValue = Convert.ToDateTime(this.dtEndDate.Value);
                    this.Close();
                }


            }
            else
            {
                this.Close();
            }

        }

        #endregion

        private void CHRIS_Personnel_FastOrISTransfer_FastEndDatePage_Load(object sender, EventArgs e)
        {
              try
            {
                if (OperationMode == "A" && DateValue == null)
                {
                    dtEndDate.Value = null;
                }
                else
                {
                    dtEndDate.Value = DateValue;
                }

                //if (this.OperationMode == "V")
                //{
                //    DisableControls();
                //}

               
                this.dtEndDate.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
             
        }

        #region Methods
        private void DisableControls()
        {
            this.dtEndDate.Enabled = false;
          
            #region Do You Want To View More Record

            //CHRIS_Personnel_FastOrISTransfer objCHRIS_Personnel_FastOrISTransfer = new CHRIS_Personnel_FastOrISTransfer();
            //DialogResult dRes1 = MessageBox.Show("Do You Want To View More Record [Y]es/[N]o..", "Note"
            //               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //if (dRes1 == DialogResult.Yes)
            //{
            //    objCHRIS_Personnel_FastOrISTransfer.ViewMoreRecords();
            //    this.Close();
            //}

            #endregion
        }
        #endregion

        private void CHRIS_Personnel_FastOrISTransfer_FastEndDatePage_Shown(object sender, EventArgs e)
        {
            if (this.OperationMode == "D")
            {
                mainTransferPage.showDeleteMsg(this);
                return;
            }

            if (this.OperationMode == "V")
            {
                mainTransferPage.ShowViewMsg(this);
                return;
            }
        }



    }
}