using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_TerminationSettHold : ChrisMasterDetailForm
    {
        #region Constructor

        public CHRIS_Personnel_TerminationSettHold()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_TerminationSettHold(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(pnlTblDept);
            this.pnlDetail.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlDetail);
           // this.DefaultDataBlock = pnlTblBenchmark;
            this.CurrentPanelBlock = "pnlDetail";

        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
           
            this.txtUser.Text = this.userID;
            this.txtDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
            this.lblUserName.Text = "User Name: " +this.UserName;
            this.txtOption.Visible = false;
            this.dtEffectiveDate.Value = null;
            this.tbtList.Visible = false;
            this.tbtSave.Visible = false;
        
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;

            this.Seg.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.Contrib.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.Dept.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.Dept.Width = 112;

            this.ShowTextOption = false;
        }
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();

            lbtnPersonnal.Click += new EventHandler(lookupBtnDate_Click);

        }
        public void mainFormQuit()
        {
            base.ClearForm(pnlTblDept.Controls);
            base.ClearForm(pnlDetail.Controls);
            txtPersonnalNo.Select();
            this.txtPersonnalNo.Focus();
        }
        protected override bool Quit()
        {
            if (dtEffectiveDate.Focused)
            {
                this.AutoValidate = AutoValidate.Disable;
                this.FunctionConfig.CurrentOption = Function.Modify;
                dtEffectiveDate.IsRequired = false;
                dtEffectiveDate.Value = null;
              
               
            }
            else
            {
                this.FunctionConfig.CurrentOption = Function.None;
                this.AutoValidate = AutoValidate.Disable;
                dtEffectiveDate.Value = null;
            }

            base.Quit();
            dtEffectiveDate.Value = null;
            //dtEffectiveDate.IsRequired = true;
            txtPersonnalNo.Select();
            this.txtPersonnalNo.Focus();
            return false;
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {

            //if (actionType == "Save")
            //{
            //    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            //}
            if (actionType == "Cancel")
            {
                this.AutoValidate = AutoValidate.Disable;
                this.dtEffectiveDate.IsRequired = false;
                this.dtEffectiveDate.Value = null;
                txtPersonnalNo.Focus();
               

            }
            base.DoToolbarActions(ctrlsCollection, actionType);
            if (actionType == "Cancel")
            {
                //this.AutoValidate = AutoValidate.;
                txtPersonnalNo.Focus();
                //this.dtEffectiveDate.IsRequired = true;
                this.txtDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
                //dtEffectiveDate.Value = null;
            }
            if (actionType == "Delete")
            {
                this.Cancel();
            }
        }
        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }
        
        #endregion

        #region Events

        private void txtPersonnalNo_Validating(object sender, CancelEventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            //if (this.FunctionConfig.CurrentOption == Function.None)
            //    return;

            if (txtPersonnalNo.Text != string.Empty)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_TR_NO", txtPersonnalNo.Text);

                DataTable dtDummy = this.GetData("CHRIS_SP_TRANSFER_MANAGER", "PersonnalLOV", param);
                if (dtDummy != null)
                {
                    if (dtDummy.Rows.Count > 0)
                    {
                        txtDesignation.Text = dtDummy.Rows[0].ItemArray[0].ToString();
                        txtLevel.Text = dtDummy.Rows[0].ItemArray[1].ToString();
                        txtCategory.Text = dtDummy.Rows[0].ItemArray[2].ToString();
                        txtBranch.Text = dtDummy.Rows[0].ItemArray[4].ToString();
                        if (dtDummy.Rows[0].ItemArray[6].ToString() != string.Empty)
                        {
                            dtEffectiveDate.Value = Convert.ToDateTime(dtDummy.Rows[0].ItemArray[6].ToString());
                        }
                        else
                        {
                            dtEffectiveDate.Value = null;
                        }
                        txtRemarks.Text = dtDummy.Rows[0].ItemArray[5].ToString();
                        if (this.ActiveControl.Name != dtEffectiveDate.Name)
                        {
                            e.Cancel = true;
                            dtEffectiveDate.Select();
                            dtEffectiveDate.Focus();
                        }
                    }
                    else
                    {
                        base.ClearForm(pnlDetail.Controls);
                        base.ClearForm(pnlTblDept.Controls);
                        MessageBox.Show("Invalid Personnel Entered ..Press [F9] = Help Or [F6] = Exit.");
                        e.Cancel = true;
                    }
                }
                else
                {
                    base.ClearForm(pnlDetail.Controls);
                    base.ClearForm(pnlTblDept.Controls);
                    MessageBox.Show("Invalid Personnel Entered ..Press [F9] = Help Or [F6] = Exit.");
                    e.Cancel = true;
                }
            }
            else
            {
                if (!(this.ActiveControl is CrplControlLibrary.LookupButton))
                    e.Cancel = true;
            }
        }
        private void txtRemarks_Validated(object sender, EventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            //if (this.FunctionConfig.CurrentOption == Function.None)
            //    return;
                if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add || this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit)
                {
                    CHRIS_Personnel_TerminationHold process = new CHRIS_Personnel_TerminationHold(this);
                    process.StartPosition = FormStartPosition.CenterScreen;
                    process.ShowDialog();
                   
                    this.txtflag.Text = process.FlagValue;
                    process.Close();
                    if (txtPersonnalNo.Text != string.Empty)
                    {
                        base.DoToolbarActions(pnlDetail.Controls, "Save");
                        this.DoToolbarActions(pnlDetail.Controls, "Cancel");
                    }
                   // base.ClearForm(pnlDetail.Controls);

             

                //this.txtRemarks.Focus();

            }

        }
        private void dtEffectiveDate_Validating(object sender, CancelEventArgs e)
        {
            if (dtEffectiveDate.Value == null)
            {
                MessageBox.Show("Date Must Be Entered.... Or [F6] To Exit.", "", MessageBoxButtons.OK);
                e.Cancel = true;
            }
        }

        private void lookupBtnDate_Click(object sender, EventArgs e)
        {

            InitializeFormObject(pnlDetail);
            base.IterateFormControls(pnlDetail.Controls, false, false, false);
            pnlDetail.LoadDependentPanels();
            //this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
        }
        private void slDataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }

        public bool CheckforTransferType()
        {
            
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_TR_NO", txtPersonnalNo.Text);

                DataTable dtRelease = this.GetData("CHRIS_SP_TRANSFER_MANAGER", "ReleaseValidate", param);
                if (dtRelease != null)
                {
                    if (dtRelease.Rows.Count > 0)
                    {

                        return true;
                    }
                    else if (dtRelease.Rows.Count <= 0)
                    {

                        MessageBox.Show("Invalid Selection. Hold record does not exist.");
                        return false;

                    }


                }
                else
                {

                    MessageBox.Show("Invalid Selection. Hold record does not exist.");
                    return false;
                   
                }
                return true;

        }
        
        #endregion
    }
}