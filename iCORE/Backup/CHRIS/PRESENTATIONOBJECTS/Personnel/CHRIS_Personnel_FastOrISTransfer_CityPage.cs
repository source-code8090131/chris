using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_FastOrISTransfer_CityPage : Form
    {

        #region Fields

        private string _furloughCity;
        private string _IsCordinate;
        private string _OperationMode;
        CHRIS_Personnel_FastOrISTransfer_TransferPage mainTransferPage;

        public string furloughCity
        {
            get
            {
                return _furloughCity;
            }

            set
            {
                _furloughCity = value;
            }
        }
        public string IsCordinate
        {
            get
            {
                return _IsCordinate;
            }

            set
            {
                _IsCordinate = value;
            }
        }
        public string OperationMode
        {
            get
            {
                return _OperationMode;
            }

            set
            {
                _OperationMode = value;
            }
        }

        #endregion

        #region Constructors
        public CHRIS_Personnel_FastOrISTransfer_CityPage(CHRIS_Personnel_FastOrISTransfer_TransferPage _mainTransferPage)
        {
            InitializeComponent();
            mainTransferPage = _mainTransferPage;
            this.Shown+=new EventHandler(CHRIS_Personnel_FastOrISTransfer_CityPage_Shown);  // new System.EventHandler(CHRIS_Personnel_FastOrISTransfer_CityPage_Shown);
        }
        #endregion

        #region Events
        private void txtISCordinate_Validating(object sender, CancelEventArgs e)
        {
            if (txtISCordinate.Text != string.Empty)
            {
                IsCordinate = txtISCordinate.Text;
            }
        }
        private void txtFurLoughCity_Validating(object sender, CancelEventArgs e)
        {
            if (txtFurLoughCity.Text != string.Empty)
            {
                furloughCity = txtFurLoughCity.Text;
                this.Close();
            }
            else
            {
                this.Close();
            }

        }
        private void CHRIS_Personnel_FastOrISTransfer_CityPage_Load(object sender, EventArgs e)
        {
            try
            {
                if ((this.OperationMode == "A"))
                {
                    this.txtISCordinate.Text = IsCordinate;
                    this.txtFurLoughCity.Text = furloughCity;
                }
                if ((this.OperationMode == "V") || (this.OperationMode == "M") || (this.OperationMode == "D"))
                {
                    this.txtISCordinate.Text = IsCordinate;
                    this.txtFurLoughCity.Text = furloughCity;
                }
                //if (this.OperationMode == "V")
                //{
                //    DisableControls();
                //}


                this.txtISCordinate.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }     
        #endregion

        #region Methods
        private void DisableControls()
        {
            this.txtISCordinate.Enabled = false;
            this.txtFurLoughCity.Enabled = false;

            #region Do You Want To View More Record

            //CHRIS_Personnel_FastOrISTransfer objCHRIS_Personnel_FastOrISTransfer = new CHRIS_Personnel_FastOrISTransfer();
            //DialogResult dRes1 = MessageBox.Show("Do You Want To View More Record [Y]es/[N]o..", "Note"
            //               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //if (dRes1 == DialogResult.Yes)
            //{
            //    objCHRIS_Personnel_FastOrISTransfer.ViewMoreRecords();
            //    this.Close();
            //}

           #endregion
        }               
        #endregion

        private void CHRIS_Personnel_FastOrISTransfer_CityPage_Shown(object sender, EventArgs e)
        {
            if (this.OperationMode == "D")
            {
                mainTransferPage.showDeleteMsgCity(this);
                return;
            }

            if (this.OperationMode == "V")
            {
                mainTransferPage.ShowViewMsgCity(this);
                return;
            }
        }

    }
}