using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_OnePagerPersonalEntry_CitiRef : ChrisSimpleForm
    {

        #region Declarations

        private CHRIS_Personnel_OnePagerPersonalEntry _mainForm = null;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Training frm_Training;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_CitiRef frm_CitiRef;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Save frm_Save;

        string pr_SegmentValue = string.Empty;
        private DataTable dtPreEmp;
        private DataTable dtEdu;
        private DataTable dtTraining;
        string lblSavePopUP = "Save All Information [Y/N]";
        TextBox txt = new TextBox();

        CmnDataManager cmnDM = new CmnDataManager();
        Result rsltCitiEmpRef;
        Dictionary<string, object> objVals = new Dictionary<string, object>();

        #endregion

        #region Constructor
        public CHRIS_Personnel_OnePagerPersonalEntry_CitiRef()
        {
            InitializeComponent();
            this.ShowBottomBar.Equals(false);

        }

        public CHRIS_Personnel_OnePagerPersonalEntry_CitiRef(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_OnePagerPersonalEntry mainForm, DataTable _dtPreEmp, DataTable _dtEdu, DataTable _dtTraining)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            dtPreEmp = _dtPreEmp;
            dtTraining  = _dtTraining;
            dtEdu = _dtEdu;
            this._mainForm = mainForm;

        }
        #endregion

        #region Methods

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            this.txtOption.Visible = false;
            this.tbtAdd.Visible = false;
            this.tbtList.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtCancel.Visible = false;
            this.tlbMain.Visible = false;
            
            UpdatePopUPForm();

            this.KeyPreview = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
            
        }

        private void UpdateMainForm()
        {
            try
            {

                _mainForm.cmbFTE.Text = cmbFTE.Text;
                _mainForm.txtBranch.Text = txtBranch.Text;
                _mainForm.txtCity.Text = txtCity.Text;
                _mainForm.txtCountry.Text = txtCountry.Text;
                _mainForm.txtDept.Text = txtDept.Text;
                _mainForm.txtDesig.Text = txtDesig.Text;
                _mainForm.txtGroup.Text = txtGroup.Text;
                _mainForm.txtName.Text = txtName.Text;
                _mainForm.txtRelation.Text = txtRelation.Text;
                _mainForm.cmbOvrLocal.Text = cmbOvrLocal.Text; 
                
            }
            catch (Exception exp)
            {
                LogException(this.Name, "UpdateMainForm", exp);
            }
        }

        /// <summary>
        /// Fill PopUP with Value from Main Form
        /// </summary>
        private void UpdatePopUPForm()
        {
            try
            {
                cmbFTE.Text  = _mainForm.cmbFTE.Text;
                txtBranch.Text = _mainForm.txtBranch.Text;
                txtCity.Text = _mainForm.txtCity.Text;
                txtCountry.Text =_mainForm.txtCountry.Text;
                txtDept.Text = _mainForm.txtDept.Text;
                txtDesig.Text= _mainForm.txtDesig.Text;
                txtGroup.Text = _mainForm.txtGroup.Text;
                txtName.Text = _mainForm.txtName.Text;
                txtRelation.Text =_mainForm.txtRelation.Text;
                cmbOvrLocal.Text  = _mainForm.cmbOvrLocal.Text;
                
                


            }
            catch (Exception exp)
            {
                LogException(this.Name, "UpdatePopUPForm", exp);
            }
        }

        #endregion

        #region Events

        ///<summary>
        ///Handler: which call the relative method in MAin Form.
        ///</summary>
        ///<param name="sender"></param>
        ///<param name="e"></param>
        void TextBox_Leave(object sender, EventArgs e)
        {

            frm_Save = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Save();
            try
            {
                TextBox txt = (TextBox)sender;
                frm_Save.CloseSaveDialog();
                frm_Save.Close();
               
                //Save All changes
                _mainForm.CallSave(txt.Text);
               
                this.Close();            

            }
            catch (Exception exp)
            {
                LogError(this.Name, "TextBox_Leave", exp);
            }
        }

        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Modifiers == Keys.Control)
                {
                    switch (e.KeyValue)
                    {
                        case 34:
                            //Ctl+PageDown  "Open Save form"
                            UpdateMainForm();
                            
                            frm_Save = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Save(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, lblSavePopUP);
                            frm_Save.MdiParent = null;
                            this.Hide();
                            frm_Save.Enabled = true;
                            frm_Save.Select();
                            frm_Save.Focus();
                        
                            frm_Save.TextBox.MaxLength = 1;
                            frm_Save.TextBox.Leave += new EventHandler(TextBox_Leave);
                            frm_Save.MdiParent = null;
                            this.Hide();
                            frm_Save.ShowDialog(this);
                            txt.Text = frm_Save.Value;                           
                            this.KeyPreview = true;

                            break;

                        case 33:
                            //Ctl+PageUp  "Hide this form and open Training form"
                            UpdateMainForm();
                            this.Hide();
                            frm_Training = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Training(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, dtPreEmp, dtEdu, dtTraining);
                            //frm_Training = new CHRIS_Personnel_OnePagerPersonalEntry_Training();
                            frm_Training.ShowDialog(this);
                            this.KeyPreview = true;
                            break;
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "ShortCutKey_Press", exp);
            }
        }

        private void cmbFTE_Validating(object sender, CancelEventArgs e)
        {

            if (cmbFTE.Text == "")
            {
                MessageBox.Show("Field Must be Entered .....");
                return;

            }

            if (cmbFTE.Text == "FTE")
            {

                string CR_PR_NO = cmbFTE.Text;
                objVals.Clear();
                objVals.Add("PR_P_NO",_mainForm.txtPersonnelNO.Text);
                 
                rsltCitiEmpRef = cmnDM.GetData("CHRIS_SP_ONEPAGERPERSONALENTRY_MANAGER", "CITIEMPREF", objVals);

                if (rsltCitiEmpRef.isSuccessful)
                {
                    if (rsltCitiEmpRef.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "0")
                    {
                        MessageBox.Show("CR_PR_NO  Not Exist");
                   
                    }
                }
            }

        }

        #endregion

    }
}