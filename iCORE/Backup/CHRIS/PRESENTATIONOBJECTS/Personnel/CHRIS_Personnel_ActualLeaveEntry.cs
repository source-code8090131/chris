using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_ActualLeaveEntry : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        #region variables
        string status;
        int w_max_adv;
        int w_bal;
        char global_del;
        int w_year;
        int w_advance;

        int global_lc_cl_bal;
        int global_lc_sl_bal;
        int global_lc_ml_bal;
        int global_lc_pl_bal;
        int global_lc_sl_hf_bal;




        #endregion
        # region constructor
        public CHRIS_Personnel_ActualLeaveEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_ActualLeaveEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #endregion
        #region Methods
        bool dtstart = true;/* Used For Shift +Tab */
        char flag = '1';
        protected override void OnLoad(EventArgs e)
        {


            base.OnLoad(e);
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF5 = false;
            this.FunctionConfig.EnableF2 = false;
            txtUser.Text = this.userID;
            txtLocation.Text = this.Location.ToString();
            txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            this.tbtList.Visible = false;
            this.tbtAdd.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtDelete.Visible = false;
            label34.Text += " " + this.UserName;
            // this.Cancel();
        }
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            txtPersonnelNO.IsRequired = false;
            txtLeavetype.IsRequired = false;
            txtOption.Select();
            txtOption.Focus();
            txtPersonnelNO.IsRequired = true;
            txtLeavetype.IsRequired = true;

        }

        private void Proc_delete()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            param.Add("lev_lv_type", txtLeavetype.Text);
            param.Add("lev_start_date", dtstartdate.Value);

            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_delete", param);

            DateTime Enddate1;
            DateTime startDate1;
            // proc_leave_standard();
            Enddate1 = Convert.ToDateTime(dtEndDate.Value);
            startDate1 = Convert.ToDateTime(dtstartdate.Value);
            TimeSpan ts1 = Enddate1.Subtract(startDate1);
            int days1 = ts1.Days;
            int one_add_day = days1 + 1;
            int w_pl_adv = 0;
            int lc_pl_bal = 0;
            int lc_pl_avail = 0;

            if (txtPlAdvance.Text != string.Empty)
            {
                w_pl_adv = int.Parse(txtPlAdvance.Text);

            }

            if (txtPlBalance.Text != string.Empty)
            {
                lc_pl_bal = int.Parse(txtPlBalance.Text);
            }

            if (txtPlAvailed.Text != string.Empty)
            {
                lc_pl_avail = int.Parse(txtPlAvailed.Text);
            }



            //if (int.TryParse(txtPlAdvance.Text, out w_pl_adv) &&
            //int.TryParse(txtPlBalance.Text, out lc_pl_bal) &&
            //int.TryParse(txtPlAvailed.Text, out lc_pl_avail))
            //{
            if (txtLeavetype.Text == "PL")
            {
                w_advance = ((w_pl_adv + lc_pl_bal) - one_add_day) - lc_pl_bal;

                if (w_advance < 0)
                {
                    txtPlAdvance.Text = "0";
                }
                else
                {
                    txtPlAdvance.Text = Convert.ToString(w_advance);
                }
                txtPlAvailed.Text = Convert.ToString(lc_pl_avail - one_add_day);

                param.Clear();
                param.Add("lev_pno", this.txtPersonnelNO.Text);
                param.Add("w_pl_adv", txtPlAdvance.Text);
                param.Add("lc_pl_avail", txtPlAvailed.Text);
                rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_delete_updateCommand", param);

            }
            // }
        }
        private void Proc_del_check()
        {


        }
        private void Proc_del_del()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            param.Add("lev_lv_type", txtLeavetype.Text);
            param.Add("lev_start_date", dtstartdate.Value);
            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_del_del", param);
            if (rslt.isSuccessful)
            {

                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtPersonnelNO.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtLeavetype.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    dtstartdate.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
                    dtEndDate.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString());
                    txtLocalCertificate.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                    txtLC_CF_APPROVED.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    txtRemarks.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[6].ToString();
                    txtClBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();
                    txtSlBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txtMlBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtPlBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();
                    txtMandatoryAvialed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[11].ToString();
                    txtClAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[12].ToString();
                    txtSlAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[13].ToString();
                    txtMlAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[14].ToString();
                    txtPlAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[15].ToString();
                    txtCarryforward.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[16].ToString();
                    txtPlAdvance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[17].ToString();
                    txtSlHalf.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[18].ToString();
                }
            }





        }
        private void proc_leave_standard()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            param.Add("w_pl_adv", this.txtPlAdvance.Text);
            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_leave_standard", param);
            if (rslt.isSuccessful)
            {

                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (int.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out global_lc_cl_bal))
                    { }
                    if (int.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString(), out global_lc_sl_bal))
                    { }
                    if (int.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString(), out global_lc_ml_bal))
                    { }
                    if (int.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString(), out global_lc_pl_bal))
                    { }
                    if (int.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString(), out global_lc_sl_hf_bal))
                    { }

                    int plAdv = 0;
                    bool chkPlAdv = int.TryParse(txtPlAdvance.Text, out plAdv);
                    if (plAdv > 0)
                    {
                        global_lc_pl_bal = 0;
                    }
                }
            }
        }
        private void Proc_Modify()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text == "" ? "0" : this.txtPersonnelNO.Text);
            param.Add("lev_lv_type", txtLeavetype.Text);
            param.Add("lev_start_date", Convert.ToDateTime(dtstartdate.Value));
            param.Add("lev_end_date", Convert.ToDateTime(dtEndDate.Value));
            param.Add("lev_med_certif", txtLocalCertificate.Text);
            param.Add("lev_approved", txtApprovedHead.Text);
            param.Add("lev_remarks", txtRemarks.Text);

            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "Proc_Modify", param);
        }
        private void proc_Mod_del()
        {
            int lc_sl_bal   = 0;
            int lc_sl_avail = 0;
            int lc_slh_bal  = 0;

            if (txtSlBalance.Text != string.Empty)
            {
                lc_sl_bal = int.Parse(txtSlBalance.Text);
            }

            if (txtSlAvailed.Text != string.Empty)
            {
                lc_sl_avail = int.Parse(txtSlAvailed.Text);
            }
            if (txtSlHalf.Text != string.Empty)
            {
                lc_slh_bal = int.Parse(txtSlHalf.Text);
            }


            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno",        this.txtPersonnelNO.Text);
            param.Add("lev_lv_type",    txtLeavetype.Text);
            param.Add("lev_start_date", dtstartdate.Value);


            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_Mod_del", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult != null)
                {
                    txtPersonnelNO.Text     = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtLeavetype.Text       = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    dtstartdate.Value       = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
                    dtEndDate.Value         = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString());
                    txtLocalCertificate.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                    txtApprovedHead.Text    = rslt.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    txtRemarks.Text         = rslt.dstResult.Tables[0].Rows[0].ItemArray[6].ToString();
                    txtClBalance.Text       = rslt.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();
                    txtSlBalance.Text       = rslt.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txtSlHalf.Text          = rslt.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtMlBalance.Text       = rslt.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();
                    txtPlBalance.Text       = rslt.dstResult.Tables[0].Rows[0].ItemArray[11].ToString();
                    txtMandatoryAvialed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[12].ToString();
                    txtClAvailed.Text       = rslt.dstResult.Tables[0].Rows[0].ItemArray[13].ToString();
                    txtSlAvailed.Text       = rslt.dstResult.Tables[0].Rows[0].ItemArray[14].ToString();
                    txtMlAvailed.Text       = rslt.dstResult.Tables[0].Rows[0].ItemArray[15].ToString();
                    txtPlAvailed.Text       = rslt.dstResult.Tables[0].Rows[0].ItemArray[16].ToString();
                    txtCarryforward.Text    = rslt.dstResult.Tables[0].Rows[0].ItemArray[17].ToString();
                    txtPlAdvance.Text       = rslt.dstResult.Tables[0].Rows[0].ItemArray[18].ToString();

                }
            }

            DateTime Enddate1;
            DateTime startDate1;
            Enddate1        = Convert.ToDateTime(dtEndDate.Value);
            startDate1      = Convert.ToDateTime(dtstartdate.Value);
            TimeSpan ts1    = Enddate1.Subtract(startDate1);
            int days1       = ts1.Days;
            int one_add_day = days1 + 1;

            if (lc_sl_bal < lc_sl_avail)
            {
                txtSlHalf.Text      = Convert.ToString((lc_sl_bal + lc_slh_bal) - (lc_sl_avail - one_add_day));
                txtSlBalance.Text   = "0";
            }
            // }

            switch (txtLeavetype.Text.ToUpper())
            {

                case "CL":
                    proc_cl();
                    break;

                case "SL":
                    proc_sl();
                    break;

                case "ML":
                    proc_ml();
                    break;

                case "PL":
                    proc_pl();
                    break;
            }
        }
        private void proc_cl()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            param.Add("lev_lv_type", txtLeavetype.Text);
            param.Add("lev_start_date", dtstartdate.Value);

            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_cl", param);


            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtClBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtClAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                }
            }

        }
        private void proc_ml()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            param.Add("lev_lv_type", txtLeavetype.Text);
            param.Add("lev_start_date", dtstartdate.Value);

            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_ml", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtMlBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtMlAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();

                }


            }



        }
        private void proc_pl()
        {

            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno",        this.txtPersonnelNO.Text);
            param.Add("lev_lv_type",    txtLeavetype.Text);
            param.Add("lev_start_date", dtstartdate.Value);

            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_pl", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult != null)
                {
                    txtPlBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtPlAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    txtPlAdvance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    if (int.TryParse(rslt.dstResult.Tables[0].Rows[0]["LS_PL_BAL"].ToString(), out global_lc_pl_bal))
                    {
                        global_lc_pl_bal = int.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString());
                    }
                }
            }
            
            int lc_pl_bal   = 0;
            int lc_pl_avail = 0;
            int w_pl_adv    = 0;

            if (txtPlBalance.Text != string.Empty)
            {
                lc_pl_bal = int.Parse(txtPlBalance.Text);
            }

            if (txtPlAvailed.Text != string.Empty)
            {
                lc_pl_avail = int.Parse(txtPlAvailed.Text);
            }

            if (txtPlAdvance.Text != string.Empty)
            {
                w_pl_adv = int.Parse(txtPlAdvance.Text);
            }

            DateTime Enddate1       = Convert.ToDateTime(dtEndDate.Value);
            DateTime startDate1     = Convert.ToDateTime(dtstartdate.Value);
            TimeSpan ts1            = Enddate1.Subtract(startDate1);
            int days1               = ts1.Days;
            int one_add_day         = days1 + 1;

            if (lc_pl_bal < 0)
            {
                txtPlBalance.Text = "0";
            }

            if (lc_pl_avail < 0)
            {
                txtPlAvailed.Text = "0";
            }

            if (txtPlAvailed.Text != "0" && txtPlAdvance.Text != "0")
            {
                txtPlAdvance.Text = Convert.ToString(w_pl_adv - one_add_day);
            }
            else
            {
                txtPlAdvance.Text = "0";
            }

            if (lc_pl_avail < global_lc_pl_bal)
            {
                txtPlBalance.Text = Convert.ToString(global_lc_pl_bal - lc_pl_avail);
                txtPlAdvance.Text = "0";
            }
        }
        private void proc_sl()
        {

            int lc_sl_bal = 0;
            int lc_sl_avail = 0;

            if (txtSlBalance.Text != string.Empty)
            {
                lc_sl_bal = int.Parse(txtSlBalance.Text);

            }
            if (txtSlAvailed.Text != string.Empty)
            {
                lc_sl_avail = int.Parse(txtSlAvailed.Text);

            }


            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            param.Add("lev_lv_type", txtLeavetype.Text);
            param.Add("lev_start_date", dtstartdate.Value);

            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_sl", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtSlBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtSlAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();

                }

            }






            //if ((Int32.TryParse(txtSlBalance.Text,out lc_sl_bal) && Int32.TryParse(txtSlAvailed.Text, out lc_sl_avail)))
            //{
            if (lc_sl_bal < lc_sl_avail)
            {
                txtSlBalance.Text = "0";
            }

            //  }


        }
        private void proc_sl_h()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            param.Add("lev_lv_type", txtLeavetype.Text);
            param.Add("lev_start_date", dtstartdate.Value);

            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_sl_h", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult != null)
                {
                    txtSlHalf.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtSlAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                }
            }
        }
        private void Proc_update()
        {

            DateTime Enddate1 = Convert.ToDateTime(dtEndDate.Value);
            DateTime startDate1 = Convert.ToDateTime(dtstartdate.Value);
            TimeSpan ts1 = Enddate1.Subtract(startDate1);
            int days1 = ts1.Days;
            int one_add_day = days1 + 1;

            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", txtPersonnelNO.Text);
            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_update", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult != null)
                {

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        int number = int.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                        if (number > 0)
                        {

                            Result rslt2;
                            Dictionary<string, object> param2 = new Dictionary<string, object>();



                            switch (txtLeavetype.Text.ToUpper())
                            {

                                case "PL":
                                    int lc_pl_bal = 0;
                                    int lc_pl_avail = 0;
                                    int lc_c_forward = 0;
                                    if (txtPlBalance.Text != string.Empty)
                                        lc_pl_bal = int.Parse(txtPlBalance.Text);

                                    if (txtPlAvailed.Text != string.Empty)

                                        lc_pl_avail = int.Parse(txtPlAvailed.Text);

                                    if (txtCarryforward.Text != string.Empty)

                                        lc_c_forward = int.Parse(txtCarryforward.Text);
                                    DateTime statrdate = Convert.ToDateTime(dtstartdate.Value);
                                    DateTime Enddate = Convert.ToDateTime(dtstartdate.Value);

                                    if ((statrdate.Month == 3) && (Enddate.Month >= 4))
                                    {
                                        if ((lc_pl_bal > 30) &&
                                        (txtCarryforward.Text != string.Empty) &&
                                        (lc_pl_avail < lc_c_forward) &&
                                        statrdate.Month >= 4 &&
                                        Enddate.Month >= 4
                                        )
                                        {
                                            txtPlBalance.Text = Convert.ToString(30 + (lc_c_forward - lc_pl_avail));
                                        }


                                        if ((lc_pl_bal > 30) &&
                                        (txtCarryforward.Text != string.Empty) &&
                                        (lc_pl_avail < lc_c_forward) &&
                                        (statrdate.Month == 3) &&
                                        (Enddate.Month >= 4))
                                        {

                                            txtPlBalance.Text = Convert.ToString(30 + lc_c_forward -
                                                                             (one_add_day - (statrdate.Day - GetLastDayOfMonth(statrdate)) + 1));
                                        }
                                        param2.Add("lev_pno", this.txtPersonnelNO.Text);
                                        param2.Add("lc_mandatory", txtMandatoryAvialed.Text);
                                        param2.Add("lc_pl_avail", txtPlAvailed.Text);
                                        param2.Add("w_pl_adv", txtPlAdvance.Text);

                                        rslt2 = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_update_PL_IF", param2);

                                    }
                                    else
                                    {

                                        param2.Add("lev_pno", this.txtPersonnelNO.Text);
                                        param2.Add("lc_mandatory", txtMandatoryAvialed.Text);
                                        param2.Add("lc_pl_avail", txtPlAvailed.Text);
                                        param2.Add("w_pl_adv", txtPlAdvance.Text);

                                        rslt2 = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_update_PL_Else", param2);

                                    }

                                    break;

                                case "SL":
                                    param2.Add("lev_pno", this.txtPersonnelNO.Text);
                                    param2.Add("lc_mandatory", txtMandatoryAvialed.Text);
                                    param2.Add("lc_sl_avail", txtSlAvailed.Text);

                                    //    param2.Add("lc_sl_bal", txtSlBalance.Text);
                                    rslt2 = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_update_SL", param2);

                                    break;

                                case "CL":
                                    param2.Add("lev_pno", this.txtPersonnelNO.Text);

                                    param2.Add("lc_cl_avail", txtClAvailed.Text);

                                    rslt2 = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_update_CL", param2);
                                    break;

                                case "ML":
                                    param2.Add("lev_pno", this.txtPersonnelNO.Text);
                                    param2.Add("lc_ml_avail", this.txtMlAvailed.Text);
                                    param2.Add("lc_mandatory", txtMandatoryAvialed.Text);

                                    rslt2 = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_update_ML", param2);
                                    break;

                            }

                        }


                    }

                }


            }

        }
        private void proc_validate()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_validate", param);
            if (rslt.isSuccessful)
            {
                if ((rslt.dstResult.Tables[0].Rows.Count > 0) && (rslt.dstResult != null))
                {

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty && rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString() != string.Empty)
                    {
                        dtlc_date.Value = DateTime.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                        dtlc_s_date.Value = DateTime.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());
                    }
                }
                else
                {

                    dtlc_date.Value = null;
                    dtlc_s_date.Value = null;

                }
            }


        }
        private void Proc_view_add()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            param.Add("lev_lv_type", this.txtLeavetype.Text);
            param.Add("lev_start_date", Convert.ToDateTime(dtstartdate.Value).ToShortDateString());
            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_view_add", param);
            if (rslt.dstResult.Tables[0].Rows.Count == 0)
            {
                flag = '0';
            }
            //dtEndDate.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

        }
        private void pro_rate_leaves()
        {
            int w_months;
            int w_days;
            int w_cl_day = 0;
            int w_cl_month = 0;
            int w_sl_day = 0;
            int w_sl_month = 0;
            int w_slh_day = 0;
            int w_slh_month = 0;
            int w_ml_day = 0;
            int w_ml_month = 0;
            int w_pl_day = 0;
            int w_pl_month = 0;
            int number;
            int cl;
            int sl;
            int ml;
            int pl;
            int slh;

            DateTime joiningdate;
            Result rslt;
            DateTime dummydate = Convert.ToDateTime("31 / 12 /" + this.CurrentDate.Year);
            joiningdate = Convert.ToDateTime(dtJoiningdate.Value);

            if (joiningdate.Year == CurrentDate.Year)
            {
                w_months = base.MonthsBetweenInOracle(dummydate, joiningdate);
                number = GetLastDayOfMonth(joiningdate);
                w_days = number - joiningdate.Day;
                w_cl_day = (12 / 12 / 30) * w_days;
                w_cl_month = (12 / 12) * w_months;
                w_sl_day = (30 / 12 / 30) * w_days;
                w_sl_month = (30 / 12) * w_months;
                w_slh_day = (30 / 12 / 30) * w_days;
                w_slh_month = (30 / 12) * w_months;
                w_ml_day = (60 / 12 / 30) * w_days;
                w_ml_month = (60 / 12) * w_months;
                w_pl_day = (30 / 12 / 30) * w_days;
                w_pl_month = (30 / 12) * w_months;

            }
            cl = (w_cl_month) + (w_cl_day);
            sl = (w_sl_month) + (w_sl_day);
            slh = (w_slh_month) + (w_slh_day);
            ml = (w_ml_month) + (w_ml_day);
            pl = (w_pl_month) + (w_pl_day);


            if (txtw_sex.Text == "M")
            {
                ml = 0;
            }

            if (status == "New")
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("LEV_PNO", txtPersonnelNO.Text);
                param.Add("cl", cl);
                param.Add("ml", ml);
                param.Add("sl", sl);
                param.Add("pl", pl);
                param.Add("w_pl_adv", txtPlAdvance.Text);
                param.Add("slh", slh);
                cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "pro_rate_leavesInsert", param);
                txtClBalance.Text = Convert.ToString(cl);
                txtMlBalance.Text = Convert.ToString(ml);
                txtSlBalance.Text = Convert.ToString(sl);
                txtSlHalf.Text = Convert.ToString(slh);
                txtPlBalance.Text = Convert.ToString(pl);

            }



        }
        private void p_ml_check()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "p_ml_check", param);
            if (rslt.isSuccessful)
            {
                if ((rslt.dstResult.Tables[0].Rows.Count > 0) && (rslt.dstResult != null))
                {
                    txtw_sex.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                }
            }
        }
        private void value_proc()
        {
            int w_months;
            int number;
            int w_days;
            int w_pl_day = 0;
            double w_pl_month;



            DateTime joiningdate;
            Result rslt;
            DateTime dummydate = Convert.ToDateTime("31 / 12 /" + this.CurrentDate.Year);

            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "value_proc_part1", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult != null)
                {
                    if (Int32.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out w_bal))
                        w_bal = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                }
                if (w_bal < 0)
                {
                    global_del = 'L';
                }

                joiningdate = Convert.ToDateTime(dtJoiningdate.Value);

                if (joiningdate.Year == CurrentDate.Year)
                {
                    w_months = base.MonthsBetweenInOracle(dummydate, joiningdate);
                    number = GetLastDayOfMonth(joiningdate);
                    w_days = number - joiningdate.Day;
                    w_pl_day = (30 / 12 / 30) * w_days;
                    w_pl_month = (30 / 12) * w_months;

                }

                else
                {
                    w_pl_month = 30;
                }

                double month = Math.Round(w_pl_month, 0);
                double pl = (month + (w_pl_day));


                if (global_del == 'L')
                {
                    Result rslt5;
                    Dictionary<string, object> param5 = new Dictionary<string, object>();
                    param5.Add("lev_pno", this.txtPersonnelNO.Text);
                    rslt5 = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "valueProc_Update", param5);
                    if (rslt5.isSuccessful)
                    { }
                    txtPlBalance.Text = Convert.ToString(pl);
                }

            }

        }
        private int GetLastDayOfMonth(DateTime dtDate)
        {

            // set return value to the last day of the month 

            // for any date passed in to the method 



            // create a datetime variable set to the passed in date 

            DateTime dtTo = dtDate;



            // overshoot the date by a month 

            dtTo = dtTo.AddMonths(1);



            // remove all of the days in the next month 

            // to get bumped down to the last day of the 

            // previous month 

            dtTo = dtTo.AddDays(-(dtTo.Day));



            // return the last day of the month 

            return dtTo.Day;

        }
        private void leave()
        {

            int lc_sl_avail = 0;
            int lc_sl_bal = 0;
            int lc_slh_bal = 0;

            if (txtOption.Text == "D")
            {
                value_proc();
            }

            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "Leave", param);

            if (rslt.isSuccessful)
            {
                if ((rslt.dstResult.Tables[0].Rows.Count > 0) && (rslt.dstResult != null))
                {
                    txtClBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtSlBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    txtMlBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    txtPlBalance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                    txtPlAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtClAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[6].ToString();
                    txtSlAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();
                    txtMlAvailed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txtCarryforward.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();
                    txtMandatoryAvialed.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    txtPlAdvance.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[12].ToString();
                    txtSlHalf.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();


                    if ((txtLC_CF_APPROVED.Text != "Y") || (txtLC_CF_APPROVED.Text == string.Empty) || (txtlc_bal_cf.Text != "Y"))
                    {
                        txtCarryforward.Text = "0";
                    }

                    if (txtSlAvailed.Text != string.Empty)
                    {
                        lc_sl_avail = int.Parse(txtSlAvailed.Text);
                    }
                    if (txtSlBalance.Text != string.Empty)
                    {
                        lc_sl_bal = int.Parse(txtSlBalance.Text);
                    }

                    if (txtSlHalf.Text != string.Empty)
                    {
                        lc_slh_bal = int.Parse(txtSlHalf.Text);
                    }


                    if (lc_sl_avail > lc_sl_bal)
                    {
                        txtSlHalf.Text = Convert.ToString(((lc_sl_bal) + (lc_slh_bal)) - (lc_sl_avail));
                        txtSlBalance.Text = "0";
                    }

                    else
                    {
                        txtSlBalance.Text = Convert.ToString(((lc_sl_bal) - (lc_sl_avail)));
                    }

                    status = "OLD";
                }

                else
                {
                    status = "New";
                    w_year = int.Parse(this.CurrentDate.ToString("yyyy"));
                    w_year = w_year - 1;
                    DateTime joingyear = Convert.ToDateTime(dtJoiningdate.Value);
                    int yearofjoin = joingyear.Year;
                    int sysyear = int.Parse(this.CurrentDate.ToString("yyyy"));

                    if (yearofjoin == sysyear)
                    {
                        pro_rate_leaves();

                    }
                    else if (txtClBalance.Text == string.Empty && txtMlBalance.Text == string.Empty)
                    {
                        Result rslt4;
                        Dictionary<string, object> param4 = new Dictionary<string, object>();
                        param4.Add("lev_pno", this.txtPersonnelNO.Text == "" ? "0" : this.txtPersonnelNO.Text);
                        param4.Add("W_PL_ADV", txtPlAdvance.Text);
                        if (txtw_sex.Text == "F")
                        {
                            rslt4 = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "If_Female", param4);
                        }

                        else
                        {
                            rslt4 = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "If_Male", param4);
                        }

                        txtClBalance.Text = "12";
                        txtSlBalance.Text = "30";
                        txtSlHalf.Text = "30";
                        txtPlBalance.Text = "30";

                    }
                }

            }




        }
        private void Leave_Cal()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", txtPersonnelNO.Text);

            switch (txtLeavetype.Text.ToUpper())
            {


                case "ML":
                    {
                        rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "Leave_cal_ML", param);
                        if (rslt.isSuccessful)
                        {
                            if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                int LC_ML_AVAIL = 0;
                                int val = 0;
                                txtMlAvailed.Text = rslt.dstResult.Tables[0].Rows[0]["LEV_SUM_ML"].ToString();

                                if (txtMlAvailed.Text != string.Empty)
                                {
                                    LC_ML_AVAIL = int.Parse(txtMlAvailed.Text);
                                }
                                val = (12 - LC_ML_AVAIL);
                                txtMlBalance.Text = val.ToString();


                            }
                        }
                    }
                    break;

                case "CL":
                    rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "Leave_cal_cl", param);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            int LC_CL_AVAIL = 0;
                            int val = 0;
                            txtClAvailed.Text = rslt.dstResult.Tables[0].Rows[0]["LEV_SUM_CL"].ToString();

                            if (txtClAvailed.Text != string.Empty)
                            {
                                LC_CL_AVAIL = int.Parse(txtClAvailed.Text);
                            }
                            val = (12 - LC_CL_AVAIL);
                            txtClBalance.Text = val.ToString();


                        }
                    }
                    break;
                case "PL":
                    rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "Leave_cal_PL", param);

                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                        {

                            int w_pl_adv = 0;
                            int LC_PL_AVAIL = 0;
                            int lc_pl_bal = 0;
                            if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                            {
                                LC_PL_AVAIL = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                            }
                            if (txtPlBalance.Text != string.Empty)
                            {
                                lc_pl_bal = Int32.Parse(txtPlBalance.Text);
                            }
                            if ((lc_pl_bal == 0) || ((txtPlBalance.Text == null) && ((txtPlBalance.Text == ""))))
                            { txtPlBalance.Text = "30"; }
                            txtPlBalance.Text = Convert.ToString((lc_pl_bal + w_pl_adv) - LC_PL_AVAIL);

                        }
                    }


                    break;


                case "SL":
                    rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "Leave_cal_SL", param);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            int LC_SL_AVAIL = 0;
                            int val = 0;

                            txtSlAvailed.Text = rslt.dstResult.Tables[0].Rows[0]["LEV_SUM_SL"].ToString();

                            if (txtSlAvailed.Text != string.Empty)
                            {
                                LC_SL_AVAIL = int.Parse(txtSlAvailed.Text);
                            }
                            val = (60 - LC_SL_AVAIL);
                            txtSlBalance.Text = val.ToString();


                        }
                    }
                    break;


            }
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {

            if (actionType == "Save")
            {
                if (txtPersonnelNO.Text != string.Empty)
                {

                    if (txtOption.Text == "A" || txtOption.Text == "M")
                    { base.DoToolbarActions(ctrlsCollection, actionType); }

                    return;
                }
                return;
            }



            if (actionType == "Cancel")
            {

                //if (txtPersonnelNO.Text == string.Empty)
                //{
                //    return;
                
                //}

                ////if (txtPersonnelNO.Text != string.Empty)
                ////{
                //this.AutoValidate = AutoValidate.Disable;
                //this.txtPersonnelNO.Text = "";
                this.CurrentPanelBlock = PnlLeaveActual.Name;
                this.FunctionConfig.CurrentOption = Function.None;
                base.DoToolbarActions(ctrlsCollection, actionType);
                dtstart = true;
                //return;
                //}
                return;
            }


            base.DoToolbarActions(ctrlsCollection, actionType);
        }
        protected override bool Add()
        {
            base.Add();
            txtApprovedHead.Text = "Y";
            return true;
        }
        private void ClearTexbox()
        { 
        
        txtPlBalance.Text="";
        txtSlBalance.Text="";
        txtMlBalance.Text="";
        txtClBalance.Text="";
        txtSlAvailed.Text="";
        txtMlAvailed.Text="";
        txtPlAvailed.Text="";
        txtClAvailed.Text = "";
        txtCarryforward.Text = "";
        txtPlAdvance.Text = "";
        txtSlHalf.Text = "";
        txtMandatoryAvialed.Text = "";
        
        }
        protected override bool View()
        {
            
            base.View();
            Chris_Personnel_ActualLeaveEntry_ grd = new Chris_Personnel_ActualLeaveEntry_();
            grd.ShowDialog();
            grd.Close();
            // this.CurrentPanelBlock = PnlLeaveActual.Name;
            base.Cancel();
            txtOption.Select();
            txtOption.Focus();
            return false;

        }

        #endregion
        #region Events
        private void txtPersonnelNO_Validating(object sender, CancelEventArgs e)
        {
            if (txtPersonnelNO.Text != string.Empty)
            {

                p_ml_check();
                leave();

                if (status == "OLD")
                {
                    pro_rate_leaves();
                }


            }
        }
        private void txtLeavetype_Validating(object sender, CancelEventArgs e)
        {

            if (FunctionConfig.CurrentOption != Function.Add)
            {
                if (txtLeavetype.Text == string.Empty)
                {
                    this.BindLOVLookupButton(this.lbnLeavetype);

                }
            }

            if ((txtLeavetype.Text != "PL") && (txtLeavetype.Text != "SL") &&
            (txtLeavetype.Text != "CL") && (txtLeavetype.Text != "ML") &&
            (txtLeavetype.Text != "LL") && (txtLeavetype.Text != "OT"))
            {

                MessageBox.Show("Enter PL/SL/CL/ML/LL/OT .......");
                e.Cancel = true;

            }


            if (txtLeavetype.Text == "ML" && txtw_sex.Text == "M")
            {
                MessageBox.Show("Maternity Leaves Are Given To Female Staff Only");
                e.Cancel = true;
                // this.Cancel();
            }
        }
        private void dtstartdate_Validating(object sender, CancelEventArgs e)
        {
            DateTime currdate = this.CurrentDate;
            DateTime strtdate;
            DateTime endate;
            DateTime cdate;
            DateTime csdate;
            DateTime Joiningdate;

            if (dtstartdate.Value != null)
            {
                strtdate    = Convert.ToDateTime(dtstartdate.Value);
                Joiningdate = Convert.ToDateTime(dtJoiningdate.Value);
                int curryr  = currdate.Year;
                int strtyr  = strtdate.Year;

                if ((strtyr > curryr))
                {
                    MessageBox.Show("Enter Leaves For  " + curryr.ToString());
                    e.Cancel = true;
                    return;
                }

                TimeSpan ts4 = strtdate.Subtract(Joiningdate);
                int ts4_days = ts4.Days;
                if (ts4_days < 0)
                {
                    MessageBox.Show("Leave Start Date Should Be Greater Then Joining Date");
                    e.Cancel = true;
                    return;
                }

                if (FunctionConfig.CurrentOption == Function.Add)
                {
                    proc_validate();
                    csdate  = Convert.ToDateTime(dtlc_s_date.Value);
                    cdate   = Convert.ToDateTime(dtlc_date.Value);


                    TimeSpan ts1    = strtdate.Subtract(cdate);
                    int c_days      = ts1.Days;
                    TimeSpan ts2    = strtdate.Subtract(csdate);
                    int c_sdays     = ts2.Days;

                    if ((c_days <= 0) && (c_sdays >= 0))
                    {
                        MessageBox.Show("Last Leave Ended On  " + cdate.ToString("dd/MM/yyyy") + " Next Leave Should Start After This Date");
                        e.Cancel = true;
                    }
                }

                if (FunctionConfig.CurrentOption == Function.Modify)
                {
                    proc_Mod_del();
                }

                if (FunctionConfig.CurrentOption == Function.Add)
                {
                    Proc_view_add();
                    if (flag == '1')
                    {
                        MessageBox.Show("Record Is Already Entered ......Press Any Key To Continue");
                        this.Cancel();
                    }
                    else
                    {
                        leave();
                    }
                }
                if (FunctionConfig.CurrentOption == Function.View)
                {
                    Proc_view_add();
                }

                if (FunctionConfig.CurrentOption == Function.Delete)
                {
                    Proc_del_check(e);
                    if (e.Cancel)
                        return;

                    Proc_del_del();

                    if (w_max_adv == 0)
                    {
                        w_max_adv = 0;
                    }
                    if (MessageBox.Show("Do You Want To Delete This Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Proc_delete();
                        Leave_Cal();
                        Proc_update();
                        this.Cancel();
                    }
                }
                dtstart = true; /* Used For Shift +Tab */
            }
        }

        private void Proc_del_check(CancelEventArgs e)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("lev_pno", this.txtPersonnelNO.Text);
            param.Add("lev_lv_type", txtLeavetype.Text);
            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "proc_del_check", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult != null)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty && dtstartdate.Value != null)
                    {

                        if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            DateTime w_date = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                            DateTime startdate = Convert.ToDateTime(dtstartdate.Value);

                            TimeSpan ts = startdate.Subtract(w_date);
                            int days = ts.Days;
                            if (days != 0)
                            {
                                MessageBox.Show("You Can Delete Record In Descending Order ... Valid Date is " + w_date.ToString("dd/MM/yyyy"));

                                e.Cancel = true;

                            }
                        }
                    }
                }
            }
        }
        private void dtEndDate_Validating(object sender, CancelEventArgs e)
        {
            if (dtstart)  /* Used For Shift +Tab */
            {
                if (dtEndDate.Value != null)
                {
                    # region key_Next
                    char flg = 'N';
                    DateTime Enddate1;
                    DateTime startDate1;

                    proc_leave_standard();

                    Enddate1 = Convert.ToDateTime(dtEndDate.Value);
                    startDate1 = Convert.ToDateTime(dtstartdate.Value);
                    TimeSpan ts1 = Enddate1.Subtract(startDate1);
                    int days1 = ts1.Days;
                    string counter = Convert.ToString(days1);
                    int length = counter.Length;

                    if (length > 2)
                    {
                        MessageBox.Show(" Exceeding Maximum Number Of Days Limit Total Days Are Coming " + counter);
                        e.Cancel = true;
                        return;

                    }
                    if (days1 < 0)
                    {
                        MessageBox.Show(" End Date Must Be Greater then Start Date ...");
                        e.Cancel = true;
                        return;

                    }
                    int one_add_day = days1 + 1;
                    txtTotalDays.Text = one_add_day.ToString();
                    int lc_pl_bal = 0;
                    int lc_cl_bal = 0;
                    int lc_sl_bal = 0;
                    int lc_c_forward = 0;
                    int lc_ml_bal = 0;


                    int lc_cl_avail = 0;
                    int lc_sl_avail = 0;
                    int lc_ml_avail = 0;
                    int lc_pl_avail = 0;



                    if (txtClAvailed.Text != string.Empty)
                    {
                        lc_cl_avail = int.Parse(txtClAvailed.Text);
                    }

                    if (txtSlAvailed.Text != string.Empty)
                    {

                        lc_sl_avail = int.Parse(txtSlAvailed.Text);
                    }
                    if (txtMlAvailed.Text != string.Empty)
                    {
                        lc_ml_avail = int.Parse(txtMlAvailed.Text);
                    }

                    if (txtPlAvailed.Text != string.Empty)
                    {
                        lc_pl_avail = int.Parse(txtPlAvailed.Text);
                    }

                    if (txtCarryforward.Text != string.Empty)
                    {
                        lc_c_forward = int.Parse(txtCarryforward.Text);
                    }

                    switch (txtLeavetype.Text.ToUpper())
                    {
                        #region Case "ML"
                        case "ML":
                            if (txtMlBalance.Text != string.Empty)
                                lc_ml_bal = Int32.Parse(txtMlBalance.Text);
                            if (one_add_day > lc_ml_bal)
                            {
                                MessageBox.Show("Leaves Exceeding By " + " " + (one_add_day - lc_ml_bal) + " " + " Day(s)");
                                e.Cancel = true;
                                return;
                            }
                            break;
                        #endregion
                        #region Case "CL"
                        case "CL":

                            if (txtClBalance.Text != string.Empty)
                                lc_cl_bal = Int32.Parse(txtClBalance.Text);
                            if (one_add_day > lc_cl_bal)
                            {
                                MessageBox.Show("Leaves Exceeding By " + " " + (one_add_day - lc_cl_bal) + " " + " Day(s)");
                                e.Cancel = true;
                                return;
                            }

                            break;
                        #endregion
                        #region Case "PL"
                        case "PL":
                            if (txtPlBalance.Text != string.Empty)
                                lc_pl_bal = Int32.Parse(txtPlBalance.Text);

                            if (one_add_day > lc_pl_bal)
                            {
                                txt_W_MAX_ADV.Text = "0";

                                #region Advance Leave
                                if (MessageBox.Show("You Want To Take Advance Leave(s) [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {

                                    if (txtPlAdvance.Text == string.Empty)
                                    {
                                        txtPlAdvance.Text = "0";
                                    }

                                    //if (int.TryParse(txtPlAdvance.Text, out w_advance))
                                    {
                                        txtPlAdvance.Text = Convert.ToString((one_add_day - lc_pl_bal) + w_advance);
                                        txtPlBalance.Text = "0";
                                        txtLocalCertificate.Select();
                                        txtLocalCertificate.Focus();
                                        flg = 'Y';
                                    }
                                }

                                else
                                {
                                    MessageBox.Show("Leaves Exceeding By " + (one_add_day - (lc_pl_bal + lc_c_forward)) + " Day(s)");

                                    if (lc_pl_avail != 0)
                                    {
                                        txtPlAvailed.Text = Convert.ToString(lc_pl_avail - one_add_day);

                                    }

                                    if ((global_lc_pl_bal != 0) && (lc_c_forward == 0))
                                    {

                                        txtPlBalance.Text = Convert.ToString((global_lc_pl_bal + w_advance) - (lc_pl_avail + w_advance));
                                    }

                                    else if

                                    (global_lc_pl_bal != 0 && lc_c_forward != 0)
                                    {
                                        txtPlBalance.Text = Convert.ToString(
                                            (
                                            (global_lc_pl_bal + lc_c_forward + w_advance) - (lc_pl_avail + w_advance)));


                                    }


                                    else
                                    {
                                        txtPlBalance.Text = "0";
                                    }

                                    if (flg == 'Y')
                                    {
                                        txtPlAdvance.Text = Convert.ToString((w_advance + lc_pl_bal) - one_add_day);
                                    }// this.Cancel();
                                    //dtEndDate.Value = null;
                                    dtEndDate.Select();
                                    dtEndDate.Focus();
                                    return;
                                }
                                #endregion
                            }
                            else
                                if ((one_add_day < lc_pl_bal) && (txtPlAdvance.Text == "0"))
                                {
                                    txtPlAdvance.Text = "0";
                                    Dictionary<string, object> param = new Dictionary<string, object>();
                                    param.Add("w_pl_adv", txtPlAdvance.Text);
                                    param.Add("LEV_PNO", txtPersonnelNO.Text);
                                    rslt = cmnDM.Execute("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "PLAdvance", param);
                                }

                            break;
                        #endregion
                        #region Case "SL"
                        case "SL":
                            int lc_sl_hf_bal = 0;

                            if (txtSlHalf.Text != string.Empty)
                                lc_sl_hf_bal = Int32.Parse(txtSlHalf.Text);

                            if (txtSlBalance.Text != string.Empty)
                                lc_sl_bal = Int32.Parse(txtSlBalance.Text);

                            if (one_add_day > lc_sl_bal)
                            {

                                /*Code Added by Myself Because oracle is crashing on this scenario*/
                                /*---Start---*/
                                MessageBox.Show("Leaves Exceeding By " + " " + (one_add_day - lc_sl_bal) + " " + " Day(s)");
                                e.Cancel = true;
                                return;
                                /*Code Added by Myself*/
                                /*---END---*/





                                int lc_slh_bal = Int32.Parse(txtSlHalf.Text);

                                //# region  Half paySick Leave
                                //if (MessageBox.Show("Do You Want To Take HalfPay Sick Leave(s)", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                //{
                                //    if ((one_add_day - global_lc_sl_bal) > global_lc_sl_hf_bal)
                                //    {
                                //        MessageBox.Show("Leaves Exceeding By " + (one_add_day - lc_sl_hf_bal) + " Day(s)");
                                //        txtSlAvailed.Text = Convert.ToString(lc_sl_avail - one_add_day);



                                //        if (lc_sl_avail < global_lc_sl_bal)
                                //        {
                                //            txtSlBalance.Text = Convert.ToString(global_lc_sl_bal - lc_sl_avail);
                                //        }
                                //        else
                                //        {

                                //            txtSlBalance.Text = "0";

                                //        }

                                //        dtEndDate.Value = null;
                                //        dtEndDate.Select();
                                //        dtEndDate.Focus();

                                //    }
                                //    else
                                //    {


                                //         txtSlHalf.Text = Convert.ToString (((global_lc_sl_bal) +
                                //         (global_lc_sl_hf_bal)) - lc_sl_avail);
                                //         txtSlBalance.Text = "0";
                                //         txtLocalCertificate.Select();
                                //         txtLocalCertificate.Focus();

                                //    }
                                //}

                                //else
                                //{
                                //    MessageBox.Show("Leaves Exceeding By " + (one_add_day - lc_sl_bal )+ " Day(s)");
                                //    leave();
                                //    dtEndDate.Value = null;
                                //    dtEndDate.Select();
                                //    dtEndDate.Focus();

                                //}
                                //#endregion
                                // go_field('blk_head.w_ans6');
                            }

                            break;
                        #endregion
                    }
                    #endregion
                    #region Post_change



                    if (FunctionConfig.CurrentOption == Function.Modify || FunctionConfig.CurrentOption == Function.Add)
                    {

                        DateTime Enddate;
                        DateTime startDate;

                        if (one_add_day >= 14 && txtLeavetype.Text == "PL")
                        {
                            txtMandatoryAvialed.Text = "Y";

                        }


                        if (txtClAvailed.Text != string.Empty)
                        {
                            lc_cl_avail = int.Parse(txtClAvailed.Text);
                        }

                        if (txtSlAvailed.Text != string.Empty)
                        {

                            lc_sl_avail = int.Parse(txtSlAvailed.Text);
                        }
                        if (txtMlAvailed.Text != string.Empty)
                        {
                            lc_ml_avail = int.Parse(txtMlAvailed.Text);
                        }

                        if (txtPlAvailed.Text != string.Empty)
                        {
                            lc_pl_avail = int.Parse(txtPlAvailed.Text);
                        }


                        Enddate = Convert.ToDateTime(dtEndDate.Value);
                        startDate = Convert.ToDateTime(dtstartdate.Value);
                        TimeSpan ts = Enddate.Subtract(startDate);
                        int count = ts.Days + 1;

                        # region switch Case CL,PL .....
                        switch (txtLeavetype.Text.ToUpper())
                        {

                            case "CL":

                                txtClAvailed.Text = Convert.ToString(lc_cl_avail + count);
                                if (txtClBalance.Text != string.Empty)
                                    lc_cl_bal = Int32.Parse(txtClBalance.Text);
                                if (txtClAvailed.Text != string.Empty)
                                    lc_cl_avail = Int32.Parse(this.txtClAvailed.Text);
                                txtClBalance.Text = Convert.ToString(global_lc_cl_bal - lc_cl_avail);
                                txtApprovedHead.Select();
                                txtApprovedHead.Focus();
                                break;


                            case "SL":
                                txtSlAvailed.Text = Convert.ToString(lc_sl_avail + count);

                                if (txtSlBalance.Text != string.Empty)
                                    lc_sl_bal = Int32.Parse(txtSlBalance.Text);
                                if (txtSlAvailed.Text != string.Empty)
                                    lc_sl_avail = Int32.Parse(this.txtSlAvailed.Text);
                                if (lc_sl_avail > global_lc_sl_bal)
                                {
                                    txtSlBalance.Text = "0";
                                }
                                else
                                {
                                    txtSlBalance.Text = Convert.ToString(global_lc_sl_bal - lc_sl_avail);
                                }
                               
                                break;

                            case "ML":
                                txtMlAvailed.Text = Convert.ToString(lc_ml_avail + count);

                                if (txtMlBalance.Text != string.Empty)
                                    lc_ml_bal = Int32.Parse(txtMlBalance.Text);
                                if (txtMlAvailed.Text != string.Empty)
                                    lc_ml_avail = Int32.Parse(this.txtMlAvailed.Text);
                                txtMlBalance.Text = Convert.ToString(global_lc_ml_bal - lc_ml_avail);

                                break;


                            case "PL":
                                txtPlAvailed.Text = Convert.ToString(lc_pl_avail + count);
                                lc_pl_avail = int.Parse(txtPlAvailed.Text);

                                if ((lc_pl_avail) <= ((global_lc_pl_bal) + (lc_c_forward)))
                                {
                                    txtPlBalance.Text = Convert.ToString(((global_lc_pl_bal) + (lc_c_forward)) - lc_pl_avail);
                                }
                                txtApprovedHead.Select();
                                txtApprovedHead.Focus();
                                break;
                        }
                        #endregion
                    }
                    //   }
                    # endregion
                }
            }
        }
        private void txtLocalCertificate_Validating(object sender, CancelEventArgs e)
        {

            if (txtLocalCertificate.Text != string.Empty)
            {
                if (txtLocalCertificate.Text.ToUpper() != "Y" && txtLocalCertificate.Text.ToUpper() != "N")
                {
                    MessageBox.Show("Field Must Be Y/N ....");
                    e.Cancel = true;
                }
            }
        }
        private void txtMandatoryAvialed_Validating(object sender, CancelEventArgs e)
        {

            if (txtMandatoryAvialed.Text == string.Empty)
            {
                MessageBox.Show("Field Must Be Y/N ....");
                e.Cancel = true;
                return;
            }


            if (txtMandatoryAvialed.Text != string.Empty)
            {
                if ((txtMandatoryAvialed.Text != "Y") && (txtMandatoryAvialed.Text != "N"))
                {
                    MessageBox.Show("Field Must Be Y/N ....");
                    e.Cancel = true;
                    return;
                }
            }


            if (FunctionConfig.CurrentOption == Function.Modify)
            {
                

                Proc_Modify();
                Leave_Cal();
                Proc_update();
                MessageBox.Show("Record Updated Sucessfully ");
                this.Cancel();
            }

            if (FunctionConfig.CurrentOption == Function.Add)
            {


                if (txt_W_MAX_ADV.Text == "0")
                { txt_W_MAX_ADV.Text = "0"; }


                if (MessageBox.Show("Do You Want to save This Record ", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("LEV_PNO", txtPersonnelNO.Text);
                    param.Add("LEV_LV_TYPE", txtLeavetype.Text);
                    param.Add("LEV_START_DATE", dtstartdate.Value.ToString());
                    param.Add("LEV_END_DATE", dtEndDate.Value.ToString());
                    param.Add("LEV_APPROVED", txtLC_CF_APPROVED.Text);
                    param.Add("LEV_REMARKS", txtRemarks.Text);
                    cmnDM.Execute("CHRIS_SP_LEAVE_ACTUAL_MANAGER", "Save", param);
                    Leave_Cal();
                    Proc_update();
                    MessageBox.Show("Record Saved Sucessfully ");
                    AddModeOnYes();
                }
                else
                {
                    this.Cancel();
                }

            }

        }
        private void txtApprovedHead_Validating(object sender, CancelEventArgs e)
        {
            if (txtApprovedHead.Text != string.Empty)
            {
                if (txtApprovedHead.Text.ToUpper() != "Y" && txtApprovedHead.Text.ToUpper() != "N")
                {
                    MessageBox.Show("Field Must Be Y/N ....");
                    e.Cancel = true;
                }
            }
        }
        private void txtOption_TextChanged(object sender, EventArgs e)
        {
            if (txtOption.Text != string.Empty)
            {
                txtApprovedHead.Text = "Y";
            }

            if (txtOption.Text.ToUpper() == "V")
            {
                //Chris_Personnel_ActualLeaveEntry_ grd = new Chris_Personnel_ActualLeaveEntry_();
                //grd.ShowDialog();
                //grd.Close();
                //this.CurrentPanelBlock = PnlLeaveActual.Name;
                //base.Cancel();
                //base.IterateFormToEnableControls(PnlLeaveActual.Controls, false);
                //txtOption.Select();
                //txtOption.Focus();
            }
        }
        protected override void DoFunction(Function function)
        {
            base.DoFunction(function);
            if (function == Function.View)
            {
                base.IterateFormToEnableControls(PnlLeaveActual.Controls, false);
            }
        }
        #endregion



        private bool AddModeOnYes()
        {

            this.Add();
            txtOption.Text = "A";
            txtPersonnelNO.Select();
            txtPersonnelNO.Focus();
            return false;
        }

        private void txtOption_Validating(object sender, CancelEventArgs e)
        {
        }

        private void txtLeavetype_Enter(object sender, EventArgs e)
        {
            if (FunctionConfig.CurrentOption == Function.Add)
            { lbnLeavetype.SkipValidationOnLeave = true; }
            else
            {
                lbnLeavetype.SkipValidationOnLeave = false;
            }
        }

        private void dtEndDate_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            /* Used For Shift +Tab    Start*/
            if ((e.KeyCode == Keys.ShiftKey) || ((e.KeyCode == Keys.LButton) || (e.KeyCode == Keys.Back)))
            {

                ClearTexbox();
                dtstart = false;
               // dtstartdate.Select();
               //dtstartdate.Focus();
            
           }
           /* Used For Shift +Tab    End*/
        }
    }
}