namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlTblBlkEmp = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvBlkEmp = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prPeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.From = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.To = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Organization_Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Designation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlTblBlkEmp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkEmp)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTblBlkEmp
            // 
            this.pnlTblBlkEmp.ConcurrentPanels = null;
            this.pnlTblBlkEmp.Controls.Add(this.dgvBlkEmp);
            this.pnlTblBlkEmp.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblBlkEmp.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBlkEmp.DependentPanels = null;
            this.pnlTblBlkEmp.DisableDependentLoad = false;
            this.pnlTblBlkEmp.EnableDelete = true;
            this.pnlTblBlkEmp.EnableInsert = true;
            this.pnlTblBlkEmp.EnableQuery = false;
            this.pnlTblBlkEmp.EnableUpdate = true;
            this.pnlTblBlkEmp.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntPreEmpCommand";
            this.pnlTblBlkEmp.Location = new System.Drawing.Point(2, 54);
            this.pnlTblBlkEmp.MasterPanel = null;
            this.pnlTblBlkEmp.Name = "pnlTblBlkEmp";
            this.pnlTblBlkEmp.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBlkEmp.Size = new System.Drawing.Size(604, 244);
            this.pnlTblBlkEmp.SPName = "CHRIS_SP_RegStHiEnt_PRE_EMP_MANAGER";
            this.pnlTblBlkEmp.TabIndex = 10;
            // 
            // dgvBlkEmp
            // 
            this.dgvBlkEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkEmp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.prPeNo,
            this.From,
            this.To,
            this.Organization_Address,
            this.Designation,
            this.Remarks,
            this.Address1,
            this.Address2,
            this.Address3,
            this.Address4});
            this.dgvBlkEmp.ColumnToHide = null;
            this.dgvBlkEmp.ColumnWidth = null;
            this.dgvBlkEmp.CustomEnabled = true;
            this.dgvBlkEmp.DisplayColumnWrapper = null;
            this.dgvBlkEmp.GridDefaultRow = 0;
            this.dgvBlkEmp.Location = new System.Drawing.Point(3, 3);
            this.dgvBlkEmp.Name = "dgvBlkEmp";
            this.dgvBlkEmp.ReadOnlyColumns = null;
            this.dgvBlkEmp.RequiredColumns = null;
            this.dgvBlkEmp.Size = new System.Drawing.Size(598, 238);
            this.dgvBlkEmp.SkippingColumns = null;
            this.dgvBlkEmp.TabIndex = 0;
            this.dgvBlkEmp.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvBlkEmp_CellValidating);
            this.dgvBlkEmp.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvBlkEmp_EditingControlShowing);
            this.dgvBlkEmp.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBlkEmp_CellEnter);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.MaxInputLength = 0;
            this.Column1.MinimumWidth = 2;
            this.Column1.Name = "Column1";
            this.Column1.Width = 2;
            // 
            // prPeNo
            // 
            this.prPeNo.DataPropertyName = "PR_P_NO";
            this.prPeNo.HeaderText = "prPeNo";
            this.prPeNo.Name = "prPeNo";
            this.prPeNo.Visible = false;
            // 
            // From
            // 
            this.From.DataPropertyName = "PR_JOB_FROM";
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.From.DefaultCellStyle = dataGridViewCellStyle3;
            this.From.HeaderText = "From";
            this.From.MaxInputLength = 10;
            this.From.Name = "From";
            // 
            // To
            // 
            this.To.DataPropertyName = "PR_JOB_TO";
            dataGridViewCellStyle4.Format = "d";
            dataGridViewCellStyle4.NullValue = null;
            this.To.DefaultCellStyle = dataGridViewCellStyle4;
            this.To.HeaderText = "To";
            this.To.MaxInputLength = 10;
            this.To.Name = "To";
            // 
            // Organization_Address
            // 
            this.Organization_Address.DataPropertyName = "PR_ORGANIZ";
            this.Organization_Address.HeaderText = "Organization Address";
            this.Organization_Address.MaxInputLength = 40;
            this.Organization_Address.Name = "Organization_Address";
            this.Organization_Address.Width = 150;
            // 
            // Designation
            // 
            this.Designation.DataPropertyName = "PR_DESIG_PR";
            this.Designation.HeaderText = "Designation";
            this.Designation.MaxInputLength = 40;
            this.Designation.Name = "Designation";
            // 
            // Remarks
            // 
            this.Remarks.DataPropertyName = "PR_REMARKS_PR";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.MaxInputLength = 40;
            this.Remarks.Name = "Remarks";
            // 
            // Address1
            // 
            this.Address1.DataPropertyName = "PR_ADD1";
            this.Address1.HeaderText = "Address1";
            this.Address1.Name = "Address1";
            this.Address1.Visible = false;
            // 
            // Address2
            // 
            this.Address2.DataPropertyName = "PR_ADD2";
            this.Address2.HeaderText = "Address2";
            this.Address2.Name = "Address2";
            this.Address2.Visible = false;
            // 
            // Address3
            // 
            this.Address3.DataPropertyName = "PR_ADD3";
            this.Address3.HeaderText = "Address3";
            this.Address3.Name = "Address3";
            this.Address3.Visible = false;
            // 
            // Address4
            // 
            this.Address4.DataPropertyName = "PR_ADD4";
            this.Address4.HeaderText = "Address4";
            this.Address4.Name = "Address4";
            this.Address4.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(181, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Previous Employment History";
            // 
            // CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 308);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlTblBlkEmp);
            this.Name = "CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP";
            this.Text = "CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP";
            this.Controls.SetChildIndex(this.pnlTblBlkEmp, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlTblBlkEmp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkEmp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblBlkEmp;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBlkEmp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn prPeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn From;
        private System.Windows.Forms.DataGridViewTextBoxColumn To;
        private System.Windows.Forms.DataGridViewTextBoxColumn Organization_Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn Designation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address4;

    }
}