using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;




namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_BranchManagerTransfer : ChrisMasterDetailForm
    {
        #region Declarations

        Dictionary<string, object> objVals = new Dictionary<string, object>();
        DataTable dtPrsnl = new DataTable();
        DataTable dtDpt = new DataTable();
        DataTable dtTransfer = new DataTable();
        //int netContrib = 0;
        //int contribValue;
        //int contribValue_total;
        // int gridIndex = 0;
        decimal TransferType;
        DateTime dtFirst, dtSecond;
        Function myCurrFunction = Function.None;
        string BranchtTransType = string.Empty;
        CmnDataManager cmnDM = new CmnDataManager();
        Result rslt, rsltDptCnt, rsltTrnfr;
        #endregion

        # region Constructor

        public CHRIS_Personnel_BranchManagerTransfer()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_BranchManagerTransfer(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            lblUserName.Text = "User Name :  " + this.UserName;
            txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");


            List<SLPanel> lstDtlPanel = new List<SLPanel>();
            lstDtlPanel.Add(pnlSDC);
            //lstDtlPanel.Add(pnlTransfer);

            PnlPersonnel.DependentPanels = lstDtlPanel;
            //this.IndependentPanels.Add(PnlPersonnel);

            List<SLPanel> lstTrnsDtlPanel = new List<SLPanel>();
            lstTrnsDtlPanel.Add(pnlTSDC);
            this.pnlTransfer.DependentPanels = lstTrnsDtlPanel;
            this.IndependentPanels.Add(pnlTransfer);

            this.CurrentPanelBlock = pnlTransfer.Name;

            this.dgvSDC.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.dgvTSDC.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.FunctionConfig.EnableF2 = false;
            this.FunctionConfig.EnableF5 = false;
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF9 = false;
            this.FunctionConfig.EnableF10 = false;
        }
        #endregion

        # region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            this.colDept.SearchColumn = "PR_SEGMENT";
            ////this.colDept.SearchColumn = "PR_P_NO";
            this.pnlSDC.Visible = false;
            this.colDept.AttachParentEntity = true;
            tbtDelete.Visible = false;
            tbtAdd.Visible = false;
            tbtList.Visible = false;
            tbtEdit.Visible = false;
            tbtSave.Visible = false;
            dtpTEffDate.Value = null;
            this.CanEnableDisableControls = true;

        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                bool pressed = tbtCancel.Pressed;
                this.lbpersonnel.SkipValidationOnLeave = true;
                base.DoToolbarActions(ctrlsCollection, actionType);
                txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
                dgvTSDC.DataSource = null;
                dtpTEffDate.Value = null;
                this.ClearForm(PnlPersonnel.Controls);
                this.ClearForm(pnlSDC.Controls);
                this.ClearForm(pnlTransfer.Controls);
                this.ClearForm(pnlTSDC.Controls);
                PnlPersonnel.ClearBusinessEntity();
                pnlSDC.ClearBusinessEntity();
                pnlTransfer.ClearBusinessEntity();
                pnlTSDC.ClearBusinessEntity();
                this.FunctionConfig.CurrentOption = Function.None;
                this.dgvSDC.Visible = false;
                this.lbpersonnel.SkipValidationOnLeave = false;
                //this.txtOption.Visible = false;
                //lkbtnDesc.PerformClick();
                //this.txtOption.Visible = true;
                //this.txtOption.Select();
                //this.txtOption.Focus();
                if (pressed)
                {
                    this.BindLOVLookupButton(lkbtnDesc);
                }
                return;
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
            if (actionType == "Save")
            {
                //this.Cancel();
                //CHRIS_Personnel_BranchManagerTransfer_Shown(null, null);
                return;
            }
        }

        protected override bool Add()
        {
            this.lbpersonnel.ActionType = "PP_LOV";
            this.lbpersonnel.ActionLOVExists = "PP_NO_EXISTS";
            if (this.CurrrentOptionTextBox != null) this.txtCurrOption.Text = "ADD";
            this.txtOption.Text = "A";
            this.FunctionConfig.CurrentOption = Function.Add;
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            //List<SLPanel> lstTrnsDtlPanel = new List<SLPanel>();
            //lstTrnsDtlPanel.Add(pnlTSDC);
            //this.pnlTransfer.DependentPanels = lstTrnsDtlPanel;
            //this.IndependentPanels.Add(pnlTransfer);
            this.pnlSDC.Visible = true;
            this.dgvSDC.Visible = true;

            return base.Add();
        }

        protected override bool Edit()
        {
            this.lbpersonnel.ActionType = "PP_LOVMVD";
            this.lbpersonnel.ActionLOVExists = "PP_LOVMVDEXIST";
            if (this.CurrrentOptionTextBox != null) this.txtCurrOption.Text = "MODIFY";
            this.txtOption.Text = "M";
            this.FunctionConfig.CurrentOption = Function.Modify;
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;

            return base.Edit();
        }

        protected override bool View()
        {
            this.lbpersonnel.ActionType = "PP_LOVMVD";
            this.lbpersonnel.ActionLOVExists = "PP_LOVMVDEXIST";
            this.txtOption.Text = "V";
            //this.FunctionConfig.CurrentOption = Function.None;
            //this.txtOption.Text = string.Empty;

            return base.View();
        }

        protected override bool Delete()
        {
            this.lbpersonnel.ActionType = "PP_LOVMVD";
            this.lbpersonnel.ActionLOVExists = "PP_LOVMVDEXIST";

            //bool flag = false;
            if (this.txtCurrOption.Text != null) this.txtCurrOption.Text = "DELETE";
            this.txtOption.Text = "D";
            this.FunctionConfig.CurrentOption = Function.Delete;
            //return flag;

            return base.Delete();
        }

        protected override bool Quit()
        {
            pnlSDC.Visible = false;
            //if (this.FunctionConfig.CurrentOption == Function.None)
            //{
            //    if (base.tlbMain.Items.Count > 0)
            //    {
            //        base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
            //        //base.Close();
            //        //this.Dispose(true);
            //    }
            //    //else

            //}
            //else
            {
                this.FunctionConfig.CurrentOption = Function.None;
                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
                ShowUtilitiesPage(false);
                this.BindLOVLookupButton(lkbtnDesc);
            }
            this.txtOption.Select();
            this.txtOption.Focus(); ;
            return false;
        }
        protected override bool Cancel()
        {
            bool flag = false;

            //this.tbtCancel.PerformClick();
            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
            pnlSDC.Visible = false;
            ShowUtilitiesPage(false);
            return flag;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.Escape)
            {
                this.BindLOVLookupButton(lkbtnDesc);
            }
        }
        private void SetFormInPreviousMode(Function func, string TypeOfBranch)
        {
            base.Cancel();
            base.DoFunction(func);
            this.txtTypeOf.Text = TypeOfBranch;
            this.txtPersonnelNO.Focus();
            if (TypeOfBranch == "Re-located" || TypeOfBranch == "Branch Managers")
            {
                ShowUtilitiesPage(true);
            }
            else
                ShowUtilitiesPage(false);
        }

        private void ResetFields()
        {

            txtPersonnelNO.Text = "";
            //dgvTSDC.DataSource = null;
            DataTable dt = (DataTable)dgvTSDC.DataSource;
            if (dt != null)
            {
                dt.Rows.Clear();
            }
            dgvTSDC.Refresh();
            txtTFLCity.Text = "";
            txtTFuncTitle.Text = "";
            txtTISCord.Text = "";
            txtTNewBranch.Text = "";
            txtTRemarks.Text = "";
            dtpTEffDate.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtDesig.Text = "";
            txtLevel.Text = "";
            txtFunctional.Text = "";
            txtTitle.Text = "";
            txtBranch.Text = "";
            txtTType.Text = "";
            //dgvSDC.DataSource = null;
            dt = (DataTable)dgvSDC.DataSource;
            if (dt != null)
            {
                dt.Rows.Clear();
            }
            dgvSDC.Refresh();



        }

        private void Pr_P_No_KeyNext()
        {
            if (txtPersonnelNO.Text != string.Empty)
            {
                txtPr_no_hidden.Text = txtPersonnelNO.Text;
                IDHidden.Text = IDHiddenPrsonl.Text;
                BranchManagerTransferCommand ent = (BranchManagerTransferCommand)this.pnlTransfer.CurrentBusinessEntity;
                if (ent != null)
                {
                    double pr_p = 0;
                    double.TryParse(txtPersonnelNO.Text, out pr_p);
                    (this.pnlTransfer.CurrentBusinessEntity as BranchManagerTransferCommand).PR_P_NO = pr_p;

                    if (IDHiddenPrsonl.Text != string.Empty && txtPersonnelNO.Text != string.Empty)
                    {
                        ent.PR_P_NO = double.Parse(txtPersonnelNO.Text.Trim() == "" ? "0" : txtPersonnelNO.Text);
                        ent.ID = Int32.Parse(IDHiddenPrsonl.Text);
                    }
                    //this.pnlTransfer.LoadDependentPanels();
                }
                if (FunctionConfig.CurrentOption == Function.Add)
                {
                    (this.pnlTransfer.CurrentBusinessEntity as BranchManagerTransferCommand).PR_OLD_BRANCH = (PnlPersonnel.CurrentBusinessEntity as PersonnelCommand).PR_NEW_BRANCH;
                    this.pnlTransfer.LoadDependentPanels();
                }

                if (FunctionConfig.CurrentOption == Function.Modify || FunctionConfig.CurrentOption == Function.Delete || FunctionConfig.CurrentOption == Function.View)
                {

                    //if (ent.ID == 0)
                    //{
                    //    MessageBox.Show("NO DATA FOUND FOR THIS OPTION", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    SetFormInPreviousMode(FunctionConfig.CurrentOption, txtTypeOf.Text);
                    //    return;
                    //}

                    Result rslt;
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("PR_P_NO", txtPr_no_hidden.Text);
                    param.Add("PR_TRANSFER_TYPE", txtTType.Text);
                    rslt = cmnDM.GetData("CHRIS_TRANSFER_BRANCHMANAGER_MANAGER", "GetAll_Fields", param);

                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {

                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                (this.pnlTransfer.CurrentBusinessEntity as BranchManagerTransferCommand).PR_OLD_BRANCH = (PnlPersonnel.CurrentBusinessEntity as PersonnelCommand).PR_NEW_BRANCH;

                                txtTNewBranch.Text = rslt.dstResult.Tables[0].Rows[0]["PR_NEW_BRANCH"].ToString();//1
                                txtTFLCity.Text = rslt.dstResult.Tables[0].Rows[0]["PR_FURLOUGH"].ToString();//2
                                txtTFuncTitle.Text = rslt.dstResult.Tables[0].Rows[0]["PR_FUNC_1"].ToString();//3
                                txtTISCord.Text = rslt.dstResult.Tables[0].Rows[0]["PR_FUNC_2"].ToString();//4
                                if (rslt.dstResult.Tables[0].Rows[0]["PR_EFFECTIVE"] != null)
                                {
                                    (this.pnlTransfer.CurrentBusinessEntity as BranchManagerTransferCommand).PR_EFFECTIVE = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0]["PR_EFFECTIVE"].ToString());
                                    dtpTEffDate.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0]["PR_EFFECTIVE"].ToString());//5
                                }
                                txtTRemarks.Text = rslt.dstResult.Tables[0].Rows[0]["PR_REMARKS"].ToString();//6
                                txt_PR_RENT.Text = rslt.dstResult.Tables[0].Rows[0]["PR_RENT"].ToString();
                                txt_PR_UTILITIES.Text = rslt.dstResult.Tables[0].Rows[0]["PR_UTILITIES"].ToString();
                                txt_PR_TAX_ON_TAX.Text = rslt.dstResult.Tables[0].Rows[0]["PR_TAX_ON_TAX"].ToString();
                                this.pnlTransfer.SetEntityObject(rslt.dstResult.Tables[0].Rows[0]);
                                this.pnlTransfer.LoadDependentPanels();
                                (pnlTransfer.CurrentBusinessEntity as BranchManagerTransferCommand).PR_OLD_BRANCH = (PnlPersonnel.CurrentBusinessEntity as PersonnelCommand).PR_NEW_BRANCH;
                            }
                            else
                            {
                                if (FunctionConfig.CurrentOption == Function.Modify)
                                {
                                    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                                    (this.pnlTransfer.CurrentBusinessEntity as BranchManagerTransferCommand).PR_OLD_BRANCH = (PnlPersonnel.CurrentBusinessEntity as PersonnelCommand).PR_NEW_BRANCH;
                                    this.pnlTransfer.LoadDependentPanels();
                                }
                            }

                        }
                    }
                }

                if (txtOption.Text == "D" && txtCurrOption.Text == "DELETE")
                {
                    DialogResult dRes = MessageBox.Show("Do you want to Delete this record [Y/N]..", "Question"
                                  , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes == DialogResult.Yes)
                    {

                        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                        pnlTransfer.Focus();

                        //base.DoToolbarActions(pnlTransfer.Controls, "Delete");
                        base.PerformAction(this.pnlTransfer, "Delete");
                        if (!(this.Message.HasErrors()))
                        {
                            this.SetMessage("Record ", "Record deleted successfully", String.Empty, MessageType.Information);
                            if (this.GetCurrentPanelBlock is SLPanelSimple)
                            {
                                this.ClearForm(this.GetCurrentPanelBlock.Controls);
                                this.GetCurrentPanelBlock.CurrentBusinessEntity = RuntimeClassLoader.GetBusinessEntity(this.GetCurrentPanelBlock.EntityName);
                                this.GetCurrentPanelBlock.LoadDependentPanels();
                                this.dtpTEffDate.Value = null;
                            }
                        }
                        this.Message.ShowMessage();
                        DialogResult viewMsg = MessageBox.Show("Continue The Process For More Records Yes or No", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (viewMsg == DialogResult.Yes)
                        {
                            SetFormInPreviousMode(Function.Delete, txtTypeOf.Text);
                            return;

                        }
                        else if (viewMsg == DialogResult.No)
                        {
                            this.FunctionConfig.CurrentOption = Function.None;
                            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
                            this.BindLOVLookupButton(lkbtnDesc);
                            this.txtOption.Select();
                            this.txtOption.Focus(); ;
                            //this.Refresh();
                            //txtOption.Focus();
                            //return;
                        }
                        //SetFormInPreviousMode(Function.Delete, txtTypeOf.Text);
                        ////base.DoToolbarActions(pnlTransfer.Controls, "Cancel");
                        ////txtOption.Select();
                        ////txtOption.Focus();
                        ////this.BindLOVLookupButton(lkbtnDesc);

                        ////this.Reset();
                        ////call save
                        return;

                    }
                    else if (dRes == DialogResult.No)
                    {
                        // this.Reset();
                        base.DoToolbarActions(pnlTransfer.Controls, "Cancel");
                        txtOption.Focus();

                        return;
                    }

                }
                if (FunctionConfig.CurrentOption == Function.View || (txtOption.Text == "V" && txtCurrOption.Text == "VIEW"))
                {
                    DialogResult viewMsg = MessageBox.Show("Do You Want To View More Record [Y/N]", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (viewMsg == DialogResult.Yes)
                    {

                        //  this.ResetFields();
                        SetFormInPreviousMode(Function.View, txtTypeOf.Text);
                        return;

                    }
                    else if (viewMsg == DialogResult.No)
                    {
                        this.FunctionConfig.CurrentOption = Function.None;
                        this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
                        this.BindLOVLookupButton(lkbtnDesc);
                        this.txtOption.Select();
                        this.txtOption.Focus(); ;
                        //this.Refresh();
                        //txtOption.Focus();
                        //return;
                    }
                }

            }

        }

        private void ShowUtilitiesPage(bool show)
        {
            label10.Visible = show;
            label11.Visible = show;
            label12.Visible = show;
            txt_PR_RENT.Visible = show;
            txt_PR_TAX_ON_TAX.Visible = show;
            txt_PR_UTILITIES.Visible = show;

        }
        #endregion

        # region Events
        private void CHRIS_Personnel_BranchManagerTransfer_Shown(object sender, EventArgs e)
        {
            this.BindLOVLookupButton(lkbtnDesc);
            //this.txtOption.Visible = false;

            //lkbtnDesc.PerformClick();
            //this.txtOption.Visible = true;
            //this.txtOption.Select();
            //this.txtOption.Focus();

            //if (txtTypeOf.Text != "")
            //{
            //    //txtPersonnelNO.Focus();
            //    //lbpersonnel.Focus();

            //}
            //if (txtTypeOf.Text == "Exit" || txtTypeOf.Text == "EXIT")
            //{
            //    this.Close();
            //}

        }

        private void txtOption_TextChanged(object sender, EventArgs e)
        {
            this.txtTempOption.Text = this.txtOption.Text;
        }

        private void txtOption_Validated(object sender, EventArgs e)
        {
            this.txtTempOption.Text = this.txtOption.Text;

            //if (txtOption.Text == "A")
            //{
            //    txtCurrOption.Text = "ADD";

            //}
            //else if (txtOption.Text == "M")
            //{
            //    txtCurrOption.Text = "MODIFY";

            //}
            //else if (txtOption.Text == "D")
            //{
            //    txtCurrOption.Text = "DELETE";

            //}
            //else if (txtOption.Text == "V")
            //{
            //    txtCurrOption.Text = "VIEW";
            //    //   this.ResetFields();
            //    txtPersonnelNO.Focus();
            //}


        }

        private void dgvTSDC_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (tbtCancel.Pressed || tbtClose.Pressed || FunctionConfig.CurrentOption == Function.None)
                return;
            if (e.ColumnIndex != dgvTSDC.Columns["colSeg"].Index && !this.dgvTSDC.CurrentCell.IsInEditMode)
            {
                if (e.ColumnIndex == dgvTSDC.Columns["PR_CONTRIB1"].Index)
                {
                    double totalCon = 0;
                    if (dgvTSDC.DataSource as DataTable != null)
                        double.TryParse((dgvTSDC.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalCon);
                    if (totalCon == 100)
                        txtTRemarks.Focus();
                }

                return;
                #region COmmentedCode
                //if (e.ColumnIndex == dgvTSDC.Columns["PR_CONTRIB1"].Index && e.RowIndex != dgvTSDC.Rows.Count)
                //{
                //    double totalCon = 0;
                //    if (dgvTSDC.DataSource as DataTable != null)
                //        double.TryParse((dgvTSDC.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalCon);
                //    if (totalCon == 100)
                //        txtTRemarks.Focus();
                //    if (dgvTSDC.CurrentCell.Value.ToString() == "")
                //    {
                //        MessageBox.Show("CONTRIBUTION MARGIN SHOULD BE LESS THAN OR EQUAL TO 100 ...", "Note", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //        e.Cancel = true;
                //    }
                //    else if (dgvTSDC.CurrentCell.Value.ToString() == "0")
                //    {
                //        e.Cancel = true;
                //    }
                //}
                //else if (e.ColumnIndex == dgvTSDC.Columns["colSeg"].Index && e.RowIndex != dgvTSDC.Rows.Count)
                //{
                //    if (dgvTSDC.CurrentCell.Value.ToString() == "")
                //    {
                //        MessageBox.Show("YOU HAVE ENTERED AN INVALID SEGMENT.ENTER EITHER 'GF' OR 'GCB'");
                //        e.Cancel = true;
                //    }
                //}


                //return;
                #endregion
            }
            //else if (e.ColumnIndex == dgvTSDC.Columns["colSeg"].Index && !this.dgvTSDC.CurrentCell.IsInEditMode)
            //{
            //    if (this.dgvTSDC.EndEdit())
            //    {
            //        dtpTEffDate.Select();
            //        dtpTEffDate.Focus();
            //    }
            //}

            if (FunctionConfig.CurrentOption == Function.Add || FunctionConfig.CurrentOption == Function.Modify)
            {
                if (e.ColumnIndex == dgvTSDC.Columns["colSeg"].Index)
                {
                    if (dgvTSDC.CurrentRow.Cells[e.ColumnIndex].EditedFormattedValue.ToString() != "" || dgvTSDC.CurrentRow.Cells[e.ColumnIndex].EditedFormattedValue.ToString() != string.Empty)
                    {
                        string seg = dgvTSDC.CurrentRow.Cells[0].EditedFormattedValue.ToString();
                        if (seg.ToUpper() == "GCB" || seg.ToUpper() == "GF")
                        {
                            this.dgvTSDC[e.ColumnIndex, e.RowIndex].Value = seg.ToUpper();
                            (this.pnlTSDC.CurrentBusinessEntity as iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelDept_ContCommand).PR_SEGMENT = seg.ToUpper();
                            //if (this.dgvTSDC.HasDataBoundItem(e.RowIndex) && this.dgvTSDC.Rows[e.RowIndex].DataBoundItem != null)
                            //{
                            //    if ((this.dgvTSDC.Rows[e.RowIndex].DataBoundItem as DataRowView).Row.RowState == DataRowState.Detached)
                            //    {
                            //        (this.dgvTSDC.Parent as SLPanel).SetEntityObject((this.dgvTSDC.Rows[e.RowIndex].DataBoundItem as DataRowView).Row);
                            //    }
                            //}
                            // this.dgvTSDC.CurrentCell.Value = seg.ToUpper();
                        }
                        else
                        {
                            MessageBox.Show("YOU HAVE ENTERED AN INVALID SEGMENT.ENTER EITHER 'GF' OR 'GCB'");
                            e.Cancel = true;
                        }

                    }
                    else
                    {
                        if (!this.dgvTSDC.CurrentCell.IsInEditMode)
                            this.dgvTSDC.BeginEdit(false);
                        MessageBox.Show("YOU HAVE ENTERED AN INVALID SEGMENT.ENTER EITHER 'GF' OR 'GCB'");
                        e.Cancel = true;
                    }
                }
                if (e.ColumnIndex == dgvTSDC.Columns["PR_CONTRIB1"].Index)
                {
                    if (dgvTSDC[e.ColumnIndex, e.RowIndex].IsInEditMode)
                    {
                        if (dgvTSDC[e.ColumnIndex, e.RowIndex].EditedFormattedValue != null && dgvTSDC[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString() != string.Empty)
                        {
                            Double GlobalSum = 0;
                            Double Contribution = 0;
                            Double PreviousValue = 0;
                            if (dgvTSDC[e.ColumnIndex, e.RowIndex].Value != null)
                                double.TryParse(dgvTSDC[e.ColumnIndex, e.RowIndex].Value.ToString(), out PreviousValue);

                            Double.TryParse(dgvTSDC[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString(), out Contribution);
                            Contribution = Math.Round(Contribution, 0, MidpointRounding.AwayFromZero);
                            double totalCon = 0;

                            if (Contribution > 100)
                            {
                                MessageBox.Show("CONTRIBUTION MARGIN SHOULD BE LESS THAN OR EQUAL TO 100 ...", "Note", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                //MessageBox.Show("If The Contribution Is Equal To 100 Then Press SAVE Button To Save The Record", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                e.Cancel = true;

                                return;
                            }

                            else if (Contribution < 1)
                            {
                                e.Cancel = true;
                                return;
                            }

                            else if (Contribution <= 100)
                            {
                                if (dgvTSDC.DataSource as DataTable != null)
                                    double.TryParse((dgvTSDC.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalCon);
                                totalCon = totalCon + Contribution - PreviousValue;

                                if (totalCon < 100)
                                {
                                    dgvTSDC.CurrentCell.Value = Contribution;
                                }
                                if (totalCon == 100)
                                {

                                    dgvTSDC.CurrentCell.Value = Contribution;

                                    dgvTSDC.EndEdit();
                                    txtTRemarks.Focus();
                                    txtTRemarks.Select();
                                    return;
                                }
                                if (totalCon > 100)
                                {
                                    MessageBox.Show("YOUR CONTRIBUTION % SUM FOR THE CURRENT ID IS EXCEEDING 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    totalCon = totalCon - Contribution;
                                    e.Cancel = true;
                                }


                            }


                        }
                        else
                        {
                            MessageBox.Show("CONTRIBUTION MARGIN SHOULD BE LESS THAN OR EQUAL TO 100 ...", "Note", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Cancel = true;
                        }


                    }
                }

            }
            #region recent Commented Code
            //if (contribValue_total == 100)
            //{
            //    dgvTSDC.RejectChanges();
            //    //dgvTSDC.CurrentRow.ReadOnly = true;
            //    return;

            //}

            //if (txtCurrOption.Text == "ADD" || txtCurrOption.Text == "MODIFY")
            //{
            //    if (e.ColumnIndex == 0)
            //    {

            //        if (dgvTSDC.CurrentRow.Cells[0].EditedFormattedValue.ToString() != "" || dgvTSDC.CurrentRow.Cells[0].EditedFormattedValue.ToString() != null)
            //        {
            //            string seg = dgvTSDC.CurrentRow.Cells[0].EditedFormattedValue.ToString();
            //            if (seg.ToUpper() == "GCB" || seg.ToUpper() == "GF")
            //            {
            //                this.dgvTSDC.CurrentCell.Value = seg.ToUpper();
            //            }
            //            else
            //            {
            //                MessageBox.Show("YOU HAVE ENTERED AN INVALID SEGMENT.ENTER EITHER 'GF' OR 'GCB'");
            //                e.Cancel = true;
            //            }

            //        }
            //    }

            //    //---------------------------------CONTRIB --------------------------//
            //    if (e.ColumnIndex == 2)
            //    {
            //        if (dgvTSDC.Rows[e.RowIndex].Cells[2].IsInEditMode)
            //        {
            //            if (dgvTSDC.CurrentRow.Cells[2].EditedFormattedValue.ToString() != "" || dgvTSDC.CurrentRow.Cells[2].EditedFormattedValue.ToString() != null)
            //            {
            //                contribValue = 0;
            //                //contribValue = Convert.ToInt32(dgvTSDC.CurrentRow.Cells["PR_CONTRIB1"].EditedFormattedValue);

            //                //Double new_Val = contribValue;
            //                //Double pr_Sum = Double.Parse(dgvTSDC.GridSource.Compute("SUM(PR_CONTRIB)", "").ToString());

            //                //if ((new_Val + pr_Sum) > 100)
            //                //{
            //                //    MessageBox.Show("stop");
            //                //    e.Cancel = true;
            //                //}


            //                if ((contribValue < 1 || contribValue <= 100) && (contribValue_total != 0 || contribValue_total < 100))
            //                {

            //                    contribValue_total = contribValue_total + contribValue;
            //                    //dgvSDC.Rows[gridIndex].ReadOnly = true;
            //                    //gridIndex = gridIndex + 1;

            //                }
            //                if (contribValue_total > 100 || (contribValue_total == 100 && contribValue == 0))
            //                {
            //                    //MessageBox.Show("Ruk");
            //                    e.Cancel = true;
            //                    contribValue_total = contribValue_total - contribValue;
            //                    return;

            //                }




            //            }
            //        }
            //    }
            //    #region Commented Code
            //    //----------------------------- END OF CONTRIB ----------------------//

            //    //if (e.ColumnIndex == 2)
            //    //{
            //    //    if (txtSum1.Text != string.Empty)
            //    //    {
            //    //        Sum1 = Convert.ToInt32(txtSum1.Text);
            //    //    }
            //    //    if (W_VAL1.Text != string.Empty)
            //    //    {
            //    //        WVal1 = Convert.ToInt32(W_VAL1.Text);
            //    //    }

            //    //    if (DGVDept.Rows[e.RowIndex].Cells[2].IsInEditMode)
            //    //    {
            //    //        if (DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue.ToString() != string.Empty)
            //    //        {
            //    //            if (Convert.ToInt32(DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue) <= 100)
            //    //            {
            //    //                Sum1 = Sum1 + Convert.ToInt32(DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue) - WVal1;
            //    //                WVal1 = 0;
            //    //            }
            //    //            if (Sum1 == 100)
            //    //            {
            //    //                W_VAL1.Text = DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue.ToString();
            //    //                txtSum1.Text = "0";

            //    //                if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
            //    //                {
            //    //                    DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
            //    //                                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    //                    if (dRes == DialogResult.Yes)
            //    //                    {
            //    //                        this.txtOption.Focus();
            //    //                        iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand)this.pnlDetail.CurrentBusinessEntity;
            //    //                        if (ent != null)
            //    //                        {
            //    //                            ent.PR_EFFECTIVE = (DateTime)(this.dtpPR_EFFECTIVE.Value);
            //    //                        }
            //    //                        base.DoToolbarActions(this.Controls, "Save");
            //    //                        //this.Reset();
            //    //                        //call save
            //    //                        return;

            //    //                    }
            //    //                    else if (dRes == DialogResult.No)
            //    //                    {
            //    //                        // this.Reset();
            //    //                        txtOption.Focus();

            //    //                        return;
            //    //                    }
            //    //                }

            //    //            }

            //    //            if (Sum1 > 100)/*** IF % > 100 ***/
            //    //            {
            //    //                MessageBox.Show("YOUR CONTRIBUTION % SUM FOR THE CURRENT ID IS EXCEEDING 100");
            //    //                e.Cancel = true;
            //    //                Sum1 = Sum1 - Convert.ToInt32(DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue);
            //    //                txtSum1.Text = Sum1.ToString();


            //    //            }


            //    //        }

            //    //    }

            //    //}
            //    #endregion

            //}
            #endregion

        }

        private void dgvTSDC_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //if (dgvTSDC.DataSource != null)
                //{
                //    if (dgvTSDC.EndEdit())
                //    {
                //        double totalCon = 0;
                //        double.TryParse((dgvTSDC.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalCon);
                //        if (totalCon == 100)
                //            txtTRemarks.Focus();
                //        else if (totalCon < 100)
                //        {
                //            if (dgvTSDC.Rows[e.RowIndex+1] != null)
                //            {
                //                this.dgvTSDC.Rows[e.RowIndex + 1].Cells[0].Selected = true;
                //                this.dgvTSDC.BeginEdit(true);

                //            }
                //        }
                //    }
                //}

            }
            //if (contribValue_total == 100)
            //{
            //    dgvTSDC.ReadOnly = true;
            //    txtTRemarks.Focus();
            //    return;
            //}
            //if (!dgvTSDC.CurrentRow.IsNewRow)
            //    dgvTSDC.Rows[e.RowIndex].ReadOnly = true;
        }

        private void CHRIS_Personnel_BranchManagerTransfer_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (tbtCancel.Pressed || tbtClose.Pressed /*|| FunctionConfig.CurrentOption == Function.None*/)
                return;
            if (actionType == "MAIN_LOV_EXIST" || actionType == "MAIN_LOV")
            {
                if (selectedRow != null)
                {
                    TransferType = 0;
                    if (selectedRow.ItemArray[0].ToString().Equals("Branch Managers"))
                        TransferType = 5;
                    else if (selectedRow.ItemArray[0].ToString().Equals("Re-located"))
                        TransferType = 4;
                    else if (selectedRow.ItemArray[0].ToString().Equals("Inter Branch"))
                        TransferType = 3;
                    else if (selectedRow.ItemArray[0].ToString().Equals("LOCAL"))
                        TransferType = 5;

                    if (txtTypeOf.Text == "Exit" || txtTypeOf.Text == "EXIT")
                    {
                        this.Close();
                    }
                    if (txtTypeOf.Text == "Re-located".ToUpper() || txtTypeOf.Text == "Branch Managers".ToUpper())
                    {
                        ShowUtilitiesPage(true);
                    }
                    else
                        ShowUtilitiesPage(false);

                }
            }
            if (actionType == "PP_LOV" || actionType == "PP_NO_EXISTS" || actionType == "PP_LOVMVD" || actionType == "PP_LOVMVDEXIST")
            {
                // if (selectedRow != null)
                //    Pr_P_No_KeyNext();
            }
            #region commented Code
            //lbpersonnel.Focus();


            //Refreshing the TRANSFER fields//
            //if ( (txtOption.Text == "A" || txtOption.Text == "D")  && txtPersonnelNO.Text != "")
            //{
            //    txtTFLCity.Text = "";
            //    txtTFuncTitle.Text = "";
            //    txtTISCord.Text = "";
            //    txtTType.Text = "";
            //    txtTRemarks.Text = "";
            //    dtpTEffDate.Text = "";
            //    //dgvTSDC.DataSource = null;
            //    //dgvTSDC.Refresh();

            //}

            //---------------POPULATING THE UPPER DEPT_CONT GRID-----------------//
            //if (txtPersonnelNO.Text != "" && txtTypeOf.Text != "")
            //{

            //this.objVals.Clear();
            //this.objVals.Add("PR_P_NO", txtPersonnelNO.Text);
            //rsltDptCnt = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "DPTCONT_GRID", objVals);

            //if (rsltDptCnt.isSuccessful)
            //{
            //    if (rsltDptCnt.dstResult.Tables.Count > 0)
            //    {
            //        dtDpt = rsltDptCnt.dstResult.Tables[0];
            //        this.dgvSDC.DataSource = dtDpt;
            //    }

            //}
            //else
            //{
            //    dgvSDC.DataSource = null;
            //    dgvSDC.Refresh();

            //}
            //}
            //----------------- W_ANS3 == DELETE CASE --------------------//
            //if (txtOption.Text == "D" && txtCurrOption.Text == "DELETE")
            //{

            //    DialogResult delMsg = MessageBox.Show("Do You Want To Delete The Record [Y/N]..", "Note", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    if (delMsg == DialogResult.Yes)
            //    {

            //        //this.objVals.Clear();
            //        //this.objVals.Add("PR_P_NO", txtPersonnelNO.Text);
            //        //rslt = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "DELETE", objVals);
            //        base.DoToolbarActions(this.Controls, "Delete");
            //        if (rslt.isSuccessful)
            //        {
            //           // this.ResetFields();

            //        }

            //        return;

            //    }
            //    else if (delMsg == DialogResult.No)
            //    {

            //        txtOption.Focus();
            //        return;
            //    }


            //}
            //--------------------------W_ANS == VIEW CASE --------------------------//

            //if (FunctionConfig.CurrentOption == Function.View  || (txtOption.Text == "V" && txtCurrOption.Text == "VIEW") )
            //{

            //    DialogResult viewMsg = MessageBox.Show("Do You Want To View More Record [Y/N]", "Note", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    if (viewMsg == DialogResult.Yes)
            //    {

            //      //  this.ResetFields();
            //        txtOption.Focus();
            //        return;

            //    }
            //    else if (viewMsg == DialogResult.No)
            //    {
            //        this.Refresh();
            //        txtOption.Focus();
            //        return;
            //    }


            //}
            #endregion

        }

        /// <summary>
        /// Date time Picker (PR_EFFECTIVE)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpTEffDate_Validating(object sender, CancelEventArgs e)
        {
            if (tbtCancel.Pressed || tbtClose.Pressed || FunctionConfig.CurrentOption == Function.None)
                return;

            DateTime dtimeTransfer = new DateTime();
            DateTime dtimeTransfer_proc1 = new DateTime();
            if (dtpTEffDate.Value != null)
            {
                if (txtPersonnelNO.Text != "")
                {

                    this.objVals.Clear();
                    this.objVals.Add("PR_P_NO", txtPersonnelNO.Text);

                    rsltTrnfr = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "TRNSFR_VAL", objVals);

                    if (rsltTrnfr.isSuccessful)
                    {
                        if (rsltTrnfr.dstResult.Tables.Count > 0)
                        {
                            dtTransfer = rsltTrnfr.dstResult.Tables[0];
                            if (dtTransfer.Rows.Count > 0)
                            {
                                dtFirst = Convert.ToDateTime(dtpTEffDate.Value);

                                if (rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0] != DBNull.Value /*|| rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0] != null*/)
                                    dtSecond = Convert.ToDateTime(rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                                if (rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[1] != DBNull.Value /*|| rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[1] != null*/)
                                    DateTime.TryParse(rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[1].ToString(), out  dtimeTransfer);
                                //if (DateTime.Compare(Convert.ToDateTime(dtpTEffDate.Text), Convert.ToDateTime(dtTransfer.Rows[0]["PR_JOINING_DATE"])) < 0)
                                if (dtFirst < dtSecond)
                                {
                                    MessageBox.Show("DATE HAS TO BE GREATER THAN THE JOINING DATE :  " + dtSecond.ToString("dd-MMM-yyyy").ToUpper()); //Convert.ToString(dtTransfer.Rows[0]["PR_JOINING_DATE"]).ToString("dd-MMM-yyyy"));
                                    //dtpTEffDate.Value = null;
                                    //dtpTEffDate.Focus();
                                    e.Cancel = true;
                                    return;
                                }
                                //horrible condition.if (dtpTEffDate.Text == Convert.ToString(dtTransfer.Rows[0]["PR_JOINING_DATE"]))
                                if (dtimeTransfer.Year != 1)
                                {
                                    if (this.FunctionConfig.CurrentOption == Function.Add)
                                    {
                                        if (dtFirst == dtimeTransfer)
                                        {
                                            MessageBox.Show("RECORD FOR THIS DATE ALREADY EXISTS ");
                                            //dtpTEffDate.Value = null;
                                            //dtpTEffDate.Focus();
                                            e.Cancel = true;
                                            return;
                                        }
                                        else
                                        {
                                            #region PROC1


                                            this.objVals.Clear();
                                            this.objVals.Add("PR_P_NO", txtPersonnelNO.Text);
                                            this.objVals.Add("PR_EFF", dtpTEffDate.Value);

                                            rsltTrnfr = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "TRNSFR_025", objVals);

                                            if (rsltTrnfr.isSuccessful)
                                            {
                                                if (rsltTrnfr.dstResult != null && rsltTrnfr.dstResult.Tables.Count > 0)
                                                {
                                                    if (rsltTrnfr.dstResult.Tables[0].Rows.Count == 1)
                                                    {
                                                        if (rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                                            DateTime.TryParse(rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out  dtimeTransfer_proc1);
                                                        MessageBox.Show("  INAVLID DATE. DATE HAS TO BE GREATER THAN  " + dtimeTransfer_proc1.ToString("dd-MMM-yyyy").ToUpper(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                        e.Cancel = true;
                                                        return;
                                                        //if (dtimeTransfer_proc1.Year != 1){}

                                                    }
                                                    else if (rsltTrnfr.dstResult.Tables[0].Rows.Count == 0)
                                                    {
                                                        dgvTSDC.Focus();
                                                        //dgvTSDC.BeginEdit(false);
                                                    }
                                                    else if (rsltTrnfr.dstResult.Tables[0].Rows.Count > 1)
                                                    {
                                                        if (rsltTrnfr.dstResult.Tables[1].Rows.Count > 0)
                                                        {
                                                            //rsltTrnfr.dstResult.Tables[1] contains SELECT MAX(pr_effective) FROM   TRANSFER WITH (NOLOCK)WHERE  pr_tr_no = @PR_P_NO
                                                            if (rsltTrnfr.dstResult.Tables[1].Rows[0].ItemArray[0] != null)
                                                                DateTime.TryParse(rsltTrnfr.dstResult.Tables[1].Rows[0].ItemArray[0].ToString(), out dtimeTransfer_proc1);
                                                            MessageBox.Show("  INAVLID DATE. DATE HAS TO BE GREATER THAN  " + dtimeTransfer_proc1.ToString("dd-MMM-yyyy").ToUpper(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                            e.Cancel = true;
                                                            return;

                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                        }

                                    }
                                    else if (this.FunctionConfig.CurrentOption == Function.Modify)
                                    {
                                        if (dtFirst != dtimeTransfer)
                                        {
                                            #region PROC1


                                            this.objVals.Clear();
                                            this.objVals.Add("PR_P_NO", txtPersonnelNO.Text);
                                            this.objVals.Add("PR_EFF", dtpTEffDate.Value);

                                            rsltTrnfr = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "TRNSFR_025", objVals);

                                            if (rsltTrnfr.isSuccessful)
                                            {
                                                if (rsltTrnfr.dstResult != null && rsltTrnfr.dstResult.Tables.Count > 0)
                                                {
                                                    if (rsltTrnfr.dstResult.Tables[0].Rows.Count == 1)
                                                    {
                                                        if (rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                                            DateTime.TryParse(rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out  dtimeTransfer_proc1);
                                                        MessageBox.Show("  INAVLID DATE. DATE HAS TO BE GREATER THAN  " + dtimeTransfer_proc1.Date.ToShortDateString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                        e.Cancel = true;
                                                        return;
                                                        //if (dtimeTransfer_proc1.Year != 1){}

                                                    }
                                                    else if (rsltTrnfr.dstResult.Tables[0].Rows.Count == 0)
                                                    {
                                                        dgvTSDC.Focus();
                                                        //dgvTSDC.BeginEdit(false);
                                                    }
                                                    else if (rsltTrnfr.dstResult.Tables[0].Rows.Count > 1)
                                                    {
                                                        if (rsltTrnfr.dstResult.Tables[1].Rows.Count > 0)
                                                        {
                                                            //rsltTrnfr.dstResult.Tables[1] contains SELECT MAX(pr_effective) FROM   TRANSFER WITH (NOLOCK)WHERE  pr_tr_no = @PR_P_NO
                                                            if (rsltTrnfr.dstResult.Tables[1].Rows[0].ItemArray[0] != null)
                                                                DateTime.TryParse(rsltTrnfr.dstResult.Tables[1].Rows[0].ItemArray[0].ToString(), out dtimeTransfer_proc1);
                                                            MessageBox.Show("  INAVLID DATE. DATE HAS TO BE GREATER THAN  " + dtimeTransfer_proc1.Date.ToShortDateString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                            e.Cancel = true;
                                                            return;

                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            dgvTSDC.Focus();
                                            //dgvTSDC.BeginEdit(false);
                                        }
                                    }

                                }

                                dgvTSDC.Focus();
                                //dgvTSDC.BeginEdit(false);
                            }

                        }
                    }
                }
            }
            //else
            //{
            //    MessageBox.Show()
            //}


            //Validating Next Event//
            //this.objVals.Clear();
            //this.objVals.Add("PR_P_NO", txtPersonnelNO.Text);
            //this.objVals.Add("PR_EFF", dtpTEffDate.Value);

            //rsltTrnfr = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "TRNSFR_025", objVals);

            //if (rsltTrnfr.isSuccessful)
            //{
            //    if (rsltTrnfr.dstResult.Tables.Count > 0)
            //    {
            //        dtTransfer = rsltTrnfr.dstResult.Tables[0];
            //        if (dtTransfer.Rows.Count > 0)
            //        {

            //            if (Convert.ToDateTime(dtpTEffDate.Value) < Convert.ToDateTime(dtTransfer.Rows[0]["global_transfer_effect"]))
            //            {
            //                MessageBox.Show("INAVLID DATE. DATE HAS TO BE GREATER THAN :  " + Convert.ToString(dtTransfer.Rows[0]["global_transfer_effect"]));
            //                dtpTEffDate.Text = "";
            //                dtpTEffDate.Focus();
            //                return;
            //            }


            //        }
            //    }

            //}





        }

        private void txtPersonnelNO_TextChanged(object sender, EventArgs e)
        {
            if (txtPersonnelNO.Text != "" && txtTypeOf.Text != "")
            {

                //this.objVals.Clear();
                //this.objVals.Add("PR_P_NO", txtPersonnelNO.Text);
                //rsltDptCnt = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "DPTCONT_GRID", objVals);

                //if (rsltDptCnt.isSuccessful)
                //{
                //    if (rsltDptCnt.dstResult.Tables.Count > 0)
                //    {
                //        dtDpt = rsltDptCnt.dstResult.Tables[0];
                //        this.dgvSDC.DataSource = dtDpt;
                //    }

                //}
                //else
                //{
                //    dgvSDC.DataSource = null;
                //    dgvSDC.Refresh();

                //}

                //Refreshing the TRANSFER fields//
                if ((txtOption.Text == "A" || txtOption.Text == "D") && txtPersonnelNO.Text != "")
                {
                    //txtTFLCity.Text = "";
                    //txtTFuncTitle.Text = "";
                    //txtTISCord.Text = "";
                    //txtTType.Text = "";
                    //txtTRemarks.Text = "";
                    //dtpTEffDate.Text = "";
                    //dgvTSDC.DataSource = null;
                    //dgvTSDC.Refresh();
                    //pnlTransfer.Controls.Clear();
                    //pnlTSDC.Controls.Clear();

                }
            }
        }

        private void txtPersonnelNO_Leave(object sender, EventArgs e)
        {


            if (txtPersonnelNO.Text != "" && txtTypeOf.Text != "")
            {


                //this.objVals.Clear();
                //this.objVals.Add("PR_P_NO", txtPersonnelNO.Text);
                //rsltDptCnt = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "DPTCONT_GRID", objVals);

                //if (rsltDptCnt.isSuccessful)
                //{
                //    if (rsltDptCnt.dstResult.Tables.Count > 0)
                //    {
                //        dtDpt = rsltDptCnt.dstResult.Tables[0];
                //        this.dgvSDC.DataSource = dtDpt;
                //    }

                //}
                //else
                //{
                //    dgvSDC.DataSource = null;
                //    dgvSDC.Refresh();

                //}

                //Refreshing the TRANSFER fields//
                if ((txtOption.Text == "A" || txtOption.Text == "D") && txtPersonnelNO.Text != "")
                {
                    //txtTFLCity.Text = "";
                    //txtTFuncTitle.Text = "";
                    //txtTISCord.Text = "";
                    //txtTType.Text = "";
                    //txtTRemarks.Text = "";
                    //dtpTEffDate.Text = "";
                    //dgvTSDC.DataSource = null;
                    //dgvTSDC.Refresh();

                }

            }



        }

        private void lbpersonnel_Click(object sender, EventArgs e)
        {

            lbpersonnel.Focus();
        }

        //------------W_ANS == SAVE CASE----------//
        private void txtTRemarks_Leave(object sender, EventArgs e)
        {
        }

        private void dtpTEffDate_Leave(object sender, EventArgs e)
        {
            //if (dtpTEffDate.Value != null)
            //    dgvTSDC.Focus();
            //  dgvTSDC.BeginEdit(true);
        }

        private void txtPersonnelNO_Validating(object sender, CancelEventArgs e)
        {
            Pr_P_No_KeyNext();
        }

        private void txtTNewBranch_TextChanged(object sender, EventArgs e)
        {
            //if (txtOption.Text == "D" && txtCurrOption.Text == "DELETE")
            //{

            //    DialogResult dRes = MessageBox.Show("Do you want to Delete this record [Y/N]..", "Note"
            //                                      , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    if (dRes == DialogResult.Yes)
            //    {

            //        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            //        pnlTransfer.Focus();

            //        base.DoToolbarActions(this.Controls, "Delete");
            //        //this.Reset();
            //        //call save
            //        return;

            //    }
            //    else if (dRes == DialogResult.No)
            //    {
            //        // this.Reset();
            //        txtOption.Focus();

            //        return;
            //    }


            //}
        }

        private void txtTRemarks_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    e.IsInputKey = true;
                }
            }
        }

        private void txtTRemarks_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == '\t')
            {
                myCurrFunction = Function.None;
                BranchtTransType = string.Empty;
                if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add || this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit)
                {

                    myCurrFunction = FunctionConfig.CurrentOption;
                    BranchtTransType = txtTypeOf.Text.Trim();

                    DialogResult saveMsg = MessageBox.Show("Do You Want to Save the Changes [Y]es or [N]o ", "Note", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (saveMsg == DialogResult.Yes)
                    {
                        if (txtPersonnelNO.Text != "" && txtTNewBranch.Text != "" && dtpTEffDate.Text != "")
                        {
                            if (this.Validate())
                            {
                                Function current = FunctionConfig.CurrentOption;
                                if ((this.dgvTSDC.DataSource as DataTable) != null)
                                {
                                    if (dgvTSDC.Rows.Count > 1)
                                    {

                                        for (int i = 0; i < (this.dgvTSDC.DataSource as DataTable).Rows.Count; i++)
                                        {
                                            if (dtpTEffDate.Value != null)
                                                (this.dgvTSDC.DataSource as DataTable).Rows[i]["PR_EFFECTIVE"] = Convert.ToDateTime(dtpTEffDate.Value);
                                        }
                                    }
                                }
                                (this.pnlTransfer.CurrentBusinessEntity as iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BranchManagerTransferCommand).PR_TRANSFER_TYPE = TransferType;
                                base.DoToolbarActions(this.Controls, "Save");
                                DialogResult viewMsg = MessageBox.Show("Continue The Process For More Records Yes or No", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (viewMsg == DialogResult.Yes)
                                {

                                    //  this.ResetFields();
                                    SetFormInPreviousMode(current, txtTypeOf.Text);
                                    return;

                                }
                                else if (viewMsg == DialogResult.No)
                                {
                                    this.FunctionConfig.CurrentOption = Function.None;
                                    this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
                                    this.BindLOVLookupButton(lkbtnDesc);
                                    this.txtOption.Select();
                                    this.txtOption.Focus(); ;
                                    //this.Refresh();
                                    //txtOption.Focus();
                                    //return;
                                }
                                return;
                            }
                        }
                    }
                    else if (saveMsg == DialogResult.No)
                    {
                        //base.Cancel();// this.ResetFields();
                        SetFormInPreviousMode(myCurrFunction, BranchtTransType);
                        return;
                    }
                }

            }

        }

        private void txtTFuncTitle_Validated(object sender, EventArgs e)
        {
            if (txtTypeOf.Text == "Re-located".ToUpper() || txtTypeOf.Text == "Branch Managers".ToUpper())
            {
                txt_PR_RENT.Focus();
            }
        }

        private void txt_PR_TAX_ON_TAX_Validated(object sender, EventArgs e)
        {
            txtTISCord.Focus();
        }

        private void dgvTSDC_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }

        private void dgvTSDC_Validated(object sender, EventArgs e)
        {
            txtTRemarks.Focus();
        }

        private void dgvTSDC_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (dgvTSDC.Rows.Count > 1)
            //{
            //    dgvTSDC.BeginEdit(true);
            //}
        }
        #endregion

    }
}