using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_IncrementOfficers_Designation : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        public String desig;
        public String level;
        public String functit1;
        public String functit2;
        public string NewBranch;
        public string strCode;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtOption.Visible      = false;
            this.tbtAdd.Visible         = false;
            this.tbtSave.Visible        = false;
            this.tbtDelete.Visible      = false;
            this.tbtList.Visible        = false;
            this.tbtCancel.Visible      = false;
            this.tbtClose.Visible       = false;
            this.txtDesignation.Select();
            this.txtDesignation.Focus();
            this.stsOptions.Visible     = false;

            if (strCode == "Level")
            {
                label1.Visible              = false;
                txtDesignation.Visible      = false;
                lbtnLevel.ConditionalFields = string.Empty;
                LBDesig.Visible             = false;
                lbtnLevel.ActionLOVExists   = "LOV_LEVEL_L_Exists";
                lbtnLevel.ActionType        = "LOV_LEVEL_L";
            }
            else
            {
                label1.Visible              = true;
                txtDesignation.Visible      = true;
                lbtnLevel.ConditionalFields = "txtDesignation";
                LBDesig.Visible             = true;
                lbtnLevel.ActionLOVExists   = "LOV_LEVEL_Exists";
                lbtnLevel.ActionType        = "LOV_LEVEL";
            }

            txtDesignation.Text     = desig;
            txtLevel.Text           = level;
            txtfunctitle1.Text      = functit1;
            txtfunctitl2.Text       = functit2;
            this.ShowStatusBar      = false;
        }

        public CHRIS_Personnel_IncrementOfficers_Designation(string Code)
        {
            txtOption.Visible = false;
            InitializeComponent();
            //  txtDesignation.Text = desig;
            strCode = Code;

        }

        private void txtDesignation_Validating(object sender, CancelEventArgs e)
        {
            if (txtDesignation.Text != string.Empty)
            {
                Result rslt1;
                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("PR_DESIG_PRESENT", this.txtDesignation.Text);
                param1.Add("PR_LEVEL_PRESENT", this.txtLevel.Text);
                rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Lov_desig_afterLov", param1);
                if (rslt1.isSuccessful)
                {
                    if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 0)
                    {
                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                            txtLevel.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    }
                    else
                    {
                        MessageBox.Show("ERROR ! YOU ENTERED AN INVALID DESIGNATION.PRESS [F9] TO SEE VALID LIST", "Error", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                        this.txtDesignation.Text = string.Empty;
                        this.txtLevel.Text = string.Empty;
                    }
                }
            }
            // desig = txtDesignation.Text;
        }

        private void txtLevel_Validating(object sender, CancelEventArgs e)
        {
            // level = txtLevel.Text;
        }

        private void txtfunctitle1_Validating(object sender, CancelEventArgs e)
        {
            // functit1 = txtfunctitle1.Text;
        }

        private void txtfunctitl2_Validating(object sender, CancelEventArgs e)
        {
            //functit2 = txtfunctitl2.Text;

        }

        private void txtfunctitl2_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    e.IsInputKey = true;
                }
            }
        }

        private void txtfunctitl2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\t' || e.KeyChar == 13)
            {
                desig = txtDesignation.Text;
                level = txtLevel.Text;
                functit1 = txtfunctitle1.Text;
                functit2 = txtfunctitl2.Text;
                this.Close();
            }

        }




    }
}