namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_FastOrISTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_FastOrISTransfer));
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtJoiningDate = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.dtTerminDate = new CrplControlLibrary.SLTextBox(this.components);
            this.dtTransferDate = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnTransferType = new CrplControlLibrary.LookupButton(this.components);
            this.txtTransferType = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPRTransferTypeGlobal = new CrplControlLibrary.SLTextBox(this.components);
            this.prTransferGlobal = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_CLOSE_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.dtPRTransferDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtPR_TERMIN_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txtPR_Func2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtPR_Func1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPR_DESG = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtPRTransfer = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtCurrOption = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlDetail.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(616, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(652, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 458);
            this.panel1.Size = new System.Drawing.Size(652, 60);
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtJoiningDate);
            this.pnlDetail.Controls.Add(this.slTextBox1);
            this.pnlDetail.Controls.Add(this.dtTerminDate);
            this.pnlDetail.Controls.Add(this.dtTransferDate);
            this.pnlDetail.Controls.Add(this.lbtnTransferType);
            this.pnlDetail.Controls.Add(this.txtTransferType);
            this.pnlDetail.Controls.Add(this.txtLocation);
            this.pnlDetail.Controls.Add(this.txtUser);
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.Controls.Add(this.label2);
            this.pnlDetail.Controls.Add(this.label6);
            this.pnlDetail.Controls.Add(this.txtPRTransferTypeGlobal);
            this.pnlDetail.Controls.Add(this.prTransferGlobal);
            this.pnlDetail.Controls.Add(this.txtPR_CLOSE_FLAG);
            this.pnlDetail.Controls.Add(this.dtPRTransferDate);
            this.pnlDetail.Controls.Add(this.dtPR_TERMIN_DATE);
            this.pnlDetail.Controls.Add(this.txtLevel);
            this.pnlDetail.Controls.Add(this.label5);
            this.pnlDetail.Controls.Add(this.txtPR_Func2);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.txtPR_Func1);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.txtPR_DESG);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.txtPRTransfer);
            this.pnlDetail.Controls.Add(this.txtName);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.Controls.Add(this.panel2);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TransferISLOCALCommand";
            this.pnlDetail.Location = new System.Drawing.Point(12, 98);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(629, 316);
            this.pnlDetail.SPName = "CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER";
            this.pnlDetail.TabIndex = 13;
            this.pnlDetail.TabStop = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(34, 298);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(574, 13);
            this.label9.TabIndex = 146;
            this.label9.Text = "_________________________________________________________________________________" +
                "";
            // 
            // txtJoiningDate
            // 
            this.txtJoiningDate.AllowSpace = true;
            this.txtJoiningDate.AssociatedLookUpName = "";
            this.txtJoiningDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJoiningDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtJoiningDate.ContinuationTextBox = null;
            this.txtJoiningDate.CustomEnabled = true;
            this.txtJoiningDate.DataFieldMapping = "PR_JOINING_DATE";
            this.txtJoiningDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJoiningDate.GetRecordsOnUpDownKeys = false;
            this.txtJoiningDate.IsDate = false;
            this.txtJoiningDate.Location = new System.Drawing.Point(396, 247);
            this.txtJoiningDate.Name = "txtJoiningDate";
            this.txtJoiningDate.NumberFormat = "###,###,##0.00";
            this.txtJoiningDate.Postfix = "";
            this.txtJoiningDate.Prefix = "";
            this.txtJoiningDate.Size = new System.Drawing.Size(100, 20);
            this.txtJoiningDate.SkipValidation = false;
            this.txtJoiningDate.TabIndex = 145;
            this.txtJoiningDate.TextType = CrplControlLibrary.TextType.DateTime;
            this.txtJoiningDate.Visible = false;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "PRName";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(460, 119);
            this.slTextBox1.MaxLength = 20;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.ReadOnly = true;
            this.slTextBox1.Size = new System.Drawing.Size(107, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 144;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // dtTerminDate
            // 
            this.dtTerminDate.AllowSpace = true;
            this.dtTerminDate.AssociatedLookUpName = "";
            this.dtTerminDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dtTerminDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dtTerminDate.ContinuationTextBox = null;
            this.dtTerminDate.CustomEnabled = true;
            this.dtTerminDate.DataFieldMapping = "";
            this.dtTerminDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtTerminDate.GetRecordsOnUpDownKeys = false;
            this.dtTerminDate.IsDate = false;
            this.dtTerminDate.Location = new System.Drawing.Point(280, 247);
            this.dtTerminDate.Name = "dtTerminDate";
            this.dtTerminDate.NumberFormat = "###,###,##0.00";
            this.dtTerminDate.Postfix = "";
            this.dtTerminDate.Prefix = "";
            this.dtTerminDate.Size = new System.Drawing.Size(100, 20);
            this.dtTerminDate.SkipValidation = false;
            this.dtTerminDate.TabIndex = 143;
            this.dtTerminDate.TextType = CrplControlLibrary.TextType.DateTime;
            this.dtTerminDate.Visible = false;
            // 
            // dtTransferDate
            // 
            this.dtTransferDate.AllowSpace = true;
            this.dtTransferDate.AssociatedLookUpName = "";
            this.dtTransferDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dtTransferDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dtTransferDate.ContinuationTextBox = null;
            this.dtTransferDate.CustomEnabled = true;
            this.dtTransferDate.DataFieldMapping = "PR_TRANSFER_DATE";
            this.dtTransferDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtTransferDate.GetRecordsOnUpDownKeys = false;
            this.dtTransferDate.IsDate = true;
            this.dtTransferDate.Location = new System.Drawing.Point(280, 273);
            this.dtTransferDate.Name = "dtTransferDate";
            this.dtTransferDate.NumberFormat = "###,###,##0.00";
            this.dtTransferDate.Postfix = "";
            this.dtTransferDate.Prefix = "";
            this.dtTransferDate.Size = new System.Drawing.Size(87, 20);
            this.dtTransferDate.SkipValidation = false;
            this.dtTransferDate.TabIndex = 142;
            this.dtTransferDate.TextType = CrplControlLibrary.TextType.DateTime;
            this.dtTransferDate.Visible = false;
            // 
            // lbtnTransferType
            // 
            this.lbtnTransferType.ActionLOVExists = "";
            this.lbtnTransferType.ActionType = "TransferType_LOV";
            this.lbtnTransferType.ConditionalFields = "";
            this.lbtnTransferType.CustomEnabled = true;
            this.lbtnTransferType.DataFieldMapping = "";
            this.lbtnTransferType.DependentLovControls = "";
            this.lbtnTransferType.HiddenColumns = "";
            this.lbtnTransferType.Image = ((System.Drawing.Image)(resources.GetObject("lbtnTransferType.Image")));
            this.lbtnTransferType.LoadDependentEntities = false;
            this.lbtnTransferType.Location = new System.Drawing.Point(384, 17);
            this.lbtnTransferType.LookUpTitle = null;
            this.lbtnTransferType.Name = "lbtnTransferType";
            this.lbtnTransferType.Size = new System.Drawing.Size(26, 21);
            this.lbtnTransferType.SkipValidationOnLeave = true;
            this.lbtnTransferType.SPName = "CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER";
            this.lbtnTransferType.TabIndex = 140;
            this.lbtnTransferType.TabStop = false;
            this.lbtnTransferType.UseVisualStyleBackColor = true;
            // 
            // txtTransferType
            // 
            this.txtTransferType.AllowSpace = true;
            this.txtTransferType.AssociatedLookUpName = "lbtnTransferType";
            this.txtTransferType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTransferType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTransferType.ContinuationTextBox = null;
            this.txtTransferType.CustomEnabled = true;
            this.txtTransferType.DataFieldMapping = "W_TYPEOF";
            this.txtTransferType.Enabled = false;
            this.txtTransferType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransferType.GetRecordsOnUpDownKeys = false;
            this.txtTransferType.IsDate = false;
            this.txtTransferType.Location = new System.Drawing.Point(213, 18);
            this.txtTransferType.MaxLength = 6;
            this.txtTransferType.Name = "txtTransferType";
            this.txtTransferType.NumberFormat = "###,###,##0.00";
            this.txtTransferType.Postfix = "";
            this.txtTransferType.Prefix = "";
            this.txtTransferType.Size = new System.Drawing.Size(165, 20);
            this.txtTransferType.SkipValidation = true;
            this.txtTransferType.TabIndex = 141;
            this.txtTransferType.TabStop = false;
            this.txtTransferType.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtTransferType, "Press <F9> Key to Display The List");
            // 
            // txtLocation
            // 
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(77, 47);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.TabIndex = 134;
            this.txtLocation.TabStop = false;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(77, 18);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.TabIndex = 133;
            this.txtUser.TabStop = false;
            this.txtUser.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(5, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 132;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(4, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 131;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Visible = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(174, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(269, 18);
            this.label6.TabIndex = 139;
            this.label6.Text = "PERSONNEL SYSTEM /FAST OR IS";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPRTransferTypeGlobal
            // 
            this.txtPRTransferTypeGlobal.AllowSpace = true;
            this.txtPRTransferTypeGlobal.AssociatedLookUpName = "";
            this.txtPRTransferTypeGlobal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRTransferTypeGlobal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPRTransferTypeGlobal.ContinuationTextBox = null;
            this.txtPRTransferTypeGlobal.CustomEnabled = false;
            this.txtPRTransferTypeGlobal.DataFieldMapping = "";
            this.txtPRTransferTypeGlobal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRTransferTypeGlobal.GetRecordsOnUpDownKeys = false;
            this.txtPRTransferTypeGlobal.IsDate = false;
            this.txtPRTransferTypeGlobal.Location = new System.Drawing.Point(471, 144);
            this.txtPRTransferTypeGlobal.MaxLength = 3;
            this.txtPRTransferTypeGlobal.Name = "txtPRTransferTypeGlobal";
            this.txtPRTransferTypeGlobal.NumberFormat = "###,###,##0.00";
            this.txtPRTransferTypeGlobal.Postfix = "";
            this.txtPRTransferTypeGlobal.Prefix = "";
            this.txtPRTransferTypeGlobal.Size = new System.Drawing.Size(25, 20);
            this.txtPRTransferTypeGlobal.SkipValidation = false;
            this.txtPRTransferTypeGlobal.TabIndex = 130;
            this.txtPRTransferTypeGlobal.TabStop = false;
            this.txtPRTransferTypeGlobal.TextType = CrplControlLibrary.TextType.String;
            this.txtPRTransferTypeGlobal.Visible = false;
            // 
            // prTransferGlobal
            // 
            this.prTransferGlobal.AllowSpace = true;
            this.prTransferGlobal.AssociatedLookUpName = "";
            this.prTransferGlobal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.prTransferGlobal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.prTransferGlobal.ContinuationTextBox = null;
            this.prTransferGlobal.CustomEnabled = false;
            this.prTransferGlobal.DataFieldMapping = "";
            this.prTransferGlobal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prTransferGlobal.GetRecordsOnUpDownKeys = false;
            this.prTransferGlobal.IsDate = false;
            this.prTransferGlobal.Location = new System.Drawing.Point(434, 144);
            this.prTransferGlobal.MaxLength = 3;
            this.prTransferGlobal.Name = "prTransferGlobal";
            this.prTransferGlobal.NumberFormat = "###,###,##0.00";
            this.prTransferGlobal.Postfix = "";
            this.prTransferGlobal.Prefix = "";
            this.prTransferGlobal.Size = new System.Drawing.Size(25, 20);
            this.prTransferGlobal.SkipValidation = false;
            this.prTransferGlobal.TabIndex = 129;
            this.prTransferGlobal.TabStop = false;
            this.prTransferGlobal.TextType = CrplControlLibrary.TextType.String;
            this.prTransferGlobal.Visible = false;
            // 
            // txtPR_CLOSE_FLAG
            // 
            this.txtPR_CLOSE_FLAG.AllowSpace = true;
            this.txtPR_CLOSE_FLAG.AssociatedLookUpName = "";
            this.txtPR_CLOSE_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_CLOSE_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_CLOSE_FLAG.ContinuationTextBox = null;
            this.txtPR_CLOSE_FLAG.CustomEnabled = false;
            this.txtPR_CLOSE_FLAG.DataFieldMapping = "";
            this.txtPR_CLOSE_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_CLOSE_FLAG.GetRecordsOnUpDownKeys = false;
            this.txtPR_CLOSE_FLAG.IsDate = false;
            this.txtPR_CLOSE_FLAG.Location = new System.Drawing.Point(399, 175);
            this.txtPR_CLOSE_FLAG.MaxLength = 3;
            this.txtPR_CLOSE_FLAG.Name = "txtPR_CLOSE_FLAG";
            this.txtPR_CLOSE_FLAG.NumberFormat = "###,###,##0.00";
            this.txtPR_CLOSE_FLAG.Postfix = "";
            this.txtPR_CLOSE_FLAG.Prefix = "";
            this.txtPR_CLOSE_FLAG.Size = new System.Drawing.Size(29, 20);
            this.txtPR_CLOSE_FLAG.SkipValidation = false;
            this.txtPR_CLOSE_FLAG.TabIndex = 128;
            this.txtPR_CLOSE_FLAG.TabStop = false;
            this.txtPR_CLOSE_FLAG.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_CLOSE_FLAG.Visible = false;
            // 
            // dtPRTransferDate
            // 
            this.dtPRTransferDate.CustomEnabled = true;
            this.dtPRTransferDate.CustomFormat = "dd/MM/yyyy";
            this.dtPRTransferDate.DataFieldMapping = "PR_TRANSFER_DATE";
            this.dtPRTransferDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPRTransferDate.HasChanges = true;
            this.dtPRTransferDate.Location = new System.Drawing.Point(296, 197);
            this.dtPRTransferDate.Name = "dtPRTransferDate";
            this.dtPRTransferDate.NullValue = " ";
            this.dtPRTransferDate.Size = new System.Drawing.Size(73, 20);
            this.dtPRTransferDate.TabIndex = 127;
            this.dtPRTransferDate.TabStop = false;
            this.dtPRTransferDate.Value = new System.DateTime(2011, 1, 17, 0, 0, 0, 0);
            this.dtPRTransferDate.Visible = false;
            // 
            // dtPR_TERMIN_DATE
            // 
            this.dtPR_TERMIN_DATE.CustomEnabled = true;
            this.dtPR_TERMIN_DATE.CustomFormat = "dd/MM/yyyy";
            this.dtPR_TERMIN_DATE.DataFieldMapping = "PR_TERMIN_DATE";
            this.dtPR_TERMIN_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPR_TERMIN_DATE.HasChanges = true;
            this.dtPR_TERMIN_DATE.Location = new System.Drawing.Point(296, 173);
            this.dtPR_TERMIN_DATE.Name = "dtPR_TERMIN_DATE";
            this.dtPR_TERMIN_DATE.NullValue = " ";
            this.dtPR_TERMIN_DATE.Size = new System.Drawing.Size(97, 20);
            this.dtPR_TERMIN_DATE.TabIndex = 126;
            this.dtPR_TERMIN_DATE.TabStop = false;
            this.dtPR_TERMIN_DATE.Value = new System.DateTime(2011, 1, 17, 0, 0, 0, 0);
            this.dtPR_TERMIN_DATE.Visible = false;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(341, 144);
            this.txtLevel.MaxLength = 3;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(87, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 110;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(277, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 20);
            this.label5.TabIndex = 109;
            this.label5.Text = "4.Level:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_Func2
            // 
            this.txtPR_Func2.AllowSpace = true;
            this.txtPR_Func2.AssociatedLookUpName = "";
            this.txtPR_Func2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_Func2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_Func2.ContinuationTextBox = null;
            this.txtPR_Func2.CustomEnabled = true;
            this.txtPR_Func2.DataFieldMapping = "";
            this.txtPR_Func2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_Func2.GetRecordsOnUpDownKeys = false;
            this.txtPR_Func2.IsDate = false;
            this.txtPR_Func2.Location = new System.Drawing.Point(154, 199);
            this.txtPR_Func2.MaxLength = 30;
            this.txtPR_Func2.Name = "txtPR_Func2";
            this.txtPR_Func2.NumberFormat = "###,###,##0.00";
            this.txtPR_Func2.Postfix = "";
            this.txtPR_Func2.Prefix = "";
            this.txtPR_Func2.ReadOnly = true;
            this.txtPR_Func2.Size = new System.Drawing.Size(114, 20);
            this.txtPR_Func2.SkipValidation = false;
            this.txtPR_Func2.TabIndex = 108;
            this.txtPR_Func2.TabStop = false;
            this.txtPR_Func2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_Func2, "[M]arried or [S]ingle");
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(15, 197);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 20);
            this.label7.TabIndex = 107;
            this.label7.Text = "Title :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_Func1
            // 
            this.txtPR_Func1.AllowSpace = true;
            this.txtPR_Func1.AssociatedLookUpName = "";
            this.txtPR_Func1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_Func1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_Func1.ContinuationTextBox = null;
            this.txtPR_Func1.CustomEnabled = true;
            this.txtPR_Func1.DataFieldMapping = "";
            this.txtPR_Func1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_Func1.GetRecordsOnUpDownKeys = false;
            this.txtPR_Func1.IsDate = false;
            this.txtPR_Func1.Location = new System.Drawing.Point(154, 173);
            this.txtPR_Func1.MaxLength = 30;
            this.txtPR_Func1.Name = "txtPR_Func1";
            this.txtPR_Func1.NumberFormat = "###,###,##0.00";
            this.txtPR_Func1.Postfix = "";
            this.txtPR_Func1.Prefix = "";
            this.txtPR_Func1.ReadOnly = true;
            this.txtPR_Func1.Size = new System.Drawing.Size(114, 20);
            this.txtPR_Func1.SkipValidation = false;
            this.txtPR_Func1.TabIndex = 88;
            this.txtPR_Func1.TabStop = false;
            this.txtPR_Func1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_Func1, "[M]arried or [S]ingle");
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(13, 171);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(137, 20);
            this.label14.TabIndex = 87;
            this.label14.Text = "5.Functional :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(14, 225);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 20);
            this.label12.TabIndex = 86;
            this.label12.Text = "6. Transfer Type :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_DESG
            // 
            this.txtPR_DESG.AllowSpace = true;
            this.txtPR_DESG.AssociatedLookUpName = "lbtnPNo";
            this.txtPR_DESG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESG.ContinuationTextBox = null;
            this.txtPR_DESG.CustomEnabled = true;
            this.txtPR_DESG.DataFieldMapping = "";
            this.txtPR_DESG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESG.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESG.IsDate = false;
            this.txtPR_DESG.Location = new System.Drawing.Point(154, 144);
            this.txtPR_DESG.MaxLength = 10;
            this.txtPR_DESG.Name = "txtPR_DESG";
            this.txtPR_DESG.NumberFormat = "###,###,##0.00";
            this.txtPR_DESG.Postfix = "";
            this.txtPR_DESG.Prefix = "";
            this.txtPR_DESG.ReadOnly = true;
            this.txtPR_DESG.Size = new System.Drawing.Size(87, 20);
            this.txtPR_DESG.SkipValidation = false;
            this.txtPR_DESG.TabIndex = 80;
            this.txtPR_DESG.TabStop = false;
            this.txtPR_DESG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(31, 144);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 20);
            this.label13.TabIndex = 82;
            this.label13.Text = "3. Designation :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRTransfer
            // 
            this.txtPRTransfer.AllowSpace = true;
            this.txtPRTransfer.AssociatedLookUpName = "";
            this.txtPRTransfer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRTransfer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPRTransfer.ContinuationTextBox = null;
            this.txtPRTransfer.CustomEnabled = true;
            this.txtPRTransfer.DataFieldMapping = "";
            this.txtPRTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRTransfer.GetRecordsOnUpDownKeys = false;
            this.txtPRTransfer.IsDate = false;
            this.txtPRTransfer.Location = new System.Drawing.Point(154, 225);
            this.txtPRTransfer.MaxLength = 1;
            this.txtPRTransfer.Name = "txtPRTransfer";
            this.txtPRTransfer.NumberFormat = "###,###,##0.00";
            this.txtPRTransfer.Postfix = "";
            this.txtPRTransfer.Prefix = "";
            this.txtPRTransfer.ReadOnly = true;
            this.txtPRTransfer.Size = new System.Drawing.Size(39, 20);
            this.txtPRTransfer.SkipValidation = false;
            this.txtPRTransfer.TabIndex = 68;
            this.txtPRTransfer.TabStop = false;
            this.txtPRTransfer.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "PRName1";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(352, 119);
            this.txtName.MaxLength = 20;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(107, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 2;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.errorProvider1.SetIconAlignment(this.label8, System.Windows.Forms.ErrorIconAlignment.TopLeft);
            this.label8.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label8.Location = new System.Drawing.Point(252, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 20);
            this.label8.TabIndex = 26;
            this.label8.Text = "2. First Name:";
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "";
            this.lbtnPNo.ActionType = "PR_P_ADD_LOV";
            this.lbtnPNo.ConditionalFields = "txtTransferType";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = false;
            this.lbtnPNo.Location = new System.Drawing.Point(222, 118);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = true;
            this.lbtnPNo.SPName = "CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_TR_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.Location = new System.Drawing.Point(154, 118);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(59, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 1;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.Leave += new System.EventHandler(this.txtPersNo_Leave);
            this.txtPersNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersNo_Validating);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(30, 118);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 20);
            this.label11.TabIndex = 23;
            this.label11.Text = "1. Pr No. :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(154, 118);
            this.txtID.MaxLength = 30;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(36, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 67;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtCurrOption);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtDate);
            this.panel2.Location = new System.Drawing.Point(449, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(167, 59);
            this.panel2.TabIndex = 139;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.Location = new System.Drawing.Point(66, 6);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.TabIndex = 137;
            this.txtCurrOption.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(20, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 136;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(15, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 135;
            this.label4.Text = "Option:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDate
            // 
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(66, 32);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.TabIndex = 138;
            this.txtDate.TabStop = false;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(443, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(133, 13);
            this.txtUserName.TabIndex = 119;
            this.txtUserName.Text = "User  Name :   CHRISTOP";
            // 
            // CHRIS_Personnel_FastOrISTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(652, 518);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.txtUserName);
            this.CurrentPanelBlock = "pnlDetail";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Personnel_FastOrISTransfer";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Personnel_FastOrISTransfer";
            this.Shown += new System.EventHandler(this.CHRIS_Personnel_FastOrISTransfer_Shown);
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_FastOrISTransfer_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txtPR_Func2;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtPR_Func1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtPR_DESG;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtPRTransfer;
        private CrplControlLibrary.SLTextBox txtName;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtID;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox prTransferGlobal;
        private CrplControlLibrary.SLTextBox txtPR_CLOSE_FLAG;
        private CrplControlLibrary.LookupButton lbtnTransferType;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtCurrOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        public iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        public CrplControlLibrary.SLTextBox txtPersNo;
        public CrplControlLibrary.SLTextBox txtLevel;
        public CrplControlLibrary.SLDatePicker dtPRTransferDate;
        public CrplControlLibrary.SLDatePicker dtPR_TERMIN_DATE;
        public CrplControlLibrary.SLTextBox txtPRTransferTypeGlobal;
        public CrplControlLibrary.SLTextBox txtTransferType;
        private System.Windows.Forms.Label txtUserName;
        private CrplControlLibrary.SLTextBox dtTransferDate;
        private CrplControlLibrary.SLTextBox dtTerminDate;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.Panel panel2;
        public CrplControlLibrary.SLTextBox txtJoiningDate;
        private System.Windows.Forms.Label label9;
    }
}