namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_SumInernsHiring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_SumInernsHiring));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtCurrOption = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAddress = new CrplControlLibrary.SLTextBox(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dtpDOB = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtPh1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPh2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMarital = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSex = new CrplControlLibrary.SLTextBox(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.txtNID = new CrplControlLibrary.SLTextBox(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.txtStipend = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRecommend = new System.Windows.Forms.Label();
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.txtPrevIntern = new CrplControlLibrary.SLTextBox(this.components);
            this.dtpToDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtLastName = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbtnBranch = new CrplControlLibrary.LookupButton(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtClass = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.dtpFromDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtUni = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlDept = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvDept = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colSeg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDept = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.colContribution = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.pnlDept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDept)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(638, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(674, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 563);
            this.panel1.Size = new System.Drawing.Size(674, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Location = new System.Drawing.Point(12, 39);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(650, 91);
            this.pnlHead.TabIndex = 0;
            // 
            // txtDate
            // 
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(560, 66);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.Location = new System.Drawing.Point(560, 40);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(517, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(509, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "Option:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(79, 66);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            // 
            // txtUser
            // 
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(79, 40);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(165, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(389, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "Personnel   System";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(165, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(389, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "S U M M E R   I N T E R N S    E N T R Y";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.label18);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.txtAddress);
            this.pnlDetail.Controls.Add(this.label27);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.dtpDOB);
            this.pnlDetail.Controls.Add(this.txtPh1);
            this.pnlDetail.Controls.Add(this.txtPh2);
            this.pnlDetail.Controls.Add(this.label19);
            this.pnlDetail.Controls.Add(this.label17);
            this.pnlDetail.Controls.Add(this.txtMarital);
            this.pnlDetail.Controls.Add(this.txtSex);
            this.pnlDetail.Controls.Add(this.label23);
            this.pnlDetail.Controls.Add(this.txtNID);
            this.pnlDetail.Controls.Add(this.label20);
            this.pnlDetail.Controls.Add(this.txtStipend);
            this.pnlDetail.Controls.Add(this.txtRecommend);
            this.pnlDetail.Controls.Add(this.slTextBox2);
            this.pnlDetail.Controls.Add(this.label16);
            this.pnlDetail.Controls.Add(this.txtPrevIntern);
            this.pnlDetail.Controls.Add(this.dtpToDate);
            this.pnlDetail.Controls.Add(this.txtLastName);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.lbtnBranch);
            this.pnlDetail.Controls.Add(this.txtBranch);
            this.pnlDetail.Controls.Add(this.txtClass);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.dtpFromDate);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.txtUni);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtFirstName);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.Controls.Add(this.panel2);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.NonIsolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SummerInternsCommand";
            this.pnlDetail.Location = new System.Drawing.Point(12, 133);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(650, 424);
            this.pnlDetail.SPName = "CHRIS_SP_SUMMER_INTERNS_MANAGER";
            this.pnlDetail.TabIndex = 1;
            this.pnlDetail.TabStop = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(260, 208);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 13);
            this.label18.TabIndex = 11;
            this.label18.Text = "(With CitiBank)";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(65, 295);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(140, 20);
            this.label11.TabIndex = 81;
            this.label11.Text = "Address:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAddress
            // 
            this.txtAddress.AllowSpace = true;
            this.txtAddress.AssociatedLookUpName = "";
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAddress.ContinuationTextBox = null;
            this.txtAddress.CustomEnabled = true;
            this.txtAddress.DataFieldMapping = "PR_ADDRESS";
            this.txtAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress.GetRecordsOnUpDownKeys = false;
            this.txtAddress.IsDate = false;
            this.txtAddress.Location = new System.Drawing.Point(213, 295);
            this.txtAddress.MaxLength = 50;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.NumberFormat = "###,###,##0.00";
            this.txtAddress.Postfix = "";
            this.txtAddress.Prefix = "";
            this.txtAddress.Size = new System.Drawing.Size(149, 20);
            this.txtAddress.SkipValidation = false;
            this.txtAddress.TabIndex = 11;
            this.txtAddress.TextType = CrplControlLibrary.TextType.String;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(445, 347);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(35, 20);
            this.label27.TabIndex = 85;
            this.label27.Text = "Sex:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(66, 321);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(140, 20);
            this.label15.TabIndex = 82;
            this.label15.Text = "Phone:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpDOB
            // 
            this.dtpDOB.CustomEnabled = true;
            this.dtpDOB.CustomFormat = "dd/MM/yyyy";
            this.dtpDOB.DataFieldMapping = "PR_DATE_BIRTH";
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOB.HasChanges = true;
            this.dtpDOB.IsRequired = true;
            this.dtpDOB.Location = new System.Drawing.Point(213, 347);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.NullValue = " ";
            this.dtpDOB.Size = new System.Drawing.Size(88, 20);
            this.dtpDOB.TabIndex = 14;
            this.dtpDOB.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpDOB.Validating += new System.ComponentModel.CancelEventHandler(this.dtpDOB_Validating);
            // 
            // txtPh1
            // 
            this.txtPh1.AllowSpace = true;
            this.txtPh1.AssociatedLookUpName = "";
            this.txtPh1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPh1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPh1.ContinuationTextBox = null;
            this.txtPh1.CustomEnabled = true;
            this.txtPh1.DataFieldMapping = "PR_PHONE1";
            this.txtPh1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPh1.GetRecordsOnUpDownKeys = false;
            this.txtPh1.IsDate = false;
            this.txtPh1.Location = new System.Drawing.Point(213, 321);
            this.txtPh1.MaxLength = 8;
            this.txtPh1.Name = "txtPh1";
            this.txtPh1.NumberFormat = "###,###,##0.00";
            this.txtPh1.Postfix = "";
            this.txtPh1.Prefix = "";
            this.txtPh1.Size = new System.Drawing.Size(76, 20);
            this.txtPh1.SkipValidation = false;
            this.txtPh1.TabIndex = 12;
            this.txtPh1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPh2
            // 
            this.txtPh2.AllowSpace = true;
            this.txtPh2.AssociatedLookUpName = "";
            this.txtPh2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPh2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPh2.ContinuationTextBox = null;
            this.txtPh2.CustomEnabled = true;
            this.txtPh2.DataFieldMapping = "PR_PHONE2";
            this.txtPh2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPh2.GetRecordsOnUpDownKeys = false;
            this.txtPh2.IsDate = false;
            this.txtPh2.Location = new System.Drawing.Point(295, 321);
            this.txtPh2.MaxLength = 8;
            this.txtPh2.Name = "txtPh2";
            this.txtPh2.NumberFormat = "###,###,##0.00";
            this.txtPh2.Postfix = "";
            this.txtPh2.Prefix = "";
            this.txtPh2.Size = new System.Drawing.Size(76, 20);
            this.txtPh2.SkipValidation = false;
            this.txtPh2.TabIndex = 13;
            this.txtPh2.TextType = CrplControlLibrary.TextType.String;
            this.txtPh2.Validating += new System.ComponentModel.CancelEventHandler(this.txtPh2_Validating);
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(65, 347);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(140, 20);
            this.label19.TabIndex = 83;
            this.label19.Text = "Date Of Birth:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(305, 347);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 20);
            this.label17.TabIndex = 84;
            this.label17.Text = "Marital Status:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMarital
            // 
            this.txtMarital.AllowSpace = true;
            this.txtMarital.AssociatedLookUpName = "";
            this.txtMarital.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarital.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarital.ContinuationTextBox = null;
            this.txtMarital.CustomEnabled = true;
            this.txtMarital.DataFieldMapping = "PR_MAR_STATUS";
            this.txtMarital.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarital.GetRecordsOnUpDownKeys = false;
            this.txtMarital.IsDate = false;
            this.txtMarital.IsRequired = true;
            this.txtMarital.Location = new System.Drawing.Point(419, 347);
            this.txtMarital.MaxLength = 1;
            this.txtMarital.Name = "txtMarital";
            this.txtMarital.NumberFormat = "###,###,##0.00";
            this.txtMarital.Postfix = "";
            this.txtMarital.Prefix = "";
            this.txtMarital.Size = new System.Drawing.Size(20, 20);
            this.txtMarital.SkipValidation = false;
            this.txtMarital.TabIndex = 15;
            this.txtMarital.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtMarital, "[M]arried or [S]ingle");
            this.txtMarital.Validating += new System.ComponentModel.CancelEventHandler(this.txtMarital_Validating);
            // 
            // txtSex
            // 
            this.txtSex.AllowSpace = true;
            this.txtSex.AssociatedLookUpName = "";
            this.txtSex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSex.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSex.ContinuationTextBox = null;
            this.txtSex.CustomEnabled = true;
            this.txtSex.DataFieldMapping = "PR_SEX";
            this.txtSex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSex.GetRecordsOnUpDownKeys = false;
            this.txtSex.IsDate = false;
            this.txtSex.IsRequired = true;
            this.txtSex.Location = new System.Drawing.Point(487, 347);
            this.txtSex.MaxLength = 1;
            this.txtSex.Name = "txtSex";
            this.txtSex.NumberFormat = "###,###,##0.00";
            this.txtSex.Postfix = "";
            this.txtSex.Prefix = "";
            this.txtSex.Size = new System.Drawing.Size(20, 20);
            this.txtSex.SkipValidation = false;
            this.txtSex.TabIndex = 16;
            this.txtSex.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSex, "[M]ale or [F]emale");
            this.txtSex.Validating += new System.ComponentModel.CancelEventHandler(this.txtSex_Validating);
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(66, 373);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(140, 20);
            this.label23.TabIndex = 86;
            this.label23.Text = "N.I.D Card No.:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNID
            // 
            this.txtNID.AllowSpace = true;
            this.txtNID.AssociatedLookUpName = "";
            this.txtNID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNID.ContinuationTextBox = null;
            this.txtNID.CustomEnabled = true;
            this.txtNID.DataFieldMapping = "PR_NID_CARD";
            this.txtNID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNID.GetRecordsOnUpDownKeys = false;
            this.txtNID.IsDate = false;
            this.txtNID.Location = new System.Drawing.Point(213, 373);
            this.txtNID.MaxLength = 13;
            this.txtNID.Name = "txtNID";
            this.txtNID.NumberFormat = "###,###,##0.00";
            this.txtNID.Postfix = "";
            this.txtNID.Prefix = "";
            this.txtNID.Size = new System.Drawing.Size(136, 20);
            this.txtNID.SkipValidation = false;
            this.txtNID.TabIndex = 17;
            this.txtNID.TextType = CrplControlLibrary.TextType.DigitAndHyphen;
            this.toolTip1.SetToolTip(this.txtNID, "Format of NID Card is 999-99-999999");
            this.txtNID.Validating += new System.ComponentModel.CancelEventHandler(this.txtNID_Validating);
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(24, 255);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(183, 20);
            this.label20.TabIndex = 73;
            this.label20.Text = "Stipend:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtStipend
            // 
            this.txtStipend.AllowSpace = true;
            this.txtStipend.AssociatedLookUpName = "lbtnDesg";
            this.txtStipend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStipend.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStipend.ContinuationTextBox = null;
            this.txtStipend.CustomEnabled = true;
            this.txtStipend.DataFieldMapping = "PR_STIPENED";
            this.txtStipend.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStipend.GetRecordsOnUpDownKeys = false;
            this.txtStipend.IsDate = false;
            this.txtStipend.IsRequired = true;
            this.txtStipend.Location = new System.Drawing.Point(213, 255);
            this.txtStipend.MaxLength = 4;
            this.txtStipend.Name = "txtStipend";
            this.txtStipend.NumberFormat = "###,###,##0.00";
            this.txtStipend.Postfix = "";
            this.txtStipend.Prefix = "";
            this.txtStipend.Size = new System.Drawing.Size(75, 20);
            this.txtStipend.SkipValidation = false;
            this.txtStipend.TabIndex = 10;
            this.txtStipend.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStipend.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtRecommend
            // 
            this.txtRecommend.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.txtRecommend.Location = new System.Drawing.Point(24, 229);
            this.txtRecommend.Name = "txtRecommend";
            this.txtRecommend.Size = new System.Drawing.Size(183, 20);
            this.txtRecommend.TabIndex = 71;
            this.txtRecommend.Text = "Recommendation To Hire:";
            this.txtRecommend.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "lbtnDesg";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "PR_RECOM";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(213, 229);
            this.slTextBox2.MaxLength = 20;
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.Size = new System.Drawing.Size(75, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 9;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(68, 204);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(139, 20);
            this.label16.TabIndex = 69;
            this.label16.Text = "Previous Internship:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPrevIntern
            // 
            this.txtPrevIntern.AllowSpace = true;
            this.txtPrevIntern.AssociatedLookUpName = "";
            this.txtPrevIntern.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrevIntern.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrevIntern.ContinuationTextBox = null;
            this.txtPrevIntern.CustomEnabled = true;
            this.txtPrevIntern.DataFieldMapping = "PR_INTERNSHIP";
            this.txtPrevIntern.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrevIntern.GetRecordsOnUpDownKeys = false;
            this.txtPrevIntern.IsDate = false;
            this.txtPrevIntern.IsRequired = true;
            this.txtPrevIntern.Location = new System.Drawing.Point(213, 204);
            this.txtPrevIntern.MaxLength = 2;
            this.txtPrevIntern.Name = "txtPrevIntern";
            this.txtPrevIntern.NumberFormat = "###,###,##0.00";
            this.txtPrevIntern.Postfix = "";
            this.txtPrevIntern.Prefix = "";
            this.txtPrevIntern.Size = new System.Drawing.Size(20, 20);
            this.txtPrevIntern.SkipValidation = false;
            this.txtPrevIntern.TabIndex = 8;
            this.txtPrevIntern.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrevIntern.TextType = CrplControlLibrary.TextType.Double;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomEnabled = true;
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.DataFieldMapping = "PR_CON_TO";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.HasChanges = true;
            this.dtpToDate.IsRequired = true;
            this.dtpToDate.Location = new System.Drawing.Point(307, 179);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.NullValue = " ";
            this.dtpToDate.Size = new System.Drawing.Size(88, 20);
            this.dtpToDate.TabIndex = 7;
            this.dtpToDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpToDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtpToDate_Validating);
            // 
            // txtLastName
            // 
            this.txtLastName.AllowSpace = true;
            this.txtLastName.AssociatedLookUpName = "";
            this.txtLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastName.ContinuationTextBox = null;
            this.txtLastName.CustomEnabled = true;
            this.txtLastName.DataFieldMapping = "PR_LAST_NAME";
            this.txtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastName.GetRecordsOnUpDownKeys = false;
            this.txtLastName.IsDate = false;
            this.txtLastName.Location = new System.Drawing.Point(213, 103);
            this.txtLastName.MaxLength = 20;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.NumberFormat = "###,###,##0.00";
            this.txtLastName.Postfix = "";
            this.txtLastName.Prefix = "";
            this.txtLastName.Size = new System.Drawing.Size(149, 20);
            this.txtLastName.SkipValidation = false;
            this.txtLastName.TabIndex = 3;
            this.txtLastName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(67, 103);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(140, 20);
            this.label14.TabIndex = 60;
            this.label14.Text = "Last Name:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(67, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(140, 20);
            this.label13.TabIndex = 58;
            this.label13.Text = "Branch:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbtnBranch
            // 
            this.lbtnBranch.ActionLOVExists = "BRANCH_LOV_EXISTS";
            this.lbtnBranch.ActionType = "BRANCH_LOV";
            this.lbtnBranch.ConditionalFields = "";
            this.lbtnBranch.CustomEnabled = true;
            this.lbtnBranch.DataFieldMapping = "";
            this.lbtnBranch.DependentLovControls = "";
            this.lbtnBranch.HiddenColumns = "";
            this.lbtnBranch.Image = ((System.Drawing.Image)(resources.GetObject("lbtnBranch.Image")));
            this.lbtnBranch.LoadDependentEntities = false;
            this.lbtnBranch.Location = new System.Drawing.Point(295, 51);
            this.lbtnBranch.LookUpTitle = null;
            this.lbtnBranch.Name = "lbtnBranch";
            this.lbtnBranch.Size = new System.Drawing.Size(26, 21);
            this.lbtnBranch.SkipValidationOnLeave = false;
            this.lbtnBranch.SPName = "CHRIS_SP_SUMMER_INTERNS_MANAGER";
            this.lbtnBranch.TabIndex = 57;
            this.lbtnBranch.TabStop = false;
            this.lbtnBranch.UseVisualStyleBackColor = true;
            this.lbtnBranch.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtn_MouseDown);
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "lbtnBranch";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "PR_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.IsRequired = true;
            this.txtBranch.Location = new System.Drawing.Point(213, 51);
            this.txtBranch.MaxLength = 3;
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.Size = new System.Drawing.Size(76, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 1;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtBranch, "Press <F9> Key to Display The List");
            // 
            // txtClass
            // 
            this.txtClass.AllowSpace = true;
            this.txtClass.AssociatedLookUpName = "lbtnContractor";
            this.txtClass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClass.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtClass.ContinuationTextBox = null;
            this.txtClass.CustomEnabled = true;
            this.txtClass.DataFieldMapping = "PR_CLASS";
            this.txtClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClass.GetRecordsOnUpDownKeys = false;
            this.txtClass.IsDate = false;
            this.txtClass.Location = new System.Drawing.Point(213, 153);
            this.txtClass.MaxLength = 20;
            this.txtClass.Name = "txtClass";
            this.txtClass.NumberFormat = "###,###,##0.00";
            this.txtClass.Postfix = "";
            this.txtClass.Prefix = "";
            this.txtClass.Size = new System.Drawing.Size(116, 20);
            this.txtClass.SkipValidation = false;
            this.txtClass.TabIndex = 5;
            this.txtClass.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(67, 153);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(140, 20);
            this.label12.TabIndex = 34;
            this.label12.Text = "Class:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomEnabled = true;
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.DataFieldMapping = "PR_CON_FROM";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.HasChanges = true;
            this.dtpFromDate.IsRequired = true;
            this.dtpFromDate.Location = new System.Drawing.Point(213, 179);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.NullValue = " ";
            this.dtpFromDate.Size = new System.Drawing.Size(88, 20);
            this.dtpFromDate.TabIndex = 6;
            this.dtpFromDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(67, 179);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(140, 20);
            this.label10.TabIndex = 30;
            this.label10.Text = "From / To:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUni
            // 
            this.txtUni.AllowSpace = true;
            this.txtUni.AssociatedLookUpName = "lbtnDesg";
            this.txtUni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUni.ContinuationTextBox = null;
            this.txtUni.CustomEnabled = true;
            this.txtUni.DataFieldMapping = "PR_UNIVERSITY";
            this.txtUni.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUni.GetRecordsOnUpDownKeys = false;
            this.txtUni.IsDate = false;
            this.txtUni.Location = new System.Drawing.Point(213, 128);
            this.txtUni.MaxLength = 20;
            this.txtUni.Name = "txtUni";
            this.txtUni.NumberFormat = "###,###,##0.00";
            this.txtUni.Postfix = "";
            this.txtUni.Prefix = "";
            this.txtUni.Size = new System.Drawing.Size(149, 20);
            this.txtUni.SkipValidation = false;
            this.txtUni.TabIndex = 4;
            this.txtUni.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(67, 128);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 20);
            this.label9.TabIndex = 28;
            this.label9.Text = "University:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.IsRequired = true;
            this.txtFirstName.Location = new System.Drawing.Point(213, 77);
            this.txtFirstName.MaxLength = 20;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.Size = new System.Drawing.Size(149, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 2;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(67, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 20);
            this.label8.TabIndex = 26;
            this.label8.Text = "First Name:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "PR_P_NO_LOV_EXISTS";
            this.lbtnPNo.ActionType = "PR_P_NO_LOV";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = true;
            this.lbtnPNo.Location = new System.Drawing.Point(336, 25);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_SUMMER_INTERNS_MANAGER";
            this.lbtnPNo.TabIndex = 2;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            this.lbtnPNo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtn_MouseDown);
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.IsRequired = true;
            this.txtPersNo.Location = new System.Drawing.Point(213, 25);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(117, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 0;
            this.txtPersNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.Enter += new System.EventHandler(this.txtPersNo_Enter);
            this.txtPersNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersNo_Validating);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(67, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 20);
            this.label7.TabIndex = 23;
            this.label7.Text = "Personnel No.:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(213, 25);
            this.txtID.MaxLength = 30;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(36, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 0;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(6, 284);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(634, 123);
            this.panel2.TabIndex = 87;
            // 
            // pnlDept
            // 
            this.pnlDept.ConcurrentPanels = null;
            this.pnlDept.Controls.Add(this.dgvDept);
            this.pnlDept.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDept.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.NonIsolated;
            this.pnlDept.DependentPanels = null;
            this.pnlDept.DisableDependentLoad = false;
            this.pnlDept.EnableDelete = true;
            this.pnlDept.EnableInsert = true;
            this.pnlDept.EnableQuery = false;
            this.pnlDept.EnableUpdate = true;
            this.pnlDept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand";
            this.pnlDept.Location = new System.Drawing.Point(426, 183);
            this.pnlDept.MasterPanel = this.pnlDetail;
            this.pnlDept.Name = "pnlDept";
            this.pnlDept.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDept.Size = new System.Drawing.Size(212, 176);
            this.pnlDept.SPName = "CHRIS_SP_SUMMER_INTERNS_DEPT_CONT_MANAGER";
            this.pnlDept.TabIndex = 2;
            this.pnlDept.TabStop = true;
            // 
            // dgvDept
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDept.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDept.ColumnHeadersHeight = 28;
            this.dgvDept.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSeg,
            this.colDept,
            this.colContribution,
            this.colType,
            this.colDNo});
            this.dgvDept.ColumnToHide = null;
            this.dgvDept.ColumnWidth = null;
            this.dgvDept.CustomEnabled = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDept.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDept.DisplayColumnWrapper = null;
            this.dgvDept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDept.GridDefaultRow = 0;
            this.dgvDept.Location = new System.Drawing.Point(0, 0);
            this.dgvDept.Name = "dgvDept";
            this.dgvDept.ReadOnlyColumns = null;
            this.dgvDept.RequiredColumns = "COLSEG,COLDEPT,COLCONTRIBUTION";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDept.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDept.RowHeadersWidth = 10;
            this.dgvDept.Size = new System.Drawing.Size(212, 176);
            this.dgvDept.SkippingColumns = null;
            this.dgvDept.TabIndex = 0;
            this.dgvDept.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvDept_CellValidating);
            this.dgvDept.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvDept_EditingControlShowing);
            this.dgvDept.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDept_CellEnter);
            // 
            // colSeg
            // 
            this.colSeg.DataPropertyName = "PR_SEGMENT";
            this.colSeg.HeaderText = "Segment";
            this.colSeg.MaxInputLength = 3;
            this.colSeg.Name = "colSeg";
            this.colSeg.ToolTipText = "Valid Segments are GF or GCB";
            this.colSeg.Width = 65;
            // 
            // colDept
            // 
            this.colDept.ActionLOV = "DEPT_LOV";
            this.colDept.ActionLOVExists = "DEPT_LOV_EXISTS";
            this.colDept.AttachParentEntity = false;
            this.colDept.DataPropertyName = "PR_DEPT";
            this.colDept.EntityName = null;
            this.colDept.HeaderText = "Dept.";
            this.colDept.LookUpTitle = null;
            this.colDept.LOVFieldMapping = "PR_DEPT";
            this.colDept.Name = "colDept";
            this.colDept.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDept.SearchColumn = "PR_DEPT";
            this.colDept.SkipValidationOnLeave = false;
            this.colDept.SpName = "CHRIS_SP_INTER_DEPT_CONT_MANAGER";
            this.colDept.ToolTipText = "Press [F9] to Display The List";
            this.colDept.Width = 80;
            // 
            // colContribution
            // 
            this.colContribution.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colContribution.DataPropertyName = "PR_CONTRIB";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.colContribution.DefaultCellStyle = dataGridViewCellStyle2;
            this.colContribution.HeaderText = "Contri. %";
            this.colContribution.MaxInputLength = 3;
            this.colContribution.Name = "colContribution";
            this.colContribution.ToolTipText = "If The Contribution Is Equal To 100 Then Press SAVE Button To Save The Record";
            // 
            // colType
            // 
            this.colType.DataPropertyName = "PR_TYPE";
            this.colType.HeaderText = "Type";
            this.colType.Name = "colType";
            this.colType.Visible = false;
            // 
            // colDNo
            // 
            this.colDNo.DataPropertyName = "PR_P_NO";
            this.colDNo.HeaderText = "D. No.";
            this.colDNo.Name = "colDNo";
            this.colDNo.Visible = false;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(457, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(133, 13);
            this.txtUserName.TabIndex = 118;
            this.txtUserName.Text = "User  Name :   CHRISTOP";
            // 
            // CHRIS_Personnel_SumInernsHiring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(674, 623);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlDept);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlDetail);
            this.CurrentPanelBlock = "pnlDetail";
            this.Name = "CHRIS_Personnel_SumInernsHiring";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "iCORE CHRISS - Summer Interns Entry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_ContractStafHiringEnt_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlDept, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.pnlDept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDept)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtCurrOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox txtClass;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLDatePicker dtpFromDate;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtUni;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label7;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlDept;
        private CrplControlLibrary.LookupButton lbtnBranch;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtLastName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLDatePicker dtpToDate;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDept;
        private CrplControlLibrary.SLTextBox txtID;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtPrevIntern;
        private System.Windows.Forms.Label txtRecommend;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private System.Windows.Forms.Label label20;
        private CrplControlLibrary.SLTextBox txtStipend;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtAddress;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLDatePicker dtpDOB;
        private CrplControlLibrary.SLTextBox txtPh1;
        private CrplControlLibrary.SLTextBox txtPh2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLTextBox txtMarital;
        private CrplControlLibrary.SLTextBox txtSex;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox txtNID;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label txtUserName;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSeg;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn colDept;
        private System.Windows.Forms.DataGridViewTextBoxColumn colContribution;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDNo;
    }
}