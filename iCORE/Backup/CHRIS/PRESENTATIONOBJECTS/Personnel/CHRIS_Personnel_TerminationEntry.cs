using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_TerminationEntry : ChrisSimpleForm
    {
        #region Fields
        String personNumer = string.Empty;
        #endregion

        #region Constructors

        public CHRIS_Personnel_TerminationEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Personnel_TerminationEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            //this.ShowOptionTextBox = false;
            this.pnlHead.SendToBack();
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            this.txtOption.Select();
            this.txtOption.Focus();
            base.OnLoad(e);
            this.lblUserName.Text = this.UserName;
            this.txtUser.Text = this.UserName;
            this.dtpTerminDate.Value = null;
            tbtAdd.Visible = false;
            tbtSave.Visible = false;
            this.txtDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
            this.tbtList.Visible = false;
            this.tbtDelete.Visible = false;
            this.ShowTextOption = true;
            this.txtOption.Select();
            this.txtOption.Focus();
            EnableControls(false);

            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF5 = false;
            this.FunctionConfig.EnableF2 = false;
            //this.CurrrentOptionTextBox = this.txtCurrOption;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Add" || actionType == "Edit")
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
                EnableControls(true);
                return;
            }
            if (actionType == "Cancel")
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
                this.dtpTerminDate.Value = null;
                EnableControls(false);
                return;
            }
            if (actionType == "Save")
            {
                if (!this.IsValidated())
                {
                    this.ActiveControl.Select();
                    this.ActiveControl.Focus();
                    return;
                }
                DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                  , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dRes == DialogResult.Yes)
                {
                    personNumer = string.Empty;
                    personNumer = txtPersNo.Text;
                    if (FunctionConfig.CurrentOption == Function.Add)
                    {
                        CallReport("AUPR", "OLD");
                        base.DoToolbarActions(ctrlsCollection, actionType);
                        CallReport("AUPO", "NEW");
                        //this.FunctionConfig.CurrentOption = Function.Add;
                        //this.CurrrentOptionTextBox.Text = "Add";
                        //this.txtOption.Text = "A";
                    }
                    else
                    {
                        CallReport("AUPR", "OLD");
                        base.DoToolbarActions(ctrlsCollection, actionType);
                        CallReport("AUPO", "NEW");
                        //this.FunctionConfig.CurrentOption = Function.Modify;
                        //this.CurrrentOptionTextBox.Text = "Modify";
                        //this.txtOption.Text = "M";
                    }
                    this.Cancel();
                    dtpTerminDate.Value = null;
                    this.EnableControls(false);
                    this.FunctionConfig.CurrentOption = Function.None;
                    this.CurrrentOptionTextBox.Text = "";
                    txtOption.Text = "";
                    txtOption.Focus();
                    txtOption.Select();
                    return;
                }
                else if (dRes == DialogResult.No)
                {
                    this.Cancel();
                    dtpTerminDate.Value = null;
                    this.EnableControls(false);
                    this.FunctionConfig.CurrentOption = Function.None;
                    this.CurrrentOptionTextBox.Text = "";
                    txtOption.Text = "";
                    txtOption.Focus();
                    txtOption.Select();
                    return;
                    
                }
            }
            if (actionType == "Delete")
            {
                personNumer = string.Empty;
                personNumer = txtPersNo.Text;
                CallReport("AUPR", "OLD");
                base.DoToolbarActions(ctrlsCollection, actionType);
                CallReport("AUPO", "NEW");
                this.Cancel();
                this.Delete();

                return;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);



            if (actionType == "Cancel")
            {
                this.dtpTerminDate.Value = null;
            }
            //if (actionType == "Add")
            //{
            //    //this.operationMode = Mode.Edit;
            //}
            //    //base.DoToolbarActions(ctrlsCollection, actionType);
            //    //switch (actionType)
            //    //{
            //    //    case "Add":
            //    //        this.FunctionConfig.CurrentOption = Function.Add;
            //    //        //this.lbtnPNo.ActionType = "LOV_PER";
            //    //        break;
            //    //    case "Edit":
            //    //        this.FunctionConfig.CurrentOption = Function.Modify;
            //    //        //this.lbtnPNo.ActionType = "LOV_GEID";
            //    //        break;
            //    //    case "Cancel":
            //    //        this.FunctionConfig.CurrentOption = Function.None;
            //    //        break;
            //    //    case "Save":
            //    //        this.FunctionConfig.CurrentOption = Function.None;
            //    //        break;
            //    //    case "List":
            //    //        this.FunctionConfig.CurrentOption = Function.Modify;
            //    //        //this.lbtnPNo.ActionType = "LOV_GEID";
            //    //        break;
            //    //    case "Delete":
            //    //        //DoFunction(Keys.F1, Function.Delete);
            //    //        this.FunctionConfig.CurrentOption = Function.None;
            //    //        break;


            //    //}

        }

        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }

        private bool IsValidated()
        {
            bool flag = true;

            if (this.txtPersNo.Text == "") { flag = false; }
            if (this.dtpTerminDate.Value == null) { flag = false; }
            if (this.txtTerminType.Text == "") { flag = false; }
            if (this.txtReason.Text == "") { flag = false; }
            if (this.txtResgRecieved.Text == "") { flag = false; }
            if (this.txtResgAppr.Text == "") { flag = false; }
            if (this.txtExitIntrvDone.Text == "") { flag = false; }
            if (this.txtIDCardRet.Text == "") { flag = false; }
            if (this.txtNoticeOfStaffChanged.Text == "") { flag = false; }
            if (this.txtPowerOfAttornyCancel.Text == "") { flag = false; }
            if (this.txtDelFromMic.Text == "") { flag = false; }
            if (this.txtFinalSettlementReport.Text == "") { flag = false; }

            return flag;
        }

        private void EnableControls(bool yes)
        {
            lbtnPNo.Enabled = yes;
            dtpTerminDate.Enabled = yes;
        }

        protected override bool View()
        {
            bool flag = false;
            base.View();
            this.EnableControls(true);

            return flag;
        }

        private void CallReport(string AuditPreOrAuditPo, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = this.GlobalReportPath;// @"C:\SPOOL\CHRIS\AUDIT\";
            string auditStatus = AuditPreOrAuditPo + DateTime.Now.ToString("yyyyMMddHms");
            //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            //string FullPath = globalPath + auditStatus;
            string FullPath = frm.m_ReportPath + auditStatus; 
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';


            CrplControlLibrary.SLTextBox txtPNo = new CrplControlLibrary.SLTextBox(comp);
            txtPNo.Name = "PNO";
            txtPNo.Text = personNumer;
            pnl.Controls.Add(txtPNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "user";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "st";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);




            frm.Controls.Add(pnl);
            frm.RptFileName = "AUDIT12A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles Key Press Event to identify the current charachter as Y or N
        /// </summary>
        /// <param name="object sender"></param>
        /// <param name="KeyPressEventArgs e"></param>
        private void txtYN_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (txt != null)
            {
                switch (e.KeyChar)
                {
                    case 'Y':
                    case 'y':
                    case 'N':
                    case 'n':
                        e.Handled = false;
                        break;
                    case '\b':
                        e.Handled = false;
                        break;
                    default:
                        e.Handled = true;
                        break;
                }
            }
        }
        /// <summary>
        /// Handles Key Press Event to identify 
        /// the current charachter as [T]ermination or [R]esignation or [S]eparation or [M]is Conduct
        /// </summary>
        /// <param name="object sender"></param>
        /// <param name="KeyPressEventArgs e"></param>
        private void txtTerminType_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (txt != null)
            {
                switch (e.KeyChar)
                {
                    case 'T':
                    case 't':
                    case 'R':
                    case 'r':
                    case 'S':
                    case 's':
                    case 'M':
                    case 'm':
                        e.Handled = false;
                        break;
                    case '\b':
                        e.Handled = false;
                        break;
                    default:
                        e.Handled = true;
                        break;
                }
            }
        }

        private void txtFinalSettlementReport_Validating(object sender, CancelEventArgs e)
        {
            this.DoToolbarActions(this.Controls, "Save");
            //if (txtFinalSettlementReport.Text.Trim() == "Y" || txtFinalSettlementReport.Text.Trim() == "N")
            //{
            //    if (this.IsValidated())
            //    {
            //        DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
            //                           , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //        if (dRes == DialogResult.Yes)
            //        {
            //            this.DoToolbarActions(this.Controls, "Save");
            //            return;
            //        }
            //        else if (dRes == DialogResult.No)
            //        {
            //            this.Cancel();
            //            dtpTerminDate.Value = null;
            //            this.EnableControls(false);
            //            this.FunctionConfig.CurrentOption = Function.None;
            //            this.CurrrentOptionTextBox.Text = "";
            //            txtOption.Text = "";
            //            txtOption.Focus();
            //            txtOption.Select();
            //            return;
            //        }
            //    }
                
            //}
        }

        private void txtPersNo_Validating(object sender, CancelEventArgs e)
        {
            if (txtPersNo.Text != string.Empty)
            {
                if (this.FunctionConfig.CurrentOption == Function.Modify)
                {
                    if (txtTerminType.Text == string.Empty)
                    {
                        MessageBox.Show("Record Not Found");

                        this.Cancel();
                        this.EnableControls(true);
                        this.FunctionConfig.CurrentOption = Function.Modify;
                        this.CurrrentOptionTextBox.Text = "Modify";
                        this.txtOption.Text = "M";
                        txtPersNo.Focus();

                    }


                }

                if (this.FunctionConfig.CurrentOption == Function.Add)
                {
                    if (txtTerminType.Text != string.Empty)
                    {
                        MessageBox.Show("Record Already Entered");

                        this.Cancel();
                        dtpTerminDate.Value = null;
                        this.EnableControls(true);
                        this.FunctionConfig.CurrentOption = Function.Add;
                        this.CurrrentOptionTextBox.Text = "Add";
                        txtOption.Text = "A";
                        txtOption.Focus();
                        txtOption.Select();

                    }
                }
                if (this.FunctionConfig.CurrentOption == Function.Delete)
                {
                    if (txtTerminType.Text == string.Empty)
                    {
                        MessageBox.Show("Record Not Found");
                        this.Cancel();
                        dtpTerminDate.Value = null;
                        this.EnableControls(true);
                        this.FunctionConfig.CurrentOption = Function.Delete;
                        this.CurrrentOptionTextBox.Text = "Delete";
                        txtOption.Text = "D";

                        txtPersNo.Focus();

                    }
                    else
                    {
                        //DialogResult dRes = MessageBox.Show("Do you want to Delete this record [Y/N]..", "Note"
                        //                 , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        //if (dRes == DialogResult.Yes)
                        //{

                        this.DoToolbarActions(this.Controls, "Delete");
                        // this.Reset();
                        //call save
                        return;

                        //}
                        //else if (dRes == DialogResult.No)
                        //{
                        //    // this.Reset();
                        //    txtOption.Focus();

                        //    return;
                        //}

                    }
                }

                if (this.FunctionConfig.CurrentOption == Function.View)
                {
                    if (txtPersNo.Text != string.Empty)
                    {
                        DialogResult dRes = MessageBox.Show("Do You Want To View More Record Yes No", "Question"
                                         , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {
                            this.Cancel();
                            dtpTerminDate.Value = null;
                            this.EnableControls(true);
                            this.FunctionConfig.CurrentOption = Function.View;
                            this.CurrrentOptionTextBox.Text = "View";
                            txtOption.Text = "V";
                            txtPersNo.Focus();

                        }
                        else
                        {
                            this.Cancel();
                            dtpTerminDate.Value = null;
                            this.EnableControls(false);
                            this.FunctionConfig.CurrentOption = Function.None;
                            this.CurrrentOptionTextBox.Text = "";
                            txtOption.Text = "";
                            txtOption.Focus();
                            txtOption.Select();
                            return;


                        }
                    }
                }

            }
            //else
            //{
            //    this.Cancel();
            //    this.txtOption.Select();
            //    this.txtOption.Focus();
            //}
        }

        private void txtTerminType_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txtTerminType.Text != "T" && txtTerminType.Text != "R" && txtTerminType.Text != "S" && txtTerminType.Text != "M")
                {
                    MessageBox.Show(" Press [T]ermination or [R]esignation or [S]epration or [M]is conduct", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txtTerminType_Validating", exp);
            }
        }

        private void lbtnPNo_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                this.lbtnPNo.Enabled = false;
                this.lbtnPNo.Enabled = true;
            }
        }

        private void dtpTerminDate_Validating(object sender, CancelEventArgs e)
        {
            if (dtpTerminDate.Value != null)
            {
                DateTime dtTermin;
                DateTime dtJoin;
                DateTime dtConfirm;
                if (DateTime.TryParse(dtpTerminDate.Value.ToString(), out dtTermin) && DateTime.TryParse(txtJoiningDate.Text, out dtJoin))
                {
                    if (DateTime.TryParse(txtConfOnDate.Text, out dtConfirm))
                    {
                        if (dtTermin < dtConfirm || dtTermin < dtJoin)
                        {
                            MessageBox.Show("Termination Date Must Be Greater Then Confirmation Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                            e.Cancel = true;
                        }
                    }
                    else if (dtTermin < dtJoin)
                    {
                        MessageBox.Show("Termination Date Must Be Greater Then Confirmation Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                        e.Cancel = true;
                    }
                }
                // :pr_termin_date < :pr_confirm_on or :pr_termin_date < :pr_joining_date
            }
        }
        #endregion
    }
}