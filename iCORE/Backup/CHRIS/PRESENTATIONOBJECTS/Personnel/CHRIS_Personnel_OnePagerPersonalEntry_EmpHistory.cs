using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory : ChrisTabularForm
    {

        #region Declarations

        private CHRIS_Personnel_OnePagerPersonalEntry _mainForm = null;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory frm_EmpHistory;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Education frm_Education;
        
        string pr_SegmentValue = string.Empty;
        private DataTable dtPreEmp;
        private DataTable dtEdu;
        private DataTable dtTraining;

        #endregion

        #region Constructor
        public CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory()
        {
            InitializeComponent();
            this.ShowBottomBar.Equals(false);
          
        }

        public CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_OnePagerPersonalEntry mainForm, DataTable _dtPreEmp, DataTable _dtEdu, DataTable _dtTraining)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            dtPreEmp = _dtPreEmp;
            dtEdu = _dtEdu;
            dtTraining = _dtTraining;
            this._mainForm = mainForm;

            dgvEmpHistory.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);


           


        
        }
        #endregion

        #region Methods
        protected override void OnLoad(EventArgs e)
        {

            try
            {
                base.OnLoad(e);

                dgvEmpHistory.GridSource = dtPreEmp;
                dgvEmpHistory.DataSource = dgvEmpHistory.GridSource;
                
                this.txtOption.Visible = false;
                this.tbtAdd.Visible = false;
                this.tbtList.Visible = false;
                this.tbtSave.Visible = false;
                this.tbtCancel.Visible = false;
                this.tlbMain.Visible = false;

                this.KeyPreview = true;
                this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }


        }
        #endregion

        #region Events

        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Modifiers == Keys.Control)
                {
                    switch (e.KeyValue)
                    {
                        case 34:
                            //Ctl+PageDown  "Open education form"
                            frm_Education = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Education(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this._mainForm.MdiParent)), this.connbean, this._mainForm,dtPreEmp,dtEdu,dtTraining);
                            frm_Education.MdiParent = null;
                            frm_Education.Enabled = true;
                            frm_Education.Select();
                            frm_Education.Focus();
                            this.Hide();  
                            frm_Education.ShowDialog(this);
                            this.KeyPreview = true;
                            break;

                        case 33:
                            //Ctl+PageUp  "Hide this form"
                            this.Hide();  //shift to the MAIN form//
                            this.KeyPreview = true;
                            break;

                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "ShortCutKey_Press", exp);
            }
        }

        private void dgvEmpHistory_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {


            bool istrueJoiningDate  = false;
            bool istrueFromDate     = false;
            bool istrueDateTo       = false;

            try
            {
               
                DateTime Job_From;
                DateTime Job_To;


                if (e.ColumnIndex == 3)
                {
                    if (dgvEmpHistory.CurrentCell.OwningColumn.Name == "From" && (dgvEmpHistory.CurrentCell.Value != null || dgvEmpHistory.CurrentCell.Value != string.Empty) )
                    {
                        istrueFromDate = DateTime.TryParse(dgvEmpHistory.CurrentCell.Value.ToString(), out Job_From);

                        if (istrueJoiningDate == true && istrueFromDate == true)
                        {
                            Job_From = Convert.ToDateTime(dgvEmpHistory.CurrentCell.Value.ToString());
                        }
                        else
                        {
                           // MessageBox.Show("Date is Not In the Correct Format", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                           // e.Cancel = true;
                        }

                    }

                }
                
               
                else if (dgvEmpHistory.CurrentCell.OwningColumn.Name == "To" && (dgvEmpHistory.CurrentCell.Value != null || dgvEmpHistory.CurrentCell.Value != string.Empty) )
                {
                    if (dgvEmpHistory.CurrentCell.FormattedValue.ToString() != "")
                    {

                        istrueDateTo = DateTime.TryParse(dgvEmpHistory.CurrentCell.Value.ToString(), out Job_To);
                        istrueFromDate = DateTime.TryParse(dgvEmpHistory.CurrentRow.Cells["From"].Value.ToString(), out Job_From);

                        if (istrueDateTo == true && istrueFromDate == true )
                        {
                         
                        }
                        else
                        {
                            //MessageBox.Show("Date is Not In the Correct Format", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                           // e.Cancel = true;
                        }
                    }
                }
                else if (dgvEmpHistory.CurrentCell.OwningColumn.Name == "Organization_Address" && dgvEmpHistory.CurrentCell.Value.ToString() == "")
                {
                    e.Cancel = false;
                }
                else if (dgvEmpHistory.CurrentCell.OwningColumn.Name == "Designation" && dgvEmpHistory.CurrentCell.Value.ToString() == "")
                {
                    e.Cancel = false;
                }


              
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dgvEmpHistory_CellValidating", exp);
            }
        }
        
        #endregion

        private void dgvEmpHistory_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (e.Value != null)
            {
                e.Value = e.Value.ToString().ToUpper();
                e.FormattingApplied = true;

            }


        }

        private void CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory_Shown(object sender, EventArgs e)
        {
            dgvEmpHistory.EndEdit();
            dgvEmpHistory.Focus();
            //dgvEmpHistory.CurrentCell = dgvEmpHistory.Rows[0].Cells[0];
            dgvEmpHistory.BeginEdit(true);
        }

    }
}