namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU));
            this.label1 = new System.Windows.Forms.Label();
            this.dgvBlkEdu = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.pnltblBlkEdu = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrEYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Degree = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_College = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_City = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_E_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkEdu)).BeginInit();
            this.pnltblBlkEdu.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(249, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Education Information";
            // 
            // dgvBlkEdu
            // 
            this.dgvBlkEdu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkEdu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.PrEYear,
            this.Pr_Degree,
            this.Pr_Grade,
            this.Pr_College,
            this.Pr_City,
            this.Pr_Country,
            this.Pr_E_No});
            this.dgvBlkEdu.ColumnToHide = null;
            this.dgvBlkEdu.ColumnWidth = null;
            this.dgvBlkEdu.CustomEnabled = true;
            this.dgvBlkEdu.DisplayColumnWrapper = null;
            this.dgvBlkEdu.GridDefaultRow = 0;
            this.dgvBlkEdu.Location = new System.Drawing.Point(4, 3);
            this.dgvBlkEdu.Name = "dgvBlkEdu";
            this.dgvBlkEdu.ReadOnlyColumns = null;
            this.dgvBlkEdu.RequiredColumns = null;
            this.dgvBlkEdu.Size = new System.Drawing.Size(647, 246);
            this.dgvBlkEdu.SkippingColumns = null;
            this.dgvBlkEdu.TabIndex = 0;
            this.dgvBlkEdu.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvBlkEdu_CellValidating);
            this.dgvBlkEdu.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvBlkEdu_EditingControlShowing);
            this.dgvBlkEdu.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBlkEdu_CellEnter);
            // 
            // pnltblBlkEdu
            // 
            this.pnltblBlkEdu.ConcurrentPanels = null;
            this.pnltblBlkEdu.Controls.Add(this.dgvBlkEdu);
            this.pnltblBlkEdu.DataManager = "iCORE.Common.CommonDataManager";
            this.pnltblBlkEdu.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnltblBlkEdu.DependentPanels = null;
            this.pnltblBlkEdu.DisableDependentLoad = false;
            this.pnltblBlkEdu.EnableDelete = true;
            this.pnltblBlkEdu.EnableInsert = true;
            this.pnltblBlkEdu.EnableQuery = false;
            this.pnltblBlkEdu.EnableUpdate = true;
            this.pnltblBlkEdu.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntEducationCommand";
            this.pnltblBlkEdu.Location = new System.Drawing.Point(12, 52);
            this.pnltblBlkEdu.MasterPanel = null;
            this.pnltblBlkEdu.Name = "pnltblBlkEdu";
            this.pnltblBlkEdu.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnltblBlkEdu.Size = new System.Drawing.Size(655, 252);
            this.pnltblBlkEdu.SPName = "CHRIS_SP_RegStHiEnt_EDUCATION_MANAGER";
            this.pnltblBlkEdu.TabIndex = 10;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.MaxInputLength = 0;
            this.Column1.MinimumWidth = 2;
            this.Column1.Name = "Column1";
            this.Column1.Width = 2;
            // 
            // PrEYear
            // 
            this.PrEYear.DataPropertyName = "PR_E_YEAR";
            this.PrEYear.HeaderText = "Year";
            this.PrEYear.MaxInputLength = 4;
            this.PrEYear.Name = "PrEYear";
            this.PrEYear.Width = 50;
            // 
            // Pr_Degree
            // 
            this.Pr_Degree.DataPropertyName = "PR_DEGREE";
            this.Pr_Degree.HeaderText = "Certificate/Degree";
            this.Pr_Degree.MaxInputLength = 10;
            this.Pr_Degree.Name = "Pr_Degree";
            this.Pr_Degree.Width = 120;
            // 
            // Pr_Grade
            // 
            this.Pr_Grade.DataPropertyName = "PR_GRADE";
            this.Pr_Grade.HeaderText = "Division/Grade";
            this.Pr_Grade.MaxInputLength = 7;
            this.Pr_Grade.Name = "Pr_Grade";
            // 
            // Pr_College
            // 
            this.Pr_College.DataPropertyName = "PR_COLLAGE";
            this.Pr_College.HeaderText = "School/College/Unv";
            this.Pr_College.MaxInputLength = 50;
            this.Pr_College.Name = "Pr_College";
            this.Pr_College.Width = 130;
            // 
            // Pr_City
            // 
            this.Pr_City.DataPropertyName = "PR_CITY";
            this.Pr_City.HeaderText = "City";
            this.Pr_City.MaxInputLength = 10;
            this.Pr_City.Name = "Pr_City";
            // 
            // Pr_Country
            // 
            this.Pr_Country.DataPropertyName = "PR_COUNTRY";
            this.Pr_Country.HeaderText = "Country";
            this.Pr_Country.MaxInputLength = 10;
            this.Pr_Country.Name = "Pr_Country";
            // 
            // Pr_E_No
            // 
            this.Pr_E_No.DataPropertyName = "PR_P_NO";
            this.Pr_E_No.HeaderText = "Pr_E_No";
            this.Pr_E_No.Name = "Pr_E_No";
            this.Pr_E_No.Visible = false;
            // 
            // CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 316);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnltblBlkEdu);
            this.Name = "CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU";
            this.Text = "CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU";
            this.Controls.SetChildIndex(this.pnltblBlkEdu, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkEdu)).EndInit();
            this.pnltblBlkEdu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBlkEdu;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnltblBlkEdu;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrEYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Degree;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_College;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_City;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_E_No;

    }
}