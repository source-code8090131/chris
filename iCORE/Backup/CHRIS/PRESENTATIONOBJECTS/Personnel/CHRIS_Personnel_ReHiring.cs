using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_ReHiring : ChrisTabularForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        bool valid = false;
        #region Constructor

        public CHRIS_Personnel_ReHiring()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_ReHiring(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this.CurrentPanelBlock = this.PnlPersonnel.Name;
        }
        #endregion

        #region Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.pnlHead.SendToBack();
            txtOption.Visible = false;
            txtDate.Text = Now().ToString("dd/MM/yyyy");
            txtLocation.Text = this.CurrentLocation;
            txtUser.Text = this.userID;
            this.label1.Text += "   " + this.UserName;
            this.DGVPersonnel.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            //DGVPersonnel.Columns["col_SA_DATE"].DefaultCellStyle.Format = "dd/MM/yyyy";
            //DGVPersonnel.Columns["REH_OLD_PR_NO"].DefaultCellStyle.NullValue =0;
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            //   DGVPersonnel.EndEdit();
            if (actionType == "Save")
            {
                bool end = true;
                if (DGVPersonnel.CurrentCell != null)
                {
                    // if (DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString() != string.Empty)
                    // {

                    end = true; //DGVPersonnel.EndEdit();
                    // }
                    if (end)
                    {
                        //***************************Commented for Required change Using FrameWork***********************
                        //if (this.validate())
                        //{
                            DataTable dt = (DataTable)DGVPersonnel.DataSource;
                            if (dt != null)
                            {
                                DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                                DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                                if (dtAdded != null || dtUpdated != null)
                                {
                                    DialogResult dr = MessageBox.Show("Do you want to save changes.", "", MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);
                                    if (dr == DialogResult.No)
                                    {
                                        return;
                                    }
                                    else
                                    {
                                        //***************************Commented for Required change***********************
                                        //this.Save_Click(this.PnlPersonnel, "Save");
                                        base.DoToolbarActions(this.PnlPersonnel.Controls, "Save");
                                        //bool isSaved =DGVPersonnel.SaveGrid(this.PnlPersonnel);
                                        //if (isSaved)
                                        //{
                                        //    MessageBox.Show("Changes Saved Successfully.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //    dt.AcceptChanges();
                                        //    this.tbtSave.Enabled = false;
                                        //}
                                        return;
                                    }
                                }
                            }
                        //}
                        //else
                        //{
                        //    MessageBox.Show("Please Provides Missing Values.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //    return;
                        //}
                    }
                }
            }
            else if (actionType == "Search")
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
                DGVPersonnel.Columns[0].ReadOnly = true;
                DGVPersonnel.Columns[1].ReadOnly = true;
                DGVPersonnel.Columns[2].ReadOnly = true;
            }
            else
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
                if (actionType == "Cancel")
                {
                    DGVPersonnel.Columns[0].ReadOnly = false;
                    DGVPersonnel.Columns[1].ReadOnly = false;
                    DGVPersonnel.Columns[2].ReadOnly = false;
                }
            }
        }

        private bool validate()
        {
            try
            {
                valid = true;
                //if (DGVPersonnel.Rows.Count > 0)
                //{
                //    for (int j = 0; j < DGVPersonnel.Rows.Count; j++)
                //    {
                //        if (!this.DGVPersonnel.Rows[j].IsNewRow)
                //        {

                //            //for (int i = 0; i < 5; i++)
                //            {
                //                if ((this.DGVPersonnel[0, j].Value == DBNull.Value || this.DGVPersonnel[1, j].Value == DBNull.Value || this.DGVPersonnel[3, j].Value == DBNull.Value))
                //                {
                //                    valid = false;
                //                    return valid;
                //                }
                //            }
                //        }
                //    }
                //    return valid;

                //}
                return valid;
            }
            catch (Exception ei)
            {
                return false;
                base.LogError("CHRIS_Personnel_ReHiring.validate()", ei);
            }
        }
        #endregion

        #region Events
        private void DGVPersonnel_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (DGVPersonnel.RowCount > 1)
            {
                //if (e.ColumnIndex == 0)
                //{
                //    if (DGVPersonnel.CurrentCell.IsInEditMode)
                //    {
                //        if (DGVPersonnel.CurrentCell.EditedFormattedValue != null)
                //            DGVPersonnel.CurrentCell.Value = DGVPersonnel.CurrentCell.EditedFormattedValue;
                //        //DGVPersonnel.RefreshEdit();
                //    }
                //}
                if (e.ColumnIndex == 1)
                {
                    if (DGVPersonnel.CurrentCell.IsInEditMode)
                    {
                        if (DGVPersonnel.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                        {
                            //MessageBox.Show(" Value Has To Be Entered ", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                        }
                    }
                }

                # region REH_DATE (cell 3)
                if (e.ColumnIndex == 3)
                {
                    if (DGVPersonnel.CurrentRow.Cells["REH_DATE"].IsInEditMode)
                    {
                        if (DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue != null && DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString() != string.Empty && DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString() != "")
                        {
                            DateTime dTime = new DateTime(1900, 1, 1);
                            DateTime dt_Maxdt = this.CurrentDate;
                            int dt_yrs = dt_Maxdt.Year;
                            int days;
                            try
                            {
                                dTime = DateTime.Parse((DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString()));
                                TimeSpan ts = dTime.Subtract(dt_Maxdt);
                                DGVPersonnel.CurrentRow.Cells["REH_DATE"].Value = DateTime.Parse((DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString()));
                                days = ts.Days;
                                int years = dTime.Year;

                                if (years < dt_yrs)
                                {
                                    MessageBox.Show("Re-hiring Year Can Not Be Less Then Current Year");
                                    e.Cancel = true;
                                    return;
                                }
                            }
                            catch (Exception ex)
                            {
                                if (dTime == new DateTime(1900, 1, 1))
                                {
                                    DGVPersonnel.CurrentCell.ErrorText = "Incorrect date format";
                                    e.Cancel = true;
                                    return;
                                }
                            }
                            if (DGVPersonnel.CurrentCell.ErrorText != string.Empty)
                            {
                                DGVPersonnel.CurrentCell.ErrorText = null;
                            }
                            //DGVPersonnel["REH_DATE", e.RowIndex].Value = dTime;
                            DataTable dt = (DataTable)DGVPersonnel.DataSource;
                            DataRow row = null;
                            if (this.DGVPersonnel.HasDataBoundItem(e.RowIndex) && this.DGVPersonnel.Rows[e.RowIndex].DataBoundItem != null)
                                row = (this.DGVPersonnel.Rows[e.RowIndex].DataBoundItem as DataRowView).Row;//((DataTable)DGVPersonnel.DataSource).Rows[DGVPersonnel.CurrentRow.Index];
                            if (row != null)
                            {
                                if (row.RowState == DataRowState.Added || row.RowState == DataRowState.Detached)
                                {
                                    Result rslt_add;
                                    Dictionary<string, object> param_add = new Dictionary<string, object>();
                                    string confirmdt;
                                    param_add.Add("REH_OLD_PR_NO", DGVPersonnel.CurrentRow.Cells["REH_OLD_PR_NO"].EditedFormattedValue);
                                    param_add.Add("REH_DATE", DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue);
                                    rslt_add = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "GETCONFIRMATIONDT_IfADD", param_add);

                                    if (rslt_add.isSuccessful)
                                    {
                                        if (rslt_add.dstResult.Tables.Count > 0 && rslt_add.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            //LC_DATE
                                            confirmdt = rslt_add.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                                            if (confirmdt != string.Empty)
                                            {
                                                DGVPersonnel.CurrentRow.Cells["LC_DATE"].Value = DateTime.Parse(confirmdt);
                                                //DGVPersonnel.CurrentRow.Cells["LC_DATE"].ReadOnly = true;
                                            }
                                        }
                                    }
                                }

                                if (row.RowState == DataRowState.Unchanged)
                                {
                                    Result rslt_Modify;
                                    Dictionary<string, object> param_Modify = new Dictionary<string, object>();
                                    string confirmdt1;
                                    param_Modify.Add("REH_PR_NO", DGVPersonnel.CurrentRow.Cells["REH_PR_NO"].EditedFormattedValue);
                                    param_Modify.Add("REH_DATE", DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue);
                                    rslt_Modify = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "GETCONFIRMATIONDT_IfModify", param_Modify);

                                    if (rslt_Modify.isSuccessful)
                                    {
                                        if (rslt_Modify.dstResult.Tables.Count > 0 && rslt_Modify.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            //LC_DATE
                                            confirmdt1 = rslt_Modify.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                                            if (confirmdt1 != string.Empty)
                                            {
                                                DGVPersonnel.CurrentRow.Cells["LC_DATE"].Value = DateTime.Parse(confirmdt1);
                                                DGVPersonnel.CurrentRow.Cells["LC_DATE"].ReadOnly = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                    else
                    {
                        if (DGVPersonnel.CurrentCell.Value == null)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }

                #endregion
                # region     LC_DATE (cell 4)

                if (e.ColumnIndex == 4)
                {
                    if (DGVPersonnel.CurrentRow.Cells["LC_Date"].IsInEditMode)
                    {
                        if (DGVPersonnel.CurrentRow.Cells["LC_Date"].EditedFormattedValue != null && DGVPersonnel.CurrentRow.Cells["LC_Date"].EditedFormattedValue.ToString() != string.Empty && DGVPersonnel.CurrentRow.Cells["LC_Date"].EditedFormattedValue.ToString().ToString() != "")
                        {
                            System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                            dtf.ShortDatePattern = "dd/MM/yyyy";
                            //this.CurrentDate
                            DateTime Lc_date = new DateTime(1900, 1, 1);
                            DateTime Reh_date = new DateTime(1900, 1, 1);
                            DateTime.TryParse(DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString(), out Reh_date);
                            int days;

                            try
                            {
                                Lc_date = DateTime.Parse((DGVPersonnel.CurrentRow.Cells["LC_Date"].EditedFormattedValue.ToString()), dtf);
                                TimeSpan ts = Lc_date.Subtract(Reh_date);
                                days = ts.Days;

                                if (days < 0)
                                {
                                    MessageBox.Show("Confirmation Date Should be Greater Then Re-Hiring Date");
                                    e.Cancel = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                if (Lc_date == new DateTime(1900, 1, 1))
                                {
                                    DGVPersonnel.CurrentCell.ErrorText = "Incorrect date format";
                                    e.Cancel = true;
                                    return;
                                }
                            }
                        }

                        else
                            e.Cancel = true;
                    }
                    else
                    {
                        if (DGVPersonnel.CurrentCell.Value == null)
                            e.Cancel = true;
                    }
                }
                #endregion
            }
        }
        private void DGVPersonnel_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataTable dt = (DataTable)this.DGVPersonnel.DataSource;
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    if (e.RowIndex < dt.Rows.Count - 1)
            //    {
            //        if (dt.Rows[e.RowIndex].RowState == DataRowState.Added)
            //        {
            //            REH_PR_NO.SkipValidationOnLeave = true;
            //        }
            //        else
            //        {
            //            REH_PR_NO.SkipValidationOnLeave = false;
            //        }
            //    }
            //    else
            //    {
            //        REH_PR_NO.SkipValidationOnLeave = true;
            //    }
            //}
            //else
            //{
            //    REH_PR_NO.SkipValidationOnLeave = true;
            //}
        }
        private void DGVPersonnel_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            Result rslt;

            rslt = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "GetPr_p_no");
            //if (DGVPersonnel.RowCount <= 1)
            {
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            int NewPr_No = int.Parse(rslt.dstResult.Tables[0].Rows[0][0] == null ? "0" : rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                            int sumObject = 0;
                            if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                            {
                                DataTable dt = (DataTable)DGVPersonnel.DataSource;
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    object temp = dt.Compute("Max(REH_PR_NO)", "ID is null");
                                    sumObject = int.Parse(temp == null || temp.ToString() == "" ? "0" : temp.ToString());
                                }
                                if (sumObject >= NewPr_No)
                                {
                                    sumObject += 1;
                                    NewPr_No = sumObject;
                                }
                                //if(!( DGVPersonnel.CurrentRow.Cells["REH_PR_NO"].IsInEditMode))
                                //DGVPersonnel.BeginEdit(true);
                                DGVPersonnel.CurrentRow.Cells["REH_PR_NO"].Value = NewPr_No;

                                if ((DGVPersonnel.CurrentRow.Cells["REH_PR_NO"].IsInEditMode))
                                {
                                    DGVPersonnel.RefreshEdit();
                                    DGVPersonnel.EndEdit();
                                    DGVPersonnel[1, DGVPersonnel.CurrentRow.Index].Selected = true;
                                    DGVPersonnel.BeginEdit(true);
                                }
                                //DGVPersonnel.EndEdit();
                                //DGVPersonnel.CurrentCell = DGVPersonnel.CurrentRow.Cells["REH_PR_NO"];

                                //DGVPersonnel.EditingControl.Text = NewPr_No.ToString();
                                //DGVPersonnel.CurrentCell = DGVPersonnel.CurrentRow.Cells[1];
                                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                            }
                        }
                        catch (Exception exp)
                        {
                            LogError(this.Name, "DGVPersonnel_UserAddedRow", exp);
                        }
                    }
                }
            }
            //else
            //{
            //    int CurrentCell = DGVPersonnel.CurrentCell.Value;
            //}
        }


        #endregion


    }
}