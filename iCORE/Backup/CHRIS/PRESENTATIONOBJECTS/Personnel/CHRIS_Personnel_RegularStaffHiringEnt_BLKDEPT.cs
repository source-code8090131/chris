using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using CrplControlLibrary;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT : TabularForm
    {
        #region --Variable--
        private DataTable dtDept;
        private DataTable dtEmp;
        private DataTable dtRef;
        private DataTable dtChild;
        private DataTable dtEdu;
        private CHRIS_Personnel_RegularStaffHiringEnt _mainForm = null;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP frm_Emp;
        iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand ent;
        string pr_SegmentValue = string.Empty;
        string w_cont = null;
        int CountributionCount;

        //iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP frm_Emp;
        #endregion

        #region --Constructor--
        public CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_RegularStaffHiringEnt mainForm, CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP BLKEMP, DataTable _dtDept, DataTable _dtEmp, DataTable _dtRef, DataTable _dtChild, DataTable _dtEdu)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            dtDept  = _dtDept;
            dtEmp   = _dtEmp;
            dtRef   = _dtRef;
            dtChild = _dtChild;
            dtEdu   = _dtEdu;
            frm_Emp = BLKEMP;
            this._mainForm = mainForm;
            this.dgvBlkDept.Rows.Clear();
        }
        #endregion

        #region --Events--

        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                this.tbtAdd.Visible = false;
                this.tbtList.Visible = false;
                this.tbtSave.Visible = false;
                this.tbtCancel.Visible = false;
                this.tlbMain.Visible = false;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CommonOnLoadMethods", exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                if (frm_Emp != null)
                    frm_Emp.Hide();

                dgvBlkDept.GridSource   = dtDept;
                dgvBlkDept.DataSource   = dtDept;//dgvBlkDept.GridSource;
                dgvBlkDept.Focus();
                dgvBlkDept.Select();
                Dept.AttachParentEntity = true;
                dgvBlkDept.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
                this.KeyPreview         = true;
                this.KeyDown            += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        /// <summary>
        /// First Test the Validation
        /// On Ctrl+Down/Ctrl+UP open new POPUPs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            bool Validate = true;
            try
            {
                if (e.Modifiers == Keys.Control)
                {
                    switch (e.KeyValue)
                    {
                        case 34:
                            dgvBlkDept.CancelEdit();
                            dgvBlkDept.EndEdit();
                            
                            CountributionCount = 0;

                            foreach (DataGridViewRow row in dgvBlkDept.Rows)
                            {
                                if (row.Cells["Contribution"].Value != null && row.Cells["Contribution"].Value.ToString() != "")
                                {
                                    CountributionCount = CountributionCount + Convert.ToInt32(row.Cells["Contribution"].Value.ToString());
                                }
                            }

                            w_cont = CountributionCount.ToString();
 
                            if (w_cont != "100")
                            {
                                MessageBox.Show("Total Contribution Should be 100 ....!", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                Validate = false;
                                dgvBlkDept.Rows[0].Cells[0].DataGridView.Focus();
                                dgvBlkDept.Rows[0].Cells[0].DataGridView.Select();
                            }
                            else if (w_cont == null)
                            {
                                MessageBox.Show("Please Enter Department In Which Employee Is Hired", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                Validate = false;
                            }
                            else
                            {
                                if (_mainForm.FunctionConfig.CurrentOption.ToString() != "A" && Validate == true)
                                {
                                    frm_Emp             = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, null,this , dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                    frm_Emp.MdiParent   = null;
                                    frm_Emp.Enabled     = true;
                                    frm_Emp.ShowDialog(this);
                                    this.Hide();
                                    _mainForm.strCurrentPopUP = "BLKEMP";
                                }
                            }

                            this.KeyPreview = true;
                            break;

                        case 33:
                            dgvBlkDept.CancelEdit();
                            dgvBlkDept.EndEdit();
                            if (_mainForm.txtOption.Text == "A" || _mainForm.txtOption.Text == "M")
                            {
                                w_cont = "0";
                                this.Hide();
                                _mainForm.txt_PR_TAX_PAID.Select();
                                _mainForm.txt_PR_TAX_PAID.Focus();
                            }
                            else if (_mainForm.txtOption.Text == "V")
                            {
                                w_cont = "0";
                                this.Hide();
                            }
                            else if (_mainForm.txtOption.Text == "D")
                            {
                                w_cont = "0";
                                this.Hide();
                            }
                            this.KeyPreview = true;
                            break;
                    }

                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "ShortCutKey_Press", exp);
            }
        }

        /// <summary>
        /// Cell Validation 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBlkDept_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (dgvBlkDept.Rows.Count > 1)
                {
                    if (dgvBlkDept.CurrentRow.Cells["colSegment"].Value != null)
                    {
                        pr_SegmentValue = dgvBlkDept.CurrentRow.Cells["colSegment"].Value.ToString();
                    }

                    if (e.ColumnIndex == 0)
                    {
                        if (dgvBlkDept.Rows[e.RowIndex].Cells[e.ColumnIndex].IsInEditMode)
                        {
                            if ((dgvBlkDept.CurrentCell.EditedFormattedValue.ToString().ToUpper() != "GF" && dgvBlkDept.CurrentCell.EditedFormattedValue.ToString().ToUpper() != "GCB") || dgvBlkDept.CurrentCell.EditedFormattedValue.ToString() == "")
                            {
                                MessageBox.Show("Press <CTRL_PAGE DOWN> To Go On Previous Screen", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                e.Cancel = true;
                                return;
                            }
                        }
                    }

                    //dgvBlkDept.EndEdit();
                    if (dgvBlkDept.CurrentCell.IsInEditMode)
                    {
                        if (dgvBlkDept.CurrentCell.OwningColumn.Name == "Dept" && dgvBlkDept.CurrentCell.Value != null)
                        {
                            

                            string pr_dept      = null;
                            string pr_deptValue = dgvBlkDept.CurrentCell.Value.ToString();


                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("PR_SEGMENT", pr_SegmentValue);
                            param.Add("PR_DEPT", pr_deptValue);

                            Result rsltCode;
                            CmnDataManager cmnDM    = new CmnDataManager();
                            rsltCode                = cmnDM.GetData("CHRIS_SP_RegStHiEnt_DEPT_CONT_MANAGER", "DEPT_EXIST", param);

                            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                            {
                                pr_dept = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                            }
                            else
                            {
                                e.Cancel = true;
                            }

                            //if (pr_dept == null && pr_dept == string.Empty)
                            //{
                            //    MessageBox.Show("Invalid Department Entered Press [F9] Key For Help", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //    e.Cancel = true;
                            //}

                            //iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelDeptContCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelDeptContCommand)this.pnlBLKDept.CurrentBusinessEntity;
                            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand)this.pnlBLKDept.CurrentBusinessEntity;
                            if (ent != null)
                            {
                                ent.PR_SEGMENT = pr_SegmentValue;
                            }

                        }
                        else if (dgvBlkDept.CurrentCell.OwningColumn.Name == "Contribution")
                        {
                            Double dContribution = 0;
                            Double PreviousValue = 0;
                            if (dgvBlkDept[e.ColumnIndex, e.RowIndex].Value != null)
                                double.TryParse(dgvBlkDept[e.ColumnIndex, e.RowIndex].Value.ToString(), out PreviousValue);

                            Double.TryParse(dgvBlkDept[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString(), out dContribution);
                            dContribution = Math.Round(dContribution, 0, MidpointRounding.AwayFromZero);

                            double totalCon = 0;
                            
                            if (dgvBlkDept.CurrentCell.EditedFormattedValue.ToString() == "0")
                            {
                                MessageBox.Show("Please Enter Contribution For The Given Dept.", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //this.Close();
                                e.Cancel = true;
                            }
                            else if (dgvBlkDept.CurrentCell.EditedFormattedValue.ToString() == "")
                            {
                                MessageBox.Show("Please Enter Contribution For The Given Dept.", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //this.Close();
                                e.Cancel = true;
                            }

                            int Contr;
                            bool ChkContr;
                            ChkContr = int.TryParse(dgvBlkDept.CurrentCell.EditedFormattedValue.ToString(), out Contr);
                            if (ChkContr)
                            {
                                if (dContribution > 100)
                                {
                                    e.Cancel = true;
                                }
                                else
                                {
                                    if (dgvBlkDept.DataSource as DataTable != null)
                                        double.TryParse((dgvBlkDept.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalCon);
                                    totalCon = totalCon + dContribution - PreviousValue;

                                    if (totalCon < 100)
                                    {
                                        dgvBlkDept.CurrentCell.Value = dContribution;
                                    }
                                    if (totalCon == 100)
                                    {
                                        dgvBlkDept.CurrentCell.Value = dContribution;
                                    }
                                    if (totalCon > 100)
                                    {
                                        MessageBox.Show("YOUR CONTRIBUTION % SUM FOR THE CURRENT ID IS EXCEEDING 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        totalCon = totalCon - dContribution;
                                        e.Cancel = true;
                                    }
                                }
                            }
                            
                            dgvBlkDept.EndEdit();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dgvBlkDept_CellValidating", exp);
            }
        }
       
        /// <summary>
        /// ON CELL LEAVE CHECK THE CONTRIBUTION CELL: IF NULL OR ZERO THAN CLOSE POPUP N MOVE TO MAIN SCREEN
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBlkDept_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvBlkDept.CurrentCell.OwningColumn.Name == "Dept" && dgvBlkDept.CurrentCell.Value != null)
            {
                ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand)this.pnlBLKDept.CurrentBusinessEntity;
                //dgvBlkDept.EndEdit();
                if (ent != null)
                {
                    ent.PR_SEGMENT = pr_SegmentValue;
                }
            }
        }
        private void dgvBlkDept_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }

        }
        private void dgvBlkDept_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgvBlkDept.BeginEdit(true);

            if (dgvBlkDept.CurrentRow.Cells["colSegment"].Value != null)
            {
                pr_SegmentValue = dgvBlkDept.CurrentRow.Cells["colSegment"].Value.ToString();
            }


            if (dgvBlkDept.CurrentCell.OwningColumn.Name == "Dept" && dgvBlkDept.CurrentCell.Value != null)
            {
                //ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelDeptContCommand)this.pnlBLKDept.CurrentBusinessEntity;
                ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand)this.pnlBLKDept.CurrentBusinessEntity;
                if (ent != null)
                {
                    ent.PR_SEGMENT = pr_SegmentValue;
                }
            }
        }
        private void CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT_Shown(object sender, EventArgs e)
        {
            //List<DataGridViewRow> grdRows = new List<DataGridViewRow>();
            //foreach (DataGridViewRow r in dgvBlkDept.Rows)
            //{
            //    if (r.Cells[0].Value == null || r.Cells[0].Value.ToString() == "" || r.Cells[1].Value == null || r.Cells[1].Value.ToString() == "" && r.Cells[3].Value != null || r.Cells[3].Value.ToString() == "")
            //    {
            //        grdRows.Add(r);
            //    }
            //}
            //foreach (DataGridViewRow r in grdRows)
            //{
            //    if (!r.IsNewRow)
            //        this.dgvBlkDept.Rows.Remove(r);
            //}
        }

        #endregion

        //private void dgvBlkDept_RowLeave(object sender, DataGridViewCellEventArgs e)
        //{

        //}

        //private void dgvBlkDept_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        //{
        //    if (dgvBlkDept.Rows[e.RowIndex].Cells[0].Value == null || dgvBlkDept.Rows[e.RowIndex].Cells[0].Value.ToString() == ""
        //        || dgvBlkDept.Rows[e.RowIndex].Cells[1].Value == null || dgvBlkDept.Rows[e.RowIndex].Cells[1].Value.ToString() == ""
        //        || dgvBlkDept.Rows[e.RowIndex].Cells[3].Value == null || dgvBlkDept.Rows[e.RowIndex].Cells[3].Value.ToString() == "")
        //    {
        //        dgvBlkDept.CancelEdit();
        //    }
        //}
    
    }
}