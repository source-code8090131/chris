namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_OnePagerPersonalEntry_Training
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_OnePagerPersonalEntry_Training));
            this.pnlTraining = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvTraining = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.TR_LOC_OVRS = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TR_PROG_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TR_PROG_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TR_FTR_PROG_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TR_FTR_PROG_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TR_COUNTRY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlTraining.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTraining)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(638, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(674, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 275);
            this.panel1.Size = new System.Drawing.Size(674, 60);
            // 
            // pnlTraining
            // 
            this.pnlTraining.ConcurrentPanels = null;
            this.pnlTraining.Controls.Add(this.dgvTraining);
            this.pnlTraining.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTraining.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTraining.DependentPanels = null;
            this.pnlTraining.DisableDependentLoad = false;
            this.pnlTraining.EnableDelete = true;
            this.pnlTraining.EnableInsert = true;
            this.pnlTraining.EnableQuery = false;
            this.pnlTraining.EnableUpdate = true;
            this.pnlTraining.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TrainingPersonnelCommand";
            this.pnlTraining.Location = new System.Drawing.Point(12, 13);
            this.pnlTraining.MasterPanel = null;
            this.pnlTraining.Name = "pnlTraining";
            this.pnlTraining.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTraining.Size = new System.Drawing.Size(650, 249);
            this.pnlTraining.SPName = "CHRIS_TRAINING_ONEPAGER_MANAGER";
            this.pnlTraining.TabIndex = 12;
            // 
            // dgvTraining
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTraining.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTraining.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTraining.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TR_LOC_OVRS,
            this.TR_PROG_NAME,
            this.TR_PROG_DATE,
            this.TR_FTR_PROG_NAME,
            this.TR_FTR_PROG_DATE,
            this.TR_COUNTRY});
            this.dgvTraining.ColumnToHide = null;
            this.dgvTraining.ColumnWidth = null;
            this.dgvTraining.CustomEnabled = true;
            this.dgvTraining.DisplayColumnWrapper = null;
            this.dgvTraining.GridDefaultRow = 0;
            this.dgvTraining.Location = new System.Drawing.Point(3, 3);
            this.dgvTraining.Name = "dgvTraining";
            this.dgvTraining.ReadOnlyColumns = null;
            this.dgvTraining.RequiredColumns = null;
            this.dgvTraining.Size = new System.Drawing.Size(644, 243);
            this.dgvTraining.SkippingColumns = null;
            this.dgvTraining.TabIndex = 11;
            this.dgvTraining.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvTraining_CellValidating);
            // 
            // TR_LOC_OVRS
            // 
            this.TR_LOC_OVRS.DataPropertyName = "TR_LOC_OVRS";
            this.TR_LOC_OVRS.HeaderText = "Local/Overseas/Dev. Assign ";
            this.TR_LOC_OVRS.Items.AddRange(new object[] {
            "Local",
            "OverSeas",
            "Developmental Assignments"});
            this.TR_LOC_OVRS.MaxDropDownItems = 10;
            this.TR_LOC_OVRS.Name = "TR_LOC_OVRS";
            this.TR_LOC_OVRS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TR_LOC_OVRS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TR_LOC_OVRS.Width = 130;
            // 
            // TR_PROG_NAME
            // 
            this.TR_PROG_NAME.DataPropertyName = "TR_PROG_NAME";
            this.TR_PROG_NAME.HeaderText = "Training Program Name";
            this.TR_PROG_NAME.MaxInputLength = 20;
            this.TR_PROG_NAME.Name = "TR_PROG_NAME";
            this.TR_PROG_NAME.Width = 120;
            // 
            // TR_PROG_DATE
            // 
            this.TR_PROG_DATE.DataPropertyName = "TR_PROG_DATE";
            this.TR_PROG_DATE.HeaderText = "Training Program Date";
            this.TR_PROG_DATE.MaxInputLength = 10;
            this.TR_PROG_DATE.Name = "TR_PROG_DATE";
            this.TR_PROG_DATE.Width = 80;
            // 
            // TR_FTR_PROG_NAME
            // 
            this.TR_FTR_PROG_NAME.DataPropertyName = "TR_FTR_PROG_NAME";
            this.TR_FTR_PROG_NAME.HeaderText = "Training Future Program Name";
            this.TR_FTR_PROG_NAME.MaxInputLength = 20;
            this.TR_FTR_PROG_NAME.Name = "TR_FTR_PROG_NAME";
            this.TR_FTR_PROG_NAME.Width = 120;
            // 
            // TR_FTR_PROG_DATE
            // 
            this.TR_FTR_PROG_DATE.DataPropertyName = "TR_FTR_PROG_DATE";
            this.TR_FTR_PROG_DATE.HeaderText = "Training Future Program Date";
            this.TR_FTR_PROG_DATE.MaxInputLength = 10;
            this.TR_FTR_PROG_DATE.Name = "TR_FTR_PROG_DATE";
            this.TR_FTR_PROG_DATE.Width = 80;
            // 
            // TR_COUNTRY
            // 
            this.TR_COUNTRY.DataPropertyName = "TR_COUNTRY";
            this.TR_COUNTRY.HeaderText = "Venue";
            this.TR_COUNTRY.MaxInputLength = 30;
            this.TR_COUNTRY.Name = "TR_COUNTRY";
            // 
            // CHRIS_Personnel_OnePagerPersonalEntry_Training
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(674, 335);
            this.Controls.Add(this.pnlTraining);
            this.Name = "CHRIS_Personnel_OnePagerPersonalEntry_Training";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iCore CHRIS - One Pager Personnel Entry [Training]";
            this.TopMost = true;
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlTraining, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlTraining.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTraining)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTraining;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvTraining;
        private System.Windows.Forms.DataGridViewComboBoxColumn TR_LOC_OVRS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_PROG_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_PROG_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_FTR_PROG_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_FTR_PROG_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_COUNTRY;
    }
}