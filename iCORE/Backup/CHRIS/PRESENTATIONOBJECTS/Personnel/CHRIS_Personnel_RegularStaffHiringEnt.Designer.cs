namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_RegularStaffHiringEnt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_RegularStaffHiringEnt));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlPersonnelMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTxtbEmail = new CrplControlLibrary.SLTextBox(this.components);
            this.lbEmail = new System.Windows.Forms.Label();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLoc = new CrplControlLibrary.SLTextBox(this.components);
            this.label29 = new System.Windows.Forms.Label();
            this.pnlView = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.pnlSave = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_W_ADD_ANS = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.txt_W_VIEW_ANS = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.lbtnRepNo = new CrplControlLibrary.LookupButton(this.components);
            this.pnlDelete = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_W_DEL_ANS = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.txt_PR_EXPIRY = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_ID_ISSUE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_NEW_ANNUAL_PACK = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_CONFIRM = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_JOINING_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_CONF_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnDesgLevel = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnDesignation = new CrplControlLibrary.LookupButton(this.components);
            this.txt_PR_NEW_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnBranch = new CrplControlLibrary.LookupButton(this.components);
            this.txt_PR_CLOSE_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPersonnelNO = new CrplControlLibrary.LookupButton(this.components);
            this.txt_PR_TAX_PAID = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_TAX_INC = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_BANK_ID = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_ACCOUNT_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txt_PR_NATIONAL_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_ANNUAL_PACK = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_EMP_TYPE = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_F_GEID_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_RELIGION = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_RELIGION = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_FUNC_Title2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_FUNC_Title1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblGeid = new System.Windows.Forms.Label();
            this.lblFunctionalTitle = new System.Windows.Forms.Label();
            this.txt_PR_CATEGORY = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LEVEL = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_F_RNAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LAST_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.txtReportTo = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_FIRST_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.lblLevel = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblBranch = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblReportTo = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblPersonnelsNo = new System.Windows.Forms.Label();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_OPTION_DIS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_USER = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.pnlBlkDept = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.dgvBlkDept = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.Segment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contribution1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contribution2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contribution3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_PR_USER_IS_PRIME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_GROUP_HOSP = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_OPD = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_GROUP_LIFE = new CrplControlLibrary.SLTextBox(this.components);
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.txt_PR_LANG_6 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LANG_5 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LANG_4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LANG_3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LANG_2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LANG_1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_MARITAL = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_OLD_ID_CARD_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_SEX = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_ID_CARD_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_D_BIRTH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_PHONE2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_PHONE1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_ADD2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txt_PR_ADD1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label38 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.pnlTblBlkEmp = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.dgvBlkEmp = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.From = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.To = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrgAdd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Designation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlSplBlkPersMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_PR_BIRTH_SP = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_w_date_2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_MARRIAGE = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_NO_OF_CHILD = new CrplControlLibrary.SLTextBox(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txt_PR_SPOUSE = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlTblBLKChild = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvBlkChild = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PrcNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRCHILDNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRDATEBIRTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTblBlkEdu = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvBlkEdu = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PrEYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Degree = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_College = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_City = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_E_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTblBlkRef = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvBlkRef = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.Ref_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prPNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ref_Add1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ref_Add2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ref_Add3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label30 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlPersonnelMain.SuspendLayout();
            this.pnlView.SuspendLayout();
            this.pnlSave.SuspendLayout();
            this.pnlDelete.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.pnlBlkDept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkDept)).BeginInit();
            this.pnlTblBlkEmp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkEmp)).BeginInit();
            this.pnlSplBlkPersMain.SuspendLayout();
            this.pnlTblBLKChild.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkChild)).BeginInit();
            this.pnlTblBlkEdu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkEdu)).BeginInit();
            this.pnlTblBlkRef.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkRef)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(626, 0);
            this.txtOption.Validating += new System.ComponentModel.CancelEventHandler(this.txtOption_Validating);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(662, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 570);
            this.panel1.Size = new System.Drawing.Size(662, 60);
            // 
            // pnlPersonnelMain
            // 
            this.pnlPersonnelMain.ConcurrentPanels = null;
            this.pnlPersonnelMain.Controls.Add(this.slTxtbEmail);
            this.pnlPersonnelMain.Controls.Add(this.lbEmail);
            this.pnlPersonnelMain.Controls.Add(this.txtID);
            this.pnlPersonnelMain.Controls.Add(this.txtLoc);
            this.pnlPersonnelMain.Controls.Add(this.label29);
            this.pnlPersonnelMain.Controls.Add(this.pnlView);
            this.pnlPersonnelMain.Controls.Add(this.lbtnRepNo);
            this.pnlPersonnelMain.Controls.Add(this.pnlDelete);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_EXPIRY);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_ID_ISSUE);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_NEW_ANNUAL_PACK);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_CONFIRM);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_JOINING_DATE);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_CONF_FLAG);
            this.pnlPersonnelMain.Controls.Add(this.lbtnDesgLevel);
            this.pnlPersonnelMain.Controls.Add(this.lbtnDesignation);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_NEW_BRANCH);
            this.pnlPersonnelMain.Controls.Add(this.lbtnBranch);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_CLOSE_FLAG);
            this.pnlPersonnelMain.Controls.Add(this.lbtnPersonnelNO);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_TAX_PAID);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_TAX_INC);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_BANK_ID);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_ACCOUNT_NO);
            this.pnlPersonnelMain.Controls.Add(this.label7);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_NATIONAL_TAX);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_ANNUAL_PACK);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_EMP_TYPE);
            this.pnlPersonnelMain.Controls.Add(this.txt_F_GEID_NO);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_RELIGION);
            this.pnlPersonnelMain.Controls.Add(this.txt_W_RELIGION);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_FUNC_Title2);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_FUNC_Title1);
            this.pnlPersonnelMain.Controls.Add(this.label18);
            this.pnlPersonnelMain.Controls.Add(this.label17);
            this.pnlPersonnelMain.Controls.Add(this.label16);
            this.pnlPersonnelMain.Controls.Add(this.label15);
            this.pnlPersonnelMain.Controls.Add(this.label14);
            this.pnlPersonnelMain.Controls.Add(this.label13);
            this.pnlPersonnelMain.Controls.Add(this.label12);
            this.pnlPersonnelMain.Controls.Add(this.label11);
            this.pnlPersonnelMain.Controls.Add(this.label10);
            this.pnlPersonnelMain.Controls.Add(this.label9);
            this.pnlPersonnelMain.Controls.Add(this.label8);
            this.pnlPersonnelMain.Controls.Add(this.label2);
            this.pnlPersonnelMain.Controls.Add(this.lblGeid);
            this.pnlPersonnelMain.Controls.Add(this.lblFunctionalTitle);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_CATEGORY);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_LEVEL);
            this.pnlPersonnelMain.Controls.Add(this.txt_F_RNAME);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_LAST_NAME);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_BRANCH);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_DESIG);
            this.pnlPersonnelMain.Controls.Add(this.txtReportTo);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_FIRST_NAME);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_P_NO);
            this.pnlPersonnelMain.Controls.Add(this.lblLevel);
            this.pnlPersonnelMain.Controls.Add(this.lblName);
            this.pnlPersonnelMain.Controls.Add(this.lblLastName);
            this.pnlPersonnelMain.Controls.Add(this.lblBranch);
            this.pnlPersonnelMain.Controls.Add(this.label1);
            this.pnlPersonnelMain.Controls.Add(this.lblReportTo);
            this.pnlPersonnelMain.Controls.Add(this.lblFirstName);
            this.pnlPersonnelMain.Controls.Add(this.lblPersonnelsNo);
            this.pnlPersonnelMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPersonnelMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPersonnelMain.DependentPanels = null;
            this.pnlPersonnelMain.DisableDependentLoad = false;
            this.pnlPersonnelMain.EnableDelete = true;
            this.pnlPersonnelMain.EnableInsert = true;
            this.pnlPersonnelMain.EnableQuery = false;
            this.pnlPersonnelMain.EnableUpdate = true;
            this.pnlPersonnelMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntCommand";
            this.pnlPersonnelMain.Location = new System.Drawing.Point(3, 145);
            this.pnlPersonnelMain.MasterPanel = null;
            this.pnlPersonnelMain.Name = "pnlPersonnelMain";
            this.pnlPersonnelMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPersonnelMain.Size = new System.Drawing.Size(654, 420);
            this.pnlPersonnelMain.SPName = "CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER";
            this.pnlPersonnelMain.TabIndex = 5;
            // 
            // slTxtbEmail
            // 
            this.slTxtbEmail.AllowSpace = true;
            this.slTxtbEmail.AssociatedLookUpName = "";
            this.slTxtbEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtbEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtbEmail.ContinuationTextBox = null;
            this.slTxtbEmail.CustomEnabled = true;
            this.slTxtbEmail.DataFieldMapping = "PR_EMAIL";
            this.slTxtbEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtbEmail.GetRecordsOnUpDownKeys = false;
            this.slTxtbEmail.IsDate = false;
            this.slTxtbEmail.Location = new System.Drawing.Point(138, 399);
            this.slTxtbEmail.MaxLength = 255;
            this.slTxtbEmail.Name = "slTxtbEmail";
            this.slTxtbEmail.NumberFormat = "###,###,##0.00";
            this.slTxtbEmail.Postfix = "";
            this.slTxtbEmail.Prefix = "";
            this.slTxtbEmail.Size = new System.Drawing.Size(135, 20);
            this.slTxtbEmail.SkipValidation = false;
            this.slTxtbEmail.TabIndex = 53;
            this.slTxtbEmail.TextType = CrplControlLibrary.TextType.String;
            this.slTxtbEmail.Leave += new System.EventHandler(this.slTxtbEmail_Leave);
            // 
            // lbEmail
            // 
            this.lbEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEmail.Location = new System.Drawing.Point(35, 403);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(54, 13);
            this.lbEmail.TabIndex = 52;
            this.lbEmail.Text = "Email :";
            this.lbEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(577, 37);
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.Size = new System.Drawing.Size(57, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 9;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // txtLoc
            // 
            this.txtLoc.AllowSpace = true;
            this.txtLoc.AssociatedLookUpName = "";
            this.txtLoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLoc.ContinuationTextBox = null;
            this.txtLoc.CustomEnabled = true;
            this.txtLoc.DataFieldMapping = "LOCATION";
            this.txtLoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoc.GetRecordsOnUpDownKeys = false;
            this.txtLoc.IsDate = false;
            this.txtLoc.Location = new System.Drawing.Point(138, 40);
            this.txtLoc.Name = "txtLoc";
            this.txtLoc.NumberFormat = "###,###,##0.00";
            this.txtLoc.Postfix = "";
            this.txtLoc.Prefix = "";
            this.txtLoc.Size = new System.Drawing.Size(325, 20);
            this.txtLoc.SkipValidation = false;
            this.txtLoc.TabIndex = 8;
            this.txtLoc.TabStop = false;
            this.txtLoc.TextType = CrplControlLibrary.TextType.String;
            this.txtLoc.Leave += new System.EventHandler(this.txtLoc_Leave);
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(13, 44);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(119, 13);
            this.label29.TabIndex = 7;
            this.label29.Text = "Location :";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlView
            // 
            this.pnlView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlView.ConcurrentPanels = null;
            this.pnlView.Controls.Add(this.label20);
            this.pnlView.Controls.Add(this.textBox1);
            this.pnlView.Controls.Add(this.label21);
            this.pnlView.Controls.Add(this.pnlSave);
            this.pnlView.Controls.Add(this.txt_W_VIEW_ANS);
            this.pnlView.Controls.Add(this.label28);
            this.pnlView.DataManager = null;
            this.pnlView.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlView.DependentPanels = null;
            this.pnlView.DisableDependentLoad = false;
            this.pnlView.EnableDelete = true;
            this.pnlView.EnableInsert = true;
            this.pnlView.EnableQuery = false;
            this.pnlView.EnableUpdate = true;
            this.pnlView.EntityName = null;
            this.pnlView.Location = new System.Drawing.Point(543, 374);
            this.pnlView.MasterPanel = null;
            this.pnlView.Name = "pnlView";
            this.pnlView.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlView.Size = new System.Drawing.Size(32, 21);
            this.pnlView.SPName = null;
            this.pnlView.TabIndex = 62;
            this.pnlView.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(55, 10);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "[N]ext P. No.";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(454, 5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(1, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.TabStop = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(170, 10);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Next [P]age";
            // 
            // pnlSave
            // 
            this.pnlSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSave.ConcurrentPanels = null;
            this.pnlSave.Controls.Add(this.txt_W_ADD_ANS);
            this.pnlSave.Controls.Add(this.label19);
            this.pnlSave.DataManager = null;
            this.pnlSave.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlSave.DependentPanels = null;
            this.pnlSave.DisableDependentLoad = false;
            this.pnlSave.EnableDelete = true;
            this.pnlSave.EnableInsert = true;
            this.pnlSave.EnableQuery = false;
            this.pnlSave.EnableUpdate = true;
            this.pnlSave.EntityName = null;
            this.pnlSave.Location = new System.Drawing.Point(-1, 31);
            this.pnlSave.MasterPanel = null;
            this.pnlSave.Name = "pnlSave";
            this.pnlSave.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlSave.Size = new System.Drawing.Size(520, 29);
            this.pnlSave.SPName = null;
            this.pnlSave.TabIndex = 1;
            this.pnlSave.Visible = false;
            // 
            // txt_W_ADD_ANS
            // 
            this.txt_W_ADD_ANS.AllowSpace = true;
            this.txt_W_ADD_ANS.AssociatedLookUpName = "";
            this.txt_W_ADD_ANS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_ADD_ANS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_ADD_ANS.ContinuationTextBox = null;
            this.txt_W_ADD_ANS.CustomEnabled = true;
            this.txt_W_ADD_ANS.DataFieldMapping = "";
            this.txt_W_ADD_ANS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_ADD_ANS.GetRecordsOnUpDownKeys = false;
            this.txt_W_ADD_ANS.IsDate = false;
            this.txt_W_ADD_ANS.Location = new System.Drawing.Point(389, 3);
            this.txt_W_ADD_ANS.MaxLength = 1;
            this.txt_W_ADD_ANS.Name = "txt_W_ADD_ANS";
            this.txt_W_ADD_ANS.NumberFormat = "###,###,##0.00";
            this.txt_W_ADD_ANS.Postfix = "";
            this.txt_W_ADD_ANS.Prefix = "";
            this.txt_W_ADD_ANS.Size = new System.Drawing.Size(30, 20);
            this.txt_W_ADD_ANS.SkipValidation = false;
            this.txt_W_ADD_ANS.TabIndex = 3;
            this.txt_W_ADD_ANS.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_ADD_ANS.Validated += new System.EventHandler(this.txt_W_ADD_ANS_Validated);
            this.txt_W_ADD_ANS.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_ADD_ANS_Validating);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(62, 7);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(303, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Do You Want To Save The Above Information [Y/N]";
            // 
            // txt_W_VIEW_ANS
            // 
            this.txt_W_VIEW_ANS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_VIEW_ANS.Location = new System.Drawing.Point(391, 5);
            this.txt_W_VIEW_ANS.Name = "txt_W_VIEW_ANS";
            this.txt_W_VIEW_ANS.Size = new System.Drawing.Size(32, 20);
            this.txt_W_VIEW_ANS.TabIndex = 3;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(283, 10);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(58, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "[O]ptions";
            // 
            // lbtnRepNo
            // 
            this.lbtnRepNo.ActionLOVExists = "REP_NO_EXIST";
            this.lbtnRepNo.ActionType = "REP_NO";
            this.lbtnRepNo.ConditionalFields = "txt_PR_P_NO";
            this.lbtnRepNo.CustomEnabled = true;
            this.lbtnRepNo.DataFieldMapping = "";
            this.lbtnRepNo.DependentLovControls = "";
            this.lbtnRepNo.HiddenColumns = "PR_REP_NO";
            this.lbtnRepNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnRepNo.Image")));
            this.lbtnRepNo.LoadDependentEntities = true;
            this.lbtnRepNo.Location = new System.Drawing.Point(220, 93);
            this.lbtnRepNo.LookUpTitle = null;
            this.lbtnRepNo.Name = "lbtnRepNo";
            this.lbtnRepNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnRepNo.SkipValidationOnLeave = false;
            this.lbtnRepNo.SPName = "CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER";
            this.lbtnRepNo.TabIndex = 16;
            this.lbtnRepNo.TabStop = false;
            this.lbtnRepNo.UseVisualStyleBackColor = true;
            // 
            // pnlDelete
            // 
            this.pnlDelete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDelete.ConcurrentPanels = null;
            this.pnlDelete.Controls.Add(this.txt_W_DEL_ANS);
            this.pnlDelete.Controls.Add(this.label22);
            this.pnlDelete.DataManager = null;
            this.pnlDelete.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDelete.DependentPanels = null;
            this.pnlDelete.DisableDependentLoad = false;
            this.pnlDelete.EnableDelete = true;
            this.pnlDelete.EnableInsert = true;
            this.pnlDelete.EnableQuery = false;
            this.pnlDelete.EnableUpdate = true;
            this.pnlDelete.EntityName = null;
            this.pnlDelete.Location = new System.Drawing.Point(554, 376);
            this.pnlDelete.MasterPanel = null;
            this.pnlDelete.Name = "pnlDelete";
            this.pnlDelete.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDelete.Size = new System.Drawing.Size(21, 17);
            this.pnlDelete.SPName = null;
            this.pnlDelete.TabIndex = 63;
            this.pnlDelete.Visible = false;
            // 
            // txt_W_DEL_ANS
            // 
            this.txt_W_DEL_ANS.AllowSpace = true;
            this.txt_W_DEL_ANS.AssociatedLookUpName = "";
            this.txt_W_DEL_ANS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_DEL_ANS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_DEL_ANS.ContinuationTextBox = null;
            this.txt_W_DEL_ANS.CustomEnabled = true;
            this.txt_W_DEL_ANS.DataFieldMapping = "";
            this.txt_W_DEL_ANS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_DEL_ANS.GetRecordsOnUpDownKeys = false;
            this.txt_W_DEL_ANS.IsDate = false;
            this.txt_W_DEL_ANS.Location = new System.Drawing.Point(393, 3);
            this.txt_W_DEL_ANS.MaxLength = 1;
            this.txt_W_DEL_ANS.Name = "txt_W_DEL_ANS";
            this.txt_W_DEL_ANS.NumberFormat = "###,###,##0.00";
            this.txt_W_DEL_ANS.Postfix = "";
            this.txt_W_DEL_ANS.Prefix = "";
            this.txt_W_DEL_ANS.Size = new System.Drawing.Size(30, 20);
            this.txt_W_DEL_ANS.SkipValidation = false;
            this.txt_W_DEL_ANS.TabIndex = 4;
            this.txt_W_DEL_ANS.TabStop = false;
            this.txt_W_DEL_ANS.TextType = CrplControlLibrary.TextType.String;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(52, 5);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(315, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Do You Want To Delete The Above Information [Y/N] ";
            // 
            // txt_PR_EXPIRY
            // 
            this.txt_PR_EXPIRY.CustomEnabled = true;
            this.txt_PR_EXPIRY.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_EXPIRY.DataFieldMapping = "PR_EXPIRY";
            this.txt_PR_EXPIRY.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_EXPIRY.HasChanges = true;
            this.txt_PR_EXPIRY.Location = new System.Drawing.Point(172, 375);
            this.txt_PR_EXPIRY.Name = "txt_PR_EXPIRY";
            this.txt_PR_EXPIRY.NullValue = " ";
            this.txt_PR_EXPIRY.Size = new System.Drawing.Size(93, 20);
            this.txt_PR_EXPIRY.TabIndex = 51;
            this.txt_PR_EXPIRY.Value = new System.DateTime(2011, 1, 18, 0, 0, 0, 0);
            this.txt_PR_EXPIRY.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_EXPIRY_Validating);
            // 
            // txt_PR_ID_ISSUE
            // 
            this.txt_PR_ID_ISSUE.CustomEnabled = true;
            this.txt_PR_ID_ISSUE.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_ID_ISSUE.DataFieldMapping = "PR_ID_ISSUE";
            this.txt_PR_ID_ISSUE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_ID_ISSUE.HasChanges = true;
            this.txt_PR_ID_ISSUE.Location = new System.Drawing.Point(172, 352);
            this.txt_PR_ID_ISSUE.Name = "txt_PR_ID_ISSUE";
            this.txt_PR_ID_ISSUE.NullValue = " ";
            this.txt_PR_ID_ISSUE.Size = new System.Drawing.Size(93, 20);
            this.txt_PR_ID_ISSUE.TabIndex = 49;
            this.txt_PR_ID_ISSUE.Value = new System.DateTime(2011, 1, 18, 0, 0, 0, 0);
            this.txt_PR_ID_ISSUE.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_ID_ISSUE_Validating);
            this.txt_PR_ID_ISSUE.LostFocus += new System.EventHandler(this.txt_PR_ID_ISSUE_LostFocus);
            // 
            // txt_PR_NEW_ANNUAL_PACK
            // 
            this.txt_PR_NEW_ANNUAL_PACK.AllowSpace = true;
            this.txt_PR_NEW_ANNUAL_PACK.AssociatedLookUpName = "";
            this.txt_PR_NEW_ANNUAL_PACK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NEW_ANNUAL_PACK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NEW_ANNUAL_PACK.ContinuationTextBox = null;
            this.txt_PR_NEW_ANNUAL_PACK.CustomEnabled = true;
            this.txt_PR_NEW_ANNUAL_PACK.DataFieldMapping = "PR_NEW_ANNUAL_PACK";
            this.txt_PR_NEW_ANNUAL_PACK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NEW_ANNUAL_PACK.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NEW_ANNUAL_PACK.IsDate = false;
            this.txt_PR_NEW_ANNUAL_PACK.Location = new System.Drawing.Point(327, 309);
            this.txt_PR_NEW_ANNUAL_PACK.Name = "txt_PR_NEW_ANNUAL_PACK";
            this.txt_PR_NEW_ANNUAL_PACK.NumberFormat = "###,###,##0.00";
            this.txt_PR_NEW_ANNUAL_PACK.Postfix = "";
            this.txt_PR_NEW_ANNUAL_PACK.Prefix = "";
            this.txt_PR_NEW_ANNUAL_PACK.Size = new System.Drawing.Size(151, 20);
            this.txt_PR_NEW_ANNUAL_PACK.SkipValidation = false;
            this.txt_PR_NEW_ANNUAL_PACK.TabIndex = 45;
            this.txt_PR_NEW_ANNUAL_PACK.TabStop = false;
            this.txt_PR_NEW_ANNUAL_PACK.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_NEW_ANNUAL_PACK.Visible = false;
            // 
            // txt_PR_CONFIRM
            // 
            this.txt_PR_CONFIRM.CustomEnabled = true;
            this.txt_PR_CONFIRM.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_CONFIRM.DataFieldMapping = "PR_CONFIRM";
            this.txt_PR_CONFIRM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_CONFIRM.HasChanges = true;
            this.txt_PR_CONFIRM.IsRequired = true;
            this.txt_PR_CONFIRM.Location = new System.Drawing.Point(435, 242);
            this.txt_PR_CONFIRM.Name = "txt_PR_CONFIRM";
            this.txt_PR_CONFIRM.NullValue = " ";
            this.txt_PR_CONFIRM.Size = new System.Drawing.Size(93, 20);
            this.txt_PR_CONFIRM.TabIndex = 38;
            this.txt_PR_CONFIRM.Value = new System.DateTime(2011, 1, 18, 0, 0, 0, 0);
            this.txt_PR_CONFIRM.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_CONFIRM_Validating);
            // 
            // txt_PR_JOINING_DATE
            // 
            this.txt_PR_JOINING_DATE.CustomEnabled = true;
            this.txt_PR_JOINING_DATE.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_JOINING_DATE.DataFieldMapping = "PR_JOINING_DATE";
            this.txt_PR_JOINING_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_JOINING_DATE.HasChanges = true;
            this.txt_PR_JOINING_DATE.IsRequired = true;
            this.txt_PR_JOINING_DATE.Location = new System.Drawing.Point(138, 242);
            this.txt_PR_JOINING_DATE.Name = "txt_PR_JOINING_DATE";
            this.txt_PR_JOINING_DATE.NullValue = " ";
            this.txt_PR_JOINING_DATE.Size = new System.Drawing.Size(93, 20);
            this.txt_PR_JOINING_DATE.TabIndex = 36;
            this.txt_PR_JOINING_DATE.Value = new System.DateTime(2011, 1, 18, 0, 0, 0, 0);
            this.txt_PR_JOINING_DATE.Leave += new System.EventHandler(this.txt_PR_JOINING_DATE_Leave);
            // 
            // txt_PR_CONF_FLAG
            // 
            this.txt_PR_CONF_FLAG.AllowSpace = true;
            this.txt_PR_CONF_FLAG.AssociatedLookUpName = "";
            this.txt_PR_CONF_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_CONF_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_CONF_FLAG.ContinuationTextBox = null;
            this.txt_PR_CONF_FLAG.CustomEnabled = true;
            this.txt_PR_CONF_FLAG.DataFieldMapping = "PR_CONF_FLAG";
            this.txt_PR_CONF_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_CONF_FLAG.GetRecordsOnUpDownKeys = false;
            this.txt_PR_CONF_FLAG.IsDate = false;
            this.txt_PR_CONF_FLAG.Location = new System.Drawing.Point(602, 285);
            this.txt_PR_CONF_FLAG.Name = "txt_PR_CONF_FLAG";
            this.txt_PR_CONF_FLAG.NumberFormat = "###,###,##0.00";
            this.txt_PR_CONF_FLAG.Postfix = "";
            this.txt_PR_CONF_FLAG.Prefix = "";
            this.txt_PR_CONF_FLAG.Size = new System.Drawing.Size(32, 20);
            this.txt_PR_CONF_FLAG.SkipValidation = false;
            this.txt_PR_CONF_FLAG.TabIndex = 59;
            this.txt_PR_CONF_FLAG.TabStop = false;
            this.txt_PR_CONF_FLAG.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_CONF_FLAG.Visible = false;
            // 
            // lbtnDesgLevel
            // 
            this.lbtnDesgLevel.ActionLOVExists = "DESG_LEVEL_EXIST";
            this.lbtnDesgLevel.ActionType = "DESG_LEVEL";
            this.lbtnDesgLevel.ConditionalFields = "txt_PR_DESIG";
            this.lbtnDesgLevel.CustomEnabled = true;
            this.lbtnDesgLevel.DataFieldMapping = "";
            this.lbtnDesgLevel.DependentLovControls = "";
            this.lbtnDesgLevel.HiddenColumns = "PR_LEVEL";
            this.lbtnDesgLevel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDesgLevel.Image")));
            this.lbtnDesgLevel.LoadDependentEntities = false;
            this.lbtnDesgLevel.Location = new System.Drawing.Point(469, 120);
            this.lbtnDesgLevel.LookUpTitle = null;
            this.lbtnDesgLevel.Name = "lbtnDesgLevel";
            this.lbtnDesgLevel.Size = new System.Drawing.Size(26, 21);
            this.lbtnDesgLevel.SkipValidationOnLeave = false;
            this.lbtnDesgLevel.SPName = "CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER";
            this.lbtnDesgLevel.TabIndex = 24;
            this.lbtnDesgLevel.TabStop = false;
            this.lbtnDesgLevel.UseVisualStyleBackColor = true;
            // 
            // lbtnDesignation
            // 
            this.lbtnDesignation.ActionLOVExists = "DESIGNATION_EXIST";
            this.lbtnDesignation.ActionType = "DESIGNATION";
            this.lbtnDesignation.ConditionalFields = "txt_PR_NEW_BRANCH";
            this.lbtnDesignation.CustomEnabled = true;
            this.lbtnDesignation.DataFieldMapping = "";
            this.lbtnDesignation.DependentLovControls = "";
            this.lbtnDesignation.HiddenColumns = "PR_DESIG|PR_LEVEL";
            this.lbtnDesignation.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDesignation.Image")));
            this.lbtnDesignation.LoadDependentEntities = false;
            this.lbtnDesignation.Location = new System.Drawing.Point(193, 120);
            this.lbtnDesignation.LookUpTitle = null;
            this.lbtnDesignation.Name = "lbtnDesignation";
            this.lbtnDesignation.Size = new System.Drawing.Size(26, 21);
            this.lbtnDesignation.SkipValidationOnLeave = false;
            this.lbtnDesignation.SPName = "CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER";
            this.lbtnDesignation.TabIndex = 21;
            this.lbtnDesignation.TabStop = false;
            this.lbtnDesignation.UseVisualStyleBackColor = true;
            // 
            // txt_PR_NEW_BRANCH
            // 
            this.txt_PR_NEW_BRANCH.AllowSpace = true;
            this.txt_PR_NEW_BRANCH.AssociatedLookUpName = "";
            this.txt_PR_NEW_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NEW_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NEW_BRANCH.ContinuationTextBox = null;
            this.txt_PR_NEW_BRANCH.CustomEnabled = true;
            this.txt_PR_NEW_BRANCH.DataFieldMapping = "PR_NEW_BRANCH";
            this.txt_PR_NEW_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NEW_BRANCH.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NEW_BRANCH.IsDate = false;
            this.txt_PR_NEW_BRANCH.Location = new System.Drawing.Point(602, 334);
            this.txt_PR_NEW_BRANCH.Name = "txt_PR_NEW_BRANCH";
            this.txt_PR_NEW_BRANCH.NumberFormat = "###,###,##0.00";
            this.txt_PR_NEW_BRANCH.Postfix = "";
            this.txt_PR_NEW_BRANCH.Prefix = "";
            this.txt_PR_NEW_BRANCH.Size = new System.Drawing.Size(32, 20);
            this.txt_PR_NEW_BRANCH.SkipValidation = false;
            this.txt_PR_NEW_BRANCH.TabIndex = 61;
            this.txt_PR_NEW_BRANCH.TabStop = false;
            this.txt_PR_NEW_BRANCH.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_NEW_BRANCH.Visible = false;
            // 
            // lbtnBranch
            // 
            this.lbtnBranch.ActionLOVExists = "BranchExist";
            this.lbtnBranch.ActionType = "Branch";
            this.lbtnBranch.ConditionalFields = "";
            this.lbtnBranch.CustomEnabled = true;
            this.lbtnBranch.DataFieldMapping = "";
            this.lbtnBranch.DependentLovControls = "";
            this.lbtnBranch.HiddenColumns = "";
            this.lbtnBranch.Image = ((System.Drawing.Image)(resources.GetObject("lbtnBranch.Image")));
            this.lbtnBranch.LoadDependentEntities = false;
            this.lbtnBranch.Location = new System.Drawing.Point(469, 14);
            this.lbtnBranch.LookUpTitle = null;
            this.lbtnBranch.Name = "lbtnBranch";
            this.lbtnBranch.Size = new System.Drawing.Size(26, 21);
            this.lbtnBranch.SkipValidationOnLeave = false;
            this.lbtnBranch.SPName = "CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER";
            this.lbtnBranch.TabIndex = 5;
            this.lbtnBranch.TabStop = false;
            this.lbtnBranch.UseVisualStyleBackColor = true;
            // 
            // txt_PR_CLOSE_FLAG
            // 
            this.txt_PR_CLOSE_FLAG.AllowSpace = true;
            this.txt_PR_CLOSE_FLAG.AssociatedLookUpName = "";
            this.txt_PR_CLOSE_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_CLOSE_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_CLOSE_FLAG.ContinuationTextBox = null;
            this.txt_PR_CLOSE_FLAG.CustomEnabled = true;
            this.txt_PR_CLOSE_FLAG.DataFieldMapping = "PR_CLOSE_FLAG";
            this.txt_PR_CLOSE_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_CLOSE_FLAG.GetRecordsOnUpDownKeys = false;
            this.txt_PR_CLOSE_FLAG.IsDate = false;
            this.txt_PR_CLOSE_FLAG.Location = new System.Drawing.Point(602, 311);
            this.txt_PR_CLOSE_FLAG.Name = "txt_PR_CLOSE_FLAG";
            this.txt_PR_CLOSE_FLAG.NumberFormat = "###,###,##0.00";
            this.txt_PR_CLOSE_FLAG.Postfix = "";
            this.txt_PR_CLOSE_FLAG.Prefix = "";
            this.txt_PR_CLOSE_FLAG.Size = new System.Drawing.Size(32, 20);
            this.txt_PR_CLOSE_FLAG.SkipValidation = false;
            this.txt_PR_CLOSE_FLAG.TabIndex = 60;
            this.txt_PR_CLOSE_FLAG.TabStop = false;
            this.txt_PR_CLOSE_FLAG.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_CLOSE_FLAG.Visible = false;
            // 
            // lbtnPersonnelNO
            // 
            this.lbtnPersonnelNO.ActionLOVExists = "PERSONNELSINFOEXIST";
            this.lbtnPersonnelNO.ActionType = "PERSONNELSINFO";
            this.lbtnPersonnelNO.ConditionalFields = "";
            this.lbtnPersonnelNO.CustomEnabled = true;
            this.lbtnPersonnelNO.DataFieldMapping = "";
            this.lbtnPersonnelNO.DependentLovControls = "";
            this.lbtnPersonnelNO.HiddenColumns = resources.GetString("lbtnPersonnelNO.HiddenColumns");
            this.lbtnPersonnelNO.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnelNO.Image")));
            this.lbtnPersonnelNO.LoadDependentEntities = true;
            this.lbtnPersonnelNO.Location = new System.Drawing.Point(220, 14);
            this.lbtnPersonnelNO.LookUpTitle = null;
            this.lbtnPersonnelNO.Name = "lbtnPersonnelNO";
            this.lbtnPersonnelNO.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnelNO.SkipValidationOnLeave = false;
            this.lbtnPersonnelNO.SPName = "CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER";
            this.lbtnPersonnelNO.TabIndex = 2;
            this.lbtnPersonnelNO.TabStop = false;
            this.lbtnPersonnelNO.UseVisualStyleBackColor = true;
            // 
            // txt_PR_TAX_PAID
            // 
            this.txt_PR_TAX_PAID.AllowSpace = true;
            this.txt_PR_TAX_PAID.AssociatedLookUpName = "";
            this.txt_PR_TAX_PAID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_TAX_PAID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_TAX_PAID.ContinuationTextBox = null;
            this.txt_PR_TAX_PAID.CustomEnabled = true;
            this.txt_PR_TAX_PAID.DataFieldMapping = "PR_TAX_PAID";
            this.txt_PR_TAX_PAID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_TAX_PAID.GetRecordsOnUpDownKeys = false;
            this.txt_PR_TAX_PAID.IsDate = false;
            this.txt_PR_TAX_PAID.Location = new System.Drawing.Point(422, 375);
            this.txt_PR_TAX_PAID.MaxLength = 9;
            this.txt_PR_TAX_PAID.Name = "txt_PR_TAX_PAID";
            this.txt_PR_TAX_PAID.NumberFormat = "###,###,##0.00";
            this.txt_PR_TAX_PAID.Postfix = "";
            this.txt_PR_TAX_PAID.Prefix = "";
            this.txt_PR_TAX_PAID.Size = new System.Drawing.Size(104, 20);
            this.txt_PR_TAX_PAID.SkipValidation = false;
            this.txt_PR_TAX_PAID.TabIndex = 58;
            this.txt_PR_TAX_PAID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PR_TAX_PAID.TextType = CrplControlLibrary.TextType.Amount;
            this.txt_PR_TAX_PAID.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txt_PR_TAX_PAID_PreviewKeyDown);
            this.txt_PR_TAX_PAID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_PR_TAX_PAID_KeyPress);
            this.txt_PR_TAX_PAID.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_TAX_PAID_Validating);
            // 
            // txt_PR_TAX_INC
            // 
            this.txt_PR_TAX_INC.AllowSpace = true;
            this.txt_PR_TAX_INC.AssociatedLookUpName = "";
            this.txt_PR_TAX_INC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_TAX_INC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_TAX_INC.ContinuationTextBox = null;
            this.txt_PR_TAX_INC.CustomEnabled = true;
            this.txt_PR_TAX_INC.DataFieldMapping = "PR_TAX_INC";
            this.txt_PR_TAX_INC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_TAX_INC.GetRecordsOnUpDownKeys = false;
            this.txt_PR_TAX_INC.IsDate = false;
            this.txt_PR_TAX_INC.Location = new System.Drawing.Point(422, 352);
            this.txt_PR_TAX_INC.MaxLength = 9;
            this.txt_PR_TAX_INC.Name = "txt_PR_TAX_INC";
            this.txt_PR_TAX_INC.NumberFormat = "###,###,##0.00";
            this.txt_PR_TAX_INC.Postfix = "";
            this.txt_PR_TAX_INC.Prefix = "";
            this.txt_PR_TAX_INC.Size = new System.Drawing.Size(104, 20);
            this.txt_PR_TAX_INC.SkipValidation = false;
            this.txt_PR_TAX_INC.TabIndex = 56;
            this.txt_PR_TAX_INC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PR_TAX_INC.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txt_PR_BANK_ID
            // 
            this.txt_PR_BANK_ID.AllowSpace = true;
            this.txt_PR_BANK_ID.AssociatedLookUpName = "";
            this.txt_PR_BANK_ID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_BANK_ID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_BANK_ID.ContinuationTextBox = null;
            this.txt_PR_BANK_ID.CustomEnabled = true;
            this.txt_PR_BANK_ID.DataFieldMapping = "PR_BANK_ID";
            this.txt_PR_BANK_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_BANK_ID.GetRecordsOnUpDownKeys = false;
            this.txt_PR_BANK_ID.IsDate = false;
            this.txt_PR_BANK_ID.Location = new System.Drawing.Point(176, 328);
            this.txt_PR_BANK_ID.MaxLength = 1;
            this.txt_PR_BANK_ID.Name = "txt_PR_BANK_ID";
            this.txt_PR_BANK_ID.NumberFormat = "###,###,##0.00";
            this.txt_PR_BANK_ID.Postfix = "";
            this.txt_PR_BANK_ID.Prefix = "";
            this.txt_PR_BANK_ID.Size = new System.Drawing.Size(32, 20);
            this.txt_PR_BANK_ID.SkipValidation = false;
            this.txt_PR_BANK_ID.TabIndex = 47;
            this.txt_PR_BANK_ID.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_BANK_ID.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_BANK_ID_Validating);
            // 
            // txt_PR_ACCOUNT_NO
            // 
            this.txt_PR_ACCOUNT_NO.AllowSpace = true;
            this.txt_PR_ACCOUNT_NO.AssociatedLookUpName = "";
            this.txt_PR_ACCOUNT_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ACCOUNT_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ACCOUNT_NO.ContinuationTextBox = null;
            this.txt_PR_ACCOUNT_NO.CustomEnabled = true;
            this.txt_PR_ACCOUNT_NO.DataFieldMapping = "PR_ACCOUNT_NO";
            this.txt_PR_ACCOUNT_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ACCOUNT_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ACCOUNT_NO.IsDate = false;
            this.txt_PR_ACCOUNT_NO.Location = new System.Drawing.Point(138, 292);
            this.txt_PR_ACCOUNT_NO.MaxLength = 24;
            this.txt_PR_ACCOUNT_NO.Name = "txt_PR_ACCOUNT_NO";
            this.txt_PR_ACCOUNT_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_ACCOUNT_NO.Postfix = "";
            this.txt_PR_ACCOUNT_NO.Prefix = "";
            this.txt_PR_ACCOUNT_NO.Size = new System.Drawing.Size(135, 20);
            this.txt_PR_ACCOUNT_NO.SkipValidation = false;
            this.txt_PR_ACCOUNT_NO.TabIndex = 44;
            this.txt_PR_ACCOUNT_NO.TextType = CrplControlLibrary.TextType.StringAndDigit;
            this.txt_PR_ACCOUNT_NO.Enter += new System.EventHandler(this.txt_PR_ACCOUNT_NO_Enter);
            this.txt_PR_ACCOUNT_NO.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_ACCOUNT_NO_Validating);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(52, 296);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "Account No :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_PR_NATIONAL_TAX
            // 
            this.txt_PR_NATIONAL_TAX.AllowSpace = true;
            this.txt_PR_NATIONAL_TAX.AssociatedLookUpName = "";
            this.txt_PR_NATIONAL_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NATIONAL_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NATIONAL_TAX.ContinuationTextBox = null;
            this.txt_PR_NATIONAL_TAX.CustomEnabled = true;
            this.txt_PR_NATIONAL_TAX.DataFieldMapping = "PR_NATIONAL_TAX";
            this.txt_PR_NATIONAL_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NATIONAL_TAX.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NATIONAL_TAX.IsDate = false;
            this.txt_PR_NATIONAL_TAX.Location = new System.Drawing.Point(434, 267);
            this.txt_PR_NATIONAL_TAX.MaxLength = 18;
            this.txt_PR_NATIONAL_TAX.Name = "txt_PR_NATIONAL_TAX";
            this.txt_PR_NATIONAL_TAX.NumberFormat = "###,###,##0.00";
            this.txt_PR_NATIONAL_TAX.Postfix = "";
            this.txt_PR_NATIONAL_TAX.Prefix = "";
            this.txt_PR_NATIONAL_TAX.Size = new System.Drawing.Size(141, 20);
            this.txt_PR_NATIONAL_TAX.SkipValidation = false;
            this.txt_PR_NATIONAL_TAX.TabIndex = 42;
            this.txt_PR_NATIONAL_TAX.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_NATIONAL_TAX.Leave += new System.EventHandler(this.txt_PR_NATIONAL_TAX_Leave);
            this.txt_PR_NATIONAL_TAX.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_NATIONAL_TAX_Validating);
            // 
            // txt_PR_ANNUAL_PACK
            // 
            this.txt_PR_ANNUAL_PACK.AllowSpace = true;
            this.txt_PR_ANNUAL_PACK.AssociatedLookUpName = "";
            this.txt_PR_ANNUAL_PACK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ANNUAL_PACK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ANNUAL_PACK.ContinuationTextBox = null;
            this.txt_PR_ANNUAL_PACK.CustomEnabled = true;
            this.txt_PR_ANNUAL_PACK.DataFieldMapping = "PR_ANNUAL_PACK";
            this.txt_PR_ANNUAL_PACK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ANNUAL_PACK.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ANNUAL_PACK.IsDate = false;
            this.txt_PR_ANNUAL_PACK.IsRequired = true;
            this.txt_PR_ANNUAL_PACK.Location = new System.Drawing.Point(138, 267);
            this.txt_PR_ANNUAL_PACK.MaxLength = 11;
            this.txt_PR_ANNUAL_PACK.Name = "txt_PR_ANNUAL_PACK";
            this.txt_PR_ANNUAL_PACK.NumberFormat = "###,###,##0";
            this.txt_PR_ANNUAL_PACK.Postfix = "";
            this.txt_PR_ANNUAL_PACK.Prefix = "";
            this.txt_PR_ANNUAL_PACK.Size = new System.Drawing.Size(93, 20);
            this.txt_PR_ANNUAL_PACK.SkipValidation = false;
            this.txt_PR_ANNUAL_PACK.TabIndex = 40;
            this.txt_PR_ANNUAL_PACK.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PR_ANNUAL_PACK.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_PR_ANNUAL_PACK.Leave += new System.EventHandler(this.txt_PR_ANNUAL_PACK_Leave);
            this.txt_PR_ANNUAL_PACK.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_ANNUAL_PACK_Validating);
            // 
            // txt_PR_EMP_TYPE
            // 
            this.txt_PR_EMP_TYPE.AllowSpace = true;
            this.txt_PR_EMP_TYPE.AssociatedLookUpName = "";
            this.txt_PR_EMP_TYPE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_EMP_TYPE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_EMP_TYPE.ContinuationTextBox = null;
            this.txt_PR_EMP_TYPE.CustomEnabled = true;
            this.txt_PR_EMP_TYPE.DataFieldMapping = "PR_EMP_TYPE";
            this.txt_PR_EMP_TYPE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_EMP_TYPE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_EMP_TYPE.IsDate = false;
            this.txt_PR_EMP_TYPE.Location = new System.Drawing.Point(435, 190);
            this.txt_PR_EMP_TYPE.MaxLength = 2;
            this.txt_PR_EMP_TYPE.Name = "txt_PR_EMP_TYPE";
            this.txt_PR_EMP_TYPE.NumberFormat = "###,###,##0.00";
            this.txt_PR_EMP_TYPE.Postfix = "";
            this.txt_PR_EMP_TYPE.Prefix = "";
            this.txt_PR_EMP_TYPE.Size = new System.Drawing.Size(66, 20);
            this.txt_PR_EMP_TYPE.SkipValidation = false;
            this.txt_PR_EMP_TYPE.TabIndex = 31;
            this.txt_PR_EMP_TYPE.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_EMP_TYPE.Leave += new System.EventHandler(this.txt_PR_EMP_TYPE_Leave);
            this.txt_PR_EMP_TYPE.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_EMP_TYPE_Validating);
            // 
            // txt_F_GEID_NO
            // 
            this.txt_F_GEID_NO.AllowSpace = true;
            this.txt_F_GEID_NO.AssociatedLookUpName = "";
            this.txt_F_GEID_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_F_GEID_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_F_GEID_NO.ContinuationTextBox = null;
            this.txt_F_GEID_NO.CustomEnabled = true;
            this.txt_F_GEID_NO.DataFieldMapping = "F_GEID_NO";
            this.txt_F_GEID_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_F_GEID_NO.GetRecordsOnUpDownKeys = false;
            this.txt_F_GEID_NO.IsDate = false;
            this.txt_F_GEID_NO.Location = new System.Drawing.Point(435, 164);
            this.txt_F_GEID_NO.Name = "txt_F_GEID_NO";
            this.txt_F_GEID_NO.NumberFormat = "###,###,##0.00";
            this.txt_F_GEID_NO.Postfix = "";
            this.txt_F_GEID_NO.Prefix = "";
            this.txt_F_GEID_NO.Size = new System.Drawing.Size(91, 20);
            this.txt_F_GEID_NO.SkipValidation = false;
            this.txt_F_GEID_NO.TabIndex = 28;
            this.txt_F_GEID_NO.TabStop = false;
            this.txt_F_GEID_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_RELIGION
            // 
            this.txt_PR_RELIGION.AllowSpace = true;
            this.txt_PR_RELIGION.AssociatedLookUpName = "";
            this.txt_PR_RELIGION.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_RELIGION.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_RELIGION.ContinuationTextBox = null;
            this.txt_PR_RELIGION.CustomEnabled = true;
            this.txt_PR_RELIGION.DataFieldMapping = "PR_RELIGION";
            this.txt_PR_RELIGION.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_RELIGION.GetRecordsOnUpDownKeys = false;
            this.txt_PR_RELIGION.IsDate = false;
            this.txt_PR_RELIGION.Location = new System.Drawing.Point(172, 216);
            this.txt_PR_RELIGION.Name = "txt_PR_RELIGION";
            this.txt_PR_RELIGION.NumberFormat = "###,###,##0.00";
            this.txt_PR_RELIGION.Postfix = "";
            this.txt_PR_RELIGION.Prefix = "";
            this.txt_PR_RELIGION.ReadOnly = true;
            this.txt_PR_RELIGION.Size = new System.Drawing.Size(101, 20);
            this.txt_PR_RELIGION.SkipValidation = false;
            this.txt_PR_RELIGION.TabIndex = 34;
            this.txt_PR_RELIGION.TabStop = false;
            this.txt_PR_RELIGION.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_RELIGION.TextChanged += new System.EventHandler(this.txt_PR_RELIGION_TextChanged);
            // 
            // txt_W_RELIGION
            // 
            this.txt_W_RELIGION.AllowSpace = true;
            this.txt_W_RELIGION.AssociatedLookUpName = "";
            this.txt_W_RELIGION.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_RELIGION.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_RELIGION.ContinuationTextBox = null;
            this.txt_W_RELIGION.CustomEnabled = true;
            this.txt_W_RELIGION.DataFieldMapping = "W_RELIGION";
            this.txt_W_RELIGION.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_RELIGION.GetRecordsOnUpDownKeys = false;
            this.txt_W_RELIGION.IsDate = false;
            this.txt_W_RELIGION.Location = new System.Drawing.Point(138, 216);
            this.txt_W_RELIGION.MaxLength = 1;
            this.txt_W_RELIGION.Name = "txt_W_RELIGION";
            this.txt_W_RELIGION.NumberFormat = "###,###,##0.00";
            this.txt_W_RELIGION.Postfix = "";
            this.txt_W_RELIGION.Prefix = "";
            this.txt_W_RELIGION.Size = new System.Drawing.Size(28, 20);
            this.txt_W_RELIGION.SkipValidation = false;
            this.txt_W_RELIGION.TabIndex = 33;
            this.txt_W_RELIGION.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_RELIGION.Leave += new System.EventHandler(this.txt_W_RELIGION_Leave);
            this.txt_W_RELIGION.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_RELIGION_Validating);
            // 
            // txt_PR_FUNC_Title2
            // 
            this.txt_PR_FUNC_Title2.AllowSpace = true;
            this.txt_PR_FUNC_Title2.AssociatedLookUpName = "";
            this.txt_PR_FUNC_Title2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_FUNC_Title2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_FUNC_Title2.ContinuationTextBox = null;
            this.txt_PR_FUNC_Title2.CustomEnabled = true;
            this.txt_PR_FUNC_Title2.DataFieldMapping = "PR_FUNC_TITTLE2";
            this.txt_PR_FUNC_Title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_FUNC_Title2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_FUNC_Title2.IsDate = false;
            this.txt_PR_FUNC_Title2.Location = new System.Drawing.Point(138, 190);
            this.txt_PR_FUNC_Title2.MaxLength = 30;
            this.txt_PR_FUNC_Title2.Name = "txt_PR_FUNC_Title2";
            this.txt_PR_FUNC_Title2.NumberFormat = "###,###,##0.00";
            this.txt_PR_FUNC_Title2.Postfix = "";
            this.txt_PR_FUNC_Title2.Prefix = "";
            this.txt_PR_FUNC_Title2.Size = new System.Drawing.Size(221, 20);
            this.txt_PR_FUNC_Title2.SkipValidation = false;
            this.txt_PR_FUNC_Title2.TabIndex = 29;
            this.txt_PR_FUNC_Title2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_FUNC_Title1
            // 
            this.txt_PR_FUNC_Title1.AllowSpace = true;
            this.txt_PR_FUNC_Title1.AssociatedLookUpName = "";
            this.txt_PR_FUNC_Title1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_FUNC_Title1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_FUNC_Title1.ContinuationTextBox = null;
            this.txt_PR_FUNC_Title1.CustomEnabled = true;
            this.txt_PR_FUNC_Title1.DataFieldMapping = "PR_FUNC_TITTLE1";
            this.txt_PR_FUNC_Title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_FUNC_Title1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_FUNC_Title1.IsDate = false;
            this.txt_PR_FUNC_Title1.Location = new System.Drawing.Point(138, 164);
            this.txt_PR_FUNC_Title1.MaxLength = 30;
            this.txt_PR_FUNC_Title1.Name = "txt_PR_FUNC_Title1";
            this.txt_PR_FUNC_Title1.NumberFormat = "###,###,##0.00";
            this.txt_PR_FUNC_Title1.Postfix = "";
            this.txt_PR_FUNC_Title1.Prefix = "";
            this.txt_PR_FUNC_Title1.Size = new System.Drawing.Size(221, 20);
            this.txt_PR_FUNC_Title1.SkipValidation = false;
            this.txt_PR_FUNC_Title1.TabIndex = 26;
            this.txt_PR_FUNC_Title1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(341, 379);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 13);
            this.label18.TabIndex = 57;
            this.label18.Text = "Tax Paid";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(307, 356);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 13);
            this.label17.TabIndex = 55;
            this.label17.Text = "Taxable Income";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(336, 332);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(215, 18);
            this.label16.TabIndex = 54;
            this.label16.Text = "Previous Employment Tax Info";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(32, 379);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(139, 13);
            this.label15.TabIndex = 50;
            this.label15.Text = "ID Card Expiry Date :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(20, 356);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(151, 13);
            this.label14.TabIndex = 48;
            this.label14.Text = "ID Card Issue Date :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(2, 332);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(169, 13);
            this.label13.TabIndex = 46;
            this.label13.Text = "Bank ID Card Issued [Y/N] :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(304, 271);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(117, 13);
            this.label12.TabIndex = 41;
            this.label12.Text = "National Tax No. :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(1, 271);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(134, 13);
            this.label11.TabIndex = 39;
            this.label11.Text = "Annual Package(Rs) :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(336, 246);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 13);
            this.label10.TabIndex = 37;
            this.label10.Text = "Confirmation :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 246);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 13);
            this.label9.TabIndex = 35;
            this.label9.Text = "Joining Date :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(370, 194);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "FT/PT";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 220);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Religion :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGeid
            // 
            this.lblGeid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGeid.Location = new System.Drawing.Point(358, 168);
            this.lblGeid.Name = "lblGeid";
            this.lblGeid.Size = new System.Drawing.Size(68, 13);
            this.lblGeid.TabIndex = 27;
            this.lblGeid.Text = "Geid No.";
            this.lblGeid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFunctionalTitle
            // 
            this.lblFunctionalTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFunctionalTitle.Location = new System.Drawing.Point(13, 168);
            this.lblFunctionalTitle.Name = "lblFunctionalTitle";
            this.lblFunctionalTitle.Size = new System.Drawing.Size(122, 13);
            this.lblFunctionalTitle.TabIndex = 25;
            this.lblFunctionalTitle.Text = "Functional Title :";
            this.lblFunctionalTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_PR_CATEGORY
            // 
            this.txt_PR_CATEGORY.AllowSpace = true;
            this.txt_PR_CATEGORY.AssociatedLookUpName = "";
            this.txt_PR_CATEGORY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_CATEGORY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_CATEGORY.ContinuationTextBox = null;
            this.txt_PR_CATEGORY.CustomEnabled = true;
            this.txt_PR_CATEGORY.DataFieldMapping = "PR_CATEGORY";
            this.txt_PR_CATEGORY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_CATEGORY.GetRecordsOnUpDownKeys = false;
            this.txt_PR_CATEGORY.IsDate = false;
            this.txt_PR_CATEGORY.Location = new System.Drawing.Point(577, 13);
            this.txt_PR_CATEGORY.Name = "txt_PR_CATEGORY";
            this.txt_PR_CATEGORY.NumberFormat = "###,###,##0.00";
            this.txt_PR_CATEGORY.Postfix = "";
            this.txt_PR_CATEGORY.Prefix = "";
            this.txt_PR_CATEGORY.ReadOnly = true;
            this.txt_PR_CATEGORY.Size = new System.Drawing.Size(57, 20);
            this.txt_PR_CATEGORY.SkipValidation = false;
            this.txt_PR_CATEGORY.TabIndex = 6;
            this.txt_PR_CATEGORY.TabStop = false;
            this.txt_PR_CATEGORY.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_LEVEL
            // 
            this.txt_PR_LEVEL.AllowSpace = true;
            this.txt_PR_LEVEL.AssociatedLookUpName = "lbtnDesgLevel";
            this.txt_PR_LEVEL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LEVEL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LEVEL.ContinuationTextBox = null;
            this.txt_PR_LEVEL.CustomEnabled = true;
            this.txt_PR_LEVEL.DataFieldMapping = "PR_LEVEL";
            this.txt_PR_LEVEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LEVEL.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LEVEL.IsDate = false;
            this.txt_PR_LEVEL.IsRequired = true;
            this.txt_PR_LEVEL.Location = new System.Drawing.Point(422, 120);
            this.txt_PR_LEVEL.Name = "txt_PR_LEVEL";
            this.txt_PR_LEVEL.NumberFormat = "###,###,##0.00";
            this.txt_PR_LEVEL.Postfix = "";
            this.txt_PR_LEVEL.Prefix = "";
            this.txt_PR_LEVEL.Size = new System.Drawing.Size(41, 20);
            this.txt_PR_LEVEL.SkipValidation = false;
            this.txt_PR_LEVEL.TabIndex = 23;
            this.txt_PR_LEVEL.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_LEVEL.Leave += new System.EventHandler(this.txt_PR_LEVEL_Leave);
            this.txt_PR_LEVEL.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_LEVEL_Validating);
            // 
            // txt_F_RNAME
            // 
            this.txt_F_RNAME.AllowSpace = true;
            this.txt_F_RNAME.AssociatedLookUpName = "";
            this.txt_F_RNAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_F_RNAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_F_RNAME.ContinuationTextBox = null;
            this.txt_F_RNAME.CustomEnabled = true;
            this.txt_F_RNAME.DataFieldMapping = "NAME";
            this.txt_F_RNAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_F_RNAME.GetRecordsOnUpDownKeys = false;
            this.txt_F_RNAME.IsDate = false;
            this.txt_F_RNAME.Location = new System.Drawing.Point(304, 93);
            this.txt_F_RNAME.MaxLength = 20;
            this.txt_F_RNAME.Name = "txt_F_RNAME";
            this.txt_F_RNAME.NumberFormat = "###,###,##0.00";
            this.txt_F_RNAME.Postfix = "";
            this.txt_F_RNAME.Prefix = "";
            this.txt_F_RNAME.ReadOnly = true;
            this.txt_F_RNAME.Size = new System.Drawing.Size(297, 20);
            this.txt_F_RNAME.SkipValidation = false;
            this.txt_F_RNAME.TabIndex = 18;
            this.txt_F_RNAME.TabStop = false;
            this.txt_F_RNAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_LAST_NAME
            // 
            this.txt_PR_LAST_NAME.AllowSpace = true;
            this.txt_PR_LAST_NAME.AssociatedLookUpName = "";
            this.txt_PR_LAST_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LAST_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LAST_NAME.ContinuationTextBox = null;
            this.txt_PR_LAST_NAME.CustomEnabled = true;
            this.txt_PR_LAST_NAME.DataFieldMapping = "PR_LAST_NAME";
            this.txt_PR_LAST_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LAST_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LAST_NAME.IsDate = false;
            this.txt_PR_LAST_NAME.Location = new System.Drawing.Point(422, 67);
            this.txt_PR_LAST_NAME.MaxLength = 20;
            this.txt_PR_LAST_NAME.Name = "txt_PR_LAST_NAME";
            this.txt_PR_LAST_NAME.NumberFormat = "###,###,##0.00";
            this.txt_PR_LAST_NAME.Postfix = "";
            this.txt_PR_LAST_NAME.Prefix = "";
            this.txt_PR_LAST_NAME.Size = new System.Drawing.Size(180, 20);
            this.txt_PR_LAST_NAME.SkipValidation = false;
            this.txt_PR_LAST_NAME.TabIndex = 13;
            this.txt_PR_LAST_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_BRANCH
            // 
            this.txt_PR_BRANCH.AllowSpace = true;
            this.txt_PR_BRANCH.AssociatedLookUpName = "lbtnBranch";
            this.txt_PR_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_BRANCH.ContinuationTextBox = null;
            this.txt_PR_BRANCH.CustomEnabled = true;
            this.txt_PR_BRANCH.DataFieldMapping = "PR_BRANCH";
            this.txt_PR_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_BRANCH.GetRecordsOnUpDownKeys = false;
            this.txt_PR_BRANCH.IsDate = false;
            this.txt_PR_BRANCH.Location = new System.Drawing.Point(422, 14);
            this.txt_PR_BRANCH.MaxLength = 3;
            this.txt_PR_BRANCH.Name = "txt_PR_BRANCH";
            this.txt_PR_BRANCH.NumberFormat = "###,###,##0.00";
            this.txt_PR_BRANCH.Postfix = "";
            this.txt_PR_BRANCH.Prefix = "";
            this.txt_PR_BRANCH.Size = new System.Drawing.Size(41, 20);
            this.txt_PR_BRANCH.SkipValidation = false;
            this.txt_PR_BRANCH.TabIndex = 4;
            this.txt_PR_BRANCH.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_BRANCH.Leave += new System.EventHandler(this.txt_PR_BRANCH_Leave);
            this.txt_PR_BRANCH.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_BRANCH_Validating);
            // 
            // txt_PR_DESIG
            // 
            this.txt_PR_DESIG.AllowSpace = true;
            this.txt_PR_DESIG.AssociatedLookUpName = "lbtnDesignation";
            this.txt_PR_DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_DESIG.ContinuationTextBox = null;
            this.txt_PR_DESIG.CustomEnabled = true;
            this.txt_PR_DESIG.DataFieldMapping = "PR_DESIG";
            this.txt_PR_DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_DESIG.GetRecordsOnUpDownKeys = false;
            this.txt_PR_DESIG.IsDate = false;
            this.txt_PR_DESIG.IsRequired = true;
            this.txt_PR_DESIG.Location = new System.Drawing.Point(138, 120);
            this.txt_PR_DESIG.MaxLength = 3;
            this.txt_PR_DESIG.Name = "txt_PR_DESIG";
            this.txt_PR_DESIG.NumberFormat = "###,###,##0.00";
            this.txt_PR_DESIG.Postfix = "";
            this.txt_PR_DESIG.Prefix = "";
            this.txt_PR_DESIG.Size = new System.Drawing.Size(49, 20);
            this.txt_PR_DESIG.SkipValidation = false;
            this.txt_PR_DESIG.TabIndex = 20;
            this.txt_PR_DESIG.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_DESIG.Leave += new System.EventHandler(this.txt_PR_DESIG_Leave);
            this.txt_PR_DESIG.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_DESIG_Validating);
            // 
            // txtReportTo
            // 
            this.txtReportTo.AllowSpace = true;
            this.txtReportTo.AssociatedLookUpName = "lbtnRepNo";
            this.txtReportTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReportTo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReportTo.ContinuationTextBox = null;
            this.txtReportTo.CustomEnabled = true;
            this.txtReportTo.DataFieldMapping = "PR_REP_NO";
            this.txtReportTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReportTo.GetRecordsOnUpDownKeys = false;
            this.txtReportTo.IsDate = false;
            this.txtReportTo.Location = new System.Drawing.Point(138, 93);
            this.txtReportTo.MaxLength = 6;
            this.txtReportTo.Name = "txtReportTo";
            this.txtReportTo.NumberFormat = "###,###,##0.00";
            this.txtReportTo.Postfix = "";
            this.txtReportTo.Prefix = "";
            this.txtReportTo.Size = new System.Drawing.Size(76, 20);
            this.txtReportTo.SkipValidation = false;
            this.txtReportTo.TabIndex = 15;
            this.txtReportTo.TextType = CrplControlLibrary.TextType.Integer;
            this.txtReportTo.Enter += new System.EventHandler(this.txtReportTo_Enter);
            this.txtReportTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtReportTo_Validating);
            // 
            // txt_PR_FIRST_NAME
            // 
            this.txt_PR_FIRST_NAME.AllowSpace = true;
            this.txt_PR_FIRST_NAME.AssociatedLookUpName = "";
            this.txt_PR_FIRST_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_FIRST_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_FIRST_NAME.ContinuationTextBox = null;
            this.txt_PR_FIRST_NAME.CustomEnabled = true;
            this.txt_PR_FIRST_NAME.DataFieldMapping = "PR_FIRST_NAME";
            this.txt_PR_FIRST_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_FIRST_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_FIRST_NAME.IsDate = false;
            this.txt_PR_FIRST_NAME.IsRequired = true;
            this.txt_PR_FIRST_NAME.Location = new System.Drawing.Point(138, 67);
            this.txt_PR_FIRST_NAME.MaxLength = 20;
            this.txt_PR_FIRST_NAME.Name = "txt_PR_FIRST_NAME";
            this.txt_PR_FIRST_NAME.NumberFormat = "###,###,##0.00";
            this.txt_PR_FIRST_NAME.Postfix = "";
            this.txt_PR_FIRST_NAME.Prefix = "";
            this.txt_PR_FIRST_NAME.Size = new System.Drawing.Size(186, 20);
            this.txt_PR_FIRST_NAME.SkipValidation = false;
            this.txt_PR_FIRST_NAME.TabIndex = 11;
            this.txt_PR_FIRST_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_P_NO
            // 
            this.txt_PR_P_NO.AllowSpace = true;
            this.txt_PR_P_NO.AssociatedLookUpName = "lbtnPersonnelNO";
            this.txt_PR_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_P_NO.ContinuationTextBox = null;
            this.txt_PR_P_NO.CustomEnabled = true;
            this.txt_PR_P_NO.DataFieldMapping = "PR_P_NO";
            this.txt_PR_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_P_NO.IsDate = false;
            this.txt_PR_P_NO.Location = new System.Drawing.Point(138, 14);
            this.txt_PR_P_NO.MaxLength = 6;
            this.txt_PR_P_NO.Name = "txt_PR_P_NO";
            this.txt_PR_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_P_NO.Postfix = "";
            this.txt_PR_P_NO.Prefix = "";
            this.txt_PR_P_NO.Size = new System.Drawing.Size(76, 20);
            this.txt_PR_P_NO.SkipValidation = false;
            this.txt_PR_P_NO.TabIndex = 1;
            this.txt_PR_P_NO.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_PR_P_NO.Leave += new System.EventHandler(this.txt_PR_P_NO_Leave);
            this.txt_PR_P_NO.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PR_P_NO_Validating);
            // 
            // lblLevel
            // 
            this.lblLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel.Location = new System.Drawing.Point(371, 124);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(46, 13);
            this.lblLevel.TabIndex = 22;
            this.lblLevel.Text = "Level";
            this.lblLevel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblName
            // 
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(259, 97);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 13);
            this.lblName.TabIndex = 17;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLastName
            // 
            this.lblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.Location = new System.Drawing.Point(348, 71);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(69, 13);
            this.lblLastName.TabIndex = 12;
            this.lblLastName.Text = "Last Name :";
            this.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBranch
            // 
            this.lblBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBranch.Location = new System.Drawing.Point(366, 18);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(51, 13);
            this.lblBranch.TabIndex = 3;
            this.lblBranch.Text = "Branch";
            this.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Designation Code :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblReportTo
            // 
            this.lblReportTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReportTo.Location = new System.Drawing.Point(27, 97);
            this.lblReportTo.Name = "lblReportTo";
            this.lblReportTo.Size = new System.Drawing.Size(108, 13);
            this.lblReportTo.TabIndex = 14;
            this.lblReportTo.Text = "Report To :";
            this.lblReportTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFirstName
            // 
            this.lblFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstName.Location = new System.Drawing.Point(19, 71);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(116, 13);
            this.lblFirstName.TabIndex = 10;
            this.lblFirstName.Text = "First Name :";
            this.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPersonnelsNo
            // 
            this.lblPersonnelsNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonnelsNo.Location = new System.Drawing.Point(16, 18);
            this.lblPersonnelsNo.Name = "lblPersonnelsNo";
            this.lblPersonnelsNo.Size = new System.Drawing.Size(119, 13);
            this.lblPersonnelsNo.TabIndex = 0;
            this.lblPersonnelsNo.Text = "Personnel No :";
            this.lblPersonnelsNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txt_W_OPTION_DIS);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txt_W_USER);
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.lblLocation);
            this.pnlHead.Controls.Add(this.lblUser);
            this.pnlHead.Location = new System.Drawing.Point(12, 74);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(645, 68);
            this.pnlHead.TabIndex = 3;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = false;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(545, 30);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(89, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 9;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_OPTION_DIS
            // 
            this.txt_W_OPTION_DIS.AllowSpace = true;
            this.txt_W_OPTION_DIS.AssociatedLookUpName = "";
            this.txt_W_OPTION_DIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OPTION_DIS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OPTION_DIS.ContinuationTextBox = null;
            this.txt_W_OPTION_DIS.CustomEnabled = false;
            this.txt_W_OPTION_DIS.DataFieldMapping = "";
            this.txt_W_OPTION_DIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OPTION_DIS.GetRecordsOnUpDownKeys = false;
            this.txt_W_OPTION_DIS.IsDate = false;
            this.txt_W_OPTION_DIS.Location = new System.Drawing.Point(545, 8);
            this.txt_W_OPTION_DIS.Name = "txt_W_OPTION_DIS";
            this.txt_W_OPTION_DIS.NumberFormat = "###,###,##0.00";
            this.txt_W_OPTION_DIS.Postfix = "";
            this.txt_W_OPTION_DIS.Prefix = "";
            this.txt_W_OPTION_DIS.ReadOnly = true;
            this.txt_W_OPTION_DIS.Size = new System.Drawing.Size(57, 20);
            this.txt_W_OPTION_DIS.SkipValidation = false;
            this.txt_W_OPTION_DIS.TabIndex = 7;
            this.txt_W_OPTION_DIS.TabStop = false;
            this.txt_W_OPTION_DIS.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = false;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(81, 31);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(100, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 3;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_USER
            // 
            this.txt_W_USER.AllowSpace = true;
            this.txt_W_USER.AssociatedLookUpName = "";
            this.txt_W_USER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_USER.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_USER.ContinuationTextBox = null;
            this.txt_W_USER.CustomEnabled = false;
            this.txt_W_USER.DataFieldMapping = "";
            this.txt_W_USER.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_USER.GetRecordsOnUpDownKeys = false;
            this.txt_W_USER.IsDate = false;
            this.txt_W_USER.Location = new System.Drawing.Point(81, 8);
            this.txt_W_USER.Name = "txt_W_USER";
            this.txt_W_USER.NumberFormat = "###,###,##0.00";
            this.txt_W_USER.Postfix = "";
            this.txt_W_USER.Prefix = "";
            this.txt_W_USER.ReadOnly = true;
            this.txt_W_USER.Size = new System.Drawing.Size(100, 20);
            this.txt_W_USER.SkipValidation = false;
            this.txt_W_USER.TabIndex = 1;
            this.txt_W_USER.TabStop = false;
            this.txt_W_USER.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(503, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Date :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(495, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Option :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(217, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(252, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "P E R S O N N E L   I N F O R M A T I O N";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(274, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Personnel System";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocation.Location = new System.Drawing.Point(10, 31);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(64, 13);
            this.lblLocation.TabIndex = 2;
            this.lblLocation.Text = "Location :";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(33, 8);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(41, 13);
            this.lblUser.TabIndex = 0;
            this.lblUser.Text = "User :";
            // 
            // pnlBlkDept
            // 
            this.pnlBlkDept.ConcurrentPanels = null;
            this.pnlBlkDept.Controls.Add(this.label23);
            this.pnlBlkDept.Controls.Add(this.dgvBlkDept);
            this.pnlBlkDept.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlBlkDept.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlBlkDept.DependentPanels = null;
            this.pnlBlkDept.DisableDependentLoad = false;
            this.pnlBlkDept.EnableDelete = true;
            this.pnlBlkDept.EnableInsert = true;
            this.pnlBlkDept.EnableQuery = false;
            this.pnlBlkDept.EnableUpdate = true;
            this.pnlBlkDept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand";
            this.pnlBlkDept.Location = new System.Drawing.Point(663, 82);
            this.pnlBlkDept.MasterPanel = null;
            this.pnlBlkDept.Name = "pnlBlkDept";
            this.pnlBlkDept.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlBlkDept.Size = new System.Drawing.Size(202, 95);
            this.pnlBlkDept.SPName = "CHRIS_SP_RegStHiEnt_DEPT_CONT_MANAGER";
            this.pnlBlkDept.TabIndex = 14;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(3, 1);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(144, 13);
            this.label23.TabIndex = 10;
            this.label23.Text = "Department Contribution";
            // 
            // dgvBlkDept
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBlkDept.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBlkDept.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkDept.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Segment,
            this.Dept,
            this.Contribution1,
            this.Contribution2,
            this.Contribution3});
            this.dgvBlkDept.ColumnToHide = null;
            this.dgvBlkDept.ColumnWidth = null;
            this.dgvBlkDept.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBlkDept.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBlkDept.DisplayColumnWrapper = null;
            this.dgvBlkDept.GridDefaultRow = 0;
            this.dgvBlkDept.Location = new System.Drawing.Point(3, 17);
            this.dgvBlkDept.Name = "dgvBlkDept";
            this.dgvBlkDept.ReadOnlyColumns = null;
            this.dgvBlkDept.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBlkDept.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvBlkDept.Size = new System.Drawing.Size(196, 75);
            this.dgvBlkDept.SkippingColumns = null;
            this.dgvBlkDept.TabIndex = 0;
            this.dgvBlkDept.TabStop = false;
            // 
            // Segment
            // 
            this.Segment.DataPropertyName = "PR_SEGMENT";
            this.Segment.HeaderText = "Segment";
            this.Segment.Name = "Segment";
            this.Segment.Width = 40;
            // 
            // Dept
            // 
            this.Dept.DataPropertyName = "PR_DEPT";
            this.Dept.HeaderText = "Dept.";
            this.Dept.Name = "Dept";
            this.Dept.Width = 50;
            // 
            // Contribution1
            // 
            this.Contribution1.DataPropertyName = "PR_CONTRIB";
            this.Contribution1.HeaderText = "";
            this.Contribution1.Name = "Contribution1";
            this.Contribution1.Width = 30;
            // 
            // Contribution2
            // 
            this.Contribution2.DataPropertyName = "PR_D_NO";
            this.Contribution2.HeaderText = "Contribution %";
            this.Contribution2.Name = "Contribution2";
            // 
            // Contribution3
            // 
            this.Contribution3.DataPropertyName = "PR_TYPE";
            this.Contribution3.HeaderText = "";
            this.Contribution3.Name = "Contribution3";
            this.Contribution3.Width = 30;
            // 
            // txt_PR_USER_IS_PRIME
            // 
            this.txt_PR_USER_IS_PRIME.AllowSpace = true;
            this.txt_PR_USER_IS_PRIME.AssociatedLookUpName = "";
            this.txt_PR_USER_IS_PRIME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_USER_IS_PRIME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_USER_IS_PRIME.ContinuationTextBox = null;
            this.txt_PR_USER_IS_PRIME.CustomEnabled = true;
            this.txt_PR_USER_IS_PRIME.DataFieldMapping = "PR_USER_IS_PRIME";
            this.txt_PR_USER_IS_PRIME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_USER_IS_PRIME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_USER_IS_PRIME.IsDate = false;
            this.txt_PR_USER_IS_PRIME.Location = new System.Drawing.Point(156, 244);
            this.txt_PR_USER_IS_PRIME.Name = "txt_PR_USER_IS_PRIME";
            this.txt_PR_USER_IS_PRIME.NumberFormat = "###,###,##0.00";
            this.txt_PR_USER_IS_PRIME.Postfix = "";
            this.txt_PR_USER_IS_PRIME.Prefix = "";
            this.txt_PR_USER_IS_PRIME.Size = new System.Drawing.Size(30, 20);
            this.txt_PR_USER_IS_PRIME.SkipValidation = false;
            this.txt_PR_USER_IS_PRIME.TabIndex = 99;
            this.txt_PR_USER_IS_PRIME.TabStop = false;
            this.txt_PR_USER_IS_PRIME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_GROUP_HOSP
            // 
            this.txt_PR_GROUP_HOSP.AllowSpace = true;
            this.txt_PR_GROUP_HOSP.AssociatedLookUpName = "";
            this.txt_PR_GROUP_HOSP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_GROUP_HOSP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_GROUP_HOSP.ContinuationTextBox = null;
            this.txt_PR_GROUP_HOSP.CustomEnabled = true;
            this.txt_PR_GROUP_HOSP.DataFieldMapping = "PR_GROUP_HOSP";
            this.txt_PR_GROUP_HOSP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_GROUP_HOSP.GetRecordsOnUpDownKeys = false;
            this.txt_PR_GROUP_HOSP.IsDate = false;
            this.txt_PR_GROUP_HOSP.Location = new System.Drawing.Point(156, 227);
            this.txt_PR_GROUP_HOSP.Name = "txt_PR_GROUP_HOSP";
            this.txt_PR_GROUP_HOSP.NumberFormat = "###,###,##0.00";
            this.txt_PR_GROUP_HOSP.Postfix = "";
            this.txt_PR_GROUP_HOSP.Prefix = "";
            this.txt_PR_GROUP_HOSP.Size = new System.Drawing.Size(30, 20);
            this.txt_PR_GROUP_HOSP.SkipValidation = false;
            this.txt_PR_GROUP_HOSP.TabIndex = 98;
            this.txt_PR_GROUP_HOSP.TabStop = false;
            this.txt_PR_GROUP_HOSP.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_OPD
            // 
            this.txt_PR_OPD.AllowSpace = true;
            this.txt_PR_OPD.AssociatedLookUpName = "";
            this.txt_PR_OPD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_OPD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_OPD.ContinuationTextBox = null;
            this.txt_PR_OPD.CustomEnabled = true;
            this.txt_PR_OPD.DataFieldMapping = "PR_OPD";
            this.txt_PR_OPD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_OPD.GetRecordsOnUpDownKeys = false;
            this.txt_PR_OPD.IsDate = false;
            this.txt_PR_OPD.Location = new System.Drawing.Point(156, 213);
            this.txt_PR_OPD.Name = "txt_PR_OPD";
            this.txt_PR_OPD.NumberFormat = "###,###,##0.00";
            this.txt_PR_OPD.Postfix = "";
            this.txt_PR_OPD.Prefix = "";
            this.txt_PR_OPD.Size = new System.Drawing.Size(30, 20);
            this.txt_PR_OPD.SkipValidation = false;
            this.txt_PR_OPD.TabIndex = 97;
            this.txt_PR_OPD.TabStop = false;
            this.txt_PR_OPD.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_GROUP_LIFE
            // 
            this.txt_PR_GROUP_LIFE.AllowSpace = true;
            this.txt_PR_GROUP_LIFE.AssociatedLookUpName = "";
            this.txt_PR_GROUP_LIFE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_GROUP_LIFE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_GROUP_LIFE.ContinuationTextBox = null;
            this.txt_PR_GROUP_LIFE.CustomEnabled = true;
            this.txt_PR_GROUP_LIFE.DataFieldMapping = "PR_GROUP_LIFE";
            this.txt_PR_GROUP_LIFE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_GROUP_LIFE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_GROUP_LIFE.IsDate = false;
            this.txt_PR_GROUP_LIFE.Location = new System.Drawing.Point(156, 196);
            this.txt_PR_GROUP_LIFE.Name = "txt_PR_GROUP_LIFE";
            this.txt_PR_GROUP_LIFE.NumberFormat = "###,###,##0.00";
            this.txt_PR_GROUP_LIFE.Postfix = "";
            this.txt_PR_GROUP_LIFE.Prefix = "";
            this.txt_PR_GROUP_LIFE.Size = new System.Drawing.Size(30, 20);
            this.txt_PR_GROUP_LIFE.SkipValidation = false;
            this.txt_PR_GROUP_LIFE.TabIndex = 96;
            this.txt_PR_GROUP_LIFE.TabStop = false;
            this.txt_PR_GROUP_LIFE.TextType = CrplControlLibrary.TextType.String;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(16, 239);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(119, 13);
            this.label60.TabIndex = 95;
            this.label60.Text = "User ID On PRIME :";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(16, 226);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(146, 13);
            this.label59.TabIndex = 94;
            this.label59.Text = "Enrolled In Group Hosp :";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(16, 213);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(147, 13);
            this.label58.TabIndex = 93;
            this.label58.Text = "Enrolled In Out patient  :";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(16, 200);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(116, 13);
            this.label57.TabIndex = 92;
            this.label57.Text = "Enrolled in Grp Life";
            // 
            // txt_PR_LANG_6
            // 
            this.txt_PR_LANG_6.AllowSpace = true;
            this.txt_PR_LANG_6.AssociatedLookUpName = "";
            this.txt_PR_LANG_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG_6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG_6.ContinuationTextBox = null;
            this.txt_PR_LANG_6.CustomEnabled = true;
            this.txt_PR_LANG_6.DataFieldMapping = "PR_LANG_6";
            this.txt_PR_LANG_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG_6.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG_6.IsDate = false;
            this.txt_PR_LANG_6.Location = new System.Drawing.Point(277, 106);
            this.txt_PR_LANG_6.Name = "txt_PR_LANG_6";
            this.txt_PR_LANG_6.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG_6.Postfix = "";
            this.txt_PR_LANG_6.Prefix = "";
            this.txt_PR_LANG_6.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_LANG_6.SkipValidation = false;
            this.txt_PR_LANG_6.TabIndex = 85;
            this.txt_PR_LANG_6.TabStop = false;
            this.txt_PR_LANG_6.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_LANG_5
            // 
            this.txt_PR_LANG_5.AllowSpace = true;
            this.txt_PR_LANG_5.AssociatedLookUpName = "";
            this.txt_PR_LANG_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG_5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG_5.ContinuationTextBox = null;
            this.txt_PR_LANG_5.CustomEnabled = true;
            this.txt_PR_LANG_5.DataFieldMapping = "PR_LANG_5";
            this.txt_PR_LANG_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG_5.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG_5.IsDate = false;
            this.txt_PR_LANG_5.Location = new System.Drawing.Point(277, 89);
            this.txt_PR_LANG_5.Name = "txt_PR_LANG_5";
            this.txt_PR_LANG_5.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG_5.Postfix = "";
            this.txt_PR_LANG_5.Prefix = "";
            this.txt_PR_LANG_5.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_LANG_5.SkipValidation = false;
            this.txt_PR_LANG_5.TabIndex = 84;
            this.txt_PR_LANG_5.TabStop = false;
            this.txt_PR_LANG_5.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_LANG_4
            // 
            this.txt_PR_LANG_4.AllowSpace = true;
            this.txt_PR_LANG_4.AssociatedLookUpName = "";
            this.txt_PR_LANG_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG_4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG_4.ContinuationTextBox = null;
            this.txt_PR_LANG_4.CustomEnabled = true;
            this.txt_PR_LANG_4.DataFieldMapping = "PR_LANG_4";
            this.txt_PR_LANG_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG_4.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG_4.IsDate = false;
            this.txt_PR_LANG_4.Location = new System.Drawing.Point(277, 72);
            this.txt_PR_LANG_4.Name = "txt_PR_LANG_4";
            this.txt_PR_LANG_4.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG_4.Postfix = "";
            this.txt_PR_LANG_4.Prefix = "";
            this.txt_PR_LANG_4.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_LANG_4.SkipValidation = false;
            this.txt_PR_LANG_4.TabIndex = 83;
            this.txt_PR_LANG_4.TabStop = false;
            this.txt_PR_LANG_4.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_LANG_3
            // 
            this.txt_PR_LANG_3.AllowSpace = true;
            this.txt_PR_LANG_3.AssociatedLookUpName = "";
            this.txt_PR_LANG_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG_3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG_3.ContinuationTextBox = null;
            this.txt_PR_LANG_3.CustomEnabled = true;
            this.txt_PR_LANG_3.DataFieldMapping = "PR_LANG_3";
            this.txt_PR_LANG_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG_3.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG_3.IsDate = false;
            this.txt_PR_LANG_3.Location = new System.Drawing.Point(277, 54);
            this.txt_PR_LANG_3.Name = "txt_PR_LANG_3";
            this.txt_PR_LANG_3.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG_3.Postfix = "";
            this.txt_PR_LANG_3.Prefix = "";
            this.txt_PR_LANG_3.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_LANG_3.SkipValidation = false;
            this.txt_PR_LANG_3.TabIndex = 82;
            this.txt_PR_LANG_3.TabStop = false;
            this.txt_PR_LANG_3.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_LANG_2
            // 
            this.txt_PR_LANG_2.AllowSpace = true;
            this.txt_PR_LANG_2.AssociatedLookUpName = "";
            this.txt_PR_LANG_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG_2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG_2.ContinuationTextBox = null;
            this.txt_PR_LANG_2.CustomEnabled = true;
            this.txt_PR_LANG_2.DataFieldMapping = "PR_LANG_2";
            this.txt_PR_LANG_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG_2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG_2.IsDate = false;
            this.txt_PR_LANG_2.Location = new System.Drawing.Point(277, 36);
            this.txt_PR_LANG_2.Name = "txt_PR_LANG_2";
            this.txt_PR_LANG_2.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG_2.Postfix = "";
            this.txt_PR_LANG_2.Prefix = "";
            this.txt_PR_LANG_2.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_LANG_2.SkipValidation = false;
            this.txt_PR_LANG_2.TabIndex = 81;
            this.txt_PR_LANG_2.TabStop = false;
            this.txt_PR_LANG_2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_LANG_1
            // 
            this.txt_PR_LANG_1.AllowSpace = true;
            this.txt_PR_LANG_1.AssociatedLookUpName = "";
            this.txt_PR_LANG_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG_1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG_1.ContinuationTextBox = null;
            this.txt_PR_LANG_1.CustomEnabled = true;
            this.txt_PR_LANG_1.DataFieldMapping = "PR_LANG_1";
            this.txt_PR_LANG_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG_1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG_1.IsDate = false;
            this.txt_PR_LANG_1.Location = new System.Drawing.Point(277, 19);
            this.txt_PR_LANG_1.Name = "txt_PR_LANG_1";
            this.txt_PR_LANG_1.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG_1.Postfix = "";
            this.txt_PR_LANG_1.Prefix = "";
            this.txt_PR_LANG_1.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_LANG_1.SkipValidation = false;
            this.txt_PR_LANG_1.TabIndex = 80;
            this.txt_PR_LANG_1.TabStop = false;
            this.txt_PR_LANG_1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_MARITAL
            // 
            this.txt_PR_MARITAL.AllowSpace = true;
            this.txt_PR_MARITAL.AssociatedLookUpName = "";
            this.txt_PR_MARITAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_MARITAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_MARITAL.ContinuationTextBox = null;
            this.txt_PR_MARITAL.CustomEnabled = true;
            this.txt_PR_MARITAL.DataFieldMapping = "PR_MARITAL";
            this.txt_PR_MARITAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_MARITAL.GetRecordsOnUpDownKeys = false;
            this.txt_PR_MARITAL.IsDate = false;
            this.txt_PR_MARITAL.Location = new System.Drawing.Point(106, 146);
            this.txt_PR_MARITAL.Name = "txt_PR_MARITAL";
            this.txt_PR_MARITAL.NumberFormat = "###,###,##0.00";
            this.txt_PR_MARITAL.Postfix = "";
            this.txt_PR_MARITAL.Prefix = "";
            this.txt_PR_MARITAL.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_MARITAL.SkipValidation = false;
            this.txt_PR_MARITAL.TabIndex = 78;
            this.txt_PR_MARITAL.TabStop = false;
            this.txt_PR_MARITAL.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_OLD_ID_CARD_NO
            // 
            this.txt_PR_OLD_ID_CARD_NO.AllowSpace = true;
            this.txt_PR_OLD_ID_CARD_NO.AssociatedLookUpName = "";
            this.txt_PR_OLD_ID_CARD_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_OLD_ID_CARD_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_OLD_ID_CARD_NO.ContinuationTextBox = null;
            this.txt_PR_OLD_ID_CARD_NO.CustomEnabled = true;
            this.txt_PR_OLD_ID_CARD_NO.DataFieldMapping = "PR_OLD_ID_CARD_NO";
            this.txt_PR_OLD_ID_CARD_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_OLD_ID_CARD_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_OLD_ID_CARD_NO.IsDate = false;
            this.txt_PR_OLD_ID_CARD_NO.Location = new System.Drawing.Point(74, 129);
            this.txt_PR_OLD_ID_CARD_NO.Name = "txt_PR_OLD_ID_CARD_NO";
            this.txt_PR_OLD_ID_CARD_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_OLD_ID_CARD_NO.Postfix = "";
            this.txt_PR_OLD_ID_CARD_NO.Prefix = "";
            this.txt_PR_OLD_ID_CARD_NO.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_OLD_ID_CARD_NO.SkipValidation = false;
            this.txt_PR_OLD_ID_CARD_NO.TabIndex = 77;
            this.txt_PR_OLD_ID_CARD_NO.TabStop = false;
            this.txt_PR_OLD_ID_CARD_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_SEX
            // 
            this.txt_PR_SEX.AllowSpace = true;
            this.txt_PR_SEX.AssociatedLookUpName = "";
            this.txt_PR_SEX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_SEX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_SEX.ContinuationTextBox = null;
            this.txt_PR_SEX.CustomEnabled = true;
            this.txt_PR_SEX.DataFieldMapping = "PR_SEX";
            this.txt_PR_SEX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_SEX.GetRecordsOnUpDownKeys = false;
            this.txt_PR_SEX.IsDate = false;
            this.txt_PR_SEX.Location = new System.Drawing.Point(153, 96);
            this.txt_PR_SEX.Name = "txt_PR_SEX";
            this.txt_PR_SEX.NumberFormat = "###,###,##0.00";
            this.txt_PR_SEX.Postfix = "";
            this.txt_PR_SEX.Prefix = "";
            this.txt_PR_SEX.Size = new System.Drawing.Size(30, 20);
            this.txt_PR_SEX.SkipValidation = false;
            this.txt_PR_SEX.TabIndex = 76;
            this.txt_PR_SEX.TabStop = false;
            this.txt_PR_SEX.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_ID_CARD_NO
            // 
            this.txt_PR_ID_CARD_NO.AllowSpace = true;
            this.txt_PR_ID_CARD_NO.AssociatedLookUpName = "";
            this.txt_PR_ID_CARD_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ID_CARD_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ID_CARD_NO.ContinuationTextBox = null;
            this.txt_PR_ID_CARD_NO.CustomEnabled = true;
            this.txt_PR_ID_CARD_NO.DataFieldMapping = "PR_ID_CARD_NO";
            this.txt_PR_ID_CARD_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ID_CARD_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ID_CARD_NO.IsDate = false;
            this.txt_PR_ID_CARD_NO.Location = new System.Drawing.Point(74, 112);
            this.txt_PR_ID_CARD_NO.Name = "txt_PR_ID_CARD_NO";
            this.txt_PR_ID_CARD_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_ID_CARD_NO.Postfix = "";
            this.txt_PR_ID_CARD_NO.Prefix = "";
            this.txt_PR_ID_CARD_NO.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_ID_CARD_NO.SkipValidation = false;
            this.txt_PR_ID_CARD_NO.TabIndex = 75;
            this.txt_PR_ID_CARD_NO.TabStop = false;
            this.txt_PR_ID_CARD_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_D_BIRTH
            // 
            this.txt_PR_D_BIRTH.AllowSpace = true;
            this.txt_PR_D_BIRTH.AssociatedLookUpName = "";
            this.txt_PR_D_BIRTH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_D_BIRTH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_D_BIRTH.ContinuationTextBox = null;
            this.txt_PR_D_BIRTH.CustomEnabled = true;
            this.txt_PR_D_BIRTH.DataFieldMapping = "PR_D_BIRTH";
            this.txt_PR_D_BIRTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_D_BIRTH.GetRecordsOnUpDownKeys = false;
            this.txt_PR_D_BIRTH.IsDate = false;
            this.txt_PR_D_BIRTH.Location = new System.Drawing.Point(74, 93);
            this.txt_PR_D_BIRTH.Name = "txt_PR_D_BIRTH";
            this.txt_PR_D_BIRTH.NumberFormat = "###,###,##0.00";
            this.txt_PR_D_BIRTH.Postfix = "";
            this.txt_PR_D_BIRTH.Prefix = "";
            this.txt_PR_D_BIRTH.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_D_BIRTH.SkipValidation = false;
            this.txt_PR_D_BIRTH.TabIndex = 74;
            this.txt_PR_D_BIRTH.TabStop = false;
            this.txt_PR_D_BIRTH.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_PHONE2
            // 
            this.txt_PR_PHONE2.AllowSpace = true;
            this.txt_PR_PHONE2.AssociatedLookUpName = "";
            this.txt_PR_PHONE2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_PHONE2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_PHONE2.ContinuationTextBox = null;
            this.txt_PR_PHONE2.CustomEnabled = true;
            this.txt_PR_PHONE2.DataFieldMapping = "PR_PHONE2";
            this.txt_PR_PHONE2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_PHONE2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_PHONE2.IsDate = false;
            this.txt_PR_PHONE2.Location = new System.Drawing.Point(153, 73);
            this.txt_PR_PHONE2.Name = "txt_PR_PHONE2";
            this.txt_PR_PHONE2.NumberFormat = "###,###,##0.00";
            this.txt_PR_PHONE2.Postfix = "";
            this.txt_PR_PHONE2.Prefix = "";
            this.txt_PR_PHONE2.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_PHONE2.SkipValidation = false;
            this.txt_PR_PHONE2.TabIndex = 73;
            this.txt_PR_PHONE2.TabStop = false;
            this.txt_PR_PHONE2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_PHONE1
            // 
            this.txt_PR_PHONE1.AllowSpace = true;
            this.txt_PR_PHONE1.AssociatedLookUpName = "";
            this.txt_PR_PHONE1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_PHONE1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_PHONE1.ContinuationTextBox = null;
            this.txt_PR_PHONE1.CustomEnabled = true;
            this.txt_PR_PHONE1.DataFieldMapping = "PR_PHONE1";
            this.txt_PR_PHONE1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_PHONE1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_PHONE1.IsDate = false;
            this.txt_PR_PHONE1.Location = new System.Drawing.Point(70, 73);
            this.txt_PR_PHONE1.Name = "txt_PR_PHONE1";
            this.txt_PR_PHONE1.NumberFormat = "###,###,##0.00";
            this.txt_PR_PHONE1.Postfix = "";
            this.txt_PR_PHONE1.Prefix = "";
            this.txt_PR_PHONE1.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_PHONE1.SkipValidation = false;
            this.txt_PR_PHONE1.TabIndex = 72;
            this.txt_PR_PHONE1.TabStop = false;
            this.txt_PR_PHONE1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_ADD2
            // 
            this.txt_PR_ADD2.AllowSpace = true;
            this.txt_PR_ADD2.AssociatedLookUpName = "";
            this.txt_PR_ADD2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ADD2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ADD2.ContinuationTextBox = null;
            this.txt_PR_ADD2.CustomEnabled = true;
            this.txt_PR_ADD2.DataFieldMapping = "PR_ADD2";
            this.txt_PR_ADD2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ADD2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ADD2.IsDate = false;
            this.txt_PR_ADD2.Location = new System.Drawing.Point(70, 54);
            this.txt_PR_ADD2.Name = "txt_PR_ADD2";
            this.txt_PR_ADD2.NumberFormat = "###,###,##0.00";
            this.txt_PR_ADD2.Postfix = "";
            this.txt_PR_ADD2.Prefix = "";
            this.txt_PR_ADD2.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_ADD2.SkipValidation = false;
            this.txt_PR_ADD2.TabIndex = 71;
            this.txt_PR_ADD2.TabStop = false;
            this.txt_PR_ADD2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(-3, 167);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(79, 12);
            this.label53.TabIndex = 69;
            this.label53.Text = "Date of Marriage :";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(0, 152);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(73, 12);
            this.label52.TabIndex = 67;
            this.label52.Text = "Material Status :";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(-3, 133);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(53, 12);
            this.label51.TabIndex = 64;
            this.label51.Text = "Old ID Card";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(-3, 112);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(54, 12);
            this.label50.TabIndex = 63;
            this.label50.Text = "ID Card No.";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(125, 96);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(28, 13);
            this.label49.TabIndex = 60;
            this.label49.Text = "Sex";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(9, 95);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(29, 12);
            this.label48.TabIndex = 59;
            this.label48.Text = "D O B";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(125, 73);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(18, 13);
            this.label47.TabIndex = 58;
            this.label47.Text = "2)";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(3, 73);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(41, 12);
            this.label46.TabIndex = 57;
            this.label46.Text = "Phone 1)";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(253, 105);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(18, 13);
            this.label45.TabIndex = 56;
            this.label45.Text = "6)";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(253, 87);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(18, 13);
            this.label44.TabIndex = 55;
            this.label44.Text = "5)";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(253, 72);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(18, 13);
            this.label43.TabIndex = 54;
            this.label43.Text = "4)";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(253, 54);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(18, 13);
            this.label42.TabIndex = 53;
            this.label42.Text = "3)";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(253, 36);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(18, 13);
            this.label41.TabIndex = 52;
            this.label41.Text = "2)";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(253, 19);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(18, 13);
            this.label40.TabIndex = 51;
            this.label40.Text = "1)";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(166, 37);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(69, 13);
            this.label39.TabIndex = 50;
            this.label39.Text = "Languages";
            // 
            // txt_PR_ADD1
            // 
            this.txt_PR_ADD1.AllowSpace = true;
            this.txt_PR_ADD1.AssociatedLookUpName = "";
            this.txt_PR_ADD1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ADD1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ADD1.ContinuationTextBox = null;
            this.txt_PR_ADD1.CustomEnabled = true;
            this.txt_PR_ADD1.DataFieldMapping = "PR_ADD1";
            this.txt_PR_ADD1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ADD1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ADD1.IsDate = false;
            this.txt_PR_ADD1.Location = new System.Drawing.Point(70, 35);
            this.txt_PR_ADD1.Name = "txt_PR_ADD1";
            this.txt_PR_ADD1.NumberFormat = "###,###,##0.00";
            this.txt_PR_ADD1.Postfix = "";
            this.txt_PR_ADD1.Prefix = "";
            this.txt_PR_ADD1.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_ADD1.SkipValidation = false;
            this.txt_PR_ADD1.TabIndex = 37;
            this.txt_PR_ADD1.TabStop = false;
            this.txt_PR_ADD1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(3, 40);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(45, 12);
            this.label38.TabIndex = 40;
            this.label38.Text = "Address :";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(226, 6);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(34, 13);
            this.label36.TabIndex = 38;
            this.label36.Text = "Date";
            // 
            // pnlTblBlkEmp
            // 
            this.pnlTblBlkEmp.ConcurrentPanels = null;
            this.pnlTblBlkEmp.Controls.Add(this.label24);
            this.pnlTblBlkEmp.Controls.Add(this.dgvBlkEmp);
            this.pnlTblBlkEmp.DataManager = null;
            this.pnlTblBlkEmp.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBlkEmp.DependentPanels = null;
            this.pnlTblBlkEmp.DisableDependentLoad = false;
            this.pnlTblBlkEmp.EnableDelete = true;
            this.pnlTblBlkEmp.EnableInsert = true;
            this.pnlTblBlkEmp.EnableQuery = false;
            this.pnlTblBlkEmp.EnableUpdate = true;
            this.pnlTblBlkEmp.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntPreEmpCommand";
            this.pnlTblBlkEmp.Location = new System.Drawing.Point(871, 75);
            this.pnlTblBlkEmp.MasterPanel = null;
            this.pnlTblBlkEmp.Name = "pnlTblBlkEmp";
            this.pnlTblBlkEmp.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBlkEmp.Size = new System.Drawing.Size(151, 101);
            this.pnlTblBlkEmp.SPName = "CHRIS_SP_RegStHiEnt_PRE_EMP_MANAGER";
            this.pnlTblBlkEmp.TabIndex = 17;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(0, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(147, 13);
            this.label24.TabIndex = 11;
            this.label24.Text = "Prev Employment History";
            // 
            // dgvBlkEmp
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBlkEmp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvBlkEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkEmp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.From,
            this.To,
            this.OrgAdd,
            this.Designation,
            this.Remarks});
            this.dgvBlkEmp.ColumnToHide = null;
            this.dgvBlkEmp.ColumnWidth = null;
            this.dgvBlkEmp.CustomEnabled = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBlkEmp.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvBlkEmp.DisplayColumnWrapper = null;
            this.dgvBlkEmp.GridDefaultRow = 0;
            this.dgvBlkEmp.Location = new System.Drawing.Point(0, 16);
            this.dgvBlkEmp.Name = "dgvBlkEmp";
            this.dgvBlkEmp.ReadOnlyColumns = null;
            this.dgvBlkEmp.RequiredColumns = null;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBlkEmp.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvBlkEmp.Size = new System.Drawing.Size(135, 82);
            this.dgvBlkEmp.SkippingColumns = null;
            this.dgvBlkEmp.TabIndex = 0;
            this.dgvBlkEmp.TabStop = false;
            // 
            // From
            // 
            this.From.DataPropertyName = "PR_JOB_FROM";
            this.From.HeaderText = "From";
            this.From.Name = "From";
            this.From.Width = 40;
            // 
            // To
            // 
            this.To.DataPropertyName = "PR_JOB_TO";
            this.To.HeaderText = "To";
            this.To.Name = "To";
            this.To.Width = 40;
            // 
            // OrgAdd
            // 
            this.OrgAdd.DataPropertyName = "PR_ORGANIZ";
            this.OrgAdd.HeaderText = "OrgAdd";
            this.OrgAdd.Name = "OrgAdd";
            this.OrgAdd.Width = 40;
            // 
            // Designation
            // 
            this.Designation.DataPropertyName = "PR_DESIG_PR";
            this.Designation.HeaderText = "Designation";
            this.Designation.Name = "Designation";
            this.Designation.Width = 40;
            // 
            // Remarks
            // 
            this.Remarks.DataPropertyName = "PR_REMARKS_PR";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.Name = "Remarks";
            this.Remarks.Width = 40;
            // 
            // pnlSplBlkPersMain
            // 
            this.pnlSplBlkPersMain.ConcurrentPanels = null;
            this.pnlSplBlkPersMain.Controls.Add(this.label58);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_BIRTH_SP);
            this.pnlSplBlkPersMain.Controls.Add(this.label57);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_w_date_2);
            this.pnlSplBlkPersMain.Controls.Add(this.label59);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_MARRIAGE);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_USER_IS_PRIME);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_NO_OF_CHILD);
            this.pnlSplBlkPersMain.Controls.Add(this.label60);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_ADD2);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_GROUP_LIFE);
            this.pnlSplBlkPersMain.Controls.Add(this.label53);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_GROUP_HOSP);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_OPD);
            this.pnlSplBlkPersMain.Controls.Add(this.label25);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_PHONE1);
            this.pnlSplBlkPersMain.Controls.Add(this.label52);
            this.pnlSplBlkPersMain.Controls.Add(this.label26);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_PHONE2);
            this.pnlSplBlkPersMain.Controls.Add(this.label51);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_D_BIRTH);
            this.pnlSplBlkPersMain.Controls.Add(this.label27);
            this.pnlSplBlkPersMain.Controls.Add(this.label50);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_ID_CARD_NO);
            this.pnlSplBlkPersMain.Controls.Add(this.label49);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_SEX);
            this.pnlSplBlkPersMain.Controls.Add(this.label48);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_SPOUSE);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_OLD_ID_CARD_NO);
            this.pnlSplBlkPersMain.Controls.Add(this.label47);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_MARITAL);
            this.pnlSplBlkPersMain.Controls.Add(this.label46);
            this.pnlSplBlkPersMain.Controls.Add(this.label36);
            this.pnlSplBlkPersMain.Controls.Add(this.label45);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG_1);
            this.pnlSplBlkPersMain.Controls.Add(this.label44);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG_2);
            this.pnlSplBlkPersMain.Controls.Add(this.label38);
            this.pnlSplBlkPersMain.Controls.Add(this.label43);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG_3);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_ADD1);
            this.pnlSplBlkPersMain.Controls.Add(this.label42);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG_4);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG_6);
            this.pnlSplBlkPersMain.Controls.Add(this.label41);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG_5);
            this.pnlSplBlkPersMain.Controls.Add(this.label39);
            this.pnlSplBlkPersMain.Controls.Add(this.label40);
            this.pnlSplBlkPersMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlSplBlkPersMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlSplBlkPersMain.DependentPanels = null;
            this.pnlSplBlkPersMain.DisableDependentLoad = false;
            this.pnlSplBlkPersMain.EnableDelete = true;
            this.pnlSplBlkPersMain.EnableInsert = true;
            this.pnlSplBlkPersMain.EnableQuery = false;
            this.pnlSplBlkPersMain.EnableUpdate = true;
            this.pnlSplBlkPersMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntPERSONALCommand";
            this.pnlSplBlkPersMain.Location = new System.Drawing.Point(663, 182);
            this.pnlSplBlkPersMain.MasterPanel = null;
            this.pnlSplBlkPersMain.Name = "pnlSplBlkPersMain";
            this.pnlSplBlkPersMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlSplBlkPersMain.Size = new System.Drawing.Size(355, 268);
            this.pnlSplBlkPersMain.SPName = "CHRIS_SP_RegStHiEnt_PERSONAL_MANAGER";
            this.pnlSplBlkPersMain.TabIndex = 100;
            // 
            // txt_PR_BIRTH_SP
            // 
            this.txt_PR_BIRTH_SP.AllowSpace = true;
            this.txt_PR_BIRTH_SP.AssociatedLookUpName = "";
            this.txt_PR_BIRTH_SP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_BIRTH_SP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_BIRTH_SP.ContinuationTextBox = null;
            this.txt_PR_BIRTH_SP.CustomEnabled = true;
            this.txt_PR_BIRTH_SP.DataFieldMapping = "PR_BIRTH_SP";
            this.txt_PR_BIRTH_SP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_BIRTH_SP.GetRecordsOnUpDownKeys = false;
            this.txt_PR_BIRTH_SP.IsDate = false;
            this.txt_PR_BIRTH_SP.Location = new System.Drawing.Point(266, 149);
            this.txt_PR_BIRTH_SP.Name = "txt_PR_BIRTH_SP";
            this.txt_PR_BIRTH_SP.NumberFormat = "###,###,##0.00";
            this.txt_PR_BIRTH_SP.Postfix = "";
            this.txt_PR_BIRTH_SP.Prefix = "";
            this.txt_PR_BIRTH_SP.Size = new System.Drawing.Size(42, 20);
            this.txt_PR_BIRTH_SP.SkipValidation = false;
            this.txt_PR_BIRTH_SP.TabIndex = 139;
            this.txt_PR_BIRTH_SP.TabStop = false;
            this.txt_PR_BIRTH_SP.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_w_date_2
            // 
            this.txt_w_date_2.AllowSpace = true;
            this.txt_w_date_2.AssociatedLookUpName = "";
            this.txt_w_date_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_w_date_2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_w_date_2.ContinuationTextBox = null;
            this.txt_w_date_2.CustomEnabled = true;
            this.txt_w_date_2.DataFieldMapping = "w_date_2";
            this.txt_w_date_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_w_date_2.GetRecordsOnUpDownKeys = false;
            this.txt_w_date_2.IsDate = false;
            this.txt_w_date_2.Location = new System.Drawing.Point(272, -2);
            this.txt_w_date_2.Name = "txt_w_date_2";
            this.txt_w_date_2.NumberFormat = "###,###,##0.00";
            this.txt_w_date_2.Postfix = "";
            this.txt_w_date_2.Prefix = "";
            this.txt_w_date_2.Size = new System.Drawing.Size(62, 20);
            this.txt_w_date_2.SkipValidation = false;
            this.txt_w_date_2.TabIndex = 138;
            this.txt_w_date_2.TabStop = false;
            this.txt_w_date_2.TextType = CrplControlLibrary.TextType.String;
            this.txt_w_date_2.Visible = false;
            // 
            // txt_PR_MARRIAGE
            // 
            this.txt_PR_MARRIAGE.AllowSpace = true;
            this.txt_PR_MARRIAGE.AssociatedLookUpName = "";
            this.txt_PR_MARRIAGE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_MARRIAGE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_MARRIAGE.ContinuationTextBox = null;
            this.txt_PR_MARRIAGE.CustomEnabled = true;
            this.txt_PR_MARRIAGE.DataFieldMapping = "PR_MARRIAGE";
            this.txt_PR_MARRIAGE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_MARRIAGE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_MARRIAGE.IsDate = false;
            this.txt_PR_MARRIAGE.Location = new System.Drawing.Point(106, 168);
            this.txt_PR_MARRIAGE.Name = "txt_PR_MARRIAGE";
            this.txt_PR_MARRIAGE.NumberFormat = "###,###,##0.00";
            this.txt_PR_MARRIAGE.Postfix = "";
            this.txt_PR_MARRIAGE.Prefix = "";
            this.txt_PR_MARRIAGE.Size = new System.Drawing.Size(54, 20);
            this.txt_PR_MARRIAGE.SkipValidation = false;
            this.txt_PR_MARRIAGE.TabIndex = 136;
            this.txt_PR_MARRIAGE.TabStop = false;
            this.txt_PR_MARRIAGE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_NO_OF_CHILD
            // 
            this.txt_PR_NO_OF_CHILD.AllowSpace = true;
            this.txt_PR_NO_OF_CHILD.AssociatedLookUpName = "";
            this.txt_PR_NO_OF_CHILD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NO_OF_CHILD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NO_OF_CHILD.ContinuationTextBox = null;
            this.txt_PR_NO_OF_CHILD.CustomEnabled = true;
            this.txt_PR_NO_OF_CHILD.DataFieldMapping = "PR_NO_OF_CHILD";
            this.txt_PR_NO_OF_CHILD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NO_OF_CHILD.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NO_OF_CHILD.IsDate = false;
            this.txt_PR_NO_OF_CHILD.Location = new System.Drawing.Point(266, 167);
            this.txt_PR_NO_OF_CHILD.Name = "txt_PR_NO_OF_CHILD";
            this.txt_PR_NO_OF_CHILD.NumberFormat = "###,###,##0.00";
            this.txt_PR_NO_OF_CHILD.Postfix = "";
            this.txt_PR_NO_OF_CHILD.Prefix = "";
            this.txt_PR_NO_OF_CHILD.Size = new System.Drawing.Size(42, 20);
            this.txt_PR_NO_OF_CHILD.SkipValidation = false;
            this.txt_PR_NO_OF_CHILD.TabIndex = 135;
            this.txt_PR_NO_OF_CHILD.TabStop = false;
            this.txt_PR_NO_OF_CHILD.TextType = CrplControlLibrary.TextType.String;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(180, 169);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 9);
            this.label25.TabIndex = 133;
            this.label25.Text = "No. of Childern`s";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(196, 150);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(62, 9);
            this.label26.TabIndex = 132;
            this.label26.Text = "Date Of Birth";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(196, 134);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 9);
            this.label27.TabIndex = 131;
            this.label27.Text = "Spouse Name";
            // 
            // txt_PR_SPOUSE
            // 
            this.txt_PR_SPOUSE.AllowSpace = true;
            this.txt_PR_SPOUSE.AssociatedLookUpName = "";
            this.txt_PR_SPOUSE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_SPOUSE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_SPOUSE.ContinuationTextBox = null;
            this.txt_PR_SPOUSE.CustomEnabled = true;
            this.txt_PR_SPOUSE.DataFieldMapping = "PR_SPOUSE";
            this.txt_PR_SPOUSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_SPOUSE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_SPOUSE.IsDate = false;
            this.txt_PR_SPOUSE.Location = new System.Drawing.Point(266, 131);
            this.txt_PR_SPOUSE.Name = "txt_PR_SPOUSE";
            this.txt_PR_SPOUSE.NumberFormat = "###,###,##0.00";
            this.txt_PR_SPOUSE.Postfix = "";
            this.txt_PR_SPOUSE.Prefix = "";
            this.txt_PR_SPOUSE.Size = new System.Drawing.Size(82, 20);
            this.txt_PR_SPOUSE.SkipValidation = false;
            this.txt_PR_SPOUSE.TabIndex = 130;
            this.txt_PR_SPOUSE.TabStop = false;
            this.txt_PR_SPOUSE.TextType = CrplControlLibrary.TextType.String;
            // 
            // pnlTblBLKChild
            // 
            this.pnlTblBLKChild.ConcurrentPanels = null;
            this.pnlTblBLKChild.Controls.Add(this.dgvBlkChild);
            this.pnlTblBLKChild.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblBLKChild.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBLKChild.DependentPanels = null;
            this.pnlTblBLKChild.DisableDependentLoad = false;
            this.pnlTblBLKChild.EnableDelete = true;
            this.pnlTblBLKChild.EnableInsert = true;
            this.pnlTblBLKChild.EnableQuery = false;
            this.pnlTblBLKChild.EnableUpdate = true;
            this.pnlTblBLKChild.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntCHILDERNCommand";
            this.pnlTblBLKChild.Location = new System.Drawing.Point(965, 453);
            this.pnlTblBLKChild.MasterPanel = null;
            this.pnlTblBLKChild.Name = "pnlTblBLKChild";
            this.pnlTblBLKChild.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBLKChild.Size = new System.Drawing.Size(129, 82);
            this.pnlTblBLKChild.SPName = "CHRIS_SP_RegStHiEnt_CHILDREN_MANAGER";
            this.pnlTblBLKChild.TabIndex = 102;
            // 
            // dgvBlkChild
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBlkChild.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvBlkChild.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkChild.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PrcNo,
            this.PRCHILDNAME,
            this.PRDATEBIRTH});
            this.dgvBlkChild.ColumnToHide = null;
            this.dgvBlkChild.ColumnWidth = null;
            this.dgvBlkChild.CustomEnabled = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBlkChild.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvBlkChild.DisplayColumnWrapper = null;
            this.dgvBlkChild.GridDefaultRow = 0;
            this.dgvBlkChild.Location = new System.Drawing.Point(3, 3);
            this.dgvBlkChild.Name = "dgvBlkChild";
            this.dgvBlkChild.ReadOnlyColumns = null;
            this.dgvBlkChild.RequiredColumns = null;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBlkChild.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvBlkChild.Size = new System.Drawing.Size(116, 76);
            this.dgvBlkChild.SkippingColumns = null;
            this.dgvBlkChild.TabIndex = 0;
            this.dgvBlkChild.TabStop = false;
            // 
            // PrcNo
            // 
            this.PrcNo.DataPropertyName = "PR_P_NO";
            this.PrcNo.HeaderText = "";
            this.PrcNo.Name = "PrcNo";
            this.PrcNo.Visible = false;
            // 
            // PRCHILDNAME
            // 
            this.PRCHILDNAME.DataPropertyName = "PR_CHILD_NAME";
            this.PRCHILDNAME.HeaderText = "child name";
            this.PRCHILDNAME.Name = "PRCHILDNAME";
            // 
            // PRDATEBIRTH
            // 
            this.PRDATEBIRTH.DataPropertyName = "PR_DATE_BIRTH";
            this.PRDATEBIRTH.HeaderText = "DOB";
            this.PRDATEBIRTH.Name = "PRDATEBIRTH";
            // 
            // pnlTblBlkEdu
            // 
            this.pnlTblBlkEdu.ConcurrentPanels = null;
            this.pnlTblBlkEdu.Controls.Add(this.dgvBlkEdu);
            this.pnlTblBlkEdu.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblBlkEdu.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBlkEdu.DependentPanels = null;
            this.pnlTblBlkEdu.DisableDependentLoad = false;
            this.pnlTblBlkEdu.EnableDelete = true;
            this.pnlTblBlkEdu.EnableInsert = true;
            this.pnlTblBlkEdu.EnableQuery = false;
            this.pnlTblBlkEdu.EnableUpdate = true;
            this.pnlTblBlkEdu.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntEducationCommand";
            this.pnlTblBlkEdu.Location = new System.Drawing.Point(815, 456);
            this.pnlTblBlkEdu.MasterPanel = null;
            this.pnlTblBlkEdu.Name = "pnlTblBlkEdu";
            this.pnlTblBlkEdu.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBlkEdu.Size = new System.Drawing.Size(147, 91);
            this.pnlTblBlkEdu.SPName = "CHRIS_SP_RegStHiEnt_EDUCATION_MANAGER";
            this.pnlTblBlkEdu.TabIndex = 103;
            // 
            // dgvBlkEdu
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBlkEdu.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvBlkEdu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkEdu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PrEYear,
            this.Pr_Degree,
            this.Pr_Grade,
            this.Pr_College,
            this.Pr_City,
            this.Pr_Country,
            this.Pr_E_No});
            this.dgvBlkEdu.ColumnToHide = null;
            this.dgvBlkEdu.ColumnWidth = null;
            this.dgvBlkEdu.CustomEnabled = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBlkEdu.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvBlkEdu.DisplayColumnWrapper = null;
            this.dgvBlkEdu.GridDefaultRow = 0;
            this.dgvBlkEdu.Location = new System.Drawing.Point(3, 3);
            this.dgvBlkEdu.Name = "dgvBlkEdu";
            this.dgvBlkEdu.ReadOnlyColumns = null;
            this.dgvBlkEdu.RequiredColumns = null;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBlkEdu.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvBlkEdu.Size = new System.Drawing.Size(140, 85);
            this.dgvBlkEdu.SkippingColumns = null;
            this.dgvBlkEdu.TabIndex = 0;
            this.dgvBlkEdu.TabStop = false;
            // 
            // PrEYear
            // 
            this.PrEYear.DataPropertyName = "PR_E_YEAR";
            this.PrEYear.HeaderText = "Year";
            this.PrEYear.Name = "PrEYear";
            // 
            // Pr_Degree
            // 
            this.Pr_Degree.DataPropertyName = "PR_DEGREE";
            this.Pr_Degree.HeaderText = "Certificate/Degree";
            this.Pr_Degree.Name = "Pr_Degree";
            // 
            // Pr_Grade
            // 
            this.Pr_Grade.DataPropertyName = "PR_GRADE";
            this.Pr_Grade.HeaderText = "Devision/Grade";
            this.Pr_Grade.Name = "Pr_Grade";
            // 
            // Pr_College
            // 
            this.Pr_College.DataPropertyName = "PR_COLLAGE";
            this.Pr_College.HeaderText = "School/College";
            this.Pr_College.Name = "Pr_College";
            // 
            // Pr_City
            // 
            this.Pr_City.DataPropertyName = "PR_CITY";
            this.Pr_City.HeaderText = "City";
            this.Pr_City.Name = "Pr_City";
            // 
            // Pr_Country
            // 
            this.Pr_Country.DataPropertyName = "PR_COUNTRY";
            this.Pr_Country.HeaderText = "Country";
            this.Pr_Country.Name = "Pr_Country";
            // 
            // Pr_E_No
            // 
            this.Pr_E_No.DataPropertyName = "PR_P_NO";
            this.Pr_E_No.HeaderText = "Pr_E_No";
            this.Pr_E_No.Name = "Pr_E_No";
            this.Pr_E_No.Visible = false;
            // 
            // pnlTblBlkRef
            // 
            this.pnlTblBlkRef.ConcurrentPanels = null;
            this.pnlTblBlkRef.Controls.Add(this.dgvBlkRef);
            this.pnlTblBlkRef.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblBlkRef.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBlkRef.DependentPanels = null;
            this.pnlTblBlkRef.DisableDependentLoad = false;
            this.pnlTblBlkRef.EnableDelete = true;
            this.pnlTblBlkRef.EnableInsert = true;
            this.pnlTblBlkRef.EnableQuery = false;
            this.pnlTblBlkRef.EnableUpdate = true;
            this.pnlTblBlkRef.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntReferenceCommand";
            this.pnlTblBlkRef.Location = new System.Drawing.Point(666, 456);
            this.pnlTblBlkRef.MasterPanel = null;
            this.pnlTblBlkRef.Name = "pnlTblBlkRef";
            this.pnlTblBlkRef.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBlkRef.Size = new System.Drawing.Size(144, 107);
            this.pnlTblBlkRef.SPName = "CHRIS_SP_RegStHiEnt_EMP_REFERENCE_MANAGER";
            this.pnlTblBlkRef.TabIndex = 104;
            // 
            // dgvBlkRef
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBlkRef.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvBlkRef.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkRef.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ref_Name,
            this.prPNo,
            this.Ref_Add1,
            this.Ref_Add2,
            this.Ref_Add3});
            this.dgvBlkRef.ColumnToHide = null;
            this.dgvBlkRef.ColumnWidth = null;
            this.dgvBlkRef.CustomEnabled = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBlkRef.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvBlkRef.DisplayColumnWrapper = null;
            this.dgvBlkRef.GridDefaultRow = 0;
            this.dgvBlkRef.Location = new System.Drawing.Point(3, 3);
            this.dgvBlkRef.Name = "dgvBlkRef";
            this.dgvBlkRef.ReadOnlyColumns = null;
            this.dgvBlkRef.RequiredColumns = null;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBlkRef.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvBlkRef.Size = new System.Drawing.Size(138, 86);
            this.dgvBlkRef.SkippingColumns = null;
            this.dgvBlkRef.TabIndex = 0;
            this.dgvBlkRef.TabStop = false;
            // 
            // Ref_Name
            // 
            this.Ref_Name.DataPropertyName = "REF_NAME";
            this.Ref_Name.HeaderText = "Name";
            this.Ref_Name.Name = "Ref_Name";
            // 
            // prPNo
            // 
            this.prPNo.DataPropertyName = "PR_P_NO";
            this.prPNo.HeaderText = "prPNo";
            this.prPNo.Name = "prPNo";
            this.prPNo.Visible = false;
            // 
            // Ref_Add1
            // 
            this.Ref_Add1.DataPropertyName = "Ref_Add1";
            this.Ref_Add1.HeaderText = "Address1";
            this.Ref_Add1.Name = "Ref_Add1";
            // 
            // Ref_Add2
            // 
            this.Ref_Add2.DataPropertyName = "Ref_Add2";
            this.Ref_Add2.HeaderText = "Address2";
            this.Ref_Add2.Name = "Ref_Add2";
            // 
            // Ref_Add3
            // 
            this.Ref_Add3.DataPropertyName = "Ref_Add3";
            this.Ref_Add3.HeaderText = "Address3";
            this.Ref_Add3.Name = "Ref_Add3";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(440, 9);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(63, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "UserName :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(503, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(35, 13);
            this.lblUserName.TabIndex = 2;
            this.lblUserName.Text = "User :";
            // 
            // CHRIS_Personnel_RegularStaffHiringEnt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(662, 630);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.pnlTblBlkRef);
            this.Controls.Add(this.pnlTblBlkEmp);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlBlkDept);
            this.Controls.Add(this.pnlPersonnelMain);
            this.Controls.Add(this.pnlSplBlkPersMain);
            this.Controls.Add(this.pnlTblBlkEdu);
            this.Controls.Add(this.pnlTblBLKChild);
            this.Name = "CHRIS_Personnel_RegularStaffHiringEnt";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Personnel_RegularStaffHiringEnt";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_RegularStaffHiringEnt_AfterLOVSelection);
            this.Controls.SetChildIndex(this.pnlTblBLKChild, 0);
            this.Controls.SetChildIndex(this.pnlTblBlkEdu, 0);
            this.Controls.SetChildIndex(this.pnlSplBlkPersMain, 0);
            this.Controls.SetChildIndex(this.pnlPersonnelMain, 0);
            this.Controls.SetChildIndex(this.pnlBlkDept, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlTblBlkEmp, 0);
            this.Controls.SetChildIndex(this.pnlTblBlkRef, 0);
            this.Controls.SetChildIndex(this.label30, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlPersonnelMain.ResumeLayout(false);
            this.pnlPersonnelMain.PerformLayout();
            this.pnlView.ResumeLayout(false);
            this.pnlView.PerformLayout();
            this.pnlSave.ResumeLayout(false);
            this.pnlSave.PerformLayout();
            this.pnlDelete.ResumeLayout(false);
            this.pnlDelete.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlBlkDept.ResumeLayout(false);
            this.pnlBlkDept.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkDept)).EndInit();
            this.pnlTblBlkEmp.ResumeLayout(false);
            this.pnlTblBlkEmp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkEmp)).EndInit();
            this.pnlSplBlkPersMain.ResumeLayout(false);
            this.pnlSplBlkPersMain.PerformLayout();
            this.pnlTblBLKChild.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkChild)).EndInit();
            this.pnlTblBlkEdu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkEdu)).EndInit();
            this.pnlTblBlkRef.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkRef)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPersonnelMain;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPersonnelsNo;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txt_W_USER;
        private CrplControlLibrary.SLTextBox txt_PR_FIRST_NAME;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblBranch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblReportTo;
        private System.Windows.Forms.Label lblFirstName;
        private CrplControlLibrary.SLTextBox txt_PR_LEVEL;
        private CrplControlLibrary.SLTextBox txt_F_RNAME;
        private CrplControlLibrary.SLTextBox txt_PR_LAST_NAME;
        private CrplControlLibrary.SLTextBox txt_PR_BRANCH;
        private CrplControlLibrary.SLTextBox txt_PR_DESIG;
        private CrplControlLibrary.SLTextBox txtReportTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblGeid;
        private System.Windows.Forms.Label lblFunctionalTitle;
        private CrplControlLibrary.SLTextBox txt_PR_CATEGORY;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txt_PR_ANNUAL_PACK;
        private CrplControlLibrary.SLTextBox txt_PR_EMP_TYPE;
        private CrplControlLibrary.SLTextBox txt_F_GEID_NO;
        private CrplControlLibrary.SLTextBox txt_PR_RELIGION;
        private CrplControlLibrary.SLTextBox txt_W_RELIGION;
        private CrplControlLibrary.SLTextBox txt_PR_FUNC_Title2;
        private CrplControlLibrary.SLTextBox txt_PR_FUNC_Title1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txt_PR_ACCOUNT_NO;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txt_PR_NATIONAL_TAX;
        private CrplControlLibrary.SLTextBox txt_PR_TAX_INC;
        private CrplControlLibrary.SLTextBox txt_PR_BANK_ID;
        private CrplControlLibrary.LookupButton lbtnPersonnelNO;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlBlkDept;
        private System.Windows.Forms.Label label23;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBlkDept;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private CrplControlLibrary.SLTextBox txt_PR_CLOSE_FLAG;
        private CrplControlLibrary.LookupButton lbtnBranch;
        private CrplControlLibrary.SLTextBox txt_PR_NEW_BRANCH;
        private CrplControlLibrary.LookupButton lbtnDesignation;
        private CrplControlLibrary.LookupButton lbtnDesgLevel;
        private CrplControlLibrary.SLTextBox txt_PR_CONF_FLAG;
        private CrplControlLibrary.SLTextBox txt_PR_NEW_ANNUAL_PACK;
        private System.Windows.Forms.DataGridViewTextBoxColumn Segment;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dept;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contribution1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contribution2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contribution3;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblBlkEmp;
        private System.Windows.Forms.Label label24;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBlkEmp;
        private System.Windows.Forms.DataGridViewTextBoxColumn From;
        private System.Windows.Forms.DataGridViewTextBoxColumn To;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrgAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn Designation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBlkChild;
        public CrplControlLibrary.SLTextBox txt_PR_P_NO;
        public CrplControlLibrary.SLDatePicker txt_PR_JOINING_DATE;
        public CrplControlLibrary.SLDatePicker txt_PR_CONFIRM;
        private CrplControlLibrary.LookupButton lbtnRepNo;
        public CrplControlLibrary.SLTextBox txt_PR_ADD1;
        public CrplControlLibrary.SLTextBox txt_PR_PHONE2;
        public CrplControlLibrary.SLTextBox txt_PR_PHONE1;
        public CrplControlLibrary.SLTextBox txt_PR_ADD2;
        public CrplControlLibrary.SLTextBox txt_PR_LANG_2;
        public CrplControlLibrary.SLTextBox txt_PR_LANG_1;
        public CrplControlLibrary.SLTextBox txtDate;
        public iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlSplBlkPersMain;
        public iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblBLKChild;
        public CrplControlLibrary.SLTextBox txt_PR_D_BIRTH;
        public CrplControlLibrary.SLTextBox txt_PR_LANG_6;
        public CrplControlLibrary.SLTextBox txt_PR_LANG_5;
        public CrplControlLibrary.SLTextBox txt_PR_LANG_4;
        public CrplControlLibrary.SLTextBox txt_PR_LANG_3;
        public CrplControlLibrary.SLTextBox txt_PR_MARITAL;
        public CrplControlLibrary.SLTextBox txt_PR_OLD_ID_CARD_NO;
        public CrplControlLibrary.SLTextBox txt_PR_SEX;
        public CrplControlLibrary.SLTextBox txt_PR_ID_CARD_NO;
        public CrplControlLibrary.SLTextBox txt_PR_USER_IS_PRIME;
        public CrplControlLibrary.SLTextBox txt_PR_GROUP_HOSP;
        public CrplControlLibrary.SLTextBox txt_PR_OPD;
        public CrplControlLibrary.SLTextBox txt_PR_GROUP_LIFE;
        public CrplControlLibrary.SLTextBox txt_PR_NO_OF_CHILD;
        public CrplControlLibrary.SLTextBox txt_PR_SPOUSE;
        public CrplControlLibrary.SLTextBox txt_PR_MARRIAGE;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblBlkEdu;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBlkEdu;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblBlkRef;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBlkRef;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn prPNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Add1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Add2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Add3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrEYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Degree;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_College;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_City;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_E_No;
        public CrplControlLibrary.SLTextBox txt_PR_TAX_PAID;
        public CrplControlLibrary.SLTextBox txt_w_date_2;
        public CrplControlLibrary.SLTextBox txt_PR_BIRTH_SP;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrcNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRCHILDNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRDATEBIRTH;
        public CrplControlLibrary.SLDatePicker txt_PR_EXPIRY;
        public CrplControlLibrary.SLDatePicker txt_PR_ID_ISSUE;
        public iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlView;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label21;
        public iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlSave;
        public CrplControlLibrary.SLTextBox txt_W_ADD_ANS;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txt_W_VIEW_ANS;
        private System.Windows.Forms.Label label28;
        public iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDelete;
        public CrplControlLibrary.SLTextBox txt_W_DEL_ANS;
        private System.Windows.Forms.Label label22;
        private CrplControlLibrary.SLTextBox txtLoc;
        private System.Windows.Forms.Label label29;
        public CrplControlLibrary.SLTextBox txt_W_OPTION_DIS;
        private CrplControlLibrary.SLTextBox txtID;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.SLTextBox slTxtbEmail;
        private System.Windows.Forms.Label lbEmail;

    }
}