using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using CrplControlLibrary;


using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_ConfirmationEntry : ChrisTabularForm
    {
        CmnDataManager cmnDm = new CmnDataManager();
        string inputMonth = string.Empty;
        bool oldrptCalled = true;
        bool newrptCalled = true;
        # region constructor
        public CHRIS_Personnel_ConfirmationEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_ConfirmationEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.FunctionConfig.EnableF8 = true;
            this.FunctionConfig.F8 = Function.Save;
            this.DGVPersonnel.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            //this.tlbMain.Items["tbtSearch"].Visible = false;
            //Exit Without Save
        }



        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            this.txtOption.Visible = false;
            this.txtDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            this.txtLocation.Text = this.CurrentLocation;
            this.txtUser.Text = this.UserName;
            lblUserName.Text += "   " + this.UserName;
            this.slTextBox1.Text = "CONFIRMATIONS";
            tbtSave.Visible = true;

            this.FunctionConfig.F8 = Function.Save;
            this.FunctionConfig.EnableF8 = true;
            this.ShowF8Option = true;
            this.F8OptionText = "[F8] To Save";



        }

        void pr_pNo()
        {


        }

        protected override bool Save()
        {
            bool flag = false;
            DGVPersonnel.EndEdit();
            //this.tbtSave.PerformClick();
            DataTable dt = (DataTable)DGVPersonnel.DataSource;
            if (dt != null)
            {
                DataTable dtAdded = dt.GetChanges(DataRowState.Unchanged);
                DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                //if (dtAdded != null || dtUpdated != null)
                {

                    //for (int z = 0; z < DGVPersonnel.Rows.Count - 1; z++)
                    //{
                    //    if (Convert.ToString(dt.Rows[z]["PR_CONFIRM_ON"]) == "" || Convert.ToString(dt.Rows[z]["PR_EXPECTED"]) == "")
                    //    {
                    //         return false;
                    //    }

                    //}


                    DialogResult dr = MessageBox.Show("Do You Want to Save Changes?", "", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        //CallReport("AUPR", "OLD");
                        this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSave"]));
                        // CallReport("AUP0", "NEW");

                    }

                    //else
                    //    return false;
                }
                if (DGVPersonnel.CurrentCell != null)
                {
                    // if (DGVPersonnel.CurrentCell.IsInEditMode)
                    {
                        DGVPersonnel.CancelEdit();
                        DGVPersonnel.EndEdit();
                        DGVPersonnel.RejectChanges();

                    }


                    (DGVPersonnel.DataSource as DataTable).RejectChanges();
                    (DGVPersonnel.DataSource as DataTable).Reset();
                    DGVPersonnel.DataSource = null;
                }
                this.CHRIS_Personnel_ConfirmationEntry_Shown(null, null);
            }


            return flag;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            //if (e.KeyCode == Keys.F8)
            //{
            //    DialogResult dr = MessageBox.Show("Do You Want to Save Changes?", "", MessageBoxButtons.YesNo,
            //       MessageBoxIcon.Question);
            //    if (dr == DialogResult.Yes)
            //    {
            //        CallReport("AUPR", "OLD");
            //        this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSave"]));
            //        CallReport("AUP0", "NEW");

            //    }

            //}
        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {

            #region CommentedCOde
            //if (actionType == "Save")
            //{
            //    DGVPersonnel.EndEdit();

            //    DataTable dt = (DataTable)DGVPersonnel.DataSource;
            //    if (dt != null)
            //    {
            //        DataTable dtAdded = dt.GetChanges(DataRowState.Added);
            //        DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
            //        if (dtAdded != null || dtUpdated != null)
            //        {
            //            DialogResult dr = MessageBox.Show("Do you want to save changes.", "", MessageBoxButtons.YesNo,
            //            MessageBoxIcon.Question);
            //            if (dr == DialogResult.No)
            //            {
            //                return;
            //            }
            //            else
            //            {
            //                this.DGVPersonnel.SaveGrid(this.PnlPersonnel);
            //            }
            //        }
            //    }
            //}
            //else 
            #endregion
            if (actionType == "Delete")
            {

                CallReport("AUPR", "OLD");
                DialogResult response = MessageBox.Show("Are You Sure You Want to Delete This Record?", "Delete row?",
                                                        MessageBoxButtons.YesNo,
                                                        MessageBoxIcon.Question,
                                                        MessageBoxDefaultButton.Button2);
                if (response == DialogResult.No)
                    return;
                if (this.PnlPersonnel.EnableDelete)
                {
                    DGVPersonnel.DeleteSelectedGridRow();
                    operationMode = Mode.Edit;
                }
                this.Message.ShowMessage();
                CallReport("AUP0", "NEW");
                this.CHRIS_Personnel_ConfirmationEntry_Shown(null, null);
                return;
            }

            if (actionType == "Save")
            {

                ////DialogResult dr = MessageBox.Show("Do You Want to Save Changes?", "", MessageBoxButtons.YesNo,
                ////   MessageBoxIcon.Question);
                ////if (dr == DialogResult.Yes)
                ////{
                //newly commented.
                if (oldrptCalled)
                {
                    CallReport("AUPR", "OLD");
                    oldrptCalled = false;
                }
                base.DoToolbarActions(ctrlsCollection, actionType);


                if (newrptCalled)
                {
                    CallReport("AUP0", "NEW");
                    oldrptCalled = true;
                }

                return;
                //}
            }
            if (actionType == "Search")
            {
                //return;
            }
            else
                base.DoToolbarActions(ctrlsCollection, actionType);
            if (actionType == "Cancel")
            {
                base.DoToolbarActions(ctrlsCollection, "Search");
                if (base.tbtCancel.Pressed)
                    this.CHRIS_Personnel_ConfirmationEntry_Shown(null, null);

            }

            //if (actionType == "Delete")
            //{

            //}

        }

        private void MonthInputProcessing()
        {
            inputMonth = string.Empty;
            frmInput input = new frmInput("Month", CrplControlLibrary.TextType.Integer);
            input.TextBox.MaxLength = 2;
            input.TextBox.TextAlign = HorizontalAlignment.Right;
            input.TextBox.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
            input.TextBox.PreviewKeyDown += new PreviewKeyDownEventHandler(TextBox_PreviewKeyDown);
            input.ShowDialog(this);

            string value = input.Value;

            if (value.Trim() != string.Empty)
            {
                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("month", value);
                rslt = cmnDm.GetData("CHRIS_SP_Personnel_confirmation_MANAGER", "Pr_Confirm", param);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0] != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        inputMonth = value.Trim();
                        DGVPersonnel.DataSource = rslt.dstResult.Tables[0];
                        DGVPersonnel.GridSource = rslt.dstResult.Tables[0];

                    }
                    else
                    {
                        inputMonth = string.Empty;
                        MessageBox.Show("No. Confirmation Due For This Month", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                        //No. Confirmation Due For This Month
                        MonthInputProcessing();

                    }
                }
            }
            else
            {
                this.CHRIS_Personnel_ConfirmationEntry_Shown(null, null);
            }


        }

        protected override bool Quit()
        {
            bool flag = false;
            if (DGVPersonnel.CurrentCell != null)
            {
                if (DGVPersonnel.CurrentCell.IsInEditMode)
                {
                    DGVPersonnel.CancelEdit();
                    DGVPersonnel.EndEdit();
                    DGVPersonnel.RejectChanges();
                }
                (DGVPersonnel.DataSource as DataTable).RejectChanges();
                (DGVPersonnel.DataSource as DataTable).Reset();
                DGVPersonnel.DataSource = null;

            }
            this.CHRIS_Personnel_ConfirmationEntry_Shown(null, null);
            //if (this.FunctionConfig.CurrentOption == Function.None)
            //{
            //    if (base.tlbMain.Items.Count > 0)
            //    {
            //        base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
            //        //base.Close();
            //        //this.Dispose(true);
            //    }
            //    //else

            //}
            //else
            //{
            //    //this.FunctionConfig.CurrentOption = Function.None;
            //    this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
            //}
            return flag;
        }

        private void CallReport(string AuditPreOrAuditPo, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = frm.m_ReportPath;// @"C:\SPOOL\CHRIS\AUDIT\";
            string auditStatus = AuditPreOrAuditPo + DateTime.Now.ToString("yyyyMMddHms");

            string FullPath = globalPath + auditStatus;

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "DT";
            txtDT.Text = DateTime.Now.ToString();//this.Now().ToShortDateString();
            pnl.Controls.Add(txtDT);

            CrplControlLibrary.SLTextBox txtMonth = new CrplControlLibrary.SLTextBox(comp);
            txtMonth.Name = "MONTH";
            txtMonth.Text = inputMonth;
            pnl.Controls.Add(txtMonth);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "USER";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "ST";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);


            frm.Controls.Add(pnl);
            frm.RptFileName = "AUDIT11A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");


        }

        #endregion

        # region Events

        private void DGVPersonnel_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DGVPersonnel.SkippingColumns = "";
                if (DGVPersonnel.Rows[e.RowIndex].Cells["PR_P_NO"].Value == DBNull.Value && DGVPersonnel.Rows[e.RowIndex].Cells["PR_P_NO"].Value.ToString() == string.Empty)
                {
                    this.DGVPersonnel.CurrentCell.Value = DBNull.Value;
                    return;
                }
                #region column 3
                if (e.ColumnIndex == 3)
                {
                    if (DGVPersonnel.CurrentRow.Cells["PR_CONF_FLAG"].IsInEditMode)
                    {
                        if ((DGVPersonnel.CurrentRow.Cells["PR_CONF_FLAG"].EditedFormattedValue.ToString().ToUpper() == "Y"))
                        {
                            if ((DGVPersonnel.CurrentRow.Cells["PR_EXPECTED"].Value != null) && (DGVPersonnel.CurrentRow.Cells["PR_EXPECTED"].Value.ToString() != string.Empty))
                            {
                                DGVPersonnel.CurrentRow.Cells["PR_CONFIRM_ON"].Value = DGVPersonnel.CurrentRow.Cells["PR_EXPECTED"].Value;
                            }
                            else
                            {
                                DGVPersonnel.CurrentRow.Cells["PR_CONFIRM_ON"].Value = DGVPersonnel.CurrentRow.Cells["PR_CONFIRM"].Value;
                                DGVPersonnel.CurrentRow.Cells["PR_EXPECTED"].Value = DGVPersonnel.CurrentRow.Cells["PR_CONFIRM"].Value;
                            }

                        }
                        else if (DGVPersonnel.CurrentRow.Cells["PR_CONF_FLAG"].EditedFormattedValue.ToString().ToUpper() == "N")
                        {
                            DGVPersonnel.CurrentRow.Cells["PR_CONFIRM_ON"].Value = DBNull.Value;
                            DGVPersonnel.SkippingColumns = "PR_CONFIRM_ON";
                            //e.Cancel = false;DGVPersonnel.CurrentCell = DGVPersonnel[5, e.RowIndex];
                        }
                        else if (DGVPersonnel.CurrentRow.Cells["PR_CONF_FLAG"].EditedFormattedValue.ToString().ToUpper() != string.Empty && DGVPersonnel.CurrentRow.Cells["PR_CONF_FLAG"].EditedFormattedValue.ToString().ToUpper() != "Y" && DGVPersonnel.CurrentRow.Cells["PR_CONF_FLAG"].EditedFormattedValue.ToString().ToUpper() != "N")
                        {
                            e.Cancel = true;
                        }
                    }

                    //if ((DGVPersonnel.CurrentRow.Cells["PR_CONF_FLAG"].EditedFormattedValue.ToString().ToUpper() == "Y") && ((DGVPersonnel.CurrentRow.Cells["PR_EXPECTED"].Value == null) || (DGVPersonnel.CurrentRow.Cells["PR_EXPECTED"].EditedFormattedValue.ToString() == string.Empty) ))
                    //{
                    //    if (!((DGVPersonnel.CurrentRow.Cells["PR_CONFIRM_ON"].Value != null)&&(DGVPersonnel.CurrentRow.Cells["PR_CONFIRM_ON"].Value.ToString() != string.Empty)))
                    //    {
                    //       // 
                    //       // string pr_confrim_on = DGVPersonnel.CurrentRow.Cells["PR_CONFIRM_ON"].EditedFormattedValue.ToString();
                    //        //fix
                    //       // string pr_confrim_on = DGVPersonnel.CurrentRow.Cells["PR_CONFIRM"].EditedFormattedValue.ToString();

                    //        DGVPersonnel.CurrentRow.Cells["PR_CONFIRM_ON"].Value = DGVPersonnel.CurrentRow.Cells["PR_CONFIRM"].Value;
                    //        DGVPersonnel.CurrentRow.Cells["pr_expected"].Value = DGVPersonnel.CurrentRow.Cells["PR_CONFIRM"].Value;
                    //    }
                    //}
                    //else
                    //    if ((DGVPersonnel.CurrentRow.Cells["PR_CONF_FLAG"].EditedFormattedValue.ToString() == "Y") && ((DGVPersonnel.CurrentRow.Cells["PR_EXPECTED"].EditedFormattedValue != null) || (DGVPersonnel.CurrentRow.Cells["PR_EXPECTED"].EditedFormattedValue.ToString() != string.Empty) || (DGVPersonnel.CurrentRow.Cells["PR_EXPECTED"].EditedFormattedValue.ToString() != "")))
                    //    {
                    //        string pr_expected = DGVPersonnel.CurrentRow.Cells["pr_expected"].EditedFormattedValue.ToString();
                    //        DGVPersonnel.CurrentRow.Cells["pr_confirm_on"].Value = pr_expected;


                    //    }
                    //    else
                    //        if (DGVPersonnel.CurrentRow.Cells["PR_CONF_FLAG"].EditedFormattedValue.ToString() == "N")
                    //        {
                    //            DGVPersonnel.CurrentRow.Cells["pr_confirm_on"].Value = Convert.DBNull;

                    //        }
                }
                #endregion endcolumn 3
                #region column 4

                if (e.ColumnIndex == 4)
                {
                    if (DGVPersonnel.CurrentRow.Cells["Pr_confirm_on"].IsInEditMode)
                    {
                        if (DGVPersonnel.CurrentRow.Cells["Pr_confirm_on"].EditedFormattedValue != null && DGVPersonnel.CurrentRow.Cells["Pr_confirm_on"].EditedFormattedValue.ToString() != "")
                        {
                            System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                            dtf.ShortDatePattern = "dd/MM/yyyy";
                            DateTime dtTemp = new DateTime();
                            if (DateTime.TryParse(DGVPersonnel.CurrentRow.Cells["Pr_confirm_on"].EditedFormattedValue.ToString(), out dtTemp))
                            {
                                DateTime Pr_confirm_on_dt_4 = Convert.ToDateTime(DGVPersonnel.CurrentRow.Cells["Pr_confirm_on"].EditedFormattedValue.ToString());
                                DateTime pr_confirm_dt_4 = Convert.ToDateTime(DGVPersonnel.CurrentRow.Cells["pr_confirm"].EditedFormattedValue.ToString());
                                string Pr_expected_dt_4 = DGVPersonnel.CurrentRow.Cells["pr_expected"].EditedFormattedValue.ToString();

                                TimeSpan ts = Pr_confirm_on_dt_4.Subtract(pr_confirm_dt_4);
                                int days = ts.Days;

                                if ((days < 0) && ((Pr_expected_dt_4 == null) || (Pr_expected_dt_4 == string.Empty) || (Pr_expected_dt_4 == "")))
                                {
                                    MessageBox.Show("Confirmation Is Less Than Due Date ....!");

                                }
                            }

                        }
                    }
                }


                #endregion end column4
                #region column 5
                {
                    if (e.ColumnIndex == 5)
                    {
                        DateTime dTime = new DateTime(1900, 1, 1);
                        DateTime dt_Maxdt = this.CurrentDate;
                        int days;
                        string pr_expected = DGVPersonnel.CurrentRow.Cells["pr_expected"].EditedFormattedValue.ToString();


                        try
                        {
                            if (DGVPersonnel.CurrentRow.Cells["pr_expected"].IsInEditMode)
                            {
                                if ((pr_expected != null) || (pr_expected != string.Empty) || (pr_expected != ""))
                                {
                                    System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                                    dtf.ShortDatePattern = "dd/MM/yyyy";
                                    DateTime pr_confirm_dt_5 = Convert.ToDateTime(DGVPersonnel.CurrentRow.Cells["pr_confirm"].EditedFormattedValue.ToString(), dtf);
                                    DateTime Pr_expected_dt_5 = Convert.ToDateTime(DGVPersonnel.CurrentRow.Cells["pr_expected"].EditedFormattedValue.ToString(), dtf);


                                    // dTime = DateTime.Parse((DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString()), dtf);
                                    //TimeSpan ts = Pr_expected_dt_5.Subtract(pr_confirm_dt_5);  //dTime.Subtract(dt_Maxdt);
                                    //days = ts.Days;
                                    //if (days < 0)
                                    if (Pr_expected_dt_5 < pr_confirm_dt_5)
                                    {
                                        MessageBox.Show("Expected Date Must Be Greater Than Confirmation Date");
                                        e.Cancel = true;
                                    }
                                }

                                else
                                    if ((pr_expected == null) || (pr_expected == string.Empty) || (pr_expected == ""))
                                    { MessageBox.Show("Expected Date Must Be Entered"); }
                            }

                        }
                        catch (Exception ex)
                        { }

                    }

                }

                #endregion column 5
            }
        }

        private void CHRIS_Personnel_ConfirmationEntry_Shown(object sender, EventArgs e)
        {


            //CHRIS_Personnel_ConfirmationEntry_NewCriteria newcriteria = new CHRIS_Personnel_ConfirmationEntry_NewCriteria();
            //newcriteria.ShowDialog();
            DialogResult dr = MessageBox.Show("Press Yes for Confirmation or No to Exit", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dr == DialogResult.Yes)
            {
                MonthInputProcessing();
            }

            else
            {
                this.Close();
            }

        }

        void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            SLTextBox txt = (SLTextBox)sender;
            string text = e.KeyChar.ToString();
            int month = 0;
            try
            {
                if (txt.Text.Trim() != string.Empty)
                {
                    month = Int32.Parse(txt.Text);

                    if (month > 12)
                    {
                        MessageBox.Show("Invalid Month Is Enterd .........!");
                        return;
                    }
                }

                if (e.KeyChar == '\r' || e.KeyChar == '\t')
                {
                    txt.FindForm().Close();
                }
            }
            catch (Exception ee)
            {
                base.LogError("TextBox_KeyPress", ee);
            }

        }

        void TextBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                e.IsInputKey = true;
            }
            //throw new Exception("The method or operation is not implemented.");
        }

        private void DGVPersonnel_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }

        # endregion
    }
}