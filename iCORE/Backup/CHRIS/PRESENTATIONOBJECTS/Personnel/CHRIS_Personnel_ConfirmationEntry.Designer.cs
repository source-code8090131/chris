namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_ConfirmationEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_ConfirmationEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVPersonnel = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_FIRST_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_CONFIRM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_CONF_FLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_CONFIRM_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_EXPECTED = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlPersonnel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPersonnel)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 420);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // PnlPersonnel
            // 
            this.PnlPersonnel.ConcurrentPanels = null;
            this.PnlPersonnel.Controls.Add(this.DGVPersonnel);
            this.PnlPersonnel.DataManager = "iCORE.Common.CommonDataManager";
            this.PnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPersonnel.DependentPanels = null;
            this.PnlPersonnel.DisableDependentLoad = false;
            this.PnlPersonnel.EnableDelete = true;
            this.PnlPersonnel.EnableInsert = false;
            this.PnlPersonnel.EnableQuery = false;
            this.PnlPersonnel.EnableUpdate = true;
            this.PnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelConfirmationCommand";
            this.PnlPersonnel.Location = new System.Drawing.Point(1, 160);
            this.PnlPersonnel.MasterPanel = null;
            this.PnlPersonnel.Name = "PnlPersonnel";
            this.PnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPersonnel.Size = new System.Drawing.Size(656, 254);
            this.PnlPersonnel.SPName = "CHRIS_SP_Personnel_confirmation_MANAGER";
            this.PnlPersonnel.TabIndex = 10;
            // 
            // DGVPersonnel
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVPersonnel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVPersonnel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVPersonnel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_P_NO,
            this.PR_FIRST_NAME,
            this.PR_CONFIRM,
            this.PR_CONF_FLAG,
            this.PR_CONFIRM_ON,
            this.PR_EXPECTED,
            this.ID});
            this.DGVPersonnel.ColumnToHide = null;
            this.DGVPersonnel.ColumnWidth = null;
            this.DGVPersonnel.CustomEnabled = true;
            this.DGVPersonnel.DisplayColumnWrapper = null;
            this.DGVPersonnel.GridDefaultRow = 0;
            this.DGVPersonnel.Location = new System.Drawing.Point(3, 3);
            this.DGVPersonnel.Name = "DGVPersonnel";
            this.DGVPersonnel.ReadOnlyColumns = null;
            this.DGVPersonnel.RequiredColumns = null;
            this.DGVPersonnel.Size = new System.Drawing.Size(644, 248);
            this.DGVPersonnel.SkippingColumns = null;
            this.DGVPersonnel.TabIndex = 0;
            this.DGVPersonnel.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVPersonnel_CellValidating);
            this.DGVPersonnel.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DGVPersonnel_EditingControlShowing);
            // 
            // PR_P_NO
            // 
            this.PR_P_NO.DataPropertyName = "PR_P_NO";
            this.PR_P_NO.HeaderText = "Personnel No.";
            this.PR_P_NO.MaxInputLength = 6;
            this.PR_P_NO.Name = "PR_P_NO";
            this.PR_P_NO.ReadOnly = true;
            // 
            // PR_FIRST_NAME
            // 
            this.PR_FIRST_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PR_FIRST_NAME.DataPropertyName = "PR_FIRST_NAME";
            this.PR_FIRST_NAME.HeaderText = "Name";
            this.PR_FIRST_NAME.MaxInputLength = 20;
            this.PR_FIRST_NAME.Name = "PR_FIRST_NAME";
            this.PR_FIRST_NAME.ReadOnly = true;
            // 
            // PR_CONFIRM
            // 
            this.PR_CONFIRM.DataPropertyName = "PR_CONFIRM";
            dataGridViewCellStyle2.NullValue = null;
            this.PR_CONFIRM.DefaultCellStyle = dataGridViewCellStyle2;
            this.PR_CONFIRM.HeaderText = "Confirmation Due";
            this.PR_CONFIRM.MaxInputLength = 10;
            this.PR_CONFIRM.Name = "PR_CONFIRM";
            this.PR_CONFIRM.ReadOnly = true;
            // 
            // PR_CONF_FLAG
            // 
            this.PR_CONF_FLAG.DataPropertyName = "PR_CONF_FLAG";
            this.PR_CONF_FLAG.HeaderText = "Confirmed (Y/N)";
            this.PR_CONF_FLAG.MaxInputLength = 1;
            this.PR_CONF_FLAG.Name = "PR_CONF_FLAG";
            // 
            // PR_CONFIRM_ON
            // 
            this.PR_CONFIRM_ON.DataPropertyName = "PR_CONFIRM_ON";
            dataGridViewCellStyle3.NullValue = null;
            this.PR_CONFIRM_ON.DefaultCellStyle = dataGridViewCellStyle3;
            this.PR_CONFIRM_ON.HeaderText = "Confirmation Date";
            this.PR_CONFIRM_ON.MaxInputLength = 10;
            this.PR_CONFIRM_ON.Name = "PR_CONFIRM_ON";
            // 
            // PR_EXPECTED
            // 
            this.PR_EXPECTED.DataPropertyName = "PR_EXPECTED";
            dataGridViewCellStyle4.NullValue = null;
            this.PR_EXPECTED.DefaultCellStyle = dataGridViewCellStyle4;
            this.PR_EXPECTED.HeaderText = "Expected Date";
            this.PR_EXPECTED.MaxInputLength = 10;
            this.PR_EXPECTED.Name = "PR_EXPECTED";
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.slTextBox1);
            this.pnlHead.Controls.Add(this.label13);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(0, 95);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(665, 59);
            this.pnlHead.TabIndex = 55;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(521, 5);
            this.slTextBox1.MaxLength = 6;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.ReadOnly = true;
            this.slTextBox1.Size = new System.Drawing.Size(102, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 21;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.Text = "CONFIRMATIONS";
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(246, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(172, 18);
            this.label13.TabIndex = 20;
            this.label13.Text = "C O N F I R M A T I O N S";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(249, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(166, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Personnel System";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(490, 30);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(65, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(490, 5);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(28, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(447, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(431, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 30);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 4);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(15, 2);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(431, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 56;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Personnel_ConfirmationEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 480);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlPersonnel);
            this.CurrentPanelBlock = "PnlPersonnel";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.F6OptionText = "[F6]=Exit Without Save";
            this.F8OptionText = "[F8]=Exit With Save";
            this.Name = "CHRIS_Personnel_ConfirmationEntry";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowF6Option = true;
            this.ShowF8Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "iCORE CHRIS - Confirmation Entry";
            this.Shown += new System.EventHandler(this.CHRIS_Personnel_ConfirmationEntry_Shown);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlPersonnel, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlPersonnel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVPersonnel)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlPersonnel;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVPersonnel;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_FIRST_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_CONFIRM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_CONF_FLAG;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_CONFIRM_ON;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_EXPECTED;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}