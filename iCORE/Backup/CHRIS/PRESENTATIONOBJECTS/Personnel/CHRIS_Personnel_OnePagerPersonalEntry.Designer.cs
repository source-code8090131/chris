namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_OnePagerPersonalEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_OnePagerPersonalEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlChildren = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvChild = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colChildName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDoB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtConfirmOn = new CrplControlLibrary.SLTextBox(this.components);
            this.txtConfirm = new CrplControlLibrary.SLTextBox(this.components);
            this.dtpJoiningDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.lkupBtnRepToNum = new CrplControlLibrary.LookupButton(this.components);
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtFuncTitle2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDesignation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrDept = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRepTo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtReligion = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRName = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrBus = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.txtSalAccnt = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNatTaxNo = new CrplControlLibrary.SLTextBox(this.components);
            this.lbpersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCurrGrp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtGEID = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLastName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFuncTitle = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNO = new CrplControlLibrary.SLTextBox(this.components);
            this.lblOperation = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.dgvEmpHistory = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.prPeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.From = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.To = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Organization_Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Designation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlEmpHistory = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.pnlEducation = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvEducation = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PrEYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Degree = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_College = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_City = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_DEG_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_E_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlCitiReference = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTextBox3 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbOvrLocal = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbFTE = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCity = new CrplControlLibrary.SLTextBox(this.components);
            this.label35 = new System.Windows.Forms.Label();
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtDesig = new CrplControlLibrary.SLTextBox(this.components);
            this.label38 = new System.Windows.Forms.Label();
            this.txtCountry = new CrplControlLibrary.SLTextBox(this.components);
            this.label40 = new System.Windows.Forms.Label();
            this.txtGroup = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDept = new CrplControlLibrary.SLTextBox(this.components);
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRelation = new CrplControlLibrary.SLTextBox(this.components);
            this.label39 = new System.Windows.Forms.Label();
            this.pnlTraining = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvTraining = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.TR_LOC_OVRS = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TR_PROG_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TR_PROG_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TR_FTR_PROG_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TR_FTR_PROG_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TR_COUNTRY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlPersonal = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtPBirth = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox23 = new CrplControlLibrary.SLTextBox(this.components);
            this.slDatePicker1 = new CrplControlLibrary.SLDatePicker(this.components);
            this.label45 = new System.Windows.Forms.Label();
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox4 = new CrplControlLibrary.SLTextBox(this.components);
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.slTextBox5 = new CrplControlLibrary.SLTextBox(this.components);
            this.label48 = new System.Windows.Forms.Label();
            this.slTextBox6 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox7 = new CrplControlLibrary.SLTextBox(this.components);
            this.label49 = new System.Windows.Forms.Label();
            this.slTextBox8 = new CrplControlLibrary.SLTextBox(this.components);
            this.label50 = new System.Windows.Forms.Label();
            this.slTextBox9 = new CrplControlLibrary.SLTextBox(this.components);
            this.label51 = new System.Windows.Forms.Label();
            this.slTextBox10 = new CrplControlLibrary.SLTextBox(this.components);
            this.label52 = new System.Windows.Forms.Label();
            this.slTextBox11 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox12 = new CrplControlLibrary.SLTextBox(this.components);
            this.slDatePicker2 = new CrplControlLibrary.SLDatePicker(this.components);
            this.slTextBox13 = new CrplControlLibrary.SLTextBox(this.components);
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.slTextBox14 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox15 = new CrplControlLibrary.SLTextBox(this.components);
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.slTextBox16 = new CrplControlLibrary.SLTextBox(this.components);
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.slTextBox17 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox18 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox19 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox20 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox21 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox22 = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlChildren.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChild)).BeginInit();
            this.PnlPersonnel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpHistory)).BeginInit();
            this.pnlEmpHistory.SuspendLayout();
            this.pnlEducation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEducation)).BeginInit();
            this.pnlCitiReference.SuspendLayout();
            this.pnlTraining.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTraining)).BeginInit();
            this.pnlPersonal.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(639, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(675, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 572);
            this.panel1.Size = new System.Drawing.Size(675, 47);
            // 
            // pnlChildren
            // 
            this.pnlChildren.ConcurrentPanels = null;
            this.pnlChildren.Controls.Add(this.dgvChild);
            this.pnlChildren.DataManager = null;
            this.pnlChildren.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlChildren.DependentPanels = null;
            this.pnlChildren.DisableDependentLoad = false;
            this.pnlChildren.EnableDelete = true;
            this.pnlChildren.EnableInsert = true;
            this.pnlChildren.EnableQuery = false;
            this.pnlChildren.EnableUpdate = true;
            this.pnlChildren.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ChildrenPersonnelCommand";
            this.pnlChildren.Location = new System.Drawing.Point(190, 455);
            this.pnlChildren.MasterPanel = null;
            this.pnlChildren.Name = "pnlChildren";
            this.pnlChildren.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlChildren.Size = new System.Drawing.Size(345, 93);
            this.pnlChildren.SPName = "CHRIS_CHILDREN_ONEPAGER_MANAGER";
            this.pnlChildren.TabIndex = 27;
            this.pnlChildren.TabStop = true;
            // 
            // dgvChild
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvChild.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvChild.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChild.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colChildName,
            this.colDoB,
            this.colSex});
            this.dgvChild.ColumnToHide = "";
            this.dgvChild.ColumnWidth = null;
            this.dgvChild.CustomEnabled = true;
            this.dgvChild.DisplayColumnWrapper = null;
            this.dgvChild.GridDefaultRow = 0;
            this.dgvChild.Location = new System.Drawing.Point(5, 5);
            this.dgvChild.Name = "dgvChild";
            this.dgvChild.ReadOnlyColumns = null;
            this.dgvChild.RequiredColumns = null;
            this.dgvChild.Size = new System.Drawing.Size(330, 81);
            this.dgvChild.SkippingColumns = "COLDOB";
            this.dgvChild.TabIndex = 27;
            this.dgvChild.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvChild_CellFormatting);
            this.dgvChild.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvChild_CellValidating);
            this.dgvChild.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvChild_KeyPress);
            // 
            // colChildName
            // 
            this.colChildName.DataPropertyName = "PR_CHILD_NAME";
            this.colChildName.FillWeight = 20F;
            this.colChildName.HeaderText = "Children Name";
            this.colChildName.MaxInputLength = 30;
            this.colChildName.Name = "colChildName";
            this.colChildName.Width = 160;
            // 
            // colDoB
            // 
            this.colDoB.DataPropertyName = "PR_DATE_BIRTH";
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            this.colDoB.DefaultCellStyle = dataGridViewCellStyle2;
            this.colDoB.FillWeight = 20F;
            this.colDoB.HeaderText = "Date of Birth";
            this.colDoB.MaxInputLength = 11;
            this.colDoB.Name = "colDoB";
            this.colDoB.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDoB.Width = 130;
            // 
            // colSex
            // 
            this.colSex.DataPropertyName = "PR_CH_SEX";
            this.colSex.FillWeight = 20F;
            this.colSex.HeaderText = "Sex";
            this.colSex.MaxInputLength = 1;
            this.colSex.Name = "colSex";
            this.colSex.Width = 50;
            // 
            // PnlPersonnel
            // 
            this.PnlPersonnel.ConcurrentPanels = null;
            this.PnlPersonnel.Controls.Add(this.txtConfirmOn);
            this.PnlPersonnel.Controls.Add(this.txtConfirm);
            this.PnlPersonnel.Controls.Add(this.dtpJoiningDate);
            this.PnlPersonnel.Controls.Add(this.lkupBtnRepToNum);
            this.PnlPersonnel.Controls.Add(this.label26);
            this.PnlPersonnel.Controls.Add(this.label25);
            this.PnlPersonnel.Controls.Add(this.label24);
            this.PnlPersonnel.Controls.Add(this.txtFuncTitle2);
            this.PnlPersonnel.Controls.Add(this.label12);
            this.PnlPersonnel.Controls.Add(this.label14);
            this.PnlPersonnel.Controls.Add(this.txtDesignation);
            this.PnlPersonnel.Controls.Add(this.txtCurrDept);
            this.PnlPersonnel.Controls.Add(this.label10);
            this.PnlPersonnel.Controls.Add(this.label11);
            this.PnlPersonnel.Controls.Add(this.txtRepTo);
            this.PnlPersonnel.Controls.Add(this.txtReligion);
            this.PnlPersonnel.Controls.Add(this.txtRName);
            this.PnlPersonnel.Controls.Add(this.label8);
            this.PnlPersonnel.Controls.Add(this.txtLevel);
            this.PnlPersonnel.Controls.Add(this.txtCurrBus);
            this.PnlPersonnel.Controls.Add(this.label17);
            this.PnlPersonnel.Controls.Add(this.txtSalAccnt);
            this.PnlPersonnel.Controls.Add(this.txtNatTaxNo);
            this.PnlPersonnel.Controls.Add(this.lbpersonnel);
            this.PnlPersonnel.Controls.Add(this.label7);
            this.PnlPersonnel.Controls.Add(this.label6);
            this.PnlPersonnel.Controls.Add(this.label5);
            this.PnlPersonnel.Controls.Add(this.label4);
            this.PnlPersonnel.Controls.Add(this.label2);
            this.PnlPersonnel.Controls.Add(this.label3);
            this.PnlPersonnel.Controls.Add(this.label1);
            this.PnlPersonnel.Controls.Add(this.txtCurrGrp);
            this.PnlPersonnel.Controls.Add(this.txtGEID);
            this.PnlPersonnel.Controls.Add(this.txtFirstName);
            this.PnlPersonnel.Controls.Add(this.txtLastName);
            this.PnlPersonnel.Controls.Add(this.txtFuncTitle);
            this.PnlPersonnel.Controls.Add(this.txtPersonnelNO);
            this.PnlPersonnel.DataManager = "iCORE.Common.CommonDataManager";
            this.PnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPersonnel.DependentPanels = null;
            this.PnlPersonnel.DisableDependentLoad = false;
            this.PnlPersonnel.EnableDelete = true;
            this.PnlPersonnel.EnableInsert = true;
            this.PnlPersonnel.EnableQuery = false;
            this.PnlPersonnel.EnableUpdate = true;
            this.PnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelOnePagerCommand";
            this.PnlPersonnel.Location = new System.Drawing.Point(5, 39);
            this.PnlPersonnel.MasterPanel = null;
            this.PnlPersonnel.Name = "PnlPersonnel";
            this.PnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPersonnel.Size = new System.Drawing.Size(650, 170);
            this.PnlPersonnel.SPName = "CHRIS_PERSONNEL_ONEPAGER_MANAGER";
            this.PnlPersonnel.TabIndex = 0;
            this.PnlPersonnel.TabStop = true;
            // 
            // txtConfirmOn
            // 
            this.txtConfirmOn.AllowSpace = true;
            this.txtConfirmOn.AssociatedLookUpName = "";
            this.txtConfirmOn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfirmOn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConfirmOn.ContinuationTextBox = null;
            this.txtConfirmOn.CustomEnabled = true;
            this.txtConfirmOn.DataFieldMapping = "PR_CONFIRM_ON";
            this.txtConfirmOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirmOn.GetRecordsOnUpDownKeys = false;
            this.txtConfirmOn.IsDate = false;
            this.txtConfirmOn.Location = new System.Drawing.Point(311, 74);
            this.txtConfirmOn.MaxLength = 10;
            this.txtConfirmOn.Name = "txtConfirmOn";
            this.txtConfirmOn.NumberFormat = "###,###,##0.00";
            this.txtConfirmOn.Postfix = "";
            this.txtConfirmOn.Prefix = "";
            this.txtConfirmOn.ReadOnly = true;
            this.txtConfirmOn.Size = new System.Drawing.Size(72, 20);
            this.txtConfirmOn.SkipValidation = false;
            this.txtConfirmOn.TabIndex = 148;
            this.txtConfirmOn.TabStop = false;
            this.txtConfirmOn.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtConfirmOn, "Confirm On");
            // 
            // txtConfirm
            // 
            this.txtConfirm.AllowSpace = true;
            this.txtConfirm.AssociatedLookUpName = "";
            this.txtConfirm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfirm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConfirm.ContinuationTextBox = null;
            this.txtConfirm.CustomEnabled = true;
            this.txtConfirm.DataFieldMapping = "PR_CONFIRM";
            this.txtConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirm.GetRecordsOnUpDownKeys = false;
            this.txtConfirm.IsDate = false;
            this.txtConfirm.Location = new System.Drawing.Point(385, 74);
            this.txtConfirm.MaxLength = 10;
            this.txtConfirm.Name = "txtConfirm";
            this.txtConfirm.NumberFormat = "###,###,##0.00";
            this.txtConfirm.Postfix = "";
            this.txtConfirm.Prefix = "";
            this.txtConfirm.ReadOnly = true;
            this.txtConfirm.Size = new System.Drawing.Size(72, 20);
            this.txtConfirm.SkipValidation = false;
            this.txtConfirm.TabIndex = 149;
            this.txtConfirm.TabStop = false;
            this.txtConfirm.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtConfirm, "Confirm Date");
            // 
            // dtpJoiningDate
            // 
            this.dtpJoiningDate.CustomEnabled = true;
            this.dtpJoiningDate.CustomFormat = "dd/MM/yyyy";
            this.dtpJoiningDate.DataFieldMapping = "PR_JOINING_DATE";
            this.dtpJoiningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpJoiningDate.HasChanges = true;
            this.dtpJoiningDate.Location = new System.Drawing.Point(311, 98);
            this.dtpJoiningDate.Name = "dtpJoiningDate";
            this.dtpJoiningDate.NullValue = " ";
            this.dtpJoiningDate.Size = new System.Drawing.Size(80, 20);
            this.dtpJoiningDate.TabIndex = 141;
            this.dtpJoiningDate.Value = new System.DateTime(2011, 1, 12, 0, 0, 0, 0);
            // 
            // lkupBtnRepToNum
            // 
            this.lkupBtnRepToNum.ActionLOVExists = "";
            this.lkupBtnRepToNum.ActionType = "REP_LOV";
            this.lkupBtnRepToNum.ConditionalFields = "";
            this.lkupBtnRepToNum.CustomEnabled = true;
            this.lkupBtnRepToNum.DataFieldMapping = "";
            this.lkupBtnRepToNum.DependentLovControls = "";
            this.lkupBtnRepToNum.HiddenColumns = resources.GetString("lkupBtnRepToNum.HiddenColumns");
            this.lkupBtnRepToNum.Image = ((System.Drawing.Image)(resources.GetObject("lkupBtnRepToNum.Image")));
            this.lkupBtnRepToNum.LoadDependentEntities = false;
            this.lkupBtnRepToNum.Location = new System.Drawing.Point(412, 6);
            this.lkupBtnRepToNum.LookUpTitle = null;
            this.lkupBtnRepToNum.Name = "lkupBtnRepToNum";
            this.lkupBtnRepToNum.Size = new System.Drawing.Size(26, 21);
            this.lkupBtnRepToNum.SkipValidationOnLeave = true;
            this.lkupBtnRepToNum.SPName = "CHRIS_SP_ONEPAGERPERSONALENTRY_MANAGER";
            this.lkupBtnRepToNum.TabIndex = 134;
            this.lkupBtnRepToNum.TabStop = false;
            this.lkupBtnRepToNum.UseVisualStyleBackColor = true;
            this.lkupBtnRepToNum.Visible = false;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(270, 126);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(38, 13);
            this.label26.TabIndex = 115;
            this.label26.Text = "Level";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(231, 102);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 13);
            this.label25.TabIndex = 113;
            this.label25.Text = "Joining Date";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(201, 78);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(108, 13);
            this.label24.TabIndex = 110;
            this.label24.Text = "Confirmation Date";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFuncTitle2
            // 
            this.txtFuncTitle2.AllowSpace = true;
            this.txtFuncTitle2.AssociatedLookUpName = "";
            this.txtFuncTitle2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFuncTitle2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFuncTitle2.ContinuationTextBox = null;
            this.txtFuncTitle2.CustomEnabled = true;
            this.txtFuncTitle2.DataFieldMapping = "PR_FUNC_TITTLE2";
            this.txtFuncTitle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuncTitle2.GetRecordsOnUpDownKeys = false;
            this.txtFuncTitle2.IsDate = false;
            this.txtFuncTitle2.Location = new System.Drawing.Point(270, 51);
            this.txtFuncTitle2.MaxLength = 30;
            this.txtFuncTitle2.Name = "txtFuncTitle2";
            this.txtFuncTitle2.NumberFormat = "###,###,##0.00";
            this.txtFuncTitle2.Postfix = "";
            this.txtFuncTitle2.Prefix = "";
            this.txtFuncTitle2.Size = new System.Drawing.Size(168, 20);
            this.txtFuncTitle2.SkipValidation = false;
            this.txtFuncTitle2.TabIndex = 3;
            this.txtFuncTitle2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(393, 123);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(117, 13);
            this.label12.TabIndex = 96;
            this.label12.Text = "Current Department";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(436, 101);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 95;
            this.label14.Text = "Designation";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDesignation
            // 
            this.txtDesignation.AllowSpace = true;
            this.txtDesignation.AssociatedLookUpName = "";
            this.txtDesignation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesignation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesignation.ContinuationTextBox = null;
            this.txtDesignation.CustomEnabled = true;
            this.txtDesignation.DataFieldMapping = "DESIG";
            this.txtDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesignation.GetRecordsOnUpDownKeys = false;
            this.txtDesignation.IsDate = false;
            this.txtDesignation.Location = new System.Drawing.Point(510, 96);
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.NumberFormat = "###,###,##0.00";
            this.txtDesignation.Postfix = "";
            this.txtDesignation.Prefix = "";
            this.txtDesignation.ReadOnly = true;
            this.txtDesignation.Size = new System.Drawing.Size(78, 20);
            this.txtDesignation.SkipValidation = false;
            this.txtDesignation.TabIndex = 94;
            this.txtDesignation.TabStop = false;
            this.txtDesignation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrDept
            // 
            this.txtCurrDept.AllowSpace = true;
            this.txtCurrDept.AssociatedLookUpName = "";
            this.txtCurrDept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrDept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrDept.ContinuationTextBox = null;
            this.txtCurrDept.CustomEnabled = true;
            this.txtCurrDept.DataFieldMapping = "DEPT";
            this.txtCurrDept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrDept.GetRecordsOnUpDownKeys = false;
            this.txtCurrDept.IsDate = false;
            this.txtCurrDept.Location = new System.Drawing.Point(510, 120);
            this.txtCurrDept.Name = "txtCurrDept";
            this.txtCurrDept.NumberFormat = "###,###,##0.00";
            this.txtCurrDept.Postfix = "";
            this.txtCurrDept.Prefix = "";
            this.txtCurrDept.ReadOnly = true;
            this.txtCurrDept.Size = new System.Drawing.Size(78, 20);
            this.txtCurrDept.SkipValidation = false;
            this.txtCurrDept.TabIndex = 93;
            this.txtCurrDept.TabStop = false;
            this.txtCurrDept.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(457, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 92;
            this.label10.Text = "Religion";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(445, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 91;
            this.label11.Text = "Repto No";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRepTo
            // 
            this.txtRepTo.AllowSpace = true;
            this.txtRepTo.AssociatedLookUpName = "lkupBtnRepToNum";
            this.txtRepTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRepTo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRepTo.ContinuationTextBox = null;
            this.txtRepTo.CustomEnabled = true;
            this.txtRepTo.DataFieldMapping = "PR_REP_NO";
            this.txtRepTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRepTo.GetRecordsOnUpDownKeys = false;
            this.txtRepTo.IsDate = false;
            this.txtRepTo.Location = new System.Drawing.Point(510, 50);
            this.txtRepTo.MaxLength = 6;
            this.txtRepTo.Name = "txtRepTo";
            this.txtRepTo.NumberFormat = "###,###,##0.00";
            this.txtRepTo.Postfix = "";
            this.txtRepTo.Prefix = "";
            this.txtRepTo.Size = new System.Drawing.Size(43, 20);
            this.txtRepTo.SkipValidation = false;
            this.txtRepTo.TabIndex = 4;
            this.txtRepTo.TextType = CrplControlLibrary.TextType.String;
            this.txtRepTo.Leave += new System.EventHandler(this.txtRepTo_Leave);
            this.txtRepTo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRepTo_KeyPress);
            // 
            // txtReligion
            // 
            this.txtReligion.AllowSpace = true;
            this.txtReligion.AssociatedLookUpName = "";
            this.txtReligion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReligion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReligion.ContinuationTextBox = null;
            this.txtReligion.CustomEnabled = true;
            this.txtReligion.DataFieldMapping = "PR_RELIGION";
            this.txtReligion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReligion.GetRecordsOnUpDownKeys = false;
            this.txtReligion.IsDate = false;
            this.txtReligion.Location = new System.Drawing.Point(510, 73);
            this.txtReligion.MaxLength = 15;
            this.txtReligion.Name = "txtReligion";
            this.txtReligion.NumberFormat = "###,###,##0.00";
            this.txtReligion.Postfix = "";
            this.txtReligion.Prefix = "";
            this.txtReligion.Size = new System.Drawing.Size(78, 20);
            this.txtReligion.SkipValidation = false;
            this.txtReligion.TabIndex = 5;
            this.txtReligion.TextType = CrplControlLibrary.TextType.String;
            this.txtReligion.Leave += new System.EventHandler(this.txtReligion_Leave);
            // 
            // txtRName
            // 
            this.txtRName.AllowSpace = true;
            this.txtRName.AssociatedLookUpName = "";
            this.txtRName.BackColor = System.Drawing.SystemColors.Control;
            this.txtRName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRName.ContinuationTextBox = null;
            this.txtRName.CustomEnabled = true;
            this.txtRName.DataFieldMapping = "name";
            this.txtRName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRName.GetRecordsOnUpDownKeys = false;
            this.txtRName.IsDate = false;
            this.txtRName.Location = new System.Drawing.Point(555, 50);
            this.txtRName.MaxLength = 20;
            this.txtRName.Name = "txtRName";
            this.txtRName.NumberFormat = "###,###,##0.00";
            this.txtRName.Postfix = "";
            this.txtRName.Prefix = "";
            this.txtRName.ReadOnly = true;
            this.txtRName.Size = new System.Drawing.Size(88, 20);
            this.txtRName.SkipValidation = false;
            this.txtRName.TabIndex = 88;
            this.txtRName.TabStop = false;
            this.txtRName.TextType = CrplControlLibrary.TextType.String;
            this.txtRName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRName_KeyPress);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(439, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 87;
            this.label8.Text = "Last Name";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "LVL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(311, 122);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(72, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 63;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrBus
            // 
            this.txtCurrBus.AllowSpace = true;
            this.txtCurrBus.AssociatedLookUpName = "";
            this.txtCurrBus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrBus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrBus.ContinuationTextBox = null;
            this.txtCurrBus.CustomEnabled = true;
            this.txtCurrBus.DataFieldMapping = "SEG";
            this.txtCurrBus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrBus.GetRecordsOnUpDownKeys = false;
            this.txtCurrBus.IsDate = false;
            this.txtCurrBus.Location = new System.Drawing.Point(105, 121);
            this.txtCurrBus.Name = "txtCurrBus";
            this.txtCurrBus.NumberFormat = "###,###,##0.00";
            this.txtCurrBus.Postfix = "";
            this.txtCurrBus.Prefix = "";
            this.txtCurrBus.ReadOnly = true;
            this.txtCurrBus.Size = new System.Drawing.Size(96, 20);
            this.txtCurrBus.SkipValidation = false;
            this.txtCurrBus.TabIndex = 171;
            this.txtCurrBus.TabStop = false;
            this.txtCurrBus.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(2, 126);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 13);
            this.label17.TabIndex = 56;
            this.label17.Text = "Current Business";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSalAccnt
            // 
            this.txtSalAccnt.AllowSpace = true;
            this.txtSalAccnt.AssociatedLookUpName = "";
            this.txtSalAccnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSalAccnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSalAccnt.ContinuationTextBox = null;
            this.txtSalAccnt.CustomEnabled = true;
            this.txtSalAccnt.DataFieldMapping = "PR_ACCOUNT_NO";
            this.txtSalAccnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalAccnt.GetRecordsOnUpDownKeys = false;
            this.txtSalAccnt.IsDate = false;
            this.txtSalAccnt.Location = new System.Drawing.Point(105, 74);
            this.txtSalAccnt.Name = "txtSalAccnt";
            this.txtSalAccnt.NumberFormat = "###,###,##0.00";
            this.txtSalAccnt.Postfix = "";
            this.txtSalAccnt.Prefix = "";
            this.txtSalAccnt.ReadOnly = true;
            this.txtSalAccnt.Size = new System.Drawing.Size(96, 20);
            this.txtSalAccnt.SkipValidation = false;
            this.txtSalAccnt.TabIndex = 151;
            this.txtSalAccnt.TabStop = false;
            this.txtSalAccnt.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNatTaxNo
            // 
            this.txtNatTaxNo.AllowSpace = true;
            this.txtNatTaxNo.AssociatedLookUpName = "";
            this.txtNatTaxNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNatTaxNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNatTaxNo.ContinuationTextBox = null;
            this.txtNatTaxNo.CustomEnabled = true;
            this.txtNatTaxNo.DataFieldMapping = "PR_NATIONAL_TAX";
            this.txtNatTaxNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNatTaxNo.GetRecordsOnUpDownKeys = false;
            this.txtNatTaxNo.IsDate = false;
            this.txtNatTaxNo.Location = new System.Drawing.Point(105, 98);
            this.txtNatTaxNo.Name = "txtNatTaxNo";
            this.txtNatTaxNo.NumberFormat = "###,###,##0.00";
            this.txtNatTaxNo.Postfix = "";
            this.txtNatTaxNo.Prefix = "";
            this.txtNatTaxNo.ReadOnly = true;
            this.txtNatTaxNo.Size = new System.Drawing.Size(96, 20);
            this.txtNatTaxNo.SkipValidation = false;
            this.txtNatTaxNo.TabIndex = 161;
            this.txtNatTaxNo.TabStop = false;
            this.txtNatTaxNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbpersonnel
            // 
            this.lbpersonnel.ActionLOVExists = "PP_NO_EXISTS";
            this.lbpersonnel.ActionType = "PP_LOV";
            this.lbpersonnel.ConditionalFields = "";
            this.lbpersonnel.CustomEnabled = true;
            this.lbpersonnel.DataFieldMapping = "";
            this.lbpersonnel.DependentLovControls = "";
            this.lbpersonnel.HiddenColumns = resources.GetString("lbpersonnel.HiddenColumns");
            this.lbpersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lbpersonnel.Image")));
            this.lbpersonnel.LoadDependentEntities = true;
            this.lbpersonnel.Location = new System.Drawing.Point(232, 4);
            this.lbpersonnel.LookUpTitle = null;
            this.lbpersonnel.Name = "lbpersonnel";
            this.lbpersonnel.Size = new System.Drawing.Size(26, 21);
            this.lbpersonnel.SkipValidationOnLeave = false;
            this.lbpersonnel.SPName = "CHRIS_SP_ONEPAGERPERSONALENTRY_MANAGER";
            this.lbpersonnel.TabIndex = 1;
            this.lbpersonnel.TabStop = false;
            this.lbpersonnel.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(18, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Current Group";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(441, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "G.E.I.D. #";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(5, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "National Tax No";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(-1, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Salary Account #";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Functional Title";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "First Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Personnel No.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCurrGrp
            // 
            this.txtCurrGrp.AllowSpace = true;
            this.txtCurrGrp.AssociatedLookUpName = "";
            this.txtCurrGrp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrGrp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrGrp.ContinuationTextBox = null;
            this.txtCurrGrp.CustomEnabled = true;
            this.txtCurrGrp.DataFieldMapping = "GRP";
            this.txtCurrGrp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrGrp.GetRecordsOnUpDownKeys = false;
            this.txtCurrGrp.IsDate = false;
            this.txtCurrGrp.Location = new System.Drawing.Point(105, 144);
            this.txtCurrGrp.Name = "txtCurrGrp";
            this.txtCurrGrp.NumberFormat = "###,###,##0.00";
            this.txtCurrGrp.Postfix = "";
            this.txtCurrGrp.Prefix = "";
            this.txtCurrGrp.ReadOnly = true;
            this.txtCurrGrp.Size = new System.Drawing.Size(96, 20);
            this.txtCurrGrp.SkipValidation = false;
            this.txtCurrGrp.TabIndex = 181;
            this.txtCurrGrp.TabStop = false;
            this.txtCurrGrp.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtGEID
            // 
            this.txtGEID.AllowSpace = true;
            this.txtGEID.AssociatedLookUpName = "";
            this.txtGEID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGEID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGEID.ContinuationTextBox = null;
            this.txtGEID.CustomEnabled = true;
            this.txtGEID.DataFieldMapping = "GEID";
            this.txtGEID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGEID.GetRecordsOnUpDownKeys = false;
            this.txtGEID.IsDate = false;
            this.txtGEID.Location = new System.Drawing.Point(510, 4);
            this.txtGEID.Name = "txtGEID";
            this.txtGEID.NumberFormat = "###,###,##0.00";
            this.txtGEID.Postfix = "";
            this.txtGEID.Prefix = "";
            this.txtGEID.ReadOnly = true;
            this.txtGEID.Size = new System.Drawing.Size(78, 20);
            this.txtGEID.SkipValidation = false;
            this.txtGEID.TabIndex = 1444;
            this.txtGEID.TabStop = false;
            this.txtGEID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtGEID.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(105, 28);
            this.txtFirstName.MaxLength = 20;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.Size = new System.Drawing.Size(119, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 222;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLastName
            // 
            this.txtLastName.AllowSpace = true;
            this.txtLastName.AssociatedLookUpName = "";
            this.txtLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastName.ContinuationTextBox = null;
            this.txtLastName.CustomEnabled = true;
            this.txtLastName.DataFieldMapping = "PR_LAST_NAME";
            this.txtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastName.GetRecordsOnUpDownKeys = false;
            this.txtLastName.IsDate = false;
            this.txtLastName.Location = new System.Drawing.Point(510, 27);
            this.txtLastName.MaxLength = 20;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.NumberFormat = "###,###,##0.00";
            this.txtLastName.Postfix = "";
            this.txtLastName.Prefix = "";
            this.txtLastName.Size = new System.Drawing.Size(78, 20);
            this.txtLastName.SkipValidation = false;
            this.txtLastName.TabIndex = 51;
            this.txtLastName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFuncTitle
            // 
            this.txtFuncTitle.AllowSpace = true;
            this.txtFuncTitle.AssociatedLookUpName = "";
            this.txtFuncTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFuncTitle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFuncTitle.ContinuationTextBox = null;
            this.txtFuncTitle.CustomEnabled = true;
            this.txtFuncTitle.DataFieldMapping = "PR_FUNC_TITTLE1";
            this.txtFuncTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuncTitle.GetRecordsOnUpDownKeys = false;
            this.txtFuncTitle.IsDate = false;
            this.txtFuncTitle.Location = new System.Drawing.Point(105, 51);
            this.txtFuncTitle.MaxLength = 30;
            this.txtFuncTitle.Name = "txtFuncTitle";
            this.txtFuncTitle.NumberFormat = "###,###,##0.00";
            this.txtFuncTitle.Postfix = "";
            this.txtFuncTitle.Prefix = "";
            this.txtFuncTitle.Size = new System.Drawing.Size(163, 20);
            this.txtFuncTitle.SkipValidation = false;
            this.txtFuncTitle.TabIndex = 2;
            this.txtFuncTitle.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnelNO
            // 
            this.txtPersonnelNO.AllowSpace = true;
            this.txtPersonnelNO.AssociatedLookUpName = "lbpersonnel";
            this.txtPersonnelNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNO.ContinuationTextBox = null;
            this.txtPersonnelNO.CustomEnabled = true;
            this.txtPersonnelNO.DataFieldMapping = "PR_P_NO";
            this.txtPersonnelNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNO.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNO.IsDate = false;
            this.txtPersonnelNO.IsRequired = true;
            this.txtPersonnelNO.Location = new System.Drawing.Point(105, 5);
            this.txtPersonnelNO.MaxLength = 6;
            this.txtPersonnelNO.Name = "txtPersonnelNO";
            this.txtPersonnelNO.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNO.Postfix = "";
            this.txtPersonnelNO.Prefix = "";
            this.txtPersonnelNO.Size = new System.Drawing.Size(119, 20);
            this.txtPersonnelNO.SkipValidation = false;
            this.txtPersonnelNO.TabIndex = 0;
            this.txtPersonnelNO.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnelNO.Leave += new System.EventHandler(this.txtPersonnelNO_Leave);
            this.txtPersonnelNO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPersonnelNO_KeyPress);
            // 
            // lblOperation
            // 
            this.lblOperation.AutoSize = true;
            this.lblOperation.BackColor = System.Drawing.Color.Silver;
            this.lblOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperation.Location = new System.Drawing.Point(1, 554);
            this.lblOperation.Name = "lblOperation";
            this.lblOperation.Size = new System.Drawing.Size(670, 15);
            this.lblOperation.TabIndex = 84;
            this.lblOperation.Text = "F6 : Exit    Ctrl + PageDown :   Next Page      Ctrl + PageUp :   Previous Page  " +
                "   New / Cancel : Shift + F7";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(414, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 85;
            this.lblUserName.Text = "User Name  :";
            // 
            // dgvEmpHistory
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmpHistory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvEmpHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmpHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.prPeNo,
            this.From,
            this.To,
            this.Organization_Address,
            this.Designation});
            this.dgvEmpHistory.ColumnToHide = null;
            this.dgvEmpHistory.ColumnWidth = null;
            this.dgvEmpHistory.CustomEnabled = true;
            this.dgvEmpHistory.DisplayColumnWrapper = null;
            this.dgvEmpHistory.GridDefaultRow = 0;
            this.dgvEmpHistory.Location = new System.Drawing.Point(3, 0);
            this.dgvEmpHistory.Name = "dgvEmpHistory";
            this.dgvEmpHistory.ReadOnlyColumns = null;
            this.dgvEmpHistory.RequiredColumns = null;
            this.dgvEmpHistory.Size = new System.Drawing.Size(143, 80);
            this.dgvEmpHistory.SkippingColumns = null;
            this.dgvEmpHistory.TabIndex = 0;
            // 
            // prPeNo
            // 
            this.prPeNo.DataPropertyName = "PR_P_NO";
            this.prPeNo.HeaderText = "";
            this.prPeNo.Name = "prPeNo";
            this.prPeNo.Visible = false;
            this.prPeNo.Width = 20;
            // 
            // From
            // 
            this.From.DataPropertyName = "PR_JOB_FROM";
            dataGridViewCellStyle4.Format = "dd/MM/yyyy";
            this.From.DefaultCellStyle = dataGridViewCellStyle4;
            this.From.HeaderText = "From";
            this.From.MaxInputLength = 10;
            this.From.Name = "From";
            this.From.ToolTipText = "Date Format : dd/MM/yyyy";
            this.From.Width = 20;
            // 
            // To
            // 
            this.To.DataPropertyName = "PR_JOB_TO";
            dataGridViewCellStyle5.Format = "dd/MM/yyyy";
            this.To.DefaultCellStyle = dataGridViewCellStyle5;
            this.To.HeaderText = "To";
            this.To.MaxInputLength = 10;
            this.To.Name = "To";
            this.To.ToolTipText = "Date Format : dd/MM/yyyy";
            this.To.Width = 20;
            // 
            // Organization_Address
            // 
            this.Organization_Address.DataPropertyName = "PR_ORGANIZ";
            this.Organization_Address.HeaderText = "Organization Address";
            this.Organization_Address.MaxInputLength = 50;
            this.Organization_Address.Name = "Organization_Address";
            this.Organization_Address.Width = 50;
            // 
            // Designation
            // 
            this.Designation.DataPropertyName = "PR_DESIG_PR";
            this.Designation.HeaderText = "Designation/Position";
            this.Designation.MaxInputLength = 50;
            this.Designation.Name = "Designation";
            this.Designation.Width = 50;
            // 
            // pnlEmpHistory
            // 
            this.pnlEmpHistory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlEmpHistory.ConcurrentPanels = null;
            this.pnlEmpHistory.Controls.Add(this.dgvEmpHistory);
            this.pnlEmpHistory.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlEmpHistory.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlEmpHistory.DependentPanels = null;
            this.pnlEmpHistory.DisableDependentLoad = false;
            this.pnlEmpHistory.EnableDelete = true;
            this.pnlEmpHistory.EnableInsert = true;
            this.pnlEmpHistory.EnableQuery = false;
            this.pnlEmpHistory.EnableUpdate = true;
            this.pnlEmpHistory.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PreEmpPersonnelCommand";
            this.pnlEmpHistory.Location = new System.Drawing.Point(840, 101);
            this.pnlEmpHistory.MasterPanel = null;
            this.pnlEmpHistory.Name = "pnlEmpHistory";
            this.pnlEmpHistory.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlEmpHistory.Size = new System.Drawing.Size(151, 86);
            this.pnlEmpHistory.SPName = "CHRIS_PRE_EMP_ONEPAGER_MANAGER";
            this.pnlEmpHistory.TabIndex = 88;
            // 
            // pnlEducation
            // 
            this.pnlEducation.ConcurrentPanels = null;
            this.pnlEducation.Controls.Add(this.dgvEducation);
            this.pnlEducation.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlEducation.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlEducation.DependentPanels = null;
            this.pnlEducation.DisableDependentLoad = false;
            this.pnlEducation.EnableDelete = true;
            this.pnlEducation.EnableInsert = true;
            this.pnlEducation.EnableQuery = false;
            this.pnlEducation.EnableUpdate = true;
            this.pnlEducation.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.EducationPersonnelCommand";
            this.pnlEducation.Location = new System.Drawing.Point(840, 207);
            this.pnlEducation.MasterPanel = null;
            this.pnlEducation.Name = "pnlEducation";
            this.pnlEducation.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlEducation.Size = new System.Drawing.Size(151, 90);
            this.pnlEducation.SPName = "CHRIS_EDUCATION_ONEPAGER_MANAGER";
            this.pnlEducation.TabIndex = 89;
            // 
            // dgvEducation
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEducation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvEducation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEducation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PrEYear,
            this.Pr_Degree,
            this.Pr_Grade,
            this.Pr_College,
            this.Pr_City,
            this.PR_DEG_TYPE,
            this.Pr_Country,
            this.Pr_E_No});
            this.dgvEducation.ColumnToHide = null;
            this.dgvEducation.ColumnWidth = null;
            this.dgvEducation.CustomEnabled = true;
            this.dgvEducation.DisplayColumnWrapper = null;
            this.dgvEducation.GridDefaultRow = 0;
            this.dgvEducation.Location = new System.Drawing.Point(3, 3);
            this.dgvEducation.Name = "dgvEducation";
            this.dgvEducation.ReadOnlyColumns = null;
            this.dgvEducation.RequiredColumns = null;
            this.dgvEducation.Size = new System.Drawing.Size(144, 84);
            this.dgvEducation.SkippingColumns = null;
            this.dgvEducation.TabIndex = 11;
            // 
            // PrEYear
            // 
            this.PrEYear.DataPropertyName = "PR_E_YEAR";
            this.PrEYear.HeaderText = "Year";
            this.PrEYear.Name = "PrEYear";
            this.PrEYear.Width = 20;
            // 
            // Pr_Degree
            // 
            this.Pr_Degree.DataPropertyName = "PR_DEGREE";
            this.Pr_Degree.HeaderText = "Degree";
            this.Pr_Degree.Name = "Pr_Degree";
            this.Pr_Degree.Width = 20;
            // 
            // Pr_Grade
            // 
            this.Pr_Grade.DataPropertyName = "PR_GRADE";
            this.Pr_Grade.HeaderText = "Grade";
            this.Pr_Grade.Name = "Pr_Grade";
            this.Pr_Grade.Width = 20;
            // 
            // Pr_College
            // 
            this.Pr_College.DataPropertyName = "PR_COLLAGE";
            this.Pr_College.HeaderText = "College";
            this.Pr_College.Name = "Pr_College";
            this.Pr_College.Width = 30;
            // 
            // Pr_City
            // 
            this.Pr_City.DataPropertyName = "PR_CITY";
            this.Pr_City.HeaderText = "City";
            this.Pr_City.Name = "Pr_City";
            this.Pr_City.Width = 20;
            // 
            // PR_DEG_TYPE
            // 
            this.PR_DEG_TYPE.DataPropertyName = "PR_DEG_TYPE";
            this.PR_DEG_TYPE.HeaderText = "PartTime/Full Time";
            this.PR_DEG_TYPE.Name = "PR_DEG_TYPE";
            this.PR_DEG_TYPE.Width = 30;
            // 
            // Pr_Country
            // 
            this.Pr_Country.DataPropertyName = "PR_COUNTRY";
            this.Pr_Country.HeaderText = "Country";
            this.Pr_Country.Name = "Pr_Country";
            this.Pr_Country.Width = 25;
            // 
            // Pr_E_No
            // 
            this.Pr_E_No.DataPropertyName = "PR_P_NO";
            this.Pr_E_No.HeaderText = "Pr_E_No";
            this.Pr_E_No.Name = "Pr_E_No";
            this.Pr_E_No.Visible = false;
            this.Pr_E_No.Width = 30;
            // 
            // pnlCitiReference
            // 
            this.pnlCitiReference.ConcurrentPanels = null;
            this.pnlCitiReference.Controls.Add(this.slTextBox3);
            this.pnlCitiReference.Controls.Add(this.slTextBox1);
            this.pnlCitiReference.Controls.Add(this.cmbOvrLocal);
            this.pnlCitiReference.Controls.Add(this.cmbFTE);
            this.pnlCitiReference.Controls.Add(this.txtCity);
            this.pnlCitiReference.Controls.Add(this.label35);
            this.pnlCitiReference.Controls.Add(this.txtBranch);
            this.pnlCitiReference.Controls.Add(this.label36);
            this.pnlCitiReference.Controls.Add(this.label37);
            this.pnlCitiReference.Controls.Add(this.txtDesig);
            this.pnlCitiReference.Controls.Add(this.label38);
            this.pnlCitiReference.Controls.Add(this.txtCountry);
            this.pnlCitiReference.Controls.Add(this.label40);
            this.pnlCitiReference.Controls.Add(this.txtGroup);
            this.pnlCitiReference.Controls.Add(this.txtDept);
            this.pnlCitiReference.Controls.Add(this.label41);
            this.pnlCitiReference.Controls.Add(this.label42);
            this.pnlCitiReference.Controls.Add(this.label43);
            this.pnlCitiReference.Controls.Add(this.label44);
            this.pnlCitiReference.Controls.Add(this.txtName);
            this.pnlCitiReference.Controls.Add(this.txtRelation);
            this.pnlCitiReference.DataManager = null;
            this.pnlCitiReference.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlCitiReference.DependentPanels = null;
            this.pnlCitiReference.DisableDependentLoad = false;
            this.pnlCitiReference.EnableDelete = true;
            this.pnlCitiReference.EnableInsert = true;
            this.pnlCitiReference.EnableQuery = false;
            this.pnlCitiReference.EnableUpdate = true;
            this.pnlCitiReference.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CitiEmpRefPersonnelCommand";
            this.pnlCitiReference.Location = new System.Drawing.Point(829, 300);
            this.pnlCitiReference.MasterPanel = null;
            this.pnlCitiReference.Name = "pnlCitiReference";
            this.pnlCitiReference.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlCitiReference.Size = new System.Drawing.Size(316, 164);
            this.pnlCitiReference.SPName = "CHRIS_CITI_EMP_REF_ONEPAGER_MANAGER";
            this.pnlCitiReference.TabIndex = 90;
            // 
            // slTextBox3
            // 
            this.slTextBox3.AllowSpace = true;
            this.slTextBox3.AssociatedLookUpName = "";
            this.slTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox3.ContinuationTextBox = null;
            this.slTextBox3.CustomEnabled = true;
            this.slTextBox3.DataFieldMapping = "CR_OVS_LCL";
            this.slTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox3.GetRecordsOnUpDownKeys = false;
            this.slTextBox3.IsDate = false;
            this.slTextBox3.Location = new System.Drawing.Point(281, 29);
            this.slTextBox3.Name = "slTextBox3";
            this.slTextBox3.NumberFormat = "###,###,##0.00";
            this.slTextBox3.Postfix = "";
            this.slTextBox3.Prefix = "";
            this.slTextBox3.Size = new System.Drawing.Size(32, 20);
            this.slTextBox3.SkipValidation = false;
            this.slTextBox3.TabIndex = 141;
            this.slTextBox3.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "CR_FTE_CONT";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(281, 3);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(32, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 140;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbOvrLocal
            // 
            this.cmbOvrLocal.AllowSpace = true;
            this.cmbOvrLocal.AssociatedLookUpName = "";
            this.cmbOvrLocal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmbOvrLocal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cmbOvrLocal.ContinuationTextBox = null;
            this.cmbOvrLocal.CustomEnabled = true;
            this.cmbOvrLocal.DataFieldMapping = "CR_OVS_LCL";
            this.cmbOvrLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOvrLocal.GetRecordsOnUpDownKeys = false;
            this.cmbOvrLocal.IsDate = false;
            this.cmbOvrLocal.Location = new System.Drawing.Point(90, 82);
            this.cmbOvrLocal.Name = "cmbOvrLocal";
            this.cmbOvrLocal.NumberFormat = "###,###,##0.00";
            this.cmbOvrLocal.Postfix = "";
            this.cmbOvrLocal.Prefix = "";
            this.cmbOvrLocal.Size = new System.Drawing.Size(30, 20);
            this.cmbOvrLocal.SkipValidation = false;
            this.cmbOvrLocal.TabIndex = 139;
            this.cmbOvrLocal.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbFTE
            // 
            this.cmbFTE.AllowSpace = true;
            this.cmbFTE.AssociatedLookUpName = "";
            this.cmbFTE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmbFTE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cmbFTE.ContinuationTextBox = null;
            this.cmbFTE.CustomEnabled = true;
            this.cmbFTE.DataFieldMapping = "CR_FTE_CONT";
            this.cmbFTE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFTE.GetRecordsOnUpDownKeys = false;
            this.cmbFTE.IsDate = false;
            this.cmbFTE.Location = new System.Drawing.Point(90, 38);
            this.cmbFTE.Name = "cmbFTE";
            this.cmbFTE.NumberFormat = "###,###,##0.00";
            this.cmbFTE.Postfix = "";
            this.cmbFTE.Prefix = "";
            this.cmbFTE.Size = new System.Drawing.Size(30, 20);
            this.cmbFTE.SkipValidation = false;
            this.cmbFTE.TabIndex = 138;
            this.cmbFTE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCity
            // 
            this.txtCity.AllowSpace = true;
            this.txtCity.AssociatedLookUpName = "";
            this.txtCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.ContinuationTextBox = null;
            this.txtCity.CustomEnabled = true;
            this.txtCity.DataFieldMapping = "CR_CITY";
            this.txtCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCity.GetRecordsOnUpDownKeys = false;
            this.txtCity.IsDate = false;
            this.txtCity.Location = new System.Drawing.Point(200, 133);
            this.txtCity.Name = "txtCity";
            this.txtCity.NumberFormat = "###,###,##0.00";
            this.txtCity.Postfix = "";
            this.txtCity.Prefix = "";
            this.txtCity.Size = new System.Drawing.Size(30, 20);
            this.txtCity.SkipValidation = false;
            this.txtCity.TabIndex = 136;
            this.txtCity.TextType = CrplControlLibrary.TextType.String;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(166, 138);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 13);
            this.label35.TabIndex = 137;
            this.label35.Text = "City";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "CR_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(279, 125);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.Size = new System.Drawing.Size(37, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 134;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(239, 140);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(47, 13);
            this.label36.TabIndex = 135;
            this.label36.Text = "Branch";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(-13, 87);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(97, 13);
            this.label37.TabIndex = 133;
            this.label37.Text = "Overseas/Local";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDesig
            // 
            this.txtDesig.AllowSpace = true;
            this.txtDesig.AssociatedLookUpName = "";
            this.txtDesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesig.ContinuationTextBox = null;
            this.txtDesig.CustomEnabled = true;
            this.txtDesig.DataFieldMapping = "CR_DESIG";
            this.txtDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesig.GetRecordsOnUpDownKeys = false;
            this.txtDesig.IsDate = false;
            this.txtDesig.Location = new System.Drawing.Point(200, 57);
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.NumberFormat = "###,###,##0.00";
            this.txtDesig.Postfix = "";
            this.txtDesig.Prefix = "";
            this.txtDesig.Size = new System.Drawing.Size(30, 20);
            this.txtDesig.SkipValidation = false;
            this.txtDesig.TabIndex = 131;
            this.txtDesig.TextType = CrplControlLibrary.TextType.String;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(155, 63);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(39, 13);
            this.label38.TabIndex = 130;
            this.label38.Text = "Desig";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCountry
            // 
            this.txtCountry.AllowSpace = true;
            this.txtCountry.AssociatedLookUpName = "";
            this.txtCountry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountry.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountry.ContinuationTextBox = null;
            this.txtCountry.CustomEnabled = true;
            this.txtCountry.DataFieldMapping = "CR_COUNTRY";
            this.txtCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountry.GetRecordsOnUpDownKeys = false;
            this.txtCountry.IsDate = false;
            this.txtCountry.Location = new System.Drawing.Point(90, 130);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.NumberFormat = "###,###,##0.00";
            this.txtCountry.Postfix = "";
            this.txtCountry.Prefix = "";
            this.txtCountry.Size = new System.Drawing.Size(30, 20);
            this.txtCountry.SkipValidation = false;
            this.txtCountry.TabIndex = 125;
            this.txtCountry.TextType = CrplControlLibrary.TextType.String;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(34, 132);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(50, 13);
            this.label40.TabIndex = 126;
            this.label40.Text = "Country";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtGroup
            // 
            this.txtGroup.AllowSpace = true;
            this.txtGroup.AssociatedLookUpName = "";
            this.txtGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGroup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGroup.ContinuationTextBox = null;
            this.txtGroup.CustomEnabled = true;
            this.txtGroup.DataFieldMapping = "CR_GRP";
            this.txtGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGroup.GetRecordsOnUpDownKeys = false;
            this.txtGroup.IsDate = false;
            this.txtGroup.Location = new System.Drawing.Point(90, 60);
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.NumberFormat = "###,###,##0.00";
            this.txtGroup.Postfix = "";
            this.txtGroup.Prefix = "";
            this.txtGroup.Size = new System.Drawing.Size(30, 20);
            this.txtGroup.SkipValidation = false;
            this.txtGroup.TabIndex = 123;
            this.txtGroup.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDept
            // 
            this.txtDept.AllowSpace = true;
            this.txtDept.AssociatedLookUpName = "";
            this.txtDept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDept.ContinuationTextBox = null;
            this.txtDept.CustomEnabled = true;
            this.txtDept.DataFieldMapping = "CR_DEPT";
            this.txtDept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDept.GetRecordsOnUpDownKeys = false;
            this.txtDept.IsDate = false;
            this.txtDept.Location = new System.Drawing.Point(90, 104);
            this.txtDept.Name = "txtDept";
            this.txtDept.NumberFormat = "###,###,##0.00";
            this.txtDept.Postfix = "";
            this.txtDept.Prefix = "";
            this.txtDept.Size = new System.Drawing.Size(30, 20);
            this.txtDept.SkipValidation = false;
            this.txtDept.TabIndex = 124;
            this.txtDept.TextType = CrplControlLibrary.TextType.String;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(43, 108);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(34, 13);
            this.label41.TabIndex = 122;
            this.label41.Text = "Dept";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(43, 64);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(41, 13);
            this.label42.TabIndex = 121;
            this.label42.Text = "Group";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(136, 19);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 13);
            this.label43.TabIndex = 119;
            this.label43.Text = "Relation";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(14, 42);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(70, 13);
            this.label44.TabIndex = 119;
            this.label44.Text = "FTE/CONT";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "lbpersonnel";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "CR_CONT_NAME";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.IsRequired = true;
            this.txtName.Location = new System.Drawing.Point(267, 68);
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.Size = new System.Drawing.Size(37, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 117;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtRelation
            // 
            this.txtRelation.AllowSpace = true;
            this.txtRelation.AssociatedLookUpName = "";
            this.txtRelation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRelation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRelation.ContinuationTextBox = null;
            this.txtRelation.CustomEnabled = true;
            this.txtRelation.DataFieldMapping = "CR_PR_RELATION";
            this.txtRelation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRelation.GetRecordsOnUpDownKeys = false;
            this.txtRelation.IsDate = false;
            this.txtRelation.IsRequired = true;
            this.txtRelation.Location = new System.Drawing.Point(200, 17);
            this.txtRelation.Name = "txtRelation";
            this.txtRelation.NumberFormat = "###,###,##0.00";
            this.txtRelation.Postfix = "";
            this.txtRelation.Prefix = "";
            this.txtRelation.Size = new System.Drawing.Size(30, 20);
            this.txtRelation.SkipValidation = false;
            this.txtRelation.TabIndex = 117;
            this.txtRelation.TextType = CrplControlLibrary.TextType.String;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(-3, 38);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(39, 13);
            this.label39.TabIndex = 129;
            this.label39.Text = "Name";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlTraining
            // 
            this.pnlTraining.ConcurrentPanels = null;
            this.pnlTraining.Controls.Add(this.dgvTraining);
            this.pnlTraining.Controls.Add(this.label39);
            this.pnlTraining.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTraining.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTraining.DependentPanels = null;
            this.pnlTraining.DisableDependentLoad = false;
            this.pnlTraining.EnableDelete = true;
            this.pnlTraining.EnableInsert = true;
            this.pnlTraining.EnableQuery = false;
            this.pnlTraining.EnableUpdate = true;
            this.pnlTraining.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TrainingPersonnelCommand";
            this.pnlTraining.Location = new System.Drawing.Point(866, 480);
            this.pnlTraining.MasterPanel = null;
            this.pnlTraining.Name = "pnlTraining";
            this.pnlTraining.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTraining.Size = new System.Drawing.Size(190, 115);
            this.pnlTraining.SPName = "CHRIS_TRAINING_ONEPAGER_MANAGER";
            this.pnlTraining.TabIndex = 91;
            // 
            // dgvTraining
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTraining.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvTraining.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTraining.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TR_LOC_OVRS,
            this.TR_PROG_NAME,
            this.TR_PROG_DATE,
            this.TR_FTR_PROG_NAME,
            this.TR_FTR_PROG_DATE,
            this.TR_COUNTRY});
            this.dgvTraining.ColumnToHide = null;
            this.dgvTraining.ColumnWidth = null;
            this.dgvTraining.CustomEnabled = true;
            this.dgvTraining.DisplayColumnWrapper = null;
            this.dgvTraining.GridDefaultRow = 0;
            this.dgvTraining.Location = new System.Drawing.Point(0, 3);
            this.dgvTraining.Name = "dgvTraining";
            this.dgvTraining.ReadOnlyColumns = null;
            this.dgvTraining.RequiredColumns = null;
            this.dgvTraining.Size = new System.Drawing.Size(191, 109);
            this.dgvTraining.SkippingColumns = null;
            this.dgvTraining.TabIndex = 11;
            // 
            // TR_LOC_OVRS
            // 
            this.TR_LOC_OVRS.DataPropertyName = "TR_LOC_OVRS";
            this.TR_LOC_OVRS.HeaderText = "Local/Overseas/Dev. Assign ";
            this.TR_LOC_OVRS.Items.AddRange(new object[] {
            "Local",
            "OverSeas",
            "Developmental Assignments"});
            this.TR_LOC_OVRS.MaxDropDownItems = 4;
            this.TR_LOC_OVRS.Name = "TR_LOC_OVRS";
            this.TR_LOC_OVRS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TR_LOC_OVRS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TR_LOC_OVRS.Width = 20;
            // 
            // TR_PROG_NAME
            // 
            this.TR_PROG_NAME.DataPropertyName = "TR_PROG_NAME";
            this.TR_PROG_NAME.HeaderText = "Training Program Name";
            this.TR_PROG_NAME.Name = "TR_PROG_NAME";
            this.TR_PROG_NAME.Width = 35;
            // 
            // TR_PROG_DATE
            // 
            this.TR_PROG_DATE.DataPropertyName = "TR_PROG_DATE";
            this.TR_PROG_DATE.HeaderText = "Training Program Date";
            this.TR_PROG_DATE.Name = "TR_PROG_DATE";
            this.TR_PROG_DATE.Width = 35;
            // 
            // TR_FTR_PROG_NAME
            // 
            this.TR_FTR_PROG_NAME.DataPropertyName = "TR_FTR_PROG_NAME";
            this.TR_FTR_PROG_NAME.HeaderText = "Training Future Program Name";
            this.TR_FTR_PROG_NAME.Name = "TR_FTR_PROG_NAME";
            this.TR_FTR_PROG_NAME.Width = 35;
            // 
            // TR_FTR_PROG_DATE
            // 
            this.TR_FTR_PROG_DATE.DataPropertyName = "TR_FTR_PROG_DATE";
            this.TR_FTR_PROG_DATE.HeaderText = "Training Future Program Date";
            this.TR_FTR_PROG_DATE.Name = "TR_FTR_PROG_DATE";
            this.TR_FTR_PROG_DATE.Width = 35;
            // 
            // TR_COUNTRY
            // 
            this.TR_COUNTRY.DataPropertyName = "TR_COUNTRY";
            this.TR_COUNTRY.HeaderText = "Venue";
            this.TR_COUNTRY.Name = "TR_COUNTRY";
            this.TR_COUNTRY.Width = 35;
            // 
            // pnlPersonal
            // 
            this.pnlPersonal.ConcurrentPanels = null;
            this.pnlPersonal.Controls.Add(this.txtPBirth);
            this.pnlPersonal.Controls.Add(this.slTextBox23);
            this.pnlPersonal.Controls.Add(this.slDatePicker1);
            this.pnlPersonal.Controls.Add(this.label45);
            this.pnlPersonal.Controls.Add(this.slTextBox2);
            this.pnlPersonal.Controls.Add(this.slTextBox4);
            this.pnlPersonal.Controls.Add(this.label46);
            this.pnlPersonal.Controls.Add(this.label47);
            this.pnlPersonal.Controls.Add(this.slTextBox5);
            this.pnlPersonal.Controls.Add(this.label48);
            this.pnlPersonal.Controls.Add(this.slTextBox6);
            this.pnlPersonal.Controls.Add(this.slTextBox7);
            this.pnlPersonal.Controls.Add(this.label49);
            this.pnlPersonal.Controls.Add(this.slTextBox8);
            this.pnlPersonal.Controls.Add(this.label50);
            this.pnlPersonal.Controls.Add(this.slTextBox9);
            this.pnlPersonal.Controls.Add(this.label51);
            this.pnlPersonal.Controls.Add(this.slTextBox10);
            this.pnlPersonal.Controls.Add(this.label52);
            this.pnlPersonal.Controls.Add(this.slTextBox11);
            this.pnlPersonal.Controls.Add(this.slTextBox12);
            this.pnlPersonal.Controls.Add(this.slDatePicker2);
            this.pnlPersonal.Controls.Add(this.slTextBox13);
            this.pnlPersonal.Controls.Add(this.label53);
            this.pnlPersonal.Controls.Add(this.label54);
            this.pnlPersonal.Controls.Add(this.label55);
            this.pnlPersonal.Controls.Add(this.slTextBox14);
            this.pnlPersonal.Controls.Add(this.slTextBox15);
            this.pnlPersonal.Controls.Add(this.label56);
            this.pnlPersonal.Controls.Add(this.label57);
            this.pnlPersonal.Controls.Add(this.slTextBox16);
            this.pnlPersonal.Controls.Add(this.label58);
            this.pnlPersonal.Controls.Add(this.label59);
            this.pnlPersonal.Controls.Add(this.label60);
            this.pnlPersonal.Controls.Add(this.label61);
            this.pnlPersonal.Controls.Add(this.label62);
            this.pnlPersonal.Controls.Add(this.slTextBox17);
            this.pnlPersonal.Controls.Add(this.slTextBox18);
            this.pnlPersonal.Controls.Add(this.slTextBox19);
            this.pnlPersonal.Controls.Add(this.slTextBox20);
            this.pnlPersonal.Controls.Add(this.slTextBox21);
            this.pnlPersonal.Controls.Add(this.slTextBox22);
            this.pnlPersonal.DataManager = null;
            this.pnlPersonal.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPersonal.DependentPanels = null;
            this.pnlPersonal.DisableDependentLoad = false;
            this.pnlPersonal.EnableDelete = true;
            this.pnlPersonal.EnableInsert = true;
            this.pnlPersonal.EnableQuery = false;
            this.pnlPersonal.EnableUpdate = true;
            this.pnlPersonal.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonalPersonnelCommand";
            this.pnlPersonal.Location = new System.Drawing.Point(5, 215);
            this.pnlPersonal.MasterPanel = null;
            this.pnlPersonal.Name = "pnlPersonal";
            this.pnlPersonal.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPersonal.Size = new System.Drawing.Size(650, 238);
            this.pnlPersonal.SPName = "CHRIS_PERSONAL_ONEPAGER_MANAGER";
            this.pnlPersonal.TabIndex = 86;
            // 
            // txtPBirth
            // 
            this.txtPBirth.AllowSpace = true;
            this.txtPBirth.AssociatedLookUpName = "";
            this.txtPBirth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPBirth.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPBirth.ContinuationTextBox = null;
            this.txtPBirth.CustomEnabled = true;
            this.txtPBirth.DataFieldMapping = "PR_PLC_BIRTH";
            this.txtPBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPBirth.GetRecordsOnUpDownKeys = false;
            this.txtPBirth.IsDate = false;
            this.txtPBirth.Location = new System.Drawing.Point(510, 7);
            this.txtPBirth.MaxLength = 30;
            this.txtPBirth.Name = "txtPBirth";
            this.txtPBirth.NumberFormat = "###,###,##0.00";
            this.txtPBirth.Postfix = "";
            this.txtPBirth.Prefix = "";
            this.txtPBirth.Size = new System.Drawing.Size(133, 20);
            this.txtPBirth.SkipValidation = false;
            this.txtPBirth.TabIndex = 7;
            this.txtPBirth.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox23
            // 
            this.slTextBox23.AllowSpace = true;
            this.slTextBox23.AssociatedLookUpName = "";
            this.slTextBox23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox23.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox23.ContinuationTextBox = null;
            this.slTextBox23.CustomEnabled = true;
            this.slTextBox23.DataFieldMapping = "PR_ADD2";
            this.slTextBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox23.GetRecordsOnUpDownKeys = false;
            this.slTextBox23.IsDate = false;
            this.slTextBox23.Location = new System.Drawing.Point(105, 77);
            this.slTextBox23.MaxLength = 30;
            this.slTextBox23.Name = "slTextBox23";
            this.slTextBox23.NumberFormat = "###,###,##0.00";
            this.slTextBox23.Postfix = "";
            this.slTextBox23.Prefix = "";
            this.slTextBox23.Size = new System.Drawing.Size(145, 20);
            this.slTextBox23.SkipValidation = false;
            this.slTextBox23.TabIndex = 13;
            this.slTextBox23.TextType = CrplControlLibrary.TextType.String;
            // 
            // slDatePicker1
            // 
            this.slDatePicker1.CustomEnabled = true;
            this.slDatePicker1.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker1.DataFieldMapping = "PR_BIRTH_SP";
            this.slDatePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker1.HasChanges = true;
            this.slDatePicker1.Location = new System.Drawing.Point(510, 195);
            this.slDatePicker1.Name = "slDatePicker1";
            this.slDatePicker1.NullValue = " ";
            this.slDatePicker1.Size = new System.Drawing.Size(134, 20);
            this.slDatePicker1.TabIndex = 26;
            this.slDatePicker1.Value = new System.DateTime(2011, 1, 12, 0, 0, 0, 0);
            this.slDatePicker1.Leave += new System.EventHandler(this.slDatePicker1_Leave);
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(422, 32);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(88, 13);
            this.label45.TabIndex = 177;
            this.label45.Text = "Current Phone";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "PR_CRNT_PHONE";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(510, 30);
            this.slTextBox2.MaxLength = 40;
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.Size = new System.Drawing.Size(133, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 8;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox4
            // 
            this.slTextBox4.AllowSpace = true;
            this.slTextBox4.AssociatedLookUpName = "";
            this.slTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox4.ContinuationTextBox = null;
            this.slTextBox4.CustomEnabled = true;
            this.slTextBox4.DataFieldMapping = "PR_NO_OF_CHILD";
            this.slTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox4.GetRecordsOnUpDownKeys = false;
            this.slTextBox4.IsDate = false;
            this.slTextBox4.Location = new System.Drawing.Point(105, 216);
            this.slTextBox4.Name = "slTextBox4";
            this.slTextBox4.NumberFormat = "###,###,##0.00";
            this.slTextBox4.Postfix = "";
            this.slTextBox4.Prefix = "";
            this.slTextBox4.ReadOnly = true;
            this.slTextBox4.Size = new System.Drawing.Size(80, 20);
            this.slTextBox4.SkipValidation = false;
            this.slTextBox4.TabIndex = 175;
            this.slTextBox4.TabStop = false;
            this.slTextBox4.TextType = CrplControlLibrary.TextType.String;
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(16, 219);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(88, 13);
            this.label46.TabIndex = 174;
            this.label46.Text = "No of Children";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label47
            // 
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(18, 196);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(85, 13);
            this.label47.TabIndex = 173;
            this.label47.Text = "Spouse Name";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox5
            // 
            this.slTextBox5.AllowSpace = true;
            this.slTextBox5.AssociatedLookUpName = "";
            this.slTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox5.ContinuationTextBox = null;
            this.slTextBox5.CustomEnabled = true;
            this.slTextBox5.DataFieldMapping = "PR_MARITAL";
            this.slTextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox5.GetRecordsOnUpDownKeys = false;
            this.slTextBox5.IsDate = false;
            this.slTextBox5.Location = new System.Drawing.Point(105, 170);
            this.slTextBox5.Name = "slTextBox5";
            this.slTextBox5.NumberFormat = "###,###,##0.00";
            this.slTextBox5.Postfix = "";
            this.slTextBox5.Prefix = "";
            this.slTextBox5.ReadOnly = true;
            this.slTextBox5.Size = new System.Drawing.Size(80, 20);
            this.slTextBox5.SkipValidation = false;
            this.slTextBox5.TabIndex = 172;
            this.slTextBox5.TabStop = false;
            this.slTextBox5.TextType = CrplControlLibrary.TextType.String;
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Red;
            this.label48.Location = new System.Drawing.Point(18, 172);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(85, 13);
            this.label48.TabIndex = 171;
            this.label48.Text = "Marital Status";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox6
            // 
            this.slTextBox6.AllowSpace = true;
            this.slTextBox6.AssociatedLookUpName = "";
            this.slTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox6.ContinuationTextBox = null;
            this.slTextBox6.CustomEnabled = true;
            this.slTextBox6.DataFieldMapping = "PR_LANG_6";
            this.slTextBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox6.GetRecordsOnUpDownKeys = false;
            this.slTextBox6.IsDate = false;
            this.slTextBox6.Location = new System.Drawing.Point(464, 123);
            this.slTextBox6.MaxLength = 10;
            this.slTextBox6.Name = "slTextBox6";
            this.slTextBox6.NumberFormat = "###,###,##0.00";
            this.slTextBox6.Postfix = "";
            this.slTextBox6.Prefix = "";
            this.slTextBox6.Size = new System.Drawing.Size(70, 20);
            this.slTextBox6.SkipValidation = false;
            this.slTextBox6.TabIndex = 22;
            this.slTextBox6.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox7
            // 
            this.slTextBox7.AllowSpace = true;
            this.slTextBox7.AssociatedLookUpName = "";
            this.slTextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox7.ContinuationTextBox = null;
            this.slTextBox7.CustomEnabled = true;
            this.slTextBox7.DataFieldMapping = "PR_LANG_5";
            this.slTextBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox7.GetRecordsOnUpDownKeys = false;
            this.slTextBox7.IsDate = false;
            this.slTextBox7.Location = new System.Drawing.Point(393, 123);
            this.slTextBox7.MaxLength = 10;
            this.slTextBox7.Name = "slTextBox7";
            this.slTextBox7.NumberFormat = "###,###,##0.00";
            this.slTextBox7.Postfix = "";
            this.slTextBox7.Prefix = "";
            this.slTextBox7.Size = new System.Drawing.Size(70, 20);
            this.slTextBox7.SkipValidation = false;
            this.slTextBox7.TabIndex = 21;
            this.slTextBox7.TextType = CrplControlLibrary.TextType.String;
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(385, 198);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(125, 13);
            this.label49.TabIndex = 168;
            this.label49.Text = "Spouse Date of Birth";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox8
            // 
            this.slTextBox8.AllowSpace = true;
            this.slTextBox8.AssociatedLookUpName = "";
            this.slTextBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox8.ContinuationTextBox = null;
            this.slTextBox8.CustomEnabled = true;
            this.slTextBox8.DataFieldMapping = "PR_MARRIAGE";
            this.slTextBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox8.GetRecordsOnUpDownKeys = false;
            this.slTextBox8.IsDate = false;
            this.slTextBox8.Location = new System.Drawing.Point(510, 172);
            this.slTextBox8.Name = "slTextBox8";
            this.slTextBox8.NumberFormat = "###,###,##0.00";
            this.slTextBox8.Postfix = "";
            this.slTextBox8.Prefix = "";
            this.slTextBox8.ReadOnly = true;
            this.slTextBox8.Size = new System.Drawing.Size(134, 20);
            this.slTextBox8.SkipValidation = false;
            this.slTextBox8.TabIndex = 166;
            this.slTextBox8.TabStop = false;
            this.slTextBox8.TextType = CrplControlLibrary.TextType.String;
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Red;
            this.label50.Location = new System.Drawing.Point(408, 174);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(102, 13);
            this.label50.TabIndex = 165;
            this.label50.Text = "Date of Marriage";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox9
            // 
            this.slTextBox9.AllowSpace = true;
            this.slTextBox9.AssociatedLookUpName = "";
            this.slTextBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox9.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox9.ContinuationTextBox = null;
            this.slTextBox9.CustomEnabled = true;
            this.slTextBox9.DataFieldMapping = "PR_MOTHER_NAME";
            this.slTextBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox9.GetRecordsOnUpDownKeys = false;
            this.slTextBox9.IsDate = false;
            this.slTextBox9.Location = new System.Drawing.Point(510, 149);
            this.slTextBox9.MaxLength = 40;
            this.slTextBox9.Name = "slTextBox9";
            this.slTextBox9.NumberFormat = "###,###,##0.00";
            this.slTextBox9.Postfix = "";
            this.slTextBox9.Prefix = "";
            this.slTextBox9.Size = new System.Drawing.Size(134, 20);
            this.slTextBox9.SkipValidation = false;
            this.slTextBox9.TabIndex = 24;
            this.slTextBox9.TextType = CrplControlLibrary.TextType.String;
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(419, 153);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(91, 13);
            this.label51.TabIndex = 163;
            this.label51.Text = "Mother\'s Name";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox10
            // 
            this.slTextBox10.AllowSpace = true;
            this.slTextBox10.AssociatedLookUpName = "";
            this.slTextBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox10.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox10.ContinuationTextBox = null;
            this.slTextBox10.CustomEnabled = true;
            this.slTextBox10.DataFieldMapping = "PR_FATHER_NAME";
            this.slTextBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox10.GetRecordsOnUpDownKeys = false;
            this.slTextBox10.IsDate = false;
            this.slTextBox10.Location = new System.Drawing.Point(105, 146);
            this.slTextBox10.MaxLength = 40;
            this.slTextBox10.Name = "slTextBox10";
            this.slTextBox10.NumberFormat = "###,###,##0.00";
            this.slTextBox10.Postfix = "";
            this.slTextBox10.Prefix = "";
            this.slTextBox10.Size = new System.Drawing.Size(145, 20);
            this.slTextBox10.SkipValidation = false;
            this.slTextBox10.TabIndex = 23;
            this.slTextBox10.TextType = CrplControlLibrary.TextType.String;
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(16, 148);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(88, 13);
            this.label52.TabIndex = 161;
            this.label52.Text = "Father\'s Name";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox11
            // 
            this.slTextBox11.AllowSpace = true;
            this.slTextBox11.AssociatedLookUpName = "";
            this.slTextBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox11.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox11.ContinuationTextBox = null;
            this.slTextBox11.CustomEnabled = true;
            this.slTextBox11.DataFieldMapping = "PR_SPOUSE";
            this.slTextBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox11.GetRecordsOnUpDownKeys = false;
            this.slTextBox11.IsDate = false;
            this.slTextBox11.Location = new System.Drawing.Point(105, 193);
            this.slTextBox11.MaxLength = 30;
            this.slTextBox11.Name = "slTextBox11";
            this.slTextBox11.NumberFormat = "###,###,##0.00";
            this.slTextBox11.Postfix = "";
            this.slTextBox11.Prefix = "";
            this.slTextBox11.Size = new System.Drawing.Size(145, 20);
            this.slTextBox11.SkipValidation = false;
            this.slTextBox11.TabIndex = 25;
            this.slTextBox11.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox11.Leave += new System.EventHandler(this.slTextBox11_Leave);
            // 
            // slTextBox12
            // 
            this.slTextBox12.AllowSpace = true;
            this.slTextBox12.AssociatedLookUpName = "";
            this.slTextBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox12.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox12.ContinuationTextBox = null;
            this.slTextBox12.CustomEnabled = true;
            this.slTextBox12.DataFieldMapping = "PR_MOBILE_NO";
            this.slTextBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox12.GetRecordsOnUpDownKeys = false;
            this.slTextBox12.IsDate = false;
            this.slTextBox12.Location = new System.Drawing.Point(105, 100);
            this.slTextBox12.MaxLength = 30;
            this.slTextBox12.Name = "slTextBox12";
            this.slTextBox12.NumberFormat = "###,###,##0.00";
            this.slTextBox12.Postfix = "";
            this.slTextBox12.Prefix = "";
            this.slTextBox12.Size = new System.Drawing.Size(145, 20);
            this.slTextBox12.SkipValidation = false;
            this.slTextBox12.TabIndex = 16;
            this.slTextBox12.TextType = CrplControlLibrary.TextType.String;
            // 
            // slDatePicker2
            // 
            this.slDatePicker2.CustomEnabled = true;
            this.slDatePicker2.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker2.DataFieldMapping = "PR_D_BIRTH";
            this.slDatePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker2.HasChanges = true;
            this.slDatePicker2.Location = new System.Drawing.Point(105, 7);
            this.slDatePicker2.Name = "slDatePicker2";
            this.slDatePicker2.NullValue = " ";
            this.slDatePicker2.Size = new System.Drawing.Size(98, 20);
            this.slDatePicker2.TabIndex = 6;
            this.slDatePicker2.Value = new System.DateTime(2011, 1, 12, 0, 0, 0, 0);
            // 
            // slTextBox13
            // 
            this.slTextBox13.AllowSpace = true;
            this.slTextBox13.AssociatedLookUpName = "";
            this.slTextBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox13.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox13.ContinuationTextBox = null;
            this.slTextBox13.CustomEnabled = true;
            this.slTextBox13.DataFieldMapping = "PR_PHONE2";
            this.slTextBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox13.GetRecordsOnUpDownKeys = false;
            this.slTextBox13.IsDate = false;
            this.slTextBox13.Location = new System.Drawing.Point(593, 53);
            this.slTextBox13.MaxLength = 15;
            this.slTextBox13.Name = "slTextBox13";
            this.slTextBox13.NumberFormat = "###,###,##0.00";
            this.slTextBox13.Postfix = "";
            this.slTextBox13.Prefix = "";
            this.slTextBox13.Size = new System.Drawing.Size(50, 20);
            this.slTextBox13.SkipValidation = false;
            this.slTextBox13.TabIndex = 11;
            this.slTextBox13.TextType = CrplControlLibrary.TextType.String;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(571, 57);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(13, 13);
            this.label53.TabIndex = 156;
            this.label53.Text = "/";
            // 
            // label54
            // 
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(440, 102);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(70, 13);
            this.label54.TabIndex = 155;
            this.label54.Text = "ID Card No";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(326, 75);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(184, 18);
            this.label55.TabIndex = 154;
            this.label55.Text = "Permanent Residence Address";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox14
            // 
            this.slTextBox14.AllowSpace = true;
            this.slTextBox14.AssociatedLookUpName = "";
            this.slTextBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox14.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox14.ContinuationTextBox = null;
            this.slTextBox14.CustomEnabled = true;
            this.slTextBox14.DataFieldMapping = "PR_PER_ADDR";
            this.slTextBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox14.GetRecordsOnUpDownKeys = false;
            this.slTextBox14.IsDate = false;
            this.slTextBox14.Location = new System.Drawing.Point(510, 76);
            this.slTextBox14.MaxLength = 60;
            this.slTextBox14.Name = "slTextBox14";
            this.slTextBox14.NumberFormat = "###,###,##0.00";
            this.slTextBox14.Postfix = "";
            this.slTextBox14.Prefix = "";
            this.slTextBox14.Size = new System.Drawing.Size(133, 20);
            this.slTextBox14.SkipValidation = false;
            this.slTextBox14.TabIndex = 14;
            this.slTextBox14.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox15
            // 
            this.slTextBox15.AllowSpace = true;
            this.slTextBox15.AssociatedLookUpName = "";
            this.slTextBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox15.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox15.ContinuationTextBox = null;
            this.slTextBox15.CustomEnabled = true;
            this.slTextBox15.DataFieldMapping = "PR_ID_CARD_NO";
            this.slTextBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox15.GetRecordsOnUpDownKeys = false;
            this.slTextBox15.IsDate = false;
            this.slTextBox15.Location = new System.Drawing.Point(510, 99);
            this.slTextBox15.MaxLength = 15;
            this.slTextBox15.Name = "slTextBox15";
            this.slTextBox15.NumberFormat = "###,###,##0.00";
            this.slTextBox15.Postfix = "";
            this.slTextBox15.Prefix = "";
            this.slTextBox15.Size = new System.Drawing.Size(133, 20);
            this.slTextBox15.SkipValidation = false;
            this.slTextBox15.TabIndex = 15;
            this.slTextBox15.TextType = CrplControlLibrary.TextType.String;
            // 
            // label56
            // 
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(327, 57);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(183, 13);
            this.label56.TabIndex = 151;
            this.label56.Text = "Permanent Residence Phone #";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label57
            // 
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(426, 11);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(84, 13);
            this.label57.TabIndex = 150;
            this.label57.Text = "Place of Birth";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox16
            // 
            this.slTextBox16.AllowSpace = true;
            this.slTextBox16.AssociatedLookUpName = "";
            this.slTextBox16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox16.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox16.ContinuationTextBox = null;
            this.slTextBox16.CustomEnabled = true;
            this.slTextBox16.DataFieldMapping = "PR_PHONE1";
            this.slTextBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox16.GetRecordsOnUpDownKeys = false;
            this.slTextBox16.IsDate = false;
            this.slTextBox16.Location = new System.Drawing.Point(510, 53);
            this.slTextBox16.MaxLength = 15;
            this.slTextBox16.Name = "slTextBox16";
            this.slTextBox16.NumberFormat = "###,###,##0.00";
            this.slTextBox16.Postfix = "";
            this.slTextBox16.Prefix = "";
            this.slTextBox16.Size = new System.Drawing.Size(59, 20);
            this.slTextBox16.SkipValidation = false;
            this.slTextBox16.TabIndex = 10;
            this.slTextBox16.TextType = CrplControlLibrary.TextType.String;
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(21, 60);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(82, 13);
            this.label58.TabIndex = 148;
            this.label58.Text = "Curr Res Add";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(39, 127);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(63, 13);
            this.label59.TabIndex = 147;
            this.label59.Text = "Language";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(37, 37);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(67, 13);
            this.label60.TabIndex = 146;
            this.label60.Text = "Nationality";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(40, 104);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(64, 13);
            this.label61.TabIndex = 145;
            this.label61.Text = "Mobile No";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(25, 11);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(79, 13);
            this.label62.TabIndex = 144;
            this.label62.Text = "Date of Birth";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox17
            // 
            this.slTextBox17.AllowSpace = true;
            this.slTextBox17.AssociatedLookUpName = "";
            this.slTextBox17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox17.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox17.ContinuationTextBox = null;
            this.slTextBox17.CustomEnabled = true;
            this.slTextBox17.DataFieldMapping = "PR_LANG_4";
            this.slTextBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox17.GetRecordsOnUpDownKeys = false;
            this.slTextBox17.IsDate = false;
            this.slTextBox17.Location = new System.Drawing.Point(321, 123);
            this.slTextBox17.MaxLength = 10;
            this.slTextBox17.Name = "slTextBox17";
            this.slTextBox17.NumberFormat = "###,###,##0.00";
            this.slTextBox17.Postfix = "";
            this.slTextBox17.Prefix = "";
            this.slTextBox17.Size = new System.Drawing.Size(70, 20);
            this.slTextBox17.SkipValidation = false;
            this.slTextBox17.TabIndex = 20;
            this.slTextBox17.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox18
            // 
            this.slTextBox18.AllowSpace = true;
            this.slTextBox18.AssociatedLookUpName = "";
            this.slTextBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox18.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox18.ContinuationTextBox = null;
            this.slTextBox18.CustomEnabled = true;
            this.slTextBox18.DataFieldMapping = "PR_LANG_3";
            this.slTextBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox18.GetRecordsOnUpDownKeys = false;
            this.slTextBox18.IsDate = false;
            this.slTextBox18.Location = new System.Drawing.Point(249, 123);
            this.slTextBox18.MaxLength = 10;
            this.slTextBox18.Name = "slTextBox18";
            this.slTextBox18.NumberFormat = "###,###,##0.00";
            this.slTextBox18.Postfix = "";
            this.slTextBox18.Prefix = "";
            this.slTextBox18.Size = new System.Drawing.Size(70, 20);
            this.slTextBox18.SkipValidation = false;
            this.slTextBox18.TabIndex = 19;
            this.slTextBox18.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox19
            // 
            this.slTextBox19.AllowSpace = true;
            this.slTextBox19.AssociatedLookUpName = "";
            this.slTextBox19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox19.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox19.ContinuationTextBox = null;
            this.slTextBox19.CustomEnabled = true;
            this.slTextBox19.DataFieldMapping = "PR_LANG_2";
            this.slTextBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox19.GetRecordsOnUpDownKeys = false;
            this.slTextBox19.IsDate = false;
            this.slTextBox19.Location = new System.Drawing.Point(177, 123);
            this.slTextBox19.MaxLength = 10;
            this.slTextBox19.Name = "slTextBox19";
            this.slTextBox19.NumberFormat = "###,###,##0.00";
            this.slTextBox19.Postfix = "";
            this.slTextBox19.Prefix = "";
            this.slTextBox19.Size = new System.Drawing.Size(70, 20);
            this.slTextBox19.SkipValidation = false;
            this.slTextBox19.TabIndex = 18;
            this.slTextBox19.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox20
            // 
            this.slTextBox20.AllowSpace = true;
            this.slTextBox20.AssociatedLookUpName = "";
            this.slTextBox20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox20.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox20.ContinuationTextBox = null;
            this.slTextBox20.CustomEnabled = true;
            this.slTextBox20.DataFieldMapping = "PR_LANG_1";
            this.slTextBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox20.GetRecordsOnUpDownKeys = false;
            this.slTextBox20.IsDate = false;
            this.slTextBox20.Location = new System.Drawing.Point(105, 123);
            this.slTextBox20.MaxLength = 10;
            this.slTextBox20.Name = "slTextBox20";
            this.slTextBox20.NumberFormat = "###,###,##0.00";
            this.slTextBox20.Postfix = "";
            this.slTextBox20.Prefix = "";
            this.slTextBox20.Size = new System.Drawing.Size(70, 20);
            this.slTextBox20.SkipValidation = false;
            this.slTextBox20.TabIndex = 17;
            this.slTextBox20.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox21
            // 
            this.slTextBox21.AllowSpace = true;
            this.slTextBox21.AssociatedLookUpName = "";
            this.slTextBox21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox21.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox21.ContinuationTextBox = null;
            this.slTextBox21.CustomEnabled = true;
            this.slTextBox21.DataFieldMapping = "PR_NATIONALITY";
            this.slTextBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox21.GetRecordsOnUpDownKeys = false;
            this.slTextBox21.IsDate = false;
            this.slTextBox21.Location = new System.Drawing.Point(105, 31);
            this.slTextBox21.MaxLength = 15;
            this.slTextBox21.Name = "slTextBox21";
            this.slTextBox21.NumberFormat = "###,###,##0.00";
            this.slTextBox21.Postfix = "";
            this.slTextBox21.Prefix = "";
            this.slTextBox21.Size = new System.Drawing.Size(145, 20);
            this.slTextBox21.SkipValidation = false;
            this.slTextBox21.TabIndex = 9;
            this.slTextBox21.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox22
            // 
            this.slTextBox22.AllowSpace = true;
            this.slTextBox22.AssociatedLookUpName = "";
            this.slTextBox22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox22.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox22.ContinuationTextBox = null;
            this.slTextBox22.CustomEnabled = true;
            this.slTextBox22.DataFieldMapping = "PR_ADD1";
            this.slTextBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox22.GetRecordsOnUpDownKeys = false;
            this.slTextBox22.IsDate = false;
            this.slTextBox22.Location = new System.Drawing.Point(105, 54);
            this.slTextBox22.MaxLength = 30;
            this.slTextBox22.Name = "slTextBox22";
            this.slTextBox22.NumberFormat = "###,###,##0.00";
            this.slTextBox22.Postfix = "";
            this.slTextBox22.Prefix = "";
            this.slTextBox22.Size = new System.Drawing.Size(145, 20);
            this.slTextBox22.SkipValidation = false;
            this.slTextBox22.TabIndex = 12;
            this.slTextBox22.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_Personnel_OnePagerPersonalEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 619);
            this.Controls.Add(this.pnlTraining);
            this.Controls.Add(this.pnlCitiReference);
            this.Controls.Add(this.pnlEducation);
            this.Controls.Add(this.pnlEmpHistory);
            this.Controls.Add(this.pnlPersonal);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.lblOperation);
            this.Controls.Add(this.pnlChildren);
            this.Controls.Add(this.PnlPersonnel);
            this.Name = "CHRIS_Personnel_OnePagerPersonalEntry";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iCore CHRIS - One Pager Personnel Entry";
            this.TopMost = true;
            this.Shown += new System.EventHandler(this.CHRIS_Personnel_OnePagerPersonalEntry_Shown);
            this.MdiChildActivate += new System.EventHandler(this.CHRIS_Personnel_OnePagerPersonalEntry_MdiChildActivate);
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_OnePagerPersonalEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.PnlPersonnel, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlChildren, 0);
            this.Controls.SetChildIndex(this.lblOperation, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.pnlPersonal, 0);
            this.Controls.SetChildIndex(this.pnlEmpHistory, 0);
            this.Controls.SetChildIndex(this.pnlEducation, 0);
            this.Controls.SetChildIndex(this.pnlCitiReference, 0);
            this.Controls.SetChildIndex(this.pnlTraining, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlChildren.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvChild)).EndInit();
            this.PnlPersonnel.ResumeLayout(false);
            this.PnlPersonnel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpHistory)).EndInit();
            this.pnlEmpHistory.ResumeLayout(false);
            this.pnlEducation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEducation)).EndInit();
            this.pnlCitiReference.ResumeLayout(false);
            this.pnlCitiReference.PerformLayout();
            this.pnlTraining.ResumeLayout(false);
            this.pnlTraining.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTraining)).EndInit();
            this.pnlPersonal.ResumeLayout(false);
            this.pnlPersonal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlChildren;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvChild;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlPersonnel;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtCurrBus;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLTextBox txtSalAccnt;
        private CrplControlLibrary.SLTextBox txtNatTaxNo;
        private CrplControlLibrary.LookupButton lbpersonnel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtCurrGrp;
        private CrplControlLibrary.SLTextBox txtGEID;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private CrplControlLibrary.SLTextBox txtLastName;
        private CrplControlLibrary.SLTextBox txtFuncTitle;
        private System.Windows.Forms.Label lblOperation;
        private CrplControlLibrary.SLTextBox txtFuncTitle2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDesignation;
        private CrplControlLibrary.SLTextBox txtCurrDept;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtRepTo;
        private CrplControlLibrary.SLTextBox txtReligion;
        private CrplControlLibrary.SLTextBox txtRName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private CrplControlLibrary.LookupButton lkupBtnRepToNum;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvEmpHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn prPeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn From;
        private System.Windows.Forms.DataGridViewTextBoxColumn To;
        private System.Windows.Forms.DataGridViewTextBoxColumn Organization_Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn Designation;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlEmpHistory;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlEducation;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvEducation;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrEYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Degree;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_College;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_City;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DEG_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_E_No;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTraining;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvTraining;
        public iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlCitiReference;
        public CrplControlLibrary.SLTextBox txtGroup;
        public CrplControlLibrary.SLTextBox txtDept;
        public CrplControlLibrary.SLTextBox txtCity;
        public CrplControlLibrary.SLTextBox txtDesig;
        public CrplControlLibrary.SLTextBox txtCountry;
        public CrplControlLibrary.SLTextBox txtRelation;
        public CrplControlLibrary.SLTextBox txtBranch;
        public CrplControlLibrary.SLTextBox txtName;
        public CrplControlLibrary.SLTextBox txtPersonnelNO;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPersonal;
        private CrplControlLibrary.SLDatePicker slDatePicker1;
        private System.Windows.Forms.Label label45;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private CrplControlLibrary.SLTextBox slTextBox4;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private CrplControlLibrary.SLTextBox slTextBox5;
        private System.Windows.Forms.Label label48;
        private CrplControlLibrary.SLTextBox slTextBox6;
        private CrplControlLibrary.SLTextBox slTextBox7;
        private System.Windows.Forms.Label label49;
        private CrplControlLibrary.SLTextBox slTextBox8;
        private System.Windows.Forms.Label label50;
        private CrplControlLibrary.SLTextBox slTextBox9;
        private System.Windows.Forms.Label label51;
        private CrplControlLibrary.SLTextBox slTextBox10;
        private System.Windows.Forms.Label label52;
        private CrplControlLibrary.SLTextBox slTextBox11;
        private CrplControlLibrary.SLTextBox slTextBox12;
        private CrplControlLibrary.SLDatePicker slDatePicker2;
        private CrplControlLibrary.SLTextBox slTextBox13;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private CrplControlLibrary.SLTextBox slTextBox14;
        private CrplControlLibrary.SLTextBox slTextBox15;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private CrplControlLibrary.SLTextBox slTextBox16;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private CrplControlLibrary.SLTextBox slTextBox17;
        private CrplControlLibrary.SLTextBox slTextBox18;
        private CrplControlLibrary.SLTextBox slTextBox19;
        private CrplControlLibrary.SLTextBox slTextBox20;
        private CrplControlLibrary.SLTextBox slTextBox21;
        private CrplControlLibrary.SLTextBox slTextBox22;
        private CrplControlLibrary.SLTextBox slTextBox23;
        private CrplControlLibrary.SLTextBox txtPBirth;
        private CrplControlLibrary.SLDatePicker dtpJoiningDate;
        private CrplControlLibrary.SLTextBox txtConfirm;
        private CrplControlLibrary.SLTextBox txtConfirmOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChildName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDoB;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSex;
        public CrplControlLibrary.SLTextBox cmbOvrLocal;
        public CrplControlLibrary.SLTextBox cmbFTE;
        private System.Windows.Forms.DataGridViewComboBoxColumn TR_LOC_OVRS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_PROG_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_PROG_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_FTR_PROG_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_FTR_PROG_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_COUNTRY;
        private CrplControlLibrary.SLTextBox slTextBox3;
        private CrplControlLibrary.SLTextBox slTextBox1;
    }
}