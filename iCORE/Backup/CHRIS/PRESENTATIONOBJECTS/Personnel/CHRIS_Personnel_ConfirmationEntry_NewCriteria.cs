using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_ConfirmationEntry_NewCriteria : ChrisSimpleForm
    {
        public CHRIS_Personnel_ConfirmationEntry_NewCriteria()
        {
            InitializeComponent();
            this.tbtAdd.Visible = false;
            this.tbtCancel.Visible = false;
            this.tbtDelete.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtList.Visible = false;
            this.tbtClose.Visible = false;
        }

        private void txtOption_Validating(object sender, CancelEventArgs e)
        {
            if (FunctionConfig.CurrentOption == Function.Add)
            {
                CHRIS_Personnel_ConfirmationEntry_Month Month = new CHRIS_Personnel_ConfirmationEntry_Month();
                Month.ShowDialog();
            
            }
        }

      


    }
}