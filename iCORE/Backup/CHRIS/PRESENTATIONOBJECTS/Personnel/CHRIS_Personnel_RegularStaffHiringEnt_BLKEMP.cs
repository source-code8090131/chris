using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP : TabularForm
    {
        #region --Variable--
        private DataTable dtDept;
        private DataTable dtEmp;
        private DataTable dtRef;
        private DataTable dtChild;
        private DataTable dtEdu;
        bool ValidateGrid = true;

        private CHRIS_Personnel_RegularStaffHiringEnt _mainForm = null;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKREF frm_Ref;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT frm_Dept;
        #endregion

        #region --Constructor--

        public CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_RegularStaffHiringEnt mainForm, 
            CHRIS_Personnel_RegularStaffHiringEnt_BLKREF BLKREF, CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT BLKDEPT, DataTable _dtDept, DataTable _dtEmp, DataTable _dtRef, DataTable _dtChild, DataTable _dtEdu)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            dtDept  = _dtDept;
            dtEmp   = _dtEmp;
            dtRef   = _dtRef;
            dtChild = _dtChild;
            dtEdu   = _dtEdu;
            frm_Dept = BLKDEPT;
            frm_Ref = BLKREF;
            this._mainForm = mainForm;
        }
        #endregion




        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                this.tbtAdd.Visible = false;
                this.tbtList.Visible = false;
                this.tbtSave.Visible = false;
                this.tbtCancel.Visible = false;
                this.tlbMain.Visible = false;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CommonOnLoadMethods", exp);
            }
        }

        /// <summary>
        /// Load the grid with the DataTable coming from MAIN From.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                if (frm_Ref != null)
                    frm_Ref.Hide();
                else if (frm_Dept != null)
                    frm_Dept.Hide();

                dgvBlkEmp.GridSource    = dtEmp;
                dgvBlkEmp.DataSource    = dgvBlkEmp.GridSource;
                dgvBlkEmp.Focus();
                dgvBlkEmp.CurrentCell = dgvBlkEmp[dgvBlkEmp.FirstDisplayedCell.ColumnIndex, 0];
                this.KeyPreview         = true;
                this.KeyDown            += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
                dgvBlkEmp.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);

            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }


        /// <summary>
        /// On Ctrl+Down/Ctrl+UP open new POPUPs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (_mainForm.txtOption.Text == "A")
                {
                    if (!dgvBlkEmp.CurrentCell.IsInEditMode)
                    {
                        if (e.Modifiers == Keys.Control)
                        {
                            switch (e.KeyValue)
                            {
                                case 34:
                                    frm_Ref             = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKREF(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, null,this ,dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                    frm_Ref.MdiParent   = null;
                                    frm_Ref.Enabled     = true;
                                    this.Hide();
                                    frm_Ref.ShowDialog(this);
                                    //this.Close();
                                    this.KeyPreview     = true;
                                    break;

                                case 33:
                                    frm_Dept            = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm ,this , dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                    frm_Dept.MdiParent  = null;
                                    frm_Dept.Enabled    = true;
                                    this.Hide();
                                    frm_Dept.ShowDialog(this);
                                    //this.Close();

                                    this.KeyPreview     = true;
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    if (e.Modifiers == Keys.Control)
                    {
                        switch (e.KeyValue)
                        {
                            case 34:
                                frm_Ref             = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKREF(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, null, this ,dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                frm_Ref.MdiParent   = null;
                                frm_Ref.Enabled     = true;
                                frm_Ref.ShowDialog(this);
                                this.Hide();
                                //this.Close();
                                this.KeyPreview     = true;
                                break;

                            case 33:
                                frm_Dept            = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm ,this , dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                frm_Dept.MdiParent  = null;
                                frm_Dept.Enabled    = true;
                                this.Hide();
                                frm_Dept.ShowDialog(this);
                                //this.Close();

                                this.KeyPreview = true;
                                break;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "ShortCutKey_Press", exp);
            }
        }


        /// <summary>
        /// Cell Validation: If current Cell is From Date and Mode is View then Cell Leave not allowed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBlkEmp_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            ValidateGrid = true;
            bool istrueJoiningDate  = false;
            bool istrueFromDate     = false;
            bool istrueDateTo       = false;

            try
            {
                if (dgvBlkEmp.RowCount > 0 && dgvBlkEmp.DataSource != null)
                {
                    DateTime Job_From;
                    DateTime Job_To;
                    DateTime Joining_Date;

                    //dgvBlkEmp.EndEdit();
                    if (dgvBlkEmp.CurrentCell.IsInEditMode)
                    {
                        if (_mainForm.txt_PR_JOINING_DATE.Value != null)
                        {
                            //istrueJoiningDate = DateTime.TryParse(_mainForm.txt_PR_JOINING_DATE.Value.ToString(), out Joining_Date);
                            istrueJoiningDate = IsValidExpression(_mainForm.txt_PR_JOINING_DATE.Value.ToString(), InputValidator.DATETIME_REGEX);

                            if (dgvBlkEmp.CurrentCell.OwningColumn.Name == "From" && dgvBlkEmp.CurrentCell.Value != null)
                            {
                                if (dgvBlkEmp.CurrentCell.EditedFormattedValue != "")
                                {
                                    istrueFromDate = DateTime.TryParse(dgvBlkEmp.CurrentCell.EditedFormattedValue.ToString(), out Job_From);
                                    istrueFromDate = IsValidExpression(dgvBlkEmp.CurrentCell.EditedFormattedValue.ToString(), InputValidator.DATE_REGEX);

                                    Joining_Date = Convert.ToDateTime(_mainForm.txt_PR_JOINING_DATE.Value);

                                    if (istrueJoiningDate == true && istrueFromDate == true)
                                    {
                                        Job_From = Convert.ToDateTime(dgvBlkEmp.CurrentCell.EditedFormattedValue.ToString());

                                        if (DateTime.Compare(Job_From, Joining_Date) > 0)
                                        {
                                            MessageBox.Show("Previous Employment Date Is Greater Then Joinning Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            ValidateGrid = false;
                                            e.Cancel = true;
                                        }
                                        else if (Job_From <= _mainForm.g_w_Date_To && _mainForm.g_up == "F")
                                        {
                                            MessageBox.Show("Next Job Should Be After The Last Job", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            ValidateGrid = false;
                                            e.Cancel = true;
                                        }
                                        else
                                        {
                                            //e.Cancel = true;
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Date is Not in Correct Format", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        ValidateGrid = false;
                                        e.Cancel = true;
                                    }
                                }
                                else
                                {
                                    ValidateGrid = false;
                                    e.Cancel = true;
                                }
                            }
                            else if (dgvBlkEmp.CurrentCell.OwningColumn.Name == "To" && dgvBlkEmp.CurrentCell.Value != null)
                            {
                                if (dgvBlkEmp.CurrentCell.EditedFormattedValue != "")
                                {
                                    istrueDateTo = IsValidExpression(dgvBlkEmp.CurrentCell.EditedFormattedValue.ToString(), InputValidator.DATE_REGEX);
                                    istrueFromDate = IsValidExpression(dgvBlkEmp.CurrentRow.Cells["From"].EditedFormattedValue.ToString(), InputValidator.DATE_REGEX);

                                    if (istrueDateTo == true && istrueFromDate == true && istrueJoiningDate == true)
                                    {
                                        Job_To = Convert.ToDateTime(dgvBlkEmp.CurrentCell.EditedFormattedValue.ToString());
                                        Job_From = Convert.ToDateTime(dgvBlkEmp.CurrentRow.Cells["From"].EditedFormattedValue.ToString());
                                        Joining_Date = Convert.ToDateTime(_mainForm.txt_PR_JOINING_DATE.Value);

                                        if (DateTime.Compare(Job_To, Joining_Date) > 0)
                                        {
                                            MessageBox.Show("Previous Employment Date Is Greater Then Joinning Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            ValidateGrid = false;
                                            e.Cancel = true;
                                        }
                                        else if (DateTime.Compare(Job_To, Job_From) < 0)
                                        {
                                            MessageBox.Show("Ending Date Can not Be Less Then Starting Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            ValidateGrid = false;
                                            e.Cancel = true;
                                        }
                                        else
                                        {
                                            _mainForm.g_w_Date_To = Job_To;
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Date is Not in Correct Format", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        ValidateGrid = false;
                                        e.Cancel = true;
                                    }
                                }
                                else
                                {
                                    ValidateGrid = false;
                                    e.Cancel = true;
                                }
                            }
                            else if (dgvBlkEmp.CurrentCell.OwningColumn.Name == "Organization_Address" && dgvBlkEmp.CurrentCell.EditedFormattedValue.ToString() == "")
                            {
                                ValidateGrid = false;
                                e.Cancel = true;
                            }
                            else if (dgvBlkEmp.CurrentCell.OwningColumn.Name == "Designation" && dgvBlkEmp.CurrentCell.EditedFormattedValue.ToString() == "")
                            {
                                ValidateGrid = false;
                                e.Cancel = true;
                            }


                            if (dgvBlkEmp.CurrentCell.OwningColumn.Name == "From")
                            {
                                if (_mainForm.FunctionConfig.CurrentOption.ToString() != "V")
                                {
                                    //e.Cancel = true;
                                }
                            }
                            else if (dgvBlkEmp.CurrentCell.OwningColumn.Name == "Address3" || dgvBlkEmp.CurrentCell.OwningColumn.Name == "Address4")
                            {
                                //                if error_code = 40600 then
                                //                      message('Employment For This Organization Already Entered');
                                //                  raise form_trigger_failure;
                                //                end if;
                                //              :global.up := 'F';

                                //e.Cancel = true;
                            }
                        }
                    }
                    else
                    {
                        //if (dgvBlkEmp.CurrentCell.OwningColumn.Name != "Remarks")
                        {
                            if (dgvBlkEmp.CurrentRow.Cells["From"].EditedFormattedValue == string.Empty)
                            {
                                ValidateGrid = false;
                            }
                            else if (dgvBlkEmp.CurrentRow.Cells["To"].EditedFormattedValue == string.Empty)
                            {
                                ValidateGrid = false;
                            }
                            else if (dgvBlkEmp.CurrentRow.Cells["Organization_Address"].EditedFormattedValue == string.Empty)
                            {
                                ValidateGrid = false;
                            }
                            else if (dgvBlkEmp.CurrentRow.Cells["Designation"].EditedFormattedValue == string.Empty)
                            {
                                ValidateGrid = false;
                            }
                        }
                    }
                }
                else
                {
                    ValidateGrid = false;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dgvBlkEmp_CellValidating", exp);
            }
        }

        private void dgvBlkEmp_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }

        /// <summary>
        /// Set the Grid Edit Mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBlkEmp_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgvBlkEmp.BeginEdit(true);
        }
    }
}