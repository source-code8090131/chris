namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    partial class CHRIS_InterestDifferential_CountryCodeAuthorization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_InterestDifferential_CountryCodeAuthorization));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.W_LOC = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.dtpMarkerDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtMarkerTime = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMarkerName = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtAuth = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.lookupButton1 = new CrplControlLibrary.LookupButton(this.components);
            this.txtPrStatus = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCitiMail = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCountryAc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCountryName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCountryCode = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHeader.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 390);
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.label8);
            this.pnlHeader.Controls.Add(this.txtDate);
            this.pnlHeader.Controls.Add(this.txtUser);
            this.pnlHeader.Controls.Add(this.label6);
            this.pnlHeader.Controls.Add(this.W_LOC);
            this.pnlHeader.Controls.Add(this.label7);
            this.pnlHeader.Controls.Add(this.label9);
            this.pnlHeader.Controls.Add(this.label10);
            this.pnlHeader.Location = new System.Drawing.Point(12, 58);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(620, 72);
            this.pnlHeader.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(478, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Date :";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(526, 34);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(84, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 8;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(53, 14);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(86, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 6;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "User :";
            // 
            // W_LOC
            // 
            this.W_LOC.AllowSpace = true;
            this.W_LOC.AssociatedLookUpName = "";
            this.W_LOC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_LOC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_LOC.ContinuationTextBox = null;
            this.W_LOC.CustomEnabled = true;
            this.W_LOC.DataFieldMapping = "";
            this.W_LOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_LOC.GetRecordsOnUpDownKeys = false;
            this.W_LOC.IsDate = false;
            this.W_LOC.Location = new System.Drawing.Point(53, 40);
            this.W_LOC.Name = "W_LOC";
            this.W_LOC.NumberFormat = "###,###,##0.00";
            this.W_LOC.Postfix = "";
            this.W_LOC.Prefix = "";
            this.W_LOC.ReadOnly = true;
            this.W_LOC.Size = new System.Drawing.Size(86, 20);
            this.W_LOC.SkipValidation = false;
            this.W_LOC.TabIndex = 7;
            this.W_LOC.TabStop = false;
            this.W_LOC.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Loc :";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(145, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(375, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Interest Differential";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(145, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(375, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "Country Code Entry (Authorization)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlDetail
            // 
            this.pnlDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.Controls.Add(this.label18);
            this.pnlDetail.Controls.Add(this.dtpMarkerDate);
            this.pnlDetail.Controls.Add(this.txtMarkerTime);
            this.pnlDetail.Controls.Add(this.label16);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.txtMarkerName);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.txtAuth);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.lookupButton1);
            this.pnlDetail.Controls.Add(this.txtPrStatus);
            this.pnlDetail.Controls.Add(this.txtCitiMail);
            this.pnlDetail.Controls.Add(this.txtCountryAc);
            this.pnlDetail.Controls.Add(this.txtCountryName);
            this.pnlDetail.Controls.Add(this.txtCountryCode);
            this.pnlDetail.Controls.Add(this.label5);
            this.pnlDetail.Controls.Add(this.label4);
            this.pnlDetail.Controls.Add(this.label3);
            this.pnlDetail.Controls.Add(this.label2);
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.DataManager = null;
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = true;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PRCountryCommand";
            this.pnlDetail.Location = new System.Drawing.Point(12, 136);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(620, 247);
            this.pnlDetail.SPName = "CHRIS_PR_COUNTRY_AUTHORIZATION_MANAGER";
            this.pnlDetail.TabIndex = 11;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.IsLookUpField = true;
            this.txtID.Location = new System.Drawing.Point(558, 30);
            this.txtID.MaxLength = 3;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.Size = new System.Drawing.Size(41, 20);
            this.txtID.SkipValidation = true;
            this.txtID.TabIndex = 36;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(150, 167);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 35;
            this.label18.Text = "Auth. Flag :";
            // 
            // dtpMarkerDate
            // 
            this.dtpMarkerDate.CustomEnabled = false;
            this.dtpMarkerDate.CustomFormat = "dd/MM/yyyy";
            this.dtpMarkerDate.DataFieldMapping = "PR_MAKER_DATE";
            this.dtpMarkerDate.Enabled = false;
            this.dtpMarkerDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMarkerDate.HasChanges = false;
            this.dtpMarkerDate.Location = new System.Drawing.Point(351, 213);
            this.dtpMarkerDate.Name = "dtpMarkerDate";
            this.dtpMarkerDate.NullValue = " ";
            this.dtpMarkerDate.Size = new System.Drawing.Size(90, 20);
            this.dtpMarkerDate.TabIndex = 34;
            this.dtpMarkerDate.Value = new System.DateTime(2010, 12, 20, 13, 43, 0, 996);
            // 
            // txtMarkerTime
            // 
            this.txtMarkerTime.AllowSpace = true;
            this.txtMarkerTime.AssociatedLookUpName = "";
            this.txtMarkerTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkerTime.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkerTime.ContinuationTextBox = null;
            this.txtMarkerTime.CustomEnabled = false;
            this.txtMarkerTime.DataFieldMapping = "PR_MAKER_TIME";
            this.txtMarkerTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkerTime.GetRecordsOnUpDownKeys = false;
            this.txtMarkerTime.IsDate = false;
            this.txtMarkerTime.Location = new System.Drawing.Point(543, 213);
            this.txtMarkerTime.MaxLength = 12;
            this.txtMarkerTime.Name = "txtMarkerTime";
            this.txtMarkerTime.NumberFormat = "###,###,##0.00";
            this.txtMarkerTime.Postfix = "";
            this.txtMarkerTime.Prefix = "";
            this.txtMarkerTime.ReadOnly = true;
            this.txtMarkerTime.Size = new System.Drawing.Size(56, 20);
            this.txtMarkerTime.SkipValidation = false;
            this.txtMarkerTime.TabIndex = 33;
            this.txtMarkerTime.TabStop = false;
            this.txtMarkerTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkerTime.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(447, 213);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 20);
            this.label16.TabIndex = 32;
            this.label16.Text = "Maker Time :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(250, 213);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(95, 20);
            this.label15.TabIndex = 31;
            this.label15.Text = "Maker Date :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMarkerName
            // 
            this.txtMarkerName.AllowSpace = true;
            this.txtMarkerName.AssociatedLookUpName = "";
            this.txtMarkerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkerName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkerName.ContinuationTextBox = null;
            this.txtMarkerName.CustomEnabled = false;
            this.txtMarkerName.DataFieldMapping = "PR_MAKER_NAME";
            this.txtMarkerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkerName.GetRecordsOnUpDownKeys = false;
            this.txtMarkerName.IsDate = false;
            this.txtMarkerName.Location = new System.Drawing.Point(124, 213);
            this.txtMarkerName.MaxLength = 12;
            this.txtMarkerName.Name = "txtMarkerName";
            this.txtMarkerName.NumberFormat = "###,###,##0.00";
            this.txtMarkerName.Postfix = "";
            this.txtMarkerName.Prefix = "";
            this.txtMarkerName.ReadOnly = true;
            this.txtMarkerName.Size = new System.Drawing.Size(120, 20);
            this.txtMarkerName.SkipValidation = false;
            this.txtMarkerName.TabIndex = 30;
            this.txtMarkerName.TabStop = false;
            this.txtMarkerName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkerName.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(23, 213);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 20);
            this.label14.TabIndex = 29;
            this.label14.Text = "Maker Name :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(270, 163);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(180, 20);
            this.label12.TabIndex = 28;
            this.label12.Text = "[A] Authorized Record";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAuth
            // 
            this.txtAuth.AllowSpace = true;
            this.txtAuth.AssociatedLookUpName = "";
            this.txtAuth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAuth.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAuth.ContinuationTextBox = null;
            this.txtAuth.CustomEnabled = true;
            this.txtAuth.DataFieldMapping = "PR_AUTH_FLAGGG";
            this.txtAuth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuth.GetRecordsOnUpDownKeys = false;
            this.txtAuth.IsDate = false;
            this.txtAuth.Location = new System.Drawing.Point(233, 163);
            this.txtAuth.MaxLength = 1;
            this.txtAuth.Name = "txtAuth";
            this.txtAuth.NumberFormat = "###,###,##0.00";
            this.txtAuth.Postfix = "";
            this.txtAuth.Prefix = "";
            this.txtAuth.ReadOnly = true;
            this.txtAuth.Size = new System.Drawing.Size(31, 20);
            this.txtAuth.SkipValidation = false;
            this.txtAuth.TabIndex = 27;
            this.txtAuth.TabStop = false;
            this.txtAuth.TextType = CrplControlLibrary.TextType.String;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(270, 141);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "[C] Close";
            // 
            // lookupButton1
            // 
            this.lookupButton1.ActionLOVExists = "UnAuthCountryCodeLovExists";
            this.lookupButton1.ActionType = "UnAuthCountryCodeLov";
            this.lookupButton1.ConditionalFields = "";
            this.lookupButton1.CustomEnabled = true;
            this.lookupButton1.DataFieldMapping = "";
            this.lookupButton1.DependentLovControls = "";
            this.lookupButton1.HiddenColumns = "PR_COUNTRY_AC|PR_CITIMAIL|PR_STATUS|PR_AUTH_FLAG|PR_MAKER_NAME|PR_MAKER_DATE|PR_M" +
                "AKER_TIME";
            this.lookupButton1.Image = ((System.Drawing.Image)(resources.GetObject("lookupButton1.Image")));
            this.lookupButton1.LoadDependentEntities = true;
            this.lookupButton1.Location = new System.Drawing.Point(339, 30);
            this.lookupButton1.LookUpTitle = null;
            this.lookupButton1.Name = "lookupButton1";
            this.lookupButton1.Size = new System.Drawing.Size(26, 21);
            this.lookupButton1.SkipValidationOnLeave = false;
            this.lookupButton1.SPName = "CHRIS_PR_COUNTRY_AUTHORIZATION_MANAGER";
            this.lookupButton1.TabIndex = 10;
            this.lookupButton1.TabStop = false;
            this.lookupButton1.Tag = "";
            this.lookupButton1.UseVisualStyleBackColor = true;
            // 
            // txtPrStatus
            // 
            this.txtPrStatus.AllowSpace = true;
            this.txtPrStatus.AssociatedLookUpName = "";
            this.txtPrStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrStatus.ContinuationTextBox = null;
            this.txtPrStatus.CustomEnabled = true;
            this.txtPrStatus.DataFieldMapping = "PR_STATUS";
            this.txtPrStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrStatus.GetRecordsOnUpDownKeys = false;
            this.txtPrStatus.IsDate = false;
            this.txtPrStatus.Location = new System.Drawing.Point(233, 137);
            this.txtPrStatus.Name = "txtPrStatus";
            this.txtPrStatus.NumberFormat = "###,###,##0.00";
            this.txtPrStatus.Postfix = "";
            this.txtPrStatus.Prefix = "";
            this.txtPrStatus.ReadOnly = true;
            this.txtPrStatus.Size = new System.Drawing.Size(31, 20);
            this.txtPrStatus.SkipValidation = false;
            this.txtPrStatus.TabIndex = 5;
            this.txtPrStatus.TabStop = false;
            this.txtPrStatus.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCitiMail
            // 
            this.txtCitiMail.AllowSpace = true;
            this.txtCitiMail.AssociatedLookUpName = "";
            this.txtCitiMail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCitiMail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCitiMail.ContinuationTextBox = null;
            this.txtCitiMail.CustomEnabled = false;
            this.txtCitiMail.DataFieldMapping = "PR_CITIMAIL";
            this.txtCitiMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCitiMail.GetRecordsOnUpDownKeys = false;
            this.txtCitiMail.IsDate = false;
            this.txtCitiMail.Location = new System.Drawing.Point(233, 110);
            this.txtCitiMail.MaxLength = 1;
            this.txtCitiMail.Name = "txtCitiMail";
            this.txtCitiMail.NumberFormat = "###,###,##0.00";
            this.txtCitiMail.Postfix = "";
            this.txtCitiMail.Prefix = "";
            this.txtCitiMail.ReadOnly = true;
            this.txtCitiMail.Size = new System.Drawing.Size(31, 20);
            this.txtCitiMail.SkipValidation = true;
            this.txtCitiMail.TabIndex = 4;
            this.txtCitiMail.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCountryAc
            // 
            this.txtCountryAc.AllowSpace = true;
            this.txtCountryAc.AssociatedLookUpName = "";
            this.txtCountryAc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountryAc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountryAc.ContinuationTextBox = null;
            this.txtCountryAc.CustomEnabled = false;
            this.txtCountryAc.DataFieldMapping = "PR_COUNTRY_AC";
            this.txtCountryAc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountryAc.GetRecordsOnUpDownKeys = false;
            this.txtCountryAc.IsDate = false;
            this.txtCountryAc.Location = new System.Drawing.Point(233, 84);
            this.txtCountryAc.MaxLength = 12;
            this.txtCountryAc.Name = "txtCountryAc";
            this.txtCountryAc.NumberFormat = "###,###,##0.00";
            this.txtCountryAc.Postfix = "";
            this.txtCountryAc.Prefix = "";
            this.txtCountryAc.ReadOnly = true;
            this.txtCountryAc.Size = new System.Drawing.Size(100, 20);
            this.txtCountryAc.SkipValidation = false;
            this.txtCountryAc.TabIndex = 3;
            this.txtCountryAc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCountryName
            // 
            this.txtCountryName.AllowSpace = true;
            this.txtCountryName.AssociatedLookUpName = "";
            this.txtCountryName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountryName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountryName.ContinuationTextBox = null;
            this.txtCountryName.CustomEnabled = false;
            this.txtCountryName.DataFieldMapping = "PR_COUNTRY_DESC";
            this.txtCountryName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountryName.GetRecordsOnUpDownKeys = false;
            this.txtCountryName.IsDate = false;
            this.txtCountryName.Location = new System.Drawing.Point(233, 58);
            this.txtCountryName.Name = "txtCountryName";
            this.txtCountryName.NumberFormat = "###,###,##0.00";
            this.txtCountryName.Postfix = "";
            this.txtCountryName.Prefix = "";
            this.txtCountryName.ReadOnly = true;
            this.txtCountryName.Size = new System.Drawing.Size(230, 20);
            this.txtCountryName.SkipValidation = true;
            this.txtCountryName.TabIndex = 2;
            this.txtCountryName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCountryCode
            // 
            this.txtCountryCode.AllowSpace = true;
            this.txtCountryCode.AssociatedLookUpName = "lookupButton1";
            this.txtCountryCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountryCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountryCode.ContinuationTextBox = null;
            this.txtCountryCode.CustomEnabled = true;
            this.txtCountryCode.DataFieldMapping = "PR_COUNTRY_CODE";
            this.txtCountryCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountryCode.GetRecordsOnUpDownKeys = false;
            this.txtCountryCode.IsDate = false;
            this.txtCountryCode.IsLookUpField = true;
            this.txtCountryCode.IsRequired = true;
            this.txtCountryCode.Location = new System.Drawing.Point(233, 30);
            this.txtCountryCode.MaxLength = 3;
            this.txtCountryCode.Name = "txtCountryCode";
            this.txtCountryCode.NumberFormat = "###,###,##0.00";
            this.txtCountryCode.Postfix = "";
            this.txtCountryCode.Prefix = "";
            this.txtCountryCode.Size = new System.Drawing.Size(100, 20);
            this.txtCountryCode.SkipValidation = true;
            this.txtCountryCode.TabIndex = 1;
            this.txtCountryCode.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(127, 141);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Record Status :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(136, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "CitiMail(Y/N) :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(141, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Debit A/c.# :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(129, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Country Name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(132, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Country Code :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(379, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "User Name :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(440, 13);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 13);
            this.lblUserName.TabIndex = 14;
            // 
            // CHRIS_InterestDifferential_CountryCodeAuthorization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 450);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.pnlDetail);
            this.F3OptionText = "[F3]=Un-Auth.";
            this.F4OptionText = "[F4]Save Record";
            this.F6OptionText = "[F6]=Exit W/O Save";
            this.F7OptionText = "[F7]=Authorized Record";
            this.Name = "CHRIS_InterestDifferential_CountryCodeAuthorization";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS - Country Code Authorization";
            this.Shown += new System.EventHandler(this.CHRIS_InterestDifferential_CountryCodeAuthorization_Shown);
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_InterestDifferential_CountryCodeAuthorization_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.pnlHeader, 0);
            this.Controls.SetChildIndex(this.label17, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox W_LOC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.LookupButton lookupButton1;
        private CrplControlLibrary.SLTextBox txtPrStatus;
        private CrplControlLibrary.SLTextBox txtCitiMail;
        private CrplControlLibrary.SLTextBox txtCountryAc;
        private CrplControlLibrary.SLTextBox txtCountryName;
        private CrplControlLibrary.SLTextBox txtCountryCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtAuth;
        private CrplControlLibrary.SLDatePicker dtpMarkerDate;
        private CrplControlLibrary.SLTextBox txtMarkerTime;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox txtMarkerName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.SLTextBox txtID;
    }
}