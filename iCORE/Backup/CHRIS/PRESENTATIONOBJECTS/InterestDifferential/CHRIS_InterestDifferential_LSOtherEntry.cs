using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    public partial class CHRIS_InterestDifferential_LSOtherEntry : ChrisSimpleForm
    {
        private bool CancelSkip = true;
        private bool AmountChk  = true;
        private bool CloseRecord = false;

        #region Constructors

        public CHRIS_InterestDifferential_LSOtherEntry()
        {
            InitializeComponent();
        }
        public CHRIS_InterestDifferential_LSOtherEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            this.txtOption.Visible = false;

        }
        
        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.pnlHeader.SendToBack();
            this.tbtAdd.Visible     = false;
            this.ShowOptionTextBox  = false;
            this.lblUserName.Text   = this.UserName;
            this.ShowStatusBar      = false;
            this.txtUser.Text       = this.userID;
            this.txtDate.Text       = this.Now().ToString("dd/MM/yyyy");
            this.FunctionConfig.F7  = Function.Cancel;
            this.FunctionConfig.F3  = Function.Modify;
            this.FunctionConfig.F4  = Function.Save;
            this.FunctionConfig.F6  = Function.Quit;
            this.CurrentPanelBlock  = this.pnlDetail.Name;

        }
        
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save") //&& this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
            {
                if (txtRentGuarantee.Text.IndexOf(".") < txtRentGuarantee.Text.Length - 3 && txtRentGuarantee.Text.Contains("."))
                    AmountChk = false;
                else
                    AmountChk = true;

                if (AmountChk == true)
                {

                    if (!this.IsValidated())
                        return;

                    FNIntDiffCommand ent = (FNIntDiffCommand)(this.pnlDetail.CurrentBusinessEntity);
                    if (ent != null)
                    {
                        //if (this.FunctionConfig.CurrentOption == Function.Cancel) { ent.FN_STATUS = "C"; }
                        //else { ent.FN_STATUS = ""; }
                    }
                    if (this.Exists(this.txtPersNo.Text))
                    {
                        this.operationMode = Mode.Edit;
                    }
                    if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
                    {
                        if (ent != null)
                        {
                            ent.FN_STATUS       = "";
                            ent.FN_MAKER_NAME   = this.userID;
                            ent.FN_MAKER_DATE   = DateTime.Parse(this.CurrentDate.ToShortDateString());
                            ent.FN_MAKER_TIME   = this.CurrentDate.TimeOfDay.Hours + "" + this.CurrentDate.TimeOfDay.Minutes + "" + this.CurrentDate.TimeOfDay.Seconds;
                            ent.FN_MAKER_LOC    = this.txtLocation.Text;
                            ent.FN_COUNTRY_CODE = this.txtCountryCode.Text ;//== string.Empty ? "" : this.txtCountryCode.Text ;
                            
                            
                            if (txtRentGuarantee.Text != string.Empty)
                                ent.FN_RENTAL_GUARANT = decimal.Parse(this.txtRentGuarantee.Text);

                            CancelSkip                      = false;
                            txtCountryCode.SkipValidation   = true;
                            txtCountryCode.IsRequired       = false;
                            txtSegment.IsRequired           = false;
                            txtSegment.SkipValidation       = true;
                            txtPersNo.IsRequired            = false;

                        }
                    }
                    else
                    {
                        if (ent != null)
                        {
                            ent.FN_STATUS       = this.txtRecStatus.Text;
                            ent.FN_MAKER_NAME   = this.userID;
                            ent.FN_MAKER_DATE   = DateTime.Parse(this.CurrentDate.ToShortDateString());
                            ent.FN_MAKER_TIME   = this.CurrentDate.TimeOfDay.Hours + "" + this.CurrentDate.TimeOfDay.Minutes + "" + this.CurrentDate.TimeOfDay.Seconds;
                            ent.FN_MAKER_LOC    = this.txtLocation.Text;
                            if (txtRentGuarantee.Text != string.Empty)
                                ent.FN_RENTAL_GUARANT = decimal.Parse(this.txtRentGuarantee.Text);

                            ent.FN_AUTH_NAME = "";
                            ent.FN_AUTH_DATE = new DateTime(1900, 01, 01);
                            ent.FN_AUTH_TERM = "";
                            ent.FN_AUTH_TIME = "";
                            ent.FN_AUTH_FLAG = "";
                            ent.FN_COUNTRY_CODE = txtCountryCode.Text;

                            this.txtAuthName.Text = string.Empty;
                            

                            CancelSkip                      = false;
                            txtCountryCode.SkipValidation   = true;
                            txtCountryCode.IsRequired       = false;
                            txtSegment.IsRequired           = false;
                            txtSegment.SkipValidation       = true;
                            txtPersNo.IsRequired            = false;
                        }
                    }
                }
                else
                {
                    base.Cancel();
                    MessageBox.Show("Field must be from 9,999,999.99", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CancelSkip                      = true;
                    txtCountryCode.SkipValidation   = true;
                    txtCountryCode.IsRequired       = false;
                    txtSegment.IsRequired           = false;
                    txtSegment.SkipValidation       = true;
                    txtPersNo.IsRequired            = false;
                    this.txtPersNo.Select();
                    this.txtPersNo.Focus();
                    return;
                }
            }
            else if (actionType == "Cancel")
            {
                this.errorProvider1.Clear();
                CancelSkip                      = false;
                txtCountryCode.SkipValidation   = true;
                txtCountryCode.IsRequired       = false;
                txtSegment.IsRequired           = false;
                txtSegment.SkipValidation       = true;
                txtPersNo.Select();
                txtPersNo.Focus();
                tbtDelete.Enabled = false;
                ClearForm(pnlDetail.Controls);
                return;
            }
            else if (actionType == "Add")
            {
                //this.CustomEnableForm(false);
                //this.EnableForm(false);
            }
            else if (actionType == "Edit")
            {
                //this.EnableForm(false);
                //this.CustomEnableForm(false);
            }
            

            base.DoToolbarActions(ctrlsCollection, actionType);
            
            if (actionType == "List")
            {
                txtPersNo.Select();
                txtPersNo.Focus();
            }

            if (actionType == "Save")
                base.Cancel();

            if (actionType == "List")
            {
                if (txtRecStatus.Text == "C")
                {
                    MessageBox.Show("This Record Is Closed...Press Re Open Button TO Re-open Or Clear Record Button To Exit...", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPersNo.Focus();
                    txtPersNo.Select();
                    CloseRecord = true;
                }
                else
                {
                    CloseRecord = false;
                }
            }
            else if(actionType == "Delete")
            {
                this.errorProvider1.Clear();
                CancelSkip = false;
                txtCountryCode.SkipValidation = true;
                txtCountryCode.IsRequired = false;
                txtSegment.IsRequired = false;
                txtSegment.SkipValidation = true;
                txtPersNo.Select();
                txtPersNo.Focus();
                tbtDelete.Enabled = false;
                ClearForm(pnlDetail.Controls);
                return;
            }
            
            //this.txtPersNo.Select();
            //this.txtPersNo.Focus();
            txtPersNo.IsRequired = true;
            CancelSkip = true;
        }

        protected override bool Quit()
        {
            if (this.txtPersNo.Text != string.Empty)
            {
                this.AutoValidate = AutoValidate.Disable;
                base.DoToolbarActions(pnlDetail.Controls, "Cancel");
                //this.ClearForm(this.pnldetail.Controls);
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
            }
            else
            {
                this.AutoValidate = AutoValidate.Disable;
                base.Quit();
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
            }
            return true;
        }
        

        protected override bool Cancel()
        {
            bool flag = false;

            if (CloseRecord == false)
            {
                this.txtRecStatus.Text = "C";
                MessageBox.Show("Press Save Button To Save This Record Or Exit Button To Exit W/O Save...", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("User Not Authrized To Change This Record..", "Note", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            return flag;
            //return base.Edit();
        }
        
        protected override bool Edit()
        {
            bool flag = false;
            if (CloseRecord == false)
            {

                this.txtRecStatus.Text = "";
                MessageBox.Show("Press Save Button To Save This Record Or Exit Button To Exit W/O Save...", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("User Not Authrized To Change This Record..", "Note", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            return flag;
            //return base.Edit();
        }
        
        private bool Exists(string code)
        {
            bool flag = false;
            DataTable dt = null;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", code);
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_FN_INT_DIFF_MANAGER", "IsExists", param);

            flag = (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0);

            return flag;
        }

        private bool IsValidated()
        {
            bool validated = true;

            if (this.txtPersNo.Text == "")
            {
                validated = false;
                this.errorProvider1.SetError(this.txtPersNo, "Select valid Personnel number.");
            }
            if (this.txtSegment.Text == "GF" || this.txtSegment.Text == "GCB")
            {
                
            }
            else
            {
                validated = false;
                this.errorProvider1.SetError(this.txtSegment, "Enter valid Segment i.e. GF or GCB.");
            }
            if (this.txtCountryCode.Text == "")
            {
                validated = false;
                this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
            }

            return validated;
        }

        private void ReadOnlyForm(bool enable)
        {
            this.txtCountryCode.ReadOnly    = enable;
            this.txtSegment.ReadOnly        = enable;
            this.txtRentGuarantee.ReadOnly  = enable;
            this.txtRecStatus.ReadOnly      = enable;
        }

        private void CustomEnableForm(bool enable)
        {
            this.txtCountryCode.CustomEnabled   = enable;
            this.txtSegment.CustomEnabled       = enable;
            this.txtRentGuarantee.CustomEnabled = enable;
            this.txtRecStatus.CustomEnabled     = enable;

            this.txtCountryCode.Enabled     = enable;
            this.txtSegment.Enabled         = enable;
            this.txtRentGuarantee.Enabled   = enable;
            this.txtRecStatus.Enabled       = enable;
            this.txtPersNo.Select();
            this.txtPersNo.Focus();

        }

        #endregion

        #region Event Handlers

        private void txtSegment_Validating(object sender, CancelEventArgs e)
        {
            if (CancelSkip)
            {
                if (txtSegment.Text != "GF" && txtSegment.Text != "GCB")
                {
                    MessageBox.Show("Valid Segment is GF & GCB .... Try Again.", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }

            //if (txtSegment.Text == "GF")
            //{
            //    return;
            //}
            //if (txtSegment.Text == "GCB")
            //{
            //    return;
            //}
            //e.Cancel = true;
        }

        private void CHRIS_InterestDifferential_LSOtherEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (selectedRow == null || selectedRow.ItemArray.Length < 3)
                return;
            

            if (this.txtPersNo.Text != "" && this.txtCountryCode.Text != ""
                && this.txtSegment.Text != "")
            {
                //base.DoFunction(Function.Modify);
                
                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtEdit"]));
                
                
                if (this.txtRecStatus.Text == "C")
                {
                    //this.EnableForm(true);
                    //this.CustomEnableForm(false);
                }
            }
            else
            {
                //this.DoFunction(Function.Add);
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                this.FunctionConfig.CurrentOption = Function.Add;
            }
            if (Exists(txtPersNo.Text))
            {
                base.m_intPKID = int.Parse(txtID.Text != string.Empty ? txtID.Text : "0");
                this.tbtDelete.Visible = true;
                this.tbtDelete.Enabled = true;
            }
            else
            {
                this.tbtDelete.Visible = false;
                this.tbtDelete.Enabled = false;
            }

            if (txtRecStatus.Text == "C" && actionType == "LOVPersonnel")
            {
                //MessageBox.Show("This Record Is Closed...Press Re Open Button TO Re-open Or Clear Record Button To Exit...", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //txtPersNo.Focus();
                //txtPersNo.Select();
            }
        }


        private void txtPersNo_Validating(object sender, CancelEventArgs e)
        {
            if (txtPersNo.Text == string.Empty && lbtnPersNo.Focused != true)
            {
                e.Cancel = true;
            }
            #region old
            //Dictionary<string, object> param = new Dictionary<string, object>();
            //param.Add("PR_P_NO", txtPersNo.Text);
            //Result rsltCode;
            //CmnDataManager cmnDM = new CmnDataManager();
            //rsltCode = cmnDM.GetData("CHRIS_SP_FN_INT_DIFF_MANAGER", "LOVPERSONNEL_LEAVE", param);

            //if (rsltCode.isSuccessful
            //          && rsltCode.dstResult.Tables.Count > 0
            //          && rsltCode.dstResult.Tables[0].Rows.Count > 0)
            //{
            //    txtPersName.Text        = rsltCode.dstResult.Tables[0].Rows[0]["Name"].ToString();
            //    txtSegment.Text         = rsltCode.dstResult.Tables[0].Rows[0]["FN_SEGMENT"].ToString();
            //    txtCountryDesc.Text     = rsltCode.dstResult.Tables[0].Rows[0]["FN_Country_Desc"].ToString();
            //    txtCountryCode.Text     = rsltCode.dstResult.Tables[0].Rows[0]["FN_Country_cODE"].ToString();
            //    txtRentGuarantee.Text   = rsltCode.dstResult.Tables[0].Rows[0]["FN_RENTAL_GUARANT"].ToString();
            //    txtRecStatus.Text       = rsltCode.dstResult.Tables[0].Rows[0]["FN_STATUS"].ToString();
            //    txtID.Text              = rsltCode.dstResult.Tables[0].Rows[0]["ID"].ToString();
            //    txtAuthName.Text        = rsltCode.dstResult.Tables[0].Rows[0]["FN_AUTH_NAME"].ToString();

            //    base.m_intPKID = int.Parse(txtID.Text != string.Empty ? txtID.Text : "0");
            //    this.tbtDelete.Visible = true;
            //    this.tbtDelete.Enabled = true;
            //}
            //if (txtID.Text != string.Empty)
            //    this.m_intPKID = int.Parse(txtID.Text);
#endregion
            if (txtRecStatus.Text == "C" && lbtnPersNo.Focused == false)
            {
                MessageBox.Show("This Record Is Closed...Press Re Open Button TO Re-open Or Clear Record Button To Exit...", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                e.Cancel = true;
                //return;
            }

            if (this.txtUser.Text == txtAuthName.Text && lbtnPersNo.Focused == false)
            {
                MessageBox.Show("User Not Authrized To Change This Record..", "Note", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Cancel = true;
                CloseRecord = true;
                return;
            }
            else
            {
                CloseRecord = false;
            }
        }

        //private void txtPersNo_Leave(object sender, EventArgs e)
        //{
        //    //if (txtPersNo.Text == string.Empty)
        //    //    txtPersNo.IsRequired = true;
        //}

        private void txtCountryCode_Leave(object sender, EventArgs e)
        {
            if (txtCountryCode.Text == string.Empty)
                txtCountryCode.IsRequired = true;
        }

        private void txtRentGuarantee_Validating(object sender, CancelEventArgs e)
        {
            //if (CancelSkip)
            //    e.Cancel = true;
            //else
            //    e.Cancel = false;
        }

        private void txtRentGuarantee_Leave(object sender, EventArgs e)
        {
            if (CancelSkip)
            {
                if (txtRentGuarantee.Text.IndexOf(".") < txtRentGuarantee.Text.Length - 3)
                    AmountChk = false;
                else
                    AmountChk = true;

            }
        }

        private void txtRentGuarantee_Enter(object sender, EventArgs e)
        {
            MessageBox.Show("Press [F2] Close Rec.[3] Re-open Rec.[F4] => Save Rec.[F6] => Exit W/O Save...", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void txtCountryCode_Validating(object sender, CancelEventArgs e)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPersNo.Text);
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_FN_INT_DIFF_MANAGER", "LOVCOUNTRY", param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count == 0)
                {
                    MessageBox.Show("Invalid Country Code Entered....Or Not Authorized.","Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #endregion
    }
}