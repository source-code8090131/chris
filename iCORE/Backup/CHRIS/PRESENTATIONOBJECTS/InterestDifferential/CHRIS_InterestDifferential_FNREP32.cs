using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    public partial class CHRIS_InterestDifferential_FNREP32 : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_InterestDifferential_FNREP32()
        {
            InitializeComponent();
        }
        public CHRIS_InterestDifferential_FNREP32(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count-1);
            //this.FromDate.Value = new DateTime(1998, 1, 1);// "01/01/1998";
            //this.ToDate.Value = new DateTime(1998, 1, 31);// "01/31/1998";
        }



        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
       
            base.RptFileName = "FNREP32";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer" )
            {
                base.PrintCustomReport();
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;
                
                if (this.txtDestName.Text == string.Empty)
                {
                    DestName = @"C:\iCORE-Spool\Report";
                }

                else
                {
                    DestName = this.txtDestName.Text;
                }

                    base.ExportCustomReport(DestName,"PDF");

            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                
                string RecipentName;
                if (this.txtDestName.Text == string.Empty)
                {
                    RecipentName = "";
                }

                else
                {
                    RecipentName = this.txtDestName.Text;
                }

                    base.EmailToReport(DestName, "PDF", RecipentName);

            }


        }

       
       

    }
}