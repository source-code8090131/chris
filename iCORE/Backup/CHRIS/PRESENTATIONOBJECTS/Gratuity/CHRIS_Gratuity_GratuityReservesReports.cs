using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Gratuity
{
    public partial class CHRIS_Gratuity_GratuityReservesReports : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_Gratuity_GratuityReservesReports()
        {
            InitializeComponent();
        }
        public CHRIS_Gratuity_GratuityReservesReports(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        private void slButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            DataTable dt = new DataTable();

            dt.Columns.Add("ValueMember");
            dt.Columns.Add("DisplayMember");

            dt.Rows.Add("Screen", "Screen");
            dt.Rows.Add("File", "File");
            dt.Rows.Add("Printer", "Printer");
            dt.Rows.Add("Mail", "Mail");
            dt.Rows.Add("Preview", "Preview");


            Dest_Type.DisplayMember = "DisplayMember";
            Dest_Type.ValueMember = "ValueMember";
            Dest_Type.DataSource = dt;
            //            this.Dest_Type.Items.RemoveAt(5);

            this.Dest_Format.Text = "wide";
            this.copies.Text = "1";
            this.W_BRN.Text = "ALL";
            this.W_SEG.Text = "ALL";
            this.w_year.Text = "1995";
            this.P_END.Text = "999999";
            this.P_START.Text = "1";




        }

        private void slButton1_Click(object sender, EventArgs e)
        {
            //base.RptFileName = "GRR01";
            //base.btnCallReport_Click(sender, e);
            {

                base.RptFileName = "GRR01";
                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (Dest_Type.Text == "Printer")
                {
                    base.PrintCustomReport();
                }
                else if (Dest_Type.Text == "File")
                {

                    string DestName;
                    string DestFormat;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }

                    else
                    {
                        DestName = this.Dest_name.Text;
                    }

                    base.ExportCustomReport(DestName, "pdf");

                }
                else if (Dest_Type.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";
                    string DestFormat;
                    string RecipentName;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_name.Text;
                    }

                        base.EmailToReport(DestName, "pdf", RecipentName);


                }


            }

        }

        private void CHRIS_Gratuity_GratuityReservesReports_Load(object sender, EventArgs e)
        {
 

        }

        private void P_END_TextChanged(object sender, EventArgs e)
        {

        }

       

  
    }
}