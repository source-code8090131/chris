using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Gratuity
{
    public partial class CHRIS_Gratuity_ResingesISAndOtherGratuity : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_Gratuity_ResingesISAndOtherGratuity()
        {
            InitializeComponent();
        }
        String DestFormat;
        public CHRIS_Gratuity_ResingesISAndOtherGratuity(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
            Dest_Format.Text = "wide";
            this.w_brn.Text = "ALL";
            this.w_year.Text = "1995";
            this.w_seg.Text = "ALL";
            this.p_start.Text = "1";
            this.p_end.Text = "999999";

        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        private void Run_Click(object sender, EventArgs e)
        {
            {

                base.RptFileName = "grr04";
                //base.btnCallReport_Click(sender, e);
                if (this.Dest_Format.Text == String.Empty)
                    DestFormat = "PDF";
                else
                    DestFormat = this.Dest_Format.Text;

                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (Dest_Type.Text == "Printer")
                {
                    base.PrintCustomReport(this.nocopies.Text );
                }
                else if (Dest_Type.Text == "File")
                {

                    string DestName;

                    if (this.Dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }

                    else
                    {
                        DestName = this.Dest_name.Text;
                    }


                    base.ExportCustomReport(DestName, "pdf");

                }
                else if (Dest_Type.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";

                    string RecipentName;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_name.Text;
                    }



                    base.EmailToReport(DestName, "pdf", RecipentName);


                }


            }

        }

    }

        


    
}