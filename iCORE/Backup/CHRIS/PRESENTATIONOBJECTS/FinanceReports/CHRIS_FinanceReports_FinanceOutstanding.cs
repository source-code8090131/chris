using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_FinanceOutstanding : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_FinanceReports_FinanceOutstanding()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "dflt";

         public CHRIS_FinanceReports_FinanceOutstanding(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }


        protected override void OnLoad(EventArgs e)
        {

            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
            branch.Text = "khi";
            Dest_Format.Text = "dflt";
        }

        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }
           
       
            no_of_copies = Convert.ToInt16(nocopies.Text);
            {
                base.RptFileName = "FNREP10";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                  
                }
                if (Dest_Type.Text == "Printer")
                {

                    base.PrintNoofCopiesReport(no_of_copies);


                }
                    

                
                if (Dest_Type.Text == "File")
                {
                    if (Dest_name.Text != String.Empty)
                        DestName = Dest_name.Text;

                    base.ExportCustomReport(DestName, "PDF");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String Res = "";
                    if (Dest_name.Text != String.Empty)
                        Res = Dest_name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", Res);

                }



                //if (Dest_Type.Text == "File")
                //{
                //    if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                //    {
                //        base.ExportCustomReport(Dest_name.Text, Dest_Format.Text);

                //    }

                //}

                //if (Dest_Type.Text == "Mail")
                //{
                //    base.EmailToReport("C:\\Report", "PDF");
                //}  

            }

        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }
    }
}