namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    partial class CHRIS_FinanceReports_FireInsurance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_FinanceReports_FireInsurance));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.WCAT = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.WBRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.txtDest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.WSEG = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNumCopies = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDestName = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.WCAT);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.WBRANCH);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtDest_Format);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.WSEG);
            this.groupBox1.Controls.Add(this.txtNumCopies);
            this.groupBox1.Controls.Add(this.txtDestName);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(11, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(509, 320);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // WCAT
            // 
            this.WCAT.AllowSpace = true;
            this.WCAT.AssociatedLookUpName = "";
            this.WCAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WCAT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.WCAT.ContinuationTextBox = null;
            this.WCAT.CustomEnabled = true;
            this.WCAT.DataFieldMapping = "";
            this.WCAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WCAT.GetRecordsOnUpDownKeys = false;
            this.WCAT.IsDate = false;
            this.WCAT.Location = new System.Drawing.Point(210, 241);
            this.WCAT.MaxLength = 1;
            this.WCAT.Name = "WCAT";
            this.WCAT.NumberFormat = "###,###,##0.00";
            this.WCAT.Postfix = "";
            this.WCAT.Prefix = "";
            this.WCAT.Size = new System.Drawing.Size(199, 20);
            this.WCAT.SkipValidation = false;
            this.WCAT.TabIndex = 6;
            this.WCAT.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(23, 243);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(164, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "[C] For CTT/Blank For OFF.";
            // 
            // WBRANCH
            // 
            this.WBRANCH.AllowSpace = true;
            this.WBRANCH.AssociatedLookUpName = "";
            this.WBRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WBRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.WBRANCH.ContinuationTextBox = null;
            this.WBRANCH.CustomEnabled = true;
            this.WBRANCH.DataFieldMapping = "";
            this.WBRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WBRANCH.GetRecordsOnUpDownKeys = false;
            this.WBRANCH.IsDate = false;
            this.WBRANCH.Location = new System.Drawing.Point(209, 215);
            this.WBRANCH.MaxLength = 3;
            this.WBRANCH.Name = "WBRANCH";
            this.WBRANCH.NumberFormat = "###,###,##0.00";
            this.WBRANCH.Postfix = "";
            this.WBRANCH.Prefix = "";
            this.WBRANCH.Size = new System.Drawing.Size(199, 20);
            this.WBRANCH.SkipValidation = false;
            this.WBRANCH.TabIndex = 5;
            this.WBRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 217);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Enter Branch";
            // 
            // txtDest_Format
            // 
            this.txtDest_Format.AllowSpace = true;
            this.txtDest_Format.AssociatedLookUpName = "";
            this.txtDest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDest_Format.ContinuationTextBox = null;
            this.txtDest_Format.CustomEnabled = true;
            this.txtDest_Format.DataFieldMapping = "";
            this.txtDest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDest_Format.GetRecordsOnUpDownKeys = false;
            this.txtDest_Format.IsDate = false;
            this.txtDest_Format.Location = new System.Drawing.Point(210, 137);
            this.txtDest_Format.MaxLength = 40;
            this.txtDest_Format.Name = "txtDest_Format";
            this.txtDest_Format.NumberFormat = "###,###,##0.00";
            this.txtDest_Format.Postfix = "";
            this.txtDest_Format.Prefix = "";
            this.txtDest_Format.Size = new System.Drawing.Size(199, 20);
            this.txtDest_Format.SkipValidation = false;
            this.txtDest_Format.TabIndex = 2;
            this.txtDest_Format.Text = "PDF";
            this.txtDest_Format.TextType = CrplControlLibrary.TextType.String;
            this.txtDest_Format.TextChanged += new System.EventHandler(this.slTextBox1_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Destination Format";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // WSEG
            // 
            this.WSEG.AllowSpace = true;
            this.WSEG.AssociatedLookUpName = "";
            this.WSEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WSEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.WSEG.ContinuationTextBox = null;
            this.WSEG.CustomEnabled = true;
            this.WSEG.DataFieldMapping = "";
            this.WSEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WSEG.GetRecordsOnUpDownKeys = false;
            this.WSEG.IsDate = false;
            this.WSEG.Location = new System.Drawing.Point(209, 189);
            this.WSEG.MaxLength = 3;
            this.WSEG.Name = "WSEG";
            this.WSEG.NumberFormat = "###,###,##0.00";
            this.WSEG.Postfix = "";
            this.WSEG.Prefix = "";
            this.WSEG.Size = new System.Drawing.Size(199, 20);
            this.WSEG.SkipValidation = false;
            this.WSEG.TabIndex = 4;
            this.WSEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNumCopies
            // 
            this.txtNumCopies.AllowSpace = true;
            this.txtNumCopies.AssociatedLookUpName = "";
            this.txtNumCopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumCopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumCopies.ContinuationTextBox = null;
            this.txtNumCopies.CustomEnabled = true;
            this.txtNumCopies.DataFieldMapping = "";
            this.txtNumCopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumCopies.GetRecordsOnUpDownKeys = false;
            this.txtNumCopies.IsDate = false;
            this.txtNumCopies.Location = new System.Drawing.Point(210, 163);
            this.txtNumCopies.MaxLength = 2;
            this.txtNumCopies.Name = "txtNumCopies";
            this.txtNumCopies.NumberFormat = "###,###,##0.00";
            this.txtNumCopies.Postfix = "";
            this.txtNumCopies.Prefix = "";
            this.txtNumCopies.Size = new System.Drawing.Size(199, 20);
            this.txtNumCopies.SkipValidation = false;
            this.txtNumCopies.TabIndex = 3;
            this.txtNumCopies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtDestName
            // 
            this.txtDestName.AllowSpace = true;
            this.txtDestName.AssociatedLookUpName = "";
            this.txtDestName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDestName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDestName.ContinuationTextBox = null;
            this.txtDestName.CustomEnabled = true;
            this.txtDestName.DataFieldMapping = "";
            this.txtDestName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDestName.GetRecordsOnUpDownKeys = false;
            this.txtDestName.IsDate = false;
            this.txtDestName.Location = new System.Drawing.Point(209, 111);
            this.txtDestName.MaxLength = 50;
            this.txtDestName.Name = "txtDestName";
            this.txtDestName.NumberFormat = "###,###,##0.00";
            this.txtDestName.Postfix = "";
            this.txtDestName.Prefix = "";
            this.txtDestName.Size = new System.Drawing.Size(199, 20);
            this.txtDestName.SkipValidation = false;
            this.txtDestName.TabIndex = 1;
            this.txtDestName.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(210, 84);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(199, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(279, 280);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(61, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(210, 280);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(63, 23);
            this.btnRun.TabIndex = 7;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(22, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Enter Segment";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Number of Copies";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Destination Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(190, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(185, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Enter values for the parameters";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(207, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Report Parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(465, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 77;
            this.pictureBox2.TabStop = false;
            // 
            // CHRIS_FinanceReports_FireInsurance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 371);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_FinanceReports_FireInsurance";
            this.Text = "CHRIS_FinancialReports_FireInsurance";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private CrplControlLibrary.SLTextBox WSEG;
        private CrplControlLibrary.SLTextBox txtNumCopies;
        private CrplControlLibrary.SLTextBox txtDestName;
        private CrplControlLibrary.SLTextBox txtDest_Format;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox WBRANCH;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox WCAT;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}