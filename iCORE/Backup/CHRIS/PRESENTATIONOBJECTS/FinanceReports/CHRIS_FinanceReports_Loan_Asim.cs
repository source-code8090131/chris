using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Finance;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_Loan_Asim : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_FinanceReports_Loan_Asim()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";

        CHRIS_Finance_FinanceApplication mainfrm = null;

        public CHRIS_FinanceReports_Loan_Asim(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)

     
        {
            InitializeComponent();

            //FN_P_NO.Text = mainfrm.txtPersonnelNo.Text;
            //TOT_MONTH_INST1.Text = mainfrm.txtTotalMonth.Text;
            //PR_MARKUP1.Text = mainfrm.txtFnMrkup.Text;
            //FN_TYPE.Text = mainfrm.txtFinanceType.Text;
            //AMT_OF_LOAN_CAN_AVAIL.Text = mainfrm.txtCanBeAvail.Text;
            //PR_AMT_AVAILED1.Text = mainfrm.txtDisbursmntAmt.Text;
        
        }

        public CHRIS_FinanceReports_Loan_Asim(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Finance_FinanceApplication mainfrm1)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            mainfrm = mainfrm1;
            
           // FN_P_NO.Text = mainfrm.txtPersonnelNo.Text;
            
            
        }



        protected override void OnLoad(EventArgs e)
        {            
            
            base.OnLoad(e);

            if (mainfrm != null)
            {
                FN_P_NO.Text = mainfrm.txtPersonnelNo.Text;
                TOT_MONTH_INST1.Text = mainfrm.txtTotalMonth.Text;
                PR_MARKUP1.Text = mainfrm.txtFnMrkup.Text;
                FN_TYPE.Text = mainfrm.txtFinanceType.Text;
                AMT_OF_LOAN_CAN_AVAIL.Text = mainfrm.txtCanBeAvail.Text;
                PR_AMT_AVAILED1.Text = mainfrm.txtDisbursmntAmt.Text;
            } 
            Dest_Type.Items.RemoveAt(5);
            cmbPrint.Items.RemoveAt(2);
           
       }


        private void CHRIS_FinanceReports_Loan_Asim_Load(object sender, EventArgs e)
        {

        }

        private void btnRun_Click(object sender, EventArgs e)
        {
           
          
                base.RptFileName = "LOAN_ASIM";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }

              
              if (Dest_Type.Text == "Printer" && cmbPrint.Text == "Yes")
              {
                    PrintCustomReport();
                }

              
                if (Dest_Type.Text == "Printer" && cmbPrint.Text == "No")
                {
                    MessageBox.Show("Failed While Printing");
                }




                if (Dest_Type.Text == "File")
                {
                    string desname = "c:\\iCORE-Spool\\report";
                    if (Dest_name.Text != String.Empty)
                        desname = Dest_name.Text;
                    base.ExportCustomReport(desname, "pdf");
                    

                }


                if (Dest_Type.Text == "Mail")
                {
                    string desname = "";
                    if (Dest_name.Text != String.Empty)
                        desname = Dest_name.Text;

                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", desname);

                }


            }



        private void btnClose_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        private void PR_MARKUP1_Validating(object sender, CancelEventArgs e)
        {double valuechk= 0;


        if (PR_MARKUP1.Text == string.Empty)
        { }
        else        if (!(double.TryParse(PR_MARKUP1.Text , out valuechk)))
            {
                PR_MARKUP1.Text = "";
                PR_MARKUP1.Select();    
                PR_MARKUP1.Focus();
                return;
            
            }
        }

        private void PR_AMT_AVAILED1_Validating(object sender, CancelEventArgs e)
        {
            double AMTAVALED = 0;


            if (PR_AMT_AVAILED1.Text == string.Empty)
            { }
            else if (!(double.TryParse(PR_AMT_AVAILED1.Text, out AMTAVALED)))
            {
                PR_AMT_AVAILED1.Text = "";
                PR_AMT_AVAILED1.Select();
                PR_AMT_AVAILED1.Focus();
                return;

            }

        }

        private void AMT_OF_LOAN_CAN_AVAIL_Validating(object sender, CancelEventArgs e)
        {
            double AMTLOAN = 0;


            if (AMT_OF_LOAN_CAN_AVAIL.Text == string.Empty)
            { }
            else if (!(double.TryParse(AMT_OF_LOAN_CAN_AVAIL.Text, out AMTLOAN)))
            {
                AMT_OF_LOAN_CAN_AVAIL.Text = "";
                AMT_OF_LOAN_CAN_AVAIL.Select();
                AMT_OF_LOAN_CAN_AVAIL.Focus();
                return;

            }

        }

        
        
    }
}