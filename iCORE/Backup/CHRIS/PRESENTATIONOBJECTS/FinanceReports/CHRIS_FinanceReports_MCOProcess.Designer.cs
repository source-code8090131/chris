namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    partial class CHRIS_FinanceReports_MCOProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.dtAsofDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtFinNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBranchCode = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtSeg = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.slButton2 = new CrplControlLibrary.SLButton();
            this.btnStart = new CrplControlLibrary.SLButton();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(451, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(487, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 268);
            this.panel1.Size = new System.Drawing.Size(487, 60);
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.dtAsofDate);
            this.pnlDetail.Controls.Add(this.txtFinNo);
            this.pnlDetail.Controls.Add(this.label4);
            this.pnlDetail.Controls.Add(this.label2);
            this.pnlDetail.Controls.Add(this.txtBranchCode);
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.Controls.Add(this.txtSeg);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.slButton2);
            this.pnlDetail.Controls.Add(this.btnStart);
            this.pnlDetail.Controls.Add(this.label3);
            this.pnlDetail.DataManager = null;
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "";
            this.pnlDetail.Location = new System.Drawing.Point(12, 84);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(468, 214);
            this.pnlDetail.SPName = "";
            this.pnlDetail.TabIndex = 41;
            // 
            // dtAsofDate
            // 
            this.dtAsofDate.CustomEnabled = true;
            this.dtAsofDate.CustomFormat = "dd/MM/yyyy";
            this.dtAsofDate.DataFieldMapping = "";
            this.dtAsofDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtAsofDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtAsofDate.HasChanges = true;
            this.dtAsofDate.Location = new System.Drawing.Point(219, 100);
            this.dtAsofDate.Name = "dtAsofDate";
            this.dtAsofDate.NullValue = " ";
            this.dtAsofDate.Size = new System.Drawing.Size(98, 20);
            this.dtAsofDate.TabIndex = 2;
            this.dtAsofDate.Value = new System.DateTime(2011, 3, 9, 0, 0, 0, 0);
            // 
            // txtFinNo
            // 
            this.txtFinNo.AllowSpace = true;
            this.txtFinNo.AssociatedLookUpName = "";
            this.txtFinNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinNo.ContinuationTextBox = null;
            this.txtFinNo.CustomEnabled = true;
            this.txtFinNo.DataFieldMapping = "";
            this.txtFinNo.Enabled = false;
            this.txtFinNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinNo.GetRecordsOnUpDownKeys = false;
            this.txtFinNo.IsDate = false;
            this.txtFinNo.Location = new System.Drawing.Point(219, 126);
            this.txtFinNo.MaxLength = 10;
            this.txtFinNo.Name = "txtFinNo";
            this.txtFinNo.NumberFormat = "###,###,##0.00";
            this.txtFinNo.Postfix = "";
            this.txtFinNo.Prefix = "";
            this.txtFinNo.ReadOnly = true;
            this.txtFinNo.Size = new System.Drawing.Size(126, 20);
            this.txtFinNo.SkipValidation = false;
            this.txtFinNo.TabIndex = 49;
            this.txtFinNo.TabStop = false;
            this.txtFinNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(114, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 15);
            this.label4.TabIndex = 48;
            this.label4.Text = "Finance No :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(87, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 15);
            this.label2.TabIndex = 46;
            this.label2.Text = "Enter As of Date :";
            // 
            // txtBranchCode
            // 
            this.txtBranchCode.AllowSpace = true;
            this.txtBranchCode.AssociatedLookUpName = "";
            this.txtBranchCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranchCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranchCode.ContinuationTextBox = null;
            this.txtBranchCode.CustomEnabled = true;
            this.txtBranchCode.DataFieldMapping = "";
            this.txtBranchCode.Enabled = false;
            this.txtBranchCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranchCode.GetRecordsOnUpDownKeys = false;
            this.txtBranchCode.IsDate = false;
            this.txtBranchCode.Location = new System.Drawing.Point(219, 74);
            this.txtBranchCode.MaxLength = 10;
            this.txtBranchCode.Name = "txtBranchCode";
            this.txtBranchCode.NumberFormat = "###,###,##0.00";
            this.txtBranchCode.Postfix = "";
            this.txtBranchCode.Prefix = "";
            this.txtBranchCode.Size = new System.Drawing.Size(83, 20);
            this.txtBranchCode.SkipValidation = false;
            this.txtBranchCode.TabIndex = 1;
            this.txtBranchCode.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(72, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 15);
            this.label1.TabIndex = 44;
            this.label1.Text = "Enter Branch Code :";
            // 
            // txtSeg
            // 
            this.txtSeg.AllowSpace = true;
            this.txtSeg.AssociatedLookUpName = "";
            this.txtSeg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSeg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSeg.ContinuationTextBox = null;
            this.txtSeg.CustomEnabled = true;
            this.txtSeg.DataFieldMapping = "";
            this.txtSeg.Enabled = false;
            this.txtSeg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeg.GetRecordsOnUpDownKeys = false;
            this.txtSeg.IsDate = false;
            this.txtSeg.Location = new System.Drawing.Point(219, 48);
            this.txtSeg.MaxLength = 10;
            this.txtSeg.Name = "txtSeg";
            this.txtSeg.NumberFormat = "###,###,##0.00";
            this.txtSeg.Postfix = "";
            this.txtSeg.Prefix = "";
            this.txtSeg.Size = new System.Drawing.Size(83, 20);
            this.txtSeg.SkipValidation = false;
            this.txtSeg.TabIndex = 0;
            this.txtSeg.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(94, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 15);
            this.label8.TabIndex = 42;
            this.label8.Text = "Enter Segment :";
            // 
            // slButton2
            // 
            this.slButton2.ActionType = "";
            this.slButton2.Location = new System.Drawing.Point(226, 177);
            this.slButton2.Name = "slButton2";
            this.slButton2.Size = new System.Drawing.Size(119, 23);
            this.slButton2.TabIndex = 3;
            this.slButton2.Text = "Exit";
            this.slButton2.UseVisualStyleBackColor = true;
            this.slButton2.Click += new System.EventHandler(this.slButton2_Click);
            // 
            // btnStart
            // 
            this.btnStart.ActionType = "";
            this.btnStart.Location = new System.Drawing.Point(100, 177);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(120, 23);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "Start Process";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(70, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 21;
            this.label3.Text = "MCO Report Process";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CHRIS_FinanceReports_MCOProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 328);
            this.Controls.Add(this.pnlDetail);
            this.Name = "CHRIS_FinanceReports_MCOProcess";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_FinanceReports_MCOProcess";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLButton slButton2;
        private CrplControlLibrary.SLButton btnStart;
        private CrplControlLibrary.SLTextBox txtSeg;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtFinNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtBranchCode;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLDatePicker dtAsofDate;
    }
}