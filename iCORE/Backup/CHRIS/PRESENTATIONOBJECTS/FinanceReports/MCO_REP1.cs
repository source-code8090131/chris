using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
/*Data is not entered*/
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_MCO_REP1 : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        string DestFormat   = "PDF";
        string strBranch    = string.Empty;
        string strSeg       = string.Empty;

        public CHRIS_FinanceReports_MCO_REP1()
        {
            InitializeComponent();
        }

        public CHRIS_FinanceReports_MCO_REP1(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, string Seg, string Branch)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            strBranch   = Branch;
            strSeg      = Seg;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            W_BRANCH.Text    = strBranch;
            W_SEG.Text       = strSeg;
        }

        private void Run_Click_1(object sender, EventArgs e)
        {
            {
                base.RptFileName = "MCOREP1";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    //if (Dest_Format.Text!= string.Empty)
                    //{
                    //   base.ExportCustomReport();
                    //}
                }
                if (Dest_Type.Text == "Printer")
                {
                    //base.MatrixReport = true;
                    ///if (Dest_Format.Text!= string.Empty)
                    ///{
                    ///base.ExportCustomReport();
                    /// }
                    base.PrintCustomReport();
                    //base.ExportCustomReport();
                    //DataSet ds = base.PrintCustomReport();

                }

                if (Dest_Type.Text == "File")
                {
                    String Res1 = "c:\\iCORE-Spool\\Report";
                if (Dest_name.Text != String.Empty )
                    Res1 = Dest_name.Text;

                        base.ExportCustomReport(Res1, "pdf");
                }
                if (Dest_Type.Text == "Mail")
                {
                    String Res1 = "";
                    if (Dest_name.Text != String.Empty )
                        Res1 = Dest_name.Text;
                    base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", Res1);

     
                }


            }

            this.Hide();
            iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports.CHRIS_FinanceReports_MCO_REP2 rep1Frm = new iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports.CHRIS_FinanceReports_MCO_REP2((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent), this.connbean, W_SEG.Text, W_BRANCH.Text);
            rep1Frm.Show();




        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }
    }

}
