namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    partial class CHRIS_FinanceReports_Loan_Asim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_FinanceReports_Loan_Asim));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbPrint = new CrplControlLibrary.SLComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.LOAN_APP_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.FN_TYPE = new CrplControlLibrary.SLTextBox(this.components);
            this.AMT_OF_LOAN_CAN_AVAIL = new CrplControlLibrary.SLTextBox(this.components);
            this.PR_MARKUP1 = new CrplControlLibrary.SLTextBox(this.components);
            this.TOT_MONTH_INST1 = new CrplControlLibrary.SLTextBox(this.components);
            this.PR_AMT_AVAILED1 = new CrplControlLibrary.SLTextBox(this.components);
            this.FN_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClose = new CrplControlLibrary.SLButton();
            this.btnRun = new CrplControlLibrary.SLButton();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cmbPrint);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.LOAN_APP_DATE);
            this.groupBox1.Controls.Add(this.FN_TYPE);
            this.groupBox1.Controls.Add(this.AMT_OF_LOAN_CAN_AVAIL);
            this.groupBox1.Controls.Add(this.PR_MARKUP1);
            this.groupBox1.Controls.Add(this.TOT_MONTH_INST1);
            this.groupBox1.Controls.Add(this.PR_AMT_AVAILED1);
            this.groupBox1.Controls.Add(this.FN_P_NO);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(481, 414);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(130, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(224, 16);
            this.label11.TabIndex = 81;
            this.label11.Text = "Enter values for the parameters";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(181, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 16);
            this.label12.TabIndex = 80;
            this.label12.Text = "Report Parameters";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(51, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 79;
            this.label10.Text = "Printjob";
            // 
            // cmbPrint
            // 
            this.cmbPrint.BusinessEntity = "";
            this.cmbPrint.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbPrint.CustomEnabled = true;
            this.cmbPrint.DataFieldMapping = "";
            this.cmbPrint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPrint.FormattingEnabled = true;
            this.cmbPrint.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPrint.Location = new System.Drawing.Point(194, 142);
            this.cmbPrint.LOVType = "";
            this.cmbPrint.Name = "cmbPrint";
            this.cmbPrint.Size = new System.Drawing.Size(156, 21);
            this.cmbPrint.SPName = "";
            this.cmbPrint.TabIndex = 3;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(414, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 77;
            this.pictureBox2.TabStop = false;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(194, 116);
            this.Dest_name.MaxLength = 80;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(156, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 1;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(194, 89);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(156, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            // 
            // LOAN_APP_DATE
            // 
            this.LOAN_APP_DATE.CustomEnabled = true;
            this.LOAN_APP_DATE.CustomFormat = "dd/MM/yyyy";
            this.LOAN_APP_DATE.DataFieldMapping = "";
            this.LOAN_APP_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.LOAN_APP_DATE.HasChanges = true;
            this.errorProvider1.SetIconAlignment(this.LOAN_APP_DATE, System.Windows.Forms.ErrorIconAlignment.TopRight);
            this.LOAN_APP_DATE.Location = new System.Drawing.Point(194, 325);
            this.LOAN_APP_DATE.Name = "LOAN_APP_DATE";
            this.LOAN_APP_DATE.NullValue = " ";
            this.LOAN_APP_DATE.Size = new System.Drawing.Size(156, 20);
            this.LOAN_APP_DATE.TabIndex = 10;
            this.LOAN_APP_DATE.Value = new System.DateTime(2010, 12, 22, 13, 54, 49, 335);
            // 
            // FN_TYPE
            // 
            this.FN_TYPE.AllowSpace = true;
            this.FN_TYPE.AssociatedLookUpName = "";
            this.FN_TYPE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FN_TYPE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.FN_TYPE.ContinuationTextBox = null;
            this.FN_TYPE.CustomEnabled = true;
            this.FN_TYPE.DataFieldMapping = "";
            this.FN_TYPE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FN_TYPE.GetRecordsOnUpDownKeys = false;
            this.FN_TYPE.IsDate = false;
            this.FN_TYPE.Location = new System.Drawing.Point(194, 299);
            this.FN_TYPE.MaxLength = 40;
            this.FN_TYPE.Name = "FN_TYPE";
            this.FN_TYPE.NumberFormat = "###,###,##0.00";
            this.FN_TYPE.Postfix = "";
            this.FN_TYPE.Prefix = "";
            this.FN_TYPE.Size = new System.Drawing.Size(156, 20);
            this.FN_TYPE.SkipValidation = false;
            this.FN_TYPE.TabIndex = 9;
            this.FN_TYPE.TextType = CrplControlLibrary.TextType.String;
            // 
            // AMT_OF_LOAN_CAN_AVAIL
            // 
            this.AMT_OF_LOAN_CAN_AVAIL.AllowSpace = true;
            this.AMT_OF_LOAN_CAN_AVAIL.AssociatedLookUpName = "";
            this.AMT_OF_LOAN_CAN_AVAIL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AMT_OF_LOAN_CAN_AVAIL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.AMT_OF_LOAN_CAN_AVAIL.ContinuationTextBox = null;
            this.AMT_OF_LOAN_CAN_AVAIL.CustomEnabled = true;
            this.AMT_OF_LOAN_CAN_AVAIL.DataFieldMapping = "";
            this.AMT_OF_LOAN_CAN_AVAIL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AMT_OF_LOAN_CAN_AVAIL.GetRecordsOnUpDownKeys = false;
            this.AMT_OF_LOAN_CAN_AVAIL.IsDate = false;
            this.AMT_OF_LOAN_CAN_AVAIL.Location = new System.Drawing.Point(194, 273);
            this.AMT_OF_LOAN_CAN_AVAIL.MaxLength = 18;
            this.AMT_OF_LOAN_CAN_AVAIL.Name = "AMT_OF_LOAN_CAN_AVAIL";
            this.AMT_OF_LOAN_CAN_AVAIL.NumberFormat = "###,###,##0.00";
            this.AMT_OF_LOAN_CAN_AVAIL.Postfix = "";
            this.AMT_OF_LOAN_CAN_AVAIL.Prefix = "";
            this.AMT_OF_LOAN_CAN_AVAIL.Size = new System.Drawing.Size(156, 20);
            this.AMT_OF_LOAN_CAN_AVAIL.SkipValidation = false;
            this.AMT_OF_LOAN_CAN_AVAIL.TabIndex = 8;
            this.AMT_OF_LOAN_CAN_AVAIL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AMT_OF_LOAN_CAN_AVAIL.TextType = CrplControlLibrary.TextType.Double;
            this.AMT_OF_LOAN_CAN_AVAIL.Validating += new System.ComponentModel.CancelEventHandler(this.AMT_OF_LOAN_CAN_AVAIL_Validating);
            // 
            // PR_MARKUP1
            // 
            this.PR_MARKUP1.AllowSpace = true;
            this.PR_MARKUP1.AssociatedLookUpName = "";
            this.PR_MARKUP1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PR_MARKUP1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PR_MARKUP1.ContinuationTextBox = null;
            this.PR_MARKUP1.CustomEnabled = true;
            this.PR_MARKUP1.DataFieldMapping = "";
            this.PR_MARKUP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PR_MARKUP1.GetRecordsOnUpDownKeys = false;
            this.PR_MARKUP1.IsDate = false;
            this.PR_MARKUP1.Location = new System.Drawing.Point(194, 247);
            this.PR_MARKUP1.MaxLength = 3;
            this.PR_MARKUP1.Name = "PR_MARKUP1";
            this.PR_MARKUP1.NumberFormat = "###,###,##0.00";
            this.PR_MARKUP1.Postfix = "";
            this.PR_MARKUP1.Prefix = "";
            this.PR_MARKUP1.Size = new System.Drawing.Size(156, 20);
            this.PR_MARKUP1.SkipValidation = false;
            this.PR_MARKUP1.TabIndex = 7;
            this.PR_MARKUP1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.PR_MARKUP1.TextType = CrplControlLibrary.TextType.Double;
            this.PR_MARKUP1.Validating += new System.ComponentModel.CancelEventHandler(this.PR_MARKUP1_Validating);
            // 
            // TOT_MONTH_INST1
            // 
            this.TOT_MONTH_INST1.AllowSpace = true;
            this.TOT_MONTH_INST1.AssociatedLookUpName = "";
            this.TOT_MONTH_INST1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TOT_MONTH_INST1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TOT_MONTH_INST1.ContinuationTextBox = null;
            this.TOT_MONTH_INST1.CustomEnabled = true;
            this.TOT_MONTH_INST1.DataFieldMapping = "";
            this.TOT_MONTH_INST1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TOT_MONTH_INST1.GetRecordsOnUpDownKeys = false;
            this.TOT_MONTH_INST1.IsDate = false;
            this.TOT_MONTH_INST1.Location = new System.Drawing.Point(194, 221);
            this.TOT_MONTH_INST1.MaxLength = 20;
            this.TOT_MONTH_INST1.Name = "TOT_MONTH_INST1";
            this.TOT_MONTH_INST1.NumberFormat = "###,###,##0.00";
            this.TOT_MONTH_INST1.Postfix = "";
            this.TOT_MONTH_INST1.Prefix = "";
            this.TOT_MONTH_INST1.Size = new System.Drawing.Size(156, 20);
            this.TOT_MONTH_INST1.SkipValidation = false;
            this.TOT_MONTH_INST1.TabIndex = 6;
            this.TOT_MONTH_INST1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TOT_MONTH_INST1.TextType = CrplControlLibrary.TextType.Double;
            // 
            // PR_AMT_AVAILED1
            // 
            this.PR_AMT_AVAILED1.AllowSpace = true;
            this.PR_AMT_AVAILED1.AssociatedLookUpName = "";
            this.PR_AMT_AVAILED1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PR_AMT_AVAILED1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PR_AMT_AVAILED1.ContinuationTextBox = null;
            this.PR_AMT_AVAILED1.CustomEnabled = true;
            this.PR_AMT_AVAILED1.DataFieldMapping = "";
            this.PR_AMT_AVAILED1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PR_AMT_AVAILED1.GetRecordsOnUpDownKeys = false;
            this.PR_AMT_AVAILED1.IsDate = false;
            this.PR_AMT_AVAILED1.Location = new System.Drawing.Point(194, 195);
            this.PR_AMT_AVAILED1.MaxLength = 18;
            this.PR_AMT_AVAILED1.Name = "PR_AMT_AVAILED1";
            this.PR_AMT_AVAILED1.NumberFormat = "###,###,##0.00";
            this.PR_AMT_AVAILED1.Postfix = "";
            this.PR_AMT_AVAILED1.Prefix = "";
            this.PR_AMT_AVAILED1.Size = new System.Drawing.Size(156, 20);
            this.PR_AMT_AVAILED1.SkipValidation = false;
            this.PR_AMT_AVAILED1.TabIndex = 5;
            this.PR_AMT_AVAILED1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.PR_AMT_AVAILED1.TextType = CrplControlLibrary.TextType.Double;
            this.PR_AMT_AVAILED1.Validating += new System.ComponentModel.CancelEventHandler(this.PR_AMT_AVAILED1_Validating);
            // 
            // FN_P_NO
            // 
            this.FN_P_NO.AllowSpace = true;
            this.FN_P_NO.AssociatedLookUpName = "";
            this.FN_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FN_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.FN_P_NO.ContinuationTextBox = null;
            this.FN_P_NO.CustomEnabled = true;
            this.FN_P_NO.DataFieldMapping = "";
            this.FN_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FN_P_NO.GetRecordsOnUpDownKeys = false;
            this.FN_P_NO.IsDate = false;
            this.FN_P_NO.Location = new System.Drawing.Point(194, 169);
            this.FN_P_NO.MaxLength = 40;
            this.FN_P_NO.Name = "FN_P_NO";
            this.FN_P_NO.NumberFormat = "###,###,##0.00";
            this.FN_P_NO.Postfix = "";
            this.FN_P_NO.Prefix = "";
            this.FN_P_NO.Size = new System.Drawing.Size(156, 20);
            this.FN_P_NO.SkipValidation = false;
            this.FN_P_NO.TabIndex = 4;
            this.FN_P_NO.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(51, 334);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Loan App Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(51, 306);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Fn Type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(51, 280);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Amt of Loan Can Avail";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(51, 254);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Pr Markup";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(51, 228);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Tot Month Inst";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(51, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Pr Amt Availed";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(51, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Fn P No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(51, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Desname";
            // 
            // btnClose
            // 
            this.btnClose.ActionType = "";
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(275, 351);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.ActionType = "";
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(194, 351);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 11;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(51, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Destype";
            // 
            // CHRIS_FinanceReports_Loan_Asim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 460);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_FinanceReports_Loan_Asim";
            this.Text = "iCORE CHRIS-Loan_Asim";
            this.Load += new System.EventHandler(this.CHRIS_FinanceReports_Loan_Asim_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLButton btnClose;
        private CrplControlLibrary.SLButton btnRun;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLDatePicker LOAN_APP_DATE;
        private CrplControlLibrary.SLTextBox FN_TYPE;
        private CrplControlLibrary.SLTextBox AMT_OF_LOAN_CAN_AVAIL;
        private CrplControlLibrary.SLTextBox PR_MARKUP1;
        private CrplControlLibrary.SLTextBox TOT_MONTH_INST1;
        private CrplControlLibrary.SLTextBox PR_AMT_AVAILED1;
        private CrplControlLibrary.SLTextBox FN_P_NO;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.PictureBox pictureBox2;
        private CrplControlLibrary.SLComboBox cmbPrint;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}