namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    partial class CHRIS_FinanceReports_FNREP18B
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_FinanceReports_FNREP18B));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.txtDest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.SEG = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNumCopies = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDestName = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.txtDest_Format);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.SEG);
            this.groupBox1.Controls.Add(this.txtNumCopies);
            this.groupBox1.Controls.Add(this.txtDestName);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(440, 273);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(242, 241);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(61, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(173, 241);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(63, 23);
            this.btnRun.TabIndex = 5;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // txtDest_Format
            // 
            this.txtDest_Format.AllowSpace = true;
            this.txtDest_Format.AssociatedLookUpName = "";
            this.txtDest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDest_Format.ContinuationTextBox = null;
            this.txtDest_Format.CustomEnabled = true;
            this.txtDest_Format.DataFieldMapping = "";
            this.txtDest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDest_Format.GetRecordsOnUpDownKeys = false;
            this.txtDest_Format.IsDate = false;
            this.txtDest_Format.Location = new System.Drawing.Point(173, 150);
            this.txtDest_Format.MaxLength = 40;
            this.txtDest_Format.Name = "txtDest_Format";
            this.txtDest_Format.NumberFormat = "###,###,##0.00";
            this.txtDest_Format.Postfix = "";
            this.txtDest_Format.Prefix = "";
            this.txtDest_Format.Size = new System.Drawing.Size(199, 20);
            this.txtDest_Format.SkipValidation = false;
            this.txtDest_Format.TabIndex = 2;
            this.txtDest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(33, 152);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Destination Format";
            // 
            // SEG
            // 
            this.SEG.AllowSpace = true;
            this.SEG.AssociatedLookUpName = "";
            this.SEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.SEG.ContinuationTextBox = null;
            this.SEG.CustomEnabled = true;
            this.SEG.DataFieldMapping = "";
            this.SEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SEG.GetRecordsOnUpDownKeys = false;
            this.SEG.IsDate = false;
            this.SEG.Location = new System.Drawing.Point(174, 202);
            this.SEG.MaxLength = 3;
            this.SEG.Name = "SEG";
            this.SEG.NumberFormat = "###,###,##0.00";
            this.SEG.Postfix = "";
            this.SEG.Prefix = "";
            this.SEG.Size = new System.Drawing.Size(199, 20);
            this.SEG.SkipValidation = false;
            this.SEG.TabIndex = 4;
            this.SEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNumCopies
            // 
            this.txtNumCopies.AllowSpace = true;
            this.txtNumCopies.AssociatedLookUpName = "";
            this.txtNumCopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumCopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumCopies.ContinuationTextBox = null;
            this.txtNumCopies.CustomEnabled = true;
            this.txtNumCopies.DataFieldMapping = "";
            this.txtNumCopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumCopies.GetRecordsOnUpDownKeys = false;
            this.txtNumCopies.IsDate = false;
            this.txtNumCopies.Location = new System.Drawing.Point(173, 176);
            this.txtNumCopies.MaxLength = 2;
            this.txtNumCopies.Name = "txtNumCopies";
            this.txtNumCopies.NumberFormat = "###,###,##0.00";
            this.txtNumCopies.Postfix = "";
            this.txtNumCopies.Prefix = "";
            this.txtNumCopies.Size = new System.Drawing.Size(199, 20);
            this.txtNumCopies.SkipValidation = false;
            this.txtNumCopies.TabIndex = 3;
            this.txtNumCopies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtDestName
            // 
            this.txtDestName.AllowSpace = true;
            this.txtDestName.AssociatedLookUpName = "";
            this.txtDestName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDestName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDestName.ContinuationTextBox = null;
            this.txtDestName.CustomEnabled = true;
            this.txtDestName.DataFieldMapping = "";
            this.txtDestName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDestName.GetRecordsOnUpDownKeys = false;
            this.txtDestName.IsDate = false;
            this.txtDestName.Location = new System.Drawing.Point(174, 124);
            this.txtDestName.MaxLength = 50;
            this.txtDestName.Name = "txtDestName";
            this.txtDestName.NumberFormat = "###,###,##0.00";
            this.txtDestName.Postfix = "";
            this.txtDestName.Prefix = "";
            this.txtDestName.Size = new System.Drawing.Size(199, 20);
            this.txtDestName.SkipValidation = false;
            this.txtDestName.TabIndex = 1;
            this.txtDestName.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(174, 97);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(199, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(33, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 39;
            this.label7.Text = "Enter Segment";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(32, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 38;
            this.label5.Text = "Number of Copies";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Destination Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(154, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(185, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "Enter values for the parameters";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(171, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 17);
            this.label3.TabIndex = 34;
            this.label3.Text = "Report Parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(397, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 78;
            this.pictureBox2.TabStop = false;
            // 
            // CHRIS_FinanceReports_FNREP18B
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 323);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_FinanceReports_FNREP18B";
            this.Text = "Form1";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLTextBox txtDest_Format;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox SEG;
        private CrplControlLibrary.SLTextBox txtNumCopies;
        private CrplControlLibrary.SLTextBox txtDestName;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}