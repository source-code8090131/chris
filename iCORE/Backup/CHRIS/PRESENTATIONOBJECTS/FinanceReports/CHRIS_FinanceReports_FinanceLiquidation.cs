using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_FinanceLiquidation : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_FinanceReports_FinanceLiquidation()
        {
            InitializeComponent();
        }
        string DestFormat = "PDF";
        public CHRIS_FinanceReports_FinanceLiquidation(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            //no_of_copies = Convert.ToInt16(nocopies.Text);

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            outputmode.Items.RemoveAt(3);
            nocopies.Text = "1";
            Dest_Format.Text = "wide";
            this.fdate.Value = new DateTime(2000, 01, 01);
            this.tdate.Value = new DateTime(2000, 06, 30);
            Dest_name.Text = "C:\\iCORE-Spool\\";
            //this.fdate.Value = "01012000";
            //this.tdate.Value = "30062000";
            
            
            

        }

        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }
            no_of_copies = Convert.ToInt16(nocopies.Text);

            {
                base.RptFileName = "FNREP09";

                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
               
                }
                if (Dest_Type.Text == "Printer")
                {

                    base.PrintNoofCopiesReport(no_of_copies);


                }

                

                if (Dest_Type.Text == "File")
                {
                    string desname = "c:\\iCORE-Spool\\report";
                    if (Dest_name.Text != String.Empty)
                        desname = Dest_name.Text;


                        base.ExportCustomReport(desname, "pdf");



                }
                if (Dest_Type.Text == "Mail")
                {
                    string desname = "";
                    if (Dest_name.Text != String.Empty)
                        desname = Dest_name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", desname);
 
                }

                //if (Dest_Type.Text == "Mail")
                //{                   
                //        base.EmailToReport("C:\\Report", "PDF");
                //}
              

            }


        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

    }
}