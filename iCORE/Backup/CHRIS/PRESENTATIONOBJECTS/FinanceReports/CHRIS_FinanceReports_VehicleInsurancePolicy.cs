using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_VehicleInsurancePolicy : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_FinanceReports_VehicleInsurancePolicy()
        {
            InitializeComponent();
        }

        public CHRIS_FinanceReports_VehicleInsurancePolicy(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.pol_exp_dt_from.Value = null;
            this.pol_exp_dt_to.Value = null;
        }

      

        private void btnRun_Click(object sender, EventArgs e)
        {
            base.RptFileName = "FNREP42";
            base.btnCallReport_Click(sender, e);
           
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}