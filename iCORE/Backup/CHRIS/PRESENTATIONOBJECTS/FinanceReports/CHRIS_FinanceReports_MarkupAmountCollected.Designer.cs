namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    partial class CHRIS_FinanceReports_MarkupAmountCollected
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_FinanceReports_MarkupAmountCollected));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.IO_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.TDATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.FDATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.W_SEG = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Close = new CrplControlLibrary.SLButton();
            this.Run = new CrplControlLibrary.SLButton();
            this.BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.nocopies = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.IO_FLAG);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.TDATE);
            this.groupBox1.Controls.Add(this.FDATE);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.W_SEG);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Close);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.BRANCH);
            this.groupBox1.Controls.Add(this.nocopies);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Location = new System.Drawing.Point(12, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(514, 342);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(67, 207);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(173, 13);
            this.label11.TabIndex = 98;
            this.label11.Text = "FROM DATE(DD/MM/YYYY):";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(67, 258);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 13);
            this.label10.TabIndex = 97;
            this.label10.Text = "[I]nside/[O]utside:";
            // 
            // IO_FLAG
            // 
            this.IO_FLAG.AllowSpace = true;
            this.IO_FLAG.AssociatedLookUpName = "";
            this.IO_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IO_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.IO_FLAG.ContinuationTextBox = null;
            this.IO_FLAG.CustomEnabled = true;
            this.IO_FLAG.DataFieldMapping = "";
            this.IO_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IO_FLAG.GetRecordsOnUpDownKeys = false;
            this.IO_FLAG.IsDate = false;
            this.IO_FLAG.Location = new System.Drawing.Point(239, 256);
            this.IO_FLAG.MaxLength = 1;
            this.IO_FLAG.Name = "IO_FLAG";
            this.IO_FLAG.NumberFormat = "###,###,##0.00";
            this.IO_FLAG.Postfix = "";
            this.IO_FLAG.Prefix = "";
            this.IO_FLAG.Size = new System.Drawing.Size(150, 20);
            this.IO_FLAG.SkipValidation = false;
            this.IO_FLAG.TabIndex = 8;
            this.IO_FLAG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(67, 233);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(155, 13);
            this.label9.TabIndex = 95;
            this.label9.Text = "TO DATE(DD/MM/YYYY):";
            // 
            // TDATE
            // 
            this.TDATE.CustomEnabled = true;
            this.TDATE.CustomFormat = "dd/MM/yyyy";
            this.TDATE.DataFieldMapping = "";
            this.TDATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TDATE.HasChanges = true;
            this.TDATE.Location = new System.Drawing.Point(239, 229);
            this.TDATE.Name = "TDATE";
            this.TDATE.NullValue = " ";
            this.TDATE.Size = new System.Drawing.Size(150, 20);
            this.TDATE.TabIndex = 7;
            this.TDATE.Value = new System.DateTime(2010, 12, 22, 12, 49, 32, 991);
            // 
            // FDATE
            // 
            this.FDATE.CustomEnabled = true;
            this.FDATE.CustomFormat = "dd/MM/yyyy";
            this.FDATE.DataFieldMapping = "";
            this.FDATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FDATE.HasChanges = true;
            this.FDATE.Location = new System.Drawing.Point(239, 203);
            this.FDATE.Name = "FDATE";
            this.FDATE.NullValue = " ";
            this.FDATE.Size = new System.Drawing.Size(150, 20);
            this.FDATE.TabIndex = 6;
            this.FDATE.Value = new System.DateTime(2010, 12, 22, 12, 49, 26, 273);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(67, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 13);
            this.label7.TabIndex = 91;
            this.label7.Text = "Segment GF or GCB:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(67, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 90;
            this.label4.Text = "Enter Branch or ALL:";
            // 
            // W_SEG
            // 
            this.W_SEG.AllowSpace = true;
            this.W_SEG.AssociatedLookUpName = "";
            this.W_SEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_SEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_SEG.ContinuationTextBox = null;
            this.W_SEG.CustomEnabled = true;
            this.W_SEG.DataFieldMapping = "";
            this.W_SEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_SEG.GetRecordsOnUpDownKeys = false;
            this.W_SEG.IsDate = false;
            this.W_SEG.Location = new System.Drawing.Point(238, 176);
            this.W_SEG.MaxLength = 3;
            this.W_SEG.Name = "W_SEG";
            this.W_SEG.NumberFormat = "###,###,##0.00";
            this.W_SEG.Postfix = "";
            this.W_SEG.Prefix = "";
            this.W_SEG.Size = new System.Drawing.Size(151, 20);
            this.W_SEG.SkipValidation = false;
            this.W_SEG.TabIndex = 5;
            this.W_SEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(67, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 85;
            this.label3.Text = "Copies";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(67, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 84;
            this.label2.Text = "Desname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(67, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 83;
            this.label1.Text = "Destype";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(169, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(185, 13);
            this.label6.TabIndex = 82;
            this.label6.Text = "Enter values for the parameters";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(205, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 13);
            this.label5.TabIndex = 81;
            this.label5.Text = "Report Parameters";
            // 
            // Close
            // 
            this.Close.ActionType = "";
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(320, 280);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(69, 23);
            this.Close.TabIndex = 10;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(239, 280);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 9;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // BRANCH
            // 
            this.BRANCH.AllowSpace = true;
            this.BRANCH.AssociatedLookUpName = "";
            this.BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.BRANCH.ContinuationTextBox = null;
            this.BRANCH.CustomEnabled = true;
            this.BRANCH.DataFieldMapping = "";
            this.BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BRANCH.GetRecordsOnUpDownKeys = false;
            this.BRANCH.IsDate = false;
            this.BRANCH.Location = new System.Drawing.Point(238, 150);
            this.BRANCH.MaxLength = 3;
            this.BRANCH.Name = "BRANCH";
            this.BRANCH.NumberFormat = "###,###,##0.00";
            this.BRANCH.Postfix = "";
            this.BRANCH.Prefix = "";
            this.BRANCH.Size = new System.Drawing.Size(151, 20);
            this.BRANCH.SkipValidation = false;
            this.BRANCH.TabIndex = 4;
            this.BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // nocopies
            // 
            this.nocopies.AllowSpace = true;
            this.nocopies.AssociatedLookUpName = "";
            this.nocopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nocopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.nocopies.ContinuationTextBox = null;
            this.nocopies.CustomEnabled = true;
            this.nocopies.DataFieldMapping = "";
            this.nocopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nocopies.GetRecordsOnUpDownKeys = false;
            this.nocopies.IsDate = false;
            this.nocopies.Location = new System.Drawing.Point(238, 124);
            this.nocopies.MaxLength = 2;
            this.nocopies.Name = "nocopies";
            this.nocopies.NumberFormat = "###,###,##0.00";
            this.nocopies.Postfix = "";
            this.nocopies.Prefix = "";
            this.nocopies.Size = new System.Drawing.Size(151, 20);
            this.nocopies.SkipValidation = false;
            this.nocopies.TabIndex = 3;
            this.nocopies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(238, 97);
            this.Dest_name.MaxLength = 80;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(151, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 2;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(238, 69);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.MaxLength = 80;
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(151, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(471, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 88;
            this.pictureBox2.TabStop = false;
            // 
            // CHRIS_FinanceReports_MarkupAmountCollected
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(538, 394);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_FinanceReports_MarkupAmountCollected";
            this.Text = "iCORE CHRIS-MarkupAmountCollected";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLButton Close;
        private CrplControlLibrary.SLButton Run;
        private CrplControlLibrary.SLTextBox BRANCH;
        private CrplControlLibrary.SLTextBox nocopies;
        private CrplControlLibrary.SLTextBox Dest_name;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLDatePicker TDATE;
        private CrplControlLibrary.SLDatePicker FDATE;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox W_SEG;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox IO_FLAG;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}