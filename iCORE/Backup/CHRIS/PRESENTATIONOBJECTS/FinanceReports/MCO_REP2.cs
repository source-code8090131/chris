using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
/*Data is not entered*/
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_MCO_REP2 : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        string strSeg       = string.Empty;
        string strBranch    = string.Empty;


        public CHRIS_FinanceReports_MCO_REP2()
        {
            InitializeComponent();
        }
        //string DestName = @"C:\Report";
        string DestFormat = "PDF";
        public CHRIS_FinanceReports_MCO_REP2(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, string Seg, string branch)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            strSeg = Seg;
            strBranch = branch;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);

            W_SEG.Text = strSeg;
            W_BRANCH.Text = strBranch;
            //this.Option.Items.RemoveAt(this.Option.Items.Count - 1);
            //this.txtNumCopies.Text = "1";
        }


        private void Run_Click(object sender, EventArgs e)
        {
            //{
            //    base.RptFileName = "MCOREP2";


            //    if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            //    {

            //        base.btnCallReport_Click(sender, e);
            //        //if (Dest_Format.Text!= string.Empty)
            //        //{
            //        //   base.ExportCustomReport();
            //        //}
            //    }
            //    if (Dest_Type.Text == "Printer")
            //    {
            //        //base.MatrixReport = true;
            //        ///if (Dest_Format.Text!= string.Empty)
            //        ///{
            //        ///base.ExportCustomReport();
            //        /// }
            //        base.PrintCustomReport();
            //        //base.ExportCustomReport();
            //        //DataSet ds = base.PrintCustomReport();

            //    }
            //    if (Dest_Type.Text == "File")
            //    {
            //    String Res1 = "c:\\Report";
            //    if (Dest_name.Text != String.Empty )
            //        Res1 = Dest_name.Text;

            //            base.ExportCustomReport(Res1, "pdf");
            //    }
            //    if (Dest_Type.Text == "Mail")
            //    {
            //        String Res1 = "";
            //        if (Dest_name.Text != String.Empty )
            //            Res1 = Dest_name.Text;
            //        base.EmailToReport("C:\\Report", "pdf", Res1);

     
            //    }

             
            //}

        



        }

        private void Close_Click(object sender, EventArgs e)
        {
            //base.btnCloseReport_Click(sender, e);
        }

        private void Run_Click_1(object sender, EventArgs e)
        {
            {
                base.RptFileName = "MCOREP2";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    //if (Dest_Format.Text!= string.Empty)
                    //{
                    //   base.ExportCustomReport();
                    //}
                }
                if (Dest_Type.Text == "Printer")
                {
                    //base.MatrixReport = true;
                    ///if (Dest_Format.Text!= string.Empty)
                    ///{
                    ///base.ExportCustomReport();
                    /// }
                    base.PrintCustomReport();
                    //base.ExportCustomReport();
                    //DataSet ds = base.PrintCustomReport();

                }
                if (Dest_Type.Text == "File")
                {
                    String Res1 = "c:\\iCORE-Spool\\Report";
                    if (Dest_name.Text != String.Empty)
                        Res1 = Dest_name.Text;

                    base.ExportCustomReport(Res1, "pdf");
                }
                if (Dest_Type.Text == "Mail")
                {
                    String Res1 = "";
                    if (Dest_name.Text != String.Empty)
                        Res1 = Dest_name.Text;
                    base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", Res1);


                }


            }

        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }
    }

}
