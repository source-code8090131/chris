namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    partial class CHRIS_FinanceReports_LoanDisbursement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_FinanceReports_LoanDisbursement));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.todate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.w_amt = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.w_type = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.brn = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.stdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtDest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.seg = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNumCopies = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDestName = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.todate);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.w_amt);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.w_type);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.brn);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.stdate);
            this.groupBox1.Controls.Add(this.txtDest_Format);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.seg);
            this.groupBox1.Controls.Add(this.txtNumCopies);
            this.groupBox1.Controls.Add(this.txtDestName);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(11, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(507, 400);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // todate
            // 
            this.todate.CustomEnabled = true;
            this.todate.CustomFormat = "dd/MM/yyyy";
            this.todate.DataFieldMapping = "";
            this.todate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.todate.HasChanges = true;
            this.todate.Location = new System.Drawing.Point(210, 267);
            this.todate.Name = "todate";
            this.todate.NullValue = " ";
            this.todate.Size = new System.Drawing.Size(199, 20);
            this.todate.TabIndex = 8;
            this.todate.Value = new System.DateTime(2010, 12, 8, 16, 12, 1, 625);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(24, 274);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(133, 13);
            this.label15.TabIndex = 42;
            this.label15.Text = "To Date (dd/mm/yyyy)";
            // 
            // w_amt
            // 
            this.w_amt.AllowSpace = true;
            this.w_amt.AssociatedLookUpName = "";
            this.w_amt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_amt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_amt.ContinuationTextBox = null;
            this.w_amt.CustomEnabled = true;
            this.w_amt.DataFieldMapping = "";
            this.w_amt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_amt.GetRecordsOnUpDownKeys = false;
            this.w_amt.IsDate = false;
            this.w_amt.Location = new System.Drawing.Point(210, 319);
            this.w_amt.MaxLength = 18;
            this.w_amt.Name = "w_amt";
            this.w_amt.NumberFormat = "###,###,##0.00";
            this.w_amt.Postfix = "";
            this.w_amt.Prefix = "";
            this.w_amt.Size = new System.Drawing.Size(199, 20);
            this.w_amt.SkipValidation = false;
            this.w_amt.TabIndex = 10;
            this.w_amt.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(23, 323);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(116, 13);
            this.label13.TabIndex = 38;
            this.label13.Text = "Enter Amount >= to";
            // 
            // w_type
            // 
            this.w_type.AllowSpace = true;
            this.w_type.AssociatedLookUpName = "";
            this.w_type.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_type.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_type.ContinuationTextBox = null;
            this.w_type.CustomEnabled = true;
            this.w_type.DataFieldMapping = "";
            this.w_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_type.GetRecordsOnUpDownKeys = false;
            this.w_type.IsDate = false;
            this.w_type.Location = new System.Drawing.Point(210, 293);
            this.w_type.MaxLength = 1;
            this.w_type.Name = "w_type";
            this.w_type.NumberFormat = "###,###,##0.00";
            this.w_type.Postfix = "";
            this.w_type.Prefix = "";
            this.w_type.Size = new System.Drawing.Size(199, 20);
            this.w_type.SkipValidation = false;
            this.w_type.TabIndex = 9;
            this.w_type.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(24, 295);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(159, 13);
            this.label12.TabIndex = 36;
            this.label12.Text = "Enter Fin. Type [H,C,V,S..]";
            // 
            // brn
            // 
            this.brn.AllowSpace = true;
            this.brn.AssociatedLookUpName = "";
            this.brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.brn.ContinuationTextBox = null;
            this.brn.CustomEnabled = true;
            this.brn.DataFieldMapping = "";
            this.brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brn.GetRecordsOnUpDownKeys = false;
            this.brn.IsDate = false;
            this.brn.Location = new System.Drawing.Point(210, 189);
            this.brn.MaxLength = 3;
            this.brn.Name = "brn";
            this.brn.NumberFormat = "###,###,##0.00";
            this.brn.Postfix = "";
            this.brn.Prefix = "";
            this.brn.Size = new System.Drawing.Size(199, 20);
            this.brn.SkipValidation = false;
            this.brn.TabIndex = 5;
            this.brn.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(23, 191);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Enter Valid Branch ";
            // 
            // stdate
            // 
            this.stdate.CustomEnabled = true;
            this.stdate.CustomFormat = "dd/MM/yyyy";
            this.stdate.DataFieldMapping = "";
            this.stdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.stdate.HasChanges = true;
            this.stdate.Location = new System.Drawing.Point(210, 241);
            this.stdate.Name = "stdate";
            this.stdate.NullValue = " ";
            this.stdate.Size = new System.Drawing.Size(199, 20);
            this.stdate.TabIndex = 7;
            this.stdate.Value = new System.DateTime(2010, 12, 8, 16, 12, 1, 625);
            // 
            // txtDest_Format
            // 
            this.txtDest_Format.AllowSpace = true;
            this.txtDest_Format.AssociatedLookUpName = "";
            this.txtDest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDest_Format.ContinuationTextBox = null;
            this.txtDest_Format.CustomEnabled = true;
            this.txtDest_Format.DataFieldMapping = "";
            this.txtDest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDest_Format.GetRecordsOnUpDownKeys = false;
            this.txtDest_Format.IsDate = false;
            this.txtDest_Format.Location = new System.Drawing.Point(210, 137);
            this.txtDest_Format.MaxLength = 40;
            this.txtDest_Format.Name = "txtDest_Format";
            this.txtDest_Format.NumberFormat = "###,###,##0.00";
            this.txtDest_Format.Postfix = "";
            this.txtDest_Format.Prefix = "";
            this.txtDest_Format.Size = new System.Drawing.Size(199, 20);
            this.txtDest_Format.SkipValidation = false;
            this.txtDest_Format.TabIndex = 3;
            this.txtDest_Format.Text = "PDF";
            this.txtDest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Destination Format";
            // 
            // seg
            // 
            this.seg.AllowSpace = true;
            this.seg.AssociatedLookUpName = "";
            this.seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.seg.ContinuationTextBox = null;
            this.seg.CustomEnabled = true;
            this.seg.DataFieldMapping = "";
            this.seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seg.GetRecordsOnUpDownKeys = false;
            this.seg.IsDate = false;
            this.seg.Location = new System.Drawing.Point(210, 215);
            this.seg.MaxLength = 3;
            this.seg.Name = "seg";
            this.seg.NumberFormat = "###,###,##0.00";
            this.seg.Postfix = "";
            this.seg.Prefix = "";
            this.seg.Size = new System.Drawing.Size(199, 20);
            this.seg.SkipValidation = false;
            this.seg.TabIndex = 6;
            this.seg.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNumCopies
            // 
            this.txtNumCopies.AllowSpace = true;
            this.txtNumCopies.AssociatedLookUpName = "";
            this.txtNumCopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumCopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumCopies.ContinuationTextBox = null;
            this.txtNumCopies.CustomEnabled = true;
            this.txtNumCopies.DataFieldMapping = "";
            this.txtNumCopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumCopies.GetRecordsOnUpDownKeys = false;
            this.txtNumCopies.IsDate = false;
            this.txtNumCopies.Location = new System.Drawing.Point(210, 163);
            this.txtNumCopies.MaxLength = 2;
            this.txtNumCopies.Name = "txtNumCopies";
            this.txtNumCopies.NumberFormat = "###,###,##0.00";
            this.txtNumCopies.Postfix = "";
            this.txtNumCopies.Prefix = "";
            this.txtNumCopies.Size = new System.Drawing.Size(199, 20);
            this.txtNumCopies.SkipValidation = false;
            this.txtNumCopies.TabIndex = 4;
            this.txtNumCopies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtDestName
            // 
            this.txtDestName.AllowSpace = true;
            this.txtDestName.AssociatedLookUpName = "";
            this.txtDestName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDestName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDestName.ContinuationTextBox = null;
            this.txtDestName.CustomEnabled = true;
            this.txtDestName.DataFieldMapping = "";
            this.txtDestName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDestName.GetRecordsOnUpDownKeys = false;
            this.txtDestName.IsDate = false;
            this.txtDestName.Location = new System.Drawing.Point(210, 111);
            this.txtDestName.MaxLength = 50;
            this.txtDestName.Name = "txtDestName";
            this.txtDestName.NumberFormat = "###,###,##0.00";
            this.txtDestName.Postfix = "";
            this.txtDestName.Prefix = "";
            this.txtDestName.Size = new System.Drawing.Size(199, 20);
            this.txtDestName.SkipValidation = false;
            this.txtDestName.TabIndex = 2;
            this.txtDestName.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(210, 84);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(199, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(291, 362);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(61, 23);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(222, 362);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(63, 23);
            this.btnRun.TabIndex = 11;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 217);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Enter Valid Segment";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 248);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "From Date (dd/mm/yyyy)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Number of Copies";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Destination Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(190, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(185, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Enter values for the parameters";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(207, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Report Parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(463, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 77;
            this.pictureBox2.TabStop = false;
            // 
            // CHRIS_FinanceReports_LoanDisbursement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 449);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_FinanceReports_LoanDisbursement";
            this.Text = "CHRIS_FinanceReports_LoanDisbursement";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private CrplControlLibrary.SLTextBox seg;
        private CrplControlLibrary.SLTextBox txtNumCopies;
        private CrplControlLibrary.SLTextBox txtDestName;
        private CrplControlLibrary.SLTextBox txtDest_Format;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLDatePicker stdate;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox brn;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox w_amt;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox w_type;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLDatePicker todate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}