using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_PY009 : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_PY009()
        {
            InitializeComponent();
        }

        public CHRIS_PayrollReports_PY009(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            
        }

       
        private void button1_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PY009";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport();
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;
                string DestFormat;
                DestName = @"C:\iCORE-Spool\Report";
                if (this.DesName.Text != string.Empty)
                {
                    DestName = this.DesName.Text;
                    
                }
                base.ExportCustomReport(DestName, "PDF");
            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                string DestFormat;
                string RecipentName;
                RecipentName = "";

                if (this.DesName.Text != string.Empty)
                {
                    DestName = this.DesName.Text;
                    
                }
                base.EmailToReport(DestName, "PDF", RecipentName);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
      
         

        
    }
}