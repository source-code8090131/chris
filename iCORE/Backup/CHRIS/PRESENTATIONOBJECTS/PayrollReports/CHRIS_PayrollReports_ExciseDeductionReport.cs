using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_ExciseDeductionReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_ExciseDeductionReport()
        {
            InitializeComponent();
        }
        string DestFormat = "PDF";
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();

            /* Set values for controls here */
            W_CITY.Text = "KHI";
            W_SEG.Text = "GF";
            MNTH.Text = "3";
            YEAR.Text = "1994";
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
            
        }
        //string DestName = @"C:\Report";
      
     
        public CHRIS_PayrollReports_ExciseDeductionReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

     

        private void btnRun_Click(object sender, EventArgs e)
        {

            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }
            no_of_copies = Convert.ToInt16(nocopies.Text);


            {
                base.RptFileName = "PY023";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    base.PrintNoofCopiesReport(no_of_copies);


                }

                if (Dest_Type.Text == "File")
                {
                    String d = "c:\\iCORE-Spool\\report";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;
                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }
            }
        }
       
          

        

        private void btnClose_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

       
    }
}