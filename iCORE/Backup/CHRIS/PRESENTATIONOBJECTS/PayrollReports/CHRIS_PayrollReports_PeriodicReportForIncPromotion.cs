using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_PeriodicReportForIncPromotion : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_PeriodicReportForIncPromotion()
        {
            InitializeComponent();
        }

        public CHRIS_PayrollReports_PeriodicReportForIncPromotion(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        string DestFormat = "PDF";

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            //Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.w_brn.Text = "NUL";
            this.nocopies.Text = "1";
            Dest_Type.Text = "C:\\iCORE-Spool\\";
        }

        private void btnRun_Click(object sender, EventArgs e)
        {

            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }

            no_of_copies = Convert.ToInt16(nocopies.Text);
            {
                base.RptFileName = "PY001";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {
                    base.PrintNoofCopiesReport(no_of_copies);
                }

                if (Dest_Type.Text == "File")
                {
                    String d = "c:\\iCORE-Spool\\report";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;
                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }


            }
            #region commented code

            // base.RptFileName = "PY001";


            //if (DESTYPE.Text == "Screen" || DESTYPE.Text == "Preview")
            //{
            //    base.btnCallReport_Click(sender, e);
            //}
            //else if (DESTYPE.Text == "Printer")
            //{
            //    base.PrintCustomReport();
            //}
            //else if (DESTYPE.Text == "File")
            //{

            //    string DestName;
            //    string DestFormat;
            //    if (DESNAME.Text == string.Empty)
            //    {
            //        DestName = @"C:\Report";
            //    }

            //    else
            //    {
            //        DestName = this.DESNAME.Text;
            //    }

            //    if (DestName != string.Empty)
            //    {
            //        base.ExportCustomReport(DestName, "pdf");
            //    }
            //}
            //else if (DESTYPE.Text == "Mail")
            //{
            //    EmailToReport("C:\\PY001", "pdf");
            //}
            #endregion commented code

        }

        private void btnClose_Click(object sender, EventArgs e)
        {

            base.btnCloseReport_Click(sender, e);
        }


    }
}