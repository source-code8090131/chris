//<<<<<<< .mine
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_RankingReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ord = new CrplControlLibrary.SLTextBox(this.components);
            this.wrank = new CrplControlLibrary.SLTextBox(this.components);
            this.w_cat = new CrplControlLibrary.SLTextBox(this.components);
            this.w_seg = new CrplControlLibrary.SLTextBox(this.components);
            this.w_branch = new CrplControlLibrary.SLTextBox(this.components);
            this.whead = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.ord);
            this.groupBox1.Controls.Add(this.wrank);
            this.groupBox1.Controls.Add(this.w_cat);
            this.groupBox1.Controls.Add(this.w_seg);
            this.groupBox1.Controls.Add(this.w_branch);
            this.groupBox1.Controls.Add(this.whead);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.Dest_Format);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(415, 376);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // ord
            // 
            this.ord.AllowSpace = true;
            this.ord.AssociatedLookUpName = "";
            this.ord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ord.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ord.ContinuationTextBox = null;
            this.ord.CustomEnabled = true;
            this.ord.DataFieldMapping = "";
            this.ord.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ord.GetRecordsOnUpDownKeys = false;
            this.ord.IsDate = false;
            this.ord.Location = new System.Drawing.Point(160, 305);
            this.ord.MaxLength = 50;
            this.ord.Name = "ord";
            this.ord.NumberFormat = "###,###,##0.00";
            this.ord.Postfix = "";
            this.ord.Prefix = "";
            this.ord.Size = new System.Drawing.Size(215, 20);
            this.ord.SkipValidation = false;
            this.ord.TabIndex = 9;
            this.ord.TextType = CrplControlLibrary.TextType.String;
            // 
            // wrank
            // 
            this.wrank.AllowSpace = true;
            this.wrank.AssociatedLookUpName = "";
            this.wrank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wrank.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wrank.ContinuationTextBox = null;
            this.wrank.CustomEnabled = true;
            this.wrank.DataFieldMapping = "";
            this.wrank.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wrank.GetRecordsOnUpDownKeys = false;
            this.wrank.IsDate = false;
            this.wrank.Location = new System.Drawing.Point(160, 279);
            this.wrank.MaxLength = 50;
            this.wrank.Name = "wrank";
            this.wrank.NumberFormat = "###,###,##0.00";
            this.wrank.Postfix = "";
            this.wrank.Prefix = "";
            this.wrank.Size = new System.Drawing.Size(215, 20);
            this.wrank.SkipValidation = false;
            this.wrank.TabIndex = 8;
            this.wrank.TextType = CrplControlLibrary.TextType.String;
            // 
            // w_cat
            // 
            this.w_cat.AllowSpace = true;
            this.w_cat.AssociatedLookUpName = "";
            this.w_cat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_cat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_cat.ContinuationTextBox = null;
            this.w_cat.CustomEnabled = true;
            this.w_cat.DataFieldMapping = "";
            this.w_cat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_cat.GetRecordsOnUpDownKeys = false;
            this.w_cat.IsDate = false;
            this.w_cat.Location = new System.Drawing.Point(160, 252);
            this.w_cat.MaxLength = 1;
            this.w_cat.Name = "w_cat";
            this.w_cat.NumberFormat = "###,###,##0.00";
            this.w_cat.Postfix = "";
            this.w_cat.Prefix = "";
            this.w_cat.Size = new System.Drawing.Size(215, 20);
            this.w_cat.SkipValidation = false;
            this.w_cat.TabIndex = 7;
            this.w_cat.TextType = CrplControlLibrary.TextType.String;
            // 
            // w_seg
            // 
            this.w_seg.AllowSpace = true;
            this.w_seg.AssociatedLookUpName = "";
            this.w_seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_seg.ContinuationTextBox = null;
            this.w_seg.CustomEnabled = true;
            this.w_seg.DataFieldMapping = "";
            this.w_seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_seg.GetRecordsOnUpDownKeys = false;
            this.w_seg.IsDate = false;
            this.w_seg.Location = new System.Drawing.Point(160, 226);
            this.w_seg.MaxLength = 3;
            this.w_seg.Name = "w_seg";
            this.w_seg.NumberFormat = "###,###,##0.00";
            this.w_seg.Postfix = "";
            this.w_seg.Prefix = "";
            this.w_seg.Size = new System.Drawing.Size(215, 20);
            this.w_seg.SkipValidation = false;
            this.w_seg.TabIndex = 6;
            this.w_seg.TextType = CrplControlLibrary.TextType.String;
            // 
            // w_branch
            // 
            this.w_branch.AllowSpace = true;
            this.w_branch.AssociatedLookUpName = "";
            this.w_branch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_branch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_branch.ContinuationTextBox = null;
            this.w_branch.CustomEnabled = true;
            this.w_branch.DataFieldMapping = "";
            this.w_branch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_branch.GetRecordsOnUpDownKeys = false;
            this.w_branch.IsDate = false;
            this.w_branch.Location = new System.Drawing.Point(160, 200);
            this.w_branch.MaxLength = 3;
            this.w_branch.Name = "w_branch";
            this.w_branch.NumberFormat = "###,###,##0.00";
            this.w_branch.Postfix = "";
            this.w_branch.Prefix = "";
            this.w_branch.Size = new System.Drawing.Size(215, 20);
            this.w_branch.SkipValidation = false;
            this.w_branch.TabIndex = 5;
            this.w_branch.TextType = CrplControlLibrary.TextType.String;
            // 
            // whead
            // 
            this.whead.AllowSpace = true;
            this.whead.AssociatedLookUpName = "";
            this.whead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.whead.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.whead.ContinuationTextBox = null;
            this.whead.CustomEnabled = true;
            this.whead.DataFieldMapping = "";
            this.whead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whead.GetRecordsOnUpDownKeys = false;
            this.whead.IsDate = false;
            this.whead.Location = new System.Drawing.Point(160, 174);
            this.whead.MaxLength = 200;
            this.whead.Name = "whead";
            this.whead.NumberFormat = "###,###,##0.00";
            this.whead.Postfix = "";
            this.whead.Prefix = "";
            this.whead.Size = new System.Drawing.Size(215, 20);
            this.whead.SkipValidation = false;
            this.whead.TabIndex = 4;
            this.whead.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 305);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(143, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Enter Reporting Order  :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 279);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Enter Ranks  :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 252);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(150, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "[O]=Officer [C]=Clerical  :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 226);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Enter GF/GCB  :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Enter Report Heading  :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Enter Branch or [ALL]  :";
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "Printer",
            "File",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(160, 67);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(215, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(225, 343);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(160, 146);
            this.copies.MaxLength = 2;
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(215, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 3;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(158, 343);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Dest_Format
            // 
            this.Dest_Format.AllowSpace = true;
            this.Dest_Format.AssociatedLookUpName = "";
            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Format.ContinuationTextBox = null;
            this.Dest_Format.CustomEnabled = true;
            this.Dest_Format.DataFieldMapping = "";
            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Format.GetRecordsOnUpDownKeys = false;
            this.Dest_Format.IsDate = false;
            this.Dest_Format.Location = new System.Drawing.Point(160, 121);
            this.Dest_Format.MaxLength = 40;
            this.Dest_Format.Name = "Dest_Format";
            this.Dest_Format.NumberFormat = "###,###,##0.00";
            this.Dest_Format.Postfix = "";
            this.Dest_Format.Prefix = "";
            this.Dest_Format.Size = new System.Drawing.Size(215, 20);
            this.Dest_Format.SkipValidation = false;
            this.Dest_Format.TabIndex = 2;
            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(160, 94);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(215, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 1;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Number Of Copies";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Destination Format";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Destination Type";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::iCORE.Properties.Resources.Citi;
            this.pictureBox1.Location = new System.Drawing.Point(377, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 64);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(114, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(208, 15);
            this.label9.TabIndex = 85;
            this.label9.Text = "Enter values for the parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(157, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 15);
            this.label8.TabIndex = 84;
            this.label8.Text = "Report Parameter";
            // 
            // CHRIS_PayrollReports_RankingReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 424);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_RankingReport";
            this.Text = "CHRIS PayrollReports Monthly Tax Report";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox copies;
        private CrplControlLibrary.SLTextBox Dest_Format;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox ord;
        private CrplControlLibrary.SLTextBox wrank;
        private CrplControlLibrary.SLTextBox w_cat;
        private CrplControlLibrary.SLTextBox w_seg;
        private CrplControlLibrary.SLTextBox w_branch;
        private CrplControlLibrary.SLTextBox whead;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
    }
}//=======
//namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
//{
//    partial class CHRIS_PayrollReports_FTEDesignationGroup
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.components = new System.ComponentModel.Container();
//            this.groupBox1 = new System.Windows.Forms.GroupBox();
//            this.w_grp = new CrplControlLibrary.SLTextBox(this.components);
//            this.label10 = new System.Windows.Forms.Label();
//            this.W_DATE = new CrplControlLibrary.SLDatePicker(this.components);
//            this.w_seg = new CrplControlLibrary.SLTextBox(this.components);
//            this.label7 = new System.Windows.Forms.Label();
//            this.w_brn = new CrplControlLibrary.SLTextBox(this.components);
//            this.label6 = new System.Windows.Forms.Label();
//            this.label5 = new System.Windows.Forms.Label();
//            this.Dest_Type = new CrplControlLibrary.SLComboBox();
//            this.pictureBox1 = new System.Windows.Forms.PictureBox();
//            this.button2 = new System.Windows.Forms.Button();
//            this.copies = new CrplControlLibrary.SLTextBox(this.components);
//            this.button1 = new System.Windows.Forms.Button();
//            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
//            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
//            this.label9 = new System.Windows.Forms.Label();
//            this.label8 = new System.Windows.Forms.Label();
//            this.label4 = new System.Windows.Forms.Label();
//            this.label3 = new System.Windows.Forms.Label();
//            this.label2 = new System.Windows.Forms.Label();
//            this.label1 = new System.Windows.Forms.Label();
//            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
//            this.groupBox1.SuspendLayout();
//            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
//            this.SuspendLayout();
//            // 
//            // groupBox1
//            // 
//            this.groupBox1.Controls.Add(this.w_grp);
//            this.groupBox1.Controls.Add(this.label10);
//            this.groupBox1.Controls.Add(this.W_DATE);
//            this.groupBox1.Controls.Add(this.w_seg);
//            this.groupBox1.Controls.Add(this.label7);
//            this.groupBox1.Controls.Add(this.w_brn);
//            this.groupBox1.Controls.Add(this.label6);
//            this.groupBox1.Controls.Add(this.label5);
//            this.groupBox1.Controls.Add(this.Dest_Type);
//            this.groupBox1.Controls.Add(this.pictureBox1);
//            this.groupBox1.Controls.Add(this.button2);
//            this.groupBox1.Controls.Add(this.copies);
//            this.groupBox1.Controls.Add(this.button1);
//            this.groupBox1.Controls.Add(this.Dest_Format);
//            this.groupBox1.Controls.Add(this.Dest_name);
//            this.groupBox1.Controls.Add(this.label9);
//            this.groupBox1.Controls.Add(this.label8);
//            this.groupBox1.Controls.Add(this.label4);
//            this.groupBox1.Controls.Add(this.label3);
//            this.groupBox1.Controls.Add(this.label2);
//            this.groupBox1.Controls.Add(this.label1);
//            this.groupBox1.Location = new System.Drawing.Point(12, 39);
//            this.groupBox1.Name = "groupBox1";
//            this.groupBox1.Size = new System.Drawing.Size(422, 344);
//            this.groupBox1.TabIndex = 0;
//            this.groupBox1.TabStop = false;
//            // 
//            // w_grp
//            // 
//            this.w_grp.AllowSpace = true;
//            this.w_grp.AssociatedLookUpName = "";
//            this.w_grp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.w_grp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.w_grp.ContinuationTextBox = null;
//            this.w_grp.CustomEnabled = true;
//            this.w_grp.DataFieldMapping = "";
//            this.w_grp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.w_grp.GetRecordsOnUpDownKeys = false;
//            this.w_grp.IsDate = false;
//            this.w_grp.Location = new System.Drawing.Point(232, 277);
//            this.w_grp.MaxLength = 3;
//            this.w_grp.Name = "w_grp";
//            this.w_grp.NumberFormat = "###,###,##0.00";
//            this.w_grp.Postfix = "";
//            this.w_grp.Prefix = "";
//            this.w_grp.Size = new System.Drawing.Size(128, 20);
//            this.w_grp.SkipValidation = false;
//            this.w_grp.TabIndex = 7;
//            this.w_grp.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // label10
//            // 
//            this.label10.AutoSize = true;
//            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label10.Location = new System.Drawing.Point(52, 279);
//            this.label10.Name = "label10";
//            this.label10.Size = new System.Drawing.Size(140, 13);
//            this.label10.TabIndex = 24;
//            this.label10.Text = "Enter Valid Group or All";
//            // 
//            // W_DATE
//            // 
//            this.W_DATE.CustomEnabled = true;
//            this.W_DATE.CustomFormat = "dd/MM/yyyy";
//            this.W_DATE.DataFieldMapping = "";
//            this.W_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
//            this.W_DATE.Location = new System.Drawing.Point(232, 225);
//            this.W_DATE.Name = "W_DATE";
//            this.W_DATE.NullValue = " ";
//            this.W_DATE.Size = new System.Drawing.Size(128, 20);
//            this.W_DATE.TabIndex = 5;
//            this.W_DATE.Value = new System.DateTime(2011, 1, 14, 10, 9, 54, 428);
//            // 
//            // w_seg
//            // 
//            this.w_seg.AllowSpace = true;
//            this.w_seg.AssociatedLookUpName = "";
//            this.w_seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.w_seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.w_seg.ContinuationTextBox = null;
//            this.w_seg.CustomEnabled = true;
//            this.w_seg.DataFieldMapping = "";
//            this.w_seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.w_seg.GetRecordsOnUpDownKeys = false;
//            this.w_seg.IsDate = false;
//            this.w_seg.Location = new System.Drawing.Point(232, 251);
//            this.w_seg.MaxLength = 3;
//            this.w_seg.Name = "w_seg";
//            this.w_seg.NumberFormat = "###,###,##0.00";
//            this.w_seg.Postfix = "";
//            this.w_seg.Prefix = "";
//            this.w_seg.Size = new System.Drawing.Size(128, 20);
//            this.w_seg.SkipValidation = false;
//            this.w_seg.TabIndex = 6;
//            this.w_seg.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // label7
//            // 
//            this.label7.AutoSize = true;
//            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label7.Location = new System.Drawing.Point(52, 253);
//            this.label7.Name = "label7";
//            this.label7.Size = new System.Drawing.Size(155, 13);
//            this.label7.TabIndex = 21;
//            this.label7.Text = "Enter Valid Segment or All";
//            // 
//            // w_brn
//            // 
//            this.w_brn.AllowSpace = true;
//            this.w_brn.AssociatedLookUpName = "";
//            this.w_brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.w_brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.w_brn.ContinuationTextBox = null;
//            this.w_brn.CustomEnabled = true;
//            this.w_brn.DataFieldMapping = "";
//            this.w_brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.w_brn.GetRecordsOnUpDownKeys = false;
//            this.w_brn.IsDate = false;
//            this.w_brn.Location = new System.Drawing.Point(232, 199);
//            this.w_brn.MaxLength = 3;
//            this.w_brn.Name = "w_brn";
//            this.w_brn.NumberFormat = "###,###,##0.00";
//            this.w_brn.Postfix = "";
//            this.w_brn.Prefix = "";
//            this.w_brn.Size = new System.Drawing.Size(128, 20);
//            this.w_brn.SkipValidation = false;
//            this.w_brn.TabIndex = 4;
//            this.w_brn.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // label6
//            // 
//            this.label6.AutoSize = true;
//            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label6.Location = new System.Drawing.Point(52, 201);
//            this.label6.Name = "label6";
//            this.label6.Size = new System.Drawing.Size(154, 13);
//            this.label6.TabIndex = 19;
//            this.label6.Text = "Enter Valid Branch or ALL";
//            // 
//            // label5
//            // 
//            this.label5.AutoSize = true;
//            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label5.Location = new System.Drawing.Point(52, 227);
//            this.label5.Name = "label5";
//            this.label5.Size = new System.Drawing.Size(166, 13);
//            this.label5.TabIndex = 17;
//            this.label5.Text = "Enter As Of [DD/MM/YYYY]";
//            // 
//            // Dest_Type
//            // 
//            this.Dest_Type.BusinessEntity = "";
//            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
//            this.Dest_Type.CustomEnabled = true;
//            this.Dest_Type.DataFieldMapping = "";
//            this.Dest_Type.FormattingEnabled = true;
//            this.Dest_Type.Items.AddRange(new object[] {
//            "Screen",
//            "Printer",
//            "File",
//            "Mail"});
//            this.Dest_Type.Location = new System.Drawing.Point(232, 94);
//            this.Dest_Type.LOVType = "";
//            this.Dest_Type.Name = "Dest_Type";
//            this.Dest_Type.Size = new System.Drawing.Size(128, 21);
//            this.Dest_Type.SPName = "";
//            this.Dest_Type.TabIndex = 0;
//            // 
//            // pictureBox1
//            // 
//            this.pictureBox1.Image = global::iCORE.Properties.Resources.Citi;
//            this.pictureBox1.Location = new System.Drawing.Point(354, 2);
//            this.pictureBox1.Name = "pictureBox1";
//            this.pictureBox1.Size = new System.Drawing.Size(68, 64);
//            this.pictureBox1.TabIndex = 11;
//            this.pictureBox1.TabStop = false;
//            // 
//            // button2
//            // 
//            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.button2.Location = new System.Drawing.Point(251, 315);
//            this.button2.Name = "button2";
//            this.button2.Size = new System.Drawing.Size(63, 23);
//            this.button2.TabIndex = 9;
//            this.button2.Text = "Close";
//            this.button2.UseVisualStyleBackColor = true;
//            this.button2.Click += new System.EventHandler(this.button2_Click);
//            // 
//            // copies
//            // 
//            this.copies.AllowSpace = true;
//            this.copies.AssociatedLookUpName = "";
//            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.copies.ContinuationTextBox = null;
//            this.copies.CustomEnabled = true;
//            this.copies.DataFieldMapping = "";
//            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.copies.GetRecordsOnUpDownKeys = false;
//            this.copies.IsDate = false;
//            this.copies.Location = new System.Drawing.Point(232, 173);
//            this.copies.Name = "copies";
//            this.copies.NumberFormat = "###,###,##0.00";
//            this.copies.Postfix = "";
//            this.copies.Prefix = "";
//            this.copies.Size = new System.Drawing.Size(128, 20);
//            this.copies.SkipValidation = false;
//            this.copies.TabIndex = 3;
//            this.copies.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // button1
//            // 
//            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.button1.Location = new System.Drawing.Point(184, 315);
//            this.button1.Name = "button1";
//            this.button1.Size = new System.Drawing.Size(63, 23);
//            this.button1.TabIndex = 8;
//            this.button1.Text = "Run";
//            this.button1.UseVisualStyleBackColor = true;
//            this.button1.Click += new System.EventHandler(this.button1_Click);
//            // 
//            // Dest_Format
//            // 
//            this.Dest_Format.AllowSpace = true;
//            this.Dest_Format.AssociatedLookUpName = "";
//            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.Dest_Format.ContinuationTextBox = null;
//            this.Dest_Format.CustomEnabled = true;
//            this.Dest_Format.DataFieldMapping = "";
//            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.Dest_Format.GetRecordsOnUpDownKeys = false;
//            this.Dest_Format.IsDate = false;
//            this.Dest_Format.Location = new System.Drawing.Point(232, 148);
//            this.Dest_Format.Name = "Dest_Format";
//            this.Dest_Format.NumberFormat = "###,###,##0.00";
//            this.Dest_Format.Postfix = "";
//            this.Dest_Format.Prefix = "";
//            this.Dest_Format.Size = new System.Drawing.Size(128, 20);
//            this.Dest_Format.SkipValidation = false;
//            this.Dest_Format.TabIndex = 2;
//            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // Dest_name
//            // 
//            this.Dest_name.AllowSpace = true;
//            this.Dest_name.AssociatedLookUpName = "";
//            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.Dest_name.ContinuationTextBox = null;
//            this.Dest_name.CustomEnabled = true;
//            this.Dest_name.DataFieldMapping = "";
//            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.Dest_name.GetRecordsOnUpDownKeys = false;
//            this.Dest_name.IsDate = false;
//            this.Dest_name.Location = new System.Drawing.Point(232, 121);
//            this.Dest_name.Name = "Dest_name";
//            this.Dest_name.NumberFormat = "###,###,##0.00";
//            this.Dest_name.Postfix = "";
//            this.Dest_name.Prefix = "";
//            this.Dest_name.Size = new System.Drawing.Size(128, 20);
//            this.Dest_name.SkipValidation = false;
//            this.Dest_name.TabIndex = 1;
//            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // label9
//            // 
//            this.label9.AutoSize = true;
//            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label9.Location = new System.Drawing.Point(110, 53);
//            this.label9.Name = "label9";
//            this.label9.Size = new System.Drawing.Size(168, 13);
//            this.label9.TabIndex = 8;
//            this.label9.Text = "Enter Values For Parameters";
//            // 
//            // label8
//            // 
//            this.label8.AutoSize = true;
//            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label8.Location = new System.Drawing.Point(124, 16);
//            this.label8.Name = "label8";
//            this.label8.Size = new System.Drawing.Size(142, 20);
//            this.label8.TabIndex = 7;
//            this.label8.Text = "Report Parameter";
//            // 
//            // label4
//            // 
//            this.label4.AutoSize = true;
//            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label4.Location = new System.Drawing.Point(52, 173);
//            this.label4.Name = "label4";
//            this.label4.Size = new System.Drawing.Size(109, 13);
//            this.label4.TabIndex = 3;
//            this.label4.Text = "Number Of Copies";
//            // 
//            // label3
//            // 
//            this.label3.AutoSize = true;
//            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label3.Location = new System.Drawing.Point(52, 148);
//            this.label3.Name = "label3";
//            this.label3.Size = new System.Drawing.Size(113, 13);
//            this.label3.TabIndex = 2;
//            this.label3.Text = "Destination Format";
//            // 
//            // label2
//            // 
//            this.label2.AutoSize = true;
//            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label2.Location = new System.Drawing.Point(52, 121);
//            this.label2.Name = "label2";
//            this.label2.Size = new System.Drawing.Size(107, 13);
//            this.label2.TabIndex = 1;
//            this.label2.Text = "Destination Name";
//            // 
//            // label1
//            // 
//            this.label1.AutoSize = true;
//            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label1.Location = new System.Drawing.Point(52, 94);
//            this.label1.Name = "label1";
//            this.label1.Size = new System.Drawing.Size(103, 13);
//            this.label1.TabIndex = 0;
//            this.label1.Text = "Destination Type";
//            // 
//            // CHRIS_PayrollReports_FTEDesignationGroup
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.ClientSize = new System.Drawing.Size(446, 395);
//            this.Controls.Add(this.groupBox1);
//            this.Name = "CHRIS_PayrollReports_FTEDesignationGroup";
//            this.Text = "CHRIS PayrollReports Monthly Tax Report";
//            this.Controls.SetChildIndex(this.groupBox1, 0);
//            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
//            this.groupBox1.ResumeLayout(false);
//            this.groupBox1.PerformLayout();
//            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
//            this.ResumeLayout(false);
//            this.PerformLayout();

//        }

//        #endregion

//        private System.Windows.Forms.GroupBox groupBox1;
//        private System.Windows.Forms.Label label4;
//        private System.Windows.Forms.Label label3;
//        private System.Windows.Forms.Label label2;
//        private System.Windows.Forms.Label label1;
//        private CrplControlLibrary.SLTextBox copies;
//        private CrplControlLibrary.SLTextBox Dest_Format;
//        private CrplControlLibrary.SLTextBox Dest_name;
//        private System.Windows.Forms.Label label9;
//        private System.Windows.Forms.Label label8;
//        private System.Windows.Forms.Button button1;
//        private System.Windows.Forms.Button button2;
//        private System.Windows.Forms.PictureBox pictureBox1;
//        private CrplControlLibrary.SLComboBox Dest_Type;
//        private System.Windows.Forms.Label label5;
//        private CrplControlLibrary.SLTextBox w_seg;
//        private System.Windows.Forms.Label label7;
//        private CrplControlLibrary.SLTextBox w_brn;
//        private System.Windows.Forms.Label label6;
//        private CrplControlLibrary.SLDatePicker W_DATE;
//        private CrplControlLibrary.SLTextBox w_grp;
//        private System.Windows.Forms.Label label10;
//    }
//}>>>>>>> .r15851
