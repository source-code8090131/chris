namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_FTE010SEG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.year = new CrplControlLibrary.SLTextBox(this.components);
            this.month = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.cmbyear = new CrplControlLibrary.SLComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbmonth = new CrplControlLibrary.SLComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.bran = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.seg = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.seg);
            this.groupBox1.Controls.Add(this.year);
            this.groupBox1.Controls.Add(this.month);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cmbyear);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cmbmonth);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.bran);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(412, 276);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // year
            // 
            this.year.AllowSpace = true;
            this.year.AssociatedLookUpName = "";
            this.year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.year.ContinuationTextBox = null;
            this.year.CustomEnabled = true;
            this.year.DataFieldMapping = "";
            this.year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.year.GetRecordsOnUpDownKeys = false;
            this.year.IsDate = false;
            this.year.Location = new System.Drawing.Point(354, 119);
            this.year.MaxLength = 4;
            this.year.Name = "year";
            this.year.NumberFormat = "###,###,##0.00";
            this.year.Postfix = "";
            this.year.Prefix = "";
            this.year.Size = new System.Drawing.Size(52, 20);
            this.year.SkipValidation = false;
            this.year.TabIndex = 29;
            this.year.TextType = CrplControlLibrary.TextType.Integer;
            this.year.Visible = false;
            // 
            // month
            // 
            this.month.AllowSpace = true;
            this.month.AssociatedLookUpName = "";
            this.month.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.month.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.month.ContinuationTextBox = null;
            this.month.CustomEnabled = true;
            this.month.DataFieldMapping = "";
            this.month.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.month.GetRecordsOnUpDownKeys = false;
            this.month.IsDate = false;
            this.month.Location = new System.Drawing.Point(354, 95);
            this.month.MaxLength = 2;
            this.month.Name = "month";
            this.month.NumberFormat = "###,###,##0.00";
            this.month.Postfix = "";
            this.month.Prefix = "";
            this.month.Size = new System.Drawing.Size(52, 20);
            this.month.SkipValidation = false;
            this.month.TabIndex = 28;
            this.month.TextType = CrplControlLibrary.TextType.Integer;
            this.month.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(35, 176);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Seg";
            // 
            // cmbyear
            // 
            this.cmbyear.BusinessEntity = "";
            this.cmbyear.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbyear.CustomEnabled = true;
            this.cmbyear.DataFieldMapping = "";
            this.cmbyear.FormattingEnabled = true;
            this.cmbyear.Location = new System.Drawing.Point(126, 148);
            this.cmbyear.LOVType = "";
            this.cmbyear.Name = "cmbyear";
            this.cmbyear.Size = new System.Drawing.Size(128, 21);
            this.cmbyear.SPName = "";
            this.cmbyear.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(35, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Year";
            // 
            // cmbmonth
            // 
            this.cmbmonth.BusinessEntity = "";
            this.cmbmonth.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbmonth.CustomEnabled = true;
            this.cmbmonth.DataFieldMapping = "";
            this.cmbmonth.FormattingEnabled = true;
            this.cmbmonth.Location = new System.Drawing.Point(126, 121);
            this.cmbmonth.LOVType = "";
            this.cmbmonth.Name = "cmbmonth";
            this.cmbmonth.Size = new System.Drawing.Size(128, 21);
            this.cmbmonth.SPName = "";
            this.cmbmonth.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(35, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Month";
            // 
            // bran
            // 
            this.bran.AllowSpace = true;
            this.bran.AssociatedLookUpName = "";
            this.bran.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bran.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.bran.ContinuationTextBox = null;
            this.bran.CustomEnabled = true;
            this.bran.DataFieldMapping = "";
            this.bran.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bran.GetRecordsOnUpDownKeys = false;
            this.bran.IsDate = false;
            this.bran.Location = new System.Drawing.Point(126, 204);
            this.bran.MaxLength = 3;
            this.bran.Name = "bran";
            this.bran.NumberFormat = "###,###,##0.00";
            this.bran.Postfix = "";
            this.bran.Prefix = "";
            this.bran.Size = new System.Drawing.Size(128, 20);
            this.bran.SkipValidation = false;
            this.bran.TabIndex = 6;
            this.bran.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 207);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Branch";
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "Printer",
            "File",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(126, 68);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(128, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(193, 240);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(126, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(126, 95);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(128, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 1;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(123, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(208, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Enter values for the parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(157, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Report Parameters";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(35, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Desname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(35, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Destype";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::iCORE.Properties.Resources.Citi;
            this.pictureBox1.Location = new System.Drawing.Point(374, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 64);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // seg
            // 
            this.seg.AllowSpace = true;
            this.seg.AssociatedLookUpName = "";
            this.seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.seg.ContinuationTextBox = null;
            this.seg.CustomEnabled = true;
            this.seg.DataFieldMapping = "";
            this.seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seg.GetRecordsOnUpDownKeys = false;
            this.seg.IsDate = false;
            this.seg.Location = new System.Drawing.Point(126, 178);
            this.seg.MaxLength = 3;
            this.seg.Name = "seg";
            this.seg.NumberFormat = "###,###,##0.00";
            this.seg.Postfix = "";
            this.seg.Prefix = "";
            this.seg.Size = new System.Drawing.Size(128, 20);
            this.seg.SkipValidation = false;
            this.seg.TabIndex = 5;
            this.seg.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_PayrollReports_FTE010SEG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 324);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_FTE010SEG";
            this.Text = "CHRIS PayrollReports Monthly Tax Report";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLTextBox bran;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLComboBox cmbyear;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLComboBox cmbmonth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox year;
        private CrplControlLibrary.SLTextBox month;
        private System.Windows.Forms.PictureBox pictureBox1;
        private CrplControlLibrary.SLTextBox seg;
    }
}