using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_FTE010JAW : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_FTE010JAW()
        {
            InitializeComponent();
        }

        public CHRIS_PayrollReports_FTE010JAW(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            //this.mode.Items.RemoveAt(this.mode.Items.Count - 1);
            //fillsegCombo();

            DataTable dt = new DataTable();
            dt.Columns.Add("Year");
            dt.Rows.Add(new object[] { DateTime.Today.Year.ToString() });

            this.cmbyear.ValueMember = "Year";
            this.cmbyear.DisplayMember = "Year";
            this.cmbyear.DataSource = dt;

            DataTable month = new DataTable();
            month.Columns.Add("Month");
            month.Rows.Add(new object[] { DateTime.Today.Month.ToString() });

            this.cmbmonth.ValueMember = "Month";
            this.cmbmonth.DisplayMember = "Month";
            this.cmbmonth.DataSource = month;

            //this.cmbmonth.Items.Add("2");
            //this.cmbYear.Items.Add("2011");
            
            //this.cmbmonth.Items.RemoveAt(0);
            //this.cmbYear.Items.RemoveAt(0);

            this.seg.Text = "GF";

            Dest_name.Text = "C:\\iCORE-Spool\\";
            

        }


        private void button1_Click(object sender, EventArgs e)
        {
               


            {

                base.RptFileName = "FTE010JAW";




                string Str = this.cmbmonth.Text.Trim();

                double Num;

                bool isNum = double.TryParse(Str, out Num);

                if (isNum)


                    this.month.Text = this.cmbmonth.Text;
                else
                    this.month.Text = "0"; //string data


                string Str1 = this.cmbyear.Text.Trim();

                double Num1;

                bool isNum1 = double.TryParse(Str1, out Num1);

                if (isNum1)


                    this.year.Text = this.cmbyear.Text;
                else
                    this.year.Text = "0"; //string data


                
                
                //this.year.Text  = this.cmbyear.Text; 



                
                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (Dest_Type.Text == "Printer")
                {
                    base.PrintCustomReport();
                }
                else if (Dest_Type.Text == "File")
                {

                    string DestName;
                    string DestFormat;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }                

                    else
                    {
                        DestName = this.Dest_name.Text;
                    }

                        base.ExportCustomReport(DestName, "pdf");

                }
                else if (Dest_Type.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\\Report";
                    string DestFormat;
                    string RecipentName;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_name.Text;
                    }

                        base.EmailToReport(DestName, "pdf", RecipentName);
                       

                }


            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fillsegCombo()
        {
            //Result rsltCode;
            //CmnDataManager cmnDataManager = new CmnDataManager();

            //Dictionary<string, object> param = new Dictionary<string, object>();
            //rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

            //if (rsltCode.isSuccessful)
            //{
            //    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
            //    {
            //        this.seg.DisplayMember = "pr_segment";
            //        this.seg.ValueMember = "pr_segment";
            //        this.seg.DataSource = rsltCode.dstResult.Tables[0];
            //    }

            //}

        }

        private void seg_TextChanged(object sender, EventArgs e)
        {

        }

        

       

    }
}