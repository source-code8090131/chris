namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_EForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PayrollReports_EForm));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sen_officer = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.init_officer = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.rate = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.l_name = new CrplControlLibrary.SLTextBox(this.components);
            this.name = new CrplControlLibrary.SLTextBox(this.components);
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(249, 261);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(63, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(182, 261);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(63, 23);
            this.btnRun.TabIndex = 7;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(65, 136);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "First Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Last Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Copies";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Destype";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sen_officer);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.init_officer);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.rate);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.l_name);
            this.groupBox1.Controls.Add(this.name);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(493, 305);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // sen_officer
            // 
            this.sen_officer.AllowSpace = true;
            this.sen_officer.AssociatedLookUpName = "";
            this.sen_officer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sen_officer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.sen_officer.ContinuationTextBox = null;
            this.sen_officer.CustomEnabled = true;
            this.sen_officer.DataFieldMapping = "";
            this.sen_officer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sen_officer.GetRecordsOnUpDownKeys = false;
            this.sen_officer.IsDate = false;
            this.sen_officer.Location = new System.Drawing.Point(184, 235);
            this.sen_officer.MaxLength = 15;
            this.sen_officer.Name = "sen_officer";
            this.sen_officer.NumberFormat = "###,###,##0.00";
            this.sen_officer.Postfix = "";
            this.sen_officer.Prefix = "";
            this.sen_officer.Size = new System.Drawing.Size(154, 20);
            this.sen_officer.SkipValidation = false;
            this.sen_officer.TabIndex = 6;
            this.sen_officer.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(65, 239);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 83;
            this.label6.Text = "Senior Officer";
            // 
            // init_officer
            // 
            this.init_officer.AllowSpace = true;
            this.init_officer.AssociatedLookUpName = "";
            this.init_officer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.init_officer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.init_officer.ContinuationTextBox = null;
            this.init_officer.CustomEnabled = true;
            this.init_officer.DataFieldMapping = "";
            this.init_officer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.init_officer.GetRecordsOnUpDownKeys = false;
            this.init_officer.IsDate = false;
            this.init_officer.Location = new System.Drawing.Point(184, 209);
            this.init_officer.MaxLength = 15;
            this.init_officer.Name = "init_officer";
            this.init_officer.NumberFormat = "###,###,##0.00";
            this.init_officer.Postfix = "";
            this.init_officer.Prefix = "";
            this.init_officer.Size = new System.Drawing.Size(154, 20);
            this.init_officer.SkipValidation = false;
            this.init_officer.TabIndex = 5;
            this.init_officer.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(65, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 81;
            this.label5.Text = "Initiating Officer";
            // 
            // rate
            // 
            this.rate.AllowSpace = true;
            this.rate.AssociatedLookUpName = "";
            this.rate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.rate.ContinuationTextBox = null;
            this.rate.CustomEnabled = true;
            this.rate.DataFieldMapping = "";
            this.rate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rate.GetRecordsOnUpDownKeys = false;
            this.rate.IsDate = false;
            this.rate.Location = new System.Drawing.Point(184, 183);
            this.rate.MaxLength = 8;
            this.rate.Name = "rate";
            this.rate.NumberFormat = "###,###,##0.00";
            this.rate.Postfix = "";
            this.rate.Prefix = "";
            this.rate.Size = new System.Drawing.Size(154, 20);
            this.rate.SkipValidation = false;
            this.rate.TabIndex = 4;
            this.rate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.rate.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 79;
            this.label2.Text = "Rate";
            // 
            // l_name
            // 
            this.l_name.AllowSpace = true;
            this.l_name.AssociatedLookUpName = "";
            this.l_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.l_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.l_name.ContinuationTextBox = null;
            this.l_name.CustomEnabled = true;
            this.l_name.DataFieldMapping = "";
            this.l_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_name.GetRecordsOnUpDownKeys = false;
            this.l_name.IsDate = false;
            this.l_name.Location = new System.Drawing.Point(184, 158);
            this.l_name.MaxLength = 20;
            this.l_name.Name = "l_name";
            this.l_name.NumberFormat = "###,###,##0.00";
            this.l_name.Postfix = "";
            this.l_name.Prefix = "";
            this.l_name.Size = new System.Drawing.Size(152, 20);
            this.l_name.SkipValidation = false;
            this.l_name.TabIndex = 3;
            this.l_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // name
            // 
            this.name.AllowSpace = true;
            this.name.AssociatedLookUpName = "";
            this.name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.name.ContinuationTextBox = null;
            this.name.CustomEnabled = true;
            this.name.DataFieldMapping = "";
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.GetRecordsOnUpDownKeys = false;
            this.name.IsDate = false;
            this.name.Location = new System.Drawing.Point(184, 132);
            this.name.MaxLength = 20;
            this.name.Name = "name";
            this.name.NumberFormat = "###,###,##0.00";
            this.name.Postfix = "";
            this.name.Prefix = "";
            this.name.Size = new System.Drawing.Size(154, 20);
            this.name.SkipValidation = false;
            this.name.TabIndex = 2;
            this.name.TextType = CrplControlLibrary.TextType.String;
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(184, 105);
            this.copies.MaxLength = 2;
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(152, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 1;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "Printer",
            "File",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(184, 75);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(154, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(171, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(168, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Enter Parameters For Report";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(188, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(142, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Report Parameter";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(450, 38);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 78;
            this.pictureBox2.TabStop = false;
            // 
            // CHRIS_PayrollReports_EForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 351);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_EForm";
            this.Text = "CHRIS_PayrollReports_SalaryArrearDetailReport";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox l_name;
        private CrplControlLibrary.SLTextBox name;
        private CrplControlLibrary.SLTextBox copies;
        private CrplControlLibrary.SLTextBox sen_officer;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox init_officer;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox rate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}