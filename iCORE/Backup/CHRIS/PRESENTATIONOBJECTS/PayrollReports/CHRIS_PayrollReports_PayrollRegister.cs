using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_PayrollRegister : BaseRptForm
    {
        private string DestFormat = "PDF";
        public CHRIS_PayrollReports_PayrollRegister()
        {
            InitializeComponent();
        }


        public CHRIS_PayrollReports_PayrollRegister(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.FillBranch();
            this.fillsegCombo();
            this.Copies.Text = "1";
            this.W_SEG.Text = "GF";
            this.branch.Text = "FBD";
            this.MONTH.Text  = "10";
            this.YEAR.Text = "2004";
        }

        private void FillBranch()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount");

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
            {
                this.branch.DisplayMember = "BRN_CODE";
                this.branch.ValueMember = "BRN_CODE";
                this.branch.DataSource = rsltCode.dstResult.Tables[0];
            }

        }
        private void fillsegCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.W_SEG.DisplayMember = "pr_segment";
                    this.W_SEG.ValueMember = "pr_segment";
                    this.W_SEG.DataSource = rsltCode.dstResult.Tables[0];
                }

            }

        }
        private void CHRIS_PayrollReports_PayrollRegister_Load(object sender, EventArgs e)
        {

        }

        private void Run_Click_1(object sender, EventArgs e)
        {
            {
                base.RptFileName = "PY007";

                CmnDataManager cmnDataManager = new CmnDataManager();
                Dictionary<String, Object> paramList = new Dictionary<string, object>();

                try
                {
                    paramList.Clear();
                    paramList.Add("branch", branch.Text);
                    paramList.Add("w_seg", W_SEG.Text);
                    paramList.Add("month", MONTH.Text);
                    paramList.Add("year", YEAR.Text);
                    paramList.Add("cat", CAT.Text);

                    cmnDataManager.Execute("CHRIS_RPT_SP_PY007U", "", paramList);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An unexpected error occured while generating Payroll Register Report.\nPlease verify your input data.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    LogException("CHRIS_PayrollReports_PayrollRegister", "Run_Click_1", ex);
                }
                finally
                {
                    cmnDataManager = null;
                    paramList = null;
                }

                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    //if (Dest_Format.Text!= string.Empty)
                    //{
                    //   base.ExportCustomReport();
                    //}
                }
                if (Dest_Type.Text == "Printer")
                {
                    //base.MatrixReport = true;
                    ///if (Dest_Format.Text!= string.Empty)
                    ///{
                    ///base.ExportCustomReport();
                    /// }
                    base.PrintCustomReport(this.Copies.Text );
                    //base.ExportCustomReport();
                    //DataSet ds = base.PrintCustomReport();

                }

                if (Dest_Type.Text == "File")
                {
                    //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                    //if (Dest_name.Text != string.Empty)
                    String Res1 = "c:\\iCORE-Spool\\Report";
                    if (Dest_name.Text != String.Empty)
                        Res1 = Dest_name.Text;
                        base.ExportCustomReport(Res1, "pdf");




                    //base.ExportCustomReport();



                }

            }

            if (Dest_Type.Text == "Mail")
            {
                String Res1 = "";
                if (Dest_name.Text != String.Empty)
                    Res1 = Dest_name.Text;

                base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", Res1);



                //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                //{
                //    base.EmailToReport(Dest_name.Text, Dest_Format.Text);
                //}
            }
            //if (Dest_Format.Text != string.Empty && Dest_Format.Text == "PDF")
            //{
            //    string Path =base.ExportReportInGivenFormat("PDF");

            //}




        }

        private void Dest_Type_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }

        
    }
}


