using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_OvertimeArrearsDetail : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_OvertimeArrearsDetail()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";

        public CHRIS_PayrollReports_OvertimeArrearsDetail(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.nocopies.Text = "1";
            this.RPT_HEAD.Text = "Working As Of Jun-30(Salaries On Old Scale As Of Mar Payroll And O.T Upto Feb-28 (paid in Mar Payroll)";
            Dest_name.Text = "C:\\iCORE-Spool\\";

        }

        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }

            no_of_copies = Convert.ToInt16(nocopies.Text);
         
            base.RptFileName = "PY040";


            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {

                base.btnCallReport_Click(sender, e);

            }
            if (Dest_Type.Text == "Printer")
            {

                base.PrintNoofCopiesReport(no_of_copies);

            }


            //if (Dest_Type.Text == "Printer")
            //{

            //    PrintCustomReport();

            //}


            if (Dest_Type.Text == "File")
            {
                base.ExportCustomReport(@"C:\iCORE-Spool\Report", "pdf");


            }


            if (Dest_Type.Text == "Mail")
            {

                //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF");

            }

            //base.RptFileName = "PY033";
            //if (slComboBox1.Text == "Screen")
            //{
            //    base.btnCallReport_Click(sender, e);
            //}
            //if (slComboBox1.Text == "Mail")
            //{
            //    EmailToReport("C:\\PY033", "pdf");
            //}
            //if (slComboBox1.Text == "Printer")
            //{
            //    PrintCustomReport();
            //}
        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

                

    }
}