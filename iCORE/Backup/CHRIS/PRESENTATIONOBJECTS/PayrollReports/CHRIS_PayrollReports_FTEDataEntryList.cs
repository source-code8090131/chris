using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_FTEDataEntryList : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_FTEDataEntryList()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";

        public CHRIS_PayrollReports_FTEDataEntryList(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
    
        }


        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;

            {
                base.RptFileName = "PY028";

                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        //no_of_copies = Convert.ToInt16(nocopies.Text == "" ? "1" : nocopies.Text);

                        base.PrintNoofCopiesReport(no_of_copies);
                    }


                }


                if (Dest_Type.Text == "File")
                {
                    String d = "c:\\iCORE-Spool\\report";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;
                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }




            }
           

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
            //this.Close();
        }
    }
}