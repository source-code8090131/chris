using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_ArrearsIndividual : BaseRptForm
    {
        public CHRIS_PayrollReports_ArrearsIndividual()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";
        public CHRIS_PayrollReports_ArrearsIndividual(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            Dest_Format.Text = "dflt";
            //this.DestFormat.Text = "dflt";
            this.copies.Text = "1";
            //Output_mod.Items.RemoveAt(3);
            this.date.Value = new DateTime(1995, 04, 22);
            Dest_name.Text = "C:\\iCORE-Spool\\";
        }

        private void Run_Click_1(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.copies.Text == String.Empty)
            {
                copies.Text = "1";
            }

            no_of_copies = Convert.ToInt16(copies.Text);

            {
                base.RptFileName = "PY043";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    //if (copies.Text != string.Empty)
                    //{
                    //    no_of_copies = Convert.ToInt16(copies.Text);
                    //    //no_of_copies = Convert.ToInt16(nocopies.Text == "" ? "1" : nocopies.Text);

                        base.PrintNoofCopiesReport(no_of_copies);
                    //}


                }




                if (Dest_Type.Text == "File")
                {
                    String d = "c:\\iCORE-Spool\\report";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;
                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }
            }

       
        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

        #endregion


    }
}