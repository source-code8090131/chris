using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_DepartmentWisePayrollClerical : BaseRptForm
    {
        public CHRIS_PayrollReports_DepartmentWisePayrollClerical()
        {
            InitializeComponent();
        }
        public CHRIS_PayrollReports_DepartmentWisePayrollClerical(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.Dest_Format.Text = "wide";
            this.Copies.Text = "1";
            this.MONTH.Text = "11";
            this.BRANCH.Text = "LHR";
            this.SEG.Text = "GCB";
            this.YEAR.Text = "2000";
            //Output_mod.Items.RemoveAt(3);
        }

        private void Run_Click_1(object sender, EventArgs e)
        {
            int no_of_copies;
            {
                base.RptFileName = "PY010";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    //if (Dest_Format.Text!= string.Empty)
                    //{
                    //   base.ExportCustomReport();
                    //}
                }
                if (Dest_Type.Text == "Printer")
                {
                    if (Copies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(Copies.Text);
                        //no_of_copies = Convert.ToInt16(nocopies.Text == "" ? "1" : nocopies.Text);

                        base.PrintNoofCopiesReport(no_of_copies);
                    }

                }

                if (Dest_Type.Text == "File")
                {
                    string d = "c:\\iCORE-Spool\\report";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;


                        base.ExportCustomReport(d, "pdf");





                }

                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;

                    base.EmailToReport("C:\\iCORE-Spool\\Report", "PDF", d);
                }
                //if (Dest_Format.Text != string.Empty && Dest_Format.Text == "PDF")
                //{
                //    string Path =base.ExportReportInGivenFormat("PDF");

                //}


            }


        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }
        #endregion 

    }
}