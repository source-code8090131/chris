using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_ArrearsReciept : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_ArrearsReciept()
        {
            InitializeComponent();
        }
    


        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();

            /* Set values for controls here */
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";
        

        public CHRIS_PayrollReports_ArrearsReciept(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            //DateTime dt = new DateTime(1993, 08, 04);
            //if (W_DATE.Value == null)
            //{
            //    W_DATE.Value = dt;
            //}
            W_DATE.Value = Convert.ToDateTime("04/08/1993");
            this.Dest_Format.Text = "dflt";
            this.copies.Text = "1";
            W_BRN.Text = "LHR";

            Dest_name.Text = "C:\\iCORE-Spool\\";
            //slDatePicker_W_FROM.Value = Convert.ToDateTime(w_date_value);

          
            //this.PrintNoofCopiesReport(1);

        }


        //private void CHRIS_PayrollReport_ArrearsReciept_Load(object sender, EventArgs e)
        //{

        //}

        private void btnRun_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PY042";


            if (this.Dest_Format.Text == string.Empty )  
                 DestFormat = "PDF";   
            else
                DestFormat = this.Dest_Format.Text;

            if (this.Dest_name.Text == string.Empty )
                DestName = @"C:\iCORE-Spool\Report";
            else
                    DestName = this.Dest_name.Text;

            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {

                base.btnCallReport_Click(sender, e);

            }
            if (Dest_Type.Text == "Printer")
            {

                PrintCustomReport(this.copies.Text );

            }


            if (Dest_Type.Text == "File")
            {
                String d = "c:\\iCORE-Spool\\report";
                if (this.Dest_name.Text != string.Empty)
                    d = this.Dest_name.Text;

                base.ExportCustomReport(d, "pdf");


            }


            if (Dest_Type.Text == "Mail")
            {
                String d = "";
                if (this.Dest_name.Text != string.Empty)
                    d = this.Dest_name.Text;
                base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

            }
            //base.RptFileName = "PY042";
            //if (slComboBox1.Text == "Screen")
            //{
            //    base.btnCallReport_Click(sender, e);
            //}
            //if (slComboBox1.Text == "Mail")
            //{
            //    EmailToReport("C:\\PY042", "pdf");
            //}
            //if (slComboBox1.Text == "Printer")
            //{
            //    PrintCustomReport();
            //}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }
    }
}