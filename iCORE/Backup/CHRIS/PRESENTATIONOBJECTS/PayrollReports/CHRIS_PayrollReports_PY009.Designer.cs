namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_PY009
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PayrollReports_PY009));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.seg = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.Year = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.branch = new CrplControlLibrary.SLTextBox(this.components);
            this.month = new CrplControlLibrary.SLTextBox(this.components);
            this.DesName = new CrplControlLibrary.SLTextBox(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(213, 224);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(63, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(146, 224);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(63, 23);
            this.btnRun.TabIndex = 6;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 141);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Month";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Branch";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "DesName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Destype";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.seg);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Year);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.branch);
            this.groupBox1.Controls.Add(this.month);
            this.groupBox1.Controls.Add(this.DesName);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 283);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(143, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(208, 15);
            this.label9.TabIndex = 83;
            this.label9.Text = "Enter values for the parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(186, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 15);
            this.label8.TabIndex = 82;
            this.label8.Text = "Report Parameter";
            // 
            // seg
            // 
            this.seg.AllowSpace = true;
            this.seg.AssociatedLookUpName = "";
            this.seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.seg.ContinuationTextBox = null;
            this.seg.CustomEnabled = true;
            this.seg.DataFieldMapping = "";
            this.seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seg.GetRecordsOnUpDownKeys = false;
            this.seg.IsDate = false;
            this.seg.Location = new System.Drawing.Point(146, 193);
            this.seg.MaxLength = 3;
            this.seg.Name = "seg";
            this.seg.NumberFormat = "###,###,##0.00";
            this.seg.Postfix = "";
            this.seg.Prefix = "";
            this.seg.Size = new System.Drawing.Size(153, 20);
            this.seg.SkipValidation = false;
            this.seg.TabIndex = 5;
            this.seg.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 81;
            this.label5.Text = "Segment";
            // 
            // Year
            // 
            this.Year.AllowSpace = true;
            this.Year.AssociatedLookUpName = "";
            this.Year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Year.ContinuationTextBox = null;
            this.Year.CustomEnabled = true;
            this.Year.DataFieldMapping = "";
            this.Year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Year.GetRecordsOnUpDownKeys = false;
            this.Year.IsDate = false;
            this.Year.Location = new System.Drawing.Point(146, 167);
            this.Year.MaxLength = 4;
            this.Year.Name = "Year";
            this.Year.NumberFormat = "###,###,##0.00";
            this.Year.Postfix = "";
            this.Year.Prefix = "";
            this.Year.Size = new System.Drawing.Size(153, 20);
            this.Year.SkipValidation = false;
            this.Year.TabIndex = 4;
            this.Year.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 79;
            this.label2.Text = "Year";
            // 
            // branch
            // 
            this.branch.AllowSpace = true;
            this.branch.AssociatedLookUpName = "";
            this.branch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.branch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.branch.ContinuationTextBox = null;
            this.branch.CustomEnabled = true;
            this.branch.DataFieldMapping = "";
            this.branch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.branch.GetRecordsOnUpDownKeys = false;
            this.branch.IsDate = false;
            this.branch.Location = new System.Drawing.Point(146, 115);
            this.branch.MaxLength = 3;
            this.branch.Name = "branch";
            this.branch.NumberFormat = "###,###,##0.00";
            this.branch.Postfix = "";
            this.branch.Prefix = "";
            this.branch.Size = new System.Drawing.Size(152, 20);
            this.branch.SkipValidation = false;
            this.branch.TabIndex = 2;
            this.branch.TextType = CrplControlLibrary.TextType.String;
            // 
            // month
            // 
            this.month.AllowSpace = true;
            this.month.AssociatedLookUpName = "";
            this.month.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.month.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.month.ContinuationTextBox = null;
            this.month.CustomEnabled = true;
            this.month.DataFieldMapping = "";
            this.month.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.month.GetRecordsOnUpDownKeys = false;
            this.month.IsDate = false;
            this.month.Location = new System.Drawing.Point(146, 141);
            this.month.MaxLength = 2;
            this.month.Name = "month";
            this.month.NumberFormat = "###,###,##0.00";
            this.month.Postfix = "";
            this.month.Prefix = "";
            this.month.Size = new System.Drawing.Size(153, 20);
            this.month.SkipValidation = false;
            this.month.TabIndex = 3;
            this.month.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // DesName
            // 
            this.DesName.AllowSpace = true;
            this.DesName.AssociatedLookUpName = "";
            this.DesName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DesName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.DesName.ContinuationTextBox = null;
            this.DesName.CustomEnabled = true;
            this.DesName.DataFieldMapping = "";
            this.DesName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DesName.GetRecordsOnUpDownKeys = false;
            this.DesName.IsDate = false;
            this.DesName.Location = new System.Drawing.Point(146, 89);
            this.DesName.MaxLength = 50;
            this.DesName.Name = "DesName";
            this.DesName.NumberFormat = "###,###,##0.00";
            this.DesName.Postfix = "";
            this.DesName.Prefix = "";
            this.DesName.Size = new System.Drawing.Size(152, 20);
            this.DesName.SkipValidation = false;
            this.DesName.TabIndex = 1;
            this.DesName.TextType = CrplControlLibrary.TextType.String;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(400, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 77;
            this.pictureBox2.TabStop = false;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "Printer",
            "File",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(146, 63);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(154, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // CHRIS_PayrollReports_PY009
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 329);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_PY009";
            this.Text = "CHRIS_PersonnelReport_SegmentWiseReport";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.PictureBox pictureBox2;
        private CrplControlLibrary.SLTextBox branch;
        private CrplControlLibrary.SLTextBox month;
        private CrplControlLibrary.SLTextBox DesName;
        private CrplControlLibrary.SLTextBox Year;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox seg;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
    }
}