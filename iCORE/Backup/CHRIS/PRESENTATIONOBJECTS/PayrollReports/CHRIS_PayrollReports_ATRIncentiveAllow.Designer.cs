namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_ATRIncentiveAllow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.p_emp_no = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.p_allow = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.p_branch = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.p_cat = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.p_emp_no);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.p_allow);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.p_branch);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.p_cat);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 288);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // p_emp_no
            // 
            this.p_emp_no.AllowSpace = true;
            this.p_emp_no.AssociatedLookUpName = "";
            this.p_emp_no.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p_emp_no.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.p_emp_no.ContinuationTextBox = null;
            this.p_emp_no.CustomEnabled = true;
            this.p_emp_no.DataFieldMapping = "";
            this.p_emp_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p_emp_no.GetRecordsOnUpDownKeys = false;
            this.p_emp_no.IsDate = false;
            this.p_emp_no.Location = new System.Drawing.Point(134, 200);
            this.p_emp_no.MaxLength = 6;
            this.p_emp_no.Name = "p_emp_no";
            this.p_emp_no.NumberFormat = "###,###,##0.00";
            this.p_emp_no.Postfix = "";
            this.p_emp_no.Prefix = "";
            this.p_emp_no.Size = new System.Drawing.Size(155, 20);
            this.p_emp_no.SkipValidation = false;
            this.p_emp_no.TabIndex = 24;
            this.p_emp_no.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(56, 200);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Emp No. :";
            // 
            // p_allow
            // 
            this.p_allow.AllowSpace = true;
            this.p_allow.AssociatedLookUpName = "";
            this.p_allow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p_allow.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.p_allow.ContinuationTextBox = null;
            this.p_allow.CustomEnabled = true;
            this.p_allow.DataFieldMapping = "";
            this.p_allow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p_allow.GetRecordsOnUpDownKeys = false;
            this.p_allow.IsDate = false;
            this.p_allow.Location = new System.Drawing.Point(134, 174);
            this.p_allow.MaxLength = 3;
            this.p_allow.Name = "p_allow";
            this.p_allow.NumberFormat = "###,###,##0.00";
            this.p_allow.Postfix = "";
            this.p_allow.Prefix = "";
            this.p_allow.Size = new System.Drawing.Size(155, 20);
            this.p_allow.SkipValidation = false;
            this.p_allow.TabIndex = 22;
            this.p_allow.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(56, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "All Code :";
            // 
            // p_branch
            // 
            this.p_branch.AllowSpace = true;
            this.p_branch.AssociatedLookUpName = "";
            this.p_branch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p_branch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.p_branch.ContinuationTextBox = null;
            this.p_branch.CustomEnabled = true;
            this.p_branch.DataFieldMapping = "";
            this.p_branch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p_branch.GetRecordsOnUpDownKeys = false;
            this.p_branch.IsDate = false;
            this.p_branch.Location = new System.Drawing.Point(134, 148);
            this.p_branch.MaxLength = 3;
            this.p_branch.Name = "p_branch";
            this.p_branch.NumberFormat = "###,###,##0.00";
            this.p_branch.Postfix = "";
            this.p_branch.Prefix = "";
            this.p_branch.Size = new System.Drawing.Size(155, 20);
            this.p_branch.SkipValidation = false;
            this.p_branch.TabIndex = 20;
            this.p_branch.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(56, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Branch :";
            // 
            // p_cat
            // 
            this.p_cat.AllowSpace = true;
            this.p_cat.AssociatedLookUpName = "";
            this.p_cat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p_cat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.p_cat.ContinuationTextBox = null;
            this.p_cat.CustomEnabled = true;
            this.p_cat.DataFieldMapping = "";
            this.p_cat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p_cat.GetRecordsOnUpDownKeys = false;
            this.p_cat.IsDate = false;
            this.p_cat.Location = new System.Drawing.Point(134, 122);
            this.p_cat.MaxLength = 1;
            this.p_cat.Name = "p_cat";
            this.p_cat.NumberFormat = "###,###,##0.00";
            this.p_cat.Postfix = "";
            this.p_cat.Prefix = "";
            this.p_cat.Size = new System.Drawing.Size(155, 20);
            this.p_cat.SkipValidation = false;
            this.p_cat.TabIndex = 18;
            this.p_cat.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(56, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Category :";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(226, 239);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(136, 239);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(98, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(191, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Enter Values For Parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(133, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Report Parameter";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::iCORE.Properties.Resources.Citi;
            this.pictureBox1.Location = new System.Drawing.Point(341, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "Printer",
            "File",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(134, 66);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(155, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 26;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(134, 93);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(155, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 27;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(56, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Desname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(56, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Destype";
            // 
            // CHRIS_PayrollReports_ATRIncentiveAllow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 339);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_ATRIncentiveAllow";
            this.Text = "CHRIS PayrollReports Monthly Tax Report";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private CrplControlLibrary.SLTextBox p_cat;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox p_allow;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox p_branch;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox p_emp_no;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}