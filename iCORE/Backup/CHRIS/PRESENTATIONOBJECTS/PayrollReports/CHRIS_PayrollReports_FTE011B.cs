using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_FTE011B : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_FTE011B()
        {
            InitializeComponent();
        }
        
        public CHRIS_PayrollReports_FTE011B(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            DataTable dt = new DataTable();
            dt.Columns.Add("Col1");
            dt.Columns.Add("Col2");

            dt.Rows.Add(new object[] {"GF", "GF" });
            dt.Rows.Add(new object[] {"GCB", "GCB" });

            this.SEG.DisplayMember = "Col1";
            this.SEG.ValueMember = "Col2";
            this.SEG.DataSource = dt;
            
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.MONTH.Text = "2";
            this.YEAR.Text = "2011";
            Dest_name.Text = "C:\\iCORE-Spool\\";
            
        }
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            //name.Text = "Working As Of Jun-30(Salaries On Old Scale As Of Mar Payroll And O.T Upto Feb-28 (paid in Mar Payroll)";
            //this.month.Text = 1;
            //this.year.Text = 2011;
            this.MONTH.Text = "1";
            this.YEAR.Text = "2011";
            this.SEG.Text = "GF";

        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            base.RptFileName = "FTE011B";
            //base.btnCallReport_Click(sender, e);
            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (Dest_Type.Text == "Printer")
            {
                base.PrintCustomReport();
            }
            else if (Dest_Type.Text == "File")
            {

                string DestName;
                string DestFormat;
                if (Dest_name.Text == string.Empty)
                {
                    DestName = @"C:\iCORE-Spool\Report";
                }
                else
                {
                    DestName = this.Dest_name.Text;
                }

                    base.ExportCustomReport(DestName, "pdf");

            }
            else if (Dest_Type.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                string DestFormat;
                string RecipentName;
                if (this.Dest_name.Text == string.Empty)
                {
                    RecipentName = "";
                }

                else
                {
                    RecipentName = this.Dest_name.Text;
                }

                    base.EmailToReport(DestName, "pdf", RecipentName);
            }

        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}