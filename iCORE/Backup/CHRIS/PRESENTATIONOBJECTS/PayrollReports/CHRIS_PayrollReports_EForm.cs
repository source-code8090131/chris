using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_EForm : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_EForm()
        {
            InitializeComponent();
        }

        public CHRIS_PayrollReports_EForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.copies.Text = "1";
        }



        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            //name.Text = "Working As Of Jun-30(Salaries On Old Scale As Of Mar Payroll And O.T Upto Feb-28 (paid in Mar Payroll)";
            rate.Text = "20.00";
            init_officer.Text = "TARIQ FEROZ";
            sen_officer.Text = "SHAUKAT TARIN";
           
        }
        private void button1_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PY013";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport(this.copies.Text  );
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;
                string DestFormat;
                DestName = @"C:\iCORE-Spool\Report";
                    base.ExportCustomReport(DestName, "PDF");

            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                string DestFormat;
                string RecipentName;
                RecipentName = "";

                    base.EmailToReport(DestName, "PDF", RecipentName);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

         

        
    }
}