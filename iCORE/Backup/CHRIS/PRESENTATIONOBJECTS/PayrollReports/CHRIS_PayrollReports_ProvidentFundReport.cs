using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_ProvidentFundReport : BaseRptForm
    {
        //private string DestFormat = "PDF";
        public CHRIS_PayrollReports_ProvidentFundReport()
        {
            InitializeComponent();
        }
        string DestFormat = "PDF";


        public CHRIS_PayrollReports_ProvidentFundReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            //Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.nocopies.Text = "1";
            //this.fillsegCombo();
            this.W_MONTH.Text = DateTime.Today.Month.ToString();
            this.W_MONTH.Text = "3";
            this.WYEAR.Text = "1994";
            //this.WYEAR.Text = DateTime.Today.Year.ToString();
            this.BRANCH.Text = "KHI";
            this.W_SEG.Text = "GF";
        }
        #region commented code
        //private void fillsegCombo()
        //{
        //    Result rsltCode;
        //    CmnDataManager cmnDataManager = new CmnDataManager();

        //    Dictionary<string, object> param = new Dictionary<string, object>();
        //    rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

        //    if (rsltCode.isSuccessful)
        //    {
        //        if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
        //        {
        //            this.W_SEG.DisplayMember = "pr_segment";
        //            this.W_SEG.ValueMember = "pr_segment";
        //            this.W_SEG.DataSource = rsltCode.dstResult.Tables[0];
        //        }

        //    }

        //}
        #endregion commented code

        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }

            no_of_copies = Convert.ToInt16(nocopies.Text);

            {
                base.RptFileName = "PY003";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    base.PrintNoofCopiesReport(no_of_copies);

                }


                if (Dest_Type.Text == "File")
                {
                    String d = "c:\\iCORE-Spool\\report";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;
                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }



            }


        }

  

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }
        #endregion 

    }
}

    
