using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_SegmentWiseReport : BaseRptForm
    {
        //private string Dest_Format = "PDF";
        public CHRIS_PayrollReports_SegmentWiseReport()
        {
            InitializeComponent();
        }


        public CHRIS_PayrollReports_SegmentWiseReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #region code by Irfan Farooqui
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.nocopies.Text = "1";
            this.Dest_Format.Text = "cond";
            this.MONTH.Text = DateTime.Today.Month.ToString();
            this.YEAR.Text = DateTime.Today.Year.ToString();
            this.FillBranch();
            this.fillsegCombo();
        }
        private void FillBranch()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount");

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
            {
                this.BRANCH.DisplayMember = "BRN_CODE";
                this.BRANCH.ValueMember = "BRN_CODE";
                this.BRANCH.DataSource = rsltCode.dstResult.Tables[0];
            }

        }
       private void fillsegCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.SEG.DisplayMember = "pr_segment";
                    this.SEG.ValueMember = "pr_segment";
                    this.SEG.DataSource = rsltCode.dstResult.Tables[0];
                    this.SEG.Text = "GCB";
                }

            }

        }


        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
           
            {
                base.RptFileName = "PY009A";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                   
                }
                if (Dest_Type.Text == "Printer")
                {

                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        
                        base.PrintNoofCopiesReport(no_of_copies);
                    }


                }



                if (Dest_Type.Text == "File")
                {
                    string d = "C:\\iCORE-Spool\\Report";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }

               



            }
        }

       
        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }


        #endregion 


    }
}


