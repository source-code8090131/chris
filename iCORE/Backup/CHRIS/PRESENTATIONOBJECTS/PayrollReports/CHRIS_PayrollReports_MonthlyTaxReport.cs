using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_MonthlyTaxReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_MonthlyTaxReport()
        {
            InitializeComponent();
        }
        
        public CHRIS_PayrollReports_MonthlyTaxReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count-1);
            this.copies.Text = "1";
            //this.mode.Items.RemoveAt(this.mode.Items.Count-1);
            this.Dest_Format.Text = "dflt";
        }


        private void button1_Click(object sender, EventArgs e)
        {
               


            {

                base.RptFileName = "MONTHLY_TAX";
                //base.btnCallReport_Click(sender, e);
                if (this.Dest_Format.Text == String.Empty)
                {
                    this.Dest_Format.Text = "PDF";
                }
                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (Dest_Type.Text == "Printer")
                {
                    base.PrintCustomReport(this.copies.Text );
                }
                else if (Dest_Type.Text == "File")
                {

                    string DestName;
                    string DestFormat;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }                

                    else
                    {
                        DestName = this.Dest_name.Text;
                    }

                        base.ExportCustomReport(DestName, "pdf");
                 
                }
                else if (Dest_Type.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";
                    string DestFormat;
                    string RecipentName;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_name.Text;
                    }

                    if (DestName != string.Empty)
                    {
                        base.EmailToReport(DestName, "pdf", RecipentName);
                       
                    }
                }


            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CHRIS_PayrollReports_MonthlyTaxReport_Load(object sender, EventArgs e)
        {
            DataTable dt  = new DataTable();
            DataTable dt1 = new DataTable();

            dt.Columns.Add("ValueMember");
            dt.Columns.Add("DisplayMember");

            dt.Rows.Add("Screen", "Screen");
            dt.Rows.Add("File", "File");
            dt.Rows.Add("Printer","Printer");
            dt.Rows.Add("Mail","Mail");
            dt.Rows.Add("Preview","Preview");

            dt1.Columns.Add("ValueMember");
            dt1.Columns.Add("DisplayMember");
            
            dt1.Rows.Add("Default","Default");
            dt1.Rows.Add("Bitmap","Bitmap");
            dt1.Rows.Add("Character","Character");


            Dest_Type.DisplayMember = "DisplayMember";
            Dest_Type.ValueMember = "ValueMember";
            Dest_Type.DataSource = dt;


            mode.DisplayMember = "DisplayMember";
            mode.ValueMember = "ValueMember";
            mode.DataSource = dt1;

        }

    }
}