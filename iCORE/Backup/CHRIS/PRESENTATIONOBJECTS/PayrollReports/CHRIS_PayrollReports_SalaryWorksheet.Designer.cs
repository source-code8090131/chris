namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_SalaryWorksheet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PayrollReports_SalaryWorksheet));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Btn_Run = new CrplControlLibrary.SLButton();
            this.Btn_Close = new CrplControlLibrary.SLButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.w_month = new CrplControlLibrary.SLTextBox(this.components);
            this.w_year = new CrplControlLibrary.SLTextBox(this.components);
            this.w_branch = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.Btn_Run);
            this.groupBox1.Controls.Add(this.Btn_Close);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.w_month);
            this.groupBox1.Controls.Add(this.w_year);
            this.groupBox1.Controls.Add(this.w_branch);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(12, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(401, 244);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(341, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 61);
            this.pictureBox1.TabIndex = 133;
            this.pictureBox1.TabStop = false;
            // 
            // Btn_Run
            // 
            this.Btn_Run.ActionType = "";
            this.Btn_Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Run.Location = new System.Drawing.Point(122, 205);
            this.Btn_Run.Name = "Btn_Run";
            this.Btn_Run.Size = new System.Drawing.Size(65, 23);
            this.Btn_Run.TabIndex = 6;
            this.Btn_Run.Text = "Run";
            this.Btn_Run.UseVisualStyleBackColor = true;
            this.Btn_Run.Click += new System.EventHandler(this.Btn_Run_Click);
            // 
            // Btn_Close
            // 
            this.Btn_Close.ActionType = "";
            this.Btn_Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Close.Location = new System.Drawing.Point(193, 205);
            this.Btn_Close.Name = "Btn_Close";
            this.Btn_Close.Size = new System.Drawing.Size(65, 23);
            this.Btn_Close.TabIndex = 7;
            this.Btn_Close.Text = "Close";
            this.Btn_Close.UseVisualStyleBackColor = true;
            this.Btn_Close.Click += new System.EventHandler(this.Btn_Close_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 181);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 96;
            this.label5.Text = "W Year";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(22, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 95;
            this.label4.Text = "W Month";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 94;
            this.label3.Text = "W Branch";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 93;
            this.label2.Text = "Desname";
            // 
            // w_month
            // 
            this.w_month.AllowSpace = true;
            this.w_month.AssociatedLookUpName = "";
            this.w_month.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_month.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_month.ContinuationTextBox = null;
            this.w_month.CustomEnabled = true;
            this.w_month.DataFieldMapping = "";
            this.w_month.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_month.GetRecordsOnUpDownKeys = false;
            this.w_month.IsDate = false;
            this.w_month.Location = new System.Drawing.Point(122, 153);
            this.w_month.MaxLength = 2;
            this.w_month.Name = "w_month";
            this.w_month.NumberFormat = "###,###,##0.00";
            this.w_month.Postfix = "";
            this.w_month.Prefix = "";
            this.w_month.Size = new System.Drawing.Size(182, 20);
            this.w_month.SkipValidation = false;
            this.w_month.TabIndex = 4;
            this.w_month.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // w_year
            // 
            this.w_year.AllowSpace = true;
            this.w_year.AssociatedLookUpName = "";
            this.w_year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_year.ContinuationTextBox = null;
            this.w_year.CustomEnabled = true;
            this.w_year.DataFieldMapping = "";
            this.w_year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_year.GetRecordsOnUpDownKeys = false;
            this.w_year.IsDate = false;
            this.w_year.Location = new System.Drawing.Point(122, 179);
            this.w_year.MaxLength = 4;
            this.w_year.Name = "w_year";
            this.w_year.NumberFormat = "###,###,##0.00";
            this.w_year.Postfix = "";
            this.w_year.Prefix = "";
            this.w_year.Size = new System.Drawing.Size(182, 20);
            this.w_year.SkipValidation = false;
            this.w_year.TabIndex = 5;
            this.w_year.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // w_branch
            // 
            this.w_branch.AllowSpace = true;
            this.w_branch.AssociatedLookUpName = "";
            this.w_branch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_branch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_branch.ContinuationTextBox = null;
            this.w_branch.CustomEnabled = true;
            this.w_branch.DataFieldMapping = "";
            this.w_branch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_branch.GetRecordsOnUpDownKeys = false;
            this.w_branch.IsDate = false;
            this.w_branch.Location = new System.Drawing.Point(122, 127);
            this.w_branch.MaxLength = 3;
            this.w_branch.Name = "w_branch";
            this.w_branch.NumberFormat = "###,###,##0.00";
            this.w_branch.Postfix = "";
            this.w_branch.Prefix = "";
            this.w_branch.Size = new System.Drawing.Size(182, 20);
            this.w_branch.SkipValidation = false;
            this.w_branch.TabIndex = 3;
            this.w_branch.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(122, 101);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(182, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 2;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 88;
            this.label1.Text = "Destype";
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(122, 74);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(182, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(96, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(208, 15);
            this.label10.TabIndex = 86;
            this.label10.Text = "Enter values for the parameters";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(137, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 15);
            this.label9.TabIndex = 85;
            this.label9.Text = "Report Parameters";
            // 
            // CHRIS_PayrollReports_SalaryWorksheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 300);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_SalaryWorksheet";
            this.Text = "CHRIS_PayrollReports_SalaryWorksheet";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox w_month;
        private CrplControlLibrary.SLTextBox w_year;
        private CrplControlLibrary.SLTextBox w_branch;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLButton Btn_Run;
        private CrplControlLibrary.SLButton Btn_Close;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}