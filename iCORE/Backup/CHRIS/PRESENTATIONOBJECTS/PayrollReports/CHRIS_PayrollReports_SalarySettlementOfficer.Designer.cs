namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_SalarySettlementOfficer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.w_dept = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.tpno = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.fpno = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.month = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.cat = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.mode = new CrplControlLibrary.SLComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.w_seg = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.w_branch = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.wyear = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pBar = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.w_dept);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.tpno);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.fpno);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.month);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cat);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.mode);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.w_seg);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.w_branch);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.wyear);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.Dest_Format);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(493, 452);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // w_dept
            // 
            this.w_dept.AllowSpace = true;
            this.w_dept.AssociatedLookUpName = "";
            this.w_dept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_dept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_dept.ContinuationTextBox = null;
            this.w_dept.CustomEnabled = true;
            this.w_dept.DataFieldMapping = "";
            this.w_dept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_dept.GetRecordsOnUpDownKeys = false;
            this.w_dept.IsDate = false;
            this.w_dept.Location = new System.Drawing.Point(162, 254);
            this.w_dept.MaxLength = 5;
            this.w_dept.Name = "w_dept";
            this.w_dept.NumberFormat = "###,###,##0.00";
            this.w_dept.Postfix = "";
            this.w_dept.Prefix = "";
            this.w_dept.Size = new System.Drawing.Size(175, 20);
            this.w_dept.SkipValidation = false;
            this.w_dept.TabIndex = 8;
            this.w_dept.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(15, 256);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(138, 13);
            this.label15.TabIndex = 33;
            this.label15.Text = "Enter Valid Department";
            // 
            // tpno
            // 
            this.tpno.AllowSpace = true;
            this.tpno.AssociatedLookUpName = "";
            this.tpno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tpno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tpno.ContinuationTextBox = null;
            this.tpno.CustomEnabled = true;
            this.tpno.DataFieldMapping = "";
            this.tpno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpno.GetRecordsOnUpDownKeys = false;
            this.tpno.IsDate = false;
            this.tpno.Location = new System.Drawing.Point(162, 384);
            this.tpno.MaxLength = 6;
            this.tpno.Name = "tpno";
            this.tpno.NumberFormat = "###,###,##0.00";
            this.tpno.Postfix = "";
            this.tpno.Prefix = "";
            this.tpno.Size = new System.Drawing.Size(175, 20);
            this.tpno.SkipValidation = false;
            this.tpno.TabIndex = 13;
            this.tpno.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(15, 386);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(96, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Enter To P. No.";
            // 
            // fpno
            // 
            this.fpno.AllowSpace = true;
            this.fpno.AssociatedLookUpName = "";
            this.fpno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fpno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.fpno.ContinuationTextBox = null;
            this.fpno.CustomEnabled = true;
            this.fpno.DataFieldMapping = "";
            this.fpno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fpno.GetRecordsOnUpDownKeys = false;
            this.fpno.IsDate = false;
            this.fpno.Location = new System.Drawing.Point(162, 358);
            this.fpno.MaxLength = 6;
            this.fpno.Name = "fpno";
            this.fpno.NumberFormat = "###,###,##0.00";
            this.fpno.Postfix = "";
            this.fpno.Prefix = "";
            this.fpno.Size = new System.Drawing.Size(175, 20);
            this.fpno.SkipValidation = false;
            this.fpno.TabIndex = 12;
            this.fpno.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(15, 360);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Enter From P. No.";
            // 
            // month
            // 
            this.month.AllowSpace = true;
            this.month.AssociatedLookUpName = "";
            this.month.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.month.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.month.ContinuationTextBox = null;
            this.month.CustomEnabled = true;
            this.month.DataFieldMapping = "";
            this.month.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.month.GetRecordsOnUpDownKeys = false;
            this.month.IsDate = false;
            this.month.Location = new System.Drawing.Point(162, 306);
            this.month.MaxLength = 2;
            this.month.Name = "month";
            this.month.NumberFormat = "###,###,##0.00";
            this.month.Postfix = "";
            this.month.Prefix = "";
            this.month.Size = new System.Drawing.Size(175, 20);
            this.month.SkipValidation = false;
            this.month.TabIndex = 10;
            this.month.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 308);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Enter Month";
            // 
            // cat
            // 
            this.cat.AllowSpace = true;
            this.cat.AssociatedLookUpName = "";
            this.cat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cat.ContinuationTextBox = null;
            this.cat.CustomEnabled = true;
            this.cat.DataFieldMapping = "";
            this.cat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cat.GetRecordsOnUpDownKeys = false;
            this.cat.IsDate = false;
            this.cat.Location = new System.Drawing.Point(162, 280);
            this.cat.MaxLength = 3;
            this.cat.Name = "cat";
            this.cat.NumberFormat = "###,###,##0.00";
            this.cat.Postfix = "";
            this.cat.Prefix = "";
            this.cat.Size = new System.Drawing.Size(175, 20);
            this.cat.SkipValidation = false;
            this.cat.TabIndex = 9;
            this.cat.TextType = CrplControlLibrary.TextType.String;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 282);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(142, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Enter Category or NULL";
            // 
            // mode
            // 
            this.mode.BusinessEntity = "";
            this.mode.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.mode.CustomEnabled = true;
            this.mode.DataFieldMapping = "";
            this.mode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mode.FormattingEnabled = true;
            this.mode.Items.AddRange(new object[] {
            "Default",
            "Bitmap",
            "Character"});
            this.mode.Location = new System.Drawing.Point(162, 201);
            this.mode.LOVType = "";
            this.mode.Name = "mode";
            this.mode.Size = new System.Drawing.Size(175, 21);
            this.mode.SPName = "";
            this.mode.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 204);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Mode";
            // 
            // w_seg
            // 
            this.w_seg.AllowSpace = true;
            this.w_seg.AssociatedLookUpName = "";
            this.w_seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_seg.ContinuationTextBox = null;
            this.w_seg.CustomEnabled = true;
            this.w_seg.DataFieldMapping = "";
            this.w_seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_seg.GetRecordsOnUpDownKeys = false;
            this.w_seg.IsDate = false;
            this.w_seg.Location = new System.Drawing.Point(162, 228);
            this.w_seg.MaxLength = 3;
            this.w_seg.Name = "w_seg";
            this.w_seg.NumberFormat = "###,###,##0.00";
            this.w_seg.Postfix = "";
            this.w_seg.Prefix = "";
            this.w_seg.Size = new System.Drawing.Size(175, 20);
            this.w_seg.SkipValidation = false;
            this.w_seg.TabIndex = 7;
            this.w_seg.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Enter Valid Segment";
            // 
            // w_branch
            // 
            this.w_branch.AllowSpace = true;
            this.w_branch.AssociatedLookUpName = "";
            this.w_branch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_branch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_branch.ContinuationTextBox = null;
            this.w_branch.CustomEnabled = true;
            this.w_branch.DataFieldMapping = "";
            this.w_branch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_branch.GetRecordsOnUpDownKeys = false;
            this.w_branch.IsDate = false;
            this.w_branch.Location = new System.Drawing.Point(162, 173);
            this.w_branch.MaxLength = 3;
            this.w_branch.Name = "w_branch";
            this.w_branch.NumberFormat = "###,###,##0.00";
            this.w_branch.Postfix = "";
            this.w_branch.Prefix = "";
            this.w_branch.Size = new System.Drawing.Size(175, 20);
            this.w_branch.SkipValidation = false;
            this.w_branch.TabIndex = 5;
            this.w_branch.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Enter Valid Branch";
            // 
            // wyear
            // 
            this.wyear.AllowSpace = true;
            this.wyear.AssociatedLookUpName = "";
            this.wyear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wyear.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wyear.ContinuationTextBox = null;
            this.wyear.CustomEnabled = true;
            this.wyear.DataFieldMapping = "";
            this.wyear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wyear.GetRecordsOnUpDownKeys = false;
            this.wyear.IsDate = false;
            this.wyear.Location = new System.Drawing.Point(162, 332);
            this.wyear.MaxLength = 4;
            this.wyear.Name = "wyear";
            this.wyear.NumberFormat = "###,###,##0.00";
            this.wyear.Postfix = "";
            this.wyear.Prefix = "";
            this.wyear.Size = new System.Drawing.Size(175, 20);
            this.wyear.SkipValidation = false;
            this.wyear.TabIndex = 11;
            this.wyear.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 334);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Enter Year";
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "Printer",
            "File",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(162, 68);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(175, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(227, 410);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(162, 147);
            this.copies.MaxLength = 2;
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(175, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 4;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(160, 410);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Dest_Format
            // 
            this.Dest_Format.AllowSpace = true;
            this.Dest_Format.AssociatedLookUpName = "";
            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Format.ContinuationTextBox = null;
            this.Dest_Format.CustomEnabled = true;
            this.Dest_Format.DataFieldMapping = "";
            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Format.GetRecordsOnUpDownKeys = false;
            this.Dest_Format.IsDate = false;
            this.Dest_Format.Location = new System.Drawing.Point(162, 122);
            this.Dest_Format.MaxLength = 50;
            this.Dest_Format.Name = "Dest_Format";
            this.Dest_Format.NumberFormat = "###,###,##0.00";
            this.Dest_Format.Postfix = "";
            this.Dest_Format.Prefix = "";
            this.Dest_Format.Size = new System.Drawing.Size(175, 20);
            this.Dest_Format.SkipValidation = false;
            this.Dest_Format.TabIndex = 3;
            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(162, 95);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(175, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 2;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(152, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(185, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Enter values for the parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(178, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Report Parameters";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Copies";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Desformat";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Desname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Destype";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::iCORE.Properties.Resources.Citi;
            this.pictureBox1.Location = new System.Drawing.Point(456, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(61, 64);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // pBar
            // 
            this.pBar.Location = new System.Drawing.Point(12, 497);
            this.pBar.Name = "pBar";
            this.pBar.Size = new System.Drawing.Size(493, 23);
            this.pBar.TabIndex = 34;
            this.pBar.Visible = false;
            // CHRIS_PayrollReports_SalarySettlementOfficer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 524);
            this.Controls.Add(this.pBar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_SalarySettlementOfficer";
            this.Text = "CHRIS PayrollReports Monthly Tax Report";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.pBar, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox copies;
        private CrplControlLibrary.SLTextBox Dest_Format;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLTextBox wyear;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox w_seg;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox w_branch;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox tpno;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox fpno;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox month;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox cat;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLComboBox mode;
        private CrplControlLibrary.SLTextBox w_dept;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ProgressBar pBar;
    }
}