using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_IncomeTaxReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
       // private string DestFormat = "PDF";
        public CHRIS_PayrollReports_IncomeTaxReport()
        {
            InitializeComponent();
        }
        string DestFormat = "PDF";

        public CHRIS_PayrollReports_IncomeTaxReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.nocopies.Text = "1";
            this.W_MONTH.Text = "3";
            this.YEAR.Text = "1994";
            this.w_city.Text = "KHI";
            this.W_SEG.Text = "GF";
            Dest_name.Text = "C:\\iCORE-Spool\\";
            //this.W_MONTH.Text = DateTime.Today.Month.ToString();
            //this.YEAR.Text = DateTime.Today.Year.ToString();
            //this.FillBranch();
            //this.fillsegCombo();
        }

    


        private void Run_Click_1(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }

            no_of_copies = Convert.ToInt16(nocopies.Text);


            {
                base.RptFileName = "PY005";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {
                    base.PrintNoofCopiesReport(no_of_copies);
                }

                if (Dest_Type.Text == "File")
                {
                    String d = "c:\\iCORE-Spool\\report";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;
                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }



                #region commented code

                //if (Dest_Type.Text == "Mail")
                //{
                //    if (DestFormat != string.Empty)
                //    {
                //        base.EmailToReport("C:\\Report", DestFormat);

                //    }


                //}
                //int no_of_copies;


                //{
                //    base.RptFileName = "PY005";


                //    if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                //    {

                //        base.btnCallReport_Click(sender, e);

                //    }
                //    if (nocopies.Text != string.Empty)
                //    {
                //        no_of_copies = Convert.ToInt16(nocopies.Text);

                //        base.PrintNoofCopiesReport(no_of_copies);
                //    }

                //    //if (Dest_Type.Text == "Mail")
                //    //{
                //    //    if (DestFormat != string.Empty)
                //    //    {
                //    //        base.EmailToReport("C:\\Report", DestFormat);

                //    //    }


                //    //}


                //    //if (Dest_Type.Text == "File")
                //    //{
                //    //    if (DestFormat != string.Empty && Dest_name.Text != string.Empty)
                //    //    {
                //    //        base.ExportCustomReport(Dest_name.Text, DestFormat);
                //    //    }



                //    //}



                //    if (Dest_Type.Text == "Mail")
                //    {

                //        if (DestFormat != string.Empty || Dest_name.Text != string.Empty)
                //        {
                //            base.EmailToReport(@"C:\Report", "PDF");
                //        }
                //    }


                //}
                #endregion commented code

            }

        }

       

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }
        #region commented code

        //private void FillBranch()
        //{
        //    Result rsltCode;
        //    CmnDataManager cmnDM = new CmnDataManager();
        //    rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount");

        //    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
        //    {
        //        this.w_city.DisplayMember = "BRN_CODE";
        //        this.w_city.ValueMember = "BRN_CODE";
        //        this.w_city.DataSource = rsltCode.dstResult.Tables[0];
        //    }

        //}
        //private void fillsegCombo()
        //{
        //    Result rsltCode;
        //    CmnDataManager cmnDataManager = new CmnDataManager();

        //    Dictionary<string, object> param = new Dictionary<string, object>();
        //    rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

        //    if (rsltCode.isSuccessful)
        //    {
        //        if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
        //        {
        //            this.W_SEG.DisplayMember = "pr_segment";
        //            this.W_SEG.ValueMember = "pr_segment";
        //            this.W_SEG.DataSource = rsltCode.dstResult.Tables[0];
        //        }

        //    }

        //}
#endregion commented code

       

        
    }
}


