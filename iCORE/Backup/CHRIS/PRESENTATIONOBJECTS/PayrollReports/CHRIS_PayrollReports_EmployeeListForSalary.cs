using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_EmployeeListForSalary : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_EmployeeListForSalary()
        {
            InitializeComponent();
        }

        public CHRIS_PayrollReports_EmployeeListForSalary(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();

            /* Set values for controls here */
            this.cmbDescType.Items.RemoveAt(5);

            Dest_name.Text = "C:\\iCORE-Spool\\";
        }


        private void btnRun_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PRREP53";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Mail")
            {
                base.EmailToReport("c:\\iCORE-Spool\\PRREP53", "PDF");
            }
            else if (cmbDescType.Text == "File")
            {
                base.ExportCustomReport("c:\\iCORE-Spool\\PRREP53", "PDF");
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport();
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}