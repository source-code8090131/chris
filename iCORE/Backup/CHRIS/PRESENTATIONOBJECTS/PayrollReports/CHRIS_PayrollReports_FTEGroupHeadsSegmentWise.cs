using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_FTEGroupHeadsSegmentWise : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_FTEGroupHeadsSegmentWise()
        {
            InitializeComponent();
        }
        string DestFormat; 
        public CHRIS_PayrollReports_FTEGroupHeadsSegmentWise(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.copies.Text = "1";
            this.w_brn.Text = "KHI";
            this.w_seg.Text = "GCB";
            this.Dest_Format.Text = "wide";
            //this.W_DATE.Value = new DateTime(1995, 1, 7);
            Dest_name.Text = "C:\\iCORE-Spool\\";

        }


        private void button1_Click(object sender, EventArgs e)
        {
               


            {

                base.RptFileName = "FTER08";
                //base.btnCallReport_Click(sender, e);
                if (this.Dest_Format.Text == String.Empty)
                {
                    DestFormat = "PDF";
                }
                else
                    DestFormat = this.Dest_Format.Text;
                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (Dest_Type.Text == "Printer")
                {
                    base.PrintCustomReport(this.copies.Text );
                }
                else if (Dest_Type.Text == "File")
                {

                    string DestName;

                    if (this.Dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }                

                    else
                    {
                        DestName = this.Dest_name.Text;
                    }

                        base.ExportCustomReport(DestName, "pdf");

                }
                else if (Dest_Type.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";
                    
                    string RecipentName;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_name.Text;
                    }

                    base.EmailToReport(DestName, "pdf", RecipentName);
                       
                }


            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       

    }
}