using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
//namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReport
{
    public partial class CHRIS_PayrollReports_MonthlyTaxPaymentReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_MonthlyTaxPaymentReport()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";

        public CHRIS_PayrollReports_MonthlyTaxPaymentReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            MNTH.Text = "07";
            YEAR.Text = "2004";
            W_BRN.Text= "FBD";
            Dest_Type.Items.RemoveAt(5);
            Dest_name.Text = "C:\\iCORE-Spool\\";
        }


        private void CHRIS_PayrollReport_MonthlyTaxPaymentReport_Load(object sender, EventArgs e)
        {

        }


        private void RUN_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PY024";


            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {

                base.btnCallReport_Click(sender, e);

            }
            if (Dest_Type.Text == "Printer")
            {
                PrintCustomReport();
            }


            if (Dest_Type.Text == "File")
            {
                String d = "c:\\iCORE-Spool\\report";
                if (this.Dest_name.Text != string.Empty)
                    d = this.Dest_name.Text;

                base.ExportCustomReport(d, "pdf");


            }


            if (Dest_Type.Text == "Mail")
            {
                String d = "";
                if (this.Dest_name.Text != string.Empty)
                    d = this.Dest_name.Text;
                base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

            }




       
        }

        private void CLOSE_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }
    }
}