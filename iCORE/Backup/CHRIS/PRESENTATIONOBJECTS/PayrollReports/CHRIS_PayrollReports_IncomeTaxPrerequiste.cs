
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_IncomeTaxPrerequiste : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_IncomeTaxPrerequiste()
        {
            InitializeComponent();
        }
        string DestFormat;
        public CHRIS_PayrollReports_IncomeTaxPrerequiste(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.copies.Text = "1";
            this.Dest_Format.Text = "cond";
           this.conv_add.Text = "40000";
            this.wyear.Text = "1997-1998";
            this.sdate.Value = new DateTime(1997, 1, 1);
            this.edate.Value = new DateTime(1997, 12, 31);
        }


        private void button1_Click(object sender, EventArgs e)
        {
               


            {

                base.RptFileName = "py044";
                //base.btnCallReport_Click(sender, e);
                if (this.Dest_Format.Text == String.Empty)
                {
                    DestFormat = "PDF";
                }
                else
                    DestFormat = this.Dest_Format.Text; 
                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (Dest_Type.Text == "Printer")
                {
                    base.PrintCustomReport(this.copies.Text );
                }
                else if (Dest_Type.Text == "File")
                {

                    string DestName;

                    if (this.Dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }                

                    else
                    {
                        DestName = this.Dest_name.Text;
                    }

                        base.ExportCustomReport(DestName, "pdf" );

                }
                else if (Dest_Type.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";
                    
                    string RecipentName;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_name.Text;
                    }

                        base.EmailToReport(DestName, "pdf", RecipentName);
                       

                }


            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       

    }
}//=======
//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;
//using System.Windows.Forms;

//namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
//{
//    public partial class CHRIS_PayrollReports_FTEDesignationGroup : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
//    {
//        public CHRIS_PayrollReports_FTEDesignationGroup()
//        {
//            InitializeComponent();
//        }
        
//        public CHRIS_PayrollReports_FTEDesignationGroup(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
//            : base(mainmenu, connbean_obj)
//        {
//            InitializeComponent();
//        }
//        protected override void CommonOnLoadMethods()
//        {
//            base.CommonOnLoadMethods();
//            this.w_brn.Text = "KHI";
//            this.w_grp.Text = "BB";
//            this.w_seg.Text = "GCB";
//        }

//        protected override void OnLoad(EventArgs e)
//        {
//            base.OnLoad(e);

//            this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
//            this.copies.Text = "1";
//        }



//        private void button1_Click(object sender, EventArgs e)
//        {
               


//            {

//                base.RptFileName = "FTER02";
//                //base.btnCallReport_Click(sender, e);
//                if (this.Dest_Format.Text == String.Empty)
//                {
//                    this.Dest_Format.Text = "PDF";
//                }
//                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
//                {
//                    base.btnCallReport_Click(sender, e);
//                }
//                else if (Dest_Type.Text == "Printer")
//                {
//                    base.PrintCustomReport();
//                }
//                else if (Dest_Type.Text == "File")
//                {

//                    string DestName;
//                    string DestFormat;
//                    if (Dest_Type.Text == string.Empty)
//                    {
//                        DestName = @"C:\Report";
//                    }                

//                    else
//                    {
//                        DestName = this.Dest_name.Text;
//                    }

//                    if (DestName != string.Empty)
//                    {
//                        base.ExportCustomReport(DestName, this.Dest_Format.Text);
//                    }
//                }
//                else if (Dest_Type.Text == "Mail")
//                {
//                    string DestName = @"C:\Report";
//                    string DestFormat;
//                    string RecipentName;
//                    if (this.Dest_name.Text == string.Empty)
//                    {
//                        RecipentName = "";
//                    }

//                    else
//                    {
//                        RecipentName = this.Dest_name.Text;
//                    }

//                    if (DestName != string.Empty)
//                    {
//                        base.EmailToReport(DestName, this.Dest_Format.Text, RecipentName);
                       
//                    }
//                }


//            }


//        }

//        private void button2_Click(object sender, EventArgs e)
//        {
//            this.Close();
//        }

       

//    }
//}
