

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PremInfoCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PREMIUM_MASTER"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	04/16/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PremInfoCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_PRE_POLICY;
        private DateTime m_PRE_PAYMENT_DATE;
        private decimal m_PRE_PREMIUM_AMT;
        private String m_PRE_BILL_NO;
        private String m_PRE_MC_NO;
        private String m_PRE_SUBJECT;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PRE_POLICY"
        public String PRE_POLICY
        {
            get { return m_PRE_POLICY; }
            set { m_PRE_POLICY = value; }
        }
        #endregion

        #region "PRE_PAYMENT_DATE"
        public DateTime PRE_PAYMENT_DATE
        {
            get { return m_PRE_PAYMENT_DATE; }
            set { m_PRE_PAYMENT_DATE = value; }
        }
        #endregion

        #region "PRE_PREMIUM_AMT"
        public decimal PRE_PREMIUM_AMT
        {
            get { return m_PRE_PREMIUM_AMT; }
            set { m_PRE_PREMIUM_AMT = value; }
        }
        #endregion

        #region "PRE_BILL_NO"
        public String PRE_BILL_NO
        {
            get { return m_PRE_BILL_NO; }
            set { m_PRE_BILL_NO = value; }
        }
        #endregion

        #region "PRE_MC_NO"
        public String PRE_MC_NO
        {
            get { return m_PRE_MC_NO; }
            set { m_PRE_MC_NO = value; }
        }
        #endregion

        #region "PRE_SUBJECT"
        public String PRE_SUBJECT
        {
            get { return m_PRE_SUBJECT; }
            set { m_PRE_SUBJECT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PRE_POLICY = "PRE_POLICY";
        public static readonly string _PRE_PAYMENT_DATE = "PRE_PAYMENT_DATE";
        public static readonly string _PRE_PREMIUM_AMT = "PRE_PREMIUM_AMT";
        public static readonly string _PRE_BILL_NO = "PRE_BILL_NO";
        public static readonly string _PRE_MC_NO = "PRE_MC_NO";
        public static readonly string _PRE_SUBJECT = "PRE_SUBJECT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
