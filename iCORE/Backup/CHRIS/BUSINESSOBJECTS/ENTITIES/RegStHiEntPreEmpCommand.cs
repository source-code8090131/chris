
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntPreEmpCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PRE_EMP"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[UMAIR MUFTI]	01/20/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class RegStHiEntPreEmpCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private DateTime m_PR_JOB_FROM;
        private DateTime m_PR_JOB_TO;
        private String m_PR_ORGANIZ;
        private String m_PR_DESIG_PR;
        private String m_PR_REMARKS_PR;
        private String m_PR_ADD1;
        private String m_PR_ADD2;
        private String m_PR_ADD3;
        private String m_PR_LET_SNT;
        private String m_PR_ANS_REC;
        private String m_PR_FLAG;
        private String m_PR_ADD4;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "PR_JOB_FROM"
        public DateTime PR_JOB_FROM
        {
            get { return m_PR_JOB_FROM; }
            set { m_PR_JOB_FROM = value; }
        }
        #endregion

        #region "PR_JOB_TO"
        public DateTime PR_JOB_TO
        {
            get { return m_PR_JOB_TO; }
            set { m_PR_JOB_TO = value; }
        }
        #endregion

        #region "PR_ORGANIZ"
        public String PR_ORGANIZ
        {
            get { return m_PR_ORGANIZ; }
            set { m_PR_ORGANIZ = value; }
        }
        #endregion

        #region "PR_DESIG_PR"
        public String PR_DESIG_PR
        {
            get { return m_PR_DESIG_PR; }
            set { m_PR_DESIG_PR = value; }
        }
        #endregion

        #region "PR_REMARKS_PR"
        public String PR_REMARKS_PR
        {
            get { return m_PR_REMARKS_PR; }
            set { m_PR_REMARKS_PR = value; }
        }
        #endregion

        #region "PR_ADD1"
        public String PR_ADD1
        {
            get { return m_PR_ADD1; }
            set { m_PR_ADD1 = value; }
        }
        #endregion

        #region "PR_ADD2"
        public String PR_ADD2
        {
            get { return m_PR_ADD2; }
            set { m_PR_ADD2 = value; }
        }
        #endregion

        #region "PR_ADD3"
        public String PR_ADD3
        {
            get { return m_PR_ADD3; }
            set { m_PR_ADD3 = value; }
        }
        #endregion

        #region "PR_LET_SNT"
        public String PR_LET_SNT
        {
            get { return m_PR_LET_SNT; }
            set { m_PR_LET_SNT = value; }
        }
        #endregion

        #region "PR_ANS_REC"
        public String PR_ANS_REC
        {
            get { return m_PR_ANS_REC; }
            set { m_PR_ANS_REC = value; }
        }
        #endregion

        #region "PR_FLAG"
        public String PR_FLAG
        {
            get { return m_PR_FLAG; }
            set { m_PR_FLAG = value; }
        }
        #endregion

        #region "PR_ADD4"
        public String PR_ADD4
        {
            get { return m_PR_ADD4; }
            set { m_PR_ADD4 = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _PR_JOB_FROM = "PR_JOB_FROM";
        public static readonly string _PR_JOB_TO = "PR_JOB_TO";
        public static readonly string _PR_ORGANIZ = "PR_ORGANIZ";
        public static readonly string _PR_DESIG_PR = "PR_DESIG_PR";
        public static readonly string _PR_REMARKS_PR = "PR_REMARKS_PR";
        public static readonly string _PR_ADD1 = "PR_ADD1";
        public static readonly string _PR_ADD2 = "PR_ADD2";
        public static readonly string _PR_ADD3 = "PR_ADD3";
        public static readonly string _PR_LET_SNT = "PR_LET_SNT";
        public static readonly string _PR_ANS_REC = "PR_ANS_REC";
        public static readonly string _PR_FLAG = "PR_FLAG";
        public static readonly string _PR_ADD4 = "PR_ADD4";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
