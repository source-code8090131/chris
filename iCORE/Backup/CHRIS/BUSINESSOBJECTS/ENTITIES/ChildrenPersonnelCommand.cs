

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CHILDRENCOMMAND
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "CHILDREN"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[AHAD ZUBAIR]	01/27/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class ChildrenPersonnelCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private String m_PR_CHILD_NAME;
        private DateTime m_PR_DATE_BIRTH;
        private String m_PR_CH_SEX;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "PR_CHILD_NAME"
        public String PR_CHILD_NAME
        {
            get { return m_PR_CHILD_NAME; }
            set { m_PR_CHILD_NAME = value; }
        }
        #endregion

        #region "PR_DATE_BIRTH"
        public DateTime PR_DATE_BIRTH
        {
            get { return m_PR_DATE_BIRTH; }
            set { m_PR_DATE_BIRTH = value; }
        }
        #endregion

        #region "PR_CH_SEX"
        public String PR_CH_SEX
        {
            get { return m_PR_CH_SEX; }
            set { m_PR_CH_SEX = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_C_NO";
        public static readonly string _PR_CHILD_NAME = "PR_CHILD_NAME";
        public static readonly string _PR_DATE_BIRTH = "PR_DATE_BIRTH";
        public static readonly string _PR_CH_SEX = "PR_CH_SEX";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
