

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFUND_DETAILCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PFUND_DETAIL"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	01/28/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PFUND_DETAILCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_PFD_PR_P_NO;
        private DateTime m_PFD_DATE;
        private decimal m_PFD_PFUND;
        private decimal m_PFD_PF_AREAR;
        private String m_PFD_FLAG;
        private String m_dummy_year;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PFD_PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public decimal PFD_PR_P_NO
        {
            get { return m_PFD_PR_P_NO; }
            set { m_PFD_PR_P_NO = value; }
        }
        #endregion

        #region "PFD_DATE"
        public DateTime PFD_DATE
        {
            get { return m_PFD_DATE; }
            set { m_PFD_DATE = value; }
        }
        #endregion

        #region "PFD_PFUND"
        public decimal PFD_PFUND
        {
            get { return m_PFD_PFUND; }
            set { m_PFD_PFUND = value; }
        }
        #endregion

        #region "PFD_PF_AREAR"
        public decimal PFD_PF_AREAR
        {
            get { return m_PFD_PF_AREAR; }
            set { m_PFD_PF_AREAR = value; }
        }
        #endregion

        #region "PFD_FLAG"
        public String PFD_FLAG
        {
            get { return m_PFD_FLAG; }
            set { m_PFD_FLAG = value; }
        }
        #endregion


        #region "dummy_year"
        [CustomAttributes(IsForeignKey = true)]
        public String dummy_year
        {
            get { return m_dummy_year; }
            set { m_dummy_year = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PFD_PR_P_NO = "PFD_PR_P_NO";
        public static readonly string _PFD_DATE = "PFD_DATE";
        public static readonly string _PFD_PFUND = "PFD_PFUND";
        public static readonly string _PFD_PF_AREAR = "PFD_PF_AREAR";
        public static readonly string _PFD_FLAG = "PFD_FLAG";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
