
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RANKCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "RANK"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	12/10/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class RANKCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private Double m_R_QUARTER;
        private String m_R_RANK;
        private Double m_YEAR;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "R_QUARTER"
        public Double R_QUARTER
        {
            get { return m_R_QUARTER; }
            set { m_R_QUARTER = value; }
        }
        #endregion

        #region "R_RANK"
        public String R_RANK
        {
            get { return m_R_RANK; }
            set { m_R_RANK = value; }
        }
        #endregion

        #region "YEAR"
        public Double YEAR
        {
            get { return m_YEAR; }
            set { m_YEAR = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _R_QUARTER = "R_QUARTER";
        public static readonly string _R_RANK = "R_RANK";
        public static readonly string _YEAR = "YEAR";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
