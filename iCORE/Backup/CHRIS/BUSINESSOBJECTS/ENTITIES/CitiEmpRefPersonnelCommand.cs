

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CITIEMPREFPERSONNELCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "CITI_EMP_REF"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[AHAD ZUBAIR]	01/28/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class CitiEmpRefPersonnelCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_CR_FTE_CONT;
        private Nullable <decimal> m_CR_PR_NO;
        private String m_CR_PR_RELATION;
        private String m_CR_CONT_NAME;
        private String m_CR_GRP;
        private String m_CR_DESIG;
        private String m_CR_DEPT;
        private String m_CR_OVS_LCL;
        private String m_CR_BRANCH;
        private String m_CR_COUNTRY;
        private String m_CR_CITY;
        private Double m_PR_P_NO;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "CR_FTE_CONT"
        public String CR_FTE_CONT
        {
            get { return m_CR_FTE_CONT; }
            set { m_CR_FTE_CONT = value; }
        }
        #endregion

        #region "CR_PR_NO"
        public Nullable<decimal> CR_PR_NO
        {
            get { return m_CR_PR_NO; }
            set { m_CR_PR_NO = value; }
        }
        #endregion

        #region "CR_PR_RELATION"
        public String CR_PR_RELATION
        {
            get { return m_CR_PR_RELATION; }
            set { m_CR_PR_RELATION = value; }
        }
        #endregion

        #region "CR_CONT_NAME"
        public String CR_CONT_NAME
        {
            get { return m_CR_CONT_NAME; }
            set { m_CR_CONT_NAME = value; }
        }
        #endregion

        #region "CR_GRP"
        public String CR_GRP
        {
            get { return m_CR_GRP; }
            set { m_CR_GRP = value; }
        }
        #endregion

        #region "CR_DESIG"
        public String CR_DESIG
        {
            get { return m_CR_DESIG; }
            set { m_CR_DESIG = value; }
        }
        #endregion

        #region "CR_DEPT"
        public String CR_DEPT
        {
            get { return m_CR_DEPT; }
            set { m_CR_DEPT = value; }
        }
        #endregion

        #region "CR_OVS_LCL"
        public String CR_OVS_LCL
        {
            get { return m_CR_OVS_LCL; }
            set { m_CR_OVS_LCL = value; }
        }
        #endregion

        #region "CR_BRANCH"
        public String CR_BRANCH
        {
            get { return m_CR_BRANCH; }
            set { m_CR_BRANCH = value; }
        }
        #endregion

        #region "CR_COUNTRY"
        public String CR_COUNTRY
        {
            get { return m_CR_COUNTRY; }
            set { m_CR_COUNTRY = value; }
        }
        #endregion

        #region "CR_CITY"
        public String CR_CITY
        {
            get { return m_CR_CITY; }
            set { m_CR_CITY = value; }
        }
        #endregion

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _CR_FTE_CONT = "CR_FTE_CONT";
        public static readonly string _CR_PR_NO = "CR_PR_NO";
        public static readonly string _CR_PR_RELATION = "CR_PR_RELATION";
        public static readonly string _CR_CONT_NAME = "CR_CONT_NAME";
        public static readonly string _CR_GRP = "CR_GRP";
        public static readonly string _CR_DESIG = "CR_DESIG";
        public static readonly string _CR_DEPT = "CR_DEPT";
        public static readonly string _CR_OVS_LCL = "CR_OVS_LCL";
        public static readonly string _CR_BRANCH = "CR_BRANCH";
        public static readonly string _CR_COUNTRY = "CR_COUNTRY";
        public static readonly string _CR_CITY = "CR_CITY";
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
