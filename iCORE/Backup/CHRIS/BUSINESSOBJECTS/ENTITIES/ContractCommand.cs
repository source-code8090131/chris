

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ContractCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "CONTRACT"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faisal Iqbal]	01/10/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class ContractCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO_C;
        private String m_PR_BRANCH;
        private String m_PR_FIRST_NAME;
        private String m_PR_LAST_NAME;
        private String m_PR_DESIG;
        private String m_PR_CONTRACTOR;
        private DateTime m_PR_FROM_DATE;
        private DateTime m_PR_TO_DATE;
        private String m_PR_ADDRESS;
        private String m_PR_PHONE_1;
        private String m_PR_PHONE_2;
        private DateTime m_PR_DATE_BIRTH;
        private String m_PR_MARITAL;
        private String m_PR_SEX;
        private String m_PR_NID;
        private decimal m_PR_MONTHLY_SALARY;
        private String m_PR_FLAG;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        public Double PR_P_NO
        {
            get { return m_PR_P_NO_C; }
            set { m_PR_P_NO_C = value; }
        }
        #endregion

        #region "PR_BRANCH"
        public String PR_BRANCH
        {
            get { return m_PR_BRANCH; }
            set { m_PR_BRANCH = value; }
        }
        #endregion

        #region "PR_FIRST_NAME"
        public String PR_FIRST_NAME
        {
            get { return m_PR_FIRST_NAME; }
            set { m_PR_FIRST_NAME = value; }
        }
        #endregion

        #region "PR_LAST_NAME"
        public String PR_LAST_NAME
        {
            get { return m_PR_LAST_NAME; }
            set { m_PR_LAST_NAME = value; }
        }
        #endregion

        #region "PR_DESIG"
        public String PR_DESIG
        {
            get { return m_PR_DESIG; }
            set { m_PR_DESIG = value; }
        }
        #endregion

        #region "PR_CONTRACTOR"
        public String PR_CONTRACTOR
        {
            get { return m_PR_CONTRACTOR; }
            set { m_PR_CONTRACTOR = value; }
        }
        #endregion

        #region "PR_FROM_DATE"
        public DateTime PR_FROM_DATE
        {
            get { return m_PR_FROM_DATE; }
            set { m_PR_FROM_DATE = value; }
        }
        #endregion

        #region "PR_TO_DATE"
        public DateTime PR_TO_DATE
        {
            get { return m_PR_TO_DATE; }
            set { m_PR_TO_DATE = value; }
        }
        #endregion

        #region "PR_ADDRESS"
        public String PR_ADDRESS
        {
            get { return m_PR_ADDRESS; }
            set { m_PR_ADDRESS = value; }
        }
        #endregion

        #region "PR_PHONE_1"
        public String PR_PHONE_1
        {
            get { return m_PR_PHONE_1; }
            set { m_PR_PHONE_1 = value; }
        }
        #endregion

        #region "PR_PHONE_2"
        public String PR_PHONE_2
        {
            get { return m_PR_PHONE_2; }
            set { m_PR_PHONE_2 = value; }
        }
        #endregion

        #region "PR_DATE_BIRTH"
        public DateTime PR_DATE_BIRTH
        {
            get { return m_PR_DATE_BIRTH; }
            set { m_PR_DATE_BIRTH = value; }
        }
        #endregion

        #region "PR_MARITAL"
        public String PR_MARITAL
        {
            get { return m_PR_MARITAL; }
            set { m_PR_MARITAL = value; }
        }
        #endregion

        #region "PR_SEX"
        public String PR_SEX
        {
            get { return m_PR_SEX; }
            set { m_PR_SEX = value; }
        }
        #endregion

        #region "PR_NID"
        public String PR_NID
        {
            get { return m_PR_NID; }
            set { m_PR_NID = value; }
        }
        #endregion

        #region "PR_MONTHLY_SALARY"
        public decimal PR_MONTHLY_SALARY
        {
            get { return m_PR_MONTHLY_SALARY; }
            set { m_PR_MONTHLY_SALARY = value; }
        }
        #endregion

        #region "PR_FLAG"
        public String PR_FLAG
        {
            get { return m_PR_FLAG; }
            set { m_PR_FLAG = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO_C";
        public static readonly string _PR_BRANCH = "PR_BRANCH";
        public static readonly string _PR_FIRST_NAME = "PR_FIRST_NAME";
        public static readonly string _PR_LAST_NAME = "PR_LAST_NAME";
        public static readonly string _PR_DESIG = "PR_DESIG";
        public static readonly string _PR_CONTRACTOR = "PR_CONTRACTOR";
        public static readonly string _PR_FROM_DATE = "PR_FROM_DATE";
        public static readonly string _PR_TO_DATE = "PR_TO_DATE";
        public static readonly string _PR_ADDRESS = "PR_ADDRESS";
        public static readonly string _PR_PHONE_1 = "PR_PHONE_1";
        public static readonly string _PR_PHONE_2 = "PR_PHONE_2";
        public static readonly string _PR_DATE_BIRTH = "PR_DATE_BIRTH";
        public static readonly string _PR_MARITAL = "PR_MARITAL";
        public static readonly string _PR_SEX = "PR_SEX";
        public static readonly string _PR_NID = "PR_NID";
        public static readonly string _PR_MONTHLY_SALARY = "PR_MONTHLY_SALARY";
        public static readonly string _PR_FLAG = "PR_FLAG";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
