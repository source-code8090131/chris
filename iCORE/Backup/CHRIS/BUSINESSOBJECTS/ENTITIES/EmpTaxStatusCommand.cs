 

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.EmpTaxStatusCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "EMP_TAX_STATUS"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Framework]	01/15/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
	public class EmpTaxStatusCommand : BusinessEntity
	{

	//-------------------------------------Start Code generation for Business-------------------------------------

	#region "Auto generated code for Business Entity"

	#region "--Field Segment--"
        private Decimal m_PR_P_NO;
	 private String m_ETS_ASST_YEAR;
	 private String m_ETS_ASST_IT;
	 private String m_ETS_ASST_WT;
	 private int m_ID;
	#endregion "--Field Segment--"

	#region "--Property Segment--"

	#region "PR_P_NO"
    [CustomAttributes(IsForeignKey = true)]
        public Decimal PR_P_NO
	{ 
		get { return m_PR_P_NO; }
		set { m_PR_P_NO = value; }
	} 
	#endregion

	#region "ETS_ASST_YEAR"
	public String ETS_ASST_YEAR
	{ 
		get { return m_ETS_ASST_YEAR; }
		set { m_ETS_ASST_YEAR = value; }
	} 
	#endregion

	#region "ETS_ASST_IT"
	public String ETS_ASST_IT
	{ 
		get { return m_ETS_ASST_IT; }
		set { m_ETS_ASST_IT = value; }
	} 
	#endregion

	#region "ETS_ASST_WT"
	public String ETS_ASST_WT
	{ 
		get { return m_ETS_ASST_WT; }
		set { m_ETS_ASST_WT = value; }
	} 
	#endregion

	#region "ID"
	public int ID
	{ 
		get { return m_ID; }
		set { m_ID = value; }
	} 
	#endregion

	#endregion --Public Properties--

	#region "--Column Mapping--"
	 public static readonly string _PR_P_NO = "PR_P_NO";//PR_P_NO
	 public static readonly string _ETS_ASST_YEAR = "ETS_ASST_YEAR";
	 public static readonly string _ETS_ASST_IT = "ETS_ASST_IT";
	 public static readonly string _ETS_ASST_WT = "ETS_ASST_WT";
	 public static readonly string _ID = "ID";
	#endregion

	#endregion "Auto generated code"

	//-------------------------------------End Code generation for Business---------------------------------------

	//----------------------Region to keep all customized business related logic----------------------------

	#region "--Customize Business Methods--"

	#endregion "Customize Business Function"

	//-----------------------------------Customized region ends here----------------------------------------
		}
}


