

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PERSONNEL"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faisal Iqbal]	11/29/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PersonnelCommand_Tax : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Decimal m_PR_P_NO;
        private String m_PR_FIRST_NAME;
        private String m_PR_LAST_NAME;
        private DateTime m_PR_JOINING_DATE;
        private String m_PR_NATIONAL_TAX;
        private String m_PR_NTN_CERT_REC;
        private String m_PR_NTN_CARD_REC;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        public Decimal PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "PR_FIRST_NAME"
        public String PR_FIRST_NAME
        {
            get { return m_PR_FIRST_NAME; }
            set { m_PR_FIRST_NAME = value; }
        }
        #endregion

        #region "PR_LAST_NAME"
        public String PR_LAST_NAME
        {
            get { return m_PR_LAST_NAME; }
            set { m_PR_LAST_NAME = value; }
        }
        #endregion

        #region "PR_JOINING_DATE"
        public DateTime PR_JOINING_DATE
        {
            get { return m_PR_JOINING_DATE; }
            set { m_PR_JOINING_DATE = value; }
        }
        #endregion

        #region "PR_NATIONAL_TAX"
        public String PR_NATIONAL_TAX
        {
            get { return m_PR_NATIONAL_TAX; }
            set { m_PR_NATIONAL_TAX = value; }
        }
        #endregion
        
        #region "PR_NTN_CERT_REC"
        public String PR_NTN_CERT_REC
        {
            get { return m_PR_NTN_CERT_REC; }
            set { m_PR_NTN_CERT_REC = value; }
        }
        #endregion

        #region "PR_NTN_CARD_REC"
        public String PR_NTN_CARD_REC
        {
            get { return m_PR_NTN_CARD_REC; }
            set { m_PR_NTN_CARD_REC = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _PR_FIRST_NAME = "PR_FIRST_NAME";
        public static readonly string _PR_LAST_NAME = "PR_LAST_NAME";
        public static readonly string _PR_JOINING_DATE = "PR_JOINING_DATE";
        public static readonly string _PR_NATIONAL_TAX = "PR_NATIONAL_TAX";
        public static readonly string _PR_NTN_CERT_REC = "PR_NTN_CERT_REC";
        public static readonly string _PR_NTN_CARD_REC = "PR_NTN_CARD_REC";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
