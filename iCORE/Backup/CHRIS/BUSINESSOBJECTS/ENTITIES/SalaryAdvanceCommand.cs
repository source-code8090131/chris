/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SalaryAdvanceCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "SAL_ADVANCE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Tahnia]	01/19/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class SalaryAdvanceCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private DateTime m_W_DATE;
        private decimal m_SA_P_NO;
        private DateTime m_SA_DATE;
        private decimal m_SA_ADV_AMOUNT;
        private String m_SA_FOR_THE_MONTH1;
        private String m_SA_FOR_THE_MONTH2;
        private String m_SA_PAYROLL_GEN;
        private int m_ID;


        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SA_P_NO"
        public decimal SA_P_NO
        {
            get { return m_SA_P_NO; }
            set { m_SA_P_NO = value; }
        }
        #endregion

        #region "W_DATE"
        public DateTime W_DATE
        {
            get { return m_W_DATE; }
            set { m_W_DATE = value; }
        }
             #endregion

        #region "SA_DATE"
        public DateTime SA_DATE
        {
            get { return m_SA_DATE; }
            set { m_SA_DATE = value; }
        }
        #endregion

        #region "SA_ADV_AMOUNT"
        public decimal SA_ADV_AMOUNT
        {
            get { return m_SA_ADV_AMOUNT; }
            set { m_SA_ADV_AMOUNT = value; }
        }
        #endregion

        #region "SA_FOR_THE_MONTH1"
        public String SA_FOR_THE_MONTH1
        {
            get { return m_SA_FOR_THE_MONTH1; }
            set { m_SA_FOR_THE_MONTH1 = value; }
        }
        #endregion

        #region "SA_FOR_THE_MONTH2"
        public String SA_FOR_THE_MONTH2
        {
            get { return m_SA_FOR_THE_MONTH2; }
            set { m_SA_FOR_THE_MONTH2 = value; }
        }
        #endregion

        #region "SA_PAYROLL_GEN"
        public String SA_PAYROLL_GEN
        {
            get { return m_SA_PAYROLL_GEN; }
            set { m_SA_PAYROLL_GEN = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _W_DATE = "W_DATE";
        public static readonly string _SA_P_NO = "SA_P_NO";
        public static readonly string _SA_DATE = "SA_DATE";
        public static readonly string _SA_ADV_AMOUNT = "SA_ADV_AMOUNT";
        public static readonly string _SA_FOR_THE_MONTH1 = "SA_FOR_THE_MONTH1";
        public static readonly string _SA_FOR_THE_MONTH2 = "SA_FOR_THE_MONTH2";
        public static readonly string _SA_PAYROLL_GEN = "SA_PAYROLL_GEN";
        public static readonly string _ID = "ID";
        public static readonly string _PR_FIRST_NAME = "PR_FIRST_NAME";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
