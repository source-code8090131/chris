

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINTypeCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "FIN_TYPE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faisal Iqbal]	12/08/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class FINTypeCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_FN_TYPE;
        private String m_FN_DESC;
        private decimal m_FN_ELIG;
        private decimal m_FN_MARKUP;
        private String m_FN_CONFIRM;
        private Double m_FN_C_RATIO;
        private decimal m_FN_SCHEDULE;
        private decimal m_FN_LESS_AMT;
        private decimal m_FN_EX_LESS;
        private decimal m_FN_EX_GREAT;
        private int m_ID;
        private DateTime m_PR_D_BIRTH;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "FN_TYPE"
        public String FN_TYPE
        {
            get { return m_FN_TYPE; }
            set { m_FN_TYPE = value; }
        }
        #endregion

        #region "FN_DESC"
        public String FN_DESC
        {
            get { return m_FN_DESC; }
            set { m_FN_DESC = value; }
        }
        #endregion

        #region "FN_ELIG"
        public decimal FN_ELIG
        {
            get { return m_FN_ELIG; }
            set { m_FN_ELIG = value; }
        }
        #endregion

        #region "FN_MARKUP"
        public decimal FN_MARKUP
        {
            get { return m_FN_MARKUP; }
            set { m_FN_MARKUP = value; }
        }
        #endregion

        #region "FN_CONFIRM"
        public String FN_CONFIRM
        {
            get { return m_FN_CONFIRM; }
            set { m_FN_CONFIRM = value; }
        }
        #endregion

        #region "FN_C_RATIO"
        public Double FN_C_RATIO
        {
            get { return m_FN_C_RATIO; }
            set { m_FN_C_RATIO = value; }
        }
        #endregion

        #region "FN_SCHEDULE"
        public decimal FN_SCHEDULE
        {
            get { return m_FN_SCHEDULE; }
            set { m_FN_SCHEDULE = value; }
        }
        #endregion

        #region "FN_LESS_AMT"
        public decimal FN_LESS_AMT
        {
            get { return m_FN_LESS_AMT; }
            set { m_FN_LESS_AMT = value; }
        }
        #endregion

        #region "FN_EX_LESS"
        public decimal FN_EX_LESS
        {
            get { return m_FN_EX_LESS; }
            set { m_FN_EX_LESS = value; }
        }
        #endregion

        #region "FN_EX_GREAT"
        public decimal FN_EX_GREAT
        {
            get { return m_FN_EX_GREAT; }
            set { m_FN_EX_GREAT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #region "PR_D_BIRTH"
        public DateTime PR_D_BIRTH
        {
            get { return m_PR_D_BIRTH; }
            set { m_PR_D_BIRTH = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _FN_TYPE = "FN_TYPE";
        public static readonly string _FN_DESC = "FN_DESC";
        public static readonly string _FN_ELIG = "FN_ELIG";
        public static readonly string _FN_MARKUP = "FN_MARKUP";
        public static readonly string _FN_CONFIRM = "FN_CONFIRM";
        public static readonly string _FN_C_RATIO = "FN_C_RATIO";
        public static readonly string _FN_SCHEDULE = "FN_SCHEDULE";
        public static readonly string _FN_LESS_AMT = "FN_LESS_AMT";
        public static readonly string _FN_EX_LESS = "FN_EX_LESS";
        public static readonly string _FN_EX_GREAT = "FN_EX_GREAT";
        public static readonly string _ID = "ID";
        public static readonly string _PR_D_BIRTH = "PR_D_BIRTH";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
