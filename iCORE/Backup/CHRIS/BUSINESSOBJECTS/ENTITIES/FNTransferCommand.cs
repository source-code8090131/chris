
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FN_TransferCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "FN_TRANSFER"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	12/21/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class FN_TransferCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_FNT_FIN_NO;
        private decimal m_FNT_P_NO;
        private DateTime m_FNT_DATE;
        private String m_FNT_OLD_TYPE;
        private String m_FNT_NEW_TYPE;
        private decimal m_FNT_ADJ_AMT;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "FNT_FIN_NO"
        public String FNT_FIN_NO
        {
            get { return m_FNT_FIN_NO; }
            set { m_FNT_FIN_NO = value; }
        }
        #endregion

        #region "FNT_P_NO"
        public decimal FNT_P_NO
        {
            get { return m_FNT_P_NO; }
            set { m_FNT_P_NO = value; }
        }
        #endregion

        #region "FNT_DATE"
        public DateTime FNT_DATE
        {
            get { return m_FNT_DATE; }
            set { m_FNT_DATE = value; }
        }
        #endregion

        #region "FNT_OLD_TYPE"
        public String FNT_OLD_TYPE
        {
            get { return m_FNT_OLD_TYPE; }
            set { m_FNT_OLD_TYPE = value; }
        }
        #endregion

        #region "FNT_NEW_TYPE"
        public String FNT_NEW_TYPE
        {
            get { return m_FNT_NEW_TYPE; }
            set { m_FNT_NEW_TYPE = value; }
        }
        #endregion

        #region "FNT_ADJ_AMT"
        public decimal FNT_ADJ_AMT
        {
            get { return m_FNT_ADJ_AMT; }
            set { m_FNT_ADJ_AMT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _FNT_FIN_NO = "FNT_FIN_NO";
        public static readonly string _FNT_P_NO = "FNT_P_NO";
        public static readonly string _FNT_DATE = "FNT_DATE";
        public static readonly string _FNT_OLD_TYPE = "FNT_OLD_TYPE";
        public static readonly string _FNT_NEW_TYPE = "FNT_NEW_TYPE";
        public static readonly string _FNT_ADJ_AMT = "FNT_ADJ_AMT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
