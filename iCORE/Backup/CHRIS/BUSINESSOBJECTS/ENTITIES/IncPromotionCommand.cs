

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.INC_PROMOTIONCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "INC_PROMOTION"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Anila]	01/14/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class IncPromotionCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Nullable <decimal> m_PR_IN_NO;
        private String m_PR_INC_TYPE;
        private String m_PR_RANK;
        private Nullable<DateTime> m_PR_RANKING_DATE;
        private Nullable<DateTime> m_PR_EFFECTIVE;
        private Nullable<decimal> m_PR_INC_AMT;
        private Nullable<decimal> m_PR_INC_PER;
        private Nullable<DateTime> m_PR_LAST_APP;
        private Nullable<DateTime> m_PR_NEXT_APP;
        private String m_PR_REMARKS;
        private Nullable<decimal> m_PR_ANNUAL_PRESENT;
        private String m_PR_DESIG_PRESENT;
        private String m_PR_LEVEL_PRESENT;
        private String m_PR_FUNCT_1_PRESENT;
        private String m_PR_FUNCT_2_PRESENT;
        private Nullable<decimal> m_PR_ANNUAL_PREVIOUS;
        private String m_PR_DESIG_PREVIOUS;
        private String m_PR_LEVEL_PREVIOUS;
        private String m_PR_FUNCT_1_PREVIOUS;
        private String m_PR_FUNCT_2_PREVIOUS;
        private String m_PR_CURRENT_PREVIOUS;
        private Nullable<decimal> m_PR_NO_INCR;
        private Nullable<decimal> m_PR_NO_MONTHS;
        private Nullable<decimal> m_PR_STEP_AMT;
        private String m_PR_PROMOTED;
        private String m_PR_FINAL;
        private Nullable<decimal> m_PR_P_NO;  //Added by Anila on 6th january 2010     
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_IN_NO"
        public Nullable <decimal> PR_IN_NO
        {
            get { return m_PR_IN_NO; }
            set { m_PR_IN_NO = value; }
        }
        #endregion

        #region "PR_INC_TYPE"
        public String PR_INC_TYPE
        {
            get { return m_PR_INC_TYPE; }
            set { m_PR_INC_TYPE = value; }
        }
        #endregion

        #region "PR_RANK"
        public String PR_RANK
        {
            get { return m_PR_RANK; }
            set { m_PR_RANK = value; }
        }
        #endregion

        #region "PR_RANKING_DATE"
        public Nullable<DateTime> PR_RANKING_DATE
        {
            get { return m_PR_RANKING_DATE; }
            set { m_PR_RANKING_DATE = value; }
        }
        #endregion

        #region "PR_EFFECTIVE"
        public Nullable<DateTime> PR_EFFECTIVE
        {
            get { return m_PR_EFFECTIVE; }
            set { m_PR_EFFECTIVE = value; }
        }
        #endregion

        #region "PR_INC_AMT"
        public Nullable<decimal> PR_INC_AMT
        {
            get { return m_PR_INC_AMT; }
            set { m_PR_INC_AMT = value; }
        }
        #endregion

        #region "PR_INC_PER"
        public Nullable<decimal> PR_INC_PER
        {
            get { return m_PR_INC_PER; }
            set { m_PR_INC_PER = value; }
        }
        #endregion

        #region "PR_LAST_APP"
        public Nullable<DateTime> PR_LAST_APP
        {
            get { return m_PR_LAST_APP; }
            set { m_PR_LAST_APP = value; }
        }
        #endregion

        #region "PR_NEXT_APP"
        public Nullable<DateTime> PR_NEXT_APP
        {
            get { return m_PR_NEXT_APP; }
            set { m_PR_NEXT_APP = value; }
        }
        #endregion

        #region "PR_REMARKS"
        public String PR_REMARKS
        {
            get { return m_PR_REMARKS; }
            set { m_PR_REMARKS = value; }
        }
        #endregion

        #region "PR_ANNUAL_PRESENT"
        public Nullable<decimal> PR_ANNUAL_PRESENT
        {
            get { return m_PR_ANNUAL_PRESENT; }
            set { m_PR_ANNUAL_PRESENT = value; }
        }
        #endregion

        #region "PR_DESIG_PRESENT"
        public String PR_DESIG_PRESENT
        {
            get { return m_PR_DESIG_PRESENT; }
            set { m_PR_DESIG_PRESENT = value; }
        }
        #endregion

        #region "PR_LEVEL_PRESENT"
        public String PR_LEVEL_PRESENT
        {
            get { return m_PR_LEVEL_PRESENT; }
            set { m_PR_LEVEL_PRESENT = value; }
        }
        #endregion

        #region "PR_FUNCT_1_PRESENT"
        public String PR_FUNCT_1_PRESENT
        {
            get { return m_PR_FUNCT_1_PRESENT; }
            set { m_PR_FUNCT_1_PRESENT = value; }
        }
        #endregion

        #region "PR_FUNCT_2_PRESENT"
        public String PR_FUNCT_2_PRESENT
        {
            get { return m_PR_FUNCT_2_PRESENT; }
            set { m_PR_FUNCT_2_PRESENT = value; }
        }
        #endregion

        #region "PR_ANNUAL_PREVIOUS"
        public Nullable<decimal> PR_ANNUAL_PREVIOUS
        {
            get { return m_PR_ANNUAL_PREVIOUS; }
            set { m_PR_ANNUAL_PREVIOUS = value; }
        }
        #endregion

        #region "PR_DESIG_PREVIOUS"
        public String PR_DESIG_PREVIOUS
        {
            get { return m_PR_DESIG_PREVIOUS; }
            set { m_PR_DESIG_PREVIOUS = value; }
        }
        #endregion

        #region "PR_LEVEL_PREVIOUS"
        public String PR_LEVEL_PREVIOUS
        {
            get { return m_PR_LEVEL_PREVIOUS; }
            set { m_PR_LEVEL_PREVIOUS = value; }
        }
        #endregion

        #region "PR_FUNCT_1_PREVIOUS"
        public String PR_FUNCT_1_PREVIOUS
        {
            get { return m_PR_FUNCT_1_PREVIOUS; }
            set { m_PR_FUNCT_1_PREVIOUS = value; }
        }
        #endregion

        #region "PR_FUNCT_2_PREVIOUS"
        public String PR_FUNCT_2_PREVIOUS
        {
            get { return m_PR_FUNCT_2_PREVIOUS; }
            set { m_PR_FUNCT_2_PREVIOUS = value; }
        }
        #endregion

        #region "PR_CURRENT_PREVIOUS"
        public String PR_CURRENT_PREVIOUS
        {
            get { return m_PR_CURRENT_PREVIOUS; }
            set { m_PR_CURRENT_PREVIOUS = value; }
        }
        #endregion

        #region "PR_NO_INCR"
        public Nullable<decimal> PR_NO_INCR
        {
            get { return m_PR_NO_INCR; }
            set { m_PR_NO_INCR = value; }
        }
        #endregion

        #region "PR_NO_MONTHS"
        public Nullable<decimal> PR_NO_MONTHS
        {
            get { return m_PR_NO_MONTHS; }
            set { m_PR_NO_MONTHS = value; }
        }
        #endregion

        #region "PR_STEP_AMT"
        public Nullable<decimal> PR_STEP_AMT
        {
            get { return m_PR_STEP_AMT; }
            set { m_PR_STEP_AMT = value; }
        }
        #endregion

        #region "PR_PROMOTED"
        public String PR_PROMOTED
        {
            get { return m_PR_PROMOTED; }
            set { m_PR_PROMOTED = value; }
        }
        #endregion

        #region "PR_FINAL"
        public String PR_FINAL
        {
            get { return m_PR_FINAL; }
            set { m_PR_FINAL = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion
        
        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Nullable<decimal> PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_IN_NO = "PR_IN_NO";
        public static readonly string _PR_INC_TYPE = "PR_INC_TYPE";
        public static readonly string _PR_RANK = "PR_RANK";
        public static readonly string _PR_RANKING_DATE = "PR_RANKING_DATE";
        public static readonly string _PR_EFFECTIVE = "PR_EFFECTIVE";
        public static readonly string _PR_INC_AMT = "PR_INC_AMT";
        public static readonly string _PR_INC_PER = "PR_INC_PER";
        public static readonly string _PR_LAST_APP = "PR_LAST_APP";
        public static readonly string _PR_NEXT_APP = "PR_NEXT_APP";
        public static readonly string _PR_REMARKS = "PR_REMARKS";
        public static readonly string _PR_ANNUAL_PRESENT = "PR_ANNUAL_PRESENT";
        public static readonly string _PR_DESIG_PRESENT = "PR_DESIG_PRESENT";
        public static readonly string _PR_LEVEL_PRESENT = "PR_LEVEL_PRESENT";
        public static readonly string _PR_FUNCT_1_PRESENT = "PR_FUNCT_1_PRESENT";
        public static readonly string _PR_FUNCT_2_PRESENT = "PR_FUNCT_2_PRESENT";
        public static readonly string _PR_ANNUAL_PREVIOUS = "PR_ANNUAL_PREVIOUS";
        public static readonly string _PR_DESIG_PREVIOUS = "PR_DESIG_PREVIOUS";
        public static readonly string _PR_LEVEL_PREVIOUS = "PR_LEVEL_PREVIOUS";
        public static readonly string _PR_FUNCT_1_PREVIOUS = "PR_FUNCT_1_PREVIOUS";
        public static readonly string _PR_FUNCT_2_PREVIOUS = "PR_FUNCT_2_PREVIOUS";
        public static readonly string _PR_CURRENT_PREVIOUS = "PR_CURRENT_PREVIOUS";
        public static readonly string _PR_NO_INCR = "PR_NO_INCR";
        public static readonly string _PR_NO_MONTHS = "PR_NO_MONTHS";
        public static readonly string _PR_STEP_AMT = "PR_STEP_AMT";
        public static readonly string _PR_PROMOTED = "PR_PROMOTED";
        public static readonly string _PR_FINAL = "PR_FINAL";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
