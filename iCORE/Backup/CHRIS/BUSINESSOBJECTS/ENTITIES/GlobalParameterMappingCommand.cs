

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.DDS.BUSINESSOBJECTS.ENTITIES.GlobalParameterMappingCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "COM_GLOBAL_PARAM_MAPPING"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Azam Farooq]	12 May 2011	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class GlobalParameterMappingCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private int m_ID;
        private String m_SOE_ID;
        private int m_GLOBAL_PARAM_ID;
        private String m_GLOBAL_PARAM_VALUE;
        private String m_GLOBAL_PARAM_NAME;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #region "SOE_ID"
        public String SOE_ID
        {
            get { return m_SOE_ID; }
            set { m_SOE_ID = value; }
        }
        #endregion

        #region "GLOBAL_PARAM_ID"
        public int GLOBAL_PARAM_ID
        {
            get { return m_GLOBAL_PARAM_ID; }
            set { m_GLOBAL_PARAM_ID = value; }
        }
        #endregion

        #region "GLOBAL_PARAM_VALUE"
        public String GLOBAL_PARAM_VALUE
        {
            get { return m_GLOBAL_PARAM_VALUE; }
            set { m_GLOBAL_PARAM_VALUE = value; }
        }
        #endregion

        #region "GLOBAL_PARAM_NAME"
        public String GLOBAL_PARAM_NAME
        {
            get { return m_GLOBAL_PARAM_NAME; }
            set { m_GLOBAL_PARAM_NAME = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _ID = "ID";
        public static readonly string _SOE_ID = "SOE_ID";
        public static readonly string _GLOBAL_PARAM_ID = "GLOBAL_PARAM_ID";
        public static readonly string _GLOBAL_PARAM_VALUE = "GLOBAL_PARAM_VALUE";
        public static readonly string _GLOBAL_PARAM_NAME = "GLOBAL_PARAM_NAME";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
