

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : CHRIS.FinLiquidationfnMonthCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "FN_MONTH"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	12/29/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{   
    public class FinLiquidationfnMonthCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private String m_FN_M_BRANCH;
        private String m_FN_TYPE;
        private String m_FN_FIN_NO;
        private DateTime m_FN_MDATE;
        private decimal m_FN_DEBIT;
        private decimal m_FN_CREDIT;
        private decimal m_FN_PAY_LEFT;
        private decimal m_FN_BALANCE;
        private decimal m_FN_LOAN_BALANCE;
        private decimal m_FN_MARKUP;
        private String m_FN_LIQ_FLAG;
        private String m_FN_SUBTYPE;
        private Nullable<DateTime> m_FN_START_DATE;
        private Nullable<DateTime> m_FN_END_DATE;
        private Nullable<DateTime> m_FN_EXTRA_TIME;
        private Nullable<DateTime> m_pr_transfer_date;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "FN_M_BRANCH"
        public String FN_M_BRANCH
        {
            get { return m_FN_M_BRANCH; }
            set { m_FN_M_BRANCH = value; }
        }
        #endregion

        #region "FN_TYPE"
        public String FN_TYPE
        {
            get { return m_FN_TYPE; }
            set { m_FN_TYPE = value; }
        }
        #endregion

        #region "FN_FIN_NO"
        public String FN_FIN_NO
        {
            get { return m_FN_FIN_NO; }
            set { m_FN_FIN_NO = value; }
        }
        #endregion

        #region "FN_MDATE"
        public DateTime FN_MDATE
        {
            get { return m_FN_MDATE; }
            set { m_FN_MDATE = value; }
        }
        #endregion

        #region "FN_DEBIT"
        public decimal FN_DEBIT
        {
            get { return m_FN_DEBIT; }
            set { m_FN_DEBIT = value; }
        }
        #endregion

        #region "FN_CREDIT"
        public decimal FN_CREDIT
        {
            get { return m_FN_CREDIT; }
            set { m_FN_CREDIT = value; }
        }
        #endregion

        #region "FN_PAY_LEFT"
        public decimal FN_PAY_LEFT
        {
            get { return m_FN_PAY_LEFT; }
            set { m_FN_PAY_LEFT = value; }
        }
        #endregion

        #region "FN_BALANCE"
        public decimal FN_BALANCE
        {
            get { return m_FN_BALANCE; }
            set { m_FN_BALANCE = value; }
        }
        #endregion

        #region "FN_LOAN_BALANCE"
        public decimal FN_LOAN_BALANCE
        {
            get { return m_FN_LOAN_BALANCE; }
            set { m_FN_LOAN_BALANCE = value; }
        }
        #endregion

        #region "FN_MARKUP"
        public decimal FN_MARKUP
        {
            get { return m_FN_MARKUP; }
            set { m_FN_MARKUP = value; }
        }
        #endregion

        #region "FN_LIQ_FLAG"
        public String FN_LIQ_FLAG
        {
            get { return m_FN_LIQ_FLAG; }
            set { m_FN_LIQ_FLAG = value; }
        }
        #endregion

        #region "FN_SUBTYPE"
        public String FN_SUBTYPE
        {
            get { return m_FN_SUBTYPE; }
            set { m_FN_SUBTYPE = value; }
        }
        #endregion

        #region "FN_START_DATE"
        public Nullable<DateTime> FN_START_DATE
        {
            get { return m_FN_START_DATE; }
            set { m_FN_START_DATE = value; }
        }
        #endregion

        #region "FN_END_DATE"
        public Nullable<DateTime> FN_END_DATE
        {
            get { return m_FN_END_DATE; }
            set { m_FN_END_DATE = value; }
        }
        #endregion

        #region "FN_EXTRA_TIME"
        public Nullable<DateTime> FN_EXTRA_TIME
        {
            get { return m_FN_EXTRA_TIME; }
            set { m_FN_EXTRA_TIME = value; }
        }
        #endregion

        #region "pr_transfer_date"
        public Nullable<DateTime> pr_transfer_date
        {
            get { return m_pr_transfer_date; }
            set { m_pr_transfer_date = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _FN_M_BRANCH = "FN_M_BRANCH";
        public static readonly string _FN_TYPE = "FN_TYPE";
        public static readonly string _FN_FIN_NO = "FN_FIN_NO";
        public static readonly string _FN_MDATE = "FN_MDATE";
        public static readonly string _FN_DEBIT = "FN_DEBIT";
        public static readonly string _FN_CREDIT = "FN_CREDIT";
        public static readonly string _FN_PAY_LEFT = "FN_PAY_LEFT";
        public static readonly string _FN_BALANCE = "FN_BALANCE";
        public static readonly string _FN_LOAN_BALANCE = "FN_LOAN_BALANCE";
        public static readonly string _FN_MARKUP = "FN_MARKUP";
        public static readonly string _FN_LIQ_FLAG = "FN_LIQ_FLAG";
        public static readonly string _FN_SUBTYPE = "FN_SUBTYPE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
