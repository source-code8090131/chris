using System;
using System.Collections.Generic;
using System.Text;
using iCORE.Common;

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    /// <summary>
    /// Claim Serach Panel Entity
    /// </summary>
    public class ClaimEntryCommand : BusinessEntity
    {

        private String m_Pol_Policy_No;

        public String CLA_POLICY
        {
            get { return m_Pol_Policy_No; }
            set { m_Pol_Policy_No = value; }
        }
        private String m_Pol_Ctt_Off;

        public String Pol_Ctt_Off
        {
            get { return m_Pol_Ctt_Off; }
            set { m_Pol_Ctt_Off = value; }
        }

        private Decimal m_Pr_P_No;

        public Decimal CLA_P_NO
        {
            get { return m_Pr_P_No; }
            set { m_Pr_P_No = value; }
        }

        private string m_Person_Name;

        public string Person_Name
        {
            get { return m_Person_Name; }
            set { m_Person_Name = value; }
        }
        
    }
}
