

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LeaSchEntCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "LEAVE_SCHEDULE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	01/26/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class LeaSchEntCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_LSH_PR_NO;
        private String m_LSH_JAN1;
        private String m_LSH_JAN2;
        private String m_LSH_JAN3;
        private String m_LSH_JAN4;
        private String m_LSH_FEB1;
        private String m_LSH_FEB2;
        private String m_LSH_FEB3;
        private String m_LSH_FEB4;
        private String m_LSH_MAR1;
        private String m_LSH_MAR2;
        private String m_LSH_MAR3;
        private String m_LSH_MAR4;
        private String m_LSH_APR1;
        private String m_LSH_APR2;
        private String m_LSH_APR3;
        private String m_LSH_APR4;
        private String m_LSH_MAY1;
        private String m_LSH_MAY2;
        private String m_LSH_MAY3;
        private String m_LSH_MAY4;
        private String m_LSH_JUN1;
        private String m_LSH_JUN2;
        private String m_LSH_JUN3;
        private String m_LSH_JUN4;
        private String m_LSH_JUL1;
        private String m_LSH_JUL2;
        private String m_LSH_JUL3;
        private String m_LSH_JUL4;
        private String m_LSH_AUG1;
        private String m_LSH_AUG2;
        private String m_LSH_AUG3;
        private String m_LSH_AUG4;
        private String m_LSH_SEP1;
        private String m_LSH_SEP2;
        private String m_LSH_SEP3;
        private String m_LSH_SEP4;
        private String m_LSH_OCT1;
        private String m_LSH_OCT2;
        private String m_LSH_OCT3;
        private String m_LSH_OCT4;
        private String m_LSH_NOV1;
        private String m_LSH_NOV2;
        private String m_LSH_NOV3;
        private String m_LSH_NOV4;
        private String m_LSH_DEC1;
        private String m_LSH_DEC2;
        private String m_LSH_DEC3;
        private String m_LSH_DEC4;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "LSH_PR_NO"
        public decimal LSH_PR_NO
        {
            get { return m_LSH_PR_NO; }
            set { m_LSH_PR_NO = value; }
        }
        #endregion

        #region "LSH_JAN1"
        public String LSH_JAN1
        {
            get { return m_LSH_JAN1; }
            set { m_LSH_JAN1 = value; }
        }
        #endregion

        #region "LSH_JAN2"
        public String LSH_JAN2
        {
            get { return m_LSH_JAN2; }
            set { m_LSH_JAN2 = value; }
        }
        #endregion

        #region "LSH_JAN3"
        public String LSH_JAN3
        {
            get { return m_LSH_JAN3; }
            set { m_LSH_JAN3 = value; }
        }
        #endregion

        #region "LSH_JAN4"
        public String LSH_JAN4
        {
            get { return m_LSH_JAN4; }
            set { m_LSH_JAN4 = value; }
        }
        #endregion

        #region "LSH_FEB1"
        public String LSH_FEB1
        {
            get { return m_LSH_FEB1; }
            set { m_LSH_FEB1 = value; }
        }
        #endregion

        #region "LSH_FEB2"
        public String LSH_FEB2
        {
            get { return m_LSH_FEB2; }
            set { m_LSH_FEB2 = value; }
        }
        #endregion

        #region "LSH_FEB3"
        public String LSH_FEB3
        {
            get { return m_LSH_FEB3; }
            set { m_LSH_FEB3 = value; }
        }
        #endregion

        #region "LSH_FEB4"
        public String LSH_FEB4
        {
            get { return m_LSH_FEB4; }
            set { m_LSH_FEB4 = value; }
        }
        #endregion

        #region "LSH_MAR1"
        public String LSH_MAR1
        {
            get { return m_LSH_MAR1; }
            set { m_LSH_MAR1 = value; }
        }
        #endregion

        #region "LSH_MAR2"
        public String LSH_MAR2
        {
            get { return m_LSH_MAR2; }
            set { m_LSH_MAR2 = value; }
        }
        #endregion

        #region "LSH_MAR3"
        public String LSH_MAR3
        {
            get { return m_LSH_MAR3; }
            set { m_LSH_MAR3 = value; }
        }
        #endregion

        #region "LSH_MAR4"
        public String LSH_MAR4
        {
            get { return m_LSH_MAR4; }
            set { m_LSH_MAR4 = value; }
        }
        #endregion

        #region "LSH_APR1"
        public String LSH_APR1
        {
            get { return m_LSH_APR1; }
            set { m_LSH_APR1 = value; }
        }
        #endregion

        #region "LSH_APR2"
        public String LSH_APR2
        {
            get { return m_LSH_APR2; }
            set { m_LSH_APR2 = value; }
        }
        #endregion

        #region "LSH_APR3"
        public String LSH_APR3
        {
            get { return m_LSH_APR3; }
            set { m_LSH_APR3 = value; }
        }
        #endregion

        #region "LSH_APR4"
        public String LSH_APR4
        {
            get { return m_LSH_APR4; }
            set { m_LSH_APR4 = value; }
        }
        #endregion

        #region "LSH_MAY1"
        public String LSH_MAY1
        {
            get { return m_LSH_MAY1; }
            set { m_LSH_MAY1 = value; }
        }
        #endregion

        #region "LSH_MAY2"
        public String LSH_MAY2
        {
            get { return m_LSH_MAY2; }
            set { m_LSH_MAY2 = value; }
        }
        #endregion

        #region "LSH_MAY3"
        public String LSH_MAY3
        {
            get { return m_LSH_MAY3; }
            set { m_LSH_MAY3 = value; }
        }
        #endregion

        #region "LSH_MAY4"
        public String LSH_MAY4
        {
            get { return m_LSH_MAY4; }
            set { m_LSH_MAY4 = value; }
        }
        #endregion

        #region "LSH_JUN1"
        public String LSH_JUN1
        {
            get { return m_LSH_JUN1; }
            set { m_LSH_JUN1 = value; }
        }
        #endregion

        #region "LSH_JUN2"
        public String LSH_JUN2
        {
            get { return m_LSH_JUN2; }
            set { m_LSH_JUN2 = value; }
        }
        #endregion

        #region "LSH_JUN3"
        public String LSH_JUN3
        {
            get { return m_LSH_JUN3; }
            set { m_LSH_JUN3 = value; }
        }
        #endregion

        #region "LSH_JUN4"
        public String LSH_JUN4
        {
            get { return m_LSH_JUN4; }
            set { m_LSH_JUN4 = value; }
        }
        #endregion

        #region "LSH_JUL1"
        public String LSH_JUL1
        {
            get { return m_LSH_JUL1; }
            set { m_LSH_JUL1 = value; }
        }
        #endregion

        #region "LSH_JUL2"
        public String LSH_JUL2
        {
            get { return m_LSH_JUL2; }
            set { m_LSH_JUL2 = value; }
        }
        #endregion

        #region "LSH_JUL3"
        public String LSH_JUL3
        {
            get { return m_LSH_JUL3; }
            set { m_LSH_JUL3 = value; }
        }
        #endregion

        #region "LSH_JUL4"
        public String LSH_JUL4
        {
            get { return m_LSH_JUL4; }
            set { m_LSH_JUL4 = value; }
        }
        #endregion

        #region "LSH_AUG1"
        public String LSH_AUG1
        {
            get { return m_LSH_AUG1; }
            set { m_LSH_AUG1 = value; }
        }
        #endregion

        #region "LSH_AUG2"
        public String LSH_AUG2
        {
            get { return m_LSH_AUG2; }
            set { m_LSH_AUG2 = value; }
        }
        #endregion

        #region "LSH_AUG3"
        public String LSH_AUG3
        {
            get { return m_LSH_AUG3; }
            set { m_LSH_AUG3 = value; }
        }
        #endregion

        #region "LSH_AUG4"
        public String LSH_AUG4
        {
            get { return m_LSH_AUG4; }
            set { m_LSH_AUG4 = value; }
        }
        #endregion

        #region "LSH_SEP1"
        public String LSH_SEP1
        {
            get { return m_LSH_SEP1; }
            set { m_LSH_SEP1 = value; }
        }
        #endregion

        #region "LSH_SEP2"
        public String LSH_SEP2
        {
            get { return m_LSH_SEP2; }
            set { m_LSH_SEP2 = value; }
        }
        #endregion

        #region "LSH_SEP3"
        public String LSH_SEP3
        {
            get { return m_LSH_SEP3; }
            set { m_LSH_SEP3 = value; }
        }
        #endregion

        #region "LSH_SEP4"
        public String LSH_SEP4
        {
            get { return m_LSH_SEP4; }
            set { m_LSH_SEP4 = value; }
        }
        #endregion

        #region "LSH_OCT1"
        public String LSH_OCT1
        {
            get { return m_LSH_OCT1; }
            set { m_LSH_OCT1 = value; }
        }
        #endregion

        #region "LSH_OCT2"
        public String LSH_OCT2
        {
            get { return m_LSH_OCT2; }
            set { m_LSH_OCT2 = value; }
        }
        #endregion

        #region "LSH_OCT3"
        public String LSH_OCT3
        {
            get { return m_LSH_OCT3; }
            set { m_LSH_OCT3 = value; }
        }
        #endregion

        #region "LSH_OCT4"
        public String LSH_OCT4
        {
            get { return m_LSH_OCT4; }
            set { m_LSH_OCT4 = value; }
        }
        #endregion

        #region "LSH_NOV1"
        public String LSH_NOV1
        {
            get { return m_LSH_NOV1; }
            set { m_LSH_NOV1 = value; }
        }
        #endregion

        #region "LSH_NOV2"
        public String LSH_NOV2
        {
            get { return m_LSH_NOV2; }
            set { m_LSH_NOV2 = value; }
        }
        #endregion

        #region "LSH_NOV3"
        public String LSH_NOV3
        {
            get { return m_LSH_NOV3; }
            set { m_LSH_NOV3 = value; }
        }
        #endregion

        #region "LSH_NOV4"
        public String LSH_NOV4
        {
            get { return m_LSH_NOV4; }
            set { m_LSH_NOV4 = value; }
        }
        #endregion

        #region "LSH_DEC1"
        public String LSH_DEC1
        {
            get { return m_LSH_DEC1; }
            set { m_LSH_DEC1 = value; }
        }
        #endregion

        #region "LSH_DEC2"
        public String LSH_DEC2
        {
            get { return m_LSH_DEC2; }
            set { m_LSH_DEC2 = value; }
        }
        #endregion

        #region "LSH_DEC3"
        public String LSH_DEC3
        {
            get { return m_LSH_DEC3; }
            set { m_LSH_DEC3 = value; }
        }
        #endregion

        #region "LSH_DEC4"
        public String LSH_DEC4
        {
            get { return m_LSH_DEC4; }
            set { m_LSH_DEC4 = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _LSH_PR_NO = "LSH_PR_NO";
        public static readonly string _LSH_JAN1 = "LSH_JAN1";
        public static readonly string _LSH_JAN2 = "LSH_JAN2";
        public static readonly string _LSH_JAN3 = "LSH_JAN3";
        public static readonly string _LSH_JAN4 = "LSH_JAN4";
        public static readonly string _LSH_FEB1 = "LSH_FEB1";
        public static readonly string _LSH_FEB2 = "LSH_FEB2";
        public static readonly string _LSH_FEB3 = "LSH_FEB3";
        public static readonly string _LSH_FEB4 = "LSH_FEB4";
        public static readonly string _LSH_MAR1 = "LSH_MAR1";
        public static readonly string _LSH_MAR2 = "LSH_MAR2";
        public static readonly string _LSH_MAR3 = "LSH_MAR3";
        public static readonly string _LSH_MAR4 = "LSH_MAR4";
        public static readonly string _LSH_APR1 = "LSH_APR1";
        public static readonly string _LSH_APR2 = "LSH_APR2";
        public static readonly string _LSH_APR3 = "LSH_APR3";
        public static readonly string _LSH_APR4 = "LSH_APR4";
        public static readonly string _LSH_MAY1 = "LSH_MAY1";
        public static readonly string _LSH_MAY2 = "LSH_MAY2";
        public static readonly string _LSH_MAY3 = "LSH_MAY3";
        public static readonly string _LSH_MAY4 = "LSH_MAY4";
        public static readonly string _LSH_JUN1 = "LSH_JUN1";
        public static readonly string _LSH_JUN2 = "LSH_JUN2";
        public static readonly string _LSH_JUN3 = "LSH_JUN3";
        public static readonly string _LSH_JUN4 = "LSH_JUN4";
        public static readonly string _LSH_JUL1 = "LSH_JUL1";
        public static readonly string _LSH_JUL2 = "LSH_JUL2";
        public static readonly string _LSH_JUL3 = "LSH_JUL3";
        public static readonly string _LSH_JUL4 = "LSH_JUL4";
        public static readonly string _LSH_AUG1 = "LSH_AUG1";
        public static readonly string _LSH_AUG2 = "LSH_AUG2";
        public static readonly string _LSH_AUG3 = "LSH_AUG3";
        public static readonly string _LSH_AUG4 = "LSH_AUG4";
        public static readonly string _LSH_SEP1 = "LSH_SEP1";
        public static readonly string _LSH_SEP2 = "LSH_SEP2";
        public static readonly string _LSH_SEP3 = "LSH_SEP3";
        public static readonly string _LSH_SEP4 = "LSH_SEP4";
        public static readonly string _LSH_OCT1 = "LSH_OCT1";
        public static readonly string _LSH_OCT2 = "LSH_OCT2";
        public static readonly string _LSH_OCT3 = "LSH_OCT3";
        public static readonly string _LSH_OCT4 = "LSH_OCT4";
        public static readonly string _LSH_NOV1 = "LSH_NOV1";
        public static readonly string _LSH_NOV2 = "LSH_NOV2";
        public static readonly string _LSH_NOV3 = "LSH_NOV3";
        public static readonly string _LSH_NOV4 = "LSH_NOV4";
        public static readonly string _LSH_DEC1 = "LSH_DEC1";
        public static readonly string _LSH_DEC2 = "LSH_DEC2";
        public static readonly string _LSH_DEC3 = "LSH_DEC3";
        public static readonly string _LSH_DEC4 = "LSH_DEC4";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
