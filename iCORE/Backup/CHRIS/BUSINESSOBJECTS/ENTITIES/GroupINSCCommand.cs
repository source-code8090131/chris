

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GroupINSCCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "GROUP_INSC"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	01/24/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class GroupINSCCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_SP_TYPE_CODE;
        private String m_SP_NAME_1;
        private String m_SP_NAME_2;
        private String m_SP_STAFF_TYPE;
        private DateTime m_SP_RENEWAL;
        private String m_SP_POLICY_NO;
        private decimal m_SP_L_COVERAGE;
        private decimal m_SP_H_COVERAGE;
        private String m_SP_DESIG_IN;
        private String m_SP_LEVEL_IN;
        private decimal m_SP_MULT_PACK;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SP_TYPE_CODE"
        public String SP_TYPE_CODE
        {
            get { return m_SP_TYPE_CODE; }
            set { m_SP_TYPE_CODE = value; }
        }
        #endregion

        #region "SP_NAME_1"
        public String SP_NAME_1
        {
            get { return m_SP_NAME_1; }
            set { m_SP_NAME_1 = value; }
        }
        #endregion

        #region "SP_NAME_2"
        public String SP_NAME_2
        {
            get { return m_SP_NAME_2; }
            set { m_SP_NAME_2 = value; }
        }
        #endregion

        #region "SP_STAFF_TYPE"
        public String SP_STAFF_TYPE
        {
            get { return m_SP_STAFF_TYPE; }
            set { m_SP_STAFF_TYPE = value; }
        }
        #endregion

        #region "SP_RENEWAL"
        public DateTime SP_RENEWAL
        {
            get { return m_SP_RENEWAL; }
            set { m_SP_RENEWAL = value; }
        }
        #endregion

        #region "SP_POLICY_NO"
        public String SP_POLICY_NO
        {
            get { return m_SP_POLICY_NO; }
            set { m_SP_POLICY_NO = value; }
        }
        #endregion

        #region "SP_L_COVERAGE"
        public decimal SP_L_COVERAGE
        {
            get { return m_SP_L_COVERAGE; }
            set { m_SP_L_COVERAGE = value; }
        }
        #endregion

        #region "SP_H_COVERAGE"
        public decimal SP_H_COVERAGE
        {
            get { return m_SP_H_COVERAGE; }
            set { m_SP_H_COVERAGE = value; }
        }
        #endregion

        #region "SP_DESIG_IN"
        public String SP_DESIG_IN
        {
            get { return m_SP_DESIG_IN; }
            set { m_SP_DESIG_IN = value; }
        }
        #endregion

        #region "SP_LEVEL_IN"
        public String SP_LEVEL_IN
        {
            get { return m_SP_LEVEL_IN; }
            set { m_SP_LEVEL_IN = value; }
        }
        #endregion

        #region "SP_MULT_PACK"
        public decimal SP_MULT_PACK
        {
            get { return m_SP_MULT_PACK; }
            set { m_SP_MULT_PACK = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_TYPE_CODE = "SP_TYPE_CODE";
        public static readonly string _SP_NAME_1 = "SP_NAME_1";
        public static readonly string _SP_NAME_2 = "SP_NAME_2";
        public static readonly string _SP_STAFF_TYPE = "SP_STAFF_TYPE";
        public static readonly string _SP_RENEWAL = "SP_RENEWAL";
        public static readonly string _SP_POLICY_NO = "SP_POLICY_NO";
        public static readonly string _SP_L_COVERAGE = "SP_L_COVERAGE";
        public static readonly string _SP_H_COVERAGE = "SP_H_COVERAGE";
        public static readonly string _SP_DESIG_IN = "SP_DESIG_IN";
        public static readonly string _SP_LEVEL_IN = "SP_LEVEL_IN";
        public static readonly string _SP_MULT_PACK = "SP_MULT_PACK";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
