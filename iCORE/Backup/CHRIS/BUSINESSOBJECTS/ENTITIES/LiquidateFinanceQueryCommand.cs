using System;
using System.Collections.Generic;
using System.Text;

using iCORE.Common;

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class LiquidateFinanceQueryCommand : BusinessEntity
    {
        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private DateTime m_DBD;
        private DateTime m_LIQD;

        private Double m_DIS_AMT;
        private Double m_LIQD_AMT;
        private string m_FN_FIN_NO;

        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "DBD"
        public DateTime DBD
        {
            get { return m_DBD; }
            set { m_DBD = value; }
        }
        #endregion

        #region "LIQD"
        public DateTime LIQD
        {
            get { return m_LIQD; }
            set { m_LIQD = value; }
        }
        #endregion

        #region "DIS_AMT"
        public Double DIS_AMT
        {
            get { return m_DIS_AMT; }
            set { m_DIS_AMT = value; }
        }
        #endregion

        #region "LIQD_AMT"
        public Double LIQD_AMT
        {
            get { return m_LIQD_AMT; }
            set { m_LIQD_AMT = value; }
        }
        #endregion

        #region "FN_FIN_NO"
        public String FN_FIN_NO
        {
            get { return m_FN_FIN_NO; }
            set { m_FN_FIN_NO = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _DBD = "DBD";
        public static readonly string _LIQD = "LIQD";
        public static readonly string _DIS_AMT = "DIS_AMT";
        public static readonly string _LIQD_AMT = "LIQD_AMT";
        public static readonly string _FN_FIN_NO = "FN_FIN_NO";

        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------

    }
}
