

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PolicyInfoCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "POLICY_MASTER"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	04/08/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PolicyInfoCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_POL_POLICY_NO;
        private String m_POL_BRANCH;
        private String m_POL_SEGMENT;
        private String m_POL_POLICY_TYPE;
        private String m_POL_CTT_OFF;
        private DateTime m_POL_FROM;
        private DateTime m_POL_TO;
        private String m_POL_CONTACT_PER;
        private String m_POL_COMP_NAME1;
        private String m_POL_COMP_NAME2;
        private String m_POL_ADD1;
        private String m_POL_ADD2;
        private String m_POL_ADD3;
        private String m_POL_ADD4;
        private Nullable<decimal> m_POL_COV_MULTI;
        private Nullable<decimal> m_POL_MAX_LIMIT;
        private Nullable<decimal> m_POL_MAX_ACC;
        private Nullable<decimal> m_POL_CTT_COV;
        private Nullable<decimal> m_POL_CTT_ACC;
        private Nullable<decimal> m_POL_AILMENT;
        private Nullable<decimal> m_POL_DELV_LIMIT;
        private Nullable<decimal> m_POL_ROOM_CHG;
        private Nullable<decimal> m_POL_OMI_LIMIT;
        private Nullable<decimal> m_POL_OMI_SELF;
        private Nullable<decimal> m_POL_PER_DEPEN;
        private Nullable<decimal> m_POL_NO_DEPEN;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "POL_POLICY_NO"
        public String POL_POLICY_NO
        {
            get { return m_POL_POLICY_NO; }
            set { m_POL_POLICY_NO = value; }
        }
        #endregion

        #region "POL_BRANCH"
        public String POL_BRANCH
        {
            get { return m_POL_BRANCH; }
            set { m_POL_BRANCH = value; }
        }
        #endregion

        #region "POL_SEGMENT"
        public String POL_SEGMENT
        {
            get { return m_POL_SEGMENT; }
            set { m_POL_SEGMENT = value; }
        }
        #endregion

        #region "POL_POLICY_TYPE"
        public String POL_POLICY_TYPE
        {
            get { return m_POL_POLICY_TYPE; }
            set { m_POL_POLICY_TYPE = value; }
        }
        #endregion

        #region "POL_CTT_OFF"
        public String POL_CTT_OFF
        {
            get { return m_POL_CTT_OFF; }
            set { m_POL_CTT_OFF = value; }
        }
        #endregion

        #region "POL_FROM"
        public DateTime POL_FROM
        {
            get { return m_POL_FROM; }
            set { m_POL_FROM = value; }
        }
        #endregion

        #region "POL_TO"
        public DateTime POL_TO
        {
            get { return m_POL_TO; }
            set { m_POL_TO = value; }
        }
        #endregion

        #region "POL_CONTACT_PER"
        public String POL_CONTACT_PER
        {
            get { return m_POL_CONTACT_PER; }
            set { m_POL_CONTACT_PER = value; }
        }
        #endregion

        #region "POL_COMP_NAME1"
        public String POL_COMP_NAME1
        {
            get { return m_POL_COMP_NAME1; }
            set { m_POL_COMP_NAME1 = value; }
        }
        #endregion

        #region "POL_COMP_NAME2"
        public String POL_COMP_NAME2
        {
            get { return m_POL_COMP_NAME2; }
            set { m_POL_COMP_NAME2 = value; }
        }
        #endregion

        #region "POL_ADD1"
        public String POL_ADD1
        {
            get { return m_POL_ADD1; }
            set { m_POL_ADD1 = value; }
        }
        #endregion

        #region "POL_ADD2"
        public String POL_ADD2
        {
            get { return m_POL_ADD2; }
            set { m_POL_ADD2 = value; }
        }
        #endregion

        #region "POL_ADD3"
        public String POL_ADD3
        {
            get { return m_POL_ADD3; }
            set { m_POL_ADD3 = value; }
        }
        #endregion

        #region "POL_ADD4"
        public String POL_ADD4
        {
            get { return m_POL_ADD4; }
            set { m_POL_ADD4 = value; }
        }
        #endregion

        #region "POL_COV_MULTI"
        public Nullable<decimal> POL_COV_MULTI
        {
            get { return m_POL_COV_MULTI; }
            set { m_POL_COV_MULTI = value; }
        }
        #endregion

        #region "POL_MAX_LIMIT"
        public Nullable<decimal> POL_MAX_LIMIT
        {
            get { return m_POL_MAX_LIMIT; }
            set { m_POL_MAX_LIMIT = value; }
        }
        #endregion

        #region "POL_MAX_ACC"
        public Nullable<decimal> POL_MAX_ACC
        {
            get { return m_POL_MAX_ACC; }
            set { m_POL_MAX_ACC = value; }
        }
        #endregion

        #region "POL_CTT_COV"
        public Nullable<decimal> POL_CTT_COV
        {
            get { return m_POL_CTT_COV; }
            set { m_POL_CTT_COV = value; }
        }
        #endregion

        #region "POL_CTT_ACC"
        public Nullable<decimal> POL_CTT_ACC
        {
            get { return m_POL_CTT_ACC; }
            set { m_POL_CTT_ACC = value; }
        }
        #endregion

        #region "POL_AILMENT"
        public Nullable<decimal> POL_AILMENT
        {
            get { return m_POL_AILMENT; }
            set { m_POL_AILMENT = value; }
        }
        #endregion

        #region "POL_DELV_LIMIT"
        public Nullable<decimal> POL_DELV_LIMIT
        {
            get { return m_POL_DELV_LIMIT; }
            set { m_POL_DELV_LIMIT = value; }
        }
        #endregion

        #region "POL_ROOM_CHG"
        public Nullable<decimal> POL_ROOM_CHG
        {
            get { return m_POL_ROOM_CHG; }
            set { m_POL_ROOM_CHG = value; }
        }
        #endregion

        #region "POL_OMI_LIMIT"
        public Nullable<decimal> POL_OMI_LIMIT
        {
            get { return m_POL_OMI_LIMIT; }
            set { m_POL_OMI_LIMIT = value; }
        }
        #endregion

        #region "POL_OMI_SELF"
        public Nullable<decimal> POL_OMI_SELF
        {
            get { return m_POL_OMI_SELF; }
            set { m_POL_OMI_SELF = value; }
        }
        #endregion

        #region "POL_PER_DEPEN"
        public Nullable<decimal> POL_PER_DEPEN
        {
            get { return m_POL_PER_DEPEN; }
            set { m_POL_PER_DEPEN = value; }
        }
        #endregion

        #region "POL_NO_DEPEN"
        public Nullable<decimal> POL_NO_DEPEN
        {
            get { return m_POL_NO_DEPEN; }
            set { m_POL_NO_DEPEN = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _POL_POLICY_NO = "POL_POLICY_NO";
        public static readonly string _POL_BRANCH = "POL_BRANCH";
        public static readonly string _POL_SEGMENT = "POL_SEGMENT";
        public static readonly string _POL_POLICY_TYPE = "POL_POLICY_TYPE";
        public static readonly string _POL_CTT_OFF = "POL_CTT_OFF";
        public static readonly string _POL_FROM = "POL_FROM";
        public static readonly string _POL_TO = "POL_TO";
        public static readonly string _POL_CONTACT_PER = "POL_CONTACT_PER";
        public static readonly string _POL_COMP_NAME1 = "POL_COMP_NAME1";
        public static readonly string _POL_COMP_NAME2 = "POL_COMP_NAME2";
        public static readonly string _POL_ADD1 = "POL_ADD1";
        public static readonly string _POL_ADD2 = "POL_ADD2";
        public static readonly string _POL_ADD3 = "POL_ADD3";
        public static readonly string _POL_ADD4 = "POL_ADD4";
        public static readonly string _POL_COV_MULTI = "POL_COV_MULTI";
        public static readonly string _POL_MAX_LIMIT = "POL_MAX_LIMIT";
        public static readonly string _POL_MAX_ACC = "POL_MAX_ACC";
        public static readonly string _POL_CTT_COV = "POL_CTT_COV";
        public static readonly string _POL_CTT_ACC = "POL_CTT_ACC";
        public static readonly string _POL_AILMENT = "POL_AILMENT";
        public static readonly string _POL_DELV_LIMIT = "POL_DELV_LIMIT";
        public static readonly string _POL_ROOM_CHG = "POL_ROOM_CHG";
        public static readonly string _POL_OMI_LIMIT = "POL_OMI_LIMIT";
        public static readonly string _POL_OMI_SELF = "POL_OMI_SELF";
        public static readonly string _POL_PER_DEPEN = "POL_PER_DEPEN";
        public static readonly string _POL_NO_DEPEN = "POL_NO_DEPEN";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}