

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FIN_BOOK_COLLATCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "FIN_BOOK_COLLAT"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	12/24/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class FINBookCollatCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        //private double m_FN_P_NO;
        private String m_FN_FINANCE_NO;
        private String m_FN_COLLAT_NO;
        private String m_FN_PARTY1;
        private String m_FN_PARTY2;
        private DateTime m_FN_COLLAT_DT;
        private DateTime m_FN_DOC_RCV_DT;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "FN_FIN_NO"        
        [CustomAttributes(IsForeignKey = true)]
        public String FN_FIN_NO
        {
            get { return m_FN_FINANCE_NO; }
            set { m_FN_FINANCE_NO = value; }
        }
        #endregion

        #region "FN_COLLAT_NO"
        public String FN_COLLAT_NO
        {
            get { return m_FN_COLLAT_NO; }
            set { m_FN_COLLAT_NO = value; }
        }
        #endregion

        #region "FN_PARTY1"
        public String FN_PARTY1
        {
            get { return m_FN_PARTY1; }
            set { m_FN_PARTY1 = value; }
        }
        #endregion

        #region "FN_PARTY2"
        public String FN_PARTY2
        {
            get { return m_FN_PARTY2; }
            set { m_FN_PARTY2 = value; }
        }
        #endregion

        #region "FN_COLLAT_DT"
        public DateTime FN_COLLAT_DT
        {
            get { return m_FN_COLLAT_DT; }
            set { m_FN_COLLAT_DT = value; }
        }
        #endregion

        #region "FN_DOC_RCV_DT"
        public DateTime FN_DOC_RCV_DT
        {
            get { return m_FN_DOC_RCV_DT; }
            set { m_FN_DOC_RCV_DT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "FN_P_NO";
        public static readonly string _FN_FIN_NO = "FN_FINANCE_NO";
        public static readonly string _FN_COLLAT_NO = "FN_COLLAT_NO";
        public static readonly string _FN_PARTY1 = "FN_PARTY1";
        public static readonly string _FN_PARTY2 = "FN_PARTY2";
        public static readonly string _FN_COLLAT_DT = "FN_COLLAT_DT";
        public static readonly string _FN_DOC_RCV_DT = "FN_DOC_RCV_DT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
