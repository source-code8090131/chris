

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TRAININGPERSONNELCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "TRAINING"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[AHAD ZUBAIR]	01/28/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class TrainingPersonnelCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private String m_TR_LOC_OVRS;
        private String m_TR_PROG_NAME;
        private DateTime m_TR_PROG_DATE;
        private String m_TR_FTR_PROG_NAME;
        private DateTime m_TR_FTR_PROG_DATE;
        private String m_TR_COUNTRY;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "TR_PR_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "TR_LOC_OVRS"
        public String TR_LOC_OVRS
        {
            get { return m_TR_LOC_OVRS; }
            set { m_TR_LOC_OVRS = value; }
        }
        #endregion

        #region "TR_PROG_NAME"
        public String TR_PROG_NAME
        {
            get { return m_TR_PROG_NAME; }
            set { m_TR_PROG_NAME = value; }
        }
        #endregion

        #region "TR_PROG_DATE"
        public DateTime TR_PROG_DATE
        {
            get { return m_TR_PROG_DATE; }
            set { m_TR_PROG_DATE = value; }
        }
        #endregion

        #region "TR_FTR_PROG_NAME"
        public String TR_FTR_PROG_NAME
        {
            get { return m_TR_FTR_PROG_NAME; }
            set { m_TR_FTR_PROG_NAME = value; }
        }
        #endregion

        #region "TR_FTR_PROG_DATE"
        public DateTime TR_FTR_PROG_DATE
        {
            get { return m_TR_FTR_PROG_DATE; }
            set { m_TR_FTR_PROG_DATE = value; }
        }
        #endregion

        #region "TR_COUNTRY"
        public String TR_COUNTRY
        {
            get { return m_TR_COUNTRY; }
            set { m_TR_COUNTRY = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "TR_PR_NO";
        public static readonly string _TR_LOC_OVRS = "TR_LOC_OVRS";
        public static readonly string _TR_PROG_NAME = "TR_PROG_NAME";
        public static readonly string _TR_PROG_DATE = "TR_PROG_DATE";
        public static readonly string _TR_FTR_PROG_NAME = "TR_FTR_PROG_NAME";
        public static readonly string _TR_FTR_PROG_DATE = "TR_FTR_PROG_DATE";
        public static readonly string _TR_COUNTRY = "TR_COUNTRY";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
