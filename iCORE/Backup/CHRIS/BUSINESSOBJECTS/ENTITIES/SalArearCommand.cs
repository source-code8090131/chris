 

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SalArearCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "SAL_AREAR"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[FrameWork]	02/04/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
	public class SalArearCommand : BusinessEntity
	{

	//-------------------------------------Start Code generation for Business-------------------------------------

	#region "Auto generated code for Business Entity"

	#region "--Field Segment--"
	 private decimal? m_SA_P_NO;
	 private DateTime? m_SA_DATE;
	 private decimal? m_SA_SAL_AREARS;
	 private decimal? m_SA_PF_AREARS;
	 private decimal? m_SA_OT_AREARS;
	 private String m_SA_AREARS_APPROV;
	 private decimal? m_SA_HOLDING_TAX;
	 private decimal? m_SA_LFA;
	 private decimal? m_SA_MEDICAL;
	 private int m_ID;
	#endregion "--Field Segment--"

	#region "--Property Segment--"

	#region "SA_P_NO"
	public decimal? SA_P_NO
	{ 
		get { return m_SA_P_NO; }
		set { m_SA_P_NO = value; }
	} 
	#endregion

	#region "SA_DATE"
	public DateTime? SA_DATE
	{ 
		get { return m_SA_DATE; }
		set { m_SA_DATE = value; }
	} 
	#endregion

	#region "SA_SAL_AREARS"
	public decimal? SA_SAL_AREARS
	{ 
		get { return m_SA_SAL_AREARS; }
		set { m_SA_SAL_AREARS = value; }
	} 
	#endregion

	#region "SA_PF_AREARS"
	public decimal? SA_PF_AREARS
	{ 
		get { return m_SA_PF_AREARS; }
		set { m_SA_PF_AREARS = value; }
	} 
	#endregion

	#region "SA_OT_AREARS"
	public decimal? SA_OT_AREARS
	{ 
		get { return m_SA_OT_AREARS; }
		set { m_SA_OT_AREARS = value; }
	} 
	#endregion

	#region "SA_AREARS_APPROV"
	public String SA_AREARS_APPROV
	{ 
		get { return m_SA_AREARS_APPROV; }
		set { m_SA_AREARS_APPROV = value; }
	} 
	#endregion

	#region "SA_HOLDING_TAX"
	public decimal? SA_HOLDING_TAX
	{ 
		get { return m_SA_HOLDING_TAX; }
		set { m_SA_HOLDING_TAX = value; }
	} 
	#endregion

	#region "SA_LFA"
	public decimal? SA_LFA
	{ 
		get { return m_SA_LFA; }
		set { m_SA_LFA = value; }
	} 
	#endregion

	#region "SA_MEDICAL"
	public decimal? SA_MEDICAL
	{ 
		get { return m_SA_MEDICAL; }
		set { m_SA_MEDICAL = value; }
	} 
	#endregion

	#region "ID"
	public int ID
	{ 
		get { return m_ID; }
		set { m_ID = value; }
	} 
	#endregion

	#endregion --Public Properties--

	#region "--Column Mapping--"
	 public static readonly string _SA_P_NO = "SA_P_NO";
	 public static readonly string _SA_DATE = "SA_DATE";
	 public static readonly string _SA_SAL_AREARS = "SA_SAL_AREARS";
	 public static readonly string _SA_PF_AREARS = "SA_PF_AREARS";
	 public static readonly string _SA_OT_AREARS = "SA_OT_AREARS";
	 public static readonly string _SA_AREARS_APPROV = "SA_AREARS_APPROV";
	 public static readonly string _SA_HOLDING_TAX = "SA_HOLDING_TAX";
	 public static readonly string _SA_LFA = "SA_LFA";
	 public static readonly string _SA_MEDICAL = "SA_MEDICAL";
	 public static readonly string _ID = "ID";
	#endregion

	#endregion "Auto generated code"

	//-------------------------------------End Code generation for Business---------------------------------------

	//----------------------Region to keep all customized business related logic----------------------------

	#region "--Customize Business Methods--"

	#endregion "Customize Business Function"

	//-----------------------------------Customized region ends here----------------------------------------
		}
}

