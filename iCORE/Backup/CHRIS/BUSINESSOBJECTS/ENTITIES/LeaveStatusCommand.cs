

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LEAVE_STATUSCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "LEAVE_STATUS"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	01/11/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class LEAVE_STATUSCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private string m_LS_P_NO;
        private double m_LS_PL_AVAIL;
        private double m_LS_CL_AVAIL;
        private double m_LS_SL_AVAIL;
        private double m_LS_ML_AVAIL;
        private double m_LS_OT_AVAIL;
        private double m_LS_C_FORWARD;
        private double m_LS_PL_BAL;
        private double m_LS_CL_BAL;
        private double m_LS_SL_BAL;
        private double m_LS_ML_BAL;
        private String m_LS_MANDATORY;
        private String m_LS_BAL_CF;
        private String m_LS_CF_APPROVED;
        private double m_LS_LEV_ADV;
        private double m_LS_SL_HF_BAL;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "LS_P_NO"
        public string LS_P_NO
        {
            get { return m_LS_P_NO; }
            set { m_LS_P_NO = value; }
        }
        #endregion

        #region "LS_PL_AVAIL"
        public double LS_PL_AVAIL
        {
            get { return m_LS_PL_AVAIL; }
            set { m_LS_PL_AVAIL = value; }
        }
        #endregion

        #region "LS_CL_AVAIL"
        public double LS_CL_AVAIL
        {
            get { return m_LS_CL_AVAIL; }
            set { m_LS_CL_AVAIL = value; }
        }
        #endregion

        #region "LS_SL_AVAIL"
        public double LS_SL_AVAIL
        {
            get { return m_LS_SL_AVAIL; }
            set { m_LS_SL_AVAIL = value; }
        }
        #endregion

        #region "LS_ML_AVAIL"
        public double LS_ML_AVAIL
        {
            get { return m_LS_ML_AVAIL; }
            set { m_LS_ML_AVAIL = value; }
        }
        #endregion

        #region "LS_OT_AVAIL"
        public double LS_OT_AVAIL
        {
            get { return m_LS_OT_AVAIL; }
            set { m_LS_OT_AVAIL = value; }
        }
        #endregion

        #region "LS_C_FORWARD"
        public double LS_C_FORWARD
        {
            get { return m_LS_C_FORWARD; }
            set { m_LS_C_FORWARD = value; }
        }
        #endregion

        #region "LS_PL_BAL"
        public double LS_PL_BAL
        {
            get { return m_LS_PL_BAL; }
            set { m_LS_PL_BAL = value; }
        }
        #endregion

        #region "LS_CL_BAL"
        public double LS_CL_BAL
        {
            get { return m_LS_CL_BAL; }
            set { m_LS_CL_BAL = value; }
        }
        #endregion

        #region "LS_SL_BAL"
        public double LS_SL_BAL
        {
            get { return m_LS_SL_BAL; }
            set { m_LS_SL_BAL = value; }
        }
        #endregion

        #region "LS_ML_BAL"
        public double LS_ML_BAL
        {
            get { return m_LS_ML_BAL; }
            set { m_LS_ML_BAL = value; }
        }
        #endregion

        #region "LS_MANDATORY"
        public String LS_MANDATORY
        {
            get { return m_LS_MANDATORY; }
            set { m_LS_MANDATORY = value; }
        }
        #endregion

        #region "LS_BAL_CF"
        public String LS_BAL_CF
        {
            get { return m_LS_BAL_CF; }
            set { m_LS_BAL_CF = value; }
        }
        #endregion

        #region "LS_CF_APPROVED"
        public String LS_CF_APPROVED
        {
            get { return m_LS_CF_APPROVED; }
            set { m_LS_CF_APPROVED = value; }
        }
        #endregion

        #region "LS_LEV_ADV"
        public double LS_LEV_ADV
        {
            get { return m_LS_LEV_ADV; }
            set { m_LS_LEV_ADV = value; }
        }
        #endregion

        #region "LS_SL_HF_BAL"
        public double LS_SL_HF_BAL
        {
            get { return m_LS_SL_HF_BAL; }
            set { m_LS_SL_HF_BAL = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _LS_P_NO = "LS_P_NO";
        public static readonly string _LS_PL_AVAIL = "LS_PL_AVAIL";
        public static readonly string _LS_CL_AVAIL = "LS_CL_AVAIL";
        public static readonly string _LS_SL_AVAIL = "LS_SL_AVAIL";
        public static readonly string _LS_ML_AVAIL = "LS_ML_AVAIL";
        public static readonly string _LS_OT_AVAIL = "LS_OT_AVAIL";
        public static readonly string _LS_C_FORWARD = "LS_C_FORWARD";
        public static readonly string _LS_PL_BAL = "LS_PL_BAL";
        public static readonly string _LS_CL_BAL = "LS_CL_BAL";
        public static readonly string _LS_SL_BAL = "LS_SL_BAL";
        public static readonly string _LS_ML_BAL = "LS_ML_BAL";
        public static readonly string _LS_MANDATORY = "LS_MANDATORY";
        public static readonly string _LS_BAL_CF = "LS_BAL_CF";
        public static readonly string _LS_CF_APPROVED = "LS_CF_APPROVED";
        public static readonly string _LS_LEV_ADV = "LS_LEV_ADV";
        public static readonly string _LS_SL_HF_BAL = "LS_SL_HF_BAL";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
