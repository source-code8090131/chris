

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptFTECommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "DEPT_FTE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Anila]	01/28/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class DeptFTECommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_DP_P_NO;
        private String m_DP_SEG;
        private String m_DP_DEPT;
        private decimal m_DP_HC;
        private decimal m_DP_FTE;
        private int m_ID;

        //added for LOV
        private string m_SP_DEPT;
        private string m_SP_DESC;
        private string m_PR_NAME;

        //added for foreign key relation
        private decimal m_PR_P_NO;
        private DateTime m_PR_TRANSFER_DATE;

        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "DP_P_NO"
        public decimal DP_P_NO
        {
            get { return m_DP_P_NO; }
            set { m_DP_P_NO = value; }
        }
        #endregion

        #region "DP_SEG"
        public String DP_SEG
        {
            get { return m_DP_SEG; }
            set { m_DP_SEG = value; }
        }
        #endregion

        #region "DP_DEPT"
        public String DP_DEPT
        {
            get { return m_DP_DEPT; }
            set { m_DP_DEPT = value; }
        }
        #endregion

        #region "DP_HC"
        public decimal DP_HC
        {
            get { return m_DP_HC; }
            set { m_DP_HC = value; }
        }
        #endregion

        #region "DP_FTE"
        public decimal DP_FTE
        {
            get { return m_DP_FTE; }
            set { m_DP_FTE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        //added for LOV

        #region "SP_DEPT"
        public String SP_DEPT
        {
            get { return m_SP_DEPT; }
            set { m_SP_DEPT = value; }
        }
        #endregion

        #region "SP_DESC"
        public String SP_DESC
        {
            get { return m_SP_DESC; }
            set { m_SP_DESC = value; }
        }
        #endregion

        #region "PR_NAME"
        public String PR_NAME
        {
            get { return m_PR_NAME; }
            set { m_PR_NAME = value; }
        }
        #endregion

        //added for foreign key relation
        [CustomAttributes(IsForeignKey = true)]
        #region "PR_P_NO"
        public decimal PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        //added for foreign key relation
        #region "PR_TRANSFER_DATE"
        //[CustomAttributes(IsForeignKey = true)]
        public DateTime PR_TRANSFER_DATE
        {
            get { return m_PR_TRANSFER_DATE; }
            set { m_PR_TRANSFER_DATE = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _DP_P_NO = "DP_P_NO";
        public static readonly string _DP_SEG = "DP_SEG";
        public static readonly string _DP_DEPT = "DP_DEPT";
        public static readonly string _DP_HC = "DP_HC";
        public static readonly string _DP_FTE = "DP_FTE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}

