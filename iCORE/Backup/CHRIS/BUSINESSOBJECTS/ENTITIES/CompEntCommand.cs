

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CompEntCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "COMP"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	01/30/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class CompEntCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_LVL_CODE;
        private String m_GRPHD;
        private decimal m_MIN_SAL;
        private decimal m_MAX_SAL;
        private decimal m_MID_SAL;
        private decimal m_BDGT_PRICE;
        private decimal m_ANL_DEPC;
        private decimal m_PTRL_LTR;
        private decimal m_PTRL_RATE;
        private decimal m_INS_RATE;
        private decimal m_MNTNC_AMT;
        private decimal m_DRVR_COST;
        private decimal m_GRPHD_ALL;
        private decimal m_BO_YEAR;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "LVL_CODE"
        public String LVL_CODE
        {
            get { return m_LVL_CODE; }
            set { m_LVL_CODE = value; }
        }
        #endregion

        #region "GRPHD"
        public String GRPHD
        {
            get { return m_GRPHD; }
            set { m_GRPHD = value; }
        }
        #endregion

        #region "MIN_SAL"
        public decimal MIN_SAL
        {
            get { return m_MIN_SAL; }
            set { m_MIN_SAL = value; }
        }
        #endregion

        #region "MAX_SAL"
        public decimal MAX_SAL
        {
            get { return m_MAX_SAL; }
            set { m_MAX_SAL = value; }
        }
        #endregion

        #region "MID_SAL"
        public decimal MID_SAL
        {
            get { return m_MID_SAL; }
            set { m_MID_SAL = value; }
        }
        #endregion

        #region "BDGT_PRICE"
        public decimal BDGT_PRICE
        {
            get { return m_BDGT_PRICE; }
            set { m_BDGT_PRICE = value; }
        }
        #endregion

        #region "ANL_DEPC"
        public decimal ANL_DEPC
        {
            get { return m_ANL_DEPC; }
            set { m_ANL_DEPC = value; }
        }
        #endregion

        #region "PTRL_LTR"
        public decimal PTRL_LTR
        {
            get { return m_PTRL_LTR; }
            set { m_PTRL_LTR = value; }
        }
        #endregion

        #region "PTRL_RATE"
        public decimal PTRL_RATE
        {
            get { return m_PTRL_RATE; }
            set { m_PTRL_RATE = value; }
        }
        #endregion

        #region "INS_RATE"
        public decimal INS_RATE
        {
            get { return m_INS_RATE; }
            set { m_INS_RATE = value; }
        }
        #endregion

        #region "MNTNC_AMT"
        public decimal MNTNC_AMT
        {
            get { return m_MNTNC_AMT; }
            set { m_MNTNC_AMT = value; }
        }
        #endregion

        #region "DRVR_COST"
        public decimal DRVR_COST
        {
            get { return m_DRVR_COST; }
            set { m_DRVR_COST = value; }
        }
        #endregion

        #region "GRPHD_ALL"
        public decimal GRPHD_ALL
        {
            get { return m_GRPHD_ALL; }
            set { m_GRPHD_ALL = value; }
        }
        #endregion

        #region "BO_YEAR"
        public decimal BO_YEAR
        {
            get { return m_BO_YEAR; }
            set { m_BO_YEAR = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _LVL_CODE = "LVL_CODE";
        public static readonly string _GRPHD = "GRPHD";
        public static readonly string _MIN_SAL = "MIN_SAL";
        public static readonly string _MAX_SAL = "MAX_SAL";
        public static readonly string _MID_SAL = "MID_SAL";
        public static readonly string _BDGT_PRICE = "BDGT_PRICE";
        public static readonly string _ANL_DEPC = "ANL_DEPC";
        public static readonly string _PTRL_LTR = "PTRL_LTR";
        public static readonly string _PTRL_RATE = "PTRL_RATE";
        public static readonly string _INS_RATE = "INS_RATE";
        public static readonly string _MNTNC_AMT = "MNTNC_AMT";
        public static readonly string _DRVR_COST = "DRVR_COST";
        public static readonly string _GRPHD_ALL = "GRPHD_ALL";
        public static readonly string _BO_YEAR = "BO_YEAR";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
