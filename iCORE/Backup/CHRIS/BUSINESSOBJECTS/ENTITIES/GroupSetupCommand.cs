

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : CHRIS.GroupSetupCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "GROUP1"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	01/28/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class GroupSetupCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_SP_SEGMENT;
        private String m_SP_GROUP_CODE;
        private String m_SP_DESC_1;
        private String m_SP_DESC_2;
        private String m_SP_GROUP_HEAD;
        private DateTime m_SP_DATE_FROM;
        private DateTime m_SP_DATE_TO;
        private int m_ID;
        
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SP_SEGMENT"
        public String SP_SEGMENT
        {
            get { return m_SP_SEGMENT; }
            set { m_SP_SEGMENT = value; }
        }
        #endregion

        #region "SP_GROUP_CODE"
        public String SP_GROUP_CODE
        {
            get { return m_SP_GROUP_CODE; }
            set { m_SP_GROUP_CODE = value; }
        }
        #endregion

        #region "SP_DESC_1"
        public String SP_DESC_1
        {
            get { return m_SP_DESC_1; }
            set { m_SP_DESC_1 = value; }
        }
        #endregion

        #region "SP_DESC_2"
        public String SP_DESC_2
        {
            get { return m_SP_DESC_2; }
            set { m_SP_DESC_2 = value; }
        }
        #endregion

        #region "SP_GROUP_HEAD"
        public String SP_GROUP_HEAD
        {
            get { return m_SP_GROUP_HEAD; }
            set { m_SP_GROUP_HEAD = value; }
        }
        #endregion

        #region "SP_DATE_FROM"
        public DateTime SP_DATE_FROM
        {
            get { return m_SP_DATE_FROM; }
            set { m_SP_DATE_FROM = value; }
        }
        #endregion

        #region "SP_DATE_TO"
        public DateTime SP_DATE_TO
        {
            get { return m_SP_DATE_TO; }
            set { m_SP_DATE_TO = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_SEGMENT = "SP_SEGMENT";
        public static readonly string _SP_GROUP_CODE = "SP_GROUP_CODE";
        public static readonly string _SP_DESC_1 = "SP_DESC_1";
        public static readonly string _SP_DESC_2 = "SP_DESC_2";
        public static readonly string _SP_GROUP_HEAD = "SP_GROUP_HEAD";
        public static readonly string _SP_DATE_FROM = "SP_DATE_FROM";
        public static readonly string _SP_DATE_TO = "SP_DATE_TO";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
