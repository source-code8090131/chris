
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SelfInsuranceCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "FN_INSURANCE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	12/14/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class SelfInsuranceCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_FNI_P_NO;
        private String m_FNI_COMP;
        private String m_FNI_POLICY;
        private String m_FNI_TYPE;
        private decimal m_FNI_INS_AMT;
        private decimal m_FNI_ANNUAL_RED;
        private decimal m_FNI_ANNUAL_PRE;
        private DateTime m_FNI_PAY_DATE;
        private DateTime m_FNI_LAST_PAY;
        private String m_FNI_NOMINATION;
        private String m_FNI_ASSIG_BANK;
        private int m_ID;

        private decimal m_INS_PREMIUM;
        private DateTime m_PA_DATE;

        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "FNI_P_NO"
        public Double FNI_P_NO
        {
            get { return m_FNI_P_NO; }
            set { m_FNI_P_NO = value; }
        }
        #endregion

        #region "FNI_COMP"
        public String FNI_COMP
        {
            get { return m_FNI_COMP; }
            set { m_FNI_COMP = value; }
        }
        #endregion

        #region "FNI_POLICY"
        public String FNI_POLICY
        {
            get { return m_FNI_POLICY; }
            set { m_FNI_POLICY = value; }
        }
        #endregion

        #region "FNI_TYPE"
        public String FNI_TYPE
        {
            get { return m_FNI_TYPE; }
            set { m_FNI_TYPE = value; }
        }
        #endregion

        #region "FNI_INS_AMT"
        public decimal FNI_INS_AMT
        {
            get { return m_FNI_INS_AMT; }
            set { m_FNI_INS_AMT = value; }
        }
        #endregion

        #region "FNI_ANNUAL_RED"
        public decimal FNI_ANNUAL_RED
        {
            get { return m_FNI_ANNUAL_RED; }
            set { m_FNI_ANNUAL_RED = value; }
        }
        #endregion

        #region "INS_PREMIUM"
        public decimal INS_PREMIUM
        {
            get { return m_INS_PREMIUM; }
            set { m_INS_PREMIUM = value; }
        }
         #endregion

        #region "FNI_ANNUAL_PRE"
        public decimal FNI_ANNUAL_PRE
        {
            get { return m_FNI_ANNUAL_PRE; }
            set { m_FNI_ANNUAL_PRE = value; }
        }
        #endregion

        #region "FNI_PAY_DATE"
        public DateTime FNI_PAY_DATE
        {
            get { return m_FNI_PAY_DATE; }
            set { m_FNI_PAY_DATE = value; }
        }
        #endregion

        #region "FNI_LAST_PAY"
        public DateTime FNI_LAST_PAY
        {
            get { return m_FNI_LAST_PAY; }
            set { m_FNI_LAST_PAY = value; }
        }
        #endregion

        #region "PA_DATE"
        public DateTime PA_DATE
        {
            get { return m_PA_DATE; }
            set { m_PA_DATE = value; }
        }
        #endregion

        #region "FNI_NOMINATION"
        public String FNI_NOMINATION
        {
            get { return m_FNI_NOMINATION; }
            set { m_FNI_NOMINATION = value; }
        }
        #endregion

        #region "FNI_ASSIG_BANK"
        public String FNI_ASSIG_BANK
        {
            get { return m_FNI_ASSIG_BANK; }
            set { m_FNI_ASSIG_BANK = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _FNI_P_NO = "FNI_P_NO";
        public static readonly string _FNI_COMP = "FNI_COMP";
        public static readonly string _FNI_POLICY = "FNI_POLICY";
        public static readonly string _FNI_TYPE = "FNI_TYPE";
        public static readonly string _FNI_INS_AMT = "FNI_INS_AMT";
        public static readonly string _FNI_ANNUAL_RED = "FNI_ANNUAL_RED";
        public static readonly string _FNI_ANNUAL_PRE = "FNI_ANNUAL_PRE";
        public static readonly string _FNI_PAY_DATE = "FNI_PAY_DATE";
        public static readonly string _FNI_LAST_PAY = "FNI_LAST_PAY";
        public static readonly string _FNI_NOMINATION = "FNI_NOMINATION";
        public static readonly string _FNI_ASSIG_BANK = "FNI_ASSIG_BANK";
        public static readonly string _ID = "ID";
        
        public static readonly string _INS_PREMIUM = "INS_PREMIUM";
        public static readonly string _PA_DATE = "PA_DATE";
        
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
