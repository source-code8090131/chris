
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "DEPT_CONT"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	12/10/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class FinanceLiquidateDeptCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private String m_PR_SEGMENT;
        private String m_PR_DEPT;
        private decimal m_PR_CONTRIB;
        private decimal m_PR_MENU_OPTION;
        private String m_PR_TYPE;
        private DateTime m_PR_EFFECTIVE_DATE;
        private Nullable<DateTime> m_pr_transfer_date;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        [CustomAttributes(IsForeignKey = true)]
        #region "PR_P_NO"
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        [CustomAttributes(IsForeignKey = true)]
        #region "pr_transfer_date"
        public Nullable<DateTime> pr_transfer_date
        {
            get { return m_pr_transfer_date; }
            set { m_pr_transfer_date = value; }
        }
        #endregion

        #region "PR_SEGMENT"
        public String PR_SEGMENT
        {
            get { return m_PR_SEGMENT; }
            set { m_PR_SEGMENT = value; }
        }
        #endregion

        #region "PR_DEPT"
        public String PR_DEPT
        {
            get { return m_PR_DEPT; }
            set { m_PR_DEPT = value; }
        }
        #endregion

        #region "PR_CONTRIB"
        public decimal PR_CONTRIB
        {
            get { return m_PR_CONTRIB; }
            set { m_PR_CONTRIB = value; }
        }
        #endregion

        #region "PR_MENU_OPTION"
        public decimal PR_MENU_OPTION
        {
            get { return m_PR_MENU_OPTION; }
            set { m_PR_MENU_OPTION = value; }
        }
        #endregion

        #region "PR_TYPE"
        public String PR_TYPE
        {
            get { return m_PR_TYPE; }
            set { m_PR_TYPE = value; }
        }
        #endregion


        #region "PR_EFFECTIVE_DATE"
        public DateTime PR_EFFECTIVE_DATE
        {
            get { return m_PR_EFFECTIVE_DATE; }
            set { m_PR_EFFECTIVE_DATE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _PR_SEGMENT = "PR_SEGMENT";
        public static readonly string _PR_DEPT = "PR_DEPT";
        public static readonly string _PR_CONTRIB = "PR_CONTRIB";
        public static readonly string _PR_MENU_OPTION = "PR_MENU_OPTION";
        public static readonly string _PR_TYPE = "PR_TYPE";
        public static readonly string _PR_EFFECTIVE_DATE = "PR_EFFECTIVE_DATE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
