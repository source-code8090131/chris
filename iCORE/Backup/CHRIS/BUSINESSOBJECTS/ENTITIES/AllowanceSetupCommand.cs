

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceSetupCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "ALLOWANCE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	01/29/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class AllowanceSetupCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_SP_ALL_CODE;
        private String m_SP_BRANCH_A;
        private String m_SP_DESC;
        private String m_SP_CATEGORY_A;
        private String m_SP_DESG_A;
        private String m_SP_LEVEL_A;
        private DateTime m_SP_VALID_FROM;
        private DateTime m_SP_VALID_TO;
        private decimal m_SP_ALL_AMOUNT;
        private decimal m_SP_PER_ASR;
        private String m_SP_TAX;
        private String m_SP_ATT_RELATED;
        private String m_SP_RELATED_LEAV;
        private String m_SP_ASR_BASIC;
        private String m_SP_MEAL_CONV;
        private String m_SP_ALL_IND;
        private String m_SP_ACOUNT_NO;
        private String m_SP_FORECAST_TAX;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SP_ALL_CODE"
        public String SP_ALL_CODE
        {
            get { return m_SP_ALL_CODE; }
            set { m_SP_ALL_CODE = value; }
        }
        #endregion

        #region "SP_BRANCH_A"
        public String SP_BRANCH_A
        {
            get { return m_SP_BRANCH_A; }
            set { m_SP_BRANCH_A = value; }
        }
        #endregion

        #region "SP_DESC"
        public String SP_DESC
        {
            get { return m_SP_DESC; }
            set { m_SP_DESC = value; }
        }
        #endregion

        #region "SP_CATEGORY_A"
        public String SP_CATEGORY_A
        {
            get { return m_SP_CATEGORY_A; }
            set { m_SP_CATEGORY_A = value; }
        }
        #endregion

        #region "SP_DESG_A"
        public String SP_DESG_A
        {
            get { return m_SP_DESG_A; }
            set { m_SP_DESG_A = value; }
        }
        #endregion

        #region "SP_LEVEL_A"
        public String SP_LEVEL_A
        {
            get { return m_SP_LEVEL_A; }
            set { m_SP_LEVEL_A = value; }
        }
        #endregion

        #region "SP_VALID_FROM"
        public DateTime SP_VALID_FROM
        {
            get { return m_SP_VALID_FROM; }
            set { m_SP_VALID_FROM = value; }
        }
        #endregion

        #region "SP_VALID_TO"
        public DateTime SP_VALID_TO
        {
            get { return m_SP_VALID_TO; }
            set { m_SP_VALID_TO = value; }
        }
        #endregion

        #region "SP_ALL_AMOUNT"
        public decimal SP_ALL_AMOUNT
        {
            get { return m_SP_ALL_AMOUNT; }
            set { m_SP_ALL_AMOUNT = value; }
        }
        #endregion

        #region "SP_PER_ASR"
        public decimal SP_PER_ASR
        {
            get { return m_SP_PER_ASR; }
            set { m_SP_PER_ASR = value; }
        }
        #endregion

        #region "SP_TAX"
        public String SP_TAX
        {
            get { return m_SP_TAX; }
            set { m_SP_TAX = value; }
        }
        #endregion

        #region "SP_ATT_RELATED"
        public String SP_ATT_RELATED
        {
            get { return m_SP_ATT_RELATED; }
            set { m_SP_ATT_RELATED = value; }
        }
        #endregion

        #region "SP_RELATED_LEAV"
        public String SP_RELATED_LEAV
        {
            get { return m_SP_RELATED_LEAV; }
            set { m_SP_RELATED_LEAV = value; }
        }
        #endregion

        #region "SP_ASR_BASIC"
        public String SP_ASR_BASIC
        {
            get { return m_SP_ASR_BASIC; }
            set { m_SP_ASR_BASIC = value; }
        }
        #endregion

        #region "SP_MEAL_CONV"
        public String SP_MEAL_CONV
        {
            get { return m_SP_MEAL_CONV; }
            set { m_SP_MEAL_CONV = value; }
        }
        #endregion

        #region "SP_ALL_IND"
        public String SP_ALL_IND
        {
            get { return m_SP_ALL_IND; }
            set { m_SP_ALL_IND = value; }
        }
        #endregion

        #region "SP_ACOUNT_NO"
        public String SP_ACOUNT_NO
        {
            get { return m_SP_ACOUNT_NO; }
            set { m_SP_ACOUNT_NO = value; }
        }
        #endregion

        #region "SP_FORECAST_TAX"
        public String SP_FORECAST_TAX
        {
            get { return m_SP_FORECAST_TAX; }
            set { m_SP_FORECAST_TAX = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_ALL_CODE = "SP_ALL_CODE";
        public static readonly string _SP_BRANCH_A = "SP_BRANCH_A";
        public static readonly string _SP_DESC = "SP_DESC";
        public static readonly string _SP_CATEGORY_A = "SP_CATEGORY_A";
        public static readonly string _SP_DESG_A = "SP_DESG_A";
        public static readonly string _SP_LEVEL_A = "SP_LEVEL_A";
        public static readonly string _SP_VALID_FROM = "SP_VALID_FROM";
        public static readonly string _SP_VALID_TO = "SP_VALID_TO";
        public static readonly string _SP_ALL_AMOUNT = "SP_ALL_AMOUNT";
        public static readonly string _SP_PER_ASR = "SP_PER_ASR";
        public static readonly string _SP_TAX = "SP_TAX";
        public static readonly string _SP_ATT_RELATED = "SP_ATT_RELATED";
        public static readonly string _SP_RELATED_LEAV = "SP_RELATED_LEAV";
        public static readonly string _SP_ASR_BASIC = "SP_ASR_BASIC";
        public static readonly string _SP_MEAL_CONV = "SP_MEAL_CONV";
        public static readonly string _SP_ALL_IND = "SP_ALL_IND";
        public static readonly string _SP_ACOUNT_NO = "SP_ACOUNT_NO";
        public static readonly string _SP_FORECAST_TAX = "SP_FORECAST_TAX";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
