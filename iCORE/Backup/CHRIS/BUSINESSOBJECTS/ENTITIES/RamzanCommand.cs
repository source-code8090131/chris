 

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RamzanCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "RAMZAN"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Kiran]	01/17/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
	public class RamzanCommand : BusinessEntity
	{

	//-------------------------------------Start Code generation for Business-------------------------------------

	#region "Auto generated code for Business Entity"

	#region "--Field Segment--"
	 private DateTime m_START_DAT;
	 private DateTime m_END_DAT;
	 private String m_TIME_IN;
	 private String m_TIME_OUT;
	 private int m_ID;
	#endregion "--Field Segment--"

	#region "--Property Segment--"

	#region "START_DAT"
	public DateTime START_DAT
	{ 
		get { return m_START_DAT; }
		set { m_START_DAT = value; }
	} 
	#endregion

	#region "END_DAT"
	public DateTime END_DAT
	{ 
		get { return m_END_DAT; }
		set { m_END_DAT = value; }
	} 
	#endregion

	#region "TIME_IN"
	public String TIME_IN
	{ 
		get { return m_TIME_IN; }
		set { m_TIME_IN = value; }
	} 
	#endregion

	#region "TIME_OUT"
	public String TIME_OUT
	{ 
		get { return m_TIME_OUT; }
		set { m_TIME_OUT = value; }
	} 
	#endregion

	#region "ID"
	public int ID
	{ 
		get { return m_ID; }
		set { m_ID = value; }
	} 
	#endregion

	#endregion --Public Properties--

	#region "--Column Mapping--"
	 public static readonly string _START_DAT = "START_DAT";
	 public static readonly string _END_DAT = "END_DAT";
	 public static readonly string _TIME_IN = "TIME_IN";
	 public static readonly string _TIME_OUT = "TIME_OUT";
	 public static readonly string _ID = "ID";
	#endregion

	#endregion "Auto generated code"

	//-------------------------------------End Code generation for Business---------------------------------------

	//----------------------Region to keep all customized business related logic----------------------------

	#region "--Customize Business Methods--"

	#endregion "Customize Business Function"

	//-----------------------------------Customized region ends here----------------------------------------
		}
}

