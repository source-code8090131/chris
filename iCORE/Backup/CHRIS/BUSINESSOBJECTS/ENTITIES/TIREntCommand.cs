

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TIREntCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "TIR"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	02/01/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class TIREntCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_SP_YEAR;
        private decimal m_SP_TIR_PER;
        private String m_SP_LEVEL;
        private decimal m_SP_RANK;
        private decimal m_SP_MIN_PER;
        private decimal m_SP_MAX_PER;
        private decimal m_SP_AVERAGE;
        private decimal m_SP_LOCAL_TIR;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SP_YEAR"
        public decimal SP_YEAR
        {
            get { return m_SP_YEAR; }
            set { m_SP_YEAR = value; }
        }
        #endregion

        #region "SP_TIR_PER"
        public decimal SP_TIR_PER
        {
            get { return m_SP_TIR_PER; }
            set { m_SP_TIR_PER = value; }
        }
        #endregion

        #region "SP_LEVEL"
        public String SP_LEVEL
        {
            get { return m_SP_LEVEL; }
            set { m_SP_LEVEL = value; }
        }
        #endregion

        #region "SP_RANK"
        public decimal SP_RANK
        {
            get { return m_SP_RANK; }
            set { m_SP_RANK = value; }
        }
        #endregion

        #region "SP_MIN_PER"
        public decimal SP_MIN_PER
        {
            get { return m_SP_MIN_PER; }
            set { m_SP_MIN_PER = value; }
        }
        #endregion

        #region "SP_MAX_PER"
        public decimal SP_MAX_PER
        {
            get { return m_SP_MAX_PER; }
            set { m_SP_MAX_PER = value; }
        }
        #endregion

        #region "SP_AVERAGE"
        public decimal SP_AVERAGE
        {
            get { return m_SP_AVERAGE; }
            set { m_SP_AVERAGE = value; }
        }
        #endregion

        #region "SP_LOCAL_TIR"
        public decimal SP_LOCAL_TIR
        {
            get { return m_SP_LOCAL_TIR; }
            set { m_SP_LOCAL_TIR = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_YEAR = "SP_YEAR";
        public static readonly string _SP_TIR_PER = "SP_TIR_PER";
        public static readonly string _SP_LEVEL = "SP_LEVEL";
        public static readonly string _SP_RANK = "SP_RANK";
        public static readonly string _SP_MIN_PER = "SP_MIN_PER";
        public static readonly string _SP_MAX_PER = "SP_MAX_PER";
        public static readonly string _SP_AVERAGE = "SP_AVERAGE";
        public static readonly string _SP_LOCAL_TIR = "SP_LOCAL_TIR";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
