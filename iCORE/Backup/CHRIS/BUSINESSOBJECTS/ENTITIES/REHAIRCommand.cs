

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.REHAIRCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "REHAIR"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	01/04/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class REHAIRCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private string m_REH_PR_NO;
        private string m_REH_OLD_PR_NO;
        private DateTime m_REH_DATE;
        private DateTime m_REH_OLD_JOINING;
        private DateTime m_REH_OLD_CONFIRM;
        private DateTime m_REH_OLD_CONF_ON;
        private DateTime m_REH_OLD_TERMIN;
        private decimal m_REH_OLD_TRANS;
        private DateTime m_LC_DATE;
        private string m_LC_NAME;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "REH_PR_NO"
        public string REH_PR_NO
        {
            get { return m_REH_PR_NO; }
            set { m_REH_PR_NO = value; }
        }
        #endregion

        #region "REH_OLD_PR_NO"
        public string REH_OLD_PR_NO
        {
            get { return m_REH_OLD_PR_NO; }
            set { m_REH_OLD_PR_NO = value; }
        }
        #endregion

        #region "REH_DATE"
        public DateTime REH_DATE
        {
            get { return m_REH_DATE; }
            set { m_REH_DATE = value; }
        }
        #endregion

        #region "REH_OLD_JOINING"
        public DateTime REH_OLD_JOINING
        {
            get { return m_REH_OLD_JOINING; }
            set { m_REH_OLD_JOINING = value; }
        }
        #endregion

        #region "REH_OLD_CONFIRM"
        public DateTime REH_OLD_CONFIRM
        {
            get { return m_REH_OLD_CONFIRM; }
            set { m_REH_OLD_CONFIRM = value; }
        }
        #endregion

        #region "REH_OLD_CONF_ON"
        public DateTime REH_OLD_CONF_ON
        {
            get { return m_REH_OLD_CONF_ON; }
            set { m_REH_OLD_CONF_ON = value; }
        }
        #endregion

        #region "REH_OLD_TERMIN"
        public DateTime REH_OLD_TERMIN
        {
            get { return m_REH_OLD_TERMIN; }
            set { m_REH_OLD_TERMIN = value; }
        }
        #endregion

        #region "REH_OLD_TRANS"
        public decimal REH_OLD_TRANS
        {
            get { return m_REH_OLD_TRANS; }
            set { m_REH_OLD_TRANS = value; }
        }
        #endregion

        #region "LC_DATE"
        private DateTime LC_DATE
        {
            get { return m_LC_DATE; }
            set { m_LC_DATE = value; }
        }
        #endregion

        #region "LC_NAME"
        public String LC_NAME
        {
            get { return m_LC_NAME; }
            set { m_LC_NAME = value; }
        }
        #endregion

        #region "ID"
            public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _REH_PR_NO = "REH_PR_NO";
        public static readonly string _REH_OLD_PR_NO = "REH_OLD_PR_NO";
        public static readonly string _REH_DATE = "REH_DATE";
        public static readonly string _REH_OLD_JOINING = "REH_OLD_JOINING";
        public static readonly string _REH_OLD_CONFIRM = "REH_OLD_CONFIRM";
        public static readonly string _REH_OLD_CONF_ON = "REH_OLD_CONF_ON";
        public static readonly string _REH_OLD_TERMIN = "REH_OLD_TERMIN";
        public static readonly string _REH_OLD_TRANS = "REH_OLD_TRANS";
        public static readonly string _LC_DATE = "LC_DATE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
