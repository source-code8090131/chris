/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BRANCHCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "BRANCH"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	12/06/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class BranchCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_BRN_CODE;
        private String m_BRN_NAME;
        private String m_BRN_ADD1;
        private String m_BRN_ADD2;
        private String m_BRN_ADD3;
        private String m_BRN_CR_AC;
        private String m_BRN_DR_AC;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "BRN_CODE"
        public String BRN_CODE
        {
            get { return m_BRN_CODE; }
            set { m_BRN_CODE = value; }
        }
        #endregion

        #region "BRN_NAME"
        public String BRN_NAME
        {
            get { return m_BRN_NAME; }
            set { m_BRN_NAME = value; }
        }
        #endregion

        #region "BRN_ADD1"
        public String BRN_ADD1
        {
            get { return m_BRN_ADD1; }
            set { m_BRN_ADD1 = value; }
        }
        #endregion

        #region "BRN_ADD2"
        public String BRN_ADD2
        {
            get { return m_BRN_ADD2; }
            set { m_BRN_ADD2 = value; }
        }
        #endregion

        #region "BRN_ADD3"
        public String BRN_ADD3
        {
            get { return m_BRN_ADD3; }
            set { m_BRN_ADD3 = value; }
        }
        #endregion

        #region "BRN_CR_AC"
        public String BRN_CR_AC
        {
            get { return m_BRN_CR_AC; }
            set { m_BRN_CR_AC = value; }
        }
        #endregion

        #region "BRN_DR_AC"
        public String BRN_DR_AC
        {
            get { return m_BRN_DR_AC; }
            set { m_BRN_DR_AC = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _BRN_CODE = "BRN_CODE";
        public static readonly string _BRN_NAME = "BRN_NAME";
        public static readonly string _BRN_ADD1 = "BRN_ADD1";
        public static readonly string _BRN_ADD2 = "BRN_ADD2";
        public static readonly string _BRN_ADD3 = "BRN_ADD3";
        public static readonly string _BRN_CR_AC = "BRN_CR_AC";
        public static readonly string _BRN_DR_AC = "BRN_DR_AC";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}

