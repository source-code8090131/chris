#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    /* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHIRS.BUSINESSOBJECTS.ENTITIES.DepositRequest
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
    /// Business entity "UserNameSetupCommand"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Kiran]	11/16/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

    class UserNameSetupCommand : BusinessEntity
    {
        //-------------------------------------Start Code generation for Business-------------------------------------

	#region "Auto generated code for Business Entity"

	#region "--Field Segment--"
	
	 private String m_SOEID;
	 private String m_USER_NAME;
	 private int m_ID;
        
	#endregion "--Field Segment--"

        
	#region "--Property Segment--"

	
	#region "SOEID"
        public String SOEID 
	{
        get { return m_SOEID; }
        set { m_SOEID = value; }
	} 
	#endregion

	#region "USER_NAME"
	public String USER_NAME
	{ 
		get { return m_USER_NAME; }
		set { m_USER_NAME = value; }
	} 
	#endregion
        	
	#region "ID"
	public int ID
	{ 
		get { return m_ID; }
		set { m_ID = value; }
	} 
	#endregion

	#endregion --Public Properties--

        
	#region "--Column Mapping--"
	 public static readonly string _SOEID = "SOEID";
	 public static readonly string _USER_NAME = "USER_NAME";
	public static readonly string _ID = "ID";
	#endregion
        	#endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

	//----------------------Region to keep all customized business related logic----------------------------

	#region "--Customize Business Methods--"

	#endregion "Customize Business Function"

	//-----------------------------------Customized region ends here----------------------------------------
    }
}
 




	
	



 





	
		