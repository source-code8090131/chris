

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "TRANSFER"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	01/17/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PersonnelTransferSegmentCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_TR_NO;
        private decimal m_PR_TRANSFER_TYPE;
        private String m_PR_FUNC_1;
        private String m_PR_FUNC_2;
        private String m_PR_NEW_BRANCH;
        private String m_PR_FURLOUGH;
        private String m_PR_DESG;
        private String m_PR_LEVEL;
        private String m_PR_COUNTRY;
        private String m_PR_CITY;
        private String m_PR_DEPARTMENT_HC;
        private decimal m_PR_ASR_DOL;
        private Nullable<DateTime> m_PR_FAST_END_DATE;
        private String m_PR_IS_COORDINAT;
        private String m_PR_REMARKS;
        private DateTime m_PR_FAST_CONVER;
        private String m_PR_FLAG;
        private Nullable<DateTime> m_PR_EFFECTIVE;
        private decimal m_PR_RENT;
        private decimal m_PR_UTILITIES;
        private decimal m_PR_TAX_ON_TAX;
        private String m_PR_OLD_BRANCH;
        private String m_CITI_FLAG1;
        private String m_CITI_FLAG2;
        private decimal m_PR_NEW_ASR;
        private String m_CURRENCY_TYPE;
        private Double m_W_REP;
        private String m_W_GOAL;
        private Nullable<DateTime> m_W_GOAL_DATE;
        private Nullable<DateTime> m_PR_JOINING_DATE;
        private Nullable<DateTime> m_PR_TERMIN_DATE;
        private Nullable<DateTime> m_PR_TRANSFER_DATE;
        private int m_ID;
        private decimal m_PR_TRANSFER;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_TR_NO"
        public Double PR_TR_NO
        {
            get { return m_PR_TR_NO; }
            set { m_PR_TR_NO = value; }
        }
        #endregion

        #region "PR_TRANSFER_TYPE"
        public decimal PR_TRANSFER_TYPE
        {
            get { return m_PR_TRANSFER_TYPE; }
            set { m_PR_TRANSFER_TYPE = value; }
        }
        #endregion

        #region "PR_FUNC_1"
        public String PR_FUNC_1
        {
            get { return m_PR_FUNC_1; }
            set { m_PR_FUNC_1 = value; }
        }
        #endregion

        #region "PR_FUNC_2"
        public String PR_FUNC_2
        {
            get { return m_PR_FUNC_2; }
            set { m_PR_FUNC_2 = value; }
        }
        #endregion

        #region "PR_NEW_BRANCH"
        public String PR_NEW_BRANCH
        {
            get { return m_PR_NEW_BRANCH; }
            set { m_PR_NEW_BRANCH = value; }
        }
        #endregion

        #region "PR_FURLOUGH"
        public String PR_FURLOUGH
        {
            get { return m_PR_FURLOUGH; }
            set { m_PR_FURLOUGH = value; }
        }
        #endregion

        #region "PR_DESG"
        public String PR_DESG
        {
            get { return m_PR_DESG; }
            set { m_PR_DESG = value; }
        }
        #endregion

        #region "PR_LEVEL"
        public String PR_LEVEL
        {
            get { return m_PR_LEVEL; }
            set { m_PR_LEVEL = value; }
        }
        #endregion

        #region "PR_COUNTRY"
        public String PR_COUNTRY
        {
            get { return m_PR_COUNTRY; }
            set { m_PR_COUNTRY = value; }
        }
        #endregion

        #region "PR_CITY"
        public String PR_CITY
        {
            get { return m_PR_CITY; }
            set { m_PR_CITY = value; }
        }
        #endregion

        #region "PR_DEPARTMENT_HC"
        public String PR_DEPARTMENT_HC
        {
            get { return m_PR_DEPARTMENT_HC; }
            set { m_PR_DEPARTMENT_HC = value; }
        }
        #endregion

        #region "PR_ASR_DOL"
        public decimal PR_ASR_DOL
        {
            get { return m_PR_ASR_DOL; }
            set { m_PR_ASR_DOL = value; }
        }
        #endregion

        #region "PR_FAST_END_DATE"
        public Nullable<DateTime> PR_FAST_END_DATE
        {
            get { return m_PR_FAST_END_DATE; }
            set { m_PR_FAST_END_DATE = value; }
        }
        #endregion

        #region "PR_IS_COORDINAT"
        public String PR_IS_COORDINAT
        {
            get { return m_PR_IS_COORDINAT; }
            set { m_PR_IS_COORDINAT = value; }
        }
        #endregion

        #region "PR_REMARKS"
        public String PR_REMARKS
        {
            get { return m_PR_REMARKS; }
            set { m_PR_REMARKS = value; }
        }
        #endregion

        #region "PR_FAST_CONVER"
        public DateTime PR_FAST_CONVER
        {
            get { return m_PR_FAST_CONVER; }
            set { m_PR_FAST_CONVER = value; }
        }
        #endregion

        #region "PR_FLAG"
        public String PR_FLAG
        {
            get { return m_PR_FLAG; }
            set { m_PR_FLAG = value; }
        }
        #endregion

        #region "PR_EFFECTIVE"
        public Nullable<DateTime> PR_EFFECTIVE
        {
            get { return m_PR_EFFECTIVE; }
            set { m_PR_EFFECTIVE = value; }
        }
        #endregion

        #region "PR_RENT"
        public decimal PR_RENT
        {
            get { return m_PR_RENT; }
            set { m_PR_RENT = value; }
        }
        #endregion

        #region "PR_UTILITIES"
        public decimal PR_UTILITIES
        {
            get { return m_PR_UTILITIES; }
            set { m_PR_UTILITIES = value; }
        }
        #endregion

        #region "PR_TAX_ON_TAX"
        public decimal PR_TAX_ON_TAX
        {
            get { return m_PR_TAX_ON_TAX; }
            set { m_PR_TAX_ON_TAX = value; }
        }
        #endregion

        #region "PR_OLD_BRANCH"
        public String PR_OLD_BRANCH
        {
            get { return m_PR_OLD_BRANCH; }
            set { m_PR_OLD_BRANCH = value; }
        }
        #endregion

        #region "CITI_FLAG1"
        public String CITI_FLAG1
        {
            get { return m_CITI_FLAG1; }
            set { m_CITI_FLAG1 = value; }
        }
        #endregion

        #region "CITI_FLAG2"
        public String CITI_FLAG2
        {
            get { return m_CITI_FLAG2; }
            set { m_CITI_FLAG2 = value; }
        }
        #endregion

        #region "PR_NEW_ASR"
        public decimal PR_NEW_ASR
        {
            get { return m_PR_NEW_ASR; }
            set { m_PR_NEW_ASR = value; }
        }
        #endregion

        #region "CURRENCY_TYPE"
        public String CURRENCY_TYPE
        {
            get { return m_CURRENCY_TYPE; }
            set { m_CURRENCY_TYPE = value; }
        }
        #endregion

        #region "W_GOAL_DATE"
        public Nullable<DateTime> W_GOAL_DATE
        {
            get { return m_W_GOAL_DATE; }
            set { m_W_GOAL_DATE = value; }
        }
        #endregion

        #region "W_REP"
        public Double W_REP
        {
            get { return m_W_REP; }
            set { m_W_REP = value; }
        }
        #endregion

        #region "W_GOAL"
        public String W_GOAL
        {
            get { return m_W_GOAL; }
            set { m_W_GOAL = value; }
        }
        #endregion

        #region "PR_JOINING_DATE"
        public Nullable<DateTime> PR_JOINING_DATE
        {
            get { return m_PR_JOINING_DATE; }
            set { m_PR_JOINING_DATE = value; }
        }
        #endregion

        #region "PR_TERMIN_DATE"
        public Nullable<DateTime> PR_TERMIN_DATE
        {
            get { return m_PR_TERMIN_DATE; }
            set { m_PR_TERMIN_DATE = value; }
        }
         #endregion

        #region "PR_TRANSFER_DATE"
        public Nullable<DateTime> PR_TRANSFER_DATE
        {
            get { return m_PR_TRANSFER_DATE; }
            set { m_PR_TRANSFER_DATE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #region "PR_TRANSFER"
        public decimal PR_TRANSFER
        {
            get { return m_PR_TRANSFER; }
            set { m_PR_TRANSFER = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_TR_NO = "PR_TR_NO";
        public static readonly string _PR_TRANSFER_TYPE = "PR_TRANSFER_TYPE";
        public static readonly string _PR_FUNC_1 = "PR_FUNC_1";
        public static readonly string _PR_FUNC_2 = "PR_FUNC_2";
        public static readonly string _PR_NEW_BRANCH = "PR_NEW_BRANCH";
        public static readonly string _PR_FURLOUGH = "PR_FURLOUGH";
        public static readonly string _PR_DESG = "PR_DESG";
        public static readonly string _PR_LEVEL = "PR_LEVEL";
        public static readonly string _PR_COUNTRY = "PR_COUNTRY";
        public static readonly string _PR_CITY = "PR_CITY";
        public static readonly string _PR_DEPARTMENT_HC = "PR_DEPARTMENT_HC";
        public static readonly string _PR_ASR_DOL = "PR_ASR_DOL";
        public static readonly string _PR_FAST_END_DATE = "PR_FAST_END_DATE";
        public static readonly string _PR_IS_COORDINAT = "PR_IS_COORDINAT";
        public static readonly string _PR_REMARKS = "PR_REMARKS";
        public static readonly string _PR_FAST_CONVER = "PR_FAST_CONVER";
        public static readonly string _PR_FLAG = "PR_FLAG";
        public static readonly string _PR_EFFECTIVE = "PR_EFFECTIVE";
        public static readonly string _PR_RENT = "PR_RENT";
        public static readonly string _PR_UTILITIES = "PR_UTILITIES";
        public static readonly string _PR_TAX_ON_TAX = "PR_TAX_ON_TAX";
        public static readonly string _PR_OLD_BRANCH = "PR_OLD_BRANCH";
        public static readonly string _CITI_FLAG1 = "CITI_FLAG1";
        public static readonly string _CITI_FLAG2 = "CITI_FLAG2";
        public static readonly string _PR_NEW_ASR = "PR_NEW_ASR";
        public static readonly string _CURRENCY_TYPE = "CURRENCY_TYPE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
