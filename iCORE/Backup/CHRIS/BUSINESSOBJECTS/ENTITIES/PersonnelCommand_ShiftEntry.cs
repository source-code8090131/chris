using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    class PersonnelCommand_ShiftEntry : BusinessEntity
    {

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_OT_P_NO;
        private DateTime m_OT_DATE_FILTER;
        private String m_PR_BRANCH;
        private String m_PR_FIRST_NAME;
        private String m_PR_DESIG;
        private String m_PR_LEVEL;
        private String m_PR_CATEGORY;
        private DateTime m_PR_PROMOTION_DATE;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "OT_P_NO"
        public Double OT_P_NO
        {
            get { return m_OT_P_NO; }
            set { m_OT_P_NO = value; }
        }
        #endregion

        #region "OT_DATE_FILTER"
        public DateTime OT_DATE_FILTER
        {
            get { return m_OT_DATE_FILTER; }
            set { m_OT_DATE_FILTER = value; }
        }
        #endregion

        #region "PR_BRANCH"
        public String PR_BRANCH
        {
            get { return m_PR_BRANCH; }
            set { m_PR_BRANCH = value; }
        }
        #endregion

        #region "PR_FIRST_NAME"
        public String PR_FIRST_NAME
        {
            get { return m_PR_FIRST_NAME; }
            set { m_PR_FIRST_NAME = value; }
        }
        #endregion


        #region "PR_DESIG"
        public String PR_DESIG
        {
            get { return m_PR_DESIG; }
            set { m_PR_DESIG = value; }
        }
        #endregion

        #region "PR_LEVEL"
        public String PR_LEVEL
        {
            get { return m_PR_LEVEL; }
            set { m_PR_LEVEL = value; }
        }
        #endregion

        #region "PR_CATEGORY"
        public String PR_CATEGORY
        {
            get { return m_PR_CATEGORY; }
            set { m_PR_CATEGORY = value; }
        }
        #endregion

        #region "PR_PROMOTION_DATE"
        public DateTime PR_PROMOTION_DATE
        {
            get { return m_PR_PROMOTION_DATE; }
            set { m_PR_PROMOTION_DATE = value; }
        }
        #endregion


        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _OT_P_NO = "OT_P_NO";
        public static readonly string _OT_DATE = "OT_DATE_FILTER";
        public static readonly string _PR_BRANCH = "PR_BRANCH";
        public static readonly string _PR_FIRST_NAME = "PR_FIRST_NAME";
        public static readonly string _PR_DESIG = "PR_DESIG";
        public static readonly string _PR_LEVEL = "PR_LEVEL";
        public static readonly string _PR_CATEGORY = "PR_CATEGORY";
        public static readonly string _PR_PROMOTION_DATE = "PR_PROMOTION_DATE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

    }
}
