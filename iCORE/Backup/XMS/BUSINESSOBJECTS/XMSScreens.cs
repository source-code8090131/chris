// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL

using System;
using System.Collections.Generic;
using System.Text;

namespace iCORE.XMS.BUSINESSOBJECTS
{
    public class XMSScreens
    {
        private string functionid;    // maintained at both xms and icore level
        private string functionshort; // maintained at both xms and icore level
        private string functioname;   // maintained at both xms and icore level
        private string objectname;    // maintained at icore level (Actual Screen Name)

        public void setFunctionId(string functionid)
        {
            this.functionid = functionid;
        }
        public void setFunctionShort(string functionshort)
        {
            this.functionshort = functionshort;
        }
        public void setFunctionName(string functioname)
        {
            this.functioname = functioname;
        }
        public void setObjectName(string objectname)
        {
            this.objectname = objectname;
        }

        public string getFunctionId()
        {
            return this.functionid;
        }
        public string getFunctionShort()
        {
            return this.functionshort;
        }
        public string getFunctionName()
        {
            return this.functioname;
        }
        public string getObjectName()
        {
            return this.objectname;
        }
    }
}
