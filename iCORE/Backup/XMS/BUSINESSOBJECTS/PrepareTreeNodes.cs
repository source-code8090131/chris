// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL



using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.SqlClient;

namespace iCORE.XMS.BUSINESSOBJECTS
{
    public class PrepareTreeNodes
    {

        XMS.DATAOBJECTS.ConnectionBean xmsconnbean = null;
        BUSINESSOBJECTS.XMSUser xmsuser = null;
        BUSINESSOBJECTS.XMSScreens xmsscreens = null;

        public SortedList modulecontainer_hash = null;
        public SortedList screenobjects_hash = null;

        // new code starts here 
        // public SortedList submodule_hash = null;

        public PrepareTreeNodes(XMS.DATAOBJECTS.ConnectionBean xmsconbeanobj, BUSINESSOBJECTS.XMSUser xmsuser)
        {
            this.xmsconnbean = xmsconbeanobj;
            this.xmsuser = xmsuser;
            modulecontainer_hash = new SortedList();
            screenobjects_hash = new SortedList();

            // submodule_hash = new SortedList();
        }
        public void populateTree()
        {
            // XMS.DATAOBJECTS.ConnectionBean connbean = new XMS.DATAOBJECTS.ConnectionBean("xms","sb13446","sb13446");
            SqlConnection sqlconn = this.xmsconnbean.getDatabaseConnection();
            string functionid = null;
            string strsql = null;
            SqlDataReader reader = null;
            string functionshort = null; string functionname = null; string objectname = null;
            string ModuleName = null; string ScreenType = null; string ScreenName = null;

            SortedList hashscreens = xmsuser.getHashScreens();
            IEnumerator ie = hashscreens.Values.GetEnumerator();
            while (ie.MoveNext() && ie.Current != null)
            {
                xmsscreens = (BUSINESSOBJECTS.XMSScreens)ie.Current;
                functionid = xmsscreens.getFunctionId();

                // below query is commented and replaced wita a query which works well
                // for both (Menu - SubMenu-Screen) and (Menu-ScreenType-Screen)
                //strsql = "select objectname,charindex('_',objectname)," +
                //         "substring(objectname,1,charindex('_',objectname)-1) as ModuleName," +
                //         " Case substring(objectname,charindex('_',objectname)+1,1) " +
                //         " when  'F' Then 'Forms' " +
                //         " else 'Reports' end as ScreenType, " +
                //         "substring(objectname,charindex('_',objectname)+3,len(objectname)) as ScreenName " +
                //         "from xms.dbo.xms_app_functions_chris where functionid = " + Int32.Parse(functionid);

                // Orginal Tree Population logic
                //strsql = "select objectname as objectname,charindex('_',objectname)," +
                //         "substring(objectname,1,charindex('_',objectname)-1) as ModuleName," +
                //         "case substring(objectname,charindex('_',objectname)+1,charindex('_',substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)))-1) " +
                //         "when  'F' Then 'Forms' " +
                //         "when  'R' Then 'Reports' " +
                //         "else substring(objectname,charindex('_',objectname)+1,charindex('_',substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)))-1) end as ScreenType," +
                //         "substring(substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)),(charindex('_',substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)))+1),(len(substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)))-3)+2) as ScreenName " +
                //         "from " + Properties.Settings.Default.MenuTable + " where functionid = " + Int32.Parse(functionid);

                // Modified Tree Population logic
                strsql = "select objectname as objectname,charindex('_',objectname)," +
                         "substring(objectname,1,charindex('_',objectname)-1) as ModuleName," +
                         "case substring(objectname,charindex('_',objectname)+1,charindex('_',substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)))-1) " +
                         "when  'F' Then 'Forms' " +
                         "when  'R' Then 'Reports' " +
                         "else substring(objectname,charindex('_',objectname)+1,charindex('_',substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)))-1) end as ScreenType," +
                         "screen_description as ScreenName " +
                         "from " + Properties.Settings.Default.MenuTable + " where functionid = " + Int32.Parse(functionid);

                reader = this.xmsconnbean.getData(strsql, sqlconn);
                if (reader.Read())
                {
                    objectname = (string)reader["objectname"];
                    ModuleName = (string)reader["ModuleName"];
                    ScreenType = (string)reader["ScreenType"];
                    ScreenName = (string)reader["ScreenName"];

                    // below if else block is reponsible for maintaining module collection
                    // to be used for populating 1st branch of tree
                    // do not check for duplication simply add the module name
                    if (modulecontainer_hash.Count == 0)
                    {
                        modulecontainer_hash.Add(functionid, ModuleName);
                    }
                    // check for duplication and then add the module
                    else
                    {
                        // if module already exists do not add to node 
                        if (modulecontainer_hash.ContainsValue(ModuleName))
                        {
                            // do nothing as node already exists
                        }
                        else
                        {
                            //add the modulecontainer_hash to node
                            modulecontainer_hash.Add(functionid, ModuleName);
                        }
                    }


                    //below block is responsible for maintaining screen collection
                    //to be used for populating 3rd branch of tree
                    BUSINESSOBJECTS.ScreenNode screennode = new BUSINESSOBJECTS.ScreenNode();
                    screennode.setFunctionId(functionid);
                    screennode.setObjectName(objectname);
                    screennode.setModuleName(ModuleName);
                    screennode.setScreenType(ScreenType);
                    screennode.setScreenName(ScreenName);
                    screenobjects_hash.Add(functionid, screennode);

                }
                this.xmsconnbean.closeReader(reader);
            }

            // do not close the connection here it might be used in main menu
            // this.xmsconnbean.closeConn(sqlconn);

        }

    }
}
