// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL

using System;
using System.Collections.Generic;
using System.Text;
using Citigroup.SST.XMS;


namespace iCORE.XMS.BUSINESSOBJECTS
{
    class XMSUser_ServiceBean
    {
        BUSINESSOBJECTS.XMSUser xmsuser = null;
        public BUSINESSOBJECTS.XMSUser verifyXMSUserService(PRESENTATIONOBJECTS.FORMBEANS.Login_FormBean formbeanobj)
        {
            XMSVerifyUser verifyuserobj = new XMSVerifyUser();
            string ret_str = verifyuserobj.VerifyUser(formbeanobj);
            xmsuser = verifyuserobj.getXMSUser();
            return xmsuser;
        }


    }
}
