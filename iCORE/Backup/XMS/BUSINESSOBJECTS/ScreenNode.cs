// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL



using System;
using System.Collections.Generic;
using System.Text;

namespace iCORE.XMS.BUSINESSOBJECTS
{
    public class ScreenNode
    {
        private string objectname = null;
        private string modulename = null;
        private string screentype = null;
        private string screenname = null;
        private string functionid = null;

        public void setObjectName(string objectname)
        {
            this.objectname = objectname;
        }
        public void setModuleName(string modulename)
        {
            this.modulename = modulename;
        }
        public void setScreenType(string screentype)
        {
            this.screentype = screentype;
        }
        public void setScreenName(string screenname)
        {
            this.screenname = screenname;
        }
        public void setFunctionId(string functionid)
        {
            this.functionid = functionid;
        }


        public string getObjectName()
        {
            return this.objectname;
        }
        public string getModuleName()
        {
            return this.modulename;
        }
        public string getScreenType()
        {
            return this.screentype;
        }
        public string getScreenName()
        {
            return this.screenname;
        }
        public string getFunctionId()
        {
            return this.functionid;
        }

    }
}
