// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Reflection;

namespace iCORE.XMS.BUSINESSOBJECTS
{
    public class ScreensCollection
    {
        SortedList RunningFormsCollection_hash = null;
        //public System.Windows.Forms.Form FormCollection = null;
        //MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_CreateBroker CreateBroker_obj;
        //MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_CreateTenor CreateTenor_obj;




        public ScreensCollection()
        {
            RunningFormsCollection_hash = new SortedList();
        }
        /// <summary>
        /// this function is use to create form and report object on run time
        /// </summary>
        /// <param name="screenid"></param>
        /// <param name="businessgroupid"></param>
        /// <returns></returns>
        public string openScreen(string screenid, PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, ScreensCollection screenobj, XMS.DATAOBJECTS.ConnectionBean conbean)
        {
            string formName = null;
            string modulName = null;
            string screenName = null;
            string submodulename = null;
            string finalObjectName = null;
            CreateFormObject formobj = new CreateFormObject(conbean);

            // generic code for (Menu-SubMenu-Screen)
            formobj.setScreenDetails(screenid);
            formName = formobj.getObjectName();
            modulName = formobj.getModuleName();
            submodulename = formobj.getSubModuleName();
            finalObjectName = "iCORE." + modulName + ".PRESENTATIONOBJECTS." + submodulename + "." + formName;


            //formName = formobj.FormName(screenid);
            //modulName = formobj.ModulName(screenid);
            //screenName = formobj.ScreenType(screenid);
            //if (screenName == "Forms")
            //{
            //    finalObjectName = "iCORE." + modulName + ".PRESENTATIONOBJECTS.FORMS." + formName;
            //}
            //else if (screenName == "Reports")
            //{
            //    finalObjectName = "iCORE." + modulName + ".PRESENTATIONOBJECTS.REPORTS." + formName;
            //}






            //finalObjectName = "iCORE.CBG.PRESENTATIONOBJECTS.FORMS.CBG_F_Main";
            if (finalObjectName.Length > 0)
            {
                Type strType = this.GetType().Assembly.GetType(finalObjectName);
                MethodInfo methobj = strType.GetMethod("setScreenCollection");
                // object for screen collection maintance
                object[] objmethod = new object[2];
                objmethod[0] = screenobj;
                objmethod[1] = screenid;
                // object for contructor
                object[] obj = new object[2];
                obj[0] = mainmenu;
                obj[1] = conbean;
                object createFormobj = Activator.CreateInstance(strType, obj);
                methobj.Invoke(createFormobj, objmethod);
                System.Windows.Forms.Form Formobj = (System.Windows.Forms.Form)createFormobj;
                Formobj.Show();

            }
            return screenid;
        }


        //        public string openScreen(string screenid, PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, ScreensCollection screenobj, XMS.DATAOBJECTS.ConnectionBean conbean)
        //        {
        //            #region MMRRFormName

        //            if (screenid.Equals("7"))
        //            {
        //                CreateBroker_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_CreateBroker(mainmenu, conbean);
        //                CreateBroker_obj.setScreenCollection(screenobj, screenid);
        //                //FormCollection = CreateBroker_obj;
        //                CreateBroker_obj.Show();

        //            }
        //            else if (screenid.Equals("8"))
        //            {
        //                CreateTenor_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_CreateTenor(mainmenu, conbean);
        //                CreateTenor_obj.setScreenCollection(screenobj, screenid);
        //                //CreateBroker_obj.Hide();
        //                CreateTenor_obj.Show();
        //            }
        //            else if (screenid.Equals("1"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.FORMS.txtReportName CreateBrokerCallRepoRate_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.FORMS.txtReportName(mainmenu, conbean);
        //                CreateBrokerCallRepoRate_obj.setScreenCollection(screenobj, screenid);
        //                CreateBrokerCallRepoRate_obj.Show();
        //            }
        //            else if (screenid.Equals("2"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_Create_PIB_Outright_Transaction CreatePibOutrightTans_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_Create_PIB_Outright_Transaction(mainmenu, conbean);
        //                CreatePibOutrightTans_obj.setScreenCollection(screenobj, screenid);
        //                CreatePibOutrightTans_obj.Show();
        //            }
        //            else if (screenid.Equals("3"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_CreateFwdCallTbillsPibRepoTransaction CreateFwdCallTBPIBRepoTrans_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_CreateFwdCallTbillsPibRepoTransaction(mainmenu, conbean);
        //                CreateFwdCallTBPIBRepoTrans_obj.setScreenCollection(screenobj, screenid);
        //                CreateFwdCallTBPIBRepoTrans_obj.Show();
        //            }
        //            else if (screenid.Equals("4"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_PrepareExceptionReport ExceptionRpt_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_PrepareExceptionReport(mainmenu, conbean);
        //                ExceptionRpt_obj.setScreenCollection(screenobj, screenid);
        //                ExceptionRpt_obj.Show();
        //            }
        //            else if (screenid.Equals("5"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.FORMS.UpdateCallRepoRates UpdateCallRepoRate_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.FORMS.UpdateCallRepoRates(mainmenu, conbean);
        //                UpdateCallRepoRate_obj.setScreenCollection(screenobj, screenid);
        //                UpdateCallRepoRate_obj.Show();
        //            }
        //            else if (screenid.Equals("6"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_Update_PIB_TBills_Outright_Transaction UpdateCallRepoRate_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_Update_PIB_TBills_Outright_Transaction(mainmenu, conbean);
        //                UpdateCallRepoRate_obj.setScreenCollection(screenobj, screenid);
        //                UpdateCallRepoRate_obj.Show();
        //            }

        //            #endregion
        //            #region MMRRReportsName
        //            else if (screenid.Equals("12"))
        //            {
        //                // Verify it
        //                MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayAverageCallRepoBidOfferRateDeals AvgCallRepoBidOfferRateDeal_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayAverageCallRepoBidOfferRateDeals(mainmenu, conbean);
        //                AvgCallRepoBidOfferRateDeal_obj.setScreenCollection(screenobj, screenid);
        //                AvgCallRepoBidOfferRateDeal_obj.Show();
        //            }
        //            else if (screenid.Equals("11"))
        //            {
        //                // Verify it
        //                MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayBrokerCallRepoRate BrokerCallRepoRate_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayBrokerCallRepoRate(mainmenu, conbean);
        //                BrokerCallRepoRate_obj.setScreenCollection(screenobj, screenid);
        //                BrokerCallRepoRate_obj.Show();
        //            }
        //            else if (screenid.Equals("9"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayMainSheetReport MainSheet_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayMainSheetReport(mainmenu, conbean);
        //                MainSheet_obj.setScreenCollection(screenobj, screenid);
        //                MainSheet_obj.Show();

        //            }
        //            else if (screenid.Equals("13"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayFWDCallRepoOutrightPIBTBills FwdCall_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayFWDCallRepoOutrightPIBTBills(mainmenu, conbean);
        //                FwdCall_obj.setScreenCollection(screenobj, screenid);
        //                FwdCall_obj.Show();

        //            }
        //            else if (screenid.Equals("14"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayTreasuryRpt Treasury_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayTreasuryRpt(mainmenu, conbean);
        //                Treasury_obj.setScreenCollection(screenobj, screenid);
        //                Treasury_obj.Show();
        //            }
        //            else if (screenid.Equals("15"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayNewSheetFXRR NewSheetFXRR_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayNewSheetFXRR(mainmenu, conbean);
        //                NewSheetFXRR_obj.setScreenCollection(screenobj, screenid);
        //                NewSheetFXRR_obj.Show();
        //            }
        //            else if (screenid.Equals("10"))
        //            {
        //                MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayExceptionReport Exception_obj = new iCORE.MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayExceptionReport(mainmenu, conbean);
        //                Exception_obj.setScreenCollection(screenobj, screenid);
        //                Exception_obj.Show();
        //            }
        //            else if (screenid.Equals("17"))
        //            {
        //                CBG.PRESENTATIONOBJECTS.FORMS.CRM_F_Item_Maintenance cim_obj = new iCORE.CBG.PRESENTATIONOBJECTS.FORMS.CRM_F_Item_Maintenance(mainmenu, conbean);
        //                cim_obj.setScreenCollection(screenobj, screenid);
        //                cim_obj.Show();

        //            }
        //            else if (screenid.Equals("19"))
        //            {
        //                CBG.PRESENTATIONOBJECTS.FORMS.CRM_F_Set_Info csi_obj = new iCORE.CBG.PRESENTATIONOBJECTS.FORMS.CRM_F_Set_Info(mainmenu, conbean);
        //                csi_obj.setScreenCollection(screenobj, screenid);
        //                csi_obj.Show();

        //            }
        //            #endregion
        //            #region CBGFormName
        //            else if (screenid.Equals("20"))
        //            {
        //                CBG.PRESENTATIONOBJECTS.FORMS.CRM_F_Set_Terms cst_obj = new iCORE.CBG.PRESENTATIONOBJECTS.FORMS.CRM_F_Set_Terms(mainmenu, conbean);
        //                cst_obj.setScreenCollection(screenobj, screenid);
        //                cst_obj.Show();

        //            }
        //            else if (screenid.Equals("17"))
        //            {
        //                CBG.PRESENTATIONOBJECTS.FORMS.CRM_F_Main cm_obj = new iCORE.CBG.PRESENTATIONOBJECTS.FORMS.CRM_F_Main(mainmenu, conbean);
        //                cm_obj.setScreenCollection(screenobj, screenid);
        //                cm_obj.Show();

        //            }

        //#endregion


        //            #region MVAFormName

        //            else if (screenid.Equals("23"))
        //            {
        //                MVA.PRESENTATIONOBJECTS.FORMS.MVA_F_CreateMasterReport CreateMasterReport_obj = new iCORE.MVA.PRESENTATIONOBJECTS.FORMS.MVA_F_CreateMasterReport(mainmenu, conbean);
        //                CreateMasterReport_obj.setScreenCollection(screenobj, screenid);
        //                CreateMasterReport_obj.Show();

        //            }
        //            else if (screenid.Equals("24"))
        //            {
        //                MVA.PRESENTATIONOBJECTS.FORMS.MVA_F_FXPortfolio fxportfolio_obj = new iCORE.MVA.PRESENTATIONOBJECTS.FORMS.MVA_F_FXPortfolio(mainmenu, conbean);
        //                fxportfolio_obj.setScreenCollection(screenobj, screenid);
        //                fxportfolio_obj.Show();

        //            }

        //            else if (screenid.Equals("26"))
        //            {
        //                MVA.PRESENTATIONOBJECTS.FORMS.MVA_F_IrsPortfolio irsportfolio_obj = new iCORE.MVA.PRESENTATIONOBJECTS.FORMS.MVA_F_IrsPortfolio(mainmenu, conbean);
        //                irsportfolio_obj.setScreenCollection(screenobj, screenid);
        //                irsportfolio_obj.Show();

        //            }

        //            else if (screenid.Equals("27"))
        //            {
        //                MVA.PRESENTATIONOBJECTS.FORMS.MVA_F_CreatePolicy createpolicy_obj = new iCORE.MVA.PRESENTATIONOBJECTS.FORMS.MVA_F_CreatePolicy(mainmenu, conbean);
        //                createpolicy_obj.setScreenCollection(screenobj, screenid);
        //                createpolicy_obj.Show();

        //            }

        //            #endregion

        //            return screenid;
        //        }

        public void addtoRunningFormsCollection(string screenid)
        {

            RunningFormsCollection_hash.Add(screenid, screenid);

        }
        public bool checkFormInstance(string screenid)
        {
            bool form_check_flag = false;
            try
            {
                form_check_flag = RunningFormsCollection_hash.ContainsKey(screenid);
            }
            catch (Exception e)
            {

                // ("Exception Encountered in Screen Collection " + e.Message);
            }





            return form_check_flag;
        }
        public void removefromRunningFormsCollection(string screenid)
        {
            RunningFormsCollection_hash.Remove(screenid);
        }

    }
}
