using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace iCORE.XMS.BUSINESSOBJECTS
{
    public class CreateFormObject
    {
        XMS.DATAOBJECTS.ConnectionBean conbean_obj = null;
        string objectname = null;
        string ModuleName = null;
        string SubModuleName = null;
        string ScreenName = null;

        public CreateFormObject(XMS.DATAOBJECTS.ConnectionBean conbean)
        {
            this.conbean_obj = conbean;
        }
        /// <summary>
        /// this function return form and report name
        /// </summary>
        /// <param name="screenid"></param>
        /// <returns></returns>
        public string FormName(string screenid)
        {
            SqlConnection sqlconn = conbean_obj.getDatabaseConnection();
            string formName = null;
            string selectAppFunctionName = " Select objectname as FormName " +
                                          " From  " + Properties.Settings.Default.MenuTable + 
                                          " Where functionid = " + screenid + "";

            SqlDataReader AppFunctionNameReader = conbean_obj.getData(selectAppFunctionName, sqlconn);
            if (AppFunctionNameReader.Read())
            {
                formName = AppFunctionNameReader["FormName"].ToString();
                conbean_obj.closeReader(AppFunctionNameReader);
            }

            return formName;

        }
        /// <summary>
        /// this function return application module name
        /// </summary>
        /// <param name="screenid"></param>
        /// <returns></returns>
        public string ModulName(string screenid)
        {
            SqlConnection sqlconn = conbean_obj.getDatabaseConnection();
            //int functionId = Int32.Parse(screenid);
            string modulName = null;

            string selectAppModulName = " Select substring(objectname,1,charindex('_',objectname)-1) as ModuleName " +
                                        " From  " + Properties.Settings.Default.MenuTable +
                                        " Where functionid = " + screenid + "";

            SqlDataReader AppModulNameReader = conbean_obj.getData(selectAppModulName, sqlconn);
            if (AppModulNameReader.Read())
            {
                modulName = AppModulNameReader["ModuleName"].ToString();
                conbean_obj.closeReader(AppModulNameReader);
            }

            return modulName;
        }
        /// <summary>
        /// this function return module screen type form or report
        /// </summary>
        /// <param name="screenid"></param>
        /// <returns></returns>
        public string ScreenType(string screenid)
        {
            SqlConnection sqlconn = conbean_obj.getDatabaseConnection();
            //int functionId = Int32.Parse(screenid);
            string screenName = null;

            string selectAppScreenName = " Select Case substring(objectname,charindex('_',objectname)+1,1) " +
                                        " When  'F' Then 'Forms' " +
                                        " else 'Reports' end as ScreenType " +
                                        " From  " + Properties.Settings.Default.MenuTable +
                                        " Where functionid = " + screenid + "";

            SqlDataReader AppScreenNameReader = conbean_obj.getData(selectAppScreenName, sqlconn);
            if (AppScreenNameReader.Read())
            {
                screenName = AppScreenNameReader["ScreenType"].ToString();
                conbean_obj.closeReader(AppScreenNameReader);
            }

            return screenName;


        }


        public void setScreenDetails(string screenid)
        {
            SqlConnection sqlconn = conbean_obj.getDatabaseConnection();
            string strsql = "select objectname as objectname,charindex('_',objectname)," +
                         "substring(objectname,1,charindex('_',objectname)-1) as ModuleName," +
                         "case substring(objectname,charindex('_',objectname)+1,charindex('_',substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)))-1) " +
                         "when  'F' Then 'FORMS' " +
                         "when  'R' Then 'REPORTS' " +
                         "else substring(objectname,charindex('_',objectname)+1,charindex('_',substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)))-1) end as ScreenType," +
                         "substring(substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)),(charindex('_',substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)))+1),(len(substring(objectname,charindex('_',objectname)+1,(len(objectname)+1)- charindex('_',objectname)))-3)+2) as ScreenName " +
                         "from  " + Properties.Settings.Default.MenuTable + "  where functionid = " + Int32.Parse(screenid);


            SqlDataReader reader = this.conbean_obj.getData(strsql, sqlconn);
            if (reader.Read())
            {
                objectname = (string)reader["objectname"];
                ModuleName = (string)reader["ModuleName"];
                SubModuleName = (string)reader["ScreenType"];
                ScreenName = (string)reader["ScreenName"];
            }
            conbean_obj.closeReader(reader);
            conbean_obj.closeConn(sqlconn);
        }

        public string getObjectName()
        {
            return this.objectname;
        }
        public string getModuleName()
        {
            return this.ModuleName;
        }
        public string getSubModuleName()
        {
            return this.SubModuleName;
        }
        public string getScreenName()
        {
            return this.ScreenName;
        }


    }
}
