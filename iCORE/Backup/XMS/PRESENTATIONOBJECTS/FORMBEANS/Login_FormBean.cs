// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL


using System;
using System.Collections.Generic;
using System.Text;

namespace iCORE.XMS.PRESENTATIONOBJECTS.FORMBEANS
{
    public class Login_FormBean
    {
        private string soeid;
        private string soepassword;

        public void setSoeId(string soeid)
        {
            this.soeid = soeid;
        }
        public void setSoePassword(string soepassword)
        {
            this.soepassword = soepassword;
        }

        public string getSoeId()
        {
            return this.soeid;
        }
        public string getSoePassword()
        {
            return this.soepassword;
        }

    }
}
