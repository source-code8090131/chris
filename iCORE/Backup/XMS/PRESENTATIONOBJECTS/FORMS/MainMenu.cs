// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using Citigroup.SST.XMS;
using System.IO;

namespace iCORE.XMS.PRESENTATIONOBJECTS.FORMS
{
    public partial class MainMenu : Form, IMessageFilter
    {
        BUSINESSOBJECTS.XMSScreens xmsscreens = null;
        BUSINESSOBJECTS.XMSUser xmsuser = null;

        BUSINESSOBJECTS.ScreensCollection screen_collection_obj = null;

        XMS.DATAOBJECTS.ConnectionBean conbean = null;
        XMS.DATAOBJECTS.ConnectionBean xmsconnbean = null;

        private Timer mTimer;
        private int mDialogCount;

        Login login = null;
        public MainMenu(XMS.DATAOBJECTS.ConnectionBean conbeanobj, XMS.DATAOBJECTS.ConnectionBean xmsconbeanobj)
        {
            this.conbean = conbeanobj;
            this.xmsconnbean = xmsconbeanobj;
            ManageErrorLogOfApplication();


        }
        public void passLoginFormReference(Login loginobj)
        {
            this.login = loginobj;
            loginobj.Hide();

        }


        public void setXMSUser(BUSINESSOBJECTS.XMSUser xmsuserobj)
        {

            xmsuser = xmsuserobj;
            screen_collection_obj = new BUSINESSOBJECTS.ScreensCollection();
            // the default InitializeComponent() function is called here 
            // in oerder topopulate xms user object before  InitializeComponent()
            InitializeComponent();
            mTimer = new Timer();
            mTimer.Interval = 1800000;
            mTimer.Tick += LogoutUser;
            mTimer.Enabled = true;
            Application.AddMessageFilter(this);
            
            // Full screen code of main menu start
            // Prevent form from being resized. 
            FormBorderStyle = FormBorderStyle.FixedSingle;
            // Get the screen bounds 
            Rectangle formrect = Screen.GetBounds(this);
            formrect.Height = 740;
            // Set the form's location and size 
            Location = formrect.Location;
            Size = formrect.Size;

            // End here

            populateTree(xmsuser);


        }
        public BUSINESSOBJECTS.XMSUser getXmsUser()
        {
            return this.xmsuser;
        }

        public void populateTree(BUSINESSOBJECTS.XMSUser xmsuser)
        {
            // XMS.DATAOBJECTS.ConnectionBean connbean = new XMS.DATAOBJECTS.ConnectionBean("xms","sb13446","sb13446");


            // SqlConnection sqlconn = this.xmsconnbean.getDatabaseConnection();
            string functionid = null;
            string strsql = null;
            SqlDataReader reader = null;
            string functionshort = null; string functionname = null; string objectname = null;
            string ModuleName = null; string ScreenType = null; string ScreenName = null;

            BUSINESSOBJECTS.PrepareTreeNodes preparenodesobj = new BUSINESSOBJECTS.PrepareTreeNodes(this.xmsconnbean, this.xmsuser);
            preparenodesobj.populateTree();
            //MessageBox.Show("Module Objects:" + preparenodesobj.modulecontainer_hash.Count.ToString());
            //MessageBox.Show("Screen objects:" + preparenodesobj.screenobjects_hash.Count.ToString());
            SortedList modulecontainer_hash = preparenodesobj.modulecontainer_hash;
            SortedList screenobjects_hash = preparenodesobj.screenobjects_hash;
            IEnumerator ie_module = modulecontainer_hash.Values.GetEnumerator();
            IEnumerator ie_screen = preparenodesobj.screenobjects_hash.Values.GetEnumerator();
            // obtain new enumerator on screen
            IEnumerator ie_screen2 = preparenodesobj.screenobjects_hash.Values.GetEnumerator();

            BUSINESSOBJECTS.ScreenNode screenode = null;
            string modulename = null;
            // prepare 1st level Module Name Menu            
            while (ie_module.MoveNext() && ie_module.Current != null)
            {
                modulename = (string)ie_module.Current;
                System.Windows.Forms.TreeNode tn_0 = new TreeNode();
                tn_0.Text = modulename;
                tn_0.Name = modulename;
                treeMainMenu.Nodes[0].Nodes.Add(tn_0);
            }
            
            // prepare 2nd level Module Type Menu
            // code is commented for checking
            TreeNodeCollection modulenodecoll = treeMainMenu.Nodes[0].Nodes;
            IEnumerator iemodulenodecoll = modulenodecoll.GetEnumerator();
            //TreeNode modulenode = null;
            //while (iemodulenodecoll.MoveNext() && iemodulenodecoll.Current != null)
            //{
            //    modulenode = (TreeNode)iemodulenodecoll.Current;

            //    System.Windows.Forms.TreeNode tn_1 = new TreeNode();
            //    tn_1.Text = "Forms";
            //    tn_1.Name = "Forms";
            //    treeMainMenu.Nodes[0].Nodes[modulenode.Index].Nodes.Add(tn_1);

            //    System.Windows.Forms.TreeNode tn_2 = new TreeNode();
            //    tn_2.Text = "Reports";
            //    tn_2.Name = "Reports";
            //    treeMainMenu.Nodes[0].Nodes[modulenode.Index].Nodes.Add(tn_2);
            //}
            // code is commented for checking

            // -- new code for sub menu starts here 

            TreeNode modulenode_check = null;
            while (ie_screen.MoveNext() && ie_screen.Current != null)
            {
                screenode = (BUSINESSOBJECTS.ScreenNode)ie_screen.Current;
                functionid = screenode.getFunctionId();
                ModuleName = screenode.getModuleName();
                objectname = screenode.getObjectName();
                ScreenName = screenode.getScreenName();
                ScreenType = screenode.getScreenType();
                IEnumerator iemodulenodecoll_1 = modulenodecoll.GetEnumerator();
                while (iemodulenodecoll_1.MoveNext() && iemodulenodecoll_1.Current != null)
                {
                    modulenode_check = (TreeNode)iemodulenodecoll_1.Current;
                    if (modulenode_check.Name.Equals(ModuleName))
                    {

                        if (treeMainMenu.Nodes[0].Nodes[modulenode_check.Index].Nodes.ContainsKey(ScreenType))
                        {
                             //    // do  nothing
                           // MessageBox.Show("inside if");
                        }
                        else
                        {
                            System.Windows.Forms.TreeNode tn_sub = new TreeNode();
                            tn_sub.Text = ScreenType;
                            tn_sub.Name = ScreenType;
                            treeMainMenu.Nodes[0].Nodes[modulenode_check.Index].Nodes.Add(tn_sub);
                            //MessageBox.Show("inside else");
                        }
                    }
                }

            }
            // -- new code for sub menu ends here 


            // prepare 3rd level node starts here
            //////TreeNode modulenode_check = null;
            TreeNode submodulenode = null;
            while (ie_screen2.MoveNext() && ie_screen2.Current != null)
            {
                screenode = (BUSINESSOBJECTS.ScreenNode)ie_screen2.Current;
                functionid = screenode.getFunctionId();
                ModuleName = screenode.getModuleName();
                objectname = screenode.getObjectName();
                ScreenName = screenode.getScreenName();
                ScreenType = screenode.getScreenType();
                IEnumerator iemodulenodecoll_1 = modulenodecoll.GetEnumerator();
                while (iemodulenodecoll_1.MoveNext() && iemodulenodecoll_1.Current != null)
                {
                    modulenode_check = (TreeNode)iemodulenodecoll_1.Current;
                    if (modulenode_check.Name.Equals(ModuleName))
                    {
                                // obtain enumerator for sub module
                                TreeNodeCollection submodulenodecoll = treeMainMenu.Nodes[0].Nodes[modulenode_check.Index].Nodes;                                
                                IEnumerator iesubmodule = submodulenodecoll.GetEnumerator();
                                while (iesubmodule.MoveNext() && iesubmodule.Current != null)
                                {
                                    submodulenode = (TreeNode)iesubmodule.Current;
                                    if (submodulenode.Name.Equals(ScreenType))
                                    {
                                        System.Windows.Forms.TreeNode tn_3 = new TreeNode();
                                        tn_3.Text = ScreenName;
                                        tn_3.Name = functionid;
                                        treeMainMenu.Nodes[0].Nodes[modulenode_check.Index].Nodes[submodulenode.Index].Nodes.Add(tn_3);
                                    }
                                }
                        
                        
                                 //System.Windows.Forms.TreeNode tn_3 = new TreeNode();
                                 //tn_3.Text = ScreenName;
                                 //tn_3.Name = functionid;
                        
                        
                        
                        //////if (ScreenType.Equals("Forms"))
                        //////{
                        //////    treeMainMenu.Nodes[0].Nodes[modulenode_check.Index].Nodes[0].Nodes.Add(tn_3);
                        //////}
                        //////else if (ScreenType.Equals("Reports"))
                        //////{
                        //////    treeMainMenu.Nodes[0].Nodes[modulenode_check.Index].Nodes[1].Nodes.Add(tn_3);
                        //////}

                    }
                }
            }
                    // prepare 3rd level node starts here

        }

        public MainMenu()
        {
            //  InitializeComponent();

        }

        /// <summary>
        /// New code for tree population with double click event on it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                // BUSINESSOBJECTS.ScreensCollection screen_collection_obj = new BUSINESSOBJECTS.ScreensCollection();
                string final_mynodename = null;
                string final_mynodetext = null;

                // get handle of event initiaing ibject      
                System.Windows.Forms.TreeView tr = (System.Windows.Forms.TreeView)(sender);



                //===================================== code to check selected node starts here=================
                TreeNode selectedtreenode = tr.SelectedNode;
                final_mynodename = selectedtreenode.Name;
                final_mynodetext = selectedtreenode.Text;
                //MessageBox.Show("Selected Node Name:" + final_mynodename);
                //MessageBox.Show("Selected Node Text:" + final_mynodetext);
                selectedtreenode.ForeColor = System.Drawing.Color.Blue;
                //===================================== code to check selected node ends here=================

                bool form_flag = screen_collection_obj.checkFormInstance(final_mynodename);

                if (form_flag == true)
                {
                    // MessageBox.Show("Inside if block");

                    MessageBox.Show("Form Instance Already Running");
                    //System.Windows.Forms.Form frm = screen_collection_obj.FormCollection;                   
                    //frm.Show();
                }
                else
                {
                    // MessageBox.Show("Inside else block");

                    screen_collection_obj.addtoRunningFormsCollection(final_mynodename);
                    string ret_id = screen_collection_obj.openScreen(final_mynodename, this, screen_collection_obj, this.conbean);
                }
            }

            catch (Exception ex)
            {
                string err = ex.Message;
                err = "";
                //MessageBox.Show(err);
            }
        }

        // old code for tree population without double click event
        //private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        //{
        //    try
        //    {
        //        // BUSINESSOBJECTS.ScreensCollection screen_collection_obj = new BUSINESSOBJECTS.ScreensCollection();
        //        string final_mynodename = null;
        //        string final_mynodetext = null;

        //        // get handle of event initiaing ibject      
        //        System.Windows.Forms.TreeView tr = (System.Windows.Forms.TreeView)(sender);



        //        //===================================== code to check selected node starts here=================
        //        TreeNode selectedtreenode = tr.SelectedNode;
        //        final_mynodename = selectedtreenode.Name;
        //        final_mynodetext = selectedtreenode.Text;
        //        //MessageBox.Show("Selected Node Name:" + final_mynodename);
        //        //MessageBox.Show("Selected Node Text:" + final_mynodetext);
        //        selectedtreenode.ForeColor = System.Drawing.Color.Blue;
        //        //===================================== code to check selected node ends here=================

        //        bool form_flag = screen_collection_obj.checkFormInstance(final_mynodename);

        //        if (form_flag == true)
        //        {
        //            // MessageBox.Show("Inside if block");

        //            MessageBox.Show("Form Instance Already Running");
        //            //System.Windows.Forms.Form frm = screen_collection_obj.FormCollection;                   
        //            //frm.Show();
        //        }
        //        else
        //        {
        //            // MessageBox.Show("Inside else block");

        //            screen_collection_obj.addtoRunningFormsCollection(final_mynodename);
        //            string ret_id = screen_collection_obj.openScreen(final_mynodename, this, screen_collection_obj, this.conbean);
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        string err = ex.Message;
        //        //err = "";
        //        MessageBox.Show(err);
        //    }
        //}

        private void MainMenu_Load(object sender, EventArgs e)
        {
            //mdiMenu.AllowMerge = true;


        }
        private void MainMenu_FormClosing(object sender, EventArgs e)
        {
            // Close login form
            login.Close();
        }

        private void mdiMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void openWinToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //MainMenu main = new MainMenu();
            //iCORE.MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_PrepareExceptionReport  newMDIChild = new iCORE.MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_PrepareExceptionReport(main,conbean);
            //// Set the parent form of the child window.
            //newMDIChild.MdiParent = this;
            //// Display the new form.
            //newMDIChild.Show();

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Item click fire");
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        public void mycontrol_click(object sender, System.EventArgs e)
        {
            //ToolStripItem btn = (ToolStripItem)sender;


            //MessageBox.Show("form name menu item is click\n" + btn.Name);

        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //InterOpCls InterOp;
            //XmsInterface xmsInterOpInterface;
            //InterOp = new InterOpCls();
            //xmsInterOpInterface = (XmsInterface)InterOp;
            //xmsInterOpInterface.XMSLogOut();
            MainMenu main = new MainMenu();
            main.Close();


        }

        public void ManageErrorLogOfApplication()
        {
            // code starts for creating log file starts here this code will be appended to main menu file

            string complete_path = "C:/iCORE-Logs/FXRR-Logs/";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            string path = complete_path + filename;
            bool bool_icorelogs = false; bool bool_icorelogs_fxrr_logs = false;
            Console.WriteLine("Fiename:" + filename);

            if (Directory.Exists("C:/iCORE-Logs"))
            {
                Console.WriteLine("Directory Already Present");
                bool_icorelogs = true;
            }
            else
            {
                Console.WriteLine("Directory should be created");
                DirectoryInfo dr = Directory.CreateDirectory("C:/iCORE-Logs");
                bool_icorelogs = true;
            }


            if (bool_icorelogs)
            {
                if (Directory.Exists("C:/iCORE-Logs/FXRR-Logs"))
                {
                    bool_icorelogs_fxrr_logs = true;
                }
                else
                {
                    DirectoryInfo dr = Directory.CreateDirectory("C:/iCORE-Logs/FXRR-Logs");
                    bool_icorelogs_fxrr_logs = true;
                }
            }

            if (bool_icorelogs_fxrr_logs)
            {
                if (File.Exists(path))
                {
                    Console.WriteLine("File already present");
                }
                else
                {
                    Console.WriteLine("Create File");
                    File.Create(path);
                }
            }

            // code starts for creating log file ends here


        }

        // Expire User Session code
        public bool PreFilterMessage(ref Message m)
        {
            // Monitor message for keyboard and mouse messages
            bool active = m.Msg == 0x100 || m.Msg == 0x101;  // WM_KEYDOWN/UP
            active = active || m.Msg == 0xA0 || m.Msg == 0x200;  // WM_(NC)MOUSEMOVE
            active = active || m.Msg == 0x10;  // WM_CLOSE, in case dialog closes
            if (active)
            {
                if (!mTimer.Enabled) label1.Text = "Wakeup";
                //if (!mTimer.Enabled)
                mTimer.Enabled = false;
                mTimer.Start();
            }
            return false;
        }
        private void LogoutUser(object sender, EventArgs e)
        {
            try
            {
                // No activity, logout user
                if (mDialogCount > 0) return;
                mTimer.Enabled = false;
                label1.Text = "Time'z up";
                //this.Enabled = false;

                string message = "Your Session is Expired,Please re-login";
                string caption = "";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                // Displays the MessageBox.

                result = MessageBox.Show(this, message, caption, buttons,
                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.RightAlign);

                if (result == DialogResult.OK)
                {
                    //this.screencoll.removefromRunningFormsCollection(this.screenid);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                err = "";
            }

        }





    }
}