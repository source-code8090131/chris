using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace iCORE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_PrepareExceptionReport());
            //Application.Run(new MMRR.PRESENTATIONOBJECTS.FORMS.UpdateCallRepoRates());
            //Application.Run(new MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_CreateBroker());
            //Application.Run(new MMRR.PRESENTATIONOBJECTS.FORMS.MMRR_F_Upload_Top_Exception_Report_Data());
            //Application.Run(new MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayExceptionReport());
            //Application.Run(new MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayFwdRepo());
            //Application.Run(new MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayFwdOutrightPIB());
            //Application.Run(new MMRR.PRESENTATIONOBJECTS.REPORTS.MMRR_R_DisplayBrokerCallRepoRate());
            Application.Run(new XMS.PRESENTATIONOBJECTS.FORMS.Login());
            //Application.Run(new Form1());
            //Application.Run(new CBG.PRESENTATIONOBJECTS.FORMS.CRM_Item_Maintenance());
            //Application.Run(new CBG.PRESENTATIONOBJECTS.FORMS.CRM_Main());
            //Application.Run(new CBG.PRESENTATIONOBJECTS.FORMS.CRM_Set_Info());
            //Application.Run(new CBG.PRESENTATIONOBJECTS.FORMS.CRM_Set_Terms());

            //Application.Run(new  Form1());
        }
    }
}